package org.linuxtag.utils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * The JndiUtil handles the work for looking in the Jndi.
 */
public class JndiUtil {

	/**
	 * Instantiates a new jndi util.
	 */
	private JndiUtil() {
	}

	/** The project name. */
	private static String projectName = "eTicketBundle";

	/**
	 * Sets the project name.
	 * 
	 * @param projectName
	 *            the new project name
	 */
	public static void setProjectName(String projectName) {
		JndiUtil.projectName = projectName;
	}

	/**
	 * Looks in the JNDI for the given class.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param className
	 *            the class that you are looking for
	 * @return the class
	 */
	public static <T> T lookup(String className) {
		@SuppressWarnings("unchecked")
		T bean = (T) lookupObject("java:global/" + projectName + "/"
				+ className);
		return bean;
	}

	/**
	 * Looks in the JNDI for the given jndiName.
	 * 
	 * @param jndiName
	 *            the jndiName that you are looking for
	 * @return the class
	 */
	private static Object lookupObject(String jndiName) {
		Context context = null;
		try {
			context = new InitialContext();
			return context.lookup(jndiName);
		} catch (NamingException ex) {
			throw new IllegalStateException("Cannot connect to bean: "
					+ jndiName + " Reason: " + ex, ex.getCause());
		} finally {
			try {
				context.close();
			} catch (NamingException ex) {
				throw new IllegalStateException(
						"Cannot close InitialContext. Reason: " + ex,
						ex.getCause());
			}
		}
	}
}
