package org.linuxtag.eticket.userfrontend.exeption;

import org.linuxtag.eticket.userfrontend.locale.GenericLocale;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

// TODO: Auto-generated Javadoc
/**
 * The Class ErrorActionBean.
 */
@UrlBinding("/dispatcher/exception")
public class ErrorActionBean extends GenericLocale {
	
	/** The context. */
	private ActionBeanContext context;
	
	/** The errormsg. */
	private String errormsg;

	/* (non-Javadoc)
	 * @see net.sourceforge.stripes.action.ActionBean#setContext(net.sourceforge.stripes.action.ActionBeanContext)
	 */
	@Override
	public void setContext(ActionBeanContext context) {
		this.context = context;

	}

	/* (non-Javadoc)
	 * @see net.sourceforge.stripes.action.ActionBean#getContext()
	 */
	@Override
	public ActionBeanContext getContext() {
		return context;
	}

	/**
	 * Redirect to error page.
	 *
	 * @return the resolution
	 */
	@DefaultHandler
	public Resolution redirectToErrorPage() {
		context.getRequest().getSession()
				.setAttribute("errorMsg", getErrormsg());
		return new ForwardResolution("/velocity/error.vm");
	}

	/**
	 * Sets the errormsg.
	 *
	 * @param errormsg the new errormsg
	 */
	public void setErrormsg(String errormsg) {
		this.errormsg = errormsg;
	}

	/**
	 * Gets the errormsg.
	 *
	 * @return the errormsg
	 */
	public String getErrormsg() {
		return errormsg;
	}

}
