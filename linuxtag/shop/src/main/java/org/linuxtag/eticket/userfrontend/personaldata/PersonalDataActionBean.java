package org.linuxtag.eticket.userfrontend.personaldata;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;

/**
 * This class handles all incoming requests which are addressed to the personal
 * information.
 */
@UrlBinding("/dispatcher/personaldata")
public class PersonalDataActionBean extends GenericLocale implements
		ValidationErrorHandler {

	/** The salutation. */
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 1)
	private String salutation;

	/** The title. */
	@Validate(on = "savePersonalData", required = false, trim = true, maxlength = 100)
	private String title;

	/** The firstname. */
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String firstname;

	/** The lastname. */
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String lastname;

	/** The address. */
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String address;

	/** The zipcode. */
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String zipcode;

	/** The city. */
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String city;

	/** The orga. */
	private String orga;

	/** The context. */
	private ActionBeanContext context;

	/**
	 * Shows the form to insert the personal information.
	 * 
	 * @return the resolution
	 */
	@DefaultHandler
	public Resolution showPersonalData() {
		HttpSession session = getContext().getRequest().getSession();
		locale = (Locale) session.getAttribute("locale");
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		return new ForwardResolution(
				generateLocaleLink("/velocity/personaldata.vm"));
	}

	/**
	 * Save the Personal information in the Database.
	 * 
	 * @return the resolution
	 * @throws UnsupportedEncodingException
	 *             if is it not possible to get the Utf8 of the arguments
	 */
	public Resolution savePersonalData() throws UnsupportedEncodingException {
		HttpSession session = getContext().getRequest().getSession();
		PersonalInformationDTO personalInformationDTO = initPersonalInformation();
		session.setAttribute("personalInformation", personalInformationDTO);
		return new ForwardResolution(generateLocaleLink("/velocity/payment.vm"));
	}

	/**
	 * Initialise the petrsonalInformationDTO.
	 * 
	 * @return the personal information dto
	 * @throws UnsupportedEncodingException
	 *             if is it not possible to get the Utf8 of the arguments
	 */
	private PersonalInformationDTO initPersonalInformation()
			throws UnsupportedEncodingException {
		HttpSession session = getContext().getRequest().getSession();
		locale = (Locale) session.getAttribute("locale");
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(
				locale.getLanguage(), locale.getCountry());
		return PersonalInformationDTO.createNewPersonalInformationDTO(
				getUtf8(getSalutation()), getUtf8(getTitle()),
				getUtf8(getFirstname()), getUtf8(getLastname()),
				getUtf8(getAddress()), getUtf8(getZipcode()),
				getUtf8(getCity()), "", localeDTO, getUtf8(getOrga()),
				null);
	}

	/**
	 * If the given personal information are incorrect show the form with the
	 * Errors.
	 * 
	 * @param errors
	 *            the errors
	 * @return the resolution
	 */
	@Override
	public Resolution handleValidationErrors(ValidationErrors errors) {
		return new ForwardResolution("/velocity/personaldata.vm");
	}

	/**
	 * Gets the salutation.
	 * 
	 * @return the salutation
	 */
	public String getSalutation() {
		return salutation;
	}

	/**
	 * Sets the salutation.
	 * 
	 * @param salutation
	 *            the new salutation
	 * @throws UnsupportedEncodingException
	 *             if is it not possible to get the Utf8 of the arguments
	 */
	public void setSalutation(String salutation)
			throws UnsupportedEncodingException {
		this.salutation = salutation;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the utf8.
	 * 
	 * @param string
	 *            the string
	 * @return the utf8
	 * @throws UnsupportedEncodingException
	 *             if is it not possible to get the Utf8 of the arguments
	 */
	private String getUtf8(String string) throws UnsupportedEncodingException {
		if (string != null
				&& (getContext().getRequest().getCharacterEncoding() == null || getContext()
						.getRequest().getCharacterEncoding().equals("UTF-8")))
			return new String(string.getBytes("8859_1"), "UTF8");
		else
			return string;
	}

	/**
	 * Gets the firstname.
	 * 
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * Sets the firstname.
	 * 
	 * @param firstname
	 *            the new firstname
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * Gets the lastname.
	 * 
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * Sets the lastname.
	 * 
	 * @param lastname
	 *            the new lastname
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * Gets the address.
	 * 
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address
	 *            the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the zipcode.
	 * 
	 * @return the zipcode
	 */
	public String getZipcode() {
		return zipcode;
	}

	/**
	 * Sets the zipcode.
	 * 
	 * @param zipcode
	 *            the new zipcode
	 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * Gets the city.
	 * 
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 * 
	 * @param city
	 *            the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}	

	/**
	 * Sets the orga.
	 * 
	 * @param orga
	 *            the new orga
	 */
	public void setOrga(String orga) {
		this.orga = orga;
	}

	/**
	 * Gets the orga.
	 * 
	 * @return the orga
	 */
	public String getOrga() {
		return orga;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sourceforge.stripes.action.ActionBean#getContext()
	 */
	public ActionBeanContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.sourceforge.stripes.action.ActionBean#setContext(net.sourceforge.
	 * stripes.action.ActionBeanContext)
	 */
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}
}
