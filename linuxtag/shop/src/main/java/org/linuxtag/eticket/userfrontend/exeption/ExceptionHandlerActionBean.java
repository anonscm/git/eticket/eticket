package org.linuxtag.eticket.userfrontend.exeption;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.exception.DefaultExceptionHandler;

/**
 * The Class ExceptionHandlerActionBean.
 */
public class ExceptionHandlerActionBean extends DefaultExceptionHandler {

	/**
	 * Handle generic.
	 *
	 * @param exc the exc
	 * @param request the request
	 * @param response the response
	 * @return the resolution
	 */
	public Resolution handleGeneric(Exception exc, HttpServletRequest request,
			HttpServletResponse response) {
		Locale locale = (Locale) request.getSession().getAttribute("locale");

		StringWriter sw = new StringWriter();
		exc.printStackTrace(new PrintWriter(sw));
		String stacktrace = sw.toString();
		System.err.println(stacktrace);
		return new ForwardResolution("/dispatcher/exception?locale=" + locale
				+ "&errormsg=" + exc.getMessage());
	}
}

