package org.linuxtag.eticket.userfrontend.payment;

import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.business.account.AccountSAShopBeanService;
import org.evolvis.eticket.business.payment.PaymentBeanService;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.business.template.ShopTextTemplateBeanService;
import org.evolvis.eticket.model.entity.dto.AccountSAShopDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticketpayment.business.payment.GenericOrderBeanService;
import org.evolvis.eticketpayment.business.paypalorder.PayPalException;
import org.evolvis.eticketpayment.business.paypalorder.PayPalOrderBeanService;
import org.evolvis.eticketpayment.model.entity.OrderStatus;
import org.evolvis.eticketpayment.model.entity.dto.PayPalOrderDTO;
import org.evolvis.eticketshop.business.mail.EMailBeanService;
import org.evolvis.eticketshop.business.product.ShopProductBeanService;
import org.evolvis.eticketshop.model.entity.PaymentMethod;
import org.evolvis.eticketshop.model.entity.TextTemplateNames;
import org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * This class handles all incoming requests which are addressed to the payment.
 */
@UrlBinding("/dispatcher/payment")
public class PaymentActionBean extends GenericLocale {

	/** The context. */
	private ActionBeanContext context;

	/**
	 * Shows all PaymentOptions e.g. Paypal,...
	 *
	 * @return the resolution
	 */
	@DefaultHandler
	public Resolution showPaymentOptions() {
		HttpSession session = getContext().getRequest().getSession();
		locale = (Locale) session.getAttribute("locale");
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		return new ForwardResolution(generateLocaleLink("/velocity/payment.vm"));
	}	

	/**
	 * Shows the summary of the buying.
	 *
	 * @return the resolution
	 */
	public Resolution summary() {
		HttpSession session = getContext().getRequest().getSession();
		PaymentMethod paymentMethod = PaymentMethod.fromString(getContext()
				.getRequest().getParameter("payment_method"));
		session.setAttribute("quantity", 1);
		session.setAttribute("paymentMethod", paymentMethod);
		if (session.getAttribute("productuin").toString() != null
				&& paymentMethod != null) {
			return new ForwardResolution("/velocity/summary.vm");
		}
		return new ForwardResolution(generateLocaleLink("/velocity/payment.vm"));
	}

	/**
	 * Method redirects the user to the selected payment-provider.
	 * 
	 * @return redirect to payment-provider.
	 */
	@SuppressWarnings("unchecked")
	@HandlesEvent("redirectToPaymentProvider")
	public RedirectResolution showRedirectToPaymentProvider() {
		HttpSession session = getContext().getRequest().getSession();
		SystemBeanService systemBeanService = JndiUtil.lookup("SystemBean");
		ESystemDTO eSystemDTO = systemBeanService.getSystemByName(session
				.getAttribute("systemName").toString());
		GenericOrderBeanService genericOrderBeanService = JndiUtil
				.lookup(session.getAttribute("paymentMethod") + "OrderBean");

		ProductLocaleDTO productLocaleDTO = ((Map<String, ProductLocaleDTO>) session
				.getAttribute("ProductLocaleMap")).get((String) session
				.getAttribute("productuin"));

		long id;	
		
		int quantity = (Integer) session.getAttribute("quantity");

		String param[] = null;
		try {
			param = genericOrderBeanService.startPaymentProcess(null, productLocaleDTO,
					(String) session.getAttribute("productuin"), eSystemDTO,
					quantity);
			id = Long.valueOf(param[0]);
			session.setAttribute("order_id", id);
			
		} catch (PayPalException e) {
			Logger.getLogger("LOGGER").log(
					Level.SEVERE,
					"ShopActionBean.showRedirectToPaymentProvider - PayPalException: "
							+ e.getMessage());
		} catch (Exception e) {
			Logger.getLogger("LOGGER").log(
					Level.SEVERE,
					"ShopActionBean.showRedirectToPaymentProvider - Exception: "
							+ e.getMessage());
		}

		return new RedirectResolution(param[1], false);
	}

	/**
	 * if Paypal has been selected by user, the method is called to complete the
	 * checkout.
	 *
	 * @return page which shows if the transaction could be completed, or not.
	 */
	@HandlesEvent("completePayPalCheckout")
	public ForwardResolution completePayPalCheckout()  {
		HttpSession session = getContext().getRequest().getSession();
		String token = getContext().getRequest().getParameter("token");
		String payerId = getContext().getRequest().getParameter("PayerID");
		Long id = (Long) session.getAttribute("order_id");
		PayPalOrderBeanService payPalOrderBeanService = JndiUtil
				.lookup("PayPalOrderBean");	
		
		PayPalOrderDTO payPalOrderDTO = null;
		session.removeAttribute("bought");
		try {
			payPalOrderDTO = payPalOrderBeanService.completeTransaction(id,
					token, payerId);
			if (payPalOrderDTO != null && payPalOrderDTO.getOrderInformationDTO().getOrderStatus().equals(OrderStatus.COMPLETED) ) {
				completePayment(payPalOrderDTO);
			}else if(payPalOrderDTO != null) {
				paymentIsPending(payPalOrderDTO);
			}
			setOrderStatusToSession(payPalOrderDTO.getOrderInformationDTO()
					.getOrderStatus());
			updateProduct();
			getContext().getRequest().setAttribute("message", "confirm.bought");
		} catch (PayPalException e) {
			getContext().getRequest().setAttribute("message", "confirm.notBought");			
			Logger.getLogger("LOGGER").log(
					Level.SEVERE,
					"ShopActionBean.completePayPalCheckout - PayPalException: "
							+ e.getMessage());
		} catch (Exception e) {
			getContext().getRequest().setAttribute("message", "confirm.notBought");
			Logger.getLogger("LOGGER").log(
					Level.SEVERE,
					"ShopActionBean.completePayPalCheckout - Exception: "
							+ e.getMessage());
		}	
		
		return new ForwardResolution("/velocity/confirm.vm");
	}
	
	private void updateProduct(){
		HttpSession session = getContext().getRequest().getSession();
		ShopProductBeanService shopProductBeanService = JndiUtil
				.lookup("ShopProductBean");
		SystemBeanService systemBeanService = JndiUtil.lookup("SystemBean");
		String productuin = session.getAttribute("productuin").toString();
		String systemName = session.getAttribute("systemName").toString();
				
		ESystemDTO eSystemDTO = systemBeanService.getSystemByName(systemName);
		ShopProductDTO shopProductDTO = shopProductBeanService
				.getShopProcutDTOByProductUinAndProductSystem(productuin,
						eSystemDTO);
		
		ProductDTO productDTO = ProductDTO.createProductDTOFromExsistingProductEntity(shopProductDTO.getProduct().getUin(), shopProductDTO.getProduct().getProductType(),
				shopProductDTO.getProduct().getSystem(), shopProductDTO.getProduct().getProductLocales(), shopProductDTO.getProduct().getCurrAmount()+1, 
				shopProductDTO.getProduct().getMaxAmount(), shopProductDTO.getProduct().getDeadline(), shopProductDTO.getProduct().getExpiryTTL(), 
				shopProductDTO.getProduct().getId());
		ShopProductDTO updatedShopProduct = ShopProductDTO.createShopProductDTOFromExsistingShopProductEntity(shopProductDTO.getId(), productDTO, 
				shopProductDTO.isAvailable(), shopProductDTO.getProductAssociation());
		shopProductBeanService.updateShopProduct(updatedShopProduct);
	}
	
	/**
	 * Complete payment.
	 */
	private void completePayment(PayPalOrderDTO payPalOrderDTO){
		ShopProductBeanService shopProductBeanService = JndiUtil
				.lookup("ShopProductBean");
		AccountSAShopBeanService accountBeanService = JndiUtil.lookup("AccountSAShopBean");
		PaymentBeanService paymentBeanService = JndiUtil.lookup("PaymentBean");
		SystemBeanService systemBeanService = JndiUtil.lookup("SystemBean");
		HttpSession session = getContext().getRequest().getSession();
		String productuin = session.getAttribute("productuin").toString();
		String systemName = session.getAttribute("systemName").toString();
		ESystemDTO eSystemDTO = systemBeanService.getSystemByName(systemName);
		PersonalInformationDTO personalInformation = (PersonalInformationDTO) session
				.getAttribute("personalInformation");
			
		ShopProductDTO shopProductDTO = shopProductBeanService
				.getShopProcutDTOByProductUinAndProductSystem(productuin,
						eSystemDTO);
		locale = (Locale) session.getAttribute("locale");
		
		accountBeanService.cerate(personalInformation, shopProductDTO, payPalOrderDTO);
		AccountSAShopDTO accountSAShopDTO = accountBeanService.getAccount(payPalOrderDTO);		
		paymentBeanService.completePayment(locale, accountSAShopDTO);
	}
	
	private void paymentIsPending(PayPalOrderDTO payPalOrderDTO){
		EMailBeanService email = JndiUtil.lookup("EMailBean");
		ShopTextTemplateBeanService templateBeanService =JndiUtil.lookup("ShopTextTemplateBean");
		AccountSAShopBeanService accountBeanService = JndiUtil.lookup("AccountSAShopBean");
		ShopProductBeanService shopProductBeanService = JndiUtil
				.lookup("ShopProductBean");
		SystemBeanService systemBeanService = JndiUtil.lookup("SystemBean");
		HttpSession session = getContext().getRequest().getSession();
		
		PersonalInformationDTO personalInformation = (PersonalInformationDTO) session
				.getAttribute("personalInformation");
		EMailAccountDTO account = EMailAccountDTO.init(personalInformation.getEmail(), personalInformation.getFirstname()+" "+personalInformation.getLastname());
				
		String productuin = session.getAttribute("productuin").toString();
		String systemName = session.getAttribute("systemName").toString();
		ESystemDTO eSystemDTO = systemBeanService.getSystemByName(systemName);
		ShopProductDTO shopProductDTO = shopProductBeanService
				.getShopProcutDTOByProductUinAndProductSystem(productuin,
						eSystemDTO);
		
		accountBeanService.cerate(personalInformation, shopProductDTO, payPalOrderDTO);		
				
		locale = (Locale) session.getAttribute("locale");
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(locale.getLanguage(), locale.getCountry());	
		ShopTextTemplateDTO template = templateBeanService
				.find(payPalOrderDTO.getOrderInformationDTO().getSystem(), localeDTO, TextTemplateNames.PENDING_PRODUCT.toString());
		
		email.sendMail(payPalOrderDTO.getOrderInformationDTO().getSystem(), account, template, null, null);
	}

	/**
	 * Status must be set in Session to display information after an
	 * transaction.
	 *
	 * @param orderStatus the new order status to session
	 */
	private void setOrderStatusToSession(OrderStatus orderStatus) {
		Logger.getLogger("LOGGER").log(Level.INFO,
				"Order-Status: " + orderStatus.toString());
		HttpSession session = getContext().getRequest().getSession();
		session.setAttribute("OrderStatus", orderStatus.toString());
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.stripes.action.ActionBean#getContext()
	 */
	public ActionBeanContext getContext() {
		return context;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.stripes.action.ActionBean#setContext(net.sourceforge.stripes.action.ActionBeanContext)
	 */
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}
}
