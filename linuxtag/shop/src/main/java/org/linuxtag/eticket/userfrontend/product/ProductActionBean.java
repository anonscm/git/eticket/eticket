package org.linuxtag.eticket.userfrontend.product;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.product.ProductLinkSAShopBeanService;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticketshop.business.product.ShopProductBeanService;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.eticket.userfrontend.welcome.WelcomeActionBean;
import org.linuxtag.utils.JndiUtil;

/**
 * This class handles all incoming requests which are addressed to the product.
 * 
 */
@UrlBinding("/dispatcher/product")
public class ProductActionBean extends GenericLocale implements
		ValidationErrorHandler {

	/** The context. */
	private ActionBeanContext context;

	/** The productuin. */
	private String productuin;

	/** The system name. */
	private String systemName;

	@Validate(on = "resend", required = true, trim = true, minlength = 1, maxlength = 254, mask = "[^@]+@.+\\.[^.]+")
	private String email;

	/**
	 * Shows the Main page with all products you can buy.
	 * 
	 * @return the resolution
	 */
	@DefaultHandler
	public Resolution showWelcome() {
		return new ForwardResolution("/dispatcher/");
	}
	
	
	public Resolution showResend(){
		HttpSession session = getContext().getRequest().getSession();
		session.removeAttribute("bought");
		return new ForwardResolution("/velocity/resend.vm");
	}

	/**
	 * Shows the gtc that the User has to accept.
	 * 
	 * @return the resolution
	 */
	public Resolution buy() {
		HttpSession session = getContext().getRequest().getSession();
		session.setAttribute("productuin", productuin);
		session.setAttribute("systemName", systemName);
		session.setAttribute("bought", "true");
		return new ForwardResolution(generateLocaleLink("/dispatcher/gtc"));
	}

	/**
	 * Shows the detail page with more information about the product.
	 * 
	 * @return the resolution
	 */
	public Resolution showDetail() {
		HttpSession session = getContext().getRequest().getSession();
		ShopProductBeanService shopProductBeanService = JndiUtil
				.lookup("ShopProductBean");
		SystemBeanService systemBeanService = JndiUtil.lookup("SystemBean");
		ESystemDTO system = systemBeanService.getSystemByName(systemName);
		ShopProductDTO shopProductDTO = shopProductBeanService
				.getShopProcutDTOByProductUinAndProductSystem(productuin,
						system);
		session.setAttribute("shopProduct", shopProductDTO);
		List<ShopProductDTO> shopProductDTOs = new ArrayList<ShopProductDTO>();
		shopProductDTOs.add(shopProductDTO);

		new WelcomeActionBean().productLocalesFromActiveProductsToSession(
				shopProductDTOs, session);
		return new ForwardResolution("/velocity/productDetail.vm");
	}

	public Resolution resend() {
		ProductLinkSAShopBeanService productLinkSAShopBeanService = JndiUtil
				.lookup("ProductLinkSAShopBean");
		productLinkSAShopBeanService.resend(email);
		getContext().getRequest().setAttribute("message", "confirm.resend");
		return new ForwardResolution("/velocity/confirm.vm");
	}

	/**
	 * Gets the productuin.
	 * 
	 * @return the productuin
	 */
	public String getProductuin() {
		return productuin;
	}

	/**
	 * Sets the productuin.
	 * 
	 * @param productuin
	 *            the new productuin
	 */
	public void setProductuin(String productuin) {
		this.productuin = productuin;
	}

	/**
	 * Gets the system name.
	 * 
	 * @return the system name
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * Sets the system name.
	 * 
	 * @param systemName
	 *            the new system name
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sourceforge.stripes.action.ActionBean#getContext()
	 */
	public ActionBeanContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.sourceforge.stripes.action.ActionBean#setContext(net.sourceforge.
	 * stripes.action.ActionBeanContext)
	 */
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		return new ForwardResolution("/velocity/index.vm");
	}
}
