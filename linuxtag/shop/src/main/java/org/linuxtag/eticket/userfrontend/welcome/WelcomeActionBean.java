package org.linuxtag.eticket.userfrontend.welcome;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJBTransactionRolledbackException;
import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticketshop.business.product.ShopProductBeanService;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * This class handles all work to show the main page.
 */
@UrlBinding(value = "/dispatcher/")
public class WelcomeActionBean extends GenericLocale {

	/** The context. */
	private ActionBeanContext context;

	/* (non-Javadoc)
	 * @see net.sourceforge.stripes.action.ActionBean#getContext()
	 */
	public ActionBeanContext getContext() {
		return context;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.stripes.action.ActionBean#setContext(net.sourceforge.stripes.action.ActionBeanContext)
	 */
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	/**
	 * Shows the Main page with all products you can buy.
	 *
	 * @return the resolution
	 */
	@DefaultHandler
	public Resolution showWelcome() {
		HttpSession session = getContext().getRequest().getSession();
		ShopProductBeanService shopProductBeanService = JndiUtil
				.lookup("ShopProductBean");
		List<ShopProductDTO> shopProductDTOs = shopProductBeanService
				.getAllShopProductDTOFromShop(true,
						ProductAssociation.SHOP_EXTERN);
		session.setAttribute("shopProduct", shopProductDTOs);
		productLocalesFromActiveProductsToSession(shopProductDTOs, session);
		session.removeAttribute("bought");
		return new ForwardResolution("/velocity/index.vm");
	}
	
	/**
	 * Save all ProductLocales from the given ShopProducts in the session.
	 *
	 * @param shopProductDTOs the shop product dt os
	 * @param session the session
	 */
	public void productLocalesFromActiveProductsToSession(List<ShopProductDTO> shopProductDTOs, HttpSession session) {
		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		
		Map<String, ProductLocaleDTO> productLocale = new HashMap<String, ProductLocaleDTO>();
		
		locale = (Locale) session.getAttribute("locale");
		
		for (ShopProductDTO spDTO : shopProductDTOs) {
			ProductLocaleDTO productLocaleDTO = null;
			try {
				productLocaleDTO = productBeanService.getProductLocale(
						locale.toString(), spDTO.getProduct().getUin(),
						spDTO.getProduct().getSystem().getId());
			} catch (EJBTransactionRolledbackException e) {
				// TODO Exception Behandlung - diese tritt auf wenn keine
				// passendes ProductLocale existiert. soll ich dies weiter
				// behandlen?
			}

			if (productLocaleDTO != null) {
				// Add corresponding productLocaleDTO
				productLocale
						.put(spDTO.getProduct().getUin(), productLocaleDTO);
			}
		}

		session.setAttribute("ProductLocaleMap", productLocale);
	}	
}
