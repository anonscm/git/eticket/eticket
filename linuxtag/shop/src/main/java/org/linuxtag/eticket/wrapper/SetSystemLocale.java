package org.linuxtag.eticket.wrapper;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class handles the work to set a other language.
 */
public class SetSystemLocale extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2781651961927950858L;

	/** The Constant LOCALE. */
	public static final String LOCALE = "locale";
	
	/**
	 * Set a other language for the Gui.
	 *
	 * @param req the request
	 * @param resp the response
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		try {
			String[] values = null;
			
			if (req.getPathInfo() != null)
				values = req.getPathInfo().split("/");
			else
				throw new IOException("No extra path.");
			if (values.length != 2 || values[1].isEmpty())
				throw new IOException("Wrong extra path.");
			Locale locale = new Locale(values[1]);
			
			req.getSession().setAttribute(LOCALE, locale);
			// we need to use redirect thereby the client knows about the url
			// change
			if(req.getSession().getAttribute("state") != null){
				resp.sendRedirect("/LinuxTagShop/dispatcher/editpersonaldata?locale="
					+ locale);
			}else{
			resp.sendRedirect("/LinuxTagShop/dispatcher/welcome?locale="
					+ locale);
			}
		} catch (Exception e) {
			try {
				resp.sendRedirect("/LinuxTagShop/dispatcher/exception?locale=en&errorMsg="
						+ e);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
}
