package org.linuxtag.eticket.userfrontend.locale;

import java.util.Locale;

import net.sourceforge.stripes.action.ActionBean;

/**
 * Stripes set the locale with parameter, every link has to contain a locale parameter.
 */
public abstract class GenericLocale implements ActionBean {
	
	/** The LOCALE. */
	public final String LOCALE = "locale";
	
	/** The locale. */
	protected Locale locale;

	/**
	 * Generate locale link.
	 *
	 * @param url the url
	 * @return the url with the locale
	 */
	protected String generateLocaleLink(String url) {
		return url + "?" + LOCALE + "=" + locale;
	}

	/**
	 * Sets the locale.
	 *
	 * @param locale the new locale
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * Gets the locale.
	 *
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}
}
