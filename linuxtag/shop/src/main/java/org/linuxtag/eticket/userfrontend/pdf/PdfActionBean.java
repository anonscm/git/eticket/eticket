package org.linuxtag.eticket.userfrontend.pdf;

import java.io.ByteArrayOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.business.account.AccountSAShopBeanService;
import org.evolvis.eticket.business.pdf.PrintSAShopProductPdfBeanService;
import org.evolvis.eticket.model.entity.dto.AccountSAShopDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * This class handles all incoming requests which are addressed to the pdf.
 */
@UrlBinding("/dispatcher/pdf")
public class PdfActionBean extends GenericLocale {

	/** The Constant APPLICATION_PDF. */
	private static final String APPLICATION_PDF = "application/pdf";

	/** The context. */
	private ActionBeanContext context;

	/**
	 * Gets the pdf with the given uin.
	 * 
	 * @return the pdf
	 */
	@DefaultHandler
	public Resolution getPdf() {
		String uin = context.getRequest().getParameter("token");
		AccountSAShopBeanService accountSAShopBeanService = JndiUtil
				.lookup("AccountSAShopBean");
		PrintSAShopProductPdfBeanService printSAShopProductPdfBean = JndiUtil
				.lookup("PrintSAShopProductPdfBean");
		AccountSAShopDTO account = accountSAShopBeanService.getAccount(uin);
		return getPdf(printSAShopProductPdfBean.createPDF(account
				.getPersonalInformation(), account.getProductLink()
				.getShopProductDTO().getProduct().getUin(), false, account
				.getPersonalInformation().getLocaleDTO().getCode(), account
				.getProductLink().getShopProductDTO().getProduct().getSystem(),
				account.getProductLink().getProductNumber()), account
				.getProductLink().getShopProductDTO().getProduct());
	}

	/**
	 * Shows the Pdf in the browser.
	 * 
	 * @param pdf
	 *            the pdf
	 * @param productDTO
	 *            the Product of the Pdf
	 * @return the pdf
	 */
	private Resolution getPdf(final byte[] pdf, final ProductDTO productDTO) {
		return new Resolution() {

			@Override
			public void execute(HttpServletRequest request,
					HttpServletResponse response) throws Exception {
				HttpServletResponse res = getContext().getResponse();

				res.setHeader("Content-Disposition",
						"inline; attachment; filename=" + productDTO.getUin()
								+ ".pdf");
				res.setContentType(APPLICATION_PDF);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				baos.write(pdf);
				res.setContentLength(baos.size());
				ServletOutputStream out = res.getOutputStream();
				baos.writeTo(out);
				out.flush();
				out.close();
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sourceforge.stripes.action.ActionBean#getContext()
	 */
	public ActionBeanContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.sourceforge.stripes.action.ActionBean#setContext(net.sourceforge.
	 * stripes.action.ActionBeanContext)
	 */
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

}
