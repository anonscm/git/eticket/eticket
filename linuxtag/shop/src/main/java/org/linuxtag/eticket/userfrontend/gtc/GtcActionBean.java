package org.linuxtag.eticket.userfrontend.gtc;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticketshop.business.gtc.GtcShopBeanService;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.dto.GtcShopDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;


@UrlBinding("/dispatcher/gtc")
/**
 * This class handles all incoming requests which are addressed to the general terms and conditions .
 * 
 * @author mjohnk
 *
 */
public class GtcActionBean extends GenericLocale{

	/** The context. */
	private ActionBeanContext context;

	/**
	 * This method shows the gtc.
	 *
	 * @return the resolution
	 */
	@DefaultHandler
	public Resolution showGtc(){
		HttpSession session = getContext().getRequest().getSession();
		Locale locale = (Locale) session.getAttribute("locale");
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(locale.getLanguage(), locale.getCountry());
		GtcShopBeanService gtcShopBeanService = JndiUtil.lookup("GtcShopBean");
		GtcShopDTO gtc = gtcShopBeanService.findByProductAssociation(ProductAssociation.SHOP_EXTERN, localeDTO);
		session.setAttribute("gtc", gtc);
		return new ForwardResolution(generateLocaleLink("/velocity/gtc.vm"));
	}
	
	/* (non-Javadoc)
	 * @see net.sourceforge.stripes.action.ActionBean#getContext()
	 */
	public ActionBeanContext getContext() {
		return context;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.stripes.action.ActionBean#setContext(net.sourceforge.stripes.action.ActionBeanContext)
	 */
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}	

}
