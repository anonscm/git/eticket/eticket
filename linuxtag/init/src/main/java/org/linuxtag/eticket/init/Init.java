package org.linuxtag.eticket.init;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.platform.PlatformBeanService;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.ProductNumberBeanService;
import org.evolvis.eticket.business.product.ProductTransferService;
import org.evolvis.eticket.business.product.TransferMethod;
import org.evolvis.eticket.business.system.ConfigurationParameterBeanService;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.business.system.TextTemplateBeanService;
import org.evolvis.eticket.business.template.ShopTextTemplateBeanService;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductNumberDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO.TextTemplateNames;
import org.evolvis.eticket.util.LoadPropertiesFile;
import org.evolvis.eticketpayment.business.invoice.InvoiceBeanService;
import org.evolvis.eticketpayment.business.system.PayPalConfigurationBeanService;
import org.evolvis.eticketpayment.model.entity.dto.InvoiceNumberDTO;
import org.evolvis.eticketpayment.model.entity.dto.InvoiceTemplateDTO;
import org.evolvis.eticketshop.business.gtc.GtcShopBeanService;
import org.evolvis.eticketshop.business.product.ShopProductBeanService;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.dto.GtcShopDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO;
import org.linuxtag.utils.JndiUtil;

/**
 * This class is to init the Application
 * 
 * @author mjohnk
 * 
 */
@UrlBinding(value = "/dispatcher/")
public class Init implements ActionBean {

	private ActionBeanContext context;
	private ProductDTO eticket;
	private SystemBeanService systemBeanRemote;
	private PlatformBeanService platformBeanRemote;
	private TextTemplateBeanService textTemplateBeanRemote;
	private ConfigurationParameterBeanService configurationParameterBeanRemote;
	private PayPalConfigurationBeanService payPalConfigurationBeanService;
	private ProductBeanService productBeanRemote;
	private AccountBeanService accountBeanRemote;
	private ProductNumberBeanService productNumberBeanRemote;
	private ShopProductBeanService shopProductBeanRemote;
	private GtcShopBeanService gtcShopBeanRemote;
	private InvoiceBeanService invoiceBeanRemote;
	private ProductTransferService productTransferService;
	private ShopTextTemplateBeanService shopTextTemplateBeanRemote;
	private AccountDTO platformAdmin = AccountDTO.getPlatformAccount(
			"linuxtag", "2012", null);
	private long platformId;
	private LocaleDTO de = LocaleDTO.createNewLocaleDTO("de", "deutsch");
	private LocaleDTO en = LocaleDTO.createNewLocaleDTO("en", "english");
	private LocaleDTO locales[] = { de, en };
	private ESystemDTO systemDTO = ESystemDTO.createNewESystemDTO("linuxtag",
			en, Arrays.asList(locales));
	private AccountDTO sysAdmin = AccountDTO.init(systemDTO,
			"linuxtag", "2012", RoleDTO.getSysAdmin(0), null);

	private TemplatesDTO templatesDTO;
	private AccountDTO accountDTO;
	private AccountDTO accountDTO2;
	private ProductDTO productDTO_intern1;
	private ProductDTO productDTO_intern2;
	private ProductDTO productDTO_extern1;
	private ProductDTO productDTO_extern2;

	@DefaultHandler
	public void init() throws IOException, IllegalAccessException {
		platformBeanRemote = JndiUtil.lookup("PlatformBean");
		systemBeanRemote = JndiUtil.lookup("SystemBean");
		productBeanRemote = JndiUtil.lookup("ProductBean");
		accountBeanRemote = JndiUtil.lookup("AccountBean");
		textTemplateBeanRemote = JndiUtil.lookup("TextTemplateBean");
		configurationParameterBeanRemote = JndiUtil
				.lookup("ConfigurationParameterBean");
		payPalConfigurationBeanService = JndiUtil
				.lookup("PayPalConfigurationBean");
		productBeanRemote = JndiUtil.lookup("ProductBean");
		productTransferService = JndiUtil.lookup("ProductTransferBean");
		accountBeanRemote = JndiUtil.lookup("AccountBean");
		productNumberBeanRemote = JndiUtil.lookup("ProductNumberBean");
		shopProductBeanRemote = JndiUtil.lookup("ShopProductBean");
		gtcShopBeanRemote = JndiUtil.lookup("GtcShopBean");
		invoiceBeanRemote = JndiUtil.lookup("InvoiceBean");
		shopTextTemplateBeanRemote = JndiUtil.lookup("ShopTextTemplateBean");
		try {
			platformId = platformBeanRemote
					.createPlatformAndReturnId(platformAdmin);
			createLocale();
			systemBeanRemote.createSystem(platformAdmin, platformId, systemDTO);
			createTextTemplate();
			createCp();
			createPayPalConfiguration();
			createAccount();
			createTemplate();
			createProductAndNumber();
			setTransferSystemProductToAccount();
			setTransferProductByMethodcall();
			createShopProduct();
			createGtc();
			createInvoiceTemplate();
			createInvoiceNumber();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	private void createLocale() {
		platformBeanRemote.createLocale(platformAdmin, platformId, de);
		platformBeanRemote.createLocale(platformAdmin, platformId, en);
	}

	private void createAccount() {
		AccountDTO helpadmin = AccountDTO.init(systemDTO,
				platformAdmin.getUsername(), platformAdmin.getPassword(),
				platformAdmin.getRole(), null);
		PersonalInformationDTO persinfo = PersonalInformationDTO
				.createNewPersonalInformationDTO("m", "Dr", " Dent", "best",
						"Hinter den 7 Bergen", "01245", "Kapphausen",
						"germany", en, "ritter aus leidenschaft",
						"d.gilles@tarent.de");
		accountBeanRemote.UpdatePersonalInformation(helpadmin, persinfo);

		accountDTO = AccountDTO.init(systemDTO,
				"m.johnki@gmx.de", "1", RoleDTO.getUser(systemDTO.getId()),
				null);
		accountBeanRemote.create(sysAdmin, accountDTO);
		persinfo = PersonalInformationDTO.createNewPersonalInformationDTO("m",
				"Dr", " gmx", "de", "Hinter den 7 Bergen", "01245",
				"Kapphausen", "germany", en, "ritter aus leidenschaft",
				"d.gilles@tarent.de");
		accountBeanRemote.UpdatePersonalInformation(accountDTO, persinfo);

		accountDTO2 = AccountDTO.init(systemDTO,
				"m.johnki@web.de", "1", RoleDTO.getUser(systemDTO.getId()),
				null);
		accountBeanRemote.create(sysAdmin, accountDTO2);
		persinfo = PersonalInformationDTO.createNewPersonalInformationDTO("m",
				"Dr", " web", "de", "Hinter den 7 Bergen", "01245",
				"Kapphausen", "germany", en, "ritter aus leidenschaft",
				"d.gilles@tarent.de");
		accountBeanRemote.UpdatePersonalInformation(accountDTO2, persinfo);
	}

	private void createTextTemplate() {
		Properties properties = (new LoadPropertiesFile())
				.open("new_account_en.properties");
		TextTemplateDTO new_account_en = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, en,
						TextTemplateNames.NEW_ACCOUNT,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("new_account_de.properties");
		TextTemplateDTO new_account_de = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, de,
						TextTemplateNames.NEW_ACCOUNT,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("sent_product_en.properties");
		TextTemplateDTO sent_product_en = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, en,
						TextTemplateNames.SENT_PRODUCT,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("sent_product_de.properties");
		TextTemplateDTO sent_product_de = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, de,
						TextTemplateNames.SENT_PRODUCT,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("received_product_en.properties");
		TextTemplateDTO received_product_en = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, en,
						TextTemplateNames.RECEIVED_PRODUCT,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("received_product_de.properties");
		TextTemplateDTO received_product_de = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, de,
						TextTemplateNames.RECEIVED_PRODUCT,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("decline_product_en.properties");
		TextTemplateDTO decline_product_en = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, en,
						TextTemplateNames.DECLINE_PRODUCT,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("decline_product_de.properties");
		TextTemplateDTO decline_product_de = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, de,
						TextTemplateNames.DECLINE_PRODUCT,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("received_product_send_by_system_en.properties");
		TextTemplateDTO received_product_by_system_en = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, en,
						TextTemplateNames.RECEIVED_PRODUCT_BY_SYSTEM,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("received_product_send_by_system_de.properties");
		TextTemplateDTO received_product_by_system_de = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, de,
						TextTemplateNames.RECEIVED_PRODUCT_BY_SYSTEM,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("canceled_product_en.properties");
		TextTemplateDTO canceled_product_en = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, en,
						TextTemplateNames.CANCELED_PRODUCT,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("canceled_product_de.properties");
		TextTemplateDTO canceled_product_de = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, de,
						TextTemplateNames.CANCELED_PRODUCT,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("bought_product_extern_en.properties");
		ShopTextTemplateDTO bought_product_extern_en = ShopTextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						en,
						org.evolvis.eticketshop.model.entity.TextTemplateNames.BOUGHT_PRODUCT_EXTERN,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("bought_product_extern_de.properties");
		ShopTextTemplateDTO bought_product_extern_de = ShopTextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						de,
						org.evolvis.eticketshop.model.entity.TextTemplateNames.BOUGHT_PRODUCT_EXTERN,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("bought_product_intern_en.properties");
		ShopTextTemplateDTO bought_product_intern_en = ShopTextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						en,
						org.evolvis.eticketshop.model.entity.TextTemplateNames.BOUGHT_PRODUCT_INTERN,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("bought_product_intern_de.properties");
		ShopTextTemplateDTO bought_product_intern_de = ShopTextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						de,
						org.evolvis.eticketshop.model.entity.TextTemplateNames.BOUGHT_PRODUCT_INTERN,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("pending_product_en.properties");
		ShopTextTemplateDTO pending_product_en = ShopTextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						en,
						org.evolvis.eticketshop.model.entity.TextTemplateNames.PENDING_PRODUCT,
						properties.getProperty("VALUE"));

		properties = (new LoadPropertiesFile())
				.open("pending_product_de.properties");
		ShopTextTemplateDTO pending_product_de = ShopTextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						de,
						org.evolvis.eticketshop.model.entity.TextTemplateNames.PENDING_PRODUCT,
						properties.getProperty("VALUE"));

		textTemplateBeanRemote.insert(sysAdmin, new_account_en);
		textTemplateBeanRemote.insert(sysAdmin, new_account_de);
		textTemplateBeanRemote.insert(sysAdmin, sent_product_en);
		textTemplateBeanRemote.insert(sysAdmin, sent_product_de);
		textTemplateBeanRemote.insert(sysAdmin, received_product_en);
		textTemplateBeanRemote.insert(sysAdmin, received_product_de);
		textTemplateBeanRemote.insert(sysAdmin, decline_product_en);
		textTemplateBeanRemote.insert(sysAdmin, decline_product_de);
		textTemplateBeanRemote.insert(sysAdmin, received_product_by_system_en);
		textTemplateBeanRemote.insert(sysAdmin, received_product_by_system_de);
		textTemplateBeanRemote.insert(sysAdmin, canceled_product_en);
		textTemplateBeanRemote.insert(sysAdmin, canceled_product_de);
		shopTextTemplateBeanRemote.insert(sysAdmin, bought_product_extern_en);
		shopTextTemplateBeanRemote.insert(sysAdmin, bought_product_extern_de);
		shopTextTemplateBeanRemote.insert(sysAdmin, bought_product_intern_en);
		shopTextTemplateBeanRemote.insert(sysAdmin, bought_product_intern_de);
		shopTextTemplateBeanRemote.insert(sysAdmin, pending_product_en);
		shopTextTemplateBeanRemote.insert(sysAdmin, pending_product_de);
	}

	private void createCp() {
		configurationParameterBeanRemote.insertCP(sysAdmin, systemDTO,
				CPKey.SYSTEM_DOMAIN,
				"https://eticket.linuxtag.org/LinuxTagUserWeb");
		configurationParameterBeanRemote.insertCP(sysAdmin, systemDTO,
				CPKey.SYSTEM_SENDER, "eticket@linuxtag.org");
		configurationParameterBeanRemote.insertCP(sysAdmin, systemDTO,
				CPKey.ACCOUNT_VALIDATION_LIMIT, "" + 10000);
		configurationParameterBeanRemote.insertCP(sysAdmin, systemDTO,
				CPKey.SHOP_DOMAIN, "https://eticket.linuxtag.org/LinuxTagShop");
		configurationParameterBeanRemote.insertCP(sysAdmin, systemDTO,
				CPKey.FIXED_TICKETNUMBER, "21111101");
	}

	private void createPayPalConfiguration() {
		// Interne-Config
		payPalConfigurationBeanService.insertConfiguration("USER",
				"b.krap_1327405301_biz_api1.tarent.de",
				ProductAssociation.SHOP_INTERN, systemDTO);
		payPalConfigurationBeanService.insertConfiguration("PWD", "1327405331",
				ProductAssociation.SHOP_INTERN, systemDTO);
		payPalConfigurationBeanService.insertConfiguration("SIGNATURE",
				"Aj.VSuWefenYfx9Vhzix0K1gYHDaAUTRZDpu0l4rfmShkjJoIbd3n4Vx",
				ProductAssociation.SHOP_INTERN, systemDTO);
		payPalConfigurationBeanService
				.insertConfiguration(
						"RETURNURL",
						"https://eticket.linuxtag.org/LinuxTagUserWeb/dispatcher/shop?completePayPalCheckout",
						ProductAssociation.SHOP_INTERN, systemDTO);
		payPalConfigurationBeanService
				.insertConfiguration(
						"CANCELURL",
						"https://eticket.linuxtag.org/LinuxTagUserWeb/dispatcher/shop?canclepayment",
						ProductAssociation.SHOP_INTERN, systemDTO);
		payPalConfigurationBeanService.insertConfiguration("PAYPALAPI",
				"https://api-3t.sandbox.paypal.com/nvp",
				ProductAssociation.SHOP_INTERN, systemDTO);
		payPalConfigurationBeanService
				.insertConfiguration(
						"PAYPALPAY",
						"https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&useraction=commit&token=",
						ProductAssociation.SHOP_INTERN, systemDTO);
		payPalConfigurationBeanService.insertConfiguration("VERSION", "84.0",
				ProductAssociation.SHOP_INTERN, systemDTO);
		payPalConfigurationBeanService.insertConfiguration(
				"PAYMENTREQUEST_0_PAYMENTACTION", "Sale",
				ProductAssociation.SHOP_INTERN, systemDTO);

		// Externe-Config
		payPalConfigurationBeanService.insertConfiguration("USER",
				"b.krap_1327405301_biz_api1.tarent.de",
				ProductAssociation.SHOP_EXTERN, systemDTO);
		payPalConfigurationBeanService.insertConfiguration("PWD", "1327405331",
				ProductAssociation.SHOP_EXTERN, systemDTO);
		payPalConfigurationBeanService.insertConfiguration("SIGNATURE",
				"Aj.VSuWefenYfx9Vhzix0K1gYHDaAUTRZDpu0l4rfmShkjJoIbd3n4Vx",
				ProductAssociation.SHOP_EXTERN, systemDTO);
		payPalConfigurationBeanService
				.insertConfiguration(
						"RETURNURL",
						"https://eticket.linuxtag.org/LinuxTagShop/dispatcher/payment?completePayPalCheckout",
						ProductAssociation.SHOP_EXTERN, systemDTO);
		payPalConfigurationBeanService
				.insertConfiguration(
						"CANCELURL",
						"https://eticket.linuxtag.org/LinuxTagShop/dispatcher/payment?canclepayment",
						ProductAssociation.SHOP_EXTERN, systemDTO);
		payPalConfigurationBeanService.insertConfiguration("PAYPALAPI",
				"https://api-3t.sandbox.paypal.com/nvp",
				ProductAssociation.SHOP_EXTERN, systemDTO);
		payPalConfigurationBeanService
				.insertConfiguration(
						"PAYPALPAY",
						"https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&useraction=commit&token=",
						ProductAssociation.SHOP_EXTERN, systemDTO);
		payPalConfigurationBeanService.insertConfiguration("VERSION", "84.0",
				ProductAssociation.SHOP_EXTERN, systemDTO);
		payPalConfigurationBeanService.insertConfiguration(
				"PAYMENTREQUEST_0_PAYMENTACTION", "Sale",
				ProductAssociation.SHOP_EXTERN, systemDTO);
	}

	private void createTemplate() throws IOException {
		InputStream is = Init.class.getResourceAsStream("/eticket.pdf");

		byte[] pdfFile = toByteArray(is);
		templatesDTO = TemplatesDTO
				.createNewTemplatesDTO(
						"TP",
						pdfFile,
						"application/pdf",
						"Das ist das LinuxtagticketPDFTemplate, es wird als Vorlage für das eTicketsystem, aber auch für die beiden Shops verwendet.");

		productBeanRemote.putTemplate(sysAdmin, templatesDTO);
	}

	private void createProductAndNumber() {

		ProductLocaleDTO[] productLocales_P1 = {
				ProductLocaleDTO
						.createNewProductLocaleDTO(
								de,
								"EUR",
								"Linuxtag 4-Tagesticket",
								"Mit dem 4-Tagesticket können sie die gesamte Linuxtag-messe wahrnehmen. Sie können damit beliebig oft die Messe betreten und verlassen",
								templatesDTO, 21.00, 0.19),
				ProductLocaleDTO
						.createNewProductLocaleDTO(
								en,
								"USD",
								"Linuxtag 4-day Ticket",
								"You can visit all days of the Linuxtag fair with this ticket. You can enter the fair as often as you want. ",
								templatesDTO, 28.19, 0.19) };

		ProductLocaleDTO[] productLocales_P2 = {
				ProductLocaleDTO
						.createNewProductLocaleDTO(
								de,
								"EUR",
								"Socialevent",
								"Dieses Ticket erlaubt ihnen die Teilnahme am abendlichen Mee-Greet beim Social_event.",
								templatesDTO, 29.41, 0.19),
				ProductLocaleDTO
						.createNewProductLocaleDTO(
								en,
								"USD",
								"Socialevent",
								"You can participate by the sovial Event buying this ticket",
								templatesDTO, 39.49, 0.19) };

		productDTO_intern1 = ProductDTO.createNewProductDTO(
				"4-Tagesticket interner Shop", ProductType.DIGITAL, systemDTO,
				productLocales_P1, 1000, 0);
		productBeanRemote.create(sysAdmin, productDTO_intern1);

		// get locales out of the db
		ProductLocaleDTO[] productlocaleoutofdb = new ProductLocaleDTO[productBeanRemote
				.getAllProductLocales(sysAdmin).size()];
		productBeanRemote.getAllProductLocales(sysAdmin).toArray(
				productlocaleoutofdb);

		productDTO_extern1 = ProductDTO.createNewProductDTO(
				"4-Tagesticket externer Shop", ProductType.DIGITAL, systemDTO,
				productlocaleoutofdb, 1000, 0);
		productBeanRemote.create(sysAdmin, productDTO_extern1);
		eticket = ProductDTO.createNewProductDTO("4-Tagesticket",
				ProductType.DIGITAL, systemDTO, productlocaleoutofdb, 1000, 0);
		productBeanRemote.create(sysAdmin, eticket);

		productDTO_intern2 = ProductDTO.createNewProductDTO(
				"Socialevent Ticket interner shop", ProductType.DIGITAL,
				systemDTO, productLocales_P2, 200, 0);
		productBeanRemote.create(sysAdmin, productDTO_intern2);
		productlocaleoutofdb = new ProductLocaleDTO[2];
		productlocaleoutofdb[0] = productBeanRemote.getAllProductLocales(
				sysAdmin).get(2);
		productlocaleoutofdb[1] = productBeanRemote.getAllProductLocales(
				sysAdmin).get(3);

		productDTO_extern2 = ProductDTO.createNewProductDTO(
				"Socialevent Ticket externer shop", ProductType.DIGITAL,
				systemDTO, productlocaleoutofdb, 200, 0);
		productBeanRemote.create(sysAdmin, productDTO_extern2);
		ProductNumberDTO productNumberDTO = ProductNumberDTO
				.createNewProductNumberDTO(
						1000,
						2000,
						5,
						productBeanRemote.get(systemDTO.getName(),
								eticket.getUin()));
		productNumberBeanRemote.cerateProductNumber(sysAdmin, productNumberDTO);
		productNumberDTO = ProductNumberDTO.createNewProductNumberDTO(
				1000,
				2000,
				5,
				productBeanRemote.get(systemDTO.getName(),
						productDTO_intern1.getUin()));
		productNumberBeanRemote.cerateProductNumber(sysAdmin, productNumberDTO);
		productNumberDTO = ProductNumberDTO.createNewProductNumberDTO(
				1000,
				2000,
				5,
				productBeanRemote.get(systemDTO.getName(),
						productDTO_intern2.getUin()));
		productNumberBeanRemote.cerateProductNumber(sysAdmin, productNumberDTO);
		productNumberDTO = ProductNumberDTO.createNewProductNumberDTO(
				1000,
				2000,
				5,
				productBeanRemote.get(systemDTO.getName(),
						productDTO_extern1.getUin()));
		productNumberBeanRemote.cerateProductNumber(sysAdmin, productNumberDTO);
		productNumberDTO = ProductNumberDTO.createNewProductNumberDTO(
				1000,
				2000,
				5,
				productBeanRemote.get(systemDTO.getName(),
						productDTO_extern2.getUin()));
		productNumberBeanRemote.cerateProductNumber(sysAdmin, productNumberDTO);
	}

	private void setTransferProductByMethodcall() {
		productBeanRemote.transferProductByMethodcall(sysAdmin, eticket,
				TransferMethod.ACTIVATEACC, 1);
	}

	private void setTransferSystemProductToAccount() {
		productTransferService.transferSystemProductToAccount(sysAdmin,
				productDTO_intern1, accountDTO, 4);
		productTransferService.transferSystemProductToAccount(sysAdmin,
				productDTO_intern1, accountDTO2, 4);
	}

	private void createShopProduct() {
		ShopProductDTO shopProductDTO = ShopProductDTO.createNewShopProductDTO(
				productDTO_intern1, true, ProductAssociation.SHOP_INTERN);
		shopProductBeanRemote.createShopProduct(shopProductDTO);

		shopProductDTO = ShopProductDTO.createNewShopProductDTO(
				productDTO_intern2, true, ProductAssociation.SHOP_INTERN);
		shopProductBeanRemote.createShopProduct(shopProductDTO);

		shopProductDTO = ShopProductDTO.createNewShopProductDTO(
				productDTO_extern1, true, ProductAssociation.SHOP_EXTERN);
		shopProductBeanRemote.createShopProduct(shopProductDTO);

		shopProductDTO = ShopProductDTO.createNewShopProductDTO(
				productDTO_extern2, true, ProductAssociation.SHOP_EXTERN);
		shopProductBeanRemote.createShopProduct(shopProductDTO);
	}

	private void createGtc() {
		GtcShopDTO gtcShopDTO = GtcShopDTO
				.init(0,
						"Ein Pufferüberlauf kann zum Absturz des betreffenden Programms, zur Verfälschung von Daten oder zur Beschädigung von Datenstrukturen der Laufzeitumgebung des Programms führen. Durch Letzteres kann die Rücksprungadresse eines Unterprogramms mit beliebigen Daten überschrieben werden, wodurch ein Angreifer durch Übermittlung von beliebigem Maschinencode beliebige Befehle mit den Privilegien des für den Pufferüberlauf anfälligen Prozesses ausführen kann. Dieser Code hat in der Regel das Ziel, dem Angreifer einen komfortableren Zugang zum System zu verschaffen, damit dieser das System dann für seine Zwecke verwenden kann. Pufferüberläufe in verbreiteter Server- und Clientsoftware werden auch von Internetwürmern ausgenutzt.",
						ProductAssociation.SHOP_EXTERN, de);
		GtcShopDTO gtcShopDTOen = GtcShopDTO
				.init(0,
						"Buffer overflows can be triggered by inputs that are designed to execute code, or alter the way the program operates. This may result in erratic program behavior, including memory access errors, incorrect results, a crash, or a breach of system security. Thus, they are the basis of many software vulnerabilities and can be maliciously exploited.",
						ProductAssociation.SHOP_EXTERN, en);
		gtcShopBeanRemote.createGtcShop(gtcShopDTO);
		gtcShopBeanRemote.createGtcShop(gtcShopDTOen);

		gtcShopDTO = GtcShopDTO
				.init(0,
						"Ein Pufferüberlauf kann zum Absturz des betreffenden Programms, zur Verfälschung von Daten oder zur Beschädigung von Datenstrukturen der Laufzeitumgebung des Programms führen. Durch Letzteres kann die Rücksprungadresse eines Unterprogramms mit beliebigen Daten überschrieben werden, wodurch ein Angreifer durch Übermittlung von beliebigem Maschinencode beliebige Befehle mit den Privilegien des für den Pufferüberlauf anfälligen Prozesses ausführen kann. Dieser Code hat in der Regel das Ziel, dem Angreifer einen komfortableren Zugang zum System zu verschaffen, damit dieser das System dann für seine Zwecke verwenden kann. Pufferüberläufe in verbreiteter Server- und Clientsoftware werden auch von Internetwürmern ausgenutzt.",
						ProductAssociation.SHOP_INTERN, de);
		gtcShopDTOen = GtcShopDTO
				.init(0,
						"Buffer overflows can be triggered by inputs that are designed to execute code, or alter the way the program operates. This may result in erratic program behavior, including memory access errors, incorrect results, a crash, or a breach of system security. Thus, they are the basis of many software vulnerabilities and can be maliciously exploited.",
						ProductAssociation.SHOP_INTERN, en);

		gtcShopBeanRemote.createGtcShop(gtcShopDTO);
		gtcShopBeanRemote.createGtcShop(gtcShopDTOen);
	}

	private void createInvoiceTemplate() throws IOException {
		InputStream is = Init.class.getResourceAsStream("/invoiceDe.pdf");

		byte[] pdfFile = toByteArray(is);
		InvoiceTemplateDTO invoiceTemplateDTO = InvoiceTemplateDTO.init(
				pdfFile, "InvoiceTemplate", de);
		invoiceBeanRemote.cerateInvoiceTemplate(invoiceTemplateDTO);
		invoiceTemplateDTO = InvoiceTemplateDTO.init(pdfFile,
				"InvoiceTemplate", en);
		invoiceBeanRemote.cerateInvoiceTemplate(invoiceTemplateDTO);
	}

	private void createInvoiceNumber() {
		InvoiceNumberDTO invoiceNumberDTO = InvoiceNumberDTO.init(1236, 0);
		invoiceBeanRemote.createOrUpdateInvoiceNumber(invoiceNumberDTO);
	}

	private byte[] toByteArray(InputStream inputStream) throws IOException {
		int currentLine;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		while ((currentLine = inputStream.read()) != -1) {
			baos.write(currentLine);
		}
		baos.flush();
		byte[] pdfFile = baos.toByteArray();

		if (baos != null) {
			baos.close();
		}

		return pdfFile;
	}

	@Override
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	@Override
	public ActionBeanContext getContext() {
		return context;
	}

	@SuppressWarnings("unused")
	private List<AccountDTO> createUsers() {
		List<AccountDTO> userAccDTOs = new ArrayList<AccountDTO>();
		List<PersonalInformationDTO> persInfoDTOs = new ArrayList<PersonalInformationDTO>();

		userAccDTOs.add(AccountDTO.init(systemDTO, "c", "d",
				RoleDTO.getUser(0), null));
		persInfoDTOs.add(PersonalInformationDTO
				.createNewPersonalInformationDTO("m", "", "Hans", "Mustermann",
						"Dietmar-Hopp-Allee 2", "69190", "Walldorf", "germany",
						en, "SAP", "mustermann@sap.de"));

		userAccDTOs.add(AccountDTO.init(systemDTO, "e", "f",
				RoleDTO.getUser(0), null));
		persInfoDTOs.add(PersonalInformationDTO
				.createNewPersonalInformationDTO("m", "", "Herribert",
						"Schmalhirn", "Weiß nicht", "00000", "Wo?", "germany",
						de, "", "brauchich@wirklich.nicht"));

		for (int i = 0; i < userAccDTOs.size(); i++) {
			accountBeanRemote.create(sysAdmin, userAccDTOs.get(i));
			accountBeanRemote.UpdatePersonalInformation(userAccDTOs.get(i),
					persInfoDTOs.get(i));
		}

		return userAccDTOs;
	}
}
