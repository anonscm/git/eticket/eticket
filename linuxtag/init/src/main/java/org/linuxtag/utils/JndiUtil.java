package org.linuxtag.utils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class JndiUtil {
	private JndiUtil() {

	}

	private static String projectName = "eTicketBundle";

	public static void setProjectName(String projectName) {
		JndiUtil.projectName = projectName;
	}

	public static <T> T lookup(String className) {
		@SuppressWarnings("unchecked")
		T bean = (T) lookupObject("java:global/"+projectName+"/"+className);
		return bean;
	}

	private static Object lookupObject(String jndiName) {
		Context context = null;
		try {
			context = new InitialContext();
			return context.lookup(jndiName);
		} catch (NamingException ex) {
			throw new IllegalStateException("Cannot connect to bean: "
					+ jndiName + " Reason: " + ex, ex.getCause());
		} finally {
			try {
				context.close();
			} catch (NamingException ex) {
				throw new IllegalStateException(
						"Cannot close InitialContext. Reason: " + ex,
						ex.getCause());
			}
		}
	}
}
