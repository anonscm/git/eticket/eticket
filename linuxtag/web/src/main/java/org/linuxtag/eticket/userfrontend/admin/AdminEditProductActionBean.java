package org.linuxtag.eticket.userfrontend.admin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ScopedLocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.TransferMethod;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticketshop.business.product.ShopProductBeanService;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;

import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * 
 * @author dgille 
 * 
 *         Shows the admineditProduct View for a given ProductDTO(ID!) and edits it after confirming the form 
 * 
 *  	   The Validation is just a little bit more then ugly.Nearly as
 *         painfull as my English :D Problem: There are many rules, which makes
 *         creating products difficult ->Products can have as much
 *         productlocales as possible, but they my not have more then on
 *         productlocale for each "real" locale --> Validition in View is now
 *         just impossible, so I use a self written validation. --> For further
 *         development an actual validation using javascript would be nice to
 *         have. Also Using Checkboxes instead of selects is circuitous, because
 *         you will get only an Array of Strings...
 */

@UrlBinding("/dispatcher/admineditproduct")
public class AdminEditProductActionBean extends GenericLocale implements
		ActionBean, ValidationErrorHandler {
	private ActionBeanContext context;
	private List<ProductLocaleDTO> productlocales;
	private ProductLocaleDTO[] selectedlocales;
	@Validate(on = "editProduct", required = true)
	private String pname;
	@Validate(on = "editProduct", required = true)
	private String amount;
	@Validate(on = "editProduct", required = true)
	private String inputdate;
	private TransferMethod[] transfers;
	private String transfermethod;
	private String active;

	
	
	/**
	 * Shows the Edit form. It's called out of the Productstatistc's view and needs an ProductID to find the productDTO to work with.
	 * @return
	 *         Edit Product form (=admineditproduct.vm)
	 * @throws IOException
	 */
	@DefaultHandler
	public Resolution showInterface() throws IOException {
		HttpSession session = getContext().getRequest().getSession();
		session.removeAttribute("editprodcutDate");
		session.removeAttribute("shopcontrol");
		try {
			if (getContext().getRequest().getParameter("shop").toString()
					.equals("$row.getShop()")) {
				session.setAttribute("shopcontrol", "1");
			}else{
				session.setAttribute("shopcontrol", getContext().getRequest().getParameter("shop").toString());
			}
			long productid = Long.parseLong(getContext().getRequest()
					.getParameter("productId").toString());
			AccountDTO myAccount = (AccountDTO) session
					.getAttribute("accountDO");
			ProductBeanService productBeanRemote = JndiUtil
					.lookup("ProductBean");
			ProductDTO product = productBeanRemote.get(myAccount, productid);
			if (product.getDeadline() != null) {
				DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				String date = formatter.format(product.getDeadline());
				session.setAttribute("editprodcutDate", date);
			}
			session.setAttribute("editprodcut", product);
		} catch (Exception e) {
			return new RedirectResolution(
					generateLocaleLink("/dispatcher/adminindex"));

		}
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		AccountDTO myAccount = (AccountDTO) session.getAttribute("accountDO");
		ProductBeanService productBeanRemote = JndiUtil.lookup("ProductBean");
		productlocales = productBeanRemote.getAllProductLocales(myAccount);
		session.setAttribute("productlocales", productlocales);
		transfers = TransferMethod.values();
		session.setAttribute("transfers", transfers);
		return new ForwardResolution("/velocity/AdminUI/admineditproduct.vm");
	}
	
	
	/**Handles the formaction of admineditproduct.vm
	 *   updates the product
	 * 
	 * @return
	 * 		Product Statistics View(=adminaction)
	 * @throws IOException
	 */

	public Resolution editProduct() throws IOException {
		ProductDTO product = createProductDTO();
		ProductBeanService productBeanRemote = JndiUtil.lookup("ProductBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO myAccount = (AccountDTO) session.getAttribute("accountDO");
		if (product == null || product.getProductLocales() == null) {
			// Es wird keine spezifische Fehlermeldung ausgegeben, da es zu
			// viele mögliche Fehlerkombinationen gibt
			getContext().getRequest().setAttribute("message", "error.baddata");
			return new ForwardResolution("/velocity/AdminUI/admineditproduct.vm");
		}
		try {
			productBeanRemote.updatebyID(myAccount, product);

			if (session.getAttribute("shopcontrol").equals("1")) {
				product = productBeanRemote.get(
						myAccount.getSystem().getName(), product.getUin());
				if (!getTransfermethod().equals("nothing")) {
					productBeanRemote.transferProductByMethodcall(myAccount,
							product,
							TransferMethod.fromString(getTransfermethod()), 1);
				} else {
					productBeanRemote.transferProductByMethodcall(myAccount,
							product, null, 1);

				}
			}else{
				ShopProductBeanService shopProductBeanRemote = JndiUtil.lookup("ShopProductBean");
				ShopProductDTO sproduct = createShopProductDTO(product);
				shopProductBeanRemote.updateShopProduct(sproduct);
					
			}

		} catch (Exception e) {
			getContext().getRequest().setAttribute("message", "error.baddata");
			return new ForwardResolution("/velocity/AdminUI/admineditproduct.vm");
		}
		session.removeAttribute("editprodcutDate");
		session.removeAttribute("productlocales");
		session.removeAttribute("transfers");
		session.removeAttribute("shopcontrol");
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/adminindex"));
	}
	
	
	private ShopProductDTO createShopProductDTO(ProductDTO product) {
		boolean activated = false;
		if (getActive().equals("true")) {
			activated = true;
		}
		String shop= (String) getContext().getRequest().getSession().getAttribute("shopcontrol");
		ProductAssociation pa = ProductAssociation.fromString(shop);
		ShopProductDTO sproduct = ShopProductDTO.createNewShopProductDTO(
				product, activated, pa);
		return sproduct;

	}
	
	/**Method for setting up the productDTO.
	 * It gets all Data by itself
	 * 
	 * @return
	 *        The new ProdcutDTO
	 */

	private ProductDTO createProductDTO() {
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {
			date = formatter.parse(getInputdate());
		} catch (ParseException e) {
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/admincreateproduct.inputdate",
					"valueNotPresent");
			ValidationErrors errors = new ValidationErrors();
			errors.add("inputdate", error);
			getContext().setValidationErrors(errors);
			return null;
		}
		int amount;
		try {
			amount = Integer.parseInt(getAmount());
		} catch (Exception e) {
			return null;
		}
		ProductDTO product = (ProductDTO) getContext().getRequest()
				.getSession().getAttribute("editprodcut");
		ProductDTO returnproduct;
		try {
			returnproduct = ProductDTO
					.createProductDTOFromExsistingProductEntity(
							getUtf8(getPname()), product.getProductType(),
							product.getSystem(), product.getProductLocales(),
							product.getCurrAmount(), amount, date,
							product.getExpiryTTL(), product.getId());
		} catch (UnsupportedEncodingException e) {
			returnproduct = null;
			e.printStackTrace();
		}

		return returnproduct;

	}

	private String getUtf8(String string) throws UnsupportedEncodingException {
		if (string != null
				&& (getContext().getRequest().getCharacterEncoding() == null || getContext()
						.getRequest().getCharacterEncoding().equals("UTF-8")))
			return new String(string.getBytes("8859_1"), "UTF8");
		else
			return string;
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {

		return new ForwardResolution("/velocity/AdminUI/admineditproduct.vm");
	}

	public void setPname(String name) {
		pname = name;
	}

	public String getPname() {
		return pname;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAmount() {
		return amount;
	}

	public ProductLocaleDTO[] getSelectedlocales() {
		return selectedlocales;
	}

	public void setSelectedlocales(ProductLocaleDTO[] selectedlocales) {
		this.selectedlocales = selectedlocales;
	}

	public String getInputdate() {
		return inputdate;
	}

	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}

	public String getTransfermethod() {
		return this.transfermethod;
	}

	public void setTransfermethod(String transfermethod) {
		this.transfermethod = transfermethod;
	}

	public List<ProductLocaleDTO> getProductlocales() {
		return productlocales;
	}

	public void setProductlocales(List<ProductLocaleDTO> productlocales) {
		this.productlocales = productlocales;
	}

	public TransferMethod[] getTransfers() {
		return transfers;
	}

	public void setTransfers(TransferMethod[] transfers) {
		this.transfers = transfers;
	}


	public String getActive() {
		return active;
	}


	public void setActive(String active) {
		this.active = active;
	}

	/**Before Test, if the user is an admin 
	 * 
	 * @throws IllegalAccessException
	 * 					If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException{
		setLocale((Locale) getContext().getRequest().getSession().getAttribute(LOCALE));
		AccountDTO myAccount = (AccountDTO) getContext().getRequest().getSession().getAttribute("accountDO");
		if (!myAccount.getRole().equals(RoleDTO.getSysAdmin(myAccount.getSystem().getId()))){
			throw new IllegalAccessException();
		}
		
	}
}