package org.linuxtag.eticket.userfrontend.admin;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.ProductPoolBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.LocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;

@UrlBinding("/dispatcher/adminlistproducts")
public class AdminListProductsBean  extends GenericLocale implements ActionBean {

	private ActionBeanContext context;

	private AccountBeanService myAccountBeanService;
	private AccountDTO myAccountDTO;
	private HttpSession session;
	boolean hasPrivileges;
	
	
	private String systemID;
	private String pseudoUID;

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public AccountBeanService getMyAccountBeanService() {
		return this.myAccountBeanService;
	}

	public void setMyAccountBeanService() {
		JndiUtil.lookup("AccountBean");
	}

	public AccountDTO getMyAccountDTO() {
		return this.myAccountDTO;
	}

	public void setMyAccountDTO() {
		this.myAccountDTO = (AccountDTO) this.session.getAttribute("accountDO");
	}

	public String getSystemID() {
		return systemID;
	}

	private void setSystemID(String s) {
		this.systemID = s;
	}

	public String getPseudoUID() {
		return pseudoUID;
	}

	public void setPseudoUID(String s) {
		this.pseudoUID = s;
	}

	public void AdminAccountBean() {
		myAccountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();

		myAccountDTO = (AccountDTO) session.getAttribute("accountDO");
		

		
	}
	
	@DefaultHandler
	public Resolution showInterface() throws IOException {
		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO myAccount =(AccountDTO) session.getAttribute("accountDO");
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		List <ProductDTO> products =productBeanService.listBySystemClass(myAccount, system);
		
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		return new ForwardResolution("/velocity/adminlistproducts.vm");
	
	
	
	}

	
}	