package org.linuxtag.eticket.userfrontend.admin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticketpayment.business.paypalorder.PayPalOrderBeanService;
import org.evolvis.eticketpayment.model.entity.OrderStatus;
import org.evolvis.eticketpayment.model.entity.dto.PayPalOrderDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**Create a ShopPorduct like the normal CreateProductActionBean
 * but does'not support ProductAutpomatTransfers
 * @author dgille
 *
 */


@UrlBinding("/dispatcher/adminlistorders")
public class AdminListOrdersActionBean extends GenericLocale implements
		ActionBean{
	private ActionBeanContext context;
	private List <PayPalOrderDTO> openOrders;
	private List <PayPalOrderDTO> pendingOrders;
	private List <PayPalOrderDTO> failedOrders;
	private List <PayPalOrderDTO> completedOrders;	
	private List <PayPalOrderDTO> completedDeliveredOrders;
	private List <PayPalOrderDTO> canceldOrders;
	private OrderStatus[] orderoptions;
	private String state;
	

	
	/**Generates a form for creating Products
	 * 
	 * @return
	 *        Admincreateproduct.vm
	 * @throws IOException
	 */
	
	
	@DefaultHandler
	public Resolution showInterface() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO myAccount = (AccountDTO) session.getAttribute("accountDO");
		PayPalOrderBeanService payPalservice =JndiUtil.lookup("PayPalOrderBean");
		openOrders=payPalservice.getOrders(myAccount, OrderStatus.OPEN);
		pendingOrders=payPalservice.getOrders(myAccount, OrderStatus.PENDING);
		failedOrders=payPalservice.getOrders(myAccount, OrderStatus.FAILURE);
		canceldOrders=payPalservice.getOrders(myAccount, OrderStatus.CANCELED);
		orderoptions=OrderStatus.values();
		//orderoptions[1].toString()
		//openOrders.get(0)
		return new ForwardResolution("/velocity/AdminUI/adminlistorders.vm");
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}
	
	
	public Resolution showCompleted(){
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO myAccount = (AccountDTO) session.getAttribute("accountDO");
		PayPalOrderBeanService payPalservice =JndiUtil.lookup("PayPalOrderBean");
		completedOrders=payPalservice.getOrders(myAccount, OrderStatus.COMPLETED);
		completedDeliveredOrders=payPalservice.getOrders(myAccount, OrderStatus.COMPLETED_AND_DELIVERED);
		orderoptions=OrderStatus.values();
		return new ForwardResolution("/velocity/AdminUI/adminlistcompletedorders.vm");
	}

	public Resolution changeState() throws IOException{
		String id;
		String status;
		int spare=0;
		for (int i=0;i<state.length();i++){
			if (state.charAt(i)=='/'){
				spare=i;
			}
		}
		id =state.substring(0, spare);
		status=state.substring(spare+1);
		PayPalOrderBeanService payPalservice =JndiUtil.lookup("PayPalOrderBean");
		payPalservice.updateOrderstatus(Integer.parseInt(id), OrderStatus.fromString(status));
		if (status.equals(OrderStatus.CANCELED.toString())||status.equals(OrderStatus.FAILURE.toString())||status.equals(OrderStatus.PENDING.toString())||status.equals(OrderStatus.OPEN.toString())){
			return new RedirectResolution(
					generateLocaleLink("/dispatcher/adminlistorders"));
		}else {
			return new RedirectResolution(
					generateLocaleLink("/dispatcher/adminlistorders?showCompleted=showCompleted"));

		}
	}
	

	
	/**Before Test, if the user is an admin 
	 * 
	 * @throws IllegalAccessException
	 * 					If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException{
		setLocale((Locale) getContext().getRequest().getSession().getAttribute(LOCALE));
		AccountDTO myAccount = (AccountDTO) getContext().getRequest().getSession().getAttribute("accountDO");
		if (!myAccount.getRole().equals(RoleDTO.getSysAdmin(myAccount.getSystem().getId()))){
			throw new IllegalAccessException();
		}
		
	}

	public List <PayPalOrderDTO> getOpenOrders() {
		return openOrders;
	}

	public void setOpenOrders(List <PayPalOrderDTO> openOrders) {
		this.openOrders = openOrders;
	}

	public List <PayPalOrderDTO> getPendingOrders() {
		return pendingOrders;
	}

	public void setPendingOrders(List <PayPalOrderDTO> pendingOrders) {
		this.pendingOrders = pendingOrders;
	}

	public List <PayPalOrderDTO> getFailedOrders() {
		return failedOrders;
	}

	public void setFailedOrders(List <PayPalOrderDTO> failedOrders) {
		this.failedOrders = failedOrders;
	}

	public OrderStatus[] getOrderoptions() {
		return orderoptions;
	}

	public void setOrderoptions(OrderStatus[] orderoptions) {
		this.orderoptions = orderoptions;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List <PayPalOrderDTO> getCompletedOrders() {
		return completedOrders;
	}

	public void setCompletedOrders(List <PayPalOrderDTO> completedOrders) {
		this.completedOrders = completedOrders;
	}

	public List <PayPalOrderDTO> getCompletedDeliveredOrders() {
		return completedDeliveredOrders;
	}

	public void setCompletedDeliveredOrders(List <PayPalOrderDTO> completedDeliveredOrders) {
		this.completedDeliveredOrders = completedDeliveredOrders;
	}

	public List <PayPalOrderDTO> getCanceldOrders() {
		return canceldOrders;
	}

	public void setCanceldOrders(List <PayPalOrderDTO> canceldOrders) {
		this.canceldOrders = canceldOrders;
	}

}
