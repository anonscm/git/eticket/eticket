package org.linuxtag.eticket.userfrontend.shop;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJBTransactionRolledbackException;
import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.SessionScope;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.LocalizableError;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.ProductTransferService;
import org.evolvis.eticket.business.template.ShopTextTemplateBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticketpayment.business.invoice.InvoiceBeanService;
import org.evolvis.eticketpayment.business.payment.GenericOrderBeanService;
import org.evolvis.eticketpayment.business.paypalorder.PayPalException;
import org.evolvis.eticketpayment.business.paypalorder.PayPalOrderBeanService;
import org.evolvis.eticketpayment.model.entity.InvoiceField;
import org.evolvis.eticketpayment.model.entity.OrderStatus;
import org.evolvis.eticketpayment.model.entity.PaymentMethod;
import org.evolvis.eticketpayment.model.entity.dto.OrderInformationDTO;
import org.evolvis.eticketpayment.model.entity.dto.PayPalOrderDTO;
import org.evolvis.eticketshop.business.gtc.GtcShopBeanService;
import org.evolvis.eticketshop.business.mail.EMailBeanService;
import org.evolvis.eticketshop.business.product.ShopProductBeanService;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.TextTemplateNames;
import org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO;
import org.evolvis.eticketshop.model.entity.dto.GtcShopDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * ShopActionBean handles all incoming requests which are addressed to the shop.
 * 
 * @author ben
 */

@SessionScope
@UrlBinding(value = "/dispatcher/shop")
public class ShopActionBean extends GenericLocale {
	private ActionBeanContext context;
	private String product_uin;
	private String temp_quantity;
	private int real_quantity;
	private String payment;
	private PaymentMethod pMethod;
	private String current_displayed_page;
	private String last_displayed_page;
	private String gtc;
	private boolean gtcAccepted;
	// String is Product.uin, ProductLocaleDTO is the corresponding
	// Productlocale
	private HashMap<String, ProductLocaleDTO> productLocaleDTOs;

	/*
	 * Static Variables to remember view stage 
	 */
	private static String PAGE_SHOP_MAIN = "shop_main";
	private static String PAGE_SHOP_PRODUCTDETAIL = "shop_productdetail";
	private static String PAGE_SHOP_GTC = "shop_gtc";
	private static String PAGE_SHOP_PAYMENT_QUANTITY = "shop_payment_quantity";
	private static String PAGE_SHOP_SUMMARY = "shop_summary";

	@Override
	public ActionBeanContext getContext() {
		return context;
	}

	@Override
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	private HttpSession getHttpSession() {
		return context.getRequest().getSession();
	}

	/**
	 * Shows the shop mainpage.
	 * 
	 * @return Mainpage of the shop.
	 */
	@DefaultHandler
	public ForwardResolution showShop() {
		loadProductLocales();
		last_displayed_page = current_displayed_page;
		current_displayed_page = "shop_main";
		return new ForwardResolution("/velocity/shop/shop_main.vm");
	}

	/**
	 * Shows a page of an specific product.
	 * 
	 * @return Information-Page of an specific product.
	 */
	@HandlesEvent("productinformation")
	public ForwardResolution showProductinformation() {
		last_displayed_page = current_displayed_page;
		current_displayed_page = PAGE_SHOP_PRODUCTDETAIL;
		setProductUinFromRequest();
		if (!checkProductUin()) {
			// submit a invalid product_uin
			ValidationErrors errors = new ValidationErrors();
			errors.add("shop_product_invalid", new LocalizableError(
					"/dispatcher/shop.product.notselected"));
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/shop/shop_main.vm");
		}

		current_displayed_page = "shop_productdetail";
		return new ForwardResolution("/velocity/shop/shop_productdetail.vm");
	}

	/**
	 * Shows a page where the user must enter quantity and payment-method.
	 * 
	 * @return Page where the user must enter quantity and payment-method.
	 */
	@HandlesEvent("selectPaymentMethodAndQuantity")
	public ForwardResolution showSelectPaymentMethodAndQuantity() {
		if (!gtcAccepted) {
			last_displayed_page = current_displayed_page;
			current_displayed_page = PAGE_SHOP_PAYMENT_QUANTITY;
			return showGtc();
		}

		setProductUinFromRequest();
		if (!checkProductUin()) {
			// submit a invalid product_uin
			ValidationErrors errors = new ValidationErrors();
			errors.add("shop_product_invalid", new LocalizableError(
					"/dispatcher/shop.product.notselected"));
			getContext().setValidationErrors(errors);
			last_displayed_page = current_displayed_page;
			current_displayed_page = PAGE_SHOP_MAIN;
			return new ForwardResolution("/velocity/shop/shop_main.vm");
		}
		last_displayed_page = current_displayed_page;
		current_displayed_page = PAGE_SHOP_PAYMENT_QUANTITY;
		return new ForwardResolution("/velocity/shop/shop_payment_quantity.vm");
	}

	/**
	 * Shows a page with the statements made by the user (product, quantity and
	 * payment-method).
	 * 
	 * @return Page which shows the selected product, quantity and
	 *         payment-method.
	 */
	@HandlesEvent("summary")
	public ForwardResolution showSummary() {
		// Payment-Method (Valid) and quantity (Integer>0) must be set, if it is
		// not set, user must set it in form

		if (!gtcAccepted) {
			return showGtc();
		}

		if (!checkProductUin()) {
			// submit a invalid product_uin
			ValidationErrors errors = new ValidationErrors();
			errors.add("shop_product_invalid", new LocalizableError(
					"/dispatcher/shop.product.notselected"));
			getContext().setValidationErrors(errors);
			last_displayed_page = current_displayed_page;
			current_displayed_page = PAGE_SHOP_MAIN;
			return new ForwardResolution("/velocity/shop/shop_main.vm");
		}

		if (!checkQuantity()) {
			// quantity is not valid
			ValidationErrors errors = new ValidationErrors();
			errors.add("shop_quantity_invalid", new LocalizableError(
					"/dispatcher/shop.quantity.value"));
			getContext().setValidationErrors(errors);
			last_displayed_page = current_displayed_page;
			current_displayed_page = PAGE_SHOP_PAYMENT_QUANTITY;
			return new ForwardResolution(
					"/velocity/shop/shop_payment_quantity.vm");
		}

		if (!checkPaymentMethod()) {
			// quantity is not valid
			ValidationErrors errors = new ValidationErrors();
			errors.add("shop_payment_invalid", new LocalizableError(
					"/dispatcher/shop.payment.notselected"));
			getContext().setValidationErrors(errors);
			last_displayed_page = current_displayed_page;
			current_displayed_page = PAGE_SHOP_PAYMENT_QUANTITY;
			return new ForwardResolution(
					"/velocity/shop/shop_payment_quantity.vm");
		}

		last_displayed_page = current_displayed_page;
		current_displayed_page = PAGE_SHOP_SUMMARY;
		return new ForwardResolution("/velocity/shop/shop_summary.vm");
	}

	/**
	 * Show the gtc page.
	 * 
	 * @return show gtc
	 */
	@HandlesEvent("gtc")
	public ForwardResolution showGtc() {
		checkPaymentMethod();
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(
				locale.getLanguage(), locale.getCountry());
		GtcShopBeanService gtcShopBeanService = JndiUtil.lookup("GtcShopBean");
		GtcShopDTO gtcShopDTO = gtcShopBeanService.findByProductAssociation(
				ProductAssociation.SHOP_INTERN, localeDTO);
		gtc = gtcShopDTO.getValue();

		last_displayed_page = current_displayed_page;
		current_displayed_page = PAGE_SHOP_GTC;
		return new ForwardResolution(
				generateLocaleLink("/velocity/shop/shop_gtc.vm"));

	}

	/**
	 * If the user accept the gtc, this handler is called.
	 * 
	 * @return
	 */
	@HandlesEvent("acceptGtc")
	public Resolution acceptGtc() {
		gtcAccepted = true;
		if (last_displayed_page.equals(PAGE_SHOP_PRODUCTDETAIL)) {
			return showProductinformation();
		} else if (last_displayed_page.equals(PAGE_SHOP_GTC)) {
			return showGtc();
		} else if (last_displayed_page.equals(PAGE_SHOP_PAYMENT_QUANTITY)) {
			return showSelectPaymentMethodAndQuantity();
		} else if (last_displayed_page.equals(PAGE_SHOP_SUMMARY)) {
			return showSummary();
		} else {
			return showShop();
		}
	}

	/**
	 * Method redirects the user to the selected payment-provider.
	 * 
	 * @return redirect to payment-provider.
	 */
	@HandlesEvent("redirectToPaymentProvider")
	public RedirectResolution showRedirectToPaymentProvider() {
		if (!gtcAccepted) {
			return new RedirectResolution(context.getRequest().getRequestURI(),
					false);
		}

		long id;
		GenericOrderBeanService genericOrderBeanService = JndiUtil
				.lookup(pMethod.name());

		ProductLocaleDTO productLocaleDTO = productLocaleDTOs.get(product_uin);

		String param[] = null;
		try {
			
			param = genericOrderBeanService.startPaymentProcess(
					(AccountDTO) getHttpSession().getAttribute("accountDO"),
					productLocaleDTO, product_uin,
					(ESystemDTO) getHttpSession().getAttribute("systemName"),
					getReal_quantity());
			id = Long.valueOf(param[0]);
			getHttpSession().setAttribute("order_id", id);
		} catch (PayPalException e) {
			// FIXME Fatal Error - Could not establish connection to paypal.
			// What is to do?
			Logger.getLogger("LOGGER").log(
					Level.SEVERE,
					"ShopActionBean.showRedirectToPaymentProvider - PayPalException: "
							+ e.getMessage());
		} catch (Exception e) {
			// FIXME Fatal Error - Could not establish connection to paypal.
			// What is to do?
			Logger.getLogger("LOGGER").log(
					Level.SEVERE,
					"ShopActionBean.showRedirectToPaymentProvider - Exception: "
							+ e.getMessage());
		}

		return new RedirectResolution(param[1], false);
	}

	/**
	 * if Paypal has been selected by user, the method is called to complete the
	 * checkout
	 * 
	 * @return page which shows if the transaction could be completed, or not.
	 */
	@HandlesEvent("completePayPalCheckout")
	public ForwardResolution completePayPalCheckout() {
		String token = getContext().getRequest().getParameter("token");
		String payerId = getContext().getRequest().getParameter("PayerID");
		Long id = (Long) getHttpSession().getAttribute("order_id");
		PayPalOrderDTO payPalOrderDTO = null;

		PayPalOrderBeanService payPalOrderBeanService = JndiUtil
				.lookup("PayPalOrderBean");
		try {
			if ((payPalOrderDTO = payPalOrderBeanService.completeTransaction(
					id, token, payerId)) != null) {

				payPalOrderDTO = payPalTransactionCompletedTransferProduct(
						payPalOrderDTO, (AccountDTO) getHttpSession()
								.getAttribute("accountDO"));

				sendInvoice(payPalOrderDTO);
			}
			setOrderStatusToSession(payPalOrderDTO.getOrderInformationDTO()
					.getOrderStatus());
		} catch (PayPalException e) {
			e.printStackTrace();
			// FIXME Fatal Error - Transaction could not been completed. Admin
			// and user should be notified.
			Logger.getLogger("LOGGER").log(
					Level.SEVERE,
					"ShopActionBean.completePayPalCheckout - PayPalException: "
							+ e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			// FIXME Fatal Error - Transaction could not been completed. Admin
			// and user should be notified.
			Logger.getLogger("LOGGER").log(
					Level.SEVERE,
					"ShopActionBean.completePayPalCheckout - Exception: "
							+ e.getMessage());
		} finally {
			resetAllVariables();
		}

		return new ForwardResolution("/velocity/shop/shop_transaction_info.vm");
	}

	/**
	 * Status must be set in Session to display information after an
	 * transaction.
	 * 
	 * @param String
	 *            - can be
	 */
	private void setOrderStatusToSession(OrderStatus orderStatus) {
		// Logger.getLogger("LOGGER").log(Level.INFO,
		// "Order-Status: " + orderStatus.toString());
		getHttpSession().setAttribute("OrderStatus", null);
		getHttpSession().setAttribute("OrderStatus", orderStatus.toString());
	}

	/**
	 * If paypal transaction has been completed, products could be transfered to
	 * the customer account.
	 * 
	 * @param token
	 *            - paypal token
	 * @param payerId
	 *            - paypal payerid
	 * @param accountDto
	 *            - accountDTO of the current user
	 * @return PayPalOrderDTO
	 */
	private PayPalOrderDTO payPalTransactionCompletedTransferProduct(
			PayPalOrderDTO payPalOrderDTO, AccountDTO accountDto) {

		ProductTransferService productTransferBeanService = JndiUtil
				.lookup("ProductTransferBean");

		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");

		ProductDTO productDTO = productBeanService.get(
				((ESystemDTO) getHttpSession().getAttribute("systemName"))
						.getName(), product_uin);

		productTransferBeanService
				.transferOrderedSystemProductToAccountAndAccept(productDTO,
						accountDto, getReal_quantity());

		PayPalOrderBeanService payPalOrderBeanService = JndiUtil
				.lookup("PayPalOrderBean");

		OrderInformationDTO orderInformationDTO = OrderInformationDTO
				.createOrderDTOFromExsistingOrderEntity(payPalOrderDTO
						.getOrderInformationDTO().getId(),
						OrderStatus.COMPLETED_AND_DELIVERED, payPalOrderDTO
								.getOrderInformationDTO().getSystem(),
						payPalOrderDTO.getOrderInformationDTO()
								.getShopProductDTO(),
						payPalOrderDTO.getOrderInformationDTO()
								.getProductLocaleDTO(), payPalOrderDTO
								.getOrderInformationDTO().getAmount(),
						payPalOrderDTO.getOrderInformationDTO().getDate());

		PayPalOrderDTO newPalOrderDTO = PayPalOrderDTO
				.createPayPalOrderDTOFromExsistingPayPalOrderEntity(
						payPalOrderDTO.getId(), payPalOrderDTO.getToken(),
						payPalOrderDTO.getPayPalEmailAddress(),
						payPalOrderDTO.getPayerId(),
						payPalOrderDTO.getTransactionId(), orderInformationDTO,
						payPalOrderDTO.getTransactionDTO());

		payPalOrderBeanService.update(null, newPalOrderDTO);

		return newPalOrderDTO;
	}

	/**
	 * The method stores all "Product"'s which are set as active in
	 * "ShopProduct"'s in productLocaleDTOs. The map is used to display the
	 * product in the webfrontend.
	 */
	private void loadProductLocales() {
		productLocaleDTOs = new HashMap<String, ProductLocaleDTO>();
		ShopProductBeanService shopProductBeanService = JndiUtil
				.lookup("ShopProductBean");
		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		ESystemDTO system = (ESystemDTO) getHttpSession().getAttribute(
				"systemName");

		List<ShopProductDTO> shopProductDTOList = shopProductBeanService
				.getAllShopProductDTOFromShop(true,
						ProductAssociation.SHOP_INTERN, system);

		for (ShopProductDTO spDTO : shopProductDTOList) {
			ProductLocaleDTO productLocaleDTO = null;
			try {
				productLocaleDTO = productBeanService.getProductLocale(
						locale.toString(), spDTO.getProduct().getUin(),
						system.getId());
			} catch (EJBTransactionRolledbackException e) {
				// No productLocaleDTO found
			}

			if (productLocaleDTO != null) {
				// Add corresponding productLocaleDTO
				productLocaleDTOs.put(spDTO.getProduct().getUin(),
						productLocaleDTO);
			}
		}
	}

	private void resetAllVariables() {
		product_uin = null;
		temp_quantity = null;
		real_quantity = 0;
		payment = null;
		pMethod = null;
		productLocaleDTOs = new HashMap<String, ProductLocaleDTO>();
	}

	/**
	 * Check if a valid payment method selected.
	 */
	private void setProductUinFromRequest() {
		String str;
		if ((str = getContext().getRequest().getParameter("product_uin")) != null) {
			product_uin = str;
		}
	}

	/**
	 * Check if enough items are in stock
	 */
	private boolean checkQuantity() {
		real_quantity = 0;
		try {
			real_quantity = (Integer.valueOf(getTemp_quantity()));
		} catch (Exception e) {
			real_quantity = 0;
		}

		if (getReal_quantity() < 1) {
			return false;
		}

		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		ProductDTO productDTO = productBeanService.get(
				((ESystemDTO) getHttpSession().getAttribute("systemName"))
						.getName(), product_uin);
		if (productDTO.getCurrAmount() + getReal_quantity() <= productDTO
				.getMaxAmount()) {
			return true;
		}

		setTemp_quantity(null);
		real_quantity = 0;
		return false;
	}

	/**
	 * Check if product_uin is valid
	 */
	private boolean checkProductUin() {
		if (product_uin != null & productLocaleDTOs != null) {
			if (productLocaleDTOs.containsKey(product_uin)) {
				return true;
			}
		}
		product_uin = null;
		return false;
	}

	/**
	 * Check if payment method is valid
	 */
	private boolean checkPaymentMethod() {
		String param_payment = getContext().getRequest()
				.getParameter("payment");
		if (param_payment != null) {
			if (param_payment.equals(PaymentMethod.PayPalOrderBean.toString())) {
				payment = param_payment;
				pMethod = PaymentMethod.PayPalOrderBean;
				return true;
			}

			payment = null;
			pMethod = null;
			return false;
		}

		if (pMethod != null) {
			return true;
		}

		return false;
	}

	/**
	 * Send invoice
	 */
	private void sendInvoice(PayPalOrderDTO payPalOrderDTO) {
		InvoiceBeanService invoiceBeanService = JndiUtil.lookup("InvoiceBean");
		EMailBeanService eMailBeanService = JndiUtil.lookup("EMailBean");
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		ShopTextTemplateBeanService shopTextTemplateBeanService = JndiUtil
				.lookup("ShopTextTemplateBean");

		AccountDTO accountDTO = (AccountDTO) getHttpSession().getAttribute(
				"accountDO");
		ESystemDTO eSystemDTO = (ESystemDTO) getHttpSession().getAttribute(
				"systemName");

		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(
				((Locale) getHttpSession().getAttribute("locale"))
						.getLanguage(), ((Locale) getHttpSession()
						.getAttribute("locale")).getCountry());
		byte pdf[] = invoiceBeanService.generateInvoice(payPalOrderDTO
				.getOrderInformationDTO().getShopProductDTO(), localeDTO,
				generatePdfMap(payPalOrderDTO.getOrderInformationDTO()
						.getShopProductDTO()));

		PersonalInformationDTO personalInformationDTO = accountBeanService
				.getPersonalInformationDTO(eSystemDTO, accountDTO.getUsername());
		EMailAccountDTO eMailAccountDTO = EMailAccountDTO.init(
				accountDTO.getUsername(), personalInformationDTO.getFirstname()
						+ " " + personalInformationDTO.getLastname());
		ShopTextTemplateDTO shopTextTemplateDTO = shopTextTemplateBeanService
				.find(eSystemDTO, localeDTO,
						TextTemplateNames.BOUGHT_PRODUCT_INTERN.toString());

		eMailBeanService.sendMail(eSystemDTO, eMailAccountDTO,
				shopTextTemplateDTO, new HashMap<String, String>(), pdf);
	}

	/**
	 * Fills out pdf
	 * 
	 * @return
	 */
	private Map<InvoiceField, Object> generatePdfMap(
			ShopProductDTO shopProductDTO) {

		Map<InvoiceField, Object> ret = new HashMap<InvoiceField, Object>();
		InvoiceBeanService invoiceBeanService = JndiUtil.lookup("InvoiceBean");

		ret.put(InvoiceField.AMOUNT, String.valueOf(getReal_quantity()));
		ret.put(InvoiceField.DATE,
				DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(
						new Date()));
		ret.put(InvoiceField.DELIVERYDATE,
				DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(
						new Date()));
		ret.put(InvoiceField.INVOICENUMBER,
				invoiceBeanService.getNextInvoiceNumber());
		ret.put(InvoiceField.PRODUCTNAME, productLocaleDTOs.get(product_uin)
				.getProductName());
		ret.put(InvoiceField.TAXNUMBER, "?TAXNUMBER?");
		ret.put(InvoiceField.TAXRATE, String.valueOf(productLocaleDTOs.get(
				product_uin).getSalesTax() * 100));
		return ret;
	}

	public String getProduct_uin() {
		return product_uin;
	}

	public void setProduct_uin(String product_uin) {
		this.product_uin = product_uin;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public HashMap<String, ProductLocaleDTO> getProductLocaleDTOs() {
		return productLocaleDTOs;
	}

	public void setProductLocaleDTOs(
			HashMap<String, ProductLocaleDTO> productLocaleDTOs) {
		this.productLocaleDTOs = productLocaleDTOs;
	}

	public int getReal_quantity() {
		return real_quantity;
	}

	public String getTemp_quantity() {
		return temp_quantity;
	}

	public void setTemp_quantity(String temp_quantity) {
		this.temp_quantity = temp_quantity;
	}

	public String getCurrent_displayed_page() {
		return current_displayed_page;
	}

	public String getGtc() {
		return gtc;
	}

	public boolean isGtcAccepted() {
		return gtcAccepted;
	}

	public void setGtcAccepted(boolean gtcAccepted) {
		this.gtcAccepted = gtcAccepted;
	}
}
