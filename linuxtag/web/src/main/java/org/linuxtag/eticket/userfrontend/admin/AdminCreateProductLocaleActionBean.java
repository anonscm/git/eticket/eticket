package org.linuxtag.eticket.userfrontend.admin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;
import org.evolvis.eticket.util.CurrencyCode;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;


/** Creates a new Productlocale, with an existing Template by an choosen supported locale
 * 
 * @author dgille
 *
 */


@UrlBinding("/dispatcher/admincreateproductlocale")
public class AdminCreateProductLocaleActionBean extends GenericLocale implements
		ActionBean, ValidationErrorHandler {
	private ActionBeanContext context;
	@Validate(on = "createProductLocale", required = true, trim = true)
	private String currency;
	private String template;
	@Validate(on = "createProductLocale", required = true, trim = true)
	private String description;
	@Validate(on = "createProductLocale", required = true, trim = true)
	private String salesTax;
	@Validate(on = "createProductLocale", required = true, trim = true)
	private String price;
	@Validate(on = "createProductLocale", required = true, trim = true)
	private String name;

	private String plocale;

	
	/**Generates the Form and gets all necessary Data
	 * 
	 * @return
	 *      admincreateproductlocale.vm
	 * @throws IOException
	 */
	@DefaultHandler
	public Resolution showInterface() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO myAccount = (AccountDTO) session.getAttribute("accountDO");
		SystemBeanService systemBean = JndiUtil.lookup("SystemBean");
		ESystemDTO system = systemBean.getSystemById(myAccount.getSystem()
				.getId());
		ProductBeanService productBean = JndiUtil.lookup("ProductBean");
		List<TemplatesDTO> templates = productBean.getAllTemplates(myAccount);
		session.setAttribute("templates", templates);
		List<LocaleDTO> locales = system.getSupportedLocales();
		session.setAttribute("plocales", locales);
		List<String>currencies=CurrencyCode.getAvailableCurrencieCodes();
		
		session.setAttribute("currencies", currencies);

		return new ForwardResolution("/velocity/AdminUI/admincreateplocale.vm");
	}
	
	/**Responds to the form and creates finally the productlocae 
	 * 
	 * @return
	 *  Product Statisitcs overivew (adminindex/action)
	 */

	public Resolution createProductLocale() {
		ProductLocaleDTO productlocale = null;
		try {
			productlocale = ProductLocaleDTO.createNewProductLocaleDTO(
					getLocaleDTO(), getfinalcurrency(), getUtf8(getName()),
					getUtf8(getDescription()), getTemplatesDTO(),
					Double.parseDouble(getUtf8((getPrice()))),
					Double.parseDouble(getUtf8((getSalesTax()))));
		} catch (NumberFormatException e) {

			e.printStackTrace();
			productlocale=null;
		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
			productlocale=null;
		} catch (Exception e) {
			productlocale=null;
		}
		if (productlocale==null||productlocale.getCurrencyCode()==null){
			getContext().getRequest().setAttribute("message", "error.baddata");
			return new ForwardResolution("/velocity/AdminUI/admincreateplocale.vm");
		}
		ProductBeanService productBean = JndiUtil.lookup("ProductBean");
		AccountDTO myAccount= (AccountDTO) getContext().getRequest()
				.getSession().getAttribute("accountDO");
		productBean.createProductlocale(myAccount, productlocale);

		return new RedirectResolution(generateLocaleLink("/dispatcher/adminlistproductlocales"));
	}

	
	/**Custom Validtion off the currency,because the standart validation Handler is just !?$!°°( doesn' work)
	 * The Currency must be a char, like € or $. But by the first attemp of getting the data, this signs are not correct encoded.
	 * So they are longer then one char and a normal validation by maxLenght= 1 is impossible. The sign must be encoded by getUTF8
	 * 
	 * @return
	 * 		currency
	 * @throws UnsupportedEncodingException 
	 */
	private String getfinalcurrency() throws UnsupportedEncodingException {
		/*String control = null;
		try {
			control = getUtf8(getCurrency());
			if (control.length() > 1) {
				return null;
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			return null;
		}
		// TODO Auto-generated method stub*/
		return getUtf8(getCurrency());
	}

	private TemplatesDTO getTemplatesDTO() {
		ProductBeanService productBean = JndiUtil.lookup("ProductBean");

		return productBean.getTemplate(getTemplate());
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	private LocaleDTO getLocaleDTO() {
		SystemBeanService systemBean = JndiUtil.lookup("SystemBean");

		return systemBean.getLocaleById(Long.parseLong(getPlocale()));
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		// TODO Auto-generated method stub
		return new ForwardResolution("/velocity/AdminUI/admincreateplocale.vm");
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSalesTax() {
		return salesTax;
	}

	public void setSalesTax(String salesTax) {
		this.salesTax = salesTax;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public String getPlocale() {
		return plocale;
	}

	public void setPlocale(String plocale) {
		this.plocale = plocale;
	}

	private String getUtf8(String string) throws UnsupportedEncodingException {
		if (string != null
				&& (getContext().getRequest().getCharacterEncoding() == null || getContext()
						.getRequest().getCharacterEncoding().equals("UTF-8")))
			return new String(string.getBytes("8859_1"), "UTF8");
		else
			return string;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}
	
	/**Before Test, if the user is an admin 
	 * 
	 * @throws IllegalAccessException
	 * 					If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException{
		AccountDTO myAccount = (AccountDTO) getContext().getRequest().getSession().getAttribute("accountDO");
		setLocale((Locale) getContext().getRequest().getSession().getAttribute(LOCALE));
		if (!myAccount.getRole().equals(RoleDTO.getSysAdmin(myAccount.getSystem().getId()))){
			throw new IllegalAccessException();
		}
		
	}

}