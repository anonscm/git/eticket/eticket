
package org.linuxtag.eticket.userfrontend.exeption;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.exception.DefaultExceptionHandler;

import org.evolvis.eticket.model.entity.dto.ESystemDTO;

public class ExceptionHandlerActionBean extends DefaultExceptionHandler {

	public Resolution handleGeneric(Exception exc, HttpServletRequest request,
			HttpServletResponse response) {
		ESystemDTO eSystemDTO = (ESystemDTO) request
				.getSession().getAttribute("systemName");
		Locale locale = (Locale) request.getSession().getAttribute("locale");

		// resp.sendRedirect("/eTicketUserWeb/dispatcher/welcome?locale=" +
		// locale);

		StringWriter sw = new StringWriter();
		exc.printStackTrace(new PrintWriter(sw));
		String stacktrace = sw.toString();
		System.err.println(stacktrace);
		if (eSystemDTO == null || locale == null)
			return new RedirectResolution("/SetSystemLocale/linuxtag/en");
		return new ForwardResolution("/dispatcher/exception?locale=" + locale
				+ "&errormsg=" + exc.getMessage());
	}
}

