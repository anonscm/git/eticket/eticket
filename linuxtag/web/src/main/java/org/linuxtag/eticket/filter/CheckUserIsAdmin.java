package org.linuxtag.eticket.filter;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.ExecutionContext;
import net.sourceforge.stripes.controller.Interceptor;
import net.sourceforge.stripes.controller.Intercepts;
import net.sourceforge.stripes.controller.LifecycleStage;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.admin.AdminActionBean;
import org.linuxtag.eticket.userfrontend.admin.AdminCreateProductActionBean;
import org.linuxtag.eticket.userfrontend.admin.AdminCreateProductLocaleActionBean;
import org.linuxtag.eticket.userfrontend.admin.AdminCreateProductTemplateActionBean;
import org.linuxtag.eticket.userfrontend.admin.AdminCreateShopProductActionBean;
import org.linuxtag.eticket.userfrontend.admin.AdminCreateUserActionBean;
import org.linuxtag.eticket.userfrontend.admin.AdminEditProductActionBean;
import org.linuxtag.eticket.userfrontend.admin.AdminEditProductlocaleActionbean;
import org.linuxtag.eticket.userfrontend.admin.AdminEditUserActionBean;
import org.linuxtag.eticket.userfrontend.admin.AdminListOrdersActionBean;
import org.linuxtag.eticket.userfrontend.admin.AdminListProductlocalesActionBean;
import org.linuxtag.eticket.userfrontend.admin.AdminShowUserActionBean;
import org.linuxtag.eticket.userfrontend.admin.HelloAdminActionBean;

@Intercepts({ LifecycleStage.CustomValidation})
public class CheckUserIsAdmin implements Interceptor {

	@Override
	public Resolution intercept(ExecutionContext context) throws Exception {
		HttpSession session = context.getActionBeanContext().getRequest()
				.getSession();
		if (context.getActionBean().getClass().equals(AdminActionBean.class)
				|| context.getActionBean().getClass()
						.equals(AdminCreateProductActionBean.class)
				|| context.getActionBean().getClass()
						.equals(AdminCreateProductLocaleActionBean.class)
				|| context.getActionBean().getClass()
						.equals(AdminCreateProductTemplateActionBean.class)
				|| context.getActionBean().getClass()
						.equals(AdminCreateShopProductActionBean.class)
				|| context.getActionBean().getClass()
						.equals(AdminCreateUserActionBean.class)
				|| context.getActionBean().getClass()
						.equals(AdminActionBean.class)
				|| context.getActionBean().getClass()
						.equals(AdminEditProductActionBean.class)
				|| context.getActionBean().getClass()
						.equals(AdminEditProductlocaleActionbean.class)
				|| context.getActionBean().getClass()
						.equals(AdminEditUserActionBean.class)
				|| context.getActionBean().getClass()
						.equals(AdminListOrdersActionBean.class)
				|| context.getActionBean().getClass()
						.equals(AdminShowUserActionBean.class)
				|| context.getActionBean().getClass()
						.equals(AdminListProductlocalesActionBean.class)
				|| context.getActionBean().getClass()
						.equals(HelloAdminActionBean.class)) {

			AccountDTO account =null;
			account = (AccountDTO) session.getAttribute("accountDO");
			if (account != null) {
				if (!(account.getRole().getRole().equals(RoleDTO.getSysAdmin(account.getSystem().getId()).getRole())))
					{context.setResolution(new ForwardResolution(
							"/velocity/AdminUI/adminillegalacess.vm"));
					}
			}

		}
		return context.proceed();
	}
}
