package org.linuxtag.eticket.userfrontend.admin;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.product.ProductPoolBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

@UrlBinding("/dispatcher/adminedituser")
public class AdminEditUserActionBean extends GenericLocale implements
		ValidationErrorHandler {
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 1)
	private String salutation;
	@Validate(on = "savePersonalData", required = false, trim = true, maxlength = 100)
	private String title;
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String firstname;
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String lastname;
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String address;
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String zipcode;
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String city;
	@Validate(on = "savePersonalData", required = false, trim = true, maxlength = 100)
	private String country;
	private String localecode;
	private String role;
	private String orga;

	private String email;

	private String pseudoUID;

	private ActionBeanContext context;

	@DefaultHandler
	public Resolution showUserData() throws Exception {
		getContext().getRequest().getSession().removeAttribute("adminerror");

		return new ForwardResolution("/velocity/AdminUI/adminedituser.vm");
	}

	public Resolution savePersonalData() throws Exception {
		ProductPoolBeanService productPoolBeanService = JndiUtil
				.lookup("ProductPoolBean");
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();

		AccountDTO useraccountDTO = getAccountDTO();

		List<ProductPoolDTO> fixedProducts = productPoolBeanService
				.getProductPoolList("FIXED", useraccountDTO.getUsername());
		RoleDTO newrole = createRoleDTO(useraccountDTO.getSystem());
		AccountDTO updatedaccount = AccountDTO
				.initWithExistingHash(
						useraccountDTO.getSystem(),
						useraccountDTO.getUsername(),
						useraccountDTO.getPassword(), newrole,
						useraccountDTO.getOpenId());

		if (fixedProducts.size() > 0) {
			accountBeanService.updateRole(
					(AccountDTO) session.getAttribute("accountDO"),
					useraccountDTO, updatedaccount);
			getContext().getRequest().setAttribute("message",
					"confirm.editpersonaldata.fixedProduct");
			session.setAttribute("useraccountDTO", updatedaccount);
			session.setAttribute("adminerror", "editerror");
		} else {
			ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
			if (getLocalecode() == null)
				setLocalecode(system.getDefaultLocale().getCode());
			accountBeanService.updateRole(
					(AccountDTO) session.getAttribute("accountDO"),
					useraccountDTO, updatedaccount);
			LocaleDTO localeDO = setLocale(system);
			PersonalInformationDTO personalInformationDTO = initPersonalInformation(
					updatedaccount, localeDO);

			if (session.getAttribute("userpersonalInformation") == null) {
				accountBeanService.UpdatePersonalInformationplusticket(
						useraccountDTO, personalInformationDTO);
			} else {
				accountBeanService.UpdatePersonalInformation(useraccountDTO,
						personalInformationDTO);
			}
			session.setAttribute("useraccountDTO", updatedaccount);
			getContext().getResponse().setContentType(
					"text/html; charset=UTF-8");
			session.setAttribute("userpersonalInformation",
					personalInformationDTO);

		}
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/adminshowuser"));
	}

	private RoleDTO createRoleDTO(ESystemDTO eSystemDTO) {
		RoleDTO back = null;
		if (getRole().equals("user")) {
			back = RoleDTO.getUser(eSystemDTO.getId());
		}
		if (getRole().equals("sysadmin")) {
			back = RoleDTO.getSysAdmin(eSystemDTO.getId());
		}
		if (getRole().equals("moderator")) {
			back = RoleDTO.getModerator(eSystemDTO.getId());
		}
		if (getRole().equals("productadmin")) {
			back = RoleDTO.getProductAdmin(eSystemDTO.getId());
		}
		return back;
	}

	private LocaleDTO setLocale(ESystemDTO system) {
		LocaleDTO localeDO = null;
		if (system.getDefaultLocale().getCode().equals(getLocalecode())) {
			localeDO = system.getDefaultLocale();
		} else {
			for (LocaleDTO locale : system.getSupportedLocales()) {
				if (locale.getCode().equals(getLocalecode())) {
					localeDO = locale;
					break;
				}
			}
		}
		return localeDO;
	}

	private PersonalInformationDTO initPersonalInformation(
			AccountDTO accountDTO, LocaleDTO localeDO)
			throws UnsupportedEncodingException {
		return PersonalInformationDTO.createNewPersonalInformationDTO(
				getUtf8(getSalutation()), getUtf8(getTitle()),
				getUtf8(getFirstname()), getUtf8(getLastname()),
				getUtf8(getAddress()), getUtf8(getZipcode()),
				getUtf8(getCity()), getUtf8(getCountry()), localeDO,
				getUtf8(getOrga()), getUtf8(getUser(accountDTO)));
	}

	public String getUser(AccountDTO accountDTO) {
		HttpSession session = getContext().getRequest().getSession();
		PersonalInformationDTO persinf = (PersonalInformationDTO) session
				.getAttribute("userpersonalInformation");
		String back = null;
		if (persinf == null) {
			back = accountDTO.getUsername();
		} else {
			back = persinf.getEmail();
		}
		return back;
	}

	private AccountDTO getAccountDTO() {
		HttpSession session = getContext().getRequest().getSession();
		return (AccountDTO) session.getAttribute("useraccountDTO");
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {

		return new ForwardResolution("/velocity/AdminUI/adminedituser.vm");
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation)
			throws UnsupportedEncodingException {
		this.salutation = salutation;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) throws UnsupportedEncodingException {
		this.title = title;
	}

	private String getUtf8(String string) throws UnsupportedEncodingException {
		if (string != null
				&& (getContext().getRequest().getCharacterEncoding() == null || getContext()
						.getRequest().getCharacterEncoding().equals("UTF-8")))
			return new String(string.getBytes("8859_1"), "UTF8");
		else
			return string;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname)
			throws UnsupportedEncodingException {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname)
			throws UnsupportedEncodingException {
		this.lastname = lastname;
		;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) throws UnsupportedEncodingException {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) throws UnsupportedEncodingException {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) throws UnsupportedEncodingException {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) throws UnsupportedEncodingException {
		this.country = country;
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public void setLocalecode(String localecode) {
		this.localecode = localecode;
	}

	public String getLocalecode() {
		return localecode;
	}

	public void setOrga(String orga) throws UnsupportedEncodingException {
		this.orga = orga;
	}

	public String getOrga() {
		return orga;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String s) {
		this.email = s;
	}

	public String getPseudoUID() {
		return pseudoUID;
	}

	public void setPseudoUID(String s) {
		this.pseudoUID = s;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) throws UnsupportedEncodingException {
		this.role = role;
	}

	/**
	 * Before Test, if the user is an admin
	 * 
	 * @throws IllegalAccessException
	 *             If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException {
		setLocale((Locale) getContext().getRequest().getSession()
				.getAttribute(LOCALE));
		AccountDTO myAccount = (AccountDTO) getContext().getRequest()
				.getSession().getAttribute("accountDO");
		if (!myAccount.getRole().equals(
				RoleDTO.getSysAdmin(myAccount.getSystem().getId()))) {
			throw new IllegalAccessException();
		}

	}
}
