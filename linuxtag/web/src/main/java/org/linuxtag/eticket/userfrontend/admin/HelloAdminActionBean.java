package org.linuxtag.eticket.userfrontend.admin;


import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * 
 * @author dgille
 *
 *This class handles the helloadmin url,
 *a welcome screen for admins.
 */



@UrlBinding("/dispatcher/helloadmin")
public class HelloAdminActionBean extends GenericLocale implements ActionBean,
		ValidationErrorHandler {
	private ActionBeanContext context;

	
	
	/**
	 * This Method handle the welcome screen,
	 * it uses only two generated values, the user amount and the user amount of unregistered Users.
	 * The rest of the view is only static Date, a short overview about things, an admin can do.
	 * 
	 * @return
	 *        helloAdmin welcome Screen
	 * @throws IOException
	 */
	@DefaultHandler
	public Resolution showInterface() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO myAccount =(AccountDTO) session.getAttribute("accountDO");
		AccountBeanService accountbean = JndiUtil.lookup("AccountBean");
		String totalAccounts =accountbean.getAccountMount(myAccount);
	    session.setAttribute("totalAccounts",totalAccounts);
		String totalinactiveAccounts =accountbean.getInaktiveAccountMount(myAccount);	
		session.setAttribute("totalInaktiveAccounts",totalinactiveAccounts);
		
		return new ForwardResolution("/velocity/AdminUI/adminwelcome.vm");
	}
	
	
	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}


	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**Before Test, if the user is an admin 
	 * 
	 * @throws IllegalAccessException
	 * 					If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException{
		setLocale((Locale) getContext().getRequest().getSession().getAttribute(LOCALE));
		AccountDTO myAccount = (AccountDTO) getContext().getRequest().getSession().getAttribute("accountDO");
		if (!myAccount.getRole().equals(RoleDTO.getSysAdmin(myAccount.getSystem().getId()))){
			throw new IllegalAccessException();
		}
		
	}
}