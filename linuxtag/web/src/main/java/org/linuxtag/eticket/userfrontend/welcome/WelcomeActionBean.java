package org.linuxtag.eticket.userfrontend.welcome;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;

@UrlBinding(value = "/dispatcher/")
public class WelcomeActionBean extends GenericLocale {

	private ActionBeanContext context;

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	@DefaultHandler
	public Resolution showWelcome() {
		HttpSession session = getContext().getRequest().getSession();
		locale = (Locale) session.getAttribute("locale");
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		AccountDTO accountDTO = (AccountDTO) session.getAttribute("accountDO");
		
		if (accountDTO != null)
			return new ForwardResolution("/dispatcher/myproduct");
		else
			return new ForwardResolution("/velocity/index.vm");
	}	
}
