package org.linuxtag.eticket.userfrontend.exeption;

import org.linuxtag.eticket.userfrontend.locale.GenericLocale;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

@UrlBinding("/dispatcher/exception")
public class ErrorActionBean extends GenericLocale {
	private ActionBeanContext context;
	private String errormsg;

	@Override
	public void setContext(ActionBeanContext context) {
		this.context = context;

	}

	@Override
	public ActionBeanContext getContext() {
		return context;
	}

	@DefaultHandler
	public Resolution redirectToErrorPage() {
		context.getRequest().getSession()
				.setAttribute("errorMsg", getErrormsg());
		return new ForwardResolution("/velocity/error.vm");
	}

	public void setErrormsg(String errormsg) {
		this.errormsg = errormsg;
	}

	public String getErrormsg() {
		return errormsg;
	}

}
