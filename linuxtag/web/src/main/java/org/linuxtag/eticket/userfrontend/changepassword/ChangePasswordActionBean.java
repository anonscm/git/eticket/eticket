
package org.linuxtag.eticket.userfrontend.changepassword;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * This class handles all work to change the password.
 *
 */
@UrlBinding("/dispatcher/passwordchange")
public class ChangePasswordActionBean extends GenericLocale implements
		ValidationErrorHandler {
	private ActionBeanContext context;
	
	/**
	 * This method shows the view to change the password
	 */
	@DefaultHandler
	public Resolution showChangePassword() {
		HttpSession session = getContext().getRequest().getSession();
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		if (session.getAttribute("state") != null)
			return new ForwardResolution(
					generateLocaleLink("/velocity/editpersonaldata.vm"));
		return new ForwardResolution("/velocity/changepassword.vm");
	}

	/**
	 *	This method call the method to generate and send a token to the eMail adress.  
	 */
	public Resolution passwordchange() {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		if (session.getAttribute("state") != null)
			return new ForwardResolution(
					generateLocaleLink("/velocity/editpersonaldata.vm"));
		AccountDTO accountDTO = (AccountDTO) session.getAttribute("accountDO");
	
		accountBeanService.generateToken(accountDTO); 
		
		getContext().getRequest().setAttribute("message", "cconfirm.passwordchange");
		
		return new  ForwardResolution("/velocity/changepasswordinfo.vm");
	}	

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {

		return showChangePassword();
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public ActionBeanContext getContext() {
		return context;
	}
}

