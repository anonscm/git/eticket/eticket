package org.linuxtag.eticket.userfrontend.product;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.ejb.EJBTransactionRolledbackException;
import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.output.OutputException;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ScopedLocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.pdf.PrintProductPdfBeanService;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.ProductPoolBeanService;
import org.evolvis.eticket.business.product.ProductTransferService;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.model.entity.PdfField;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;
import org.linuxtag.eticket.userfrontend.history.HistoryActionBean;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

@UrlBinding("/dispatcher/myproduct")
public class MyProductActionBean extends GenericLocale implements
		ValidationErrorHandler {
	private static final String APPLICATION_PDF = "application/pdf";

	private long productId;
	private ActionBeanContext context;

	@Validate(on = "giveaway", required = true, mask = "[^@]+@.+\\.[^.]+")
	private String acceptor;
	@Validate(on = "giveaway", required = true, minvalue = 1)
	private int amount = 1;
	@Validate(on = "giveaway", required = true, minvalue = 1)
	private String password;
	private String productUin;
	private String templateUin;

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public String getProductUin() {
		return productUin;
	}

	public void setProductUin(String productUin) {
		this.productUin = productUin;
	}

	@DefaultHandler
	public Resolution showProductDetail() throws Exception,
			InvalidParameterException {
		HttpSession session = getContext().getRequest().getSession();
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		refreshPoolLists(session);
		return new ForwardResolution("/velocity/myproduct.vm");
	}

	private void productLocalesFromActiveProductsToSession(
			List<ProductPoolDTO> productPoolDTOs, HttpSession session) {
		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		locale = (Locale) session.getAttribute("locale");
		Map<String, ProductLocaleDTO> productLocale = (Map<String, ProductLocaleDTO>) session
				.getAttribute("ProductLocaleMap");
		if (productLocale == null) {
			productLocale = new HashMap<String, ProductLocaleDTO>();
		}

		for (ProductPoolDTO ppDTO : productPoolDTOs) {
			ProductLocaleDTO productLocaleDTO = null;
			try {
				productLocaleDTO = productBeanService.getProductLocale(
						locale.toString(), ppDTO.getProductDTO().getUin(),
						system.getId());
			} catch (EJBTransactionRolledbackException e) {
				// TODO Exception Behandlung - diese tritt auf wenn keine
				// passendes ProductLocale existiert. soll ich dies weiter
				// behandlen?
			}

			if (productLocaleDTO != null) {
				// Add corresponding productLocaleDTO
				productLocale.put(ppDTO.getProductDTO().getUin(),
						productLocaleDTO);
			}
		}

		session.setAttribute("ProductLocaleMap", productLocale);
	}

	public void refreshPoolLists(HttpSession session)
			throws InvalidParameterException {
		ProductTransferService productTransferService = JndiUtil
				.lookup("ProductTransferBean");
		ProductPoolBeanService productPoolBeanService = JndiUtil
				.lookup("ProductPoolBean");
		AccountDTO accountDO = (AccountDTO) session.getAttribute("accountDO");

		try {
			refreshPoolListsVolatileProductPools(session,
					productPoolBeanService, accountDO);
			refreshPoolListsFixedProductPools(session, productPoolBeanService,
					accountDO);
			HistoryActionBean historyActionBean = new HistoryActionBean();
			historyActionBean.refreshPoolListsTransferactivitiesOpen(session,
					productTransferService, accountDO);
			historyActionBean.refreshPoolListsTransferactivitiesOpenRecipient(
					session, productTransferService, accountDO);
		} catch (NullPointerException npe) {
			System.err
					.println("Either AccountDO or AccountAgentRemote EJB is NULL");
			System.err.println(npe);
		}
	}

	private void refreshPoolListsVolatileProductPools(HttpSession session,
			ProductPoolBeanService productPoolBeanService, AccountDTO accountDO) {
		List<ProductPoolDTO> volatileProductPools = new ArrayList<ProductPoolDTO>();
		for (ProductPoolDTO productPoolDTO : productPoolBeanService
				.getProductPoolList("VOLATILE", accountDO.getUsername())) {
			if (!productPoolDTO.getAmount().equals("0")) {
				volatileProductPools.add(productPoolDTO);
			}
		}
		productLocalesFromActiveProductsToSession(volatileProductPools, session);
		session.setAttribute("VolatileProductPools", volatileProductPools);
	}

	private void refreshPoolListsFixedProductPools(HttpSession session,
			ProductPoolBeanService productPoolBeanService, AccountDTO accountDO) {
		List<ProductPoolDTO> fixedProducts = null;
		fixedProducts = productPoolBeanService.getProductPoolList("FIXED",
				accountDO.getUsername());
		productLocalesFromActiveProductsToSession(fixedProducts, session);
		session.setAttribute("FixedProducts", fixedProducts);
	}

	public Resolution print() throws Exception {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");

		HttpSession session = getContext().getRequest().getSession();
		AccountDTO accountDO = (AccountDTO) session.getAttribute("accountDO");

		if (accountBeanService.getPersonalInformationDTO(accountDO.getSystem(),
				accountDO.getUsername()) != null) {
			ProductDTO productDO = getProductDTO();
			return getPdf(accountDO, productDO);
		} else {
			throw new Exception(
					"No personal data set, please set personal data at 'personal data'.");
		}
	}

	private Resolution getPdf(final AccountDTO accountDO,
			final ProductDTO productDO) throws IOException,
			InvalidParameterException, NamingException {
		return new Resolution() {
			@Override
			public void execute(HttpServletRequest request,
					HttpServletResponse response) throws Exception {
				HttpServletResponse res = getContext().getResponse();
				res.setHeader("Content-Disposition",
						"inline; attachment; filename=" + productDO.getUin()
								+ ".pdf");
				res.setContentType(APPLICATION_PDF);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				byte[] pdf = printPdf(accountDO);

				baos.write(pdf);
				res.setContentLength(baos.size());
				ServletOutputStream out = res.getOutputStream();
				baos.writeTo(out);
				out.flush();
				out.close();
			}
		};

	}

	private byte[] printPdf(AccountDTO accountDO) throws IOException,
			OutputException, BarcodeException {
		PrintProductPdfBeanService printProductPdfBeanService = JndiUtil
				.lookup("PrintProductPdfBean");
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		SystemBeanService systemBeanService = JndiUtil.lookup("SystemBean");
		PersonalInformationDTO personalInformationDTO = accountBeanService
				.getPersonalInformationDTO(accountDO.getSystem(),
						accountDO.getUsername());

		ProductPoolBeanService productPoolBeanService = JndiUtil
				.lookup("ProductPoolBean");
		String productNumber = productPoolBeanService
				.getProductNumber(productId);

		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		TemplatesDTO templatesDTO = productBeanService.getTemplate(templateUin);

		HttpSession session = getContext().getRequest().getSession();
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");

		return printProductPdfBeanService.createPDF(
				createMap(personalInformationDTO), templatesDTO, productNumber,
				system);
	}

	private Map<PdfField, Object> createMap(
			PersonalInformationDTO personalInformationDTO)
			throws OutputException, BarcodeException {
		Map<PdfField, Object> values = new HashMap<PdfField, Object>();
		values.put(PdfField.FIRSTNAME, personalInformationDTO.getFirstname());
		values.put(PdfField.LASTNAME, personalInformationDTO.getLastname());
		values.put(PdfField.CITY, personalInformationDTO.getCity());
		values.put(PdfField.ADRESS, personalInformationDTO.getAddress());
		values.put(PdfField.ORGA, personalInformationDTO.getOrganisation());
		values.put(PdfField.ZIP, personalInformationDTO.getZipcode());
		values.put(PdfField.BARCODE, true);
		return values;
	}

	public Resolution fix() throws Exception {
		ProductPoolBeanService productPoolBeanService = JndiUtil
				.lookup("ProductPoolBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO accountDO = (AccountDTO) session.getAttribute("accountDO");
		try {
			ProductDTO productDO = getProductDTO();
			try {
				productPoolBeanService.fixProduct(accountDO, productDO);
				getPdf(accountDO, productDO);
				getContext().getRequest().setAttribute("message",
						"confirm.myproductFix");
			} catch (RuntimeException e) {
				getContext().getRequest().setAttribute("message",
						"inform.alreadyfixedproduct");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidParameterException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}

		return showProductDetail();
	}

	public Resolution send() {
		ProductDTO productDO = getProductDTO();
		HttpSession session = getContext().getRequest().getSession();
		session.setAttribute("productDTO", productDO);
		getContext().getRequest().setAttribute("product", productDO);
		return new ForwardResolution("/velocity/send.vm");
	}

	public Resolution giveaway() throws Exception {
		ProductTransferService productTransferService = JndiUtil
				.lookup("ProductTransferBean");
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		getContext().getRequest().setAttribute("product",
				session.getAttribute("productDTO"));

		String mail = getContext().getRequest().getParameter("acceptor");
		ESystemDTO eSystemDTO = (ESystemDTO) session.getAttribute("systemName");
		long menge = Long.parseLong(getContext().getRequest().getParameter(
				"amount"));
		AccountDTO accountDTO = setPasswordInPlainText((AccountDTO) session
				.getAttribute("accountDO"));

		if (!checkPassword(accountDTO)) {
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/myproduct", "myproduct.incorrectPassword");

			ValidationErrors errors = new ValidationErrors();
			errors.add("password", error);
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/send.vm");
		}

		if (accountDTO.getUsername().equals(mail)) {
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/myproduct", "sendProduct.incorrectAcceptor");
			ValidationErrors errors = new ValidationErrors();
			errors.add("acceptor", error);
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/send.vm");
		}

		ProductDTO productDTO = getProductDTO();
		AccountDTO acceptor;
		try {
			acceptor = accountBeanService.find(eSystemDTO, mail);
		} catch (Exception e) {
			acceptor = AccountDTO.init(eSystemDTO, mail, UUID
					.randomUUID().toString().replace("-", ""),
					RoleDTO.getUser(eSystemDTO.getId()), null);
			accountBeanService.createUser(acceptor, eSystemDTO
					.getDefaultLocale().getCode());
		}
		accountDTO = (AccountDTO) session.getAttribute("accountDO");
		productTransferService.transferProductToAccount(accountDTO, productDTO,
				acceptor, menge);
		getContext().getRequest().setAttribute("message",
				"confirm.myproductGiveaway");
		return showProductDetail();
	}

	private boolean checkPassword(AccountDTO accountDTO) {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		try {
			accountBeanService.validatePassword(accountDTO);
			return true;
		} catch (IllegalAccessException e) {
			return false;
		}
	}

	private AccountDTO setPasswordInPlainText(AccountDTO accountDTO) {
		return AccountDTO.init(accountDTO.getSystem(),
				accountDTO.getUsername(), password, accountDTO.getRole(), null);
	}

	/**
	 * Returns the ProduktDTO of the given productId and the System
	 * 
	 * @return ProduktDTO the Data transfer object of Product
	 */
	private ProductDTO getProductDTO() {
		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		HttpSession session = getContext().getRequest().getSession();
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		return productBeanService.get(system.getName(), productUin);
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		HttpSession session = getContext().getRequest().getSession();
		getContext().getRequest().setAttribute("product",
				session.getAttribute("productDTO"));
		return new ForwardResolution("/velocity/send.vm");
	}

	public String getAcceptor() {
		return acceptor;
	}

	public void setAcceptor(String acceptor) {
		this.acceptor = acceptor;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getTemplateUin() {
		return templateUin;
	}

	public void setTemplateUin(String templateUin) {
		this.templateUin = templateUin;
	}
}