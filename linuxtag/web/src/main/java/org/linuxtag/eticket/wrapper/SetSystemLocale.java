package org.linuxtag.eticket.wrapper;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.linuxtag.utils.JndiUtil;

public class SetSystemLocale extends HttpServlet {
	private static final long serialVersionUID = -2781651961927950858L;

	public static final String LOCALE = "locale";
	public static final String SYSTEM = "systemName";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		try {
			SystemBeanService systemBeanService = JndiUtil.lookup("SystemBean");
			String[] values = null;

			if (req.getPathInfo() != null)
				values = req.getPathInfo().split("/");
			else
				throw new IOException("No extra path.");
			if (values.length != 3 || values[1].isEmpty()
					|| values[2].isEmpty())
				throw new IOException("Wrong extra path.");
			ESystemDTO system = null;
			Locale locale = new Locale(values[2]);
			system = systemBeanService.getSystemByName(values[1]);

			req.getSession().setAttribute(SYSTEM, system);
			req.getSession().setAttribute(LOCALE, locale);
			// we need to use redirect thereby the client knows about the url
			// change
			if (req.getSession().getAttribute("state") != null) {
				resp.sendRedirect("/LinuxTagUserWeb/dispatcher/editpersonaldata?locale="
						+ locale);
			} else {

				resp.sendRedirect("/LinuxTagUserWeb/dispatcher/welcome?locale="
						+ locale);
			}
		} catch (Exception e) {
			try {
				resp.sendRedirect("/LinuxTagUserWeb/dispatcher/exception?locale=en&errorMsg="
						+ e);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
}
