package org.linuxtag.eticket.userfrontend.admin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**Create a ShopPorduct like the normal CreateProductActionBean
 * but does'not support ProductAutpomatTransfers
 * @author dgille
 *
 */


@UrlBinding("/dispatcher/adminlistproductlocales")
public class AdminListProductlocalesActionBean extends GenericLocale implements
		ActionBean{
	private ActionBeanContext context;
	private List<ProductLocaleDTO> liste;
	

	
	/**Generates a form for creating Products
	 * 
	 * @return
	 *        Admincreateproduct.vm
	 * @throws IOException
	 */
	
	
	@DefaultHandler
	public Resolution showInterface() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO myAccount = (AccountDTO) session.getAttribute("accountDO");
		ProductBeanService productBeanRemote = JndiUtil.lookup("ProductBean");
		liste = productBeanRemote.getAllProductLocales(myAccount);

		return new ForwardResolution("/velocity/AdminUI/adminlistproductlocales.vm");
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	

	public List<ProductLocaleDTO> getListe() {
		return liste;
	}

	public void setListe(List<ProductLocaleDTO> liste) {
		this.liste = liste;
	}
	
	
	/**Before Test, if the user is an admin 
	 * 
	 * @throws IllegalAccessException
	 * 					If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException{
		setLocale((Locale) getContext().getRequest().getSession().getAttribute(LOCALE));
		AccountDTO myAccount = (AccountDTO) getContext().getRequest().getSession().getAttribute("accountDO");
		if (!myAccount.getRole().equals(RoleDTO.getSysAdmin(myAccount.getSystem().getId()))){
			throw new IllegalAccessException();
		}
		
	}

}