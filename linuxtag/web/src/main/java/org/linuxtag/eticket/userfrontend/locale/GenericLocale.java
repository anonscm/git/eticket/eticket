package org.linuxtag.eticket.userfrontend.locale;

import java.util.Locale;

import net.sourceforge.stripes.action.ActionBean;
/**
 * Stripes set the locale with parameter, every link has to contain a locale parameter 
 *
 */
public abstract class GenericLocale implements ActionBean {
	public final String LOCALE = "locale";
	protected Locale locale;

	protected String generateLocaleLink(String url) {
		return url + "?" + LOCALE + "=" + getLocale();
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Locale getLocale() {
		return locale;
	}
}
