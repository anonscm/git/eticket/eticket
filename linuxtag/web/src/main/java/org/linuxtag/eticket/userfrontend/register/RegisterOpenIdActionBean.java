package org.linuxtag.eticket.userfrontend.register;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ScopedLocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

@UrlBinding(value = "/dispatcher/registerOpenId")
public class RegisterOpenIdActionBean extends GenericLocale implements
		ValidationErrorHandler {

	private ActionBeanContext context;
	@Validate(on = "register", required = true, trim = true, minlength = 1, maxlength = 100, mask = "[^@]+@.+\\.[^.]+")
	private String registerUsername;

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public String getRegisterUsername() {
		return registerUsername;
	}

	public void setRegisterUsername(String registerUsername) {
		this.registerUsername = registerUsername;
	}

	public Resolution register() {
		HttpSession session = getContext().getRequest().getSession();
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		
		String password = UUID.randomUUID().toString().replace("-", "");
		AccountDTO accountDTO = AccountDTO.init(system, registerUsername,
				password, RoleDTO.getUser(system.getId()), session.getAttribute("openid").toString());
		PersonalInformationDTO personalInformationDTO = setPersonalInformation();
		try{
			accountBeanService.createUser(accountDTO, system.getDefaultLocale().getCode()); 
		}catch (Exception e) {
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/register", "usernameAlreadyExists");
			ValidationErrors errors = new ValidationErrors();
			errors.add("registerUsername", error);
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/setEmail.vm");
		}
		accountBeanService.UpdatePersonalInformation(accountDTO, personalInformationDTO);
		List<String> openIds = Arrays.asList(session.getAttribute("openid").toString());
		
		
		getContext().getRequest().setAttribute("message",
				"confirm.register");
		return new ForwardResolution("/velocity/confirm.vm");
		
	}

	private PersonalInformationDTO setPersonalInformation() {
		HttpSession session = getContext().getRequest().getSession();
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		String gender = "";
		if (session.getAttribute("gender").toString().equals("M")) {
			gender = "m";
		} else if (session.getAttribute("gender").toString().equals("F")) {
			gender = "f";
		}
		String country ="";
		if(session.getAttribute("country").toString().equals("DE")){
			country = "de";
		}else if(session.getAttribute("country").toString().equals("GB")){
			country = "en";
		}else if(session.getAttribute("country").toString().equals("AU")){
			country = "en";
		}else if(session.getAttribute("country").toString().equals("US")){
			country = "en";
		}  
		
		PersonalInformationDTO personalInformationDTO = PersonalInformationDTO
				.createNewPersonalInformationDTO(gender, "", "", "", "", session
						.getAttribute("postcode").toString(), "", country, system
						.getDefaultLocale(), "", registerUsername);
		return personalInformationDTO;
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		return null;
	}

}
