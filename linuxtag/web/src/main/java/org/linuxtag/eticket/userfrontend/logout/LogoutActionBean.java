
package org.linuxtag.eticket.userfrontend.logout;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;

@UrlBinding("/dispatcher/logout")
public class LogoutActionBean extends GenericLocale {
	private ActionBeanContext context;

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	@DefaultHandler
	public Resolution doLogout() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		
		HttpSession session = getContext().getRequest().getSession();
		if (session.getAttribute("accountDO") != null) {
			session.removeAttribute("VolatileProductPools");
			session.removeAttribute("FixedProducts");
			session.removeAttribute("accountDO");
		}
		//we need to get system and locale first, so we're able to redirect to a concrete system 
		String system = ((ESystemDTO) session.getAttribute("systemName")).getName();
		Locale locale = ((Locale) session.getAttribute("locale"));
		setLocale(locale);
		session.invalidate();
		return new RedirectResolution(generateLocaleLink("/dispatcher/welcome"));
	}
}

