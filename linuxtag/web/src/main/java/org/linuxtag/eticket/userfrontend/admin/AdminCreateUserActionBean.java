package org.linuxtag.eticket.userfrontend.admin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ScopedLocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

@UrlBinding("/dispatcher/admincreateuser")
public class AdminCreateUserActionBean extends GenericLocale implements
		ActionBean, ValidationErrorHandler {

	@Validate(on = "createUser", required = true, trim = true, minlength = 1, maxlength = 254, mask = "[^@]+@.+\\.[^.]+")
	private String registerUsername;
	private ActionBeanContext context;

	private String salutation;

	private String title;

	private String firstname;

	private String lastname;

	private String address;

	private String zipcode;

	private String city;

	private String country;
	private String localecode;
	private String role;
	private String orga;

	@DefaultHandler
	public Resolution showInterface() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		return new ForwardResolution("/velocity/AdminUI/admincreateuser.vm");
	}

	public Resolution createUser() throws IOException {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO adminaccount = (AccountDTO) session
				.getAttribute("accountDO");
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");

		RoleDTO newrole = createRoleDTO(adminaccount.getSystem());
		AccountDTO user = AccountDTO.init(system,
				registerUsername, UUID.randomUUID().toString(), newrole, null);
		LocaleDTO localeDO = setLocale(system);
		PersonalInformationDTO personalInformationDTO = initPersonalInformation(
				user, localeDO);
		try {
			accountBeanService.createunactivatedAccount(adminaccount, user,
					personalInformationDTO);
			accountBeanService.UpdatePersonalInformation(user,
					personalInformationDTO);
			session.setAttribute("useraccountDTO", user);
			session.setAttribute("userpersonalInformation",
					personalInformationDTO);
			ProductBeanService productBeanService = JndiUtil
					.lookup("ProductBean");
			List<ProductDTO> products = productBeanService.listBySystemClass(
					adminaccount, system);
			session.setAttribute("products", products);
			return new RedirectResolution(
					generateLocaleLink("/dispatcher/adminshowuser"));

		} catch (Exception e) {
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/register", "usernameAlreadyExists");
			ValidationErrors errors = new ValidationErrors();
			errors.add("registerUsername", error);
			getContext().setValidationErrors(errors);
		}
		return showInterface();
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public String getRegisterUsername() {
		return registerUsername;
	}

	public void setRegisterUsername(String registerUsername) {
		this.registerUsername = registerUsername;
	}

	public ActionBeanContext getContext() {
		return context;
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {

		return new ForwardResolution("/velocity/AdminUI/admincreateuser.vm");
	}

	private RoleDTO createRoleDTO(ESystemDTO eSystemDTO) {

		RoleDTO back = null;
		if (getRole().equals("user")) {
			back = RoleDTO.getUser(eSystemDTO.getId());
		}
		if (getRole().equals("sysadmin")) {
			back = RoleDTO.getSysAdmin(eSystemDTO.getId());
		}
		if (getRole().equals("moderator")) {
			back = RoleDTO.getModerator(eSystemDTO.getId());
		}
		if (getRole().equals("productadmin")) {
			back = RoleDTO.getProductAdmin(eSystemDTO.getId());
		}
		return back;
	}

	private LocaleDTO setLocale(ESystemDTO system) {
		LocaleDTO locale = null;
		if (getLocalecode().equals("en")) {
			locale = LocaleDTO.createNewLocaleDTO(getLocalecode(), "english");
		} else {
			locale = LocaleDTO.createNewLocaleDTO(getLocalecode(), "deutsch");
		}

		return locale;
	}

	private PersonalInformationDTO initPersonalInformation(
			AccountDTO accountDTO, LocaleDTO localeDO)
			throws UnsupportedEncodingException {
		return PersonalInformationDTO.createNewPersonalInformationDTO(
				getUtf8(getSalutation()), getUtf8(getTitle()),
				getUtf8(getFirstname()), getUtf8(getLastname()),
				getUtf8(getAddress()), getUtf8(getZipcode()),
				getUtf8(getCity()), getUtf8(getCountry()), localeDO,
				getUtf8(getOrga()), accountDTO.getUsername());
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation)
			throws UnsupportedEncodingException {
		this.salutation = salutation;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) throws UnsupportedEncodingException {
		this.title = title;
	}

	private String getUtf8(String string) throws UnsupportedEncodingException {
		if (string != null
				&& (getContext().getRequest().getCharacterEncoding() == null || getContext()
						.getRequest().getCharacterEncoding().equals("UTF-8")))
			return new String(string.getBytes("8859_1"), "UTF8");
		else
			return string;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname)
			throws UnsupportedEncodingException {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname)
			throws UnsupportedEncodingException {
		this.lastname = lastname;
		;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) throws UnsupportedEncodingException {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) throws UnsupportedEncodingException {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) throws UnsupportedEncodingException {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) throws UnsupportedEncodingException {
		this.country = country;
	}

	public void setLocalecode(String localecode) {
		this.localecode = localecode;
	}

	public String getLocalecode() {
		return localecode;
	}

	public void setOrga(String orga) throws UnsupportedEncodingException {
		this.orga = orga;
	}

	public String getOrga() {
		return orga;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) throws UnsupportedEncodingException {
		this.role = role;
	}
	/**Before Test, if the user is an admin 
	 * 
	 * @throws IllegalAccessException
	 * 					If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException{
		setLocale((Locale) getContext().getRequest().getSession().getAttribute(LOCALE));
		AccountDTO myAccount = (AccountDTO) getContext().getRequest().getSession().getAttribute("accountDO");
		if (!myAccount.getRole().equals(RoleDTO.getSysAdmin(myAccount.getSystem().getId()))){
			throw new IllegalAccessException();
		}
		
	}
}