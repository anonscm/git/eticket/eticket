package org.linuxtag.eticket.filter;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.ExecutionContext;
import net.sourceforge.stripes.controller.Interceptor;
import net.sourceforge.stripes.controller.Intercepts;
import net.sourceforge.stripes.controller.LifecycleStage;

import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.linuxtag.utils.JndiUtil;

/**
 * This interceptor check if a system is in the session
 * 
 * @author mjohnk
 * 
 */
@Intercepts({ LifecycleStage.ResolutionExecution })
public class CheckSystem implements Interceptor {

	/**
	 * This method checks if a system object is in the session if not it will be
	 * set to the session
	 */
	@Override
	public Resolution intercept(ExecutionContext context) throws Exception {
		HttpSession session = context.getActionBeanContext().getRequest()
				.getSession();
		if (context.getActionBean().getContext().getEventName().equals("showWelcome")||
				context.getActionBean().getContext().getEventName().equals("setPassword")) {
			if (session.getAttribute("systemName") == null) {
				SystemBeanService systemBeanService = JndiUtil.lookup("SystemBean");
				ESystemDTO system = systemBeanService.getSystemByName("linuxtag");
				session.setAttribute("systemName", system);
			}
		}
		return context.proceed();
	}
}
