package org.linuxtag.eticket.filter;

import javax.servlet.http.HttpSession;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.linuxtag.utils.JndiUtil;

import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.ExecutionContext;
import net.sourceforge.stripes.controller.Interceptor;
import net.sourceforge.stripes.controller.Intercepts;
import net.sourceforge.stripes.controller.LifecycleStage;

@Intercepts({ LifecycleStage.ResolutionExecution })
public class CheckPersonalData implements Interceptor{

	@Override
	public Resolution intercept(ExecutionContext context) throws Exception {
		HttpSession session = context.getActionBeanContext().getRequest().getSession();
		if (context.getActionBean().getContext().getEventName().equals("loginSuccess") || 
				context.getActionBean().getContext().getEventName().equals("showPersonalData") ||
				context.getActionBean().getContext().getEventName().equals("showShop") ||
				context.getActionBean().getContext().getEventName().equals("showProductDetail") ||
				context.getActionBean().getContext().getEventName().equals("showHistory")){
			AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
			AccountDTO accountDTO = (AccountDTO) session.getAttribute("accountDO");
			
			PersonalInformationDTO informationDO = accountBeanService.getPersonalInformationDTO(accountDTO.getSystem(), accountDTO.getUsername());
			if (informationDO == null) {
				context.setResolution(new RedirectResolution("/dispatcher/editpersonaldata"));
			}
		}
		return context.proceed();
	}

}
