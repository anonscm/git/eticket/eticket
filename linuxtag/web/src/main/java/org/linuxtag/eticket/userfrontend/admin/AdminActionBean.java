package org.linuxtag.eticket.userfrontend.admin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.management.relation.Role;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.AccountShortSummeryDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductStatisticDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;
import org.evolvis.eticketshop.business.product.ShopProductBeanService;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;




/**
 * 
 * @author dgille
 *
 *
 *This class handles the usersearch an it also display'S the Product stattistis.
 *to-do: Exclude Statistics in its own class
 */



@UrlBinding("/dispatcher/adminindex")
public class AdminActionBean extends GenericLocale {

	private ActionBeanContext context;
	private static final String APPLICATION_PDF = "application/pdf";
	private AccountBeanService myAccountBeanService;
	private AccountDTO myAccountDTO;
	private HttpSession session;
	private String totalAccounts;
	boolean hasPrivileges;
	private List<ProductStatisticDTO> Statisticlist=new ArrayList<ProductStatisticDTO>();
	private String systemID;
	private String pseudoUID;
	
	private List<AccountShortSummeryDTO> userliste;

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public AccountBeanService getMyAccountBeanService() {
		return this.myAccountBeanService;
	}
	
	public void setMyAccountBeanService() {
		JndiUtil.lookup("AccountBean");
	}

	public AccountDTO getMyAccountDTO() {
		return this.myAccountDTO;
	}

	public void setMyAccountDTO() {
		this.myAccountDTO = (AccountDTO) this.session.getAttribute("accountDO");
	}

	public String getSystemID() {
		return systemID;
	}

	public String getPseudoUID() {
		return pseudoUID;
	}

	public void setPseudoUID(String s) {
		this.pseudoUID = s;
	}

	public String getTotalAccounts() {
		return totalAccounts;
	}

	public void settotalAccounts(String s) {
		this.totalAccounts = s;
	}
	

	public void AdminAccountBean() {
		myAccountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		myAccountDTO = (AccountDTO) session.getAttribute("accountDO");

	}

	
	/**
	 * Shows the product Statistics
	 * 
	 * @return
	 *        product statistics view
	 * @throws IOException
	 */
	@DefaultHandler
	public Resolution showInterface() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		buildstatistics();
		//Statisticlist.get(0).getProductDTO().getProductLocales()[0].getTemplate().getDescription()
		return new ForwardResolution("/velocity/AdminUI/adminaction.vm");
	}

	
	
	public Resolution printPDFTemplate(){
		String templateUin =getContext().getRequest().getParameter("templateUI");
		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		TemplatesDTO template = productBeanService.getTemplate(templateUin);
		return getPdf(template.getBinaryData());
	}
	
	
	private Resolution getPdf(final byte[] pdf) {
		return new Resolution() {
			@Override
		public void execute(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
				HttpServletResponse res = getContext().getResponse();
				res.setHeader("Content-Disposition",
						"inline; attachment; filename=123.pdf");
				res.setContentType(APPLICATION_PDF);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				baos.write(pdf);
				res.setContentLength(baos.size());
				ServletOutputStream out = res.getOutputStream();
				baos.writeTo(out);
				out.flush();
				out.close();
				}
			};
		}

	
	/**
	 * private Method for building product stattistics.
	 * It gets all products by the sytem and gets the corresponding statisic.
	 * The  products statics are saved in the private Statisticlist, which will be called by the view.
	 */
	
	
	private void buildstatistics() {
		
		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		ShopProductBeanService shopProductBeanService = JndiUtil.lookup("ShopProductBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO myAccount = (AccountDTO) session.getAttribute("accountDO");
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		List<ProductDTO> products = productBeanService.listBySystemClass(
				myAccount, system);
		if (!(products == null)) {
			for (int i = 0; i < products.size(); i++) {
				ProductStatisticDTO statistics = shopProductBeanService
						.getStatistic(products.get(i));
				Statisticlist.add(statistics);
			}
		
		}
	}
	
	
	/**This Method is called in the UserListView, to show the UserOverview (=adminshowuser)
	 * by the username
	 * 
	 * @return
	 *       Useroverview(=adminshowuser)
	 * @throws Exception
	 */
	
	public Resolution getUserFromUsername()throws Exception{
		String username =getContext().getRequest().getParameter("AccountUsername");
		AccountBeanService accountbean = JndiUtil.lookup("AccountBean");
		session = getContext().getRequest().getSession();
		AccountDTO myAccount =(AccountDTO) session.getAttribute("accountDO");
		AccountDTO user =accountbean.find(myAccount.getSystem(), username);
		session.setAttribute("useraccountDTO", user);
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/adminshowuser"));
	}
	
	
	/**
	 * This Method implements the user search. In the first try, it will search for the e-mail by a quit strict 
	 * comparistion. If its fit, the user will be directly redirected to the Useroverview. Otherwise the method will call the much more powerfull 
	 * method searchUser. It gets the result off all possible combinations of the string partition search in the fields e-mail,first name and last name.
	 * Then it will return a view off the list of the found users.
	 * @return
	 *        UserOverView or UserList
	 * @throws Exception
	 */


	public Resolution showUserData() throws Exception {
		getContext().getRequest().getSession().removeAttribute("adminerror");
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		HttpSession session = getContext().getRequest().getSession();
		myAccountBeanService=JndiUtil.lookup("AccountBean");
		myAccountDTO = (AccountDTO) session.getAttribute("accountDO");
		setPseudoUID(getUtf8(getPseudoUID()));
		if (getPseudoUID() != null) {
			try {
				AccountDTO thirdPartyAccountDTO = myAccountBeanService
						.searchUser(myAccountDTO, myAccountDTO.getSystem(),
								pseudoUID);
				session.setAttribute("useraccountDTO", thirdPartyAccountDTO);
				return new RedirectResolution(
						generateLocaleLink("/dispatcher/adminshowuser"));
			} catch (Exception e) {
				userliste= myAccountBeanService.searchUsers(
						myAccountDTO, myAccountDTO.getSystem(), pseudoUID);
				if (userliste == null||userliste.size()==0) {
					userliste=null;
					return new ForwardResolution("/velocity/AdminUI/adminlistusers.vm");
				}
				else {
					return new ForwardResolution("/velocity/AdminUI/adminlistusers.vm");
				}
			}

		} else {
			return new ForwardResolution("/velocity/AdminUI/adminlistusers.vm");
		}
	}

	private String getUtf8(String string) throws UnsupportedEncodingException {
		if (string != null
				&& (getContext().getRequest().getCharacterEncoding() == null || getContext()
						.getRequest().getCharacterEncoding().equals("UTF-8")))
			return new String(string.getBytes("8859_1"), "UTF8");
		else
			return string;
	}

	public List<ProductStatisticDTO> getStatisticlist() {
		return Statisticlist;
	}

	public void setStatisticlist(List<ProductStatisticDTO> statisticlist) {
		Statisticlist = statisticlist;
	}

	public List<AccountShortSummeryDTO> getUserliste() {
		return userliste;
	}

	public void setUserliste(List<AccountShortSummeryDTO> userliste) {
		this.userliste = userliste;
	}
	
	/**Before Test, if the user is an admin 
	 * 
	 * @throws IllegalAccessException
	 * 					If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException{
		AccountDTO myAccount = (AccountDTO) getContext().getRequest().getSession().getAttribute("accountDO");
		setLocale((Locale) getContext().getRequest().getSession().getAttribute(LOCALE));
		if (!myAccount.getRole().equals(RoleDTO.getSysAdmin(myAccount.getSystem().getId()))){
			throw new IllegalAccessException();
		}
		
	}
	

}
