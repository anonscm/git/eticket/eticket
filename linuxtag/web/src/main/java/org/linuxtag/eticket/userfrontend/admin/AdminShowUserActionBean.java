package org.linuxtag.eticket.userfrontend.admin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.ProductPoolBeanService;
import org.evolvis.eticket.business.product.ProductTransferService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.history.HistoryActionBean;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * 
 * @author dgille
 *
 *
 *This class handles the UserOverview
 */


@UrlBinding("/dispatcher/adminshowuser")
public class AdminShowUserActionBean extends GenericLocale implements
		ValidationErrorHandler {
	private static final String APPLICATION_PDF = "application/pdf";
	private String localecode;
	private String product;
	private String productUin;
	private String transferActivityId;
	private String amount;
	private String deleteamount;
	private List<ProductDTO> products;
	private PersonalInformationDTO persinfo;

	private String pseudoUID;

	private ActionBeanContext context;

	
	/** Gets the necassery Data, to show the User and his/her correspondening data
	 * 
	 * @return
	 *        Userovervievie (=adminshowuser.vm)
	 * @throws Exception
	 */
	
	@DefaultHandler
	public Resolution showUserData() throws Exception {
		product = null;
		HttpSession session = getContext().getRequest().getSession();
		session.setAttribute("product", product);
		if (session.getAttribute("userpersonalInformation") != null) {
			persinfo = (PersonalInformationDTO) session
					.getAttribute("userpersonalInformation");
		}
		AccountDTO useraccount = (AccountDTO) session
				.getAttribute("useraccountDTO");
		session.setAttribute("userpersonalInformation", persinfo);
		refreshPoolLists(session);
		prepareSessionForShowUser(useraccount,
				(AccountDTO) session.getAttribute("accountDO"));
		HistoryActionBean historyActionBean = new HistoryActionBean();
		ProductTransferService productTransferService = JndiUtil
				.lookup("ProductTransferBean");
		historyActionBean.refreshPoolListsTransferactivitiesOpen(session,
				productTransferService, useraccount);
		historyActionBean.refreshPoolListsTransferactivitiesOpenRecipient(
				session, productTransferService, useraccount);

		return new ForwardResolution("/velocity/AdminUI/adminshowuser.vm");
	}
	
	/**
	 * Some excluded Session Preparing for showing UserData,
	 *    like setting the personalInfomartion up, or loading al Producst for the sendign-option
	 * 
	 * @param useraccount
	 *        The User AccountDTO to work with
	 * @param admin
	 *        The Admin AccountDTO, to get acces to the backend methods
	 */

	private void prepareSessionForShowUser(AccountDTO useraccount,
			AccountDTO admin) {
		AccountBeanService service = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		PersonalInformationDTO thirdPartyPersonalInformationDTO = service
				.getPersonalInformationDTO(admin.getSystem(),
						useraccount.getUsername());
		session.setAttribute("useraccountDTO", useraccount);
		session.setAttribute("userpersonalInformation",
				thirdPartyPersonalInformationDTO);
		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		List<ProductDTO> helpproducts = productBeanService.listBySystemClass(admin,
				system);
		products=new ArrayList<ProductDTO>();
		for (ProductDTO product : helpproducts) {
			if (product.getCurrAmount() != product.getMaxAmount()) {
				products.add(product);
			}
		}
		helpproducts=null;
		getContext().getResponse().setContentType("text/html; charset=UTF-8");

	}
	
	
	
	/** Method for sending a new Passordlink to the user
	 * 
	 * @return
	 *        the Userview(=adminshowuser)
	 */
	public Resolution sendPW() {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO accountDO = (AccountDTO) session
				.getAttribute("useraccountDTO");
		accountBeanService.generateToken(accountDO);
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/adminshowuser"));
	}
    
	
	/**Method for deleting preservative Tickets off the user 
	 * 
	 * @return
	 *       The UserOverView (=adminshowuser)
	 * @throws Exception
	 */
	
	public Resolution deletepreservedtickets() throws Exception {
		getContext().getRequest().getSession().removeAttribute("adminerror");
		ProductTransferService productTransferService = JndiUtil
				.lookup("ProductTransferBean");
		HttpSession session = getContext().getRequest().getSession();
		long transferActivityId = Long.parseLong(getContext().getRequest()
				.getParameter("transferActivityId").toString());
		productTransferService.declinebyadmin(
				((AccountDTO) session.getAttribute("accountDO")),
				((AccountDTO) session.getAttribute("useraccountDTO")),
				transferActivityId);
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/adminshowuser"));
	}

	/**Method for deleting open Tickets off the user 
	 * 
	 * @return
	 *      Generated View for the TicketAmoun(=adminamount.vm)
	 * @throws Exception
	 */
	public Resolution deleteopentickets() throws Exception {
		getContext().getRequest().getSession().removeAttribute("adminerror");
		HttpSession session = getContext().getRequest().getSession();
		try {
			ProductDTO productDO = getProductDTO();
			@SuppressWarnings("unchecked")
			List<ProductPoolDTO> volatileProductPools = (List<ProductPoolDTO>) session
					.getAttribute("UserVolatileProductPools");
			ProductPoolDTO pool = null;
			for (ProductPoolDTO check : volatileProductPools) {
				if (check.getProductDTO().equals(productDO)) {
					pool = check;
				}

			}
			session.setAttribute("deletePool", pool);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ForwardResolution("/velocity/AdminUI/adminamount.vm");
	}
	
	
	/**Method for final deleting off open tickets. It's called by the adminAmount view,
	 * and deletes by the given Amount the Ticket or adjustets the amount
	 * 
	 * @return
	 *       The UserOverView (=adminshowuser)
	 * @throws Exception
	 */

	public Resolution deleteopenticketsfinally() throws Exception {
		String s = getDeleteAmount();
		int test = Integer.parseInt(s);
		HttpSession session = getContext().getRequest().getSession();
		ProductPoolDTO pool = (ProductPoolDTO) session
				.getAttribute("deletePool");
		int compare = Integer.parseInt(pool.getAmount());
		if (test > compare) {
			return new ForwardResolution("/velocity/AdminUI/adminamount.vm");
		}
		ProductTransferService productTransferBeanService = JndiUtil
				.lookup("ProductTransferBean");
		try {
			productTransferBeanService.deleteopentickets(
					((AccountDTO) session.getAttribute("accountDO")), pool,
					test);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/adminshowuser"));
	}

	/**These Method changes the state of the selectet Product to fixed.
	 * 
	 * @return
	 *       The UserOverView (=adminshowuser)
	 * @throws Exception
	 */
	public Resolution fix() throws Exception {
		getContext().getRequest().getSession().removeAttribute("adminerror");
		ProductPoolBeanService productPoolBeanService = JndiUtil
				.lookup("ProductPoolBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO accountDO = (AccountDTO) session
				.getAttribute("useraccountDTO");
		AccountDTO myaccountDO = (AccountDTO) session.getAttribute("accountDO");
		try {
			ProductDTO productDO = getProductDTO();
			try {
				productPoolBeanService.fixProductasAdmin(myaccountDO,
						accountDO, productDO);
				getPdf(accountDO, productDO);
				getContext().getRequest().setAttribute("message",
						"confirm.myproductFix");
			} catch (RuntimeException e) {
				getContext().getRequest().setAttribute("message",
						"inform.alreadyfixedproduct");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidParameterException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}

		return new RedirectResolution(
				generateLocaleLink("/dispatcher/adminshowuser"));
	}

	private Resolution getPdf(final AccountDTO accountDO,
			final ProductDTO productDO) throws IOException,
			InvalidParameterException, NamingException {
		return new Resolution() {

			@Override
			public void execute(HttpServletRequest request,
					HttpServletResponse response) throws Exception {
				HttpServletResponse res = getContext().getResponse();

				res.setHeader("Content-Disposition",
						"inline; attachment; filename=" + productDO.getUin()
								+ ".pdf");
				res.setContentType(APPLICATION_PDF);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				// byte[] pdf = printPdf(accountDO);

				// baos.write(pdf);
				res.setContentLength(baos.size());
				ServletOutputStream out = res.getOutputStream();
				baos.writeTo(out);
				out.flush();
				out.close();
			}
		};

	}


	/**
	 * Returns the ProduktDTO of the given productId and the System
	 * 
	 * @return ProduktDTO the Data transfer object of Product
	 * @throws UnsupportedEncodingException 
	 */
	private ProductDTO getProductDTO() throws UnsupportedEncodingException {
		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		HttpSession session = getContext().getRequest().getSession();
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		return productBeanService.get(system.getName(), getUtf8(productUin));
	}

	/**Deletes the User 
	 * 
	 * @return
	 *        AdminWelcomview (adminindex)
	 * @throws Exception
	 */
	public Resolution deleteUser() throws Exception {
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO useraccount = (AccountDTO) session
				.getAttribute("useraccountDTO");
		AccountDTO myaccount = (AccountDTO) session.getAttribute("accountDO");
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		accountBeanService.deletePwhashed(myaccount, useraccount);

		return new RedirectResolution(
				generateLocaleLink("/dispatcher/helloadmin"));

	}

	/** Redirect to the perosnalInforamtion edit Class
	 * 
	 * @return
	 *       adminedituser
	 */
	public Resolution renderEdit(){
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/adminedituser"));
	}

	
	/** Method for sending tickets to the user
	 * Actual validition "well-coded" or well-formed, but it works
	 * 
	 * @return
	 *       UserOverview (=adminshowuser)
	 * @throws Exception
	 */
	
	// Really ugly lookup of the selectet Ticket
	public Resolution sendTickets() throws Exception {
		getContext().getRequest().getSession().removeAttribute("adminerror");
		HttpSession session = getContext().getRequest().getSession();
		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		AccountDTO myAccount =(AccountDTO)session.getAttribute("accountDO");
		ProductDTO finalproduct=productBeanService.get(myAccount.getSystem().getName(), getUtf8(getProduct()));
		long anzahl = 0;
		int rest=finalproduct.getMaxAmount()-finalproduct.getCurrAmount();
		if (!(getAmount() == null)) {
			anzahl = Long.parseLong(getAmount());
		}
		if (anzahl < 1 || anzahl > 10||(rest-anzahl<0)) {
			prepareSessionForShowUser(getAccountDTO(), myAccount);
			getContext().getRequest().setAttribute("error", "e");
			return new ForwardResolution("/velocity/AdminUI/adminshowuser.vm");
		}
		ProductTransferService productTransferBeanService = JndiUtil
				.lookup("ProductTransferBean");
		productTransferBeanService.transferSystemProductToAccount(
				(AccountDTO) session.getAttribute("accountDO"), finalproduct,
				(AccountDTO) session.getAttribute("useraccountDTO"), anzahl);
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/adminshowuser"));
	}

	private AccountDTO getAccountDTO() {
		HttpSession session = getContext().getRequest().getSession();
		return (AccountDTO) session.getAttribute("useraccountDTO");
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {

		return new ForwardResolution("/velocity/AdminUI/adminuser.vm");
	}

	/**Method for refreshing off the ProductPoolLoists off the user
	 * 
	 * @param session
	 * @throws InvalidParameterException
	 */
	
	public void refreshPoolLists(HttpSession session)
			throws InvalidParameterException {
		ProductPoolBeanService productPoolBeanService = JndiUtil
				.lookup("ProductPoolBean");
		AccountDTO useraccountDO = (AccountDTO) session
				.getAttribute("useraccountDTO");

		try {
			refreshPoolListsVolatileProductPools(session,
					productPoolBeanService, useraccountDO);
			refreshPoolListsFixedProductPools(session, productPoolBeanService,
					useraccountDO);

		} catch (NullPointerException npe) {
			System.err
					.println("Either AccountDO or AccountAgentRemote EJB is NULL");
			System.err.println(npe);
		}
	}
	
	
	
	/**Method for refreshing off the VolatileProductPoolLoists off the user
	 * 
	 * @param session
	 * @throws InvalidParameterException
	 */
	

	private void refreshPoolListsVolatileProductPools(HttpSession session,
			ProductPoolBeanService productPoolBeanService,
			AccountDTO useraccountDO) {
		List<ProductPoolDTO> volatileProductPools = new ArrayList<ProductPoolDTO>();
		for (ProductPoolDTO productPoolDTO : productPoolBeanService
				.getProductPoolList("VOLATILE", useraccountDO.getUsername())) {
			if (!productPoolDTO.getAmount().equals("0")) {
				volatileProductPools.add(productPoolDTO);
			}
		}
		session.setAttribute("UserVolatileProductPools", volatileProductPools);
	}
	
	/**Method for refreshing off the FixedProductPoolLoists off the user
	 * 
	 * @param session
	 * @throws InvalidParameterException
	 */
	private void refreshPoolListsFixedProductPools(HttpSession session,
			ProductPoolBeanService productPoolBeanService,
			AccountDTO useraccountDO) {
		List<ProductPoolDTO> fixedProducts = null;
		fixedProducts = productPoolBeanService.getProductPoolList("FIXED",
				useraccountDO.getUsername());
		session.setAttribute("UserFixedProducts", fixedProducts);
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public void setLocalecode(String localecode) {
		this.localecode = localecode;
	}

	public String getLocalecode() {
		return localecode;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getProduct() {
		return product;
	}

	public String getPseudoUID() {
		return pseudoUID;
	}

	public void setPseudoUID(String s) {
		this.pseudoUID = s;
	}

	public String getDeleteAmount() {
		return deleteamount;
	}

	public void setDeleteAmount(String deleteamount) {
		this.deleteamount = deleteamount;
	}

	public String gettransferActivityId() {
		return this.transferActivityId;
	}

	public void settransferActivityId(String s) {
		this.transferActivityId = s;
	}

	public String getProductUin() {
		return productUin;
	}

	public void setProductUin(String productUin) {
		this.productUin = productUin;
	}

	public List<ProductDTO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDTO> products) {
		this.products = products;
	}
	
	
	private String getUtf8(String string) throws UnsupportedEncodingException {
		if (string != null
				&& (getContext().getRequest().getCharacterEncoding() == null || getContext()
						.getRequest().getCharacterEncoding().equals("UTF-8")))
			return new String(string.getBytes("8859_1"), "UTF8");
		else
			return string;
	}
	
	
	/**Before Test, if the user is an admin 
	 * 
	 * @throws IllegalAccessException
	 * 					If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException{
		setLocale((Locale) getContext().getRequest().getSession().getAttribute(LOCALE));
		AccountDTO myAccount = (AccountDTO) getContext().getRequest().getSession().getAttribute("accountDO");
		if (!myAccount.getRole().equals(RoleDTO.getSysAdmin(myAccount.getSystem().getId()))){
			throw new IllegalAccessException();
		}
		}
}
