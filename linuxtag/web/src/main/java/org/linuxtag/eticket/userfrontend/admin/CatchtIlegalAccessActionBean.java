package org.linuxtag.eticket.userfrontend.admin;


import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * 
 * @author dgille
 *
 *This class handles the helloadmin url,
 *a welcome screen for admins.
 */



@UrlBinding("/dispatcher/catchillegalacces")
public class CatchtIlegalAccessActionBean extends GenericLocale implements ActionBean,
		ValidationErrorHandler {
	private ActionBeanContext context;

	
	
	
	@DefaultHandler
	public Resolution showInterface() throws IOException {
		
		
		return new ForwardResolution("/velocity/catchillegalacess.vm");
	}
	
	
	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}


	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**Before Test, if the user is an admin 
	 * 
	 * @throws IllegalAccessException
	 * 					If the user is not an admin, this exception will be thrown
	 */

}