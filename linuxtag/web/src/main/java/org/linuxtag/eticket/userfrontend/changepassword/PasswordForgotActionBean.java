
package org.linuxtag.eticket.userfrontend.changepassword;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ScopedLocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * This class handles all work if a User don´t know this password 
 */
@UrlBinding("/dispatcher/passwordforgot")
public class PasswordForgotActionBean extends GenericLocale implements
		ValidationErrorHandler {

	private ActionBeanContext context;
	@Validate(on = "passwordforgot", required = true, mask = "[^@]+@.+\\.[^.]+")
	private String username;
    private String captchacode;
	/**
	 * This method shows the view to get a new password 
	 */
	@DefaultHandler
	public Resolution forgot() {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		return new ForwardResolution("/velocity/passwordforgot.vm");
	}

	/**
	 * This method gets a user by the username and the system. 
	 * Then we call the method to send a email with a URI to change the password
	 */
	public Resolution passwordforgot() {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();		
		ESystemDTO esystem = (ESystemDTO) session
				.getAttribute("systemName");
		AccountDTO accountDO;
		
		try {
			accountDO = accountBeanService.find(esystem, username);
			
			accountBeanService.generateToken(accountDO);
			getContext().getRequest().setAttribute("message",
			"confirm.forgot");
			return new ForwardResolution("/velocity/confirm.vm");
		} catch (Exception e) {
			e.printStackTrace();
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/passwordforgot", "usernameDoesNotExist");
			ValidationErrors errors = new ValidationErrors();
			errors.add("username", error);
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/passwordforgot.vm");
		}
		
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		return new ForwardResolution("/velocity/passwordforgot.vm");
	}

	@Override
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	@Override
	public ActionBeanContext getContext() {
		return context;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public String getCaptchacode() {
		return captchacode;
	}

	public void setCaptchacode(String captchacode) {
		this.captchacode = captchacode;
	}
}
