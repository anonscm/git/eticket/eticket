
package org.linuxtag.eticket.userfrontend.product;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.linuxtag.eticket.userfrontend.locale.GenericLocale;

@UrlBinding("/dispatcher/productdetail")
public class ProductDetailActionBean extends GenericLocale implements
		ActionBean {
	private ActionBeanContext context;

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	@DefaultHandler
	public Resolution showProductDetail() throws Exception {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		MyProductActionBean myProductActionBean = new MyProductActionBean();
		myProductActionBean.refreshPoolLists(getContext().getRequest()
				.getSession());		
		return new ForwardResolution("/velocity/productdetail.vm");
	}
}

