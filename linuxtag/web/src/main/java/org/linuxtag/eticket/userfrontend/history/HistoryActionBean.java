package org.linuxtag.eticket.userfrontend.history;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJBTransactionRolledbackException;
import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.ProductTransferService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.TransferHistoryDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * This class handles all work to show the History
 */
@UrlBinding("/dispatcher/history")
public class HistoryActionBean extends GenericLocale {
	
	private ActionBeanContext context;

	@Override
	public ActionBeanContext getContext() {
		return context;
	}

	@Override
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}
	
	/**
	 * This method shows History view 
	 */
	@DefaultHandler
	public Resolution showHistory(){
		HttpSession session = getContext().getRequest().getSession();
		ProductTransferService productTransferService = JndiUtil.lookup("ProductTransferBean");
		AccountDTO accountDO = (AccountDTO) session.getAttribute("accountDO");	
		refreshPoolLists(session, productTransferService, accountDO);
		return new ForwardResolution("/velocity/history.vm");
	}
	
	/** 
	 * This method calls all methods to update the TransferHistory Objects in the session 
	 * @param session
	 * @param productTransferService
	 * @param accountDO
	 */
	private void refreshPoolLists(HttpSession session, ProductTransferService productTransferService, AccountDTO accountDO){			
		refreshPoolListsTransferactivitiesAccepted(session, productTransferService, accountDO);
		refreshPoolListsTransferactivitiesAcceptedRecipient(session, productTransferService, accountDO);
		refreshPoolListsTransferactivitiesCanceled(session, productTransferService, accountDO);
		refreshPoolListsTransferactivitiesDeclined(session, productTransferService, accountDO);
		refreshPoolListsTransferactivitiesDeclinedRecipient(session, productTransferService, accountDO);
		refreshPoolListsTransferactivitiesOpen(session, productTransferService, accountDO);
		refreshPoolListsTransferactivitiesOpenRecipient(session, productTransferService, accountDO);
		refreshPoolListsTransferactivitiesDeletedbyAdmin(session, productTransferService, accountDO);
		refreshPoolListsTransferactivitiesDeletedbyAdminRecipient(session, productTransferService, accountDO);
	}

	/**
	 * Sets all open transfer activities in the session
	 * @param session
	 * @param productTransferService
	 * @param accountDO
	 */
	public void refreshPoolListsTransferactivitiesOpen(HttpSession session, ProductTransferService productTransferService, AccountDTO accountDO){
		List<TransferHistoryDTO> transferactivitiesOpen = null;
		transferactivitiesOpen = productTransferService
				.findTransferActivitiesSender(accountDO.getUsername(), "OPEN");
		session.setAttribute("TransferActivitiesOpen", transferactivitiesOpen);
	}
		
	/**
	 * Sets all deleted by admin transfer activities in the session
	 * @param session
	 * @param productTransferService
	 * @param accountDO
	 */
	private void refreshPoolListsTransferactivitiesDeletedbyAdmin(HttpSession session, ProductTransferService productTransferService, AccountDTO accountDO){
		List<TransferHistoryDTO> transferactivitiesOpen = null;
		transferactivitiesOpen = productTransferService
				.findTransferActivitiesSender(accountDO.getUsername(), "TAKENBYADMIN");
		session.setAttribute("TransferActivitiesDeletedByAdmin", transferactivitiesOpen);
		productLocalesFromActiveProductsToSession(transferactivitiesOpen, session );
	} 
	
	/**
	 * Sets all canceled transfer activities in the session
	 * @param session
	 * @param productTransferService
	 * @param accountDO
	 */
	private void refreshPoolListsTransferactivitiesCanceled(HttpSession session, ProductTransferService productTransferService, AccountDTO accountDO){
		List<TransferHistoryDTO> transferactivitiesCanceled = null;
		transferactivitiesCanceled = productTransferService
				.findTransferActivitiesSender(accountDO.getUsername(), "CANCELED");
		session.setAttribute("TransferActivitiesCanceled", transferactivitiesCanceled);	
		productLocalesFromActiveProductsToSession(transferactivitiesCanceled, session );
	}
	
	/**
	 * Sets all declined transfer activities in the session
	 * @param session
	 * @param productTransferService
	 * @param accountDO
	 */
	private void refreshPoolListsTransferactivitiesDeclined(HttpSession session, ProductTransferService productTransferService, AccountDTO accountDO){
		List<TransferHistoryDTO> transferactivitiesDeclined = null;
		transferactivitiesDeclined = productTransferService
				.findTransferActivitiesSender(accountDO.getUsername(), "DECLINED");
		session.setAttribute("TransferActivitiesDeclined", transferactivitiesDeclined);
		productLocalesFromActiveProductsToSession(transferactivitiesDeclined, session );
	}
	
	/**
	 * Sets all accepted transfer activities in the session
	 * @param session
	 * @param productTransferService
	 * @param accountDO
	 */
	private void refreshPoolListsTransferactivitiesAccepted(HttpSession session, ProductTransferService productTransferService, AccountDTO accountDO){
		List<TransferHistoryDTO> transferactivitiesAccepted = null;
		transferactivitiesAccepted = productTransferService
				.findTransferActivitiesSender(accountDO.getUsername(), "ACCEPTED");
		session.setAttribute("TransferActivitiesAccepted", transferactivitiesAccepted);	
		productLocalesFromActiveProductsToSession(transferactivitiesAccepted, session );
	}
	
	private void refreshPoolListsTransferactivitiesDeletedbyAdminRecipient(HttpSession session, ProductTransferService productTransferService, AccountDTO accountDO){
		List<TransferHistoryDTO> transferactivitiesOpen = null;
		transferactivitiesOpen = productTransferService
				.findTransferActivitiesRecipient(accountDO.getUsername(), "TAKENBYADMIN");
		session.setAttribute("TransferActivitiesTakenbyAdminRecipient", transferactivitiesOpen);
	}
	
	/**
	 * Sets all open if the user are the recipient transfer activities in the session
	 * @param session
	 * @param productTransferService
	 * @param accountDO
	 */
	public void refreshPoolListsTransferactivitiesOpenRecipient(HttpSession session, ProductTransferService productTransferService, AccountDTO accountDO){
		List<TransferHistoryDTO> transferactivitiesOpenRecipient = null;
		transferactivitiesOpenRecipient = productTransferService
				.findTransferActivitiesRecipient(accountDO.getUsername(), "OPEN");
		session.setAttribute("TransferActivitiesOpenRecipient", transferactivitiesOpenRecipient);
		productLocalesFromActiveProductsToSession(transferactivitiesOpenRecipient, session );
	} 
	
	/**
	 * Sets all accepted if the user are the recipient transfer activities in the session
	 * @param session
	 * @param productTransferService
	 * @param accountDO
	 */
	private void refreshPoolListsTransferactivitiesAcceptedRecipient(HttpSession session, ProductTransferService productTransferService, AccountDTO accountDO){
		List<TransferHistoryDTO> transferactivitiesAcceptedRecipient = null;
		transferactivitiesAcceptedRecipient = productTransferService
				.findTransferActivitiesRecipient(accountDO.getUsername(), "ACCEPTED");
		session.setAttribute("TransferActivitiesAcceptedRecipient", transferactivitiesAcceptedRecipient);
		productLocalesFromActiveProductsToSession(transferactivitiesAcceptedRecipient, session );
	}
	
	/**
	 * Sets all declined if the user are the recipient transfer activities in the session
	 * @param session
	 * @param productTransferService
	 * @param accountDO
	 */
	private void refreshPoolListsTransferactivitiesDeclinedRecipient(HttpSession session, ProductTransferService productTransferService, AccountDTO accountDO){
		List<TransferHistoryDTO> transferactivitiesDeclinedRecipient = null;
		transferactivitiesDeclinedRecipient = productTransferService
				.findTransferActivitiesRecipient(accountDO.getUsername(), "DECLINED");
		session.setAttribute("TransferActivitiesDeclinedRecipient", transferactivitiesDeclinedRecipient);
		productLocalesFromActiveProductsToSession(transferactivitiesDeclinedRecipient, session );
	}
	
	/**
	 * Save all ProductLocales from the given TransferHistorys in the session
	 * 
	 * @param shopProductDTOs
	 */
	private void productLocalesFromActiveProductsToSession(
			List<TransferHistoryDTO>  transferHistoryDTOs, HttpSession session) {
		ProductBeanService productBeanService = JndiUtil.lookup("ProductBean");
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		locale = (Locale) session.getAttribute("locale");
		Map<String, ProductLocaleDTO> productLocale =  (Map<String, ProductLocaleDTO>) session.getAttribute("ProductLocaleMap");
		if(productLocale == null){
			productLocale = new HashMap<String, ProductLocaleDTO>();
		}

		for (TransferHistoryDTO thDTO : transferHistoryDTOs) {
			ProductLocaleDTO productLocaleDTO = null;
			try {
				productLocaleDTO = productBeanService.getProductLocale(
						locale.toString(), thDTO.getProductDTO().getUin(),
						system.getId());
			} catch (EJBTransactionRolledbackException e) {
				// TODO Exception Behandlung - diese tritt auf wenn keine
				// passendes ProductLocale existiert. soll ich dies weiter
				// behandlen?
			}

			if (productLocaleDTO != null) {
				// Add corresponding productLocaleDTO
				productLocale.put(thDTO.getProductDTO().getUin(),
						productLocaleDTO);
			}
		}

		session.setAttribute("ProductLocaleMap", productLocale);
	}
}
