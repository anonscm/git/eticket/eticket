package org.linuxtag.eticket.userfrontend.login;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ScopedLocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * This class handles the work with the login
 */
@UrlBinding("/dispatcher/login")
public class LoginActionBean extends GenericLocale implements
		ValidationErrorHandler {

	private ActionBeanContext context;
	@Validate(on = "login", required = true, trim = true, minlength = 1, maxlength = 100)
	private String loginUsername;
	@Validate(on = "login", required = true, trim = true, minlength = 1, maxlength = 100)
	private String loginPassword;

	private final String PRODUCT_DETAIL = "/dispatcher/productdetail";
	private final String PERSONAL_INFORMATION = "/dispatcher/editpersonaldata";

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	/**
	 * This method shows the login view
	 */
	@DefaultHandler
	public Resolution showLogin() {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		return new ForwardResolution("/velocity/login.vm");
	}

	public String getLoginUsername() {
		return loginUsername;
	}

	public void setLoginUsername(String loginUsername) {
		this.loginUsername = loginUsername;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	/**
	 * This method check if the user has typing the right password and email. By
	 * the right password and email its call the method loginSuccess().
	 */
	public Resolution login() {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		AccountDTO accountDTO = null;
		HttpSession session = getContext().getRequest().getSession();
		ESystemDTO ets = (ESystemDTO) session.getAttribute("systemName");
		if (ets == null)
			throw new IllegalArgumentException();
		if (getLoginUsername() != null) {
			try {
				accountDTO = AccountDTO.init(ets,
						getLoginUsername(), getLoginPassword(),
						RoleDTO.getUser(ets.getId()), null);
			} catch (Exception e) {
				// ignore that, the real error handling is below
			}
		}

		try {
			accountDTO = accountBeanService.validatePassword(accountDTO);
			getContext().getResponse().setContentType(
					"text/html; charset=UTF-8");

			session.setAttribute("accountDO", accountDTO);
			return new RedirectResolution(
					"/dispatcher/login?loginSuccess=loginSuccess");
		} catch (IllegalAccessException e) {

			session.removeAttribute("accountDO");
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/login", "incorrectUsernamePasswordCombination");
			ValidationErrors errors = new ValidationErrors();
			errors.add("loginPassword", error);
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/index.vm");
		}
	}

	/**
	 * This method check if the personal information are set if not it call the
	 * view to insert the personal information
	 */
	public Resolution loginSuccess() {
		HttpSession session = getContext().getRequest().getSession();
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		AccountDTO accountDTO = (AccountDTO) session.getAttribute("accountDO");

		PersonalInformationDTO informationDO = accountBeanService
				.getPersonalInformationDTO(accountDTO.getSystem(),
						accountDTO.getUsername());

		if (informationDO == null) {
			return new RedirectResolution(
					generateLocaleLink(PERSONAL_INFORMATION));
		} else {
			session.setAttribute("accountDO", accountDTO);
			locale = new Locale(informationDO.getLocaleDTO().getCode());
			session.setAttribute(LOCALE, locale);

			if (accountDTO.getRole().getRole() == "user") {
				return new RedirectResolution(
						generateLocaleLink(PRODUCT_DETAIL));
			} else {
				return new RedirectResolution(
						generateLocaleLink("/dispatcher/helloadmin"));
			}

		}
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		return new ForwardResolution("/velocity/index.vm");
	}
}
