package org.linuxtag.eticket.userfrontend.login;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ScopedLocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.system.ConfigurationParameterBeanService;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;
import org.openid4java.OpenIDException;
import org.openid4java.consumer.ConsumerException;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.consumer.InMemoryConsumerAssociationStore;
import org.openid4java.consumer.InMemoryNonceVerifier;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.DiscoveryException;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.AuthSuccess;
import org.openid4java.message.MessageException;
import org.openid4java.message.MessageExtension;
import org.openid4java.message.ParameterList;
import org.openid4java.message.sreg.SRegMessage;
import org.openid4java.message.sreg.SRegRequest;
import org.openid4java.message.sreg.SRegResponse;

/**
 * This class handles the work to login by OpenId
 * 
 * @author mjohnk
 * 
 */
@UrlBinding(value = "/dispatcher/loginOpenId")
public class LoginOpenIdActionBean extends GenericLocale implements
		ValidationErrorHandler {

	private final String PRODUCT_DETAIL = "/dispatcher/productdetail";
	private final String PERSONAL_INFORMATION = "/dispatcher/editpersonaldata";
	private ActionBeanContext context;
	@Validate(on = "login", required = true, trim = true, minlength = 1, maxlength = 100)
	private String openid;

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	/**
	 * This method shows the view to login by Openid
	 */
	@DefaultHandler
	public Resolution showLogin() {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		return new ForwardResolution("/velocity/loginOpenId.vm");
	}

	/**
	 * This method send the user to the OpenId provider, that he can login there
	 */
	public Resolution loginOpenId() throws IOException {
		Object o = getContext().getRequest().getAttribute("consumermanager");
		if (o == null) {
			setConsumerManager();
		}
		ConsumerManager manager = (ConsumerManager) getContext().getRequest()
				.getAttribute("consumermanager");

		try {
			String returnToUrl = getReturnURL();

			DiscoveryInformation discovered = setDiscoveryInformation();

			final AuthRequest authReq = manager.authenticate(discovered,
					returnToUrl);
			addSimpleRegistrationToAuthRequest(authReq);

			if (!discovered.isVersion2()) {
				getContext().getResponse().sendRedirect(
						authReq.getDestinationUrl(true));
			} else {

				final String params = buildParamUrlAndSetAttributes(authReq);

				return new Resolution() {

					@Override
					public void execute(HttpServletRequest request,
							HttpServletResponse response) throws Exception {
						response.sendRedirect(authReq.getOPEndpoint() + ""
								+ params);
					}
				};
			}

		} catch (OpenIDException e) {
			return showError();
		}
		return null;
	}

	/**
	 * This method is for error handling it is called when here is something
	 * wrong with the OpenId
	 */
	private Resolution showError() {
		ValidationError error = new ScopedLocalizableError(
				"/dispatcher/loginOpenId", "openid.valueNotPresent");
		ValidationErrors errors = new ValidationErrors();
		errors.add("openid", error);
		getContext().setValidationErrors(errors);
		return new ForwardResolution("/velocity/index.vm");
	}

	/**
	 * This method set the ConsumerManager to the session
	 */
	private void setConsumerManager() {
		HttpSession session = getContext().getRequest().getSession();
		ConsumerManager newmgr = new ConsumerManager();
		newmgr.setAssociations(new InMemoryConsumerAssociationStore());
		newmgr.setNonceVerifier(new InMemoryNonceVerifier(5000));
		getContext().getRequest().setAttribute("consumermanager", newmgr);
		session.setAttribute("consumermanager", newmgr);
	}

	/**
	 * This method builds the return URL. To this URI the OpenId provider sends
	 * the user back if he typing the right password.
	 */
	private String getReturnURL() {
		ConfigurationParameterBeanService configurationParameterBeanService = JndiUtil
				.lookup("ConfigurationParameterBean");
		HttpSession session = getContext().getRequest().getSession();
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		String returnToUrl = configurationParameterBeanService.find(system,
				CPKey.SYSTEM_DOMAIN.toString())
				+ "/dispatcher/loginOpenId?returnUrl=returnUrl";
		return returnToUrl;
	}

	/**
	 * This method set the DiscoveryInformation to the session
	 * @return DiscoveryInformation
	 * @throws DiscoveryException
	 */
	private DiscoveryInformation setDiscoveryInformation()
			throws DiscoveryException {
		HttpSession session = getContext().getRequest().getSession();
		ConsumerManager manager = (ConsumerManager) session
				.getAttribute("consumermanager");

		String openid = getContext().getRequest().getParameter("openid");
		List discoveries = manager.discover(openid);

		DiscoveryInformation discovered = manager.associate(discoveries);

		session.setAttribute("openid-disco", discovered);
		return discovered;
	}

	/**
	 * This method returns the params, that are add to the call to the OpenId provider
	 * @param authReq
	 * @return
	 */
	private String buildParamUrlAndSetAttributes(AuthRequest authReq) {
		HttpSession session = getContext().getRequest().getSession();
		Map pm = authReq.getParameterMap();
		Set<String> keys = pm.keySet();
		String params = "?";

		for (String key : keys) {
			params = params + "" + key + "=" + pm.get(key) + "&";
		}

		session.setAttribute("keys", keys);
		session.setAttribute("pm", pm);
		session.setAttribute("authReq", authReq);
		return params;
	}

	/**
	 * This method add the attributes which we want from the OpenId provider about the user
	 * @param authReq
	 * @throws MessageException
	 */
	private void addSimpleRegistrationToAuthRequest(AuthRequest authReq)
			throws MessageException {
		SRegRequest sregReq = SRegRequest.createFetchRequest();

		String[] attributes = { "nickname", "email", "fullname", "dob",
				"gender", "postcode", "country", "language", "timezone" };
		for (int i = 0, l = attributes.length; i < l; i++) {
			String attribute = attributes[i];
			sregReq.addAttribute(attribute, false);
		}

		if (!sregReq.getAttributes().isEmpty()) {
			authReq.addExtension(sregReq);
		}
	}

	/**
	 * This method is called from the OpenId Provider after the succeed Login. 
	 * It verify the Response. 
	 */
	@HandlesEvent(value = "returnUrl")
	public Resolution verifyResponse() {
		HttpSession session = getContext().getRequest().getSession();
		ConsumerManager manager = (ConsumerManager) session
				.getAttribute("consumermanager");
		try {
			ParameterList responselist = new ParameterList(getContext()
					.getRequest().getParameterMap());

			DiscoveryInformation discovered = (DiscoveryInformation) session
					.getAttribute("openid-disco");

			StringBuffer receivingURL = getContext().getRequest()
					.getRequestURL();
			String queryString = getContext().getRequest().getQueryString();
			if (queryString != null && queryString.length() > 0)
				receivingURL.append("?").append(
						getContext().getRequest().getQueryString());

			VerificationResult verification = manager.verify(
					receivingURL.toString(), responselist, discovered);

			Identifier verified = verification.getVerifiedId();
			if (verified != null) {
				AuthSuccess authSuccess = (AuthSuccess) verification
						.getAuthResponse();
				getParameter(authSuccess);
				session.setAttribute("openid", authSuccess.getIdentity());
				session.setAttribute("openid-claimed", authSuccess.getClaimed());
			} else {
				// Failed to login!
			}
		} catch (OpenIDException e) {
			session.removeAttribute("accountDO");
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/loginOpenId", "openid.valueNotPresent");
			ValidationErrors errors = new ValidationErrors();
			errors.add("openid", error);
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/index.vm");
		}
		return getAccountFromOpenId();
	}

	/**
	 * This method get the Parameter from the given AuthSuccess Object 
	 * @param authSuccess
	 * @throws MessageException
	 */
	private void getParameter(AuthSuccess authSuccess) throws MessageException {
		HttpSession session = getContext().getRequest().getSession();
		if (authSuccess.hasExtension(SRegMessage.OPENID_NS_SREG)) {
			MessageExtension messageExtension = authSuccess
					.getExtension(SRegMessage.OPENID_NS_SREG);
			if (messageExtension instanceof SRegResponse) {
				SRegResponse sRegResponse = (SRegResponse) messageExtension;
				for (Iterator i = sRegResponse.getAttributeNames().iterator(); i
						.hasNext();) {
					String name = (String) i.next();
					String value = sRegResponse.getParameterValue(name);
					session.setAttribute(name, value);
				}
			}
		}
	}

	/**
	 * This method try to get a fit account to the OpenId. 
	 * If there is no it show the view to insert a email so that the user can  verify his email.
	 * If there is a account it checks if the account is activated, if the account is activated
	 * the user is logged in. If the account is not activated it show a view with a error 
	 */
	private Resolution getAccountFromOpenId() {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		String openId = session.getAttribute("openid").toString();
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		AccountDTO accountDTO;
		try {
			accountDTO = accountBeanService.findWithOpenId(system, openId);
		} catch (Exception e) {
			accountDTO = null;
		}

		if (accountDTO == null) {
			return new ForwardResolution("/velocity/setEmail.vm");
		} else {
			if (accountBeanService
					.isActivated(system, accountDTO.getUsername())) {
				return loginSuccess(accountDTO, session);
			}
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/loginOpenId.openid", "notactivated");
			ValidationErrors errors = new ValidationErrors();
			errors.add("openid", error);
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/index.vm");
		}
	}

	/**
	 * 
	 * @param accountDTO
	 * @param session
	 * @return
	 */
	public Resolution loginSuccess(AccountDTO accountDTO, HttpSession session) {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		PersonalInformationDTO informationDTO = accountBeanService
				.getPersonalInformationDTO(accountDTO.getSystem(),
						accountDTO.getUsername());
		session.setAttribute("accountDO", accountDTO);
		if (checkPersonalInformation(informationDTO)) {
			locale = new Locale(informationDTO.getLocaleDTO().getCode());
			session.setAttribute(LOCALE, locale);
			return new RedirectResolution(generateLocaleLink(PRODUCT_DETAIL));
		} else {
			session.setAttribute("state", "nopersonalinfo");
			return new RedirectResolution(
					generateLocaleLink(PERSONAL_INFORMATION));
		}
	}

	private boolean checkPersonalInformation(
			PersonalInformationDTO informationDTO) {
		if (informationDTO == null) {
			return false;
		} else if (informationDTO.getSalutation() == null
				|| (!informationDTO.getSalutation().equals("m") && !informationDTO
						.getSalutation().equals("f"))) {
			return false;
		} else if (informationDTO.getFirstname() == null
				|| informationDTO.getFirstname().trim().equals("")) {
			return false;
		} else if (informationDTO.getLastname() == null
				|| informationDTO.getLastname().trim().equals("")) {
			return false;
		} else if (informationDTO.getAddress() == null
				|| informationDTO.getAddress().trim().equals("")) {
			return false;
		} else if (informationDTO.getZipcode() == null
				|| informationDTO.getZipcode().trim().equals("")) {
			return false;
		} else if (informationDTO.getCity() == null
				|| informationDTO.getCity().trim().equals("")) {
			return false;
		}
		return true;
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		return new ForwardResolution("/velocity/index.vm");
	}
}
