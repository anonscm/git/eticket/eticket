package org.linuxtag.eticket.filter;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.ExecutionContext;
import net.sourceforge.stripes.controller.Interceptor;
import net.sourceforge.stripes.controller.Intercepts;
import net.sourceforge.stripes.controller.LifecycleStage;

@Intercepts({ LifecycleStage.ResolutionExecution })
public class CheckAccount implements Interceptor{
	
	@Override
	public Resolution intercept(ExecutionContext context)
			throws Exception {
		
		HttpSession session = context.getActionBeanContext().getRequest().getSession();

		if (!(context.getActionBean().getContext().getEventName().equals("redirectToErrorPage")|| 
				context.getActionBean().getContext().getEventName().equals("register")||
				context.getActionBean().getContext().getEventName().equals("showWelcome")|| 
				context.getActionBean().getContext().getEventName().equals("doLogout") || 
				context.getActionBean().getContext().getEventName().equals("login")|| 
				context.getActionBean().getContext().getEventName().equals("passwordforgot")|| 
				context.getActionBean().getContext().getEventName().equals("forgot")|| 
				context.getActionBean().getContext().getEventName().equals("setPassword")|| 
				context.getActionBean().getContext().getEventName().equals("activateUser"))) {
			if (session.getAttribute("accountDO") == null) {
				context.setResolution(new RedirectResolution("/dispatcher/"));
			}
		
		}
		return context.proceed();
	}
}
