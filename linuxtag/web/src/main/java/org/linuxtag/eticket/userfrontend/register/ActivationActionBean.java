package org.linuxtag.eticket.userfrontend.register;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ScopedLocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

@UrlBinding("/dispatcher/activate")
public class ActivationActionBean extends GenericLocale implements
		ValidationErrorHandler {

	private ActionBeanContext context;
	@Validate(on = "activate", required = true, trim = true, minlength = 8, maxlength = 100)
	private String newPassword1;
	@Validate(on = "activate", required = true, trim = true, minlength = 8, maxlength = 100, expression = "newPassword2==newPassword1")
	private String newPassword2;

	@DefaultHandler
	public Resolution setPassword() {
		String token = context.getRequest().getParameter("token");
		String clientname = context.getRequest().getParameter("clientname");
		String system = context.getRequest().getParameter("system");
		String platform = context.getRequest().getParameter("platform");
		HttpSession session = getContext().getRequest().getSession();
		session.setAttribute("Token", token);
		session.setAttribute("Platform", platform);
		session.setAttribute("System", system);
		session.setAttribute("Clientname", clientname);
		return new ForwardResolution("/velocity/activationSetPassword.vm");
	}

	public Resolution activateUser() {
		Pattern pattern;
		Matcher matcher;
		pattern = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[_@#!§$%]).{8,})");
		matcher = pattern.matcher(getNewPassword1());
		if(matcher.matches()){
			getContext().getRequest().setAttribute("messagePattern",
					"/dispatcher/passwordchange.password.incorrectPassword");
			
			return new ForwardResolution("/velocity/activationSetPassword.vm");
		}
			
		try {
			HttpSession session = getContext().getRequest().getSession();

			AccountBeanService accountBeanService = JndiUtil
					.lookup("AccountBean");
			ESystemDTO eSystemDTO = (ESystemDTO) session
					.getAttribute("systemName");
			String username = session.getAttribute("Clientname").toString();
			String token = session.getAttribute("Token").toString();
			AccountDTO accountDTO = accountBeanService.find(eSystemDTO,
					username);
			AccountDTO aDto = AccountDTO.hashPasswordAndGetAccount(
					accountDTO.getSystem(), accountDTO.getUsername(),
					newPassword1,

					RoleDTO.getUser(accountDTO.getSystem().getId()),
					accountDTO.getOpenId());
			accountBeanService.activate(aDto, token);
			session.setAttribute("accountDO", accountDTO);
			return new RedirectResolution("/dispatcher/editpersonaldata");

		} catch (Exception e) {		
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/passwordchange", "hashCode.incorrectHash");
			ValidationErrors errors = new ValidationErrors();
			errors.add("hashCode", error);
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/activationSetPassword.vm");
		}

	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public String getNewPassword1() {
		return newPassword1;
	}

	public void setNewPassword1(String newPassword1) {
		this.newPassword1 = newPassword1;
	}

	public String getNewPassword2() {
		return newPassword2;
	}

	public void setNewPassword2(String newPassword2) {
		this.newPassword2 = newPassword2;
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {		
		return new ForwardResolution("/velocity/activationSetPassword.vm");
	}

}
