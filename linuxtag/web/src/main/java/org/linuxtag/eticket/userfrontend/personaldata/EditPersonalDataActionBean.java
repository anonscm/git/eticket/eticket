package org.linuxtag.eticket.userfrontend.personaldata;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.product.ProductPoolBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

@UrlBinding("/dispatcher/editpersonaldata")
public class EditPersonalDataActionBean extends GenericLocale implements
		ValidationErrorHandler {
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 1)
	private String salutation;
	@Validate(on = "savePersonalData", required = false, trim = true, maxlength = 100)
	private String title;
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String firstname;
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String lastname;
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String address;
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String zipcode;
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String city;
	@Validate(on = "savePersonalData", required = false, trim = true, maxlength = 100)
	private String username;
	@Validate(on = "savePersonalData", required = true, trim = true, maxlength = 100)
	private String country;
	private String localecode;

	private String orga;

	private ActionBeanContext context;

	private PersonalInformationDTO personalInformationDTO;

	@DefaultHandler
	public Resolution showPData() throws Exception {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO accountDTO = getAccountDTO();
		locale = (Locale) session.getAttribute("locale");
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		personalInformationDTO = accountBeanService.getPersonalInformationDTO(
				accountDTO.getSystem(), accountDTO.getUsername());
		session.setAttribute("personalInformation", personalInformationDTO);
		return new ForwardResolution(
				generateLocaleLink("/velocity/editpersonaldata.vm"));
	}

	public Resolution savePersonalData() throws Exception {
		ProductPoolBeanService productPoolBeanService = JndiUtil
				.lookup("ProductPoolBean");
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO accountDTO = getAccountDTO();

		List<ProductPoolDTO> fixedProducts = productPoolBeanService
				.getProductPoolList("FIXED", accountDTO.getUsername());

		if (fixedProducts.size() > 0) {
			getContext().getRequest().setAttribute("message",
					"confirm.editpersonaldata.fixedProduct");
			return new ForwardResolution("/velocity/editpersonaldata.vm");
		} else {
			ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
			if (getLocalecode() == null)
				setLocalecode(system.getDefaultLocale().getCode());

			LocaleDTO localeDO = setLocale(system);

			PersonalInformationDTO personalInformationDTO = initPersonalInformation(
					accountDTO, localeDO);
			accountBeanService.UpdatePersonalInformation(accountDTO,
					personalInformationDTO);
			session.removeAttribute("state");
			getContext().getResponse().setContentType(
					"text/html; charset=UTF-8");
			getContext().getRequest().setAttribute("message",
					"confirm.editpersonaldata");

			locale = new Locale(getLocalecode());
			session.setAttribute("locale", locale);
		}
		
		return new RedirectResolution(generateLocaleLink("/dispatcher/"));
	}

	public Resolution abort() {
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/showpersonaldata"));
	}

	private LocaleDTO setLocale(ESystemDTO system) {
		LocaleDTO localeDO = null;
		if (system.getDefaultLocale().getCode().equals(getLocalecode())) {
			localeDO = system.getDefaultLocale();
		} else {
			for (LocaleDTO locale : system.getSupportedLocales()) {
				if (locale.getCode().equals(getLocalecode())) {
					localeDO = locale;
					break;
				}
			}
		}
		return localeDO;
	}

	private PersonalInformationDTO initPersonalInformation(
			AccountDTO accountDTO, LocaleDTO localeDO)
			throws UnsupportedEncodingException {
		return PersonalInformationDTO.createNewPersonalInformationDTO(
				getUtf8(getSalutation()), getUtf8(getTitle()),
				getUtf8(getFirstname()), getUtf8(getLastname()),
				getUtf8(getAddress()), getUtf8(getZipcode()),
				getUtf8(getCity()), getUtf8(getCountry()), localeDO,
				getUtf8(getOrga()), getUtf8(getEmail()));
	}

	private AccountDTO getAccountDTO() {
		HttpSession session = getContext().getRequest().getSession();
		return (AccountDTO) session.getAttribute("accountDO");
	}

	private PersonalInformationDTO getPeronalInformationDTO() {
		HttpSession session = getContext().getRequest().getSession();

		return (PersonalInformationDTO) session
				.getAttribute("personalInformation");
	}

	private String getEmail() {
		String email = null;
		if (getPeronalInformationDTO() == null) {
			email = getAccountDTO().getUsername();
		} else {
			email = getPeronalInformationDTO().getEmail();
		}
		return email;

	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		HttpSession session = getContext().getRequest().getSession();
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		session.setAttribute("personalInformation",
				initPersonalInformation(getAccountDTO(), setLocale(system)));
		return new ForwardResolution("/velocity/editpersonaldata.vm");
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation)
			throws UnsupportedEncodingException {
		this.salutation = salutation;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) throws UnsupportedEncodingException {
		this.title = title;
	}

	private String getUtf8(String string) throws UnsupportedEncodingException {
		if (string != null
				&& (getContext().getRequest().getCharacterEncoding() == null || getContext()
						.getRequest().getCharacterEncoding().equals("UTF-8")))
			return new String(string.getBytes("8859_1"), "UTF8");
		else
			return string;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname)
			throws UnsupportedEncodingException {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname)
			throws UnsupportedEncodingException {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) throws UnsupportedEncodingException {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) throws UnsupportedEncodingException {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) throws UnsupportedEncodingException {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) throws UnsupportedEncodingException {
		this.country = country;
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public void setLocalecode(String localecode) {
		this.localecode = localecode;
	}

	public String getLocalecode() {
		return localecode;
	}

	public void setOrga(String orga) throws UnsupportedEncodingException {
		this.orga = orga;
	}

	public String getOrga() {
		return orga;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username)
			throws UnsupportedEncodingException {
		this.username = username;
	}
}
