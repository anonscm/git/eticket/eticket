package org.linuxtag.eticket.userfrontend.personaldata;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

@UrlBinding("/dispatcher/showpersonaldata")
public class ShowPersonalDataActionBean extends GenericLocale {
	private ActionBeanContext context;
	private String salutation;
	private String title;
	private String firstname;
	private String lastname;
	private String address;
	private String zipcode;
	private String city;
	private String country;
	private String localecode;
	private String orga;

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	@DefaultHandler
	public Resolution showPersonalData() throws Exception {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO accountDTO = getAccountDTO();
		locale = (Locale) session.getAttribute("locale");
		session.setAttribute("locale", locale);
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		PersonalInformationDTO personalInformationDTO = accountBeanService
				.getPersonalInformationDTO(accountDTO.getSystem(),
						accountDTO.getUsername());
		session.setAttribute("personalInformation", personalInformationDTO);
		return new ForwardResolution(
				generateLocaleLink("/velocity/showpersonaldata.vm"));
	}

	private AccountDTO getAccountDTO() {
		HttpSession session = getContext().getRequest().getSession();
		return (AccountDTO) session.getAttribute("accountDO");
	}

	public Resolution renderEdit() {
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/editpersonaldata"));
	}

	public String getSalutation() {
		return salutation;
	}

	public String getTitle() {
		return title;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getAddress() {
		return address;
	}

	public String getZipcode() {
		return zipcode;
	}
}