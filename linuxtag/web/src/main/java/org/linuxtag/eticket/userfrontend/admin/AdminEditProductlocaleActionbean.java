package org.linuxtag.eticket.userfrontend.admin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;
import org.evolvis.eticket.util.CurrencyCode;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * Creates a new Productlocale, with an existing Template by an choosen
 * supported locale
 * 
 * @author dgille
 * 
 */

@UrlBinding("/dispatcher/admineditproductlocale")
public class AdminEditProductlocaleActionbean extends GenericLocale implements
		ActionBean, ValidationErrorHandler {
	private ActionBeanContext context;
	@Validate(on = "editProductLocale", required = true, trim = true)
	private String currency;
	private String template;
	@Validate(on = "editProductLocale", required = true, trim = true)
	private String description;
	@Validate(on = "editProductLocale", required = true, trim = true)
	private String salesTax;
	@Validate(on = "editProductLocale", required = true, trim = true)
	private String price;
	@Validate(on = "editProductLocale", required = true, trim = true)
	private String name;

	private String plocale;

	/**
	 * Generates the Form and gets all necessary Data
	 * 
	 * @return admincreateproductlocale.vm
	 * @throws IOException
	 */
	@DefaultHandler
	public Resolution showInterface() throws IOException {

		long plocaleId = Long.parseLong(getContext().getRequest()
				.getParameter("plocale").toString());
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO myAccount = (AccountDTO) session.getAttribute("accountDO");
		ProductBeanService productBean = JndiUtil.lookup("ProductBean");
		List<TemplatesDTO> templates = productBean.getAllTemplates(myAccount);
		session.setAttribute("templates", templates);
		ProductLocaleDTO plocale = productBean.getProductLocalebyId(myAccount,
				plocaleId);
		session.setAttribute("plocale", plocale);
		List<String>currencies=CurrencyCode.getAvailableCurrencieCodes();
		
		session.setAttribute("currencies", currencies);

		return new ForwardResolution("/velocity/AdminUI/admineditproductlocale.vm");
	}

	/**
	 * Responds to the form and creates finally the productlocae
	 * 
	 * @return Product Statisitcs overivew (adminindex/action)
	 */

	public Resolution editProductLocale() {
		ProductLocaleDTO productlocale = null;
		HttpSession session = getContext().getRequest().getSession();
		ProductLocaleDTO plocale= (ProductLocaleDTO) session.getAttribute("plocale");
		try {
			productlocale = ProductLocaleDTO.createProductLocaleDTOFromExsistingProductLocaleEntity(plocale.getId(),
					plocale.getLocale(), getCurrency(), getUtf8(getName()),
					getUtf8(getDescription()), getTemplatesDTO(),
					Double.parseDouble(getUtf8((getPrice()))),
					Double.parseDouble(getUtf8((getSalesTax()))));
		} catch (NumberFormatException e) {

			e.printStackTrace();
			productlocale = null;
		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
			productlocale = null;
		} catch (Exception e) {
			productlocale = null;
		}
		if (productlocale == null || productlocale.getCurrencyCode() == null) {
			getContext().getRequest().setAttribute("message", "error.baddata");
			return new ForwardResolution("/velocity/AdminUI/admineditproductlocale.vm");
		}
		ProductBeanService productBean = JndiUtil.lookup("ProductBean");
		AccountDTO myAccount = (AccountDTO) getContext().getRequest()
				.getSession().getAttribute("accountDO");
		productBean.updateProductlocale(myAccount, productlocale);
		session.removeAttribute("plocale");
		session.removeAttribute("templates");
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/adminlistproductlocales"));
	}



	private TemplatesDTO getTemplatesDTO() {
		ProductBeanService productBean = JndiUtil.lookup("ProductBean");

		return productBean.getTemplate(getTemplate());
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}


	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		// TODO Auto-generated method stub
		return new ForwardResolution("/velocity/AdminUI/admineditproductlocale.vm");
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSalesTax() {
		return salesTax;
	}

	public void setSalesTax(String salesTax) {
		this.salesTax = salesTax;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlocale() {
		return plocale;
	}

	public void setPlocale(String plocale) {
		this.plocale = plocale;
	}

	private String getUtf8(String string) throws UnsupportedEncodingException {
		if (string != null
				&& (getContext().getRequest().getCharacterEncoding() == null || getContext()
						.getRequest().getCharacterEncoding().equals("UTF-8")))
			return new String(string.getBytes("8859_1"), "UTF8");
		else
			return string;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}
	
	/**Before Test, if the user is an admin 
	 * 
	 * @throws IllegalAccessException
	 * 					If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException{
		setLocale((Locale) getContext().getRequest().getSession().getAttribute(LOCALE));
		AccountDTO myAccount = (AccountDTO) getContext().getRequest().getSession().getAttribute("accountDO");
		if (!myAccount.getRole().equals(RoleDTO.getSysAdmin(myAccount.getSystem().getId()))){
			throw new IllegalAccessException();
		}
		
	}

}