package org.linuxtag.eticket.userfrontend.register;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ScopedLocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

@UrlBinding("/dispatcher/register")
public class RegisterActionBean extends GenericLocale implements ActionBean,
		ValidationErrorHandler {

	@Validate(on = "register", required = true, trim = true, minlength = 1, maxlength = 254, mask = "[^@]+@.+\\.[^.]+")
	private String registerUsername;
	private ActionBeanContext context;

	@DefaultHandler
	public Resolution showRegister() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		return new ForwardResolution("/velocity/index.vm");
	}

	public Resolution register() throws IOException {
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		HttpSession session = getContext().getRequest().getSession();
		
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
				
		AccountDTO accountDTO = AccountDTO.init(system, registerUsername, UUID
				.randomUUID().toString(), RoleDTO.getUser(0), null);
		try {
			accountBeanService.createUser(accountDTO, "en");
			
			getContext().getRequest().setAttribute("message",
					"confirm.register");
			return new ForwardResolution("/velocity/confirm.vm");

		} catch (Exception e) {
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/register", "usernameAlreadyExists");
			ValidationErrors errors = new ValidationErrors();
			errors.add("registerUsername", error);
			getContext().setValidationErrors(errors);
		}
		return showRegister();
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public String getRegisterUsername() {
		return registerUsername;
	}

	public void setRegisterUsername(String registerUsername) {
		this.registerUsername = registerUsername;
	}

	public ActionBeanContext getContext() {
		return context;
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		return new ForwardResolution("/velocity/index.vm");
	}	
}
