package org.linuxtag.eticket.userfrontend.admin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.output.OutputException;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.FileBean;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.apache.commons.io.IOUtils;
import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.pdf.PrintProductPdfBeanService;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.ProductPoolBeanService;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.model.entity.PdfField;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

@UrlBinding("/dispatcher/admincreateproducttemplate")
public class AdminCreateProductTemplateActionBean extends GenericLocale
		implements ActionBean, ValidationErrorHandler {
	private FileBean attachment;
	private ActionBeanContext context;
	private static final String APPLICATION_PDF = "application/pdf";
	@Validate(on = "saveProductTemplate", required = true)
	private String productTemplateDescription;
	@Validate(on = "saveProductTemplate", required = true)
	private String productTemplateName;
	private TemplatesDTO thistemplate;

	@DefaultHandler
	public Resolution showInterface() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		return new ForwardResolution(
				"/velocity/AdminUI/admincreateproducttemplate.vm");
	}

	public Resolution saveProductTemplate() throws InvalidParameterException,
			IOException, NamingException {
		HttpSession session = getContext().getRequest().getSession();
		byte[] pdf = null;
		try {
			pdf = IOUtils.toByteArray(attachment.getInputStream());
		} catch (Exception e) {
			getContext().getRequest().setAttribute("message", "error.badpdf");
			return new ForwardResolution(
					"/velocity/AdminUI/admincreateproducttemplate.vm");
		}

		thistemplate = TemplatesDTO.createNewTemplatesDTO(
				getProductTemplateName(), pdf, getApplicationPdf(),
				getProductTemplateDescription());
		session.setAttribute("thistemplate", thistemplate);

		try {
			attachment.delete();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ForwardResolution("/velocity/AdminUI/adminvalidatepdf.vm");
	}

	public Resolution save() {
		try {
			HttpSession session = getContext().getRequest().getSession();
			thistemplate = (TemplatesDTO) session.getAttribute("thistemplate");
			AccountDTO myAccount = (AccountDTO) session
					.getAttribute("accountDO");
			ProductBeanService pbean = JndiUtil.lookup("ProductBean");
			pbean.putTemplate(myAccount, thistemplate);
			session.removeAttribute("thistemplate");
		} catch (Exception e) {
			getContext().getRequest().setAttribute("message", "error.badpdf");
			return new ForwardResolution(
					"/velocity/AdminUI/admincreateproducttemplate.vm");
		}

		return new RedirectResolution(
				generateLocaleLink("/dispatcher/helloadmin"));
	}

	public Resolution print() {
		HttpSession session = getContext().getRequest().getSession();
		thistemplate = (TemplatesDTO) session.getAttribute("thistemplate");

		try {
			return getPdf();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			getContext().getRequest().setAttribute("message", "error.badpdf");
			return new ForwardResolution(
					"/velocity/AdminUI/admincreateproducttemplate.vm");
		}
	}

	public Resolution getPdf() throws IOException, InvalidParameterException,
			NamingException {
		return new Resolution() {
			@Override
			public void execute(HttpServletRequest request,
					HttpServletResponse response) throws Exception {
			
				HttpServletResponse res = getContext().getResponse();
				res.setHeader("Content-Disposition",
						"inline; attachment; filename=" + "test" + ".pdf");
				res.setContentType(APPLICATION_PDF);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				try {
					byte[] pdf = printPdf();

					baos.write(pdf);
					res.setContentLength(baos.size());
					ServletOutputStream out = res.getOutputStream();
					baos.writeTo(out);
					out.flush();
					out.close();
				} catch (Exception e) {
					
				}
			}
		};

	}

	private byte[] printPdf() throws IOException, OutputException,
			BarcodeException {
		PrintProductPdfBeanService printProductPdfBeanService = JndiUtil
				.lookup("PrintProductPdfBean");
		HttpSession session = getContext().getRequest().getSession();
		ESystemDTO system = (ESystemDTO) session.getAttribute("systemName");
		String productNumber = "100000";
		TemplatesDTO templatesDTO = thistemplate;
		
		return printProductPdfBeanService.createPDF(createMap(), templatesDTO,
				productNumber, system);
	}

	private Map<PdfField, Object> createMap() throws OutputException,
			BarcodeException {
		Map<PdfField, Object> values = new HashMap<PdfField, Object>();
		values.put(PdfField.FIRSTNAME, "Max");
		values.put(PdfField.LASTNAME, "Mustermann");
		values.put(PdfField.CITY, "Musterstadt");
		values.put(PdfField.ADRESS, "Musterstrase");
		values.put(PdfField.ORGA, "Musterorga");
		values.put(PdfField.ZIP, "012345");
		values.put(PdfField.BARCODE, true);
		return values;
	}

	@Override
	public void setContext(ActionBeanContext context) {
		// TODO Auto-generated method stub
		this.context = context;
	}

	@Override
	public ActionBeanContext getContext() {
		// TODO Auto-generated method stub
		return context;
	}

	public FileBean getAttachment() {
		return attachment;
	}

	public void setAttachment(FileBean attachment) {
		this.attachment = attachment;
	}

	public static String getApplicationPdf() {
		return APPLICATION_PDF;
	}

	public String getProductTemplateDescription() {
		return productTemplateDescription;
	}

	public void setProductTemplateDescription(String productTemplateDescription) {
		this.productTemplateDescription = productTemplateDescription;
	}

	public String getProductTemplateName() {
		return productTemplateName;
	}

	public void setProductTemplateName(String productTemplateName) {
		this.productTemplateName = productTemplateName;
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		// TODO Auto-generated method stub
		return new ForwardResolution(
				"/velocity/AdminUI/admincreateproducttemplate.vm");
	}

	private String getUtf8(String string) throws UnsupportedEncodingException {
		if (string != null
				&& (getContext().getRequest().getCharacterEncoding() == null || getContext()
						.getRequest().getCharacterEncoding().equals("UTF-8")))
			return new String(string.getBytes("8859_1"), "UTF8");
		else
			return string;
	}
	
	/**Before Test, if the user is an admin 
	 * 
	 * @throws IllegalAccessException
	 * 					If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException {
		setLocale((Locale) getContext().getRequest().getSession()
				.getAttribute(LOCALE));
		AccountDTO myAccount = (AccountDTO) getContext().getRequest()
				.getSession().getAttribute("accountDO");
		if (!myAccount.getRole().equals(
				RoleDTO.getSysAdmin(myAccount.getSystem().getId()))) {
			throw new IllegalAccessException();

		}

	}
}
