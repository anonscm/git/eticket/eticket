package org.linuxtag.eticket.userfrontend.product;

import java.security.InvalidParameterException;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ScopedLocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.product.ProductTransferService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

@UrlBinding("/dispatcher/transaction")
public class TransactionActionBean extends GenericLocale implements
	ValidationErrorHandler{
	
	private ActionBeanContext context;
	@Validate(on = {"cancel", "accept", "decline"}, required = true, trim = true)
	private String password;
		
	@Override
	public void setContext(ActionBeanContext context) {
		this.context = context;		
	}

	@Override
	public ActionBeanContext getContext() {
		return context;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@DefaultHandler
	public Resolution showProductDetail() throws Exception, InvalidParameterException {
		return new ForwardResolution("/dispatcher/myproduct");
	} 
	
	@HandlesEvent(value="showCancel")
	public Resolution showTransactionAktionCancel() throws Exception {
		HttpSession session = getContext().getRequest().getSession();
		long transferActivityId = Long.parseLong(getContext().getRequest().getParameter("transferActivityId").toString());
		//String recipient = getContext().getRequest().getParameter("recipient").toString();
		session.setAttribute("transferActivityId", transferActivityId);
		//session.setAttribute("recipient", recipient);
		session.setAttribute("transactionAktion", "Cancel");
		getContext().getRequest().setAttribute("transactionAktion", "Cancel");
		return new ForwardResolution("/velocity/transactionaktion.vm");
	}
	
	@HandlesEvent(value="showAccept")
	public Resolution showTransactionAktionAccept() throws Exception {
		HttpSession session = getContext().getRequest().getSession();
		long transferActivityId = Long.parseLong(getContext().getRequest().getParameter("transferActivityId").toString());
		session.setAttribute("transferActivityId", transferActivityId);
		session.setAttribute("transactionAktion", "Accept");
		getContext().getRequest().setAttribute("transactionAktion", "Accept");
		return new ForwardResolution("/velocity/transactionaktion.vm");
	}
	
	@HandlesEvent(value="showDecline")
	public Resolution showTransactionAktionDecline() throws Exception {
		HttpSession session = getContext().getRequest().getSession();
		long transferActivityId = Long.parseLong(getContext().getRequest().getParameter("transferActivityId").toString());
		session.setAttribute("transferActivityId", transferActivityId);
		session.setAttribute("transactionAktion", "Decline");
		getContext().getRequest().setAttribute("transactionAktion", "Decline");
		return new ForwardResolution("/velocity/transactionaktion.vm");
	}
	
	public Resolution cancel() throws IllegalAccessException{
		ProductTransferService productTransferService = JndiUtil.lookup("ProductTransferBean");
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO accountDTO = (AccountDTO)session.getAttribute("accountDO");
		AccountDTO sender = AccountDTO.init(accountDTO.getSystem(), accountDTO.getUsername(), password,
				accountDTO.getRole(), accountDTO.getOpenId() );
				
		if(!checkPassword(sender)){
			ValidationError error = new ScopedLocalizableError("/dispatcher/myproduct",
					"sendProduct.incorrectPassword");
			ValidationErrors errors = new ValidationErrors();
			errors.add("password", error);
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/transactionaktion.vm");
		}
		
		long transferActivityId = Long.parseLong(session.getAttribute("transferActivityId").toString());
		sender = accountBeanService.validatePassword(sender);
		productTransferService.cancel(sender, transferActivityId);
		return new ForwardResolution("/dispatcher/myproduct");
	}
	
	public Resolution accept() throws IllegalAccessException{
		ProductTransferService productTransferService = JndiUtil.lookup("ProductTransferBean");
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO accountDTO = (AccountDTO)session.getAttribute("accountDO");
		AccountDTO sender = AccountDTO.init(accountDTO.getSystem(), accountDTO.getUsername(), password,
				accountDTO.getRole(), accountDTO.getOpenId());
		
		if(!checkPassword(sender)){
			ValidationError error = new ScopedLocalizableError("/dispatcher/myproduct",
					"sendProduct.incorrectPassword");
			ValidationErrors errors = new ValidationErrors();
			errors.add("password", error);
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/transactionaktion.vm");
		}
		
		long transferActivityId = Long.parseLong(session.getAttribute("transferActivityId").toString());
		sender = accountBeanService.validatePassword(sender);
		productTransferService.accept(sender, transferActivityId);
		return new ForwardResolution("/dispatcher/myproduct");
	}
	
	public Resolution decline() throws IllegalAccessException{
		ProductTransferService productTransferService = JndiUtil.lookup("ProductTransferBean");
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO accountDTO = (AccountDTO)session.getAttribute("accountDO");
		AccountDTO sender = AccountDTO.init(accountDTO.getSystem(), accountDTO.getUsername(), password,
				accountDTO.getRole(), accountDTO.getOpenId());
		
		if(!checkPassword(sender)){
			ValidationError error = new ScopedLocalizableError("/dispatcher/myproduct",
					"sendProduct.incorrectPassword");
			ValidationErrors errors = new ValidationErrors();
			errors.add("password", error);
			getContext().setValidationErrors(errors);
			return new ForwardResolution("/velocity/transactionaktion.vm");
		}
		
		long transferActivityId = Long.parseLong(session.getAttribute("transferActivityId").toString());
		sender = accountBeanService.validatePassword(sender);
		productTransferService.decline(sender, transferActivityId);
		return new ForwardResolution("/dispatcher/myproduct");
	}
	
	private boolean checkPassword(AccountDTO accountDTO){
		AccountBeanService accountBeanService = JndiUtil.lookup("AccountBean");
		try{
			accountBeanService.validatePassword(accountDTO);
			return true;	
		}catch (IllegalAccessException e) {
			return false;
		}
	}
	
	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {
		HttpSession session = getContext().getRequest().getSession();
		getContext().getRequest().setAttribute("transactionAktion", session.getAttribute("transactionAktion"));
		return new ForwardResolution("/velocity/transactionaktion.vm");
	}
}
