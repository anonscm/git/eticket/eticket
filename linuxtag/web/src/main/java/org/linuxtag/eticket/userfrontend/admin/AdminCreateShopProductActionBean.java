package org.linuxtag.eticket.userfrontend.admin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ScopedLocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.ProductNumberBeanService;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductNumberDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticketshop.business.product.ShopProductBeanService;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;
import org.linuxtag.eticket.userfrontend.locale.GenericLocale;
import org.linuxtag.utils.JndiUtil;

/**
 * Create a ShopPorduct like the normal CreateProductActionBean but does'not
 * support ProductAutpomatTransfers
 * 
 * @author dgille
 * 
 */

@UrlBinding("/dispatcher/admincreateshopproduct")
public class AdminCreateShopProductActionBean extends GenericLocale implements
		ActionBean, ValidationErrorHandler {
	private ActionBeanContext context;
	private List<ProductLocaleDTO> liste;
	private ProductLocaleDTO[] selectedlocales;
	@Validate(on = "createProduct", required = true)
	private String pname;
	@Validate(on = "createProduct", required = true)
	private String amount;
	@Validate(on = "createProduct", required = true)
	private String inputdate;
	@Validate(on = "createProduct", required = true)
	private String active;
	@Validate(on = "createProduct", required = true)
	private String shop;

	private String startNumber;
	private String endNumber;
	private String stepNumber;
	long begin = 0;
	long end = 0;
	long step = 0;

	/**
	 * Generates a form for creating Products
	 * 
	 * @return Admincreateproduct.vm
	 * @throws IOException
	 */

	@DefaultHandler
	public Resolution showInterface() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO myAccount = (AccountDTO) session.getAttribute("accountDO");
		ProductBeanService productBeanRemote = JndiUtil.lookup("ProductBean");
		liste = productBeanRemote.getAllProductLocales(myAccount);
		session.setAttribute("productlocales", liste);
		ProductAssociation[] passociations = ProductAssociation.values();
		session.setAttribute("passociations", passociations);
		return new ForwardResolution(
				"/velocity/AdminUI/admincreateshopproduct.vm");
	}

	/**
	 * Creates the product
	 * 
	 * @return Product overview (adminindex/adminaction)
	 * @throws IOException
	 */
	public Resolution createShopProduct() throws IOException {
		ProductDTO product = createProductDTO();
		ProductBeanService productBeanRemote = JndiUtil.lookup("ProductBean");
		HttpSession session = getContext().getRequest().getSession();
		if (product == null || product.getProductLocales() == null) {
			// Es wird keine spezifische Fehlermeldung ausgegeben, da es zu
			// viele mögliche Fehlerkombinationen gibt
			getContext().getRequest().setAttribute("message", "error.baddata");
			return new ForwardResolution(
					"/velocity/AdminUI/admincreateshopproduct.vm");
		}
		try {
			if (validatePnumber()) {
				AccountDTO myAccount = (AccountDTO) session
						.getAttribute("accountDO");
				productBeanRemote.createPWhashed(myAccount, product);
				ShopProductDTO sproduct = createShopProductDTO(product);
				ShopProductBeanService shopProductBeanRemote = JndiUtil
						.lookup("ShopProductBean");
				shopProductBeanRemote.createShopProduct(sproduct);
				ProductNumberBeanService productNumberBeanRemote = JndiUtil
						.lookup("ProductNumberBean");
				ProductNumberDTO nummer = ProductNumberDTO
						.createNewProductNumberDTO(begin, end, step,
								productBeanRemote.get(myAccount.getSystem()
										.getName(), product.getUin()));
				productNumberBeanRemote.cerateProductNumber(myAccount, nummer);
			} else {
				throw new Exception();
			}
		}

		catch (Exception e) {
			getContext().getRequest().setAttribute("message", "error.baddata");
			return new ForwardResolution(
					"/velocity/AdminUI/admincreateshopproduct.vm");

		}
		return new RedirectResolution(
				generateLocaleLink("/dispatcher/adminindex"));
	}

	/**
	 * Excluded Method for getting the selected Productlocels. The validition
	 * works, but could be refcatored.
	 * 
	 * @return
	 */

	@SuppressWarnings("unchecked")
	private ProductLocaleDTO[] getplocales() {
		String[] selected = getContext().getRequest().getParameterValues(
				"Selectedlocales");
		if (selected == null) {
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/admincreateproduct.plocales",
					"valueNotPresent");
			ValidationErrors errors = new ValidationErrors();
			errors.add("plocales", error);
			return null;
			// ValidationError
		} else {
			HttpSession session = getContext().getRequest().getSession();
			liste = (List<ProductLocaleDTO>) session
					.getAttribute("productlocales");
			List<ProductLocaleDTO> returnlist = new ArrayList<ProductLocaleDTO>();
			for (ProductLocaleDTO artifact : liste) {
				for (int i = 0; i < selected.length; i++) {
					if (artifact.getId() == Long.parseLong(selected[i])) {
						returnlist.add(artifact);
					}
				}
			}

			// Validation of amount of supported locales. Exampla : A product ca
			// not have 2 geman locales
			AccountDTO myaccount = (AccountDTO) session
					.getAttribute("accountDO");
			SystemBeanService systemBean = JndiUtil.lookup("SystemBean");
			ESystemDTO system = systemBean.getSystemById(myaccount.getSystem()
					.getId());
			List<LocaleDTO> locales = system.getSupportedLocales();
			for (LocaleDTO locale : locales) {
				int counter = 0;
				for (ProductLocaleDTO artifact : returnlist) {
					if (artifact.getLocale().equals(locale)) {
						counter++;
					}
				}
				if (counter > 1) {
					return null;
				}

			}

			ProductLocaleDTO[] plocales = new ProductLocaleDTO[returnlist
					.size()];
			returnlist.toArray(plocales);
			return plocales;
		}
	}

	
	
	/**This private Method validates the Productnumberdata
	 * 
	 * @return
	 *        boolean, if the ProductnumberData is valid.
	 */
	private boolean validatePnumber() {

		boolean failed = true;
		try {
			begin = Integer.parseInt(getStartNumber());
			end = Integer.parseInt(getEndNumber());
			step = Integer.parseInt(getStepNumber());

		} catch (Exception e) {
			e.printStackTrace();
			failed = false;
		}
		if (begin > end) {
			failed = false;
		}
		if (step > begin) {
			failed = false;
		}
		if (begin > 10000 || end > 10000) {
			failed = false;
		}
		return failed;

	}

	/**
	 * Creates the Shopproduct, using the created Product
	 * 
	 * @param product
	 *            the created ProductDTO
	 * @return the shopproductDTO
	 */
	private ShopProductDTO createShopProductDTO(ProductDTO product) {
		boolean activated = false;
		if (getActive().equals("true")) {
			activated = true;
		}
		ProductAssociation pa = ProductAssociation.fromString(getShop());
		ShopProductDTO sproduct = ShopProductDTO.createNewShopProductDTO(
				product, activated, pa);
		return sproduct;

	}

	/**
	 * Method for setting up the productDTO. It gets all Data by itself
	 * 
	 * @return The new ProdcutDTO
	 */
	private ProductDTO createProductDTO() {
		ProductDTO product = null;
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {
			date = formatter.parse(getInputdate());
		} catch (ParseException e) {
			ValidationError error = new ScopedLocalizableError(
					"/dispatcher/admincreateproduct.inputdate",
					"valueNotPresent");
			ValidationErrors errors = new ValidationErrors();
			errors.add("inputdate", error);
			getContext().setValidationErrors(errors);
			return null;
		}
		HttpSession session = getContext().getRequest().getSession();
		AccountDTO myaccount = (AccountDTO) session.getAttribute("accountDO");
		int amount;
		try {
			amount = Integer.parseInt(getAmount());
		} catch (Exception e) {
			return null;
		}
		try {
			product = ProductDTO.createNewProductDTO(getUtf8(getPname()),
					ProductType.DIGITAL, myaccount.getSystem(), getplocales(),
					10, amount, date);
		} catch (UnsupportedEncodingException e) {
			return null;
		}
		return product;

	}

	private String getUtf8(String string) throws UnsupportedEncodingException {
		if (string != null
				&& (getContext().getRequest().getCharacterEncoding() == null || getContext()
						.getRequest().getCharacterEncoding().equals("UTF-8")))
			return new String(string.getBytes("8859_1"), "UTF8");
		else
			return string;
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	@Override
	public Resolution handleValidationErrors(ValidationErrors errors)
			throws Exception {

		return new ForwardResolution(
				"/velocity/AdminUI/admincreateshopproduct.vm");
	}

	public List<ProductLocaleDTO> getListe() {
		return liste;
	}

	public void setListe(List<ProductLocaleDTO> liste) {
		this.liste = liste;
	}

	public void setPname(String name) {
		pname = name;
	}

	public String getPname() {
		return pname;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAmount() {
		return amount;
	}

	public ProductLocaleDTO[] getSelectedlocales() {
		return selectedlocales;
	}

	public void setSelectedlocales(ProductLocaleDTO[] selectedlocales) {
		this.selectedlocales = selectedlocales;
	}

	public String getInputdate() {
		return inputdate;
	}

	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getShop() {
		return shop;
	}

	public void setShop(String shop) {
		this.shop = shop;
	}

	/**
	 * Before Test, if the user is an admin
	 * 
	 * @throws IllegalAccessException
	 *             If the user is not an admin, this exception will be thrown
	 */
	@Before
	private void testRole() throws IllegalAccessException {
		AccountDTO myAccount = (AccountDTO) getContext().getRequest()
				.getSession().getAttribute("accountDO");
		setLocale((Locale) getContext().getRequest().getSession()
				.getAttribute(LOCALE));
		if (!myAccount.getRole().equals(
				RoleDTO.getSysAdmin(myAccount.getSystem().getId()))) {
			throw new IllegalAccessException();
		}

	}

	public String getStartNumber() {
		return startNumber;
	}

	public void setStartNumber(String startNumber) {
		this.startNumber = startNumber;
	}

	public String getEndNumber() {
		return endNumber;
	}

	public void setEndNumber(String endNumber) {
		this.endNumber = endNumber;
	}

	public String getStepNumber() {
		return stepNumber;
	}

	public void setStepNumber(String stepNumber) {
		this.stepNumber = stepNumber;
	}
}