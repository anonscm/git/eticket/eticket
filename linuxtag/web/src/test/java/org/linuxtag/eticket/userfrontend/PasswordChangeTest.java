package org.linuxtag.eticket.userfrontend;

import java.sql.SQLException;

import javax.naming.NamingException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.linuxtag.eticket.db.Database;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PasswordChangeTest {
	
	private static WebDriver driver = new FirefoxDriver();

	@BeforeClass
	public static void init() throws NamingException {
		driver.get("http://localhost:8080/LinuxTagInit/dispatcher/");
	}

	@Before
	public void deleteCookiesAndDeleteAccount() throws SQLException {
		driver.manage().deleteAllCookies();
	}
	
	@Test
	public void forgotPasswordWithoutPassword() throws SQLException, ClassNotFoundException {
		generateToken("m.johnki@web.de", driver);		
		driver.findElement(By.name("activateUser")).click();
		if (!driver.getPageSource().contains("Dieser Link ist nicht gültig. ")) {
			throw new IllegalArgumentException();
		}
	}
	
	@Test
	public void forgotPasswordWithWrongToken(){
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/");
		driver.findElement(By.linkText("Passwort vergessen?")).click();
		driver.findElement(By.name("username")).sendKeys("m.johnki@web.de");
		driver.findElement(By.name("passwordforgot")).click();
		if (!driver.getPageSource().contains("Es wurde eine Mail mit dem weiteren Vorgehen an die Adresse verschickt.")) {
			throw new IllegalArgumentException();
		}
		String url = "http://localhost:8080/LinuxTagUserWeb/dispatcher/activate?token="
				+ "1"
				+ "&clientname=m.johnki@web.de&platform=3&system=linuxtag";
		driver.get(url);
		driver.findElement(By.name("newPassword1")).sendKeys("1");
		driver.findElement(By.name("newPassword2")).sendKeys("1");
		driver.findElement(By.name("activateUser")).click();
		if (!driver.getPageSource().contains("Dieser Link ist nicht gültig. ")) {
			throw new IllegalArgumentException();
		}
	}
	

	@Test
	public void forgotPasswordWithTwoDiffenercePassword() throws SQLException, ClassNotFoundException {
		generateToken("m.johnki@web.de", driver);
		driver.findElement(By.name("newPassword1")).sendKeys("1");
		driver.findElement(By.name("newPassword2")).sendKeys("2");
		driver.findElement(By.name("activateUser")).click();
		if (!driver.getPageSource().contains("Die Passwordwiederholung war leider nicht korrekt.")) {
			throw new IllegalArgumentException();
		}
	}
	
	@Test
	public void forgotPassword() throws SQLException, ClassNotFoundException {
		setPassword("m.johnki@web.de", "1", driver);
		new LoginTest().login("m.johnki@web.de", "1", driver);
		setPassword("m.johnki@web.de", "d", driver);
		new LoginTest().login("m.johnki@web.de", "d", driver);
	}
	
	private void setPassword(String username, String password, WebDriver driver) throws SQLException, ClassNotFoundException{
		generateToken(username, driver);	
		driver.findElement(By.name("newPassword1")).sendKeys(password);
		driver.findElement(By.name("newPassword2")).sendKeys(password);
		driver.findElement(By.name("activateUser")).click();
	}
	
	private void generateToken(String username, WebDriver driver) throws SQLException, ClassNotFoundException{
		driver.manage().deleteAllCookies();		
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/");
		driver.findElement(By.linkText("Passwort vergessen?")).click();
		driver.findElement(By.name("username")).sendKeys(username);
		driver.findElement(By.name("passwordforgot")).click();
		if (!driver.getPageSource().contains("Es wurde eine Mail mit dem weiteren Vorgehen an die Adresse verschickt.")) {
			throw new IllegalArgumentException();
		}
		String token = Database.getActivateToken(username);
		String url = "http://localhost:8080/LinuxTagUserWeb/dispatcher/activate?token="
				+ token
				+ "&clientname="+username+"&platform=3&system=linuxtag";
		driver.get(url);	
	}
	
	@After
	public void cleanUp() throws SQLException, ClassNotFoundException{
		Database.deleteAllActivateTokens();
	}
	
	@AfterClass
	public static void clean() {
		driver.close();
	}

}
