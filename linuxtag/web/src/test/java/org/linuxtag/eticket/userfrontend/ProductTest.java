package org.linuxtag.eticket.userfrontend;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ProductTest {

	private static WebDriver driver = new FirefoxDriver();

	@BeforeClass
	public static void init() {
		driver.get("http://localhost:8080/LinuxTagInit/dispatcher/");
		try {
			new LoginTest().login("m.johnki@gmx.de", "c", driver);
			insertPersonaldata(driver);
			acceptProduct("c", driver);
			driver.manage().deleteAllCookies();
			new LoginTest().login("m.johnki@web.de", "d", driver);
			insertPersonaldata(driver);
			acceptProduct("d", driver);
		} catch (Exception e) {}
	}

	@Before
	public void deleteCookies() {
		driver.manage().deleteAllCookies();
	}
	
	@Test
	public void test(){
		
	}

	@Test
	public void sendProduct() {
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		sendProduct("c", "m.johnki@web.de", driver);
	}

	@Test
	public void cancel() {
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		driver.findElement(By.linkText("Abbrechen")).click();
		driver.findElement(By.name("password")).sendKeys("c");
		driver.findElement(By.name("cancel")).click();
	}

	@Test
	public void cancelWithTwoOpenProducts() {
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		sendProduct("c", "m.johnki@web.de", driver);
		sendProduct("c", "m.johnki@web.de", driver);
		driver.findElement(By.linkText("Abbrechen")).click();
		driver.findElement(By.name("password")).sendKeys("c");
		driver.findElement(By.name("cancel")).click();
		if (!driver.getPageSource().contains("Offene Tickets")) {
			throw new IllegalArgumentException();
		}
		if (driver.getPageSource().contains("Versendete Tickets")) {
			throw new IllegalArgumentException();
		}
	}

	@Test
	public void acceptProduct() {
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		sendProduct("c", "m.johnki@web.de", driver);
		driver.manage().deleteAllCookies();
		new LoginTest().login("m.johnki@web.de", "d", driver);
		acceptProduct("d", driver);
		bookProductBack("m.johnki@web.de", "d", "m.johnki@gmx.de", "c", driver);
	}

	@Test
	public void acceptProductWithTwoOpenProducts() {
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		sendProduct("c", "m.johnki@web.de", driver);
		sendProduct("c", "m.johnki@web.de", driver);
		driver.manage().deleteAllCookies();
		new LoginTest().login("m.johnki@web.de", "d", driver);
		acceptProduct("d", driver);
		bookProductBack("m.johnki@web.de", "d", "m.johnki@gmx.de", "c", driver);
		bookProductBack("m.johnki@web.de", "d", "m.johnki@gmx.de", "c", driver);
	}

	@Test
	public void declineProduct() {
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		sendProduct("c", "m.johnki@web.de", driver);
		driver.manage().deleteAllCookies();
		new LoginTest().login("m.johnki@web.de", "d", driver);
		declineProduct("d", driver);
		if (!driver.getPageSource().contains("4")) {
			throw new IllegalArgumentException();
		}
	}

	@Test
	public void declineProductWithTwoOpenProducts() {
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		sendProduct("c", "m.johnki@web.de", driver);
		sendProduct("c", "m.johnki@web.de", driver);
		driver.manage().deleteAllCookies();
		new LoginTest().login("m.johnki@web.de", "d", driver);
		declineProduct("d", driver);
		if (!driver.getPageSource().contains("4")) {
			throw new IllegalArgumentException();
		}
	}

	void sendProduct(String password, String receiverUsername, WebDriver driver) {
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/productdetail?locale=de");
		driver.findElement(By.linkText("Senden")).click();
		driver.findElement(By.name("acceptor")).sendKeys(receiverUsername);
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.name("giveaway")).click();

		if (!driver
				.getPageSource()
				.contains(
						"Vielen Dank für das Versenden von Einladungen zum LinuxTag. Der Empfänger "
								+ "erhält nun eine E-Mail mit einer Anleitung für die Annahme der Einladung. "
								+ "Bitte beachten Sie, dass die Gültigkeit der Einladung befristet ist. "
								+ "Bei Ablauf der Gültigkeit erhalten Sie die versendeten Einladungen wieder "
								+ "zurück und können diese neu versenden. ")) {
			throw new IllegalArgumentException();
		}
	}

	void declineProduct(String password, WebDriver driver) {
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/productdetail?locale=de");
		driver.findElement(By.linkText("Ablehnen")).click();
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.name("decline")).click();
	}

	static void acceptProduct(String password, WebDriver driver) {
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/productdetail?locale=de");
		driver.findElement(By.linkText("Akzeptieren")).click();
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.name("accept")).click();
	}

	void bookProductBack(String senderUsername, String senderPassword,
			String receiverUsername, String recieverPassword, WebDriver driver) {
		driver.manage().deleteAllCookies();
		new LoginTest().login(senderUsername, senderPassword, driver);
		sendProduct(senderPassword, receiverUsername, driver);
		driver.manage().deleteAllCookies();
		new LoginTest().login(receiverUsername, recieverPassword, driver);
		acceptProduct(recieverPassword, driver);
		driver.manage().deleteAllCookies();
	}

	static void insertPersonaldata(WebDriver driver) {
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		new PersonalInformationTest().updateDropDownList("salutation", "Herr",
				driver);
		new PersonalInformationTest().insertPersonalData("", "Max",
				"Mustermann", "MusterStraße 42", "77777", "Musterstadt", "",
				driver);
		driver.findElement(By.name("savePersonalData")).click();
	}

	@AfterClass
	public static void clean() {
		driver.close();
	}

}
