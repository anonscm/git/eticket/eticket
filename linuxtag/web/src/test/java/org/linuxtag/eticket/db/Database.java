package org.linuxtag.eticket.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Database {
	
	private static Connection connection = null;
	
	private static void connect() throws SQLException, ClassNotFoundException{
		Class.forName("org.postgresql.Driver");
		connection = DriverManager.getConnection(
				"jdbc:postgresql://127.0.0.1:5432/mjohnk", "mjohnk",
				"test");
	}
	
	public static void deleteUser(String username) throws SQLException, ClassNotFoundException{
		deletetransferHistory();
		connect();
		connection.createStatement().execute("DELETE FROM activateaccounttoken WHERE account_id IN (SELECT id FROM actor WHERE credential_id IN (SELECT id FROM credentials WHERE username = '"+username+"')) ; ");
		connection.createStatement().execute("DELETE FROM account WHERE id in (SELECT id FROM actor WHERE credential_id IN (SELECT id FROM credentials WHERE username = '"+username+"')) ; ");
		connection.createStatement().execute("DELETE FROM productpool WHERE actor_id IN (SELECT id FROM actor WHERE credential_id IN (SELECT id FROM credentials WHERE username = '"+username+"')) ; ");
		connection.createStatement().execute("DELETE FROM actor_role WHERE actor_id IN (SELECT id FROM actor WHERE credential_id IN (SELECT id FROM credentials WHERE username = '"+username+"')) ; ");
		connection.createStatement().execute("DELETE FROM actor WHERE credential_id IN (SELECT id FROM credentials WHERE username = '"+username+"') ; ");
		connection.createStatement().execute("DELETE FROM credentials WHERE username = '"+username+"'");
		connection.close();
		connection = null;
	}
	
	public static String getActivateToken(String username) throws SQLException, ClassNotFoundException{
		connect();
		ResultSet rs = connection.createStatement().executeQuery("SELECT token FROM activateaccounttoken WHERE account_id IN (SELECT id FROM actor WHERE credential_id IN (SELECT id FROM credentials WHERE username = '"+username+"')) ; ");
		rs.next();
		String token = rs.getString("token");
		connection.close();
		connection = null;
		return token;		
	}
	
	public static void deleteAllActivateTokens() throws SQLException, ClassNotFoundException{
		connect();
		connection.createStatement().execute("DELETE FROM activateaccounttoken");
		connection.close();
		connection = null;
	}
	
	public static void deletetransferHistory() throws SQLException, ClassNotFoundException{
		connect();
		connection.createStatement().execute("DELETE FROM transferhistory");
		connection.close();
		connection = null;
	}

}
