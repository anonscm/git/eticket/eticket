package org.linuxtag.eticket.userfrontend;

import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PersonalInformationTest {
	private static WebDriver driver = new FirefoxDriver();

	@BeforeClass
	public static void init() {
		driver.get("http://localhost:8080/LinuxTagInit/dispatcher/");
	}

	@Before
	public void deleteCookies() {
		driver.manage().deleteAllCookies();
	}
	
	@Test
	public void updatePersonalInformation() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData("", "Max", "Mustermann", "MusterStraße 42", "77777",
				"Musterstadt", "", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null, "Ihre Änderungen wurden übernommen.");
	}

	@Test
	public void updatePersonalInformationWithOrgaAndTitle() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData("Dr.", "Max", "Mustermann", "MusterStraße 42",
				"77777", "Musterstadt", "MusterOrga", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields("Dr.", "MusterOrga",
				"Ihre Änderungen wurden übernommen.");
	}

	@Test
	public void updatePersonalInformationWithEuropeanCountry() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData("Dr.", "Max", "Mustermann", "MusterStraße 42",
				"77777", "Musterstadt", "MusterOrga", driver);
		updateDropDownList("country", "europäisches Land", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields("Dr.", "MusterOrga",
				"Ihre Änderungen wurden übernommen.");
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		if (!driver.getPageSource().contains("value=\"european country\"")) {
			throw new IllegalArgumentException();
		}
	}
	
	@Test
	public void updatePersonalInformationWithOtherCountry() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData("Dr.", "Max", "Mustermann", "MusterStraße 42",
				"77777", "Musterstadt", "MusterOrga", driver);
		updateDropDownList("country", "anderes Land", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields("Dr.", "MusterOrga",
				"Ihre Änderungen wurden übernommen.");
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		if (!driver.getPageSource().contains("value=\"other country\"")) {
			throw new IllegalArgumentException();
		}
	}
	
	@Test
	public void updatePersonalInformationWithEnglishLocalecode() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData("Dr.", "Max", "Mustermann", "MusterStraße 42",
				"77777", "Musterstadt", "MusterOrga", driver);
		updateDropDownList("localecode", "Englisch", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields("Dr.", "MusterOrga",
				"Ihre Änderungen wurden übernommen.");
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		if (!driver.getPageSource().contains("value=\"en\"")) {
			throw new IllegalArgumentException();
		}
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata");
		updateDropDownList("localecode", "German", driver);
		driver.findElement(By.name("savePersonalData")).click();
	}

	@Test
	public void updatePersonalInformationWithoutSalutation() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "--", driver);
		insertPersonalData("", "Max", "Mustermann", "MusterStraße 42", "77777",
				"Musterstadt", "", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null, "Bitte geben Sie eine Anrede an.");
	}

	@Test
	public void updatePersonalInformationWithoutFirstname() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData("", "", "Mustermann", "MusterStraße 42", "77777",
				"Musterstadt", "", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null, "Bitte geben Sie einen Vornamen ein.");
	}

	@Test
	public void updatePersonalInformationWithoutLastname() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData("", "Max", "", "MusterStraße 42", "77777",
				"Musterstadt", "", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null, "Bitte geben Sie einen Nachnamen ein.");
	}

	@Test
	public void updatePersonalInformationWithoutAddress() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData("", "Max", "Mustermann", "", "77777", "Musterstadt",
				"", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null,
				"Bitte geben Sie eine Straße und Hausnummer ein.");
	}

	@Test
	public void updatePersonalInformationWithoutZipcode() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData("", "Max", "Mustermann", "MusterStraße 42", "",
				"Musterstadt", "", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null,
				"Bitte geben Sie eine Postleitzahl ein.");
	}

	@Test
	public void updatePersonalInformationWithoutCity() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData("", "Max", "Mustermann", "MusterStraße 42", "77777",
				"", "", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null, "Bitte geben Sie einen Wohnort ein.");
	}

	@Test
	public void updatePersonalInformationWithToLongFirstname() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData(
				"",
				"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcaa",
				"Mustermann", "MusterStraße 42", "77777", "Musterstadt", "",
				driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null, "Der Vorname ist zu lang.");
	}

	@Test
	public void updatePersonalInformationWithToLongLastname() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData(
				"",
				"Max",
				"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcaa",
				"MusterStraße 42", "77777", "Musterstadt", "", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null, "Der Nachname ist zu lang.");
	}

	@Test
	public void updatePersonalInformationWithToLongAddress() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData(
				"",
				"Max",
				"Mustermann",
				"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcaa",
				"77777", "Musterstadt", "", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null, "Die Addresse ist zu lang.");
	}

	@Test
	public void updatePersonalInformationWithToLongZipcode() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData(
				"",
				"Max",
				"Mustermann",
				"MusterStraße 42",
				"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcaa",
				"Musterstadt", "", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null, "Die Postleitzahl ist zu lang.");
	}

	@Test
	public void updatePersonalInformationWithToLongCity() {
		new LoginTest().login("a", "b", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/editpersonaldata?locale=de");
		updateDropDownList("salutation", "Herr", driver);
		insertPersonalData(
				"",
				"Max",
				"Mustermann",
				"MusterStraße 42",
				"77777",
				"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcaa",
				"", driver);
		driver.findElement(By.name("savePersonalData")).click();
		checkFormFields(null, null, "Der Wohnort ist zu lang.");
	}

	private void clearFormFields(WebDriver webDriver) {
		webDriver.findElement(By.name("title")).clear();
		webDriver.findElement(By.name("firstname")).clear();
		webDriver.findElement(By.name("lastname")).clear();
		webDriver.findElement(By.name("address")).clear();
		webDriver.findElement(By.name("zipcode")).clear();
		webDriver.findElement(By.name("city")).clear();
		webDriver.findElement(By.name("orga")).clear();
	}

	private void checkFormFields(String title, String orga, String message) {

		if (title != null) {
			if (!driver.getPageSource().contains(title)) {
				throw new IllegalArgumentException();
			}
		}

		if (!driver.getPageSource().contains("Max")) {
			throw new IllegalArgumentException();
		}

		if (!driver.getPageSource().contains("Mustermann")) {
			throw new IllegalArgumentException();
		}

		if (!driver.getPageSource().contains("MusterStraße 42")) {
			throw new IllegalArgumentException();
		}

		if (!driver.getPageSource().contains("77777")) {
			throw new IllegalArgumentException();
		}

		if (!driver.getPageSource().contains("Musterstadt")) {
			throw new IllegalArgumentException();
		}

		if (title != null) {
			if (!driver.getPageSource().contains(orga)) {
				throw new IllegalArgumentException();
			}
		}

		if (!driver.getPageSource().contains(message)) {
			throw new IllegalArgumentException();
		}
	}

	void insertPersonalData(String title, String firstname, String lastname,
			String address, String zipcode, String city, String orga,
			WebDriver webDriver) {
		clearFormFields(webDriver);
		webDriver.findElement(By.name("title")).sendKeys(title);
		webDriver.findElement(By.name("firstname")).sendKeys(firstname);
		webDriver.findElement(By.name("lastname")).sendKeys(lastname);
		webDriver.findElement(By.name("address")).sendKeys(address);
		webDriver.findElement(By.name("zipcode")).sendKeys(zipcode);
		webDriver.findElement(By.name("city")).sendKeys(city);
		webDriver.findElement(By.name("orga")).sendKeys(orga);
	}

	void updateDropDownList(String nameDropDownList, String name,
			WebDriver webDriver) {
		WebElement select = webDriver.findElement(By.name(nameDropDownList));
		List<WebElement> salutation = select.findElements(By.tagName("option"));
		for (WebElement webElement : salutation) {
			select.sendKeys(Keys.ARROW_UP);
		}
		for (WebElement webElement : salutation) {
			if (webElement.getText().trim().equals(name)) {
				break;
			}
			select.sendKeys(Keys.ARROW_DOWN);
		}
	}

	@AfterClass
	public static void clean() {
		driver.close();
	}

}
