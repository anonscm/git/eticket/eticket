package org.linuxtag.eticket.userfrontend;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class OpenIdTest {
	
private static WebDriver driver = new FirefoxDriver();
	
	@BeforeClass
	public static void init(){
		driver.get("http://localhost:8080/LinuxTagInit/dispatcher/");
	}
	
	@Before
	public void deleteCookies(){
		driver.manage().deleteAllCookies();
	}
	
	@Test
	public void loginWithWrongOpenId(){
		
	}
	
	@Test
	public void loginWithOpenId(){
		
	}
	
	@AfterClass
	public static void clean() {
		driver.close();
	}

}
