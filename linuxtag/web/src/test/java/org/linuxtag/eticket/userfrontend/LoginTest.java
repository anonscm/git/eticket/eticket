package org.linuxtag.eticket.userfrontend;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class LoginTest {	
	
	private static WebDriver driver = new FirefoxDriver();
	
	@BeforeClass
	public static void init(){
		driver.get("http://localhost:8080/LinuxTagInit/dispatcher/");
	}
	
	@Before
	public void deleteCookies(){
		driver.manage().deleteAllCookies();
	}
	
	@Test
	public void loginWithFalseUsernameAndPassword() {	
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/");
		driver.findElement(By.id("loginUsername")).sendKeys("a@b.de");
		driver.findElement(By.id("loginPassword")).sendKeys("123");
		driver.findElement(By.name("login")).click();
		if(!driver.getPageSource().contains("Entweder ist der Nutzername oder das Passwort falsch")){
			throw new IllegalArgumentException();
		}
	}
	
	@Test
	public void login() {
		login("a", "b", driver);		
	}	
	
	void login(String username, String password, WebDriver webDriver){
		webDriver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/");
		webDriver.findElement(By.id("loginUsername")).sendKeys(username);
		webDriver.findElement(By.id("loginPassword")).sendKeys(password);
		webDriver.findElement(By.name("login")).click();
		if(webDriver.getPageSource().contains("Entweder ist der Nutzername oder das Passwort falsch")){
			throw new IllegalArgumentException();
		}
		
	}
	
	@AfterClass
	public static void clean(){
		driver.close(); 
	}
	
}
