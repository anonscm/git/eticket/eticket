package org.linuxtag.eticket.userfrontend;

import java.sql.SQLException;

import javax.naming.NamingException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.linuxtag.eticket.db.Database;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class RegisterTest {

	private static WebDriver driver = new FirefoxDriver();

	@BeforeClass
	public static void init() throws NamingException {
		driver.get("http://localhost:8080/LinuxTagInit/dispatcher/");
	}

	@Before
	public void deleteCookiesAndDeleteAccount() throws SQLException {
		driver.manage().deleteAllCookies();
	}

	@Test
	public void register() {
		try {
			Database.deleteUser("EmailMuster@gmx.de");
		} catch (Exception e) {
			e.printStackTrace();
		}

		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/");
		driver.findElement(By.id("registerUsername")).sendKeys(
				"EmailMuster@gmx.de");
		driver.findElement(By.name("register")).click();
		if (!driver.getPageSource().contains("Vielen Dank für die Registrierung.")) {
			throw new IllegalArgumentException();
		}
	}

	@Test
	public void activateAccountWithWrongToken() throws SQLException,
			ClassNotFoundException {
		String url = "http://localhost:8080/LinuxTagUserWeb/dispatcher/activate?token="
				+ "1"
				+ "&clientname=EmailMuster@gmx.de&platform=3&system=linuxtag";
		driver.get(url);
		driver.findElement(By.name("newPassword1")).sendKeys("1");
		driver.findElement(By.name("newPassword2")).sendKeys("1");
		driver.findElement(By.name("activateUser")).click();
		if (!driver.getPageSource().contains("Dieser Link ist nicht gültig. ")) {
			throw new IllegalArgumentException();
		}
	}

	@Test
	public void activateAccountWithoutPassword() throws SQLException,
			ClassNotFoundException {
		String token = Database.getActivateToken("EmailMuster@gmx.de");
		String url = "http://localhost:8080/LinuxTagUserWeb/dispatcher/activate?token="
				+ token
				+ "&clientname=EmailMuster@gmx.de&platform=3&system=linuxtag";
		driver.get(url);
		driver.findElement(By.name("activateUser")).click();
		if (!driver.getPageSource().contains("Dieser Link ist nicht gültig. ")) {
			throw new IllegalArgumentException();
		}
	}

	@Test
	public void activateAccount() throws SQLException, ClassNotFoundException {
		String token = Database.getActivateToken("EmailMuster@gmx.de");
		String url = "http://localhost:8080/LinuxTagUserWeb/dispatcher/activate?token="
				+ token
				+ "&clientname=EmailMuster@gmx.de&platform=3&system=linuxtag";
		driver.get(url);
		driver.findElement(By.name("newPassword1")).sendKeys("1");
		driver.findElement(By.name("newPassword2")).sendKeys("1");
		driver.findElement(By.name("activateUser")).click();
		new LoginTest().login("EmailMuster@gmx.de", "1", driver);
	}

	@AfterClass
	public static void clean() {
		driver.close();
	}

}
