package org.linuxtag.eticket.userfrontend;

import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.linuxtag.eticket.db.Database;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TransferHistoryTest {
	
	private static WebDriver driver = new FirefoxDriver();
	
	@BeforeClass
	public static void init(){
		driver.get("http://localhost:8080/LinuxTagInit/dispatcher/");
		try {
			new LoginTest().login("m.johnki@gmx.de", "c", driver);
			ProductTest.insertPersonaldata(driver);
			ProductTest.acceptProduct("c", driver);
			driver.manage().deleteAllCookies();
			driver.get("http://localhost:8080/LinuxTagInit/dispatcher/");
			new LoginTest().login("m.johnki@web.de", "d", driver);
			ProductTest.insertPersonaldata(driver);
			ProductTest.acceptProduct("d", driver);
		} catch (Exception e) {}
	}
	
	@Before
	public void deleteCookies() throws SQLException, ClassNotFoundException {
		Database.deletetransferHistory();
		driver.manage().deleteAllCookies();		
	}

	@Test	
	public void TransferHistoryWithAcceptedProduct(){
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		new ProductTest().sendProduct("c", "m.johnki@web.de", driver);
		driver.manage().deleteAllCookies();
		new LoginTest().login("m.johnki@web.de", "d", driver);
		ProductTest.acceptProduct("d", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/history?locale=de");
		if(driver.getPageSource().contains("Versendete Tickets Offen") || 
				driver.getPageSource().contains("Versendete Tickets Abgebrochen") || 
				driver.getPageSource().contains("Versendete Tickets Zurückgewiesen") || 
				driver.getPageSource().contains("Versendete Tickets Akzeptiert") ||
				driver.getPageSource().contains("Erhaltende Tickets Zurückgewiesen") ||
				driver.getPageSource().contains("Erhaltende Tickets Offen")){
			throw new IllegalArgumentException();
		}
		if(!driver.getPageSource().contains("Erhaltende Tickets Akzeptiert")){
			throw new IllegalArgumentException();
		}
		
		new ProductTest().bookProductBack("m.johnki@web.de", "d", "m.johnki@gmx.de", "c", driver);
		driver.manage().deleteAllCookies();
		new LoginTest().login("m.johnki@web.de", "d", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/history?locale=de");
		if(driver.getPageSource().contains("Versendete Tickets Offen") || 
				driver.getPageSource().contains("Versendete Tickets Abgebrochen") || 
				driver.getPageSource().contains("Versendete Tickets Zurückgewiesen") || 
				driver.getPageSource().contains("Erhaltende Tickets Zurückgewiesen") ||
				driver.getPageSource().contains("Erhaltende Tickets Offen")){
			throw new IllegalArgumentException();
		}
		if(!driver.getPageSource().contains("Erhaltende Tickets Akzeptiert") && !driver.getPageSource().contains("Versendete Tickets Akzeptiert")){
			throw new IllegalArgumentException();
		}
	}

	@Test
	public void TransferHistoryWithCanceledProduct(){
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		new ProductTest().sendProduct("c", "m.johnki@web.de", driver);
		driver.findElement(By.linkText("Abbrechen")).click();
		driver.findElement(By.name("password")).sendKeys("c");
		driver.findElement(By.name("cancel")).click();
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/history?locale=de");
		if(driver.getPageSource().contains("Versendete Tickets Offen")|| 
				driver.getPageSource().contains("Versendete Tickets Zurückgewiesen")|| 
				driver.getPageSource().contains("Erhaltende Tickets Offen")||
				driver.getPageSource().contains("Erhaltende Tickets Akzeptiert")||
				driver.getPageSource().contains("Erhaltende Tickets Zurückgewiesen")||
				driver.getPageSource().contains("Versendete Tickets Akzeptiert")){
			throw new IllegalArgumentException();
		}
		if(!driver.getPageSource().contains("Versendete Tickets Abgebrochen")){
			throw new IllegalArgumentException();
		}
	}	

	@Test
	public void TransferHistoryWithDeclinedProduct(){
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		new ProductTest().sendProduct("c", "m.johnki@web.de", driver);
		driver.manage().deleteAllCookies();
		new LoginTest().login("m.johnki@web.de", "d", driver);
		new ProductTest().declineProduct("d", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/history?locale=de");
		if(driver.getPageSource().contains("Versendete Tickets Offen")|| 
				driver.getPageSource().contains("Versendete Tickets Abgebrochen")|| 
				driver.getPageSource().contains("Erhaltende Tickets Offen")||
				driver.getPageSource().contains("Erhaltende Tickets Akzeptiert")||
				driver.getPageSource().contains("Versendete Tickets Zurückgewiesen")||
				driver.getPageSource().contains("Versendete Tickets Akzeptiert")){
			throw new IllegalArgumentException();
		}
		if(!driver.getPageSource().contains("Erhaltende Tickets Zurückgewiesen")){
			throw new IllegalArgumentException();
		}
		
		driver.manage().deleteAllCookies();
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/history?locale=de");
		if(driver.getPageSource().contains("Versendete Tickets Offen")|| 
				driver.getPageSource().contains("Erhaltende Tickets Offen")||
				driver.getPageSource().contains("Versendete Tickets Abgebrochen")||
				driver.getPageSource().contains("Erhaltende Tickets Akzeptiert")||
				driver.getPageSource().contains("Erhaltende Tickets Zurückgewiesen")||
				driver.getPageSource().contains("Versendete Tickets Akzeptiert")){
			throw new IllegalArgumentException();
		}
		if(!driver.getPageSource().contains("Versendete Tickets Zurückgewiesen")){
			throw new IllegalArgumentException();
		}
	}

	@Test
	public void TransferHistoryWithOpenProduct(){
		new LoginTest().login("m.johnki@gmx.de", "c", driver);
		new ProductTest().sendProduct("c", "m.johnki@web.de", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/history?locale=de");
		if(driver.getPageSource().contains("Erhaltende Tickets Zurückgewiesen")|| 
				driver.getPageSource().contains("Versendete Tickets Abgebrochen")|| 
				driver.getPageSource().contains("Erhaltende Tickets Offen")||
				driver.getPageSource().contains("Erhaltende Tickets Akzeptiert")||
				driver.getPageSource().contains("Versendete Tickets Zurückgewiesen")||
				driver.getPageSource().contains("Versendete Tickets Akzeptiert")){
			throw new IllegalArgumentException();
		}
		if(!driver.getPageSource().contains("Versendete Tickets Offen")){
			throw new IllegalArgumentException();
		}
		
		driver.manage().deleteAllCookies();
		new LoginTest().login("m.johnki@web.de", "d", driver);
		driver.get("http://localhost:8080/LinuxTagUserWeb/dispatcher/history?locale=de");
		if(driver.getPageSource().contains("Erhaltende Tickets Zurückgewiesen")|| 
				driver.getPageSource().contains("Versendete Tickets Abgebrochen")|| 
				driver.getPageSource().contains("Versendete Tickets Offen")||
				driver.getPageSource().contains("Erhaltende Tickets Akzeptiert")||
				driver.getPageSource().contains("Versendete Tickets Zurückgewiesen")||
				driver.getPageSource().contains("Versendete Tickets Akzeptiert")){
			throw new IllegalArgumentException();
		}
		if(!driver.getPageSource().contains("Erhaltende Tickets Offen")){
			throw new IllegalArgumentException();
		}
	}
		
	@AfterClass
	public static void clean() {
		driver.close();
	}

}
