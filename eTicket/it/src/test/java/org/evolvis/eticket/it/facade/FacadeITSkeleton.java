package org.evolvis.eticket.it.facade;

import java.util.List;

import org.evolvis.eticket.it.utils.JndiUtil;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
@RunAsClient
public class FacadeITSkeleton {

	protected static final String PROJECT_ADDRESS = "http://localhost:8080/test";

	@Deployment
	public static WebArchive createTestArchive() {
		return ShrinkWrap
				.create(WebArchive.class, "test.war")
				.addAsLibrary(JndiUtil.generateArchive())
				.addAsManifestResource("META-INF/test-persistence.xml",
						ArchivePaths.create("persistence.xml"));
	}

	protected String helpCreatePlatform(AccountDTO accDo)
			throws NumberFormatException, Exception {
		return helpCreateClientRequestWithUserPass(accDo,
				PROJECT_ADDRESS + "/service/insert/platform").put(String.class)
				.getEntity();
	}

	protected ClientRequest helpCreateClientRequestWithUserPass(
			AccountDTO accDo, String url) {
		ClientRequest clientRequest = new ClientRequest(url);
		clientRequest.formParameter("user", accDo.getUsername());
		clientRequest.formParameter("pass", accDo.getPassword());
		return clientRequest;
	}

	protected void helpCreateLocales(AccountDTO pa, long pId,
			List<LocaleDTO> supportedLocales) throws Exception {
		for (LocaleDTO l : supportedLocales)
			helpCreateLocale(pa, pId, l.getCode(), l.getName());
	}

	protected String helpCreateLocale(AccountDTO pa, long pID, String code,
			String name) throws Exception {
		return helpCreateClientRequestWithUserPass(
				pa,
				PROJECT_ADDRESS + "/service/insert/locale/" + pID + "/" + code
						+ "/" + name).put(String.class).getEntity();
	}

	protected String helpCreateSystem(AccountDTO sysAdmin, long platformId)
			throws Exception {
		return helpSetParamsForCreOrUpdSystem(sysAdmin, platformId, PROJECT_ADDRESS + "/service/insert/system/").put(String.class).getEntity();
	}

	protected ClientRequest helpSetParamsForCreOrUpdSystem(AccountDTO sysAdmin, long platformId, String serviceUrl) {
		ClientRequest clientRequest = helpCreateClientRequestWithUserPass(
				sysAdmin, serviceUrl
						+ platformId + "/" + sysAdmin.getSystem().getName());
		clientRequest.formParameter("defaultLocale", sysAdmin.getSystem()
				.getDefaultLocale().getCode());
		clientRequest.formParameter("supportedLocales",
				helpGetSupportedLocalesCodeStrings(sysAdmin.getSystem()
						.getSupportedLocales()));
		return clientRequest;
	}

	private String helpGetSupportedLocalesCodeStrings(
			List<LocaleDTO> supportedLocales) {
		String result = "";
		for (LocaleDTO l : supportedLocales) {
			if (!result.equals(""))
				result += ",";
			result += l.getCode();
		}
		return result;
	}

}
