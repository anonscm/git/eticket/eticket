package org.evolvis.eticket.it.utils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.evolvis.eticket.it.GenericIT;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.json.JSONObject;

public class JndiUtil {
	private JndiUtil() {

	}

	private static String projectName = "test";

	public static void setProjectName(String projectName) {
		JndiUtil.projectName = projectName;
	}

	public static <T> T lookup(String className) {
		@SuppressWarnings("unchecked")
		T bean = (T) lookupObject("java:global/" + projectName + "/"
				+ className);
		return bean;
	}

	private static Object lookupObject(String jndiName) {
		Context context = null;
		try {
			context = new InitialContext();
			return context.lookup(jndiName);
		} catch (NamingException ex) {
			throw new IllegalStateException("Cannot connect to bean: "
					+ jndiName + " Reason: " + ex, ex.getCause());
		} finally {
			try {
				context.close();
			} catch (NamingException ex) {
				throw new IllegalStateException(
						"Cannot close InitialContext. Reason: " + ex,
						ex.getCause());
			}
		}
	}

	public static JavaArchive generateArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "test.jar")
				.addPackages(true, "org.evolvis.eticket.annotation")
				.addPackages(true, "org.evolvis.eticket.business")
				.addPackages(true, "org.evolvis.eticket.interceptors")
				.addPackages(true, "org.evolvis.eticket.model")
				.addPackages(true, "org.evolvis.eticket.util")
				.addPackages(true, "org.evolvis.eticket.business.facades")
				.addPackages(true, "org.apache.commons")
				.addClass(JSONObject.class)
				.addClass(JndiUtil.class)
				.addClass(GenericIT.class)
				.addAsResource("project.properties")
//				.addAsResource("email.properties")
				.addAsManifestResource("META-INF/test-persistence.xml",
						ArchivePaths.create("persistence.xml"))
				.addAsManifestResource(
						"META-INF/services/org.evolvis.eticket.business.communication.Communicate",
						ArchivePaths.create("services/org.evolvis.eticket.business.communication.Communicate"));
	}
}
