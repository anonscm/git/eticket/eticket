package org.evolvis.eticket.it;

import java.util.UUID;

import javax.naming.NamingException;

import junit.framework.Assert;

import org.evolvis.eticket.it.utils.JndiUtil;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class PlatformBeanIT extends GenericIT {

	@Deployment
	public static JavaArchive createTestArchive() {
		return JndiUtil.generateArchive();
	}

	private AccountDTO platformAdmin = AccountDTO.getPlatformAccount("test",
			"test", "openID@OpenId.de");
	private LocaleDTO en = LocaleDTO.createNewLocaleDTO("en", "english");

	@Before
	public void init() throws IllegalAccessException {
		platformBeanRemote = JndiUtil.lookup("PlatformBean");
		platformBeanRemote.createPlatform(platformAdmin, PLATFORM);
	}

	@After
	public void shouldDeletePlatform() throws IllegalAccessException,
			NamingException {
		platformBeanRemote.deletePlatform(platformAdmin, PLATFORM);
	}

	@Test(expected = IllegalAccessException.class)
	public void shouldNotCreatePlatformWhenUserPasswordIsWrong()
			throws IllegalAccessException {
		// than try to gain access with a wrong password...
		platformBeanRemote.createPlatform(AccountDTO.getPlatformAccount("test",
				"est", "openID@OpenId.de"), "ARGH!");
	}

	@Test(expected = IllegalAccessException.class)
	public void shouldNotCreatePlatformWhenUsernameIsWrong()
			throws IllegalAccessException {
		// than try to gain access with a wrong username...
		platformBeanRemote.createPlatform(AccountDTO.getPlatformAccount(
				"testus", "test", "openID@OpenId.de"), "NOT!");
	}

	@Test
	public void shouldCreateLocale() throws IllegalAccessException {
		platformBeanRemote.createLocale(platformAdmin, PLATFORM, en);
		LocaleDTO dto = platformBeanRemote.getLocale("en");
		Assert.assertEquals("en", dto.getCode());
		Assert.assertEquals("english", dto.getName());
	}

	@Test
	public void shouldDeleteLocale() throws IllegalAccessException {
		platformBeanRemote.deleteLocale(platformAdmin, PLATFORM, "en");
		try {
			platformBeanRemote.getLocale("en");
			Assert.fail("get locale should have thrown a exception because en isn't there.");
		} catch (Exception e) {
			String msg = e.getMessage();
			Assert.assertEquals("No entity found for query", msg);
		}
	}

}
