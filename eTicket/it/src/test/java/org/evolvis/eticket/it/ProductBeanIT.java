package org.evolvis.eticket.it;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.ejb.EJBException;

import org.evolvis.eticket.business.product.TransferMethod;
import org.evolvis.eticket.it.utils.JndiUtil;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ProductBeanIT extends GenericIT {


	@Deployment
	public static JavaArchive createTestArchive() {
		return JndiUtil.generateArchive();//.addAsResource("email.properties");
	}

	private AccountDTO accountDTO;
	private ESystemDTO systemDTO;

	private LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO("rhh",
			"Rhoihessisch");

	private static final String SYSTEM_NAME = "Dreiser-Zitronen-Limonade";

	@Before
	public void init() throws IllegalAccessException {
		AccountDTO pA = AccountDTO.getPlatformAccount("c", "b",
				"openID@OpenId.de");
		platformBeanRemote.createPlatform(pA, PLATFORM);
		helpCreateLocale(pA);
		helpCreateEsystem(pA);
		accountDTO = AccountDTO.init(systemDTO, "c", "b",
				RoleDTO.getSysAdmin(0), "openID@OpenId.de");
	}

	private void helpCreateLocale(AccountDTO pA) {
		try {
			platformBeanRemote.createLocale(pA, PLATFORM, localeDTO);
		} catch (Exception e) {
			// I don't care if there is already a locale with the same code.
		}
	}

	public void helpCreateEsystem(AccountDTO pa) {
		List<LocaleDTO> localeDTOs = new ArrayList<LocaleDTO>();
		localeDTOs.add(localeDTO);
		systemDTO = ESystemDTO.createNewESystemDTO(SYSTEM_NAME,
				localeDTOs.get(0), localeDTOs);
		systemBeanRemote.createSystem(pa, PLATFORM, systemDTO);
	}

	private void helpCreateProductinDB() {
		ProductLocaleDTO[] productLocales = { ProductLocaleDTO
				.createNewProductLocaleDTO(localeDTO, "€", "ProductName123",
						"ProductDes123", null, 23.42, 0.19) };

		ProductDTO productDTO = ProductDTO.createNewProductDTO("TP",
				ProductType.DIGITAL, systemDTO, productLocales, 0, 20);

		productBeanRemote.create(accountDTO, productDTO);
	}

	@Test
	public void shouldCreateProduct() {
		helpCreateProduct(accountDTO);
	}

	private ProductDTO helpCreateProduct(AccountDTO acc) {
		TemplatesDTO templatesDTO = TemplatesDTO.createNewTemplatesDTO("TP",
				new byte[] { 0x23, 0x42 }, "example/test", null);

		ProductLocaleDTO[] productLocales = { ProductLocaleDTO
				.createNewProductLocaleDTO(localeDTO, "€", "TestTemplate",
						"ProductDes", templatesDTO, 23.42, 0.19) };

		ProductDTO productDTO = ProductDTO.createNewProductDTO("TP",
				ProductType.DIGITAL, systemDTO, productLocales, 0, 20);

		productBeanRemote.create(acc, productDTO);
		productDTO = productBeanRemote.get(productDTO.getSystem().getName(),
				productDTO.getUin());

		Assert.assertNotNull(productBeanRemote.get(SYSTEM_NAME, "TP"));
		return productDTO;
	}

	@Test
	public void shouldCreateProductWhenCallerIsProductAdmin()
			throws IllegalAccessException {
		AccountDTO ourProductAdmin = AccountDTO.init(systemDTO,
				"product_admin", "ImTotallySave", RoleDTO.getProductAdmin(0),
				"openID@OpenId.de");
		accountBeanRemote.create(accountDTO, ourProductAdmin);
		helpCreateProduct(ourProductAdmin);
	}

	@Test
	public void shouldDeleteProduct() {
		ProductDTO productDTO = helpCreateProduct(accountDTO);
		productBeanRemote.delete(accountDTO, productDTO);
		try {
			productBeanRemote.get(SYSTEM_NAME, productDTO.getUin());
		} catch (Exception e) {
			Assert.assertTrue(e instanceof EJBException);
			Assert.assertEquals("No entity found for query", e.getMessage());
		}
	}

	@Test
	public void shouldNotDeleteProductWhenCallerHasNotEnoughRights() {
		ProductDTO productDTO = helpCreateProduct(accountDTO);
		try {
			AccountDTO account = AccountDTO.init(systemDTO, "ehw", "hä?",
					RoleDTO.getUser(0), "openID@OpenId.de");
			accountBeanRemote.create(accountDTO, account);
			productBeanRemote.delete(account, productDTO);
			Assert.fail("A user should not be able to delete a product.");
		} catch (Exception e) {
			Assert.assertTrue(e.getCause().getCause() instanceof IllegalAccessException);
		}
	}

	@Test
	public void shouldNotCreateProductWhenCallerIsLowerThanProductAdmin() {
		AccountDTO ourModerator = AccountDTO.init(systemDTO, "moderator",
				"ImTotallySaveLikeAM0d3r4t0rShouldBe", RoleDTO.getModerator(0),
				"openID@OpenId.de");
		accountBeanRemote.create(accountDTO, ourModerator);
		try {
			helpCreateProduct(ourModerator);
			Assert.fail("should have thrown a exception, because a moderator should not be able to create products");
		} catch (Exception e) {
			// damn it arquillian
			Assert.assertTrue(e.getCause().getCause() instanceof IllegalAccessException);
		}
	}

	@Test
	public void shouldCreateTemplate() {
		helpCreateTemplate(accountDTO);
		Assert.assertNotNull(productBeanRemote.getTemplate("shiny"));
	}

	@Test
	public void shouldNotCreateTemplateWhenuserTryIt() {
		AccountDTO acc = AccountDTO.init(systemDTO, "eh", "hu",
				RoleDTO.getUser(0), "openID@OpenId.de");
		accountBeanRemote.create(accountDTO, acc);
		try {
			helpCreateTemplate(acc);
			Assert.fail("Shpuld have thrown a Exception");
		} catch (Exception e) {
			Assert.assertTrue(e.getCause().getCause() instanceof IllegalAccessException);
		}
	}

	private TemplatesDTO helpCreateTemplate(AccountDTO acc) {
		TemplatesDTO templatesDTO = TemplatesDTO
				.createNewTemplatesDTO(
						"shiny",
						new byte[] { 0x23, 0x42 },
						"example/test",
						"I'm totally shiny, and, ya, I don't care if you need sun glasses because of me");
		productBeanRemote.putTemplate(acc, templatesDTO);
		return templatesDTO;
	}

	@Test
	public void shouldCreateProductlocale() {

		TemplatesDTO templatesDTO = productBeanRemote.getTemplate("shiny");
		ProductLocaleDTO pDto = ProductLocaleDTO.createNewProductLocaleDTO(
				localeDTO,

				"€,$,Horses,Cows,Weapons,Drugs,...",
				"Shiny-New-Product-Totally-Cool-Try-It-Out-Or-Be-Out",
				"ProductDes", templatesDTO, 2323.42, 0.19);
		productBeanRemote.putProductLocale(accountDTO, pDto);
		Assert.assertNotNull(productBeanRemote.getProductLocale(
				localeDTO.getCode(), "shiny"));
	}

	@Test
	public void shouldUpdateProduct() {
		ProductDTO productDTO = helpCreateProduct(accountDTO);
		ProductDTO pdto = ProductDTO
				.createProductDTOFromExsistingProductEntity(
						productDTO.getUin(),
						ProductType.ANALOG,
						productDTO.getSystem(),
						new ProductLocaleDTO[] { ProductLocaleDTO
								.createProductLocaleDTOFromExsistingProductLocaleEntity(
										productDTO.getProductLocales()[0]
												.getId(), productDTO
												.getProductLocales()[0]
												.getLocale(), "$$",
										"TESTESTES", "12345", productDTO
												.getProductLocales()[0]
												.getTemplate(), 22.00, 0.5) },
						0, 55, productDTO.getDeadline(), productDTO
								.getExpiryTTL(), productDTO.getId());

		productBeanRemote.update(accountDTO, pdto);
		ProductDTO product = productBeanRemote.get(SYSTEM_NAME, pdto.getUin());
		Assert.assertEquals(ProductType.ANALOG, product.getProductType());
		Assert.assertEquals(55, product.getMaxAmount());
		productBeanRemote.delete(accountDTO, productDTO);
	}

	@Test
	public void shouldSetProductAutomaticTransfer() {
		ProductDTO productDTO = helpCreateProduct(accountDTO);
		productBeanRemote.transferProductByMethodcall(accountDTO, productDTO,
				TransferMethod.ACTIVATEACC, 1);
		ESystem system = eSystemCRUD.findByName(productDTO.getSystem()
				.getName());
		Product product = productCRUD.findBySystemAndUin(system,
				productDTO.getUin());
		Assert.assertNotNull(product.getPat());
		Assert.assertEquals(TransferMethod.ACTIVATEACC, product.getPat()
				.getMethod());
		Assert.assertEquals(1, product.getPat().getAmount());
	}

	@Test
	public void shouldFindProductLocaleDtoByLocalecodeProductuinAndSystemdid() {
		helpCreateProductinDB();
		ESystemDTO esdto = systemBeanRemote.getSystemByName(SYSTEM_NAME);
		Assert.assertNotNull(productBeanRemote.getProductLocale(
				localeDTO.getCode(), "TP", esdto.getId()));
	}

	@After
	public void cleanUp() {
		accountDTO = AccountDTO.init(systemDTO, "c", "b",
				RoleDTO.getSysAdmin(0), "openID@OpenId.de");
		platformBeanRemote.deletePlatform(accountDTO, PLATFORM);
	}

}
