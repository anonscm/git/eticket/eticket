//package org.evolvis.eticket.it.facade;
//
//import java.util.Arrays;
//import java.util.regex.Pattern;
//
//import junit.framework.Assert;
//
//import org.evolvis.eticket.model.entity.dto.AccountDTO;
//import org.evolvis.eticket.model.entity.dto.ESystemDTO;
//import org.evolvis.eticket.model.entity.dto.LocaleDTO;
//import org.evolvis.eticket.model.entity.dto.RoleDTO;
//import org.jboss.resteasy.client.ClientRequest;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//public class PlatformFacadeIT extends FacadeITSkeleton {
//	private AccountDTO platformAdmin = AccountDTO.getPlatformAccount("test",
//			"test", "openID@OpenId.de");
//	private long platformId = -1;
//	private ESystemDTO systemDTO = ESystemDTO.createNewESystemDTO("linuxtag",
//			LocaleDTO.createNewLocaleDTO("de", "deutsch"),
//			Arrays.asList(LocaleDTO.createNewLocaleDTO("de", "deutsch")));
//	private AccountDTO sysAdmin = AccountDTO.init(systemDTO,
//			"test", "test", RoleDTO.getSysAdmin(0), "");
//
//	@Before
//	public void init() throws NumberFormatException, Exception {
//		platformId = Long.valueOf(helpCreatePlatform(platformAdmin));
//
//		String reqParam = "?user=" + platformAdmin.getUsername() + "&pass="
//				+ platformAdmin.getPassword();
//		ClientRequest clientRequest = new ClientRequest(PROJECT_ADDRESS
//				+ "/service/insert/system/" + platformId + "/linuxtag"
//				+ reqParam);
//		clientRequest.formParameter("defaultLocale", "de");
//		clientRequest.formParameter("supportedLocales", "de");
//		clientRequest.put();
//
//		helpCreateSystem(sysAdmin, platformId);
//
//	}
//
//	@After
//	public void delete() throws Exception {
//		String reqParam = "?user=" + platformAdmin.getUsername() + "&pass="
//				+ platformAdmin.getPassword();
//		ClientRequest clientRequest = new ClientRequest(PROJECT_ADDRESS
//				+ "/service/delete/platform/" + platformId + reqParam);
//		clientRequest.delete();
//	}
//
//	@Test
//	public void shouldThrowAccessDeniedExceptionWhenTryingToCreatePlatformWithWrongUser()
//			throws Exception {
//		String result = helpCreatePlatform(AccountDTO.getPlatformAccount(
//				"test", "hahahaha", "openID@OpenId.de"));
//		// really primitive but does the trick ...
//		Pattern pattern = Pattern.compile(
//				".*user is unknown or the password is invalid.*",
//				Pattern.DOTALL | Pattern.MULTILINE);
//		Assert.assertTrue(pattern.matcher(result).matches());
//	}
//
//	@Test
//	public void shouldCreateLocale() throws Exception {
//		String result = helpCreateClientRequestWithUserPass(
//				platformAdmin,
//				PROJECT_ADDRESS + "/service/insert/locale/" + platformId
//						+ "/de/deutsch").put(String.class).getEntity();
//		Assert.assertEquals("{\"id\":-1,\"name\":\"deutsch\",\"code\":\"de\"}",
//				result);
//	}
//}
