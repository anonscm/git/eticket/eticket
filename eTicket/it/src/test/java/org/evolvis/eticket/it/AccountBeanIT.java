package org.evolvis.eticket.it;

import java.util.Arrays;

import junit.framework.Assert;

import org.evolvis.eticket.business.product.TransferMethod;
import org.evolvis.eticket.it.utils.JndiUtil;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ActivateAccountToken;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO.TextTemplateNames;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class AccountBeanIT extends GenericIT {
	private static final String USERNAME = "m.johnki@web.de";

	@Deployment
	public static JavaArchive createTestArchive() {
		return JndiUtil.generateArchive();
	}

	private AccountDTO platformAdmin = AccountDTO.getPlatformAccount("a", "b",
			"openID@OpenId.de");
	private LocaleDTO de = LocaleDTO.createNewLocaleDTO("de", "deutsch");
	private LocaleDTO en = LocaleDTO.createNewLocaleDTO("en", "english");
	private LocaleDTO locales[] = { de, en };
	private ESystemDTO systemDTO = ESystemDTO.createNewESystemDTO("testsystem",
			en, Arrays.asList(locales));
	private AccountDTO sysAdmin = AccountDTO.init(systemDTO,
			platformAdmin.getUsername(), "b", RoleDTO.getSysAdmin(0),
			"openID@OpenId.de");
	private RoleDTO role = RoleDTO.getUser(0);
	private AccountDTO accountDTO = AccountDTO.init(systemDTO, USERNAME,
			"dontcare", role, "openID@OpenId.de");
	private static boolean initialised = false;

	@Before
	public void init() throws IllegalAccessException {
		createDefaultSystem(platformAdmin, systemDTO);
		TextTemplateDTO textTemplate = TextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						en,
						TextTemplateNames.NEW_ACCOUNT,
						"Hello,\n you need to activate your new account in %system% by following the %activationlink%");
		if (!initialised) {
			textTemplateBeanService.insert(sysAdmin, textTemplate);

			configurationParameterBeanService.insertCP(sysAdmin, systemDTO,
					CPKey.SYSTEM_DOMAIN, "http://localhost:8080/test");
			configurationParameterBeanService.insertCP(sysAdmin, systemDTO,
					CPKey.SYSTEM_SENDER, "test_tarent@all-off-it.de");
			configurationParameterBeanService.insertCP(sysAdmin, systemDTO,
					CPKey.ACCOUNT_VALIDATION_LIMIT, "" + 10000);

			ProductLocaleDTO[] productLocales = { ProductLocaleDTO
					.createNewProductLocaleDTO(en, "€", "TestProduct",
							"ProductDes", null, 23.42, 0.19) };
			ProductDTO productDTO = ProductDTO.createNewProductDTO("TE",
					ProductType.DIGITAL, systemDTO, productLocales, 30, 23);

			productBeanRemote.create(sysAdmin, productDTO);
			productBeanRemote.transferProductByMethodcall(sysAdmin, productDTO,
					TransferMethod.ACTIVATEACC, 1);
			initialised = true;
		}

	}

	private void createDefaultSystem(AccountDTO platformAdmin2,
			ESystemDTO systemDTO) throws IllegalAccessException {
		insertPlatformReturnId(platformAdmin, PLATFORM);
		try {
			insertLocales(platformAdmin, PLATFORM, de, en);
		} catch (Exception e) {
			// I don't care if there is already a locale with the same code.
		}
		try {
			systemBeanRemote.getSystemByName(systemDTO.getName());
		} catch (Exception e) {
			systemBeanRemote.createSystem(platformAdmin, PLATFORM, systemDTO);
		}
	}

	// @Test
	public void shouldCreateUserAccount() {
		accountBeanRemote.createUser(accountDTO, en.getCode());
		AccountDTO acc = accountBeanRemote.find(systemDTO, USERNAME);
		Assert.assertEquals(accountDTO.getUsername(), acc.getUsername());
	}

	// @Test
	public void shouldActivateAccount() throws IllegalAccessException {
		final ESystem system = eSystemCRUD.findByName(sysAdmin.getSystem()
				.getName());
		final Account account = accountCRUD.findBySystemAndUserName(system,
				USERNAME);
		final ActivateAccountToken activateAccountToken = activateAccountTokenCRUD
				.findByAccount(account);
		Assert.assertNotNull(activateAccountToken);
		accountBeanRemote.activate(accountDTO, activateAccountToken.getToken());
		accountBeanRemote.validatePassword(accountDTO);
	}

	@Test
	public void shouldCreateUserAndActivateIt() throws IllegalAccessException {
		shouldCreateUserAccount();
		shouldActivateAccount();
	}

	@Test
	public void shouldDeleteUser() {
		AccountDTO acc = accountBeanRemote.find(systemDTO, USERNAME);
		accountBeanRemote.delete(sysAdmin, acc);
	}

	@Test
	public void shouldDeleteUserWithoutActivation() {
		shouldCreateUserAccount();
		shouldDeleteUser();
	}

	@Test
	public void shouldGerenateToken() throws IllegalAccessException {
		shouldCreateUserAndActivateIt();
		accountBeanRemote.generateToken(accountDTO);
		final ESystem system = eSystemCRUD.findByName(sysAdmin.getSystem()
				.getName());
		final Account account = accountCRUD.findBySystemAndUserName(system,
				USERNAME);
		final ActivateAccountToken activateAccountToken = activateAccountTokenCRUD
				.findByAccount(account);
		Assert.assertNotNull(activateAccountToken);
		shouldDeleteUser();
	}

	@Test
	public void shouldUpdateAccount() throws IllegalAccessException {
		shouldCreateUserAndActivateIt();
		systemDTO = systemBeanRemote.getSystemByName(systemDTO.getName());
		AccountDTO acc = AccountDTO.init(systemDTO, accountDTO.getUsername(),
				accountDTO.getPassword(), accountDTO.getRole(),
				"Sheldon.openid.org");
		accountBeanRemote.update(sysAdmin, acc);
		final ESystem system = eSystemCRUD.findByName(sysAdmin.getSystem()
				.getName());
		final Account account = accountCRUD.findBySystemAndUserName(system,
				acc.getUsername());
		Assert.assertEquals("Sheldon.openid.org", account.getCredential()
				.getOpenId());
		shouldDeleteUser();
	}

	@Test
	public void shouldValidatePassword() throws IllegalAccessException {
		shouldCreateUserAndActivateIt();
		accountBeanRemote.validatePassword(accountDTO);
		shouldDeleteUser();
	}

	@Test(expected = IllegalAccessException.class)
	public void shouldNotValidatePasswordWhenPasswordIsNotInPlaintext()
			throws IllegalAccessException {
		shouldCreateUserAndActivateIt();
		AccountDTO acc = AccountDTO.init(accountDTO.getSystem(),
				accountDTO.getUsername(), accountDTO.getPassword(),
				accountDTO.getRole(), accountDTO.getOpenId());
		accountBeanRemote.validatePassword(acc);
	}

	@Test(expected = IllegalAccessException.class)
	public void shouldNotValidatePasswordWhenPasswordIsInvalid()
			throws IllegalAccessException {
		AccountDTO acc = AccountDTO.init(accountDTO.getSystem(),
				accountDTO.getUsername(), "1234", accountDTO.getRole(),
				accountDTO.getOpenId());
		accountBeanRemote.validatePassword(acc);
	}

	@Test(expected = IllegalAccessException.class)
	public void shouldNotValidatePasswordWhenUserIsInvalid()
			throws IllegalAccessException {
		AccountDTO acc = AccountDTO.init(accountDTO.getSystem(), "a@b.org",
				accountDTO.getPassword(), accountDTO.getRole(),
				accountDTO.getOpenId());
		accountBeanRemote.validatePassword(acc);
	}

	@Test
	public void shouldUpdatePersoalInformation() {
		PersonalInformationDTO personalInformation = PersonalInformationDTO
				.createNewPersonalInformationDTO("m", "Dr.", "tarent",
						"solutions GmbH", "Rochusstraße 2-4", "53123", "Bonn",
						"Germany", en, "nothing", "a@b.org");
		accountBeanRemote.UpdatePersonalInformation(accountDTO,
				personalInformation);
		final ESystem system = eSystemCRUD.findByName(sysAdmin.getSystem()
				.getName());
		final Account account = accountCRUD.findBySystemAndUserName(system,
				accountDTO.getUsername());
		validatePersonalInformation(account.getPersonalInformation().toDto());
	}

	private void validatePersonalInformation(
			PersonalInformationDTO personalInformation) {
		Assert.assertNotNull(personalInformation);
		Assert.assertEquals("m", personalInformation.getSalutation());
		Assert.assertEquals("Dr.", personalInformation.getTitle());
		Assert.assertEquals("tarent", personalInformation.getFirstname());
		Assert.assertEquals("solutions GmbH", personalInformation.getLastname());
		Assert.assertEquals("Rochusstraße 2-4",
				personalInformation.getAddress());
		Assert.assertEquals("53123", personalInformation.getZipcode());
		Assert.assertEquals("Bonn", personalInformation.getCity());
		Assert.assertEquals("Germany", personalInformation.getCountry());
		Assert.assertEquals(en, personalInformation.getLocaleDTO());
		Assert.assertEquals("nothing", personalInformation.getOrganisation());
		Assert.assertEquals("a@b.org", personalInformation.getEmail());
	}

	@Test
	public void shouldGetPersonalInformation() {
		PersonalInformationDTO personalInformationDTO = accountBeanRemote
				.getPersonalInformationDTO(systemDTO, accountDTO.getUsername());
		validatePersonalInformation(personalInformationDTO);
	}

	@Test
	public void shouldGetActivationState() {
		boolean activated = accountBeanRemote.isActivated(systemDTO,
				accountDTO.getUsername());
		Assert.assertEquals(true, activated);
		shouldDeleteUser();

		shouldCreateUserAccount();
		activated = accountBeanRemote.isActivated(systemDTO,
				accountDTO.getUsername());
		Assert.assertEquals(false, activated);
		shouldDeleteUser();
	}

	@Test
	public void shouldFindWithOpenId() {
		shouldCreateUserAccount();
		AccountDTO account = accountBeanRemote.findWithOpenId(systemDTO,
				accountDTO.getOpenId());
		Assert.assertEquals(accountDTO.getUsername(), account.getUsername());
		shouldDeleteUser();
	}

}
