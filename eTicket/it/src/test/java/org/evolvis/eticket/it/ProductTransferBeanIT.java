package org.evolvis.eticket.it;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.evolvis.eticket.it.utils.JndiUtil;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;
import org.evolvis.eticket.model.entity.TransferHistory;
import org.evolvis.eticket.model.entity.TransferHistory.TransferState;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO.TextTemplateNames;
import org.evolvis.eticket.model.entity.dto.TransferHistoryDTO;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ProductTransferBeanIT extends GenericIT {

	private static final String USERNAME2 = "m.johnki@gmx.de"; // "EmailMuster@gmx.de";
	private static final String USERNAME = "m.johnki@web.de";
	private AccountDTO accountDTO;
	private ESystemDTO systemDTO;
	private LocaleDTO de = LocaleDTO.createNewLocaleDTO("de", "deutsch");
	private LocaleDTO en = LocaleDTO.createNewLocaleDTO("en", "english");
	private static final String SYSTEM_NAME = "Dreiser-Zitronen-Limonade";
	private AccountDTO receiver;
	private AccountDTO sender;

	@Deployment
	public static JavaArchive createTestArchive() {
		return JndiUtil.generateArchive();
	}

	@Before
	public void init() throws IllegalAccessException {
		AccountDTO pA = AccountDTO.getPlatformAccount("c", "b",
				"openID@OpenId.de");
		platformBeanRemote.createPlatform(pA, PLATFORM);
		try {
			helpCreateLocale(pA);
			helpCreateEsystem(pA);
		} catch (Exception e) {
			e.printStackTrace();
		}

		accountDTO = AccountDTO.init(systemDTO,
				pA.getUsername(), "b", RoleDTO.getSysAdmin(systemDTO.getId()),
				"");

		try {
			helpInsertTexttemplates();
			configurationParameterBeanService.insertCP(accountDTO, systemDTO,
					CPKey.SYSTEM_DOMAIN, "http://localhost:8080/test");
			configurationParameterBeanService.insertCP(accountDTO, systemDTO,
					CPKey.SYSTEM_SENDER, "test_tarent@all-off-it.de");
			configurationParameterBeanService.insertCP(accountDTO, systemDTO,
					CPKey.ACCOUNT_VALIDATION_LIMIT, "" + 10000);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			sender = helpCreateUserAccount(USERNAME);
			receiver = helpCreateUserAccount(USERNAME2);
			helpCreateProduct(accountDTO);

		} catch (Exception e) {
			e.printStackTrace();
		}

		sender = AccountDTO.init(systemDTO, USERNAME, "123",
				RoleDTO.getUser(systemDTO.getId()), null);
		receiver = AccountDTO.init(systemDTO, USERNAME2, "123",
				RoleDTO.getUser(systemDTO.getId()), null);
	}

	@Test
	public void shouldTransferSystemProductToAccount()
			throws IllegalAccessException {

		productTransferBeanRemote.transferSystemProductToAccount(accountDTO,
				helpGetProduct().toDto(), receiver, 1);
		ProductPool pool = productPoolCRUD.findByProductStatusUsername(
				helpGetProduct(), ProductPoolStatus.REGISTERED,
				receiver.getUsername());
		Assert.assertEquals(1, pool.getAmount());
	}

	@Test
	public void shouldTransferProductToAccount() throws IllegalAccessException {
		productTransferBeanRemote.transferSystemProductToAccount(accountDTO,
				helpGetProduct().toDto(), sender, 1);
		List<TransferHistoryDTO> transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesRecipient(sender.getUsername(), "OPEN");
		Assert.assertEquals(1, transferHistoryDTOs.size());
		productTransferBeanRemote.accept(sender, transferHistoryDTOs.get(0)
				.getId());
		productTransferBeanRemote.transferProductToAccount(sender,
				helpGetProduct().toDto(), receiver, 1);

		List<ProductPoolDTO> userProductPool = productPoolBeanRemote
				.getProductPoolList("REGISTERED", receiver.getUsername());
		Assert.assertEquals("1", userProductPool.get(0).getAmount());
	}

	@Test
	public void shouldAcceptProductPool() throws IllegalAccessException {
		productTransferBeanRemote.transferSystemProductToAccount(accountDTO,
				helpGetProduct().toDto(), sender, 1);
		List<ProductPoolDTO> productPoolDTOs = productPoolBeanRemote
				.getProductPoolList("VOLATILE", sender.getUsername());
		Assert.assertEquals("0", productPoolDTOs.get(0).getAmount());
		List<TransferHistoryDTO> transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesRecipient(sender.getUsername(), "OPEN");
		Assert.assertEquals(1, transferHistoryDTOs.size());
		productTransferBeanRemote.accept(sender, transferHistoryDTOs.get(0)
				.getId());

	}

	@Test
	public void shouldDeclineProductPool() throws IllegalAccessException {
		productTransferBeanRemote.transferSystemProductToAccount(accountDTO,
				helpGetProduct().toDto(), sender, 1);
		List<ProductPoolDTO> userProductPool = productPoolBeanRemote
				.getProductPoolList("VOLATILE", sender.getUsername());
		Assert.assertEquals("1", userProductPool.get(0).getAmount());
		List<TransferHistoryDTO> historyDTO = productTransferBeanRemote
				.findTransferActivitiesRecipient(sender.getUsername(), "OPEN");
		productTransferBeanRemote.decline(sender, historyDTO.get(0).getId());
		userProductPool = productPoolBeanRemote.getProductPoolList("VOLATILE",
				sender.getUsername());
		Assert.assertEquals("1", userProductPool.get(0).getAmount());
	}

	@Test
	public void shouldCancelProductPool() throws IllegalAccessException {
		productTransferBeanRemote.transferSystemProductToAccount(accountDTO,
				helpGetProduct().toDto(), sender, 1);
		List<TransferHistoryDTO> transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesRecipient(sender.getUsername(), "OPEN");
		productTransferBeanRemote.accept(sender, transferHistoryDTOs.get(0)
				.getId());

		List<ProductPoolDTO> userProductPool = productPoolBeanRemote
				.getProductPoolList("VOLATILE", sender.getUsername());
		Assert.assertEquals("2", userProductPool.get(0).getAmount());
		productTransferBeanRemote.transferProductToAccount(sender,
				helpGetProduct().toDto(), receiver, 1);

		List<TransferHistoryDTO> historyDTO = productTransferBeanRemote
				.findTransferActivitiesSender(sender.getUsername(), "OPEN");
		productTransferBeanRemote.cancel(sender, historyDTO.get(0).getId());
		userProductPool = productPoolBeanRemote.getProductPoolList("VOLATILE",
				sender.getUsername());
		Assert.assertEquals("2", userProductPool.get(0).getAmount());
	}

	@Test
	public void shouldFindTransferActivitiesSenderWithTransferStateCanceled()
			throws IllegalAccessException {
		productTransferBeanRemote.transferSystemProductToAccount(accountDTO,
				helpGetProduct().toDto(), sender, 1);
		List<TransferHistoryDTO> transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesRecipient(sender.getUsername(), "OPEN");
		List<ProductPoolDTO> userProductPool = productPoolBeanRemote
				.getProductPoolList("VOLATILE", sender.getUsername());
		Assert.assertEquals("2", userProductPool.get(0).getAmount());
		productTransferBeanRemote.accept(sender, transferHistoryDTOs.get(0)
				.getId());

		userProductPool = productPoolBeanRemote.getProductPoolList("VOLATILE",
				sender.getUsername());
		Assert.assertEquals("3", userProductPool.get(0).getAmount());
		productTransferBeanRemote.transferProductToAccount(sender,
				helpGetProduct().toDto(), receiver, 1);
		transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesSender(USERNAME,
						TransferState.CANCELED.toString());
		Assert.assertEquals(1, transferHistoryDTOs.size());
		List<TransferHistoryDTO> historyDTO = productTransferBeanRemote
				.findTransferActivitiesSender(sender.getUsername(), "OPEN");
		productTransferBeanRemote.cancel(sender, historyDTO.get(0).getId());

		transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesSender(USERNAME,
						TransferState.CANCELED.toString());
		Assert.assertEquals(2, transferHistoryDTOs.size());
	}

	@Test
	public void shouldFindTransferActivitiesRecipientWithTransferStateCanceled()
			throws IllegalAccessException {

		List<ProductPoolDTO> userProductPool = productPoolBeanRemote
				.getProductPoolList("VOLATILE", sender.getUsername());
		Assert.assertEquals("3", userProductPool.get(0).getAmount());
		productTransferBeanRemote.transferProductToAccount(sender,
				helpGetProduct().toDto(), receiver, 1);
		List<TransferHistoryDTO> transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesSender(sender.getUsername(),
						TransferState.CANCELED.toString());
		Assert.assertEquals(2, transferHistoryDTOs.size());
		transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesRecipient(receiver.getUsername(),
						TransferState.CANCELED.toString());
		Assert.assertEquals(2, transferHistoryDTOs.size());
		List<TransferHistoryDTO> historyDTO = productTransferBeanRemote
				.findTransferActivitiesSender(sender.getUsername(), "OPEN");
		productTransferBeanRemote.cancel(sender, historyDTO.get(0).getId());
		transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesRecipient(receiver.getUsername(),
						TransferState.CANCELED.toString());
		Assert.assertEquals(3, transferHistoryDTOs.size());
		userProductPool = productPoolBeanRemote.getProductPoolList("VOLATILE",
				sender.getUsername());
		Assert.assertEquals("3", userProductPool.get(0).getAmount());
	}

	@Test
	public void shouldFindTransferActivitiesSenderWithTransferStateDeclined()
			throws IllegalAccessException {
		productTransferBeanRemote.transferProductToAccount(sender,
				helpGetProduct().toDto(), receiver, 1);

		List<TransferHistoryDTO> historyDTO = productTransferBeanRemote
				.findTransferActivitiesSender(sender.getUsername(), "OPEN");
		productTransferBeanRemote.decline(receiver, historyDTO.get(0).getId());

		List<TransferHistoryDTO> transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesSender(USERNAME,
						TransferState.DECLINED.toString());
		Assert.assertEquals(1, transferHistoryDTOs.size());
	}

	@Test
	public void shouldFindTransferActivitiesRecipientWithTransferStateDeclined()
			throws IllegalAccessException {
		productTransferBeanRemote.transferProductToAccount(sender,
				helpGetProduct().toDto(), receiver, 1);

		List<TransferHistoryDTO> transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesRecipient(USERNAME2,
						TransferState.DECLINED.toString());
		Assert.assertEquals(1, transferHistoryDTOs.size());

		List<TransferHistoryDTO> historyDTO = productTransferBeanRemote
				.findTransferActivitiesSender(sender.getUsername(), "OPEN");
		productTransferBeanRemote.decline(receiver, historyDTO.get(0).getId());

		transferHistoryDTOs = productTransferBeanRemote
				.findTransferActivitiesRecipient(USERNAME2,
						TransferState.DECLINED.toString());
		Assert.assertEquals(2, transferHistoryDTOs.size());
	}

	private void helpCreateLocale(AccountDTO pA) {
		try {
			insertLocales(pA, PLATFORM, de, en);
		} catch (Exception e) {
			// I don't care if there is already a locale with the same code.
		}
	}

	private void helpInsertTexttemplates() {
		TextTemplateDTO textTemplate = TextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						en,
						TextTemplateNames.NEW_ACCOUNT,
						"Hello,\n you need to activate your new account in %system% by following the %activationlink%");
		TextTemplateDTO textTemplateDTO = TextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						en,
						TextTemplateNames.SENT_PRODUCT,
						"Invitations sent \n Hello %senderfirstname% %senderlastname%, \n You have successfully sent invitations "
								+ "or other products to the following E-Mail address: %recemail%. \n  If the recipient already has an LinuxTag "
								+ "eTicket account, he will get the products added to her/his account. \n If this is a new user, instructions "
								+ "will be sent via E-Mail how to accept the invitation. \n Please note that invitations have to be accepted within "
								+ "a specific time. If the invitations expire, they will be automatically returned to your account so you can sent them again. \n "
								+ "The invitations you sent will expire at the following date: \n %expiry% \n	Thanks, \n The LinuxTag Team");
		TextTemplateDTO textTemplateDTO2 = TextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						en,
						TextTemplateNames.RECEIVED_PRODUCT,
						"New invitations for LinuxTag Expo and Conference \n Hello, \n You got new invitations or other products for LinuxTag. "
								+ "You received them from %senderfirstname% %senderlastname%. \n The new products can now be accessed thru your "
								+ "account at:\nhttps://eticket.linuxtag.org/ \n You can use these invitations for yourself or you can send them to "
								+ "friends and co-workers. \n Thanks, \n The LinuxTag Team");
		TextTemplateDTO textTemplateDTO3 = TextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						en,
						TextTemplateNames.DECLINE_PRODUCT,
						"Neuer LinuxTag eTicket Account "
								+ "Hallo, \n \n für Sie wurde ein neuer LinuxTag eTicket-Account angelegt. Falls Sie den Account nicht selbst angelegt haben, "
								+ "bedeutet das, das Sie jemand zum LinuxTag eingeladen hat. Informationen dazu erhalten Sie in einer seperaten E-Mail. Um Ihren "
								+ "neuen Account zu nutzen, müssen sie ihn aktivieren. Klicken Sie dazu auf folgenden Aktivierungslink: \n \n %activationlink% \n \n "
								+ "Sie erhalten in den nächsten Tagen ebenfalls Einladungen, die Sie an Freunde oder Bekannte weitersenden können.\n \n Wenn Sie "
								+ "Fragen haben, helfen wir Ihnen gerne unter tickets@linuxtag.org weiter. \n \n Ihr \n LinuxTag Team");
		TextTemplateDTO textTemplateDTO4 = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, en,
						TextTemplateNames.CANCELED_PRODUCT,
						"Hello, \n \n Sie haben eine ProduktTransaktion "
								+ "abgebrochen ");
		TextTemplateDTO textTemplateDTO5 = TextTemplateDTO
				.createNewTextTemplateDTO(systemDTO, en,
						TextTemplateNames.RECEIVED_PRODUCT_BY_SYSTEM,
						"Hello, \n \n Sie haben eine ProduktTransaktion "
								+ "abgebrochen ");
		textTemplateBeanService.insert(accountDTO, textTemplate);
		textTemplateBeanService.insert(accountDTO, textTemplateDTO);
		textTemplateBeanService.insert(accountDTO, textTemplateDTO2);
		textTemplateBeanService.insert(accountDTO, textTemplateDTO3);
		textTemplateBeanService.insert(accountDTO, textTemplateDTO4);
		textTemplateBeanService.insert(accountDTO, textTemplateDTO5);
	}

	private void helpCreateEsystem(AccountDTO pa) {
		List<LocaleDTO> localeDTOs = new ArrayList<LocaleDTO>();
		localeDTOs.add(en);
		systemDTO = ESystemDTO.createNewESystemDTO(SYSTEM_NAME,
				localeDTOs.get(0), localeDTOs);
		try {
			systemBeanRemote.getSystemByName(systemDTO.getName());
		} catch (Exception e) {
			systemBeanRemote.createSystem(pa, PLATFORM, systemDTO);
		}
	}

	private Product helpGetProduct() {
		Product product = null;

		try {
			product = productCRUD.findBySystemAndUin(
					eSystemCRUD.findByName(accountDTO.getSystem().getName()),
					"TP");
		} catch (Exception e) {
		}

		if (product != null) {
			return product;
		} else {
			return helpCreateProduct(accountDTO);
		}
	}

	private Product helpCreateProduct(AccountDTO acc) {
		TemplatesDTO templatesDTO = TemplatesDTO.createNewTemplatesDTO("TP",
				new byte[] { 0x23, 0x42 }, "example/test", null);

		ProductLocaleDTO[] productLocales = { ProductLocaleDTO
				.createNewProductLocaleDTO(en, "€", "TestTemplate",
						"ProductDes", templatesDTO, 23.42, 0.19) };
		ProductDTO productDTO = ProductDTO.createNewProductDTO("TP",
				ProductType.DIGITAL, systemDTO, productLocales, 30, 20);

		productBeanRemote.create(acc, productDTO);
		final ESystem system = eSystemCRUD.findByName(accountDTO.getSystem()
				.getName());
		return productCRUD.findBySystemAndUin(system, productDTO.getUin());
	}

	private AccountDTO helpCreateUserAccount(String username) {
		AccountDTO accountDTO = AccountDTO.init(systemDTO,
				username, "123", RoleDTO.getUser(systemDTO.getId()), null);
		accountBeanRemote.createUser(accountDTO, en.getCode());
		return accountDTO;
	}

	private void deleteProductPools() {
		List<ProductPool> userProductPool = productPoolCRUD
				.findByStatusAndUsername(ProductPoolStatus.REGISTERED,
						receiver.getUsername());
		for (ProductPool productPool : userProductPool) {
			productPoolCRUD.delete(productPool);
		}
		userProductPool = productPoolCRUD.findByStatusAndUsername(
				ProductPoolStatus.REGISTERED, sender.getUsername());
		for (ProductPool productPool : userProductPool) {
			productPoolCRUD.delete(productPool);
		}
		userProductPool = productPoolCRUD.findByStatusAndUsername(
				ProductPoolStatus.FIXED, receiver.getUsername());
		for (ProductPool productPool : userProductPool) {
			productPoolCRUD.delete(productPool);
		}
		userProductPool = productPoolCRUD.findByStatusAndUsername(
				ProductPoolStatus.FIXED, sender.getUsername());
		for (ProductPool productPool : userProductPool) {
			productPoolCRUD.delete(productPool);
		}
		userProductPool = productPoolCRUD.findByStatusAndUsername(
				ProductPoolStatus.VOLATILE, receiver.getUsername());
		for (ProductPool productPool : userProductPool) {
			productPoolCRUD.delete(productPool);
		}
		userProductPool = productPoolCRUD.findByStatusAndUsername(
				ProductPoolStatus.VOLATILE, sender.getUsername());
		for (ProductPool productPool : userProductPool) {
			productPoolCRUD.delete(productPool);
		}
	}

	private void deleteTransferHistory() {
		List<TransferHistory> transferHistorys = transferHistoryCRUD
				.findByRecipientUsernameAndTransferState(sender.getUsername(),
						TransferState.ACCEPTED);
		for (TransferHistory transferHistory : transferHistorys) {
			transferHistoryCRUD.delete(transferHistory);
		}
		transferHistorys = transferHistoryCRUD
				.findByRecipientUsernameAndTransferState(
						receiver.getUsername(), TransferState.ACCEPTED);
		for (TransferHistory transferHistory : transferHistorys) {
			transferHistoryCRUD.delete(transferHistory);
		}
		transferHistorys = transferHistoryCRUD
				.findByRecipientUsernameAndTransferState(sender.getUsername(),
						TransferState.CANCELED);
		for (TransferHistory transferHistory : transferHistorys) {
			transferHistoryCRUD.delete(transferHistory);
		}
		transferHistorys = transferHistoryCRUD
				.findByRecipientUsernameAndTransferState(
						receiver.getUsername(), TransferState.CANCELED);
		for (TransferHistory transferHistory : transferHistorys) {
			transferHistoryCRUD.delete(transferHistory);
		}
		transferHistorys = transferHistoryCRUD
				.findByRecipientUsernameAndTransferState(sender.getUsername(),
						TransferState.DECLINED);
		for (TransferHistory transferHistory : transferHistorys) {
			transferHistoryCRUD.delete(transferHistory);
		}
		transferHistorys = transferHistoryCRUD
				.findByRecipientUsernameAndTransferState(
						receiver.getUsername(), TransferState.DECLINED);
		for (TransferHistory transferHistory : transferHistorys) {
			transferHistoryCRUD.delete(transferHistory);
		}
		transferHistorys = transferHistoryCRUD
				.findByRecipientUsernameAndTransferState(sender.getUsername(),
						TransferState.OPEN);
		for (TransferHistory transferHistory : transferHistorys) {
			transferHistoryCRUD.delete(transferHistory);
		}
		transferHistorys = transferHistoryCRUD
				.findByRecipientUsernameAndTransferState(
						receiver.getUsername(), TransferState.OPEN);
		for (TransferHistory transferHistory : transferHistorys) {
			transferHistoryCRUD.delete(transferHistory);
		}
	}

	@After
	public void clean() {
		try {
			accountBeanRemote.delete(accountDTO, receiver);
			accountBeanRemote.delete(accountDTO, sender);
			deleteTransferHistory();
			deleteProductPools();
			systemBeanRemote.deleteSystem(accountDTO, PLATFORM, systemDTO);
			platformBeanRemote.deleteLocale(accountDTO, PLATFORM, "de");
			platformBeanRemote.deleteLocale(accountDTO, PLATFORM, "en");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
