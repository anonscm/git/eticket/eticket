package org.evolvis.eticket.it;

import java.util.Arrays;

import javax.ejb.EJBException;

import junit.framework.Assert;

import org.evolvis.eticket.it.utils.JndiUtil;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductNumberDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO.TextTemplateNames;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ProductPoolBeanIT extends GenericIT {

	@Deployment
	public static JavaArchive createTestArchive() {
		return JndiUtil.generateArchive();
	}

	private AccountDTO platformAdmin = AccountDTO.getPlatformAccount("a", "b",
			"openID@OpenId.de");
	private LocaleDTO de = LocaleDTO.createNewLocaleDTO("de", "deutsch");
	private LocaleDTO en = LocaleDTO.createNewLocaleDTO("en", "english");
	private LocaleDTO locales[] = { de, en };
	private ESystemDTO systemDTO = ESystemDTO.createNewESystemDTO("testsystem",
			en, Arrays.asList(locales));
	private AccountDTO accountDTO;
	private AccountDTO sysAdmin;

	@Before
	public void init() throws IllegalAccessException {
		insertPlatformReturnId(platformAdmin, PLATFORM);
		insertLocales(platformAdmin, PLATFORM, de, en);
		systemBeanRemote.createSystem(platformAdmin, PLATFORM, systemDTO);
		systemDTO = systemBeanRemote.getSystemByName(systemDTO.getName());
		
		TextTemplateDTO textTemplate = TextTemplateDTO
				.createNewTextTemplateDTO(
						systemDTO,
						en,
						TextTemplateNames.NEW_ACCOUNT,
						"Hello,\n you need to activate your new account in %system% by following the %activationlink%");

		accountDTO = AccountDTO.init(systemDTO,
				"m.johnki@web.de", "1234", RoleDTO.getUser(0), "openid.org/a");
		sysAdmin = AccountDTO.init(systemDTO,
				platformAdmin.getUsername(), "b", RoleDTO.getSysAdmin(0),
				"openID@OpenId.de");
		textTemplateBeanService.insert(sysAdmin, textTemplate);
		configurationParameterBeanService.insertCP(sysAdmin, systemDTO,
				CPKey.SYSTEM_DOMAIN, "http://localhost:8080/test");
		configurationParameterBeanService.insertCP(sysAdmin, systemDTO,
				CPKey.SYSTEM_SENDER, "test_tarent@all-off-it.de");
		configurationParameterBeanService.insertCP(sysAdmin, systemDTO,
				CPKey.ACCOUNT_VALIDATION_LIMIT, "" + 10000);
		accountBeanRemote.createUser(accountDTO, "en");

	}

	@Test
	public void shouldGetProductPoolList() {
		helpCeratePool(ProductPoolStatus.VOLATILE);
		ProductPoolDTO productPoolDTO = productPoolBeanRemote
				.getProductPoolList("VOLATILE", accountDTO.getUsername())
				.get(0);
		Assert.assertEquals("42", productPoolDTO.getAmount());
		Assert.assertEquals("TP", productPoolDTO.getProductDTO().getUin());
	}

	@Test(expected = EJBException.class)
	public void shouldNotGetProductPoolList() {
		helpCeratePool(ProductPoolStatus.VOLATILE);
		productPoolBeanRemote.getProductPoolList("VOLATILE1",
				accountDTO.getUsername()).get(0);
	}

	@Test
	public void shouldFixProduct() {
		ProductPoolDTO productPoolDTO = helpCeratePool(
				ProductPoolStatus.VOLATILE).toDto();
		final ESystem system = eSystemCRUD.findByName(sysAdmin.getSystem()
				.getName());
		final Account account = accountCRUD.findBySystemAndUserName(system,
				accountDTO.getUsername());
		Product product = productCRUD.findBySystemAndUin(system, productPoolDTO
				.getProductDTO().getUin());
		ProductNumberDTO pn = ProductNumberDTO.createNewProductNumberDTO(100,
				200, 1, product.toDto());
		productNumberBeanRemote.cerateProductNumber(sysAdmin, pn);
		productPoolBeanRemote.fixProduct(account.toAccountDTO(),
				productPoolDTO.getProductDTO());
		ProductPool productPool = productPoolCRUD.findByProductStatusActor(
				product, ProductPoolStatus.FIXED, account);
		Assert.assertNotNull(productPool);
		productNumberBeanRemote.deleteProductNumber(sysAdmin, pn);
	}

	private Product helpCreateProduct(AccountDTO acc) {
		TemplatesDTO templatesDTO = TemplatesDTO.createNewTemplatesDTO("TP",
				new byte[] { 0x23, 0x42 }, "example/test", null);

		ProductLocaleDTO[] productLocales = { ProductLocaleDTO
				.createNewProductLocaleDTO(en, "€", "TestTemplate",
						"ProductDes", templatesDTO, 23.42, 0.19) };

		ProductDTO productDTO = ProductDTO.createNewProductDTO("TP",
				ProductType.DIGITAL, systemDTO, productLocales, 0, 20);

		productBeanRemote.create(acc, productDTO);

		final ESystem system = eSystemCRUD.findByName(sysAdmin.getSystem()
				.getName());
		return productCRUD.findBySystemAndUin(system, productDTO.getUin());
	}

	private ProductPool helpCeratePool(ProductPoolStatus status) {
		final ESystem system = eSystemCRUD.findByName(sysAdmin.getSystem()
				.getName());
		final Account account = accountCRUD.findBySystemAndUserName(system,
				accountDTO.getUsername());
		ProductPool productPool = new ProductPool();
		productPool.setActor(account);
		productPool.setAmount(42);
		productPool.setProduct(helpCreateProduct(sysAdmin));
		productPool.setType(status);
		productPoolCRUD.insert(productPool);
		return productPool;
	}

	@After
	public void cleanUp() throws IllegalAccessException {
		accountBeanRemote.delete(sysAdmin, accountDTO);
		sysAdmin = AccountDTO.init(systemDTO,
				platformAdmin.getUsername(), "b",
				RoleDTO.getSysAdmin(0), "openID@OpenId.de");
		systemBeanRemote.deleteSystem(sysAdmin, PLATFORM, systemDTO);
		platformBeanRemote.deleteLocale(sysAdmin, PLATFORM, "de");
		platformBeanRemote.deleteLocale(sysAdmin, PLATFORM, "en");
		platformBeanRemote.deletePlatform(sysAdmin, PLATFORM);
	}

}
