package org.evolvis.eticket.it;

import java.util.UUID;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.platform.PlatformBeanService;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.ProductNumberBeanService;
import org.evolvis.eticket.business.product.ProductPoolBeanService;
import org.evolvis.eticket.business.product.ProductTransferService;
import org.evolvis.eticket.business.system.ConfigurationParameterBeanService;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.business.system.TextTemplateBeanService;
import org.evolvis.eticket.it.utils.JndiUtil;
import org.evolvis.eticket.model.crud.platform.PlatformCRUD;
import org.evolvis.eticket.model.crud.product.ProductCRUD;
import org.evolvis.eticket.model.crud.product.ProductPoolCRUD;
import org.evolvis.eticket.model.crud.product.TransferHistoryCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.crud.user.ActivateAccountTokenCRUD;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.junit.Before;

public class GenericIT {
	String PLATFORM = UUID.randomUUID().toString()
			.toString();
	protected SystemBeanService systemBeanRemote;
	protected PlatformBeanService platformBeanRemote;
	protected ProductBeanService productBeanRemote;
	protected AccountBeanService accountBeanRemote;
	protected ProductPoolBeanService productPoolBeanRemote;
	protected TextTemplateBeanService textTemplateBeanService;
	protected ConfigurationParameterBeanService configurationParameterBeanService;
	protected ProductTransferService productTransferBeanRemote;
	protected ProductNumberBeanService productNumberBeanRemote;
	protected ESystemCRUD eSystemCRUD;
	protected AccountCRUD accountCRUD;
	protected ActivateAccountTokenCRUD activateAccountTokenCRUD;
	protected PlatformCRUD platformCRUD;
	protected ProductCRUD productCRUD;
	protected ProductPoolCRUD productPoolCRUD;
	protected TransferHistoryCRUD transferHistoryCRUD;

	@Before
	public void aSetOfEjbs() {
		platformBeanRemote = JndiUtil.lookup("PlatformBean");
		systemBeanRemote = JndiUtil.lookup("SystemBean");
		productBeanRemote = JndiUtil.lookup("ProductBean");
		accountBeanRemote = JndiUtil.lookup("AccountBean");
		textTemplateBeanService = JndiUtil.lookup("TextTemplateBean");
		configurationParameterBeanService = JndiUtil
				.lookup("ConfigurationParameterBean");
		productPoolBeanRemote = JndiUtil.lookup("ProductPoolBean");
		productTransferBeanRemote = JndiUtil.lookup("ProductTransferBean");
		productNumberBeanRemote = JndiUtil.lookup("ProductNumberBean");
		eSystemCRUD = JndiUtil.lookup("ESystemCRUDBean");
		accountCRUD = JndiUtil.lookup("AccountCRUDBean");
		activateAccountTokenCRUD = JndiUtil
				.lookup("ActivateAccountTokenCRUDBean");
		platformCRUD = JndiUtil.lookup("PlatformCRUDBean");
		productCRUD = JndiUtil.lookup("ProductCRUDBean");
		productPoolCRUD = JndiUtil.lookup("ProductPoolCRUDBean");
		transferHistoryCRUD = JndiUtil.lookup("TransferHistoryCRUDBean");
	}

	protected void insertPlatformReturnId(AccountDTO platformAdmin, String name)
			throws IllegalAccessException {
		platformBeanRemote.createPlatform(platformAdmin, name);
	}

	protected void insertLocales(AccountDTO platformAdmin, String platformId,
			LocaleDTO... localeDTOs) {

		for (LocaleDTO l : localeDTOs) {
			platformBeanRemote.createLocale(platformAdmin, platformId, l);
		}

	}
}
