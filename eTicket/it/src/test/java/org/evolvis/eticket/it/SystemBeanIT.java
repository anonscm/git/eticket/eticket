package org.evolvis.eticket.it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJBException;
import javax.naming.NamingException;

import junit.framework.Assert;

import org.evolvis.eticket.it.utils.JndiUtil;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class SystemBeanIT extends GenericIT {// extends ServiceManagement {

	@Deployment
	public static JavaArchive createTestArchive() {
		return JndiUtil.generateArchive();
	}

	private AccountDTO accountDTO = AccountDTO.getPlatformAccount("c", "b",
			"openID@OpenId.de");

	private LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO("rhh",
			"Rheinhessisch");

	private static final String SYSTEM_NAME = "Dreiser-Zitronen-Limonade";

	private boolean insertSystem = false;

	@Before
	public void prepare() throws IllegalAccessException, NamingException,
			IllegalArgumentException, InvocationTargetException {
		platformBeanRemote.createPlatform(accountDTO, PLATFORM);
		platformBeanRemote.createLocale(accountDTO, PLATFORM, localeDTO);
	}

	@Test
	public void createSystem() {
		List<LocaleDTO> localeDTOs = new ArrayList<LocaleDTO>();
		localeDTOs.add(localeDTO);
		ESystemDTO systemDTO = ESystemDTO.createNewESystemDTO(SYSTEM_NAME,
				localeDTOs.get(0), localeDTOs);
		systemBeanRemote.createSystem(accountDTO, PLATFORM, systemDTO);
		insertSystem = true;
	}

	@Test
	public void compositeGetSystemByName() {
		createSystem();
		ESystemDTO systemDTO = systemBeanRemote.getSystemByName(SYSTEM_NAME);
		assertNotNull(systemDTO);
		assertEquals(SYSTEM_NAME, systemDTO.getName());
	}

	@Test(expected = EJBException.class)
	public void failGetSystemNothingGotPersisted() {
		systemBeanRemote.getSystemByName(SYSTEM_NAME);
	}

	@Test
	public void compositeUpdateSystem() {
		compositeGetSystemByName();
		ESystemDTO oldSystem = systemBeanRemote.getSystemByName(SYSTEM_NAME);
		ESystemDTO systemDTO = ESystemDTO
				.createESystemDTOFromExsistingESystemEntity(oldSystem.getId(),
						"UpdateJoar", oldSystem.getDefaultLocale(),
						oldSystem.getSupportedLocales());
		systemBeanRemote.updateSystem(accountDTO, PLATFORM, systemDTO);
		systemDTO = systemBeanRemote.getSystemByName("UpdateJoar");
		// assertNotNull(systemDTO);
		assertEquals("UpdateJoar", systemDTO.getName());
		// set name back for delete ...
		systemBeanRemote.updateSystem(accountDTO, PLATFORM, oldSystem);
	}

	@Test
	public void shouldGetSystemById() {
		compositeGetSystemByName();
		ESystemDTO eSystemDTO = systemBeanRemote.getSystemByName(SYSTEM_NAME);
		ESystemDTO systemDTO = systemBeanRemote.getSystemById(eSystemDTO
				.getId());
		Assert.assertNotNull(systemDTO);
	}

	@After
	public void deleteSystem() {
		if (insertSystem) {
			systemBeanRemote.deleteSystem(accountDTO, PLATFORM,
					systemBeanRemote.getSystemByName(SYSTEM_NAME));
			try {
				systemBeanRemote.getSystemByName(SYSTEM_NAME);
				fail("Hum...they fought back: failed to destroy the system.");
			} catch (EJBException e) {
				// fight the establishment: successfully destroyed the system,
				// everything is shiny.
			}
		}
		platformBeanRemote.deleteLocale(accountDTO, PLATFORM,
				localeDTO.getCode());
	}
}
