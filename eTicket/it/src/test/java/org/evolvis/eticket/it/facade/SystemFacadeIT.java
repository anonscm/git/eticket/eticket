//package org.evolvis.eticket.it.facade;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//
//import junit.framework.Assert;
//
//import org.evolvis.eticket.model.entity.dto.AccountDTO;
//import org.evolvis.eticket.model.entity.dto.ESystemDTO;
//import org.evolvis.eticket.model.entity.dto.LocaleDTO;
//import org.evolvis.eticket.model.entity.dto.RoleDTO;
//import org.jboss.resteasy.client.ClientRequest;
//import org.json.JSONObject;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//public class SystemFacadeIT extends FacadeITSkeleton {
//	private static Long platformId = -1L;
//	private AccountDTO platformAdmin = AccountDTO.getPlatformAccount("test",
//			"test", "openID@OpenId.de");
//	private ESystemDTO eSystemDTO;
//
//	@Before
//	public void init() throws NumberFormatException, Exception {
//		List<LocaleDTO> supportedLocales = helpInitEsystemDto();
//		// create a platform.
//		platformId = Long.valueOf(helpCreatePlatform(platformAdmin));
//		// create locales
//		helpCreateLocales(platformAdmin, platformId, supportedLocales);
//
//	}
//
//	private List<LocaleDTO> helpInitEsystemDto() {
//		List<LocaleDTO> supportedLocales = new ArrayList<LocaleDTO>();
//		supportedLocales.add(LocaleDTO.createNewLocaleDTO("de", "deutsch"));
//		supportedLocales.add(LocaleDTO.createNewLocaleDTO("en", "english"));
//		eSystemDTO = ESystemDTO.createNewESystemDTO(UUID.randomUUID().toString(),
//				supportedLocales.get(0), supportedLocales);
//		return supportedLocales;
//	}
//
//	private ClientRequest helpSetRequestToUpdSystem(AccountDTO sysAdmin)
//			throws Exception {
//		ClientRequest clientRequest = helpSetParamsForCreOrUpdSystem(sysAdmin,
//				platformId, PROJECT_ADDRESS + "/service/update/system/");
//		return clientRequest;
//	}
//
//	@Test
//	public void shouldCreateASystem() throws Exception {
//		String result = helpCreateSystem(
//				AccountDTO.createNewAccountDTO(eSystemDTO,
//						platformAdmin.getUsername(), "test",
//						RoleDTO.getSysAdmin(0), "openID@OpenId.de"), platformId);
//		Assert.assertEquals(null, result);
//		helpGetSystemAndCheck(eSystemDTO);
//	}
//
//	private void helpGetSystemAndCheck(ESystemDTO espected) throws Exception {
//		String result = helpGetSystem(espected.getName());
//		JSONObject jsonObject = new JSONObject(result);
//		JSONObject object = jsonObject.getJSONObject("defaultLocale");
//		ESystemDTO eSystemDTO = ESystemDTO
//				.createESystemDTOFromExsistingESystemEntity(jsonObject
//						.getLong("id"), jsonObject.getString("name"), LocaleDTO
//						.createNewLocaleDTO(object.getString("code"),
//								object.getString("name")), null);
//		Assert.assertTrue(espected.getName().equals(eSystemDTO.getName()));
//		Assert.assertTrue(espected.getDefaultLocale().equals(
//				eSystemDTO.getDefaultLocale()));
//	}
//
//	private String helpGetSystem(String systemName) throws Exception {
//		ClientRequest clientRequest;
//		clientRequest = new ClientRequest(PROJECT_ADDRESS
//				+ "/service/get/system/" + systemName);
//		String result = clientRequest.get(String.class).getEntity();
//		return result;
//	}
//
//	@Test
//	public void shouldUpdateSystemWithoutId() throws Exception {
//		shouldCreateASystem();
//		eSystemDTO = ESystemDTO
//				.createNewESystemDTO("TestSystem", eSystemDTO
//						.getSupportedLocales().get(1), eSystemDTO
//						.getSupportedLocales());
//		ClientRequest clientRequest = helpSetRequestToUpdSystem(AccountDTO
//				.createNewAccountDTO(eSystemDTO, platformAdmin.getUsername(),
//						"test", RoleDTO.getSysAdmin(0), "openID@OpenId.de"));
//		clientRequest.post();
//		helpGetSystemAndCheck(eSystemDTO);
//	}
//
//	@Test
//	public void shouldUpdateSystemWithId() throws Exception {
//		String result = helpCreateSystem(
//				AccountDTO.createNewAccountDTO(eSystemDTO,
//						platformAdmin.getUsername(), "test",
//						RoleDTO.getSysAdmin(0), "openID@OpenId.de"), platformId);
//		Assert.assertEquals(null, result);
//		JSONObject jsonObject = new JSONObject(
//				helpGetSystem(eSystemDTO.getName()));
//		eSystemDTO = ESystemDTO
//				.createNewESystemDTO("SystemTest", eSystemDTO
//						.getSupportedLocales().get(1), eSystemDTO
//						.getSupportedLocales());
//		ClientRequest clientRequest = helpSetRequestToUpdSystem(AccountDTO
//				.createNewAccountDTO(eSystemDTO, platformAdmin.getUsername(),
//						"test", RoleDTO.getSysAdmin(0), "openID@OpenId.de"));
//		clientRequest.formParameter("systemID", jsonObject.getLong("id"));
//		clientRequest.post();
//		helpGetSystemAndCheck(eSystemDTO);
//	}
//
//	@After
//	public void cleanUp() throws Exception {
////		String reqParam = "?user=" + platformAdmin.getUsername() + "&pass="
////				+ "test";
////		ClientRequest clientRequest = new ClientRequest(PROJECT_ADDRESS
////				+ "/service/delete/platform/" + platformId + reqParam);
////		clientRequest.delete();
//	}
//
//}
