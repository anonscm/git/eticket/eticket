package org.evolvis.eticket.config;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

import org.junit.Test;

public class Parser {

	ConfigurationFileParser cfp = new ConfigurationFileParser(ClassLoader.getSystemResource("LinuxTag.xml").toString());
//			"/home/phil/workspace/eticket/eTicket/core/init/src/test/resources/LinuxTag.xml");

	@Test
	public void shouldLoadConfigFile() {
		assertNotNull(cfp);
	}

	@Test
	public void shouldParseConfigFile() {
		assertThat("one Happy Vertical People Transporter", is(" unhappy"));
	}

	@Test
	public void shouldGenerateNeededObjects() {
		assertThat("one Happy Vertical People Transporter", is(" unhappy"));
	}
}
