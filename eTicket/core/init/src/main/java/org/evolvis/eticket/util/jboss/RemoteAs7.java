package org.evolvis.eticket.util.jboss;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

//TODO need to add Stateful?
public class RemoteAs7 {

	static RemoteAs7 instance = new RemoteAs7();
	static String moduleName = "eTicketBundle";

	public static void setModuleName(String moduleName) {
		RemoteAs7.moduleName = moduleName;
	}

	public static Object lookup(final String beanName, final Class<?> clazz) {
		try {
			return instance.lookupStateless("", moduleName, "", beanName,
					clazz.getName());
		} catch (NamingException e) {
			throw new IllegalStateException("Configurations correct?", e);
		}
	}

	private Object lookupStateless(final String appName,
			final String moduleName, final String distinctName,
			final String beanName, final String viewClassName)
			throws NamingException {
		final Properties jndiProperties = new Properties();
		jndiProperties.put(Context.URL_PKG_PREFIXES,
				"org.jboss.ejb.client.naming");
		Context context = new InitialContext(jndiProperties);
		return context.lookup(genereteLookup(appName, moduleName, distinctName,
				beanName, viewClassName));
	}

	private String genereteLookup(final String appName,
			final String moduleName, final String distinctName,
			final String beanName, final String viewClassName) {

		return "ejb:" + appName + "/" + moduleName + "/" + distinctName + "/"
				+ beanName + "!" + viewClassName;
	}

}
