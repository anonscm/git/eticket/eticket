package org.evolvis.eticket.util;

import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.internal.Lists;

public class Arguments {
	@Parameter
	List<String> parameters = Lists.newArrayList();

	@Parameter(names = { "--config", "-c" }, description = "The path to the configfile.", required = true)
	String configFilePath = "";

}
