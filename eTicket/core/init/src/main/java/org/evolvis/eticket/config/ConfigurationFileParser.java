package org.evolvis.eticket.config;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ConfigurationFileParser {

	private final Document configurationFile;

	public ConfigurationFileParser(String path) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			configurationFile = loadconfigFile(path, dbf);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	private Document loadconfigFile(String path, DocumentBuilderFactory dbf)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilder db = dbf.newDocumentBuilder();
		return db.parse(path);
	}

}
