package org.evolvis.eticket.business.product;

import java.util.HashMap;
import java.util.Map;

public enum TransferMethod {
	ACTIVATEACC("org.evolvis.eticket.business.system.account.AccountBean",
			"activate");

	private String fqnMethod;

	private TransferMethod(String clazz, String method) {
		fqnMethod = clazz + "." + method;
	}

	@Override
	public String toString() {
		return fqnMethod;
	}

	private static final Map<String, TransferMethod> stringToEnum = new HashMap<String, TransferMethod>();
	static {// initialise map from const name to enum const
		for (TransferMethod tP : values())
			stringToEnum.put(tP.toString(), tP);
	}

	public static TransferMethod getTransferMethod(Class<?> clazz, String method) {
		return fromString(getFqn(clazz, method));
	}

	private static String getFqn(Class<?> clazz, String method) {
		return clazz.getName() + "." + method;
	}

	public static TransferMethod fromString(String fqn) {
		return stringToEnum.get(fqn);
	}
}
