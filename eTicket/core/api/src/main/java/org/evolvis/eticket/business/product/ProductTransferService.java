package org.evolvis.eticket.business.product;

import java.util.List;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;
import org.evolvis.eticket.model.entity.dto.TransferHistoryDTO;

public interface ProductTransferService {
	/**
	 * transfers a product directly from the system to a account.
	 * 
	 * @param sender
	 *            the account of the caller.
	 * @param product
	 *            the product to send.
	 * @param receiver
	 *            the one who becomes the product.
	 * @param amount
	 *            the amount of the product which the receiver becomes.
	 */
	public void transferSystemProductToAccount(AccountDTO sender,
			ProductDTO product, AccountDTO receiver, long amount);
	
	/**
	 * 
	 * transfers a product directly from the system to a account and automatically accept product.
	 * 
	 * @param product
	 *            the product to send.
	 * @param receiver
	 *            the one who becomes the product.
	 * @param amount
	 *            the amount of the product which the receiver becomes.
	 */
	public void transferOrderedSystemProductToAccountAndAccept(ProductDTO product, AccountDTO receiver, long amount);

	public void transferProductToAccount(AccountDTO sender, ProductDTO product,
			AccountDTO receiver, long amount);

	public void accept(AccountDTO sender, long id);

	public void decline(AccountDTO sender, long id);

	public void cancel(AccountDTO sender, long id);
	
	/**
	 * 
	 * @param senderUsername
	 * 			the username of the Sender
	 * @param state
	 * 			the state of the Transfer, it can be OPEN, CANCELED, DECLINED or ACCEPTED 
	 * @return	a List of TransferHistoryDTOs 
	 * 			
	 */
	public List<TransferHistoryDTO> findTransferActivitiesSender(String senderUsername, String state);
	
	/**
	 * deletes the amount of the pool 
	 * 
	 * @param admin
	 *            should be a admin
	 * @param poolDTO
	 *            the ProductPoolDTO to update
	 * @param amount
	 *            the amount
	 */
	public void deleteopentickets(AccountDTO admin, ProductPoolDTO poolDTO,int amount);
	
	
	/**
	 * deletes the pool by the history 
	 * 
	 * @param admin
	 *            should be a admin
	 * @param sender
	 *            the sender, usersaccount
	 * @param id
	 *          history id
	 */
	public void declinebyadmin(AccountDTO admin,AccountDTO sender, long id);
	
	 public List<TransferHistoryDTO> findTransferActivitiesRecipient(String recipientUsername, String state);

}
