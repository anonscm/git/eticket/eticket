package org.evolvis.eticket.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.evolvis.eticket.model.entity.dto.AccountDTO;

public class CalculateHash {

	private static CalculateHash instance = new CalculateHash();

	private CalculateHash() {
	}

	public static String calculate(String input) {
		return instance.calculateHash(null, input);
	}

	private static String generateCheckForFurtherUnderstandmentMsg(String input) {
		return "An error occured while trying to calculate the hash value of "
				+ input + ", please check "
				+ CalculateHash.class.getSimpleName()
				+ " for further unterstandment.";
	}

	private static final String CHARSET_NAME = "UTF-8";
	public static final int ALGOR_BIT_SIZE = 512;
	private static final String ALGORITHM = "SHA-" + ALGOR_BIT_SIZE;

	private String calculateHash(String salt, String input) {
		try {
			return doCalculateHash(salt, input);
		} catch (Exception e) {
			throw new IllegalArgumentException(
					generateCheckForFurtherUnderstandmentMsg(input), e);
		}
	}

	private String doCalculateHash(String salt, String input)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = prepareDigestWithSalt(salt);
		byte[] hash = md.digest(input.getBytes(CHARSET_NAME));
		String result = new String(Base64.encodeBase64(hash), "UTF-8");
		return (String) result.subSequence(0, result.length() - 2);
	}

	private MessageDigest prepareDigestWithSalt(String salt)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance(ALGORITHM);
		if (salt != null)
			md.update(salt.getBytes("UTF-8"));
		return md;
	}

	public static String calculateWithSalt(String salt, String passwordhash) {
		// String input = salt + passwordhash;
		return instance.calculateHash(salt, passwordhash);
	}

	public static String generateSalt() {
		// why not?
		return calculate(UUID.randomUUID().toString());
	}

	public static boolean isPasswordCorrect(AccountDTO accountDTO, String salt,
			String savedHash) {
		String actualHash = CalculateHash.calculateWithSalt(salt,
				accountDTO.getPassword());
		return actualHash.equals(savedHash);
	}

}
