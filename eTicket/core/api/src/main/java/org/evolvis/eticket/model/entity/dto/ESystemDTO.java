/*
 * 
 */
package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class ESystemDO.
 */
// @XmlRootElement
// @XmlAccessorType(XmlAccessType.FIELD)
public final class ESystemDTO implements Serializable {

	private static final long serialVersionUID = -69991309327812442L;

	/** The id. */
	// @XmlElement
	private final long id;

	/** The name. */
	// @XmlElement
	private final String name;

	/** The default locale. */
	// @XmlElement
	private final LocaleDTO defaultLocale;

	/** The supported locales. */
	// @XmlElement
	private final List<LocaleDTO> supportedLocales;

	/**
	 * Instantiates a new e system do.
	 * 
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 * @param defaultLocale
	 *            the default locale
	 * @param supportedLocales
	 *            the supported locales
	 */
	private ESystemDTO(long id, String name, LocaleDTO defaultLocale,
			List<LocaleDTO> supportedLocales) {
		if (name == null)
			throw new IllegalArgumentException("Name must not be null");
		this.id = id;
		this.name = name;
		this.defaultLocale = defaultLocale;
		this.supportedLocales = supportedLocales;
	}
	
	/**
	 * Creates a new ESystemDTO. This method is typically called from the frontend.
	 * 
	 * @param systemname
	 * @param defaultLocale
	 * @param supportedLocales
	 * @return
	 */
	public static ESystemDTO createNewESystemDTO(String systemname, LocaleDTO defaultLocale,
			List<LocaleDTO> supportedLocales){
		return new ESystemDTO(-1, systemname, defaultLocale, supportedLocales);
	}
	
	/**
	 * 
	 * Creates a new ESystemDTO. This method is typically called from the frontend.
	 * 
	 * @param systemname
	 * @return
	 */
	public static ESystemDTO createNewESystemDTO(String systemname){
		return new ESystemDTO(-1, systemname, null, null);
	}
	
	/**
	 * 
	 * Creates ESystemDTO from an existing ESystem which is already stored in dbs. This method is typically called from the backend.
	 * 
	 * @param id
	 * @param systemname
	 * @param defaultLocale
	 * @param supportedLocales
	 * @return
	 */
	public static ESystemDTO createESystemDTOFromExsistingESystemEntity(long id, String systemname, LocaleDTO defaultLocale,
			List<LocaleDTO> supportedLocales) {
		return new ESystemDTO(id, systemname, defaultLocale, supportedLocales);
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the default locale.
	 * 
	 * @return the default locale
	 */
	public LocaleDTO getDefaultLocale() {
		return defaultLocale;
	}

	/**
	 * Gets the supported locales.
	 * 
	 * @return the supported locales
	 */
	public List<LocaleDTO> getSupportedLocales() {
		if (supportedLocales != null)
			return new ArrayList<LocaleDTO>(supportedLocales);
		else
			return new ArrayList<LocaleDTO>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ESystemDTO))
			return false;
		ESystemDTO eSy = (ESystemDTO) obj;
		return (eSy.getId() == getId() && eSy.getName().equals(getName())
				&& eSy.getDefaultLocale().equals(getDefaultLocale()) && eSy
				.getSupportedLocales().equals(getSupportedLocales()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + getDefaultLocale().hashCode();
		result += 31 * result + getName().hashCode();
		result += 31 * result + getSupportedLocales().hashCode();
		return result;
	}

}
