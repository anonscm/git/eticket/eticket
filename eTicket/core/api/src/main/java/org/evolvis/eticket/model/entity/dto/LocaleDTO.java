/*
 * 
 */
package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;


/**
 * The Class LocaleDO.
 */
//@XmlRootElement
//@XmlAccessorType(XmlAccessType.FIELD)
public final class LocaleDTO implements Serializable {

	private static final long serialVersionUID = -4523424037865026645L;
	/** The id. */
	//@XmlElement
	/** The localecode. */
	//@XmlElement
	private final String localecode;
	
	private final long id;

	/** The name. */
	//@XmlElement
	private final String name;

	/**
	 * Instantiates a new locale do.
	 * 
	 * @param id
	 *            the id
	 * @param code
	 *            the code
	 * @param name
	 *            the name
	 */
	private LocaleDTO(long id, String code, String name) {
		if (code == null)
			throw new IllegalArgumentException("localecode must not be null");
		this.localecode = code;
		this.name = name;
		this.id = id;
	}

	/**
	 * 
	 * Creates a new LocaleDTO. This method is typically called from the frontend.
	 * 
	 * @param code
	 * @param name
	 * @return
	 */
	public static LocaleDTO createNewLocaleDTO(String code, String name){
		return new LocaleDTO(-1, code, name);
	}
	
	/**
	 * 
	 * Creates LocaleDTO from an existing Locale which is already stored in dbs. This method is typically called from the backend.
	 * 
	 * @param code
	 * @param name
	 * @return
	 */
	public static LocaleDTO createLocaleDTOFromExsistingLocaleEntity(long id, String code, String name) {
		return new LocaleDTO(id, code, name);
	}
	
	/**
	 * Gets the localecode.
	 * 
	 * @return the localecode
	 */
	public String getCode() {
		return localecode;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof LocaleDTO))
			return false;
		LocaleDTO locale = (LocaleDTO) obj;
		return (locale.getCode().equals(getCode()) && (locale
				.getName() == null ? getName() == null : locale.getName()
				.equals(getName())));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + getCode().hashCode();
		result += 31 * result + (getName() == null ? 0 : getName().hashCode());
		return result;
	}

	public long getId() {
		return id;
	}

}
