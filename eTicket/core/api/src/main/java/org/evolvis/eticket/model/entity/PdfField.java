package org.evolvis.eticket.model.entity;

import java.util.HashMap;
import java.util.Map;

public enum PdfField {
	FIRSTNAME("firstname", false), LASTNAME("lastname", false), ADRESS("adress", false), ZIP("zip", false), CITY("city", false), ORGA("orga", false), BARCODE(
			"barcode", true);
	private String fieldName;
	private boolean img;

	PdfField(String fieldName, boolean img) {
		this.fieldName = fieldName;
		this.img = img;
	}

	@Override
	public String toString() {
		return fieldName;
	}

	public boolean isImg() {
		return img;
	}
	
	
	private static final Map<String, PdfField> stringToEnum = new HashMap<String, PdfField>();
	static {// initialise map from const name to enum const
		for (PdfField tP : values())
			stringToEnum.put(tP.toString(), tP);
	}
}