/*
 * 
 */
package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;

import org.evolvis.eticket.util.CalculateHash;

/**
 * The Class AccountDTO.
 */
public final class AccountDTO implements Serializable {

	private static final long serialVersionUID = -5327202065004535372L;
	private final String username;
	private final String password;
	private final RoleDTO role;
	// private final boolean passwordHashed;
	private final ESystemDTO eSystemDTO;
	private final String openId;

	/**
	 * Instantiates a new account dto.
	 * 
	 * @param eSystemDTO
	 *            the e system dto
	 * @param username
	 *            the username
	 * @param password
	 *            the passwordhash
	 * @param role
	 *            the role
	 * @param openId
	 *            the openId
	 * @param passwordHashed
	 *            the password hashed
	 */
	private AccountDTO(ESystemDTO eSystemDTO, String username, String password,
			RoleDTO role, String openId) {
		if (username == null || password == null || role == null)
			throw new IllegalArgumentException(
					"Username and passwordhash and role must not be null");
		this.username = username;
		this.password = checkIfPasswordIsHashed(password);
		this.role = role;
		this.eSystemDTO = eSystemDTO;
		this.openId = openId;
	}

	private String checkIfPasswordIsHashed(String password2) {
		if (doesTheSizeOfPasswordDifferExpectedHash(password2))
			throw new IllegalArgumentException("Password is not hashed.");
		return password2;
	}

	private boolean doesTheSizeOfPasswordDifferExpectedHash(String password2) {
		int length = password2.length();
		long aLgorSizeInByte = CalculateHash.ALGOR_BIT_SIZE / 8;
		long base64Overhead = Math.round(aLgorSizeInByte * 0.33) + 1;
		long expected = aLgorSizeInByte + base64Overhead;
		return length != expected;
	}

	/**
	 * Hashes password and get account.
	 * 
	 * @param eSystemDTO
	 *            the e system dto
	 * @param username
	 *            the username
	 * @param password
	 *            the password
	 * @param role
	 *            the role
	 * @param openId
	 *            the openId
	 * @return the account dto
	 */
	@Deprecated
	public static AccountDTO hashPasswordAndGetAccount(ESystemDTO eSystemDTO,
			String username, String password, RoleDTO role, String openId) {
		return new AccountDTO(eSystemDTO, username,
				CalculateHash.calculate(password), role, openId);
	}

	/**
	 * Creates a new AccountDTO. This method is typically called from the
	 * frontend.
	 * 
	 * @param eSystemDTO
	 *            the e system dto
	 * @param username
	 *            the username
	 * @param password
	 *            the password as hash or plaintext
	 * @param role
	 *            the role
	 * @param pwhashed
	 *            the pwhashed
	 * @param openId
	 *            the openId
	 * @return the account
	 */
	public static AccountDTO init(ESystemDTO eSystemDTO,
			String username, String password, RoleDTO role, String openId) {
		return new AccountDTO(eSystemDTO, username,
				CalculateHash.calculate(password), role, openId);
	}

	/**
	 * Creates AccountDTO from an existing Account which is already stored in
	 * dbs. This method is typically called from the backend.
	 * 
	 * @param eSystemDTO
	 *            the e system dto
	 * @param username
	 *            the username
	 * @param password
	 *            the password as hash or plaintext
	 * @param role
	 *            the role
	 * @param pwhashed
	 *            the pwhashed
	 * @param openId
	 *            the openId
	 * @return the account
	 */
	public static AccountDTO initWithExistingHash(
			ESystemDTO eSystemDTO, String username, String password,
			RoleDTO role, String openId) {
		return new AccountDTO(eSystemDTO, username, password, role, openId);
	}

	/**
	 * Gets the username.
	 * 
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Gets the password.
	 * 
	 * @return the password as a hash or plaintext
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Gets the role.
	 * 
	 * @return the role
	 */
	public RoleDTO getRole() {
		return new RoleDTO(role);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "username=" + getUsername() + "&password=" + getPassword()
				+ "role=" + getRole().getRole();
	}

	/**
	 * Gets the system.
	 * 
	 * @return the system
	 */
	public ESystemDTO getSystem() {
		return eSystemDTO;
	}

	/**
	 * Gets the openId
	 * 
	 * @return the openId
	 */
	public String getOpenId() {
		return openId;
	}

	/**
	 * Gets a new platform account.
	 * 
	 * @param username
	 *            the username
	 * @param password
	 *            the password
	 * @param passwordHashed
	 *            the password hashed
	 * @param openId
	 *            the openId
	 * @return the platform account
	 */
	public static AccountDTO getPlatformAccount(String username,
			String password, String openId) {
		return init(null, username, password,
				RoleDTO.getPlatformAdmin(), openId);
	}
	

}
