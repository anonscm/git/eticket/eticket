package org.evolvis.eticket.business.account;

import java.io.Serializable;
import java.util.List;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.AccountShortSummeryDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;

/**
 * The Interface AccountBeanRemote.
 */
public interface AccountBeanService extends Serializable {

    /**
     * Creates the user.
     * 
     * @param system
     *            the system
     * @param accountDTO
     *            the account dto
     */
    public void createUser(AccountDTO accountDTO, String localecode);

    /**
     * Creates the account.
     * 
     * @param caller
     *            the caller must be a systemadmin
     * @param accountDTO
     *            the accountdto which will be created
     */
    public void create(AccountDTO caller, AccountDTO accountDTO);

    /**
     * Delete account.
     * 
     * @param caller
     *            the caller must be a systemadmin
     * @param deleteAccount
     *            the account which will be deleted
     */
    public void delete(AccountDTO caller, AccountDTO deleteAccount);
    
    
    /**
     * Delete account. The Admin Password must be hashed
     * 
     * @param caller
     *            the caller must be a systemadmin
     * @param deleteAccount
     *            the account which will be deleted
     */
    
    public void deletePwhashed (AccountDTO caller, AccountDTO deleteAccount);

    /**
     * Update account.
     * 
     * @param caller
     *            the caller
     * @param updAccount
     *            the upd account
     */
    public void update(AccountDTO caller, AccountDTO updAccount);
    
    /**
     * Update account.
     * 
     * @param caller
     *            the caller
     * @param accounttoupdate
     *            the account, to be replaced           
     * @param updAccount
     *            the upd account
     */
    public void updateRole(AccountDTO caller, AccountDTO accounttoupdate, AccountDTO updAccount);

    
    
    /**
     * Find account.
     * 
     * @param caller
     *            the caller
     * @param username
     *            the username
     * @return the account dto
     */
    public AccountDTO find(ESystemDTO systemDTO, String username);

    /**
     * Activate a user, if a hashed Password is sent then it will also set a new
     * Password for the account.
     * 
     * @param accountDTO
     *            the account
     * @param token
     *            the activation token
     */
    public void activate(AccountDTO accountDTO, String token);

    /**
     * generates a token for a user, so she can set a new password or reactivate her account.
     * @param accountDTO
     */
    public void generateToken(AccountDTO accountDTO);

	/**
	 * Gets the PersonalInformation of the User with this username
	 * 
	 * @param systemDTO
	 * 			the system
	 * @param username
	 * 			the username
	 * @return the personalInformation
	 * 
	 */
	public PersonalInformationDTO getPersonalInformationDTO(ESystemDTO systemDTO, String username);
	
	
	/**
	 * Updates the PersonalInformation of this Account and gives a ticket to it
	 * 
	 * @param accountDTO
	 * 			the account
	 * @param personalInformationDTO
	 * 			the personalInformation
	 */	
	public void UpdatePersonalInformationplusticket(AccountDTO accountDTO,
			PersonalInformationDTO personalInformationDTO); 

	
	/**
	 * Updates the PersonalInformation of this Account
	 * 
	 * @param accountDTO
	 * 			the account
	 * @param personalInformationDTO
	 * 			the personalInformation
	 */
	public void UpdatePersonalInformation(AccountDTO accountDTO, PersonalInformationDTO personalInformationDTO);

    public AccountDTO validatePassword(AccountDTO accountDTO) throws IllegalAccessException;

    public void setPassword(AccountDTO accountDTO, String token);

	public boolean isActivated(ESystemDTO systemDTO, String username);
	
	

	/**
	 * Returns the overall Accountmount of the system of the account
	 * 
	 * @param accountDTO
	 * 			the account
     */
	public String getAccountMount(AccountDTO accountDTO);
	
	/**
	 * Returns the overall inactive Accountmount of the system of the account
	 * 
	 * @param accountDTO
	 * 			the account
     */
	public String getInaktiveAccountMount(AccountDTO accountDTO);
	
	/**
	 * Creates an account, The creator must be an sysadmin
	 * 
	 * @param admin
	 * 			the sysadmin
	 *  @param user
	 * 			the user to create
	 * @param persinf
	 * 			otional PersonalInformationDTO
     */
	
	
	public void createunactivatedAccount(AccountDTO admin, AccountDTO user,PersonalInformationDTO persinf);
	
	public AccountDTO findWithOpenId(ESystemDTO systemDTO, String openId);

	public AccountDTO searchUser(AccountDTO caller,ESystemDTO systemDTO, String username);

	
	/**
	 * Creates an list of user short summeries, who fit into the search string username
	 * 
	 * @param caller
	 * 			the sysadmin
	 *  @param systemDTO
	 * 			the system
	 * @param  username
	 * 			String username to search for
     */
	public List<AccountShortSummeryDTO> searchUsers(AccountDTO caller, ESystemDTO systemDTO,
			String username);

}
