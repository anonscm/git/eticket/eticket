package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;
import java.util.Arrays;

public final class TemplatesDTO implements Serializable {

	private static final long serialVersionUID = -8874830084553512006L;

	private final byte[] binaryData;
	private final String mimeType;
	private final String description;
	private final String uin;
	private final long id;

	private TemplatesDTO(long id, String uin, byte[] binaryData,
			String mimeType, String description) {
		if (uin == null || binaryData == null || mimeType == null)
			throw new IllegalArgumentException(
					"uin, mimetype or binarydata must not be null.");
		this.binaryData = binaryData;
		this.mimeType = mimeType;
		this.description = description;
		this.uin = uin;
		this.id = id;
	}

	/**
	 * 
	 * Creates a new TemplatesDTO. This method is typically called from the
	 * frontend.
	 * 
	 * @param uin
	 * @param binaryData
	 * @param mimeType
	 * @param description
	 * @return
	 */
	public static TemplatesDTO createNewTemplatesDTO(String uin,
			byte[] binaryData, String mimeType, String description) {
		return new TemplatesDTO(-1, uin, binaryData, mimeType, description);
	}

	/**
	 * 
	 * Creates TemplatesDTO from an existing Templates which is already stored
	 * in dbs. This method is typically called from the backend.
	 * 
	 * @param uin
	 * @param binaryData
	 * @param mimeType
	 * @param description
	 * @return
	 */
	public static TemplatesDTO createTemplatesDTOFromExsistingTemplatesEntity(
			long id, String uin, byte[] binaryData, String mimeType,
			String description) {
		return new TemplatesDTO(id, uin, binaryData, mimeType, description);
	}

	public byte[] getBinaryData() {
		return binaryData;
	}

	public String getMimeType() {
		return mimeType;
	}

	public String getDescription() {
		return description;
	}

	public String getUin() {
		return uin;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof TemplatesDTO))
			return false;
		TemplatesDTO dto = (TemplatesDTO) obj;
		return uin.equals(dto.getUin())
				&& Arrays.equals(binaryData, dto.getBinaryData())
				&& (description != null ? description.equals(dto
						.getDescription()) : dto.getDescription() == null)
				&& mimeType.equals(dto.getMimeType());
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + uin.hashCode();
		result = 31 * result + Arrays.hashCode(binaryData);
		result = 31 * result + mimeType.hashCode();
		result = 31 * result
				+ (description == null ? 0 : description.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "uin: " + uin + " mime: " + mimeType;
	}

	public long getId() {
		return id;
	}
}
