package org.evolvis.eticket.business.product;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductNumberDTO;

/**
 * With {@link ProductNumberBeanService} you are able to create, read, update and
 * delete a productnumber in a system. The CUD parts are limited to user who have at
 * least 'productadmin' rights.
 * 
 * @author mjohnk
 * 
 */
public interface ProductNumberBeanService {
	
	public void cerateProductNumber(AccountDTO caller, ProductNumberDTO productNumber);
	public void updateProductNumber(AccountDTO caller, ProductNumberDTO productNumber);
	public void deleteProductNumber(AccountDTO caller, ProductNumberDTO productNumber);
	public String getProductNumber(ProductDTO productDTO);

}
