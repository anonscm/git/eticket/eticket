/*
 * 
 */
package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class RoleDO.
 */
//@XmlRootElement
//@XmlAccessorType(XmlAccessType.FIELD)
public final class RoleDTO implements Serializable{

	private static final long serialVersionUID = 437128795175385015L;

	/**
	 * The Enum AllowedRoles.
	 */
	public enum AllowedRoles {

		/** The USER. */
		USER("user"),
		/** The MODERATOR. */
		MODERATOR("moderator"),
		/** The PRODUCTADMIN. */
		PRODUCTADMIN("productadmin"),
		/** The SYSADMIN. */
		SYSADMIN("sysadmin"),
		/** The PLATFORMADMIN. */
		PLATFORMADMIN("platformadmin");

		/** The type. */
		private String type;

		/**
		 * Instantiates a new allowed roles.
		 * 
		 * @param type
		 *            the type
		 */
		AllowedRoles(String type) {
			this.type = type;

		}

		/** The Constant stringToEnum. */
		private static final Map<String, AllowedRoles> stringToEnum = new HashMap<String, RoleDTO.AllowedRoles>();
		static {// initialise map from const name to enum const
			for (AllowedRoles tP : values())
				stringToEnum.put(tP.toString(), tP);
		}

		/**
		 * From string.
		 * 
		 * @param type
		 *            the type
		 * @return the allowed roles
		 */
		public static AllowedRoles fromString(String type) {
			return stringToEnum.get(type);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return type;
		}

		public static AllowedRoles fromInt(int size) {
			return values()[size -1];
		}
	}


	/** The role. */
	//@XmlElement
	private final String role;
	

	/** The system id. */
	//@XmlElement
	private final long systemID;

	/** The hashcode. */
	private volatile int hashcode;

	/**
	 * Instantiates a new role do.
	 * 
	 * @param role
	 *            the role
	 * @param systemID
	 *            the system id
	 */
	private RoleDTO(AllowedRoles role, long systemID) {
		if (role == null)
			throw new IllegalArgumentException("role must not be null");
		this.role = role.toString();
		this.systemID = systemID;
	}

	/**
	 * Instantiates a new role do.
	 * 
	 * @param role
	 *            the role
	 */
	public RoleDTO(RoleDTO role) {
		this.role = role.getRole();
		this.systemID = role.getSystemID();
	}

	/**
	 * Gets the system id.
	 * 
	 * @return the system id
	 */
	public long getSystemID() {
		return systemID;
	}

	/**
	 * Gets the role.
	 * 
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof RoleDTO))
			return false;
		RoleDTO roleDTO = (RoleDTO) obj;
		return roleDTO.getRole().equals(getRole())
				&& roleDTO.getSystemID() == getSystemID();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = hashcode;
		if (hashcode == 0) {
			result = 17;
			result = 31 * result + role.hashCode();
			result += 31 * result + systemID;
			hashcode = result;
		}
		return result;
	}

	/**
	 * Gets the user.
	 * 
	 * @param systemID
	 *            the system id
	 * @return the user
	 */
	public static RoleDTO getUser(long systemID) {
		return new RoleDTO(AllowedRoles.USER, systemID);
	}

	/**
	 * Gets the moderator.
	 * 
	 * @param systemID
	 *            the system id
	 * @return the moderator
	 */
	public static RoleDTO getModerator(long systemID) {
		return new RoleDTO(AllowedRoles.MODERATOR, systemID);
	}

	/**
	 * Gets the product admin.
	 * 
	 * @param systemID
	 *            the system id
	 * @return the product admin
	 */
	public static RoleDTO getProductAdmin(long systemID) {
		return new RoleDTO(AllowedRoles.PRODUCTADMIN, systemID);
	}

	/**
	 * Gets the sys admin.
	 * 
	 * @param systemID
	 *            the system id
	 * @return the sys admin
	 */
	public static RoleDTO getSysAdmin(long systemID) {
		return new RoleDTO(AllowedRoles.SYSADMIN, systemID);
	}

	/**
	 * Gets the platform admin.
	 * 
	 * @return the platform admin
	 */
	public static RoleDTO getPlatformAdmin() {
		return new RoleDTO(AllowedRoles.PLATFORMADMIN, -1);
	}

	public static RoleDTO get(AllowedRoles role, long id) {
		return new RoleDTO(role, id);
	}
}
