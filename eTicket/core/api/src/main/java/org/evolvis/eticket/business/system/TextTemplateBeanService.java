package org.evolvis.eticket.business.system;

import java.io.Serializable;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO;

public interface TextTemplateBeanService extends Serializable{

	public void insert(AccountDTO caller, TextTemplateDTO textTemplate);
	
	public void update(AccountDTO caller, TextTemplateDTO textTemplate);
	
	public void delete(AccountDTO caller, TextTemplateDTO textTemplate);

	public TextTemplateDTO find(ESystemDTO eSystem, LocaleDTO locale, String name);

}
