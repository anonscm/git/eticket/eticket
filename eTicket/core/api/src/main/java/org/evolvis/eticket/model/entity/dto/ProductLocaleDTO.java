package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import org.evolvis.eticket.util.CurrencyCode;

public final class ProductLocaleDTO implements Serializable {
	private static final long serialVersionUID = 2572506616040799315L;
	private final long id;
	private final LocaleDTO locale;
	private final String currencyCode;
	private final double price;
	private final double salesTax;
	private final TemplatesDTO template;
	private String productName;
	private String productDescription;

	private ProductLocaleDTO(long id, LocaleDTO locale, String currencyCode,
			String productName, String productDescription,
			TemplatesDTO templatesDTO, double price,

			double salesTax) {
		if (locale == null || currencyCode == null || productName == null)
			throw new IllegalArgumentException(
					"product name, locale and currencieCode must be set.");
		this.id = id;
		this.locale = locale;
		this.currencyCode = currencyCode;
		this.price = price;
		this.salesTax = salesTax;
		this.template = templatesDTO;
		this.setProductName(productName);
		this.setProductDescription(productDescription);
	}

	/**
	 * 
	 * Creates a new ProductLocaleDTO. This method is typically called from the
	 * frontend.
	 * 
	 * @param locale
	 * @param currency
	 * @param productName
	 * @param productDescription
	 * @param templatesDTO
	 * @param price
	 * @param salesTax
	 * @return
	 */
	public static ProductLocaleDTO createNewProductLocaleDTO(LocaleDTO locale,
			String currencyCode, String productName, String productDescription,
			TemplatesDTO templatesDTO, double price, double salesTax) {
		return new ProductLocaleDTO(-1, locale, currencyCode, productName,
				productDescription, templatesDTO, price, salesTax);
	}

	/**
	 * 
	 * Creates ProductLocaleDTO from an existing ProductLocale which is already
	 * stored in dbs. This method is typically called from the backend.
	 * 
	 * @param id
	 * @param locale
	 * @param currency
	 * @param productName
	 * @param productDescription
	 * @param templatesDTO
	 * @param price
	 * @param salesTax
	 * @return
	 */
	public static ProductLocaleDTO createProductLocaleDTOFromExsistingProductLocaleEntity(
			long id, LocaleDTO locale, String currencyCode, String productName,
			String productDescription, TemplatesDTO templatesDTO, double price,
			double salesTax) {
		return new ProductLocaleDTO(id, locale, currencyCode, productName,
				productDescription, templatesDTO, price, salesTax);
	}

	public LocaleDTO getLocale() {
		return locale;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public double getPrice() {
		return price;
	}

	public double getSalesTax() {
		return salesTax;
	}

	public TemplatesDTO getTemplate() {
		return template;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductName() {
		return productName;
	}

	public long getId() {
		return id;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ProductLocaleDTO))
			return false;
		ProductLocaleDTO pDto = (ProductLocaleDTO) obj;
		return pDto.getCurrencyCode().equals(getCurrencyCode())
				&& pDto.getLocale().equals(getLocale())
				&& pDto.getId() == getId()
				&& pDto.getProductName().equals(getProductName())
				&& pDto.getPrice() == getPrice()
				&& pDto.getSalesTax() == getSalesTax()
				&& (getTemplate() == null ? pDto.getTemplate() == null
						: getTemplate().equals(pDto.getTemplate()));
	}

	@Override
	public int hashCode() {
		int result = 17;
		result += 31 * result + getCurrencyCode().hashCode();
		result += 31 * result + getLocale().hashCode();
		result += 31 * result + getProductName().hashCode();
		long longBits = Double.doubleToLongBits(price);
		result += 31 * result + (int) (longBits ^ (longBits >>> 32));
		longBits = Double.doubleToLongBits(salesTax);
		result += 31 * result + (int) (longBits ^ (longBits >>> 32));
		result += 31 * result + (template == null ? 0 : template.hashCode());
		return result;
	}

	public BigDecimal getPriceIncludingTax() {
		BigDecimal bdPrice = new BigDecimal(String.valueOf(price));
		BigDecimal bdSalesTax = new BigDecimal(String.valueOf(salesTax));

		bdSalesTax = bdSalesTax.multiply(bdPrice);
		bdPrice = bdPrice.add(bdSalesTax);
		bdPrice = bdPrice.setScale(2, BigDecimal.ROUND_HALF_UP);

		return bdPrice;
	}

	public BigDecimal getTaxOfPrice() {
		BigDecimal bdRet;
		BigDecimal bdTax = new BigDecimal(
				String.valueOf(getPriceIncludingTax()));
		BigDecimal bdPrice = new BigDecimal(String.valueOf(price));
		bdRet = bdTax.subtract(bdPrice);

		return bdRet;
	}

	public String getCurrencySymbol() {
		return CurrencyCode.getSymbolFromCode(currencyCode);
	}

}
