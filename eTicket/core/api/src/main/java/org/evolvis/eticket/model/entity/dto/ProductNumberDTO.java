package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;

/**
 * This is data transfer object for ProductNumber. 
 * 
 * @author mjohnk
 * 
 */
public class ProductNumberDTO implements Serializable {

	private static final long serialVersionUID = 8121137756747612941L;
	private final long maxNumber;
	private final long currentNumber;
	private final long id;
	private final long increment;
	private final ProductDTO product;

	private ProductNumberDTO(long id, long maxNumber, long currentNumber,
			long increment, ProductDTO product ) {
		super();
		this.maxNumber = maxNumber;
		this.currentNumber = currentNumber;
		this.increment = increment;
		this.id=id;
		this.product=product;
	}
	
	/**
	 * 
	 * Creates a new ProductNumberDTO. This method is typically called from the frontend.
	 * 
	 * @param maxNumber
	 * @param currentNumber
	 * @param increment
	 * @param product
	 * @return
	 */
	public static ProductNumberDTO createNewProductNumberDTO(long maxNumber, long currentNumber,
			long increment, ProductDTO product) {
		return new ProductNumberDTO(-1, maxNumber, currentNumber, increment, product);
	}
	
	/**
	 * 
	 * Creates ProductNumberDTO from an existing ProductNumber which is already stored in dbs. This method is typically called from the backend.
	 * 
	 * @param maxNumber
	 * @param currentNumber
	 * @param increment
	 * @param product
	 * @return
	 */
	public static ProductNumberDTO createProductNumberDTOFromExsistingProductNumberLocaleEntity(long id, long maxNumber, long currentNumber,
			long increment, ProductDTO product) {
		return new ProductNumberDTO(id, maxNumber, currentNumber, increment, product);
	}


	public long getMaxNumber() {
		return maxNumber;
	}

	public long getCurrentNumber() {
		return currentNumber;
	}

	public long getIncrement() {
		return increment;
	}

	public long getId() {
		return id;
	}

	public ProductDTO getProduct() {
		return product;
	}
		
}
