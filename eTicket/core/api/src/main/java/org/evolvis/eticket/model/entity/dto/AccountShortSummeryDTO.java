package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;

public final class AccountShortSummeryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8026941172647846086L;

	/** The username. */
	// @XmlElement
	private final String username;

	/** The real name. */
	// @XmlElement
	private final String name;

	/** The users ticketamount. */
	// @XmlElement
	private final String amount;

	public String getUsername() {
		return username;
	}

	private AccountShortSummeryDTO(String username, String name, String amount) {
		this.name = name;
		this.username = username;
		this.amount = amount;
	}

	public static AccountShortSummeryDTO createAccountShortSummeryDTO(
			String username, String name, String amount) {
		return new AccountShortSummeryDTO(username, name, amount);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof AccountShortSummeryDTO))
			return false;
		AccountShortSummeryDTO account = (AccountShortSummeryDTO) obj;
		return this.name == account.getName()
				&& this.username == account.getUsername()
				&& this.amount == account.getAmount();
	}

	public String getName() {
		return name;
	}

	public String getAmount() {
		return amount;
	}

}
