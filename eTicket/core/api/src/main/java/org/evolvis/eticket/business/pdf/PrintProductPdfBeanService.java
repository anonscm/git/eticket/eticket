package org.evolvis.eticket.business.pdf;

import java.io.Serializable;
import java.util.Map;

import org.evolvis.eticket.model.entity.PdfField;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;

public interface PrintProductPdfBeanService extends Serializable{
	
	public byte[] createPDF(Map<PdfField, Object> values, TemplatesDTO pdfTemplate, String productNumber, ESystemDTO eSystemDTO ) ;
}
