package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * This is data transfer object for Product.
 * 
 * A Product can be any kind of thing (e.g. a ticket, t-shirt, pullover, ...).
 * 
 * @author phil
 * 
 */
public final class ProductDTO implements Serializable {
	/**
	 * A product can be either digital, means that the system can handle
	 * transfer itself, or analog, means somebody has the pack and send it.
	 * 
	 * FIXME: atm only digital is supported.
	 * 
	 * @author phil
	 * 
	 */
	public enum ProductType {
		DIGITAL, ANALOG;
	}

	private static final long serialVersionUID = -6597762101182188911L;

	/**
	 * Creates a new ProductDTO. This method is typically called from the
	 * frontend.
	 * 
	 * @param uin
	 *            the uin of the product. Must not be null.
	 * @param system
	 *            the system in which the product exists. Must not be null.
	 * @param productLocales
	 *            the productlocales contains locale, a currency for that
	 *            locale, a product price, sales tax and a binary template for
	 *            that product.
	 * @param maxAmount
	 *            the maximal amount of the product, when max amount is 0 then
	 *            the system assumes that the amount of that product can be
	 *            infinite.
	 * @param currAmount 
	 * 			  the current amount of the product
	 * @return a single instance of ProductDTO.
	 * @throws IllegalArgumentException
	 *             when either the uin or the system is null.
	 */


	public static ProductDTO createNewProductDTO(String uin,
			ProductType productType, ESystemDTO system,
			ProductLocaleDTO[] productLocales, int maxAmount, int ttdl) {
		return new ProductDTO(uin, productType, system, productLocales, null,ttdl,
				0, maxAmount, -1);
}

	/**
	 * Creates a new ProductDTO. This method is typically called from the
	 * frontend.
	 * 
	 * @param uin
	 *            the uin of the product. Must not be null.
	 * @param system
	 *            the system in which the product exists. Must not be null.
	 * @param productLocales
	 *            the productlocales contains locale, a currency for that
	 *            locale, a product price, sales tax and a binary template for
	 *            that product.
	 * @param maxAmount
	 *            the maximal amount of the product, when max amount is 0 then
	 *            the system assumes that the amount of that product can be
	 *            infinite.
	 * @param currAmount 
	 * 			  the current amount of the product            
	 * @param deadline
	 *            this parameter says that the transfer (sending this product to
	 *            an other user) is limited to a deadline. When deadline is null
	 *            there is no deadline.
	 * @return single instance of ProductDTO
	 * @throws IllegalArgumentException
	 *             when either the uin or the system is null.
	 */

	
	public static ProductDTO createNewProductDTO(String uin,
			ProductType productType, ESystemDTO system,
			ProductLocaleDTO[] productLocales,int currAmount, int maxAmount, Date deadline) {
		return new ProductDTO(uin, productType, system, productLocales,
				deadline,currAmount, 0, maxAmount, -1);


	}

	/**
	 * Creates a new ProductDTO. This method is typically called from the
	 * frontend.
	 * 
	 * @param uin
	 *            the uin of the product. Must not be null.
	 * @param system
	 *            the system in which the product exists. Must not be null.
	 * @param productLocales
	 *            the productlocales contains locale, a currency for that
	 *            locale, a product price, sales tax and a binary template for
	 *            that product.
	 * @param maxAmount
	 *            the maximal amount of the product, when max amount is 0 then
	 *            the system assumes that the amount of that product can be
	 *            infinite.
	 * @param currAmount 
	 * 			  the current amount of the product
	 * @param deadline
	 *            this parameter says that the transfer (sending this product to
	 *            an other user) is limited to a deadline. If deadline is null
	 *            than there is no deadline.
	 * @param expiryTTL
	 *            expiryTTL expresses a duration in which the receiver of this
	 *            product must accept the product, when she didn't accept it the
	 *            product will be transfered back to sender. If expiryTTL is 0
	 *            than there's no limitation.
	 * @return single instance of ProductDTO
	 * @throws IllegalArgumentException
	 *             when either the uin or the system is null.
	 */

	public static ProductDTO createNewProductDTO(String uin,
			ProductType productType, ESystemDTO system,
			ProductLocaleDTO[] productLocales,int currAmount, int maxAmount, Date deadline,
			long expiryTTL) {
		return new ProductDTO(uin, productType, system, productLocales,
				deadline, expiryTTL, currAmount, maxAmount, -1);
	}

	/**
	 * Creates ProductDTO from an existing Product which is already stored in
	 * dbs. This method is typically called from the backend.
	 * 
	 * @param uin
	 * @param productType
	 * @param system
	 * @param productLocales
	 * @param maxAmount
	 * @param deadline
	 * @param expiryTTL,
	 * @param id
	 * @return
	 */
	public static ProductDTO createProductDTOFromExsistingProductEntity(
			String uin, ProductType productType, ESystemDTO system,
			ProductLocaleDTO[] productLocales,int currAmount, int maxAmount, Date deadline,
			long expiryTTL, long id) {
		return new ProductDTO(uin, productType, system, productLocales,
				deadline, expiryTTL,currAmount, maxAmount, id);

	}

	private final String uin;
	private final ESystemDTO system;
	
	private final long id;

	private final int maxAmount;
	private final int currAmount;

	private final Date deadline;

	private final long expiryTTL;

	private final ProductLocaleDTO[] productLocales;
	private final ProductType productType;

	private ProductDTO(String uin, ProductType productType, ESystemDTO system,
			ProductLocaleDTO[] productLocales, Date deadline, long expiryTTL,int currAmount,
			int maxAmount, long id) {

		if (uin == null || system == null) {
			throw new IllegalArgumentException("uin, system must be set.");
		}
		this.uin = uin;
		this.system = system;
		if (deadline != null)
			this.deadline = new Date(deadline.getTime());
		else
			this.deadline = null;
		this.expiryTTL = expiryTTL;
		this.maxAmount = maxAmount;
		this.currAmount = currAmount;
		this.productLocales = productLocales;
		this.productType = productType;
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof ProductDTO)) {
			return false;
		}
		ProductDTO pDto = (ProductDTO) obj;
		return uin.equals(pDto.getUin())
				&& system.equals(pDto.getSystem())
				&& maxAmount == pDto.getMaxAmount()
				&& expiryTTL == pDto.getExpiryTTL()
				&& (deadline == null ? pDto.getDeadline() == null : deadline
						.equals(pDto.getDeadline()));
	}

	/**
	 * Gets the deadline
	 * 
	 * @return the allow transfer until
	 */
	public Date getDeadline() {
		if (deadline != null)
			return new Date(deadline.getTime());
		else
			return null;
	}

	/**
	 * Gets the expiry ttl.
	 * 
	 * @return the expiry ttl
	 */
	public long getExpiryTTL() {
		return expiryTTL;
	}

	/**
	 * Gets the max amount.
	 * 
	 * @return the max amount
	 */
	public int getMaxAmount() {
		return maxAmount;
	}

	/**
	 * Gets the system.
	 * 
	 * @return the system
	 */
	public ESystemDTO getSystem() {
		return system;
	}

	/**
	 * Gets the uin.
	 * 
	 * @return the uin
	 */
	public String getUin() {
		return uin;
	}
	
	public int getCurrAmount() {
		return currAmount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + uin.hashCode();
		result = 31 * result + system.hashCode();
		result = 31 * result + (deadline == null ? 0 : deadline.hashCode());
		result = 31 * result + maxAmount;
		result = 31 * result + (int) (expiryTTL ^ expiryTTL >>> 32);
		return result;
	}

	public ProductLocaleDTO[] getProductLocales() {
		return productLocales;
	}

	public ProductType getProductType() {
		return productType;
	}

	public long getId() {
		return id;
	}

}
