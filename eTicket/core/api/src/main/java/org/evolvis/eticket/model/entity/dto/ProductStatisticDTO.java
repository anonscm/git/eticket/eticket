package org.evolvis.eticket.model.entity.dto;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public final class ProductStatisticDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1933909823939387274L;
	/**
	 * 
	 */

	private final ProductDTO productDTO;
	private final String invintations;
	private final String acceptedinvintations;
	private final String fixedinvintations;
	private final String shop;

	private ProductStatisticDTO(ProductDTO productDTO, String invintations,
			String acceptedinvintations, String fixedinvintitations, String shop) {
		this.invintations = invintations;
		this.acceptedinvintations = acceptedinvintations;
		this.fixedinvintations = fixedinvintitations;
		this.productDTO = productDTO;
		this.shop = shop;

	}

	public static ProductStatisticDTO createIntern(ProductDTO productDTO,
			String invintations, String acceptedinvintations,
			String fixedinvintations) {
		return new ProductStatisticDTO(productDTO, invintations,
				acceptedinvintations, fixedinvintations, null);
	}

	public static ProductStatisticDTO init(ProductDTO productDTO,
			String invintations, String acceptedinvintations,
			String fixedinvintations, String shop) {
		return new ProductStatisticDTO(productDTO, invintations,
				acceptedinvintations, fixedinvintations, shop);
	}

	public ProductDTO getProductDTO() {
		return productDTO;
	}

	public String getInvintations() {
		return invintations;
	}

	public String getAcceptedInvintations() {
		return acceptedinvintations;
	}

	public String getFixedInvintations() {
		return fixedinvintations;
	}

	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof ProductStatisticDTO)) {
			return false;
		}
		ProductStatisticDTO pDto = (ProductStatisticDTO) obj;
		return this.getProductDTO().equals(pDto.getProductDTO());
	}

	private void writeObject(ObjectOutputStream aOutputStream)
			throws IOException {
		// perform the default serialization for all non-transient, non-static
		// fields
		aOutputStream.defaultWriteObject();
	}

	public String getShop() {
		return shop;
	}
}
