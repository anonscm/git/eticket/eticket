package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;

/**
 * This is data transfer object for ProductPool. 
 * 
 * @author mjohnk
 * 
 */
public final class ProductPoolDTO implements Serializable{

	private static final long serialVersionUID = 7935308760749843913L;
	
	/**	the id */
	private final long id;
	/** the ProductDTO */
	private final ProductDTO productDTO;
	/** the amount */
	private final String amount;
	
	/**
	 * Instantiates a new ProductPool dto.
	 * 
	 * @param id
	 * 			the id
	 * @param productDTO
	 * 			the product dto	
	 * @param balance
	 * 			the balance
	 */
	private ProductPoolDTO(long id, ProductDTO productDTO, String amount) {
		this.id = id;
		this.productDTO = productDTO;
		this.amount = amount;
	}
	
	/**
	 * 
	 * Creates a new ProductPoolDTO. This method is typically called from the frontend.
	 * 
	 * @param productDTO
	 * @param amount
	 * @return
	 */
	public static ProductPoolDTO createNewProductPoolDTO(ProductDTO productDTO, String amount){
		return new ProductPoolDTO(-1, productDTO, amount);
	}
	
	
	/**
	 * 
	 * Creates ProductPoolDTO from an existing ProductPool which is already stored in dbs. This method is typically called from the backend.
	 * 
	 * @param id
	 * @param productDTO
	 * @param amount
	 * @return
	 */
	public static ProductPoolDTO createProductPoolDTOFromExsistingProductPoolEntity(long id, ProductDTO productDTO, String amount){
		return new ProductPoolDTO(id, productDTO, amount);
	}

	/**
	 * Gets the id
	 * 
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Gets the Product dto
	 * 
	 * @return the Product dto
	 */
	public ProductDTO getProductDTO() {
		return productDTO;
	}

	/**
	 * Gets the amount 
	 * 
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
}
