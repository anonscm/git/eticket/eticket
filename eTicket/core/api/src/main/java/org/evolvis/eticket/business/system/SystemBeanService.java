/*
 * 
 */
package org.evolvis.eticket.business.system;

import java.io.Serializable;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;

/**
 * The Interface SystemBeanRemote.
 */
public interface SystemBeanService extends Serializable {

	/**
	 * Creates the system.
	 *
	 * @param platformadmin the platformadmin
	 * @param platformID the platform id
	 * @param systemDTO the system dto
	 */
	public void createSystem(AccountDTO platformadmin, String platformId, ESystemDTO systemDTO);

	/**
	 * Gets the system by name.
	 *
	 * @param systemname the systemname
	 * @return the system by name
	 */
	public ESystemDTO getSystemByName(String systemname);

	/**
	 * Gets the system by id.
	 *
	 * @param id the id
	 * @return the system by id
	 */
	public ESystemDTO getSystemById(long id);

	/**
	 * Update system.
	 *
	 * @param platformadmin the platformadmin
	 * @param platformID the platform id
	 * @param systemDTO the system dto
	 */
	public void updateSystem(AccountDTO platformadmin, String platformId, 
			ESystemDTO systemDTO);

	/**
	 * Delete system.
	 *
	 * @param platformadmin the platformadmin
	 * @param platformID the platform id
	 * @param systemDTO the system dto
	 */
	public void deleteSystem(AccountDTO platformadmin, String platformId, ESystemDTO systemDTO);
	
	/** Gets the locale
	 * 
	 * @param id the Id of the local
	 * @return the LocaleDTO
	 */
	public LocaleDTO getLocaleById(long id);

}
