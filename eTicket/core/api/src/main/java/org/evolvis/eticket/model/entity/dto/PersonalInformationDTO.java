package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;

/**
 * This is data transfer object for PersonalInformation.
 * 
 * @author mjohnk
 * 
 */
public final class PersonalInformationDTO implements Serializable {

	private static final long serialVersionUID = 919532211739469203L;

	/** The salution */
	private final String salutation;

	/** The title */
	private final String title;

	/** The firstname */
	private final String firstname;

	/** The lastname */
	private final String lastname;

	/** The adress */
	private final String address;

	/** The zipcode */
	private final String zipcode;

	/** The city */
	private final String city;

	/** The country */
	private final String country;

	/** The final localcode */
	private final LocaleDTO localeDTO;

	/** The organisation */
	private final String organisation;

	/** The email */
	private final String email;

	private final long id;

	/**
	 * Instantiates a new PersonalInformation dto.
	 * 
	 * @param salutation
	 *            the salutation
	 * @param title
	 *            the title
	 * @param firstname
	 *            the firstname
	 * @param lastname
	 *            the lastname
	 * @param address
	 *            the address
	 * @param zipcode
	 *            the zipcode
	 * @param city
	 *            the city
	 * @param country
	 *            the country
	 * @param localeDTO
	 *            the locale dto
	 * @param organisation
	 *            the organisation
	 * @param email
	 *            the email
	 */
	private PersonalInformationDTO(long id, String salutation, String title,
			String firstname, String lastname, String address, String zipcode,
			String city, String country, LocaleDTO localeDTO,
			String organisation, String email) {
		this.id = id;
		this.salutation = salutation;
		this.title = title;
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
		this.zipcode = zipcode;
		this.city = city;
		this.country = country;
		this.localeDTO = localeDTO;
		this.organisation = organisation;
		this.email = email;
	}

	/**
	 * 
	 * Creates a new PersonalInformationDTO. This method is typically called
	 * from the frontend.
	 * 
	 * @param salutation
	 * @param title
	 * @param firstname
	 * @param lastname
	 * @param address
	 * @param zipcode
	 * @param city
	 * @param country
	 * @param localeDTO
	 * @param organisation
	 * @param email
	 * @return
	 */
	public static PersonalInformationDTO createNewPersonalInformationDTO(
			String salutation, String title, String firstname, String lastname,
			String address, String zipcode, String city, String country,
			LocaleDTO localeDTO, String organisation, String email) {
		return new PersonalInformationDTO(-1, salutation, title, firstname,
				lastname, address, zipcode, city, country, localeDTO,
				organisation, email);
	}

	/**
	 * Creates PersonalInformationDTO from an existing PersonalInformation which
	 * is already stored in dbs. This method is typically called from the
	 * backend.
	 * 
	 * @param salutation
	 * @param title
	 * @param firstname
	 * @param lastname
	 * @param address
	 * @param zipcode
	 * @param city
	 * @param country
	 * @param localeDTO
	 * @param organisation
	 * @param email
	 * @return
	 */
	public static PersonalInformationDTO createPersonalInformationDTOFromExsistingPersonalInformationEntity(
			long id, String salutation, String title, String firstname, String lastname,
			String address, String zipcode, String city, String country,
			LocaleDTO localeDTO, String organisation, String email) {
		return new PersonalInformationDTO(id, salutation, title, firstname,
				lastname, address, zipcode, city, country, localeDTO,
				organisation, email);
	}

	/**
	 * Gets the salutation
	 * 
	 * @return the salutation
	 */
	public String getSalutation() {
		return salutation;
	}

	/**
	 * Gets the title
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Gets the firstname
	 * 
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * Gets the lastname
	 * 
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * Gets the address
	 * 
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Gets the zipcode
	 * 
	 * @return the zipcode
	 */
	public String getZipcode() {
		return zipcode;
	}

	/**
	 * Gets the city
	 * 
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Gets the country
	 * 
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Gets the locale dto
	 * 
	 * @return the locale dto
	 */
	public LocaleDTO getLocaleDTO() {
		return localeDTO;
	}

	/**
	 * Gets the organisation
	 * 
	 * @return the organisation
	 */
	public String getOrganisation() {
		return organisation;
	}

	/**
	 * Gets the email
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	
	/**
	 * Checks if the personal information is complete,
	 * 
	 * 
	 * @return boolean
	 */
	public boolean iscomplete(){
     
		if ((firstname!=null)&&(lastname!=null)&&(salutation!=null)&&(zipcode!=null)&&(address!=null)&&(city!=null))
			{return true;}
		else{return false;}
	}


	public long getId() {
		return id;

	}
}
