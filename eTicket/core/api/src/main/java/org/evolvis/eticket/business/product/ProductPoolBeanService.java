package org.evolvis.eticket.business.product;

import java.util.List;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;


/**
 * With {@link ProductPoolBeanService} you can get the ProductPool.
 * 
 * @author mjohnk
 *
 */
public interface ProductPoolBeanService {
	
	/**
	 * Get a List of ProductPoolDTO 
	 * 
	 * @param status
	 * 			the status of the ProductPool can only be VOLATILE, FIXED or REGISTERED
	 * @param username
	 * 			the Username of the account 
	 * @return the List of the ProductPoolDTO
	 */
	public List<ProductPoolDTO> getProductPoolList(String status, String username);
	
	public void fixProduct(AccountDTO accountDTO, ProductDTO productDTO);

	public void fixProductasAdmin(AccountDTO admin, AccountDTO user, ProductDTO product);

	
	public String getProductNumber(long productPoolId);


}
