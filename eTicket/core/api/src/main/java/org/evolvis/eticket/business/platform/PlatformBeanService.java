package org.evolvis.eticket.business.platform;

import java.io.Serializable;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;

/**
 * {@link PlatformBeanService} is a interface to create a Platform, add systems
 * to it, delete systems from a platform and delete the platform. For the most
 * operation of the interface you must PlatformAdmin to call the methods.
 * 
 * @author phil
 * 
 */
public interface PlatformBeanService extends Serializable {

	/**
	 * Creates the platform and return id.
	 * 
	 * @param platformAdmin
	 *            the platform admin, if no platform admin exists a platform
	 *            admin will be created with given credentials
	 * @return platformID
	 * @throws IllegalAccessException
	 *             if "platformAdmin" has not the role Platformadmin.
	 */
	public void createPlatform(AccountDTO platformAdmin, String platform)
			throws IllegalAccessException;

	/**
	 * Adds the system to platform.
	 * 
	 * @param platformAdmin
	 *            the platform admin
	 * @param platformId
	 *            the platform id
	 * @param system
	 *            the system
	 * @throws Exception
	 *             if "platformAdmin" has not the role Platformadmin.
	 */
	public void addSystemToPlatform(AccountDTO platformAdmin,
			String platformId, ESystemDTO system);

	/**
	 * Delete system from platform.
	 * 
	 * @param platformAdmin
	 *            the platform admin
	 * @param platformId
	 *            the platform id
	 * @param system
	 *            the system
	 * @throws Exception
	 *             if "platformAdmin" has not the role Platformadmin.
	 */
	public void deleteSystemFromPlatform(AccountDTO platformAdmin,
			String platformId, ESystemDTO system);

	/**
	 * Delete platform.
	 * 
	 * @param platformAdmin
	 *            the platform admin
	 * @param platformId
	 *            the platform id
	 * @throws Exception
	 *             if "platformAdmin" has not the role Platformadmin.
	 */
	public void deletePlatform(AccountDTO platformAdmin, String platformId);

	/**
	 * creates a new locale in platform.
	 * 
	 * @param locale
	 * 
	 */
	public void createLocale(AccountDTO platformAdmin, String platFormID,
			LocaleDTO locale);

	/**
	 * returns a locale found by its localecode.
	 * 
	 * @param localecode
	 *            the localecode e.g. 'de' or 'en';
	 * @return a locale found by its localecode.
	 */
	public LocaleDTO getLocale(String localecode);

	/**
	 * deletes a locale, for this operation you have to be
	 * PlatformAdministrator.
	 * 
	 * @param platformAdmin
	 *            your account.
	 * @param id
	 *            the id of the platform.
	 * @param localecode
	 *            the localecode.
	 */
	public void deleteLocale(AccountDTO platformAdmin, String id,
			String localecode);

}
