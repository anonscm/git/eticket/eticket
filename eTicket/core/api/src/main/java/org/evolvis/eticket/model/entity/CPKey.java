package org.evolvis.eticket.model.entity;

import java.util.HashMap;
import java.util.Map;

public enum CPKey {

	ACCOUNT_VALIDATION_LIMIT("ACCOUNT_VALIDATION_LIMIT"), 
	SYSTEM_SENDER("SYSTEM_SENDER"), 
	SYSTEM_DOMAIN("SYSTEM_DOMAIN"), 
	SHOP_DOMAIN("SHOP_DOMAIN"),
	FIXED_TICKETNUMBER("FIXED_TICKETNUMBER");

	private String type;

	CPKey(String type) {
		this.type = type;
	}

	private static final Map<String, CPKey> stringToEnum = new HashMap<String, CPKey>();
	static {// initialise map from const name to enum const
		for (CPKey tP : values())
			stringToEnum.put(tP.toString(), tP);
	}

	public static CPKey fromString(String type) {
		return stringToEnum.get(type);
	}

	@Override
	public String toString() {
		return type;
	}

	public static CPKey fromInt(int size) {
		return values()[size - 1];
	}
}