package org.evolvis.eticket.util;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CurrencyCode {
	// String1 = Currency Code, String2 = Symbol
	private static Map<String, String> currencieCodesMap = new HashMap<String, String>();
	private static ArrayList<String> currencieCodes = new ArrayList<String>();

	static {
		String code;
		Locale[] locales = Locale.getAvailableLocales();
		for (Locale locale : locales) {
			try {
				code = Currency.getInstance(locale).getCurrencyCode();
				if (!currencieCodesMap.containsKey(code)) {
					currencieCodesMap.put(code, Currency.getInstance(code)
							.getSymbol(locale));
				}
			} catch (Exception e) {
				// locale is not supported, do nothing.
			}
		}
		currencieCodes.addAll(currencieCodesMap.keySet());
	}

	/**
	 * returns a list of all available ISO 4217 currency codes.
	 * 
	 * @return ArrayList
	 */
	public static ArrayList<String> getAvailableCurrencieCodes() {
		return currencieCodes;
	}

	/**
	 * Apply currency code and get the corresponding symbol.
	 * 
	 * @param code
	 * @return symbol
	 */
	public static String getSymbolFromCode(String code) {
		return currencieCodesMap.get(code);
	}

	/**
	 * Verify if currency code is correct.
	 * 
	 * @param code
	 * @return
	 */
	public static boolean isCurrencyCodeCorrect(String code) {
		return currencieCodes.contains(code);
	}
}