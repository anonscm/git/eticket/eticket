package org.evolvis.eticket.business.system;

import java.io.Serializable;

import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;

public interface ConfigurationParameterBeanService extends Serializable {

	public void insertCP(AccountDTO caller, ESystemDTO system, CPKey key,
			String value);

	public void setValueOfCP(AccountDTO caller, ESystemDTO system, CPKey key,
			String value);

	public void deleteCP(AccountDTO caller, ESystemDTO system, CPKey key);

	public String find(ESystemDTO eSystem, String key);

	public int findAsInt(ESystemDTO eSystem, String key);

	public long findAsLong(ESystemDTO eSystem, String key);

}