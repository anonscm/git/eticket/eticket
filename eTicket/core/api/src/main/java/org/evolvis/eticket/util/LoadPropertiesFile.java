package org.evolvis.eticket.util;

import java.io.IOException;
import java.util.Properties;

public class LoadPropertiesFile {

	public Properties open(String fileName) {
		Properties properties = new Properties();
		try {
			properties.load(this.getClass().getClassLoader()
					.getResourceAsStream(fileName));
		} catch (IOException e) {
			throw new IllegalArgumentException("unable to load " + fileName
					+ " as a properties file.", e);
		} catch (NullPointerException e) {
			throw new IllegalArgumentException(fileName + " not found.");
		}
		return properties;
	}
}
