package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;

/**
 * This is a data transfer object for TransferHistory. 
 * 
 * @author mjohnk
 * 
 */
public final class TransferHistoryDTO implements Serializable {

	private static final long serialVersionUID = -8377895975963419274L;
	
	/** the id */
	private final long id;
	/** the Product dto */
	private final ProductDTO productDTO;
	/** the recipient Account dto */
	private final AccountDTO recipient;
	/** 
	 * the amount of products which are transfered
	 */
	private final long amount;
	/** the ProductPool dto */
	private final ProductPoolDTO productPoolDTO;
	/** the sender Account dto */
	private final AccountDTO sender;
	
	
	/**
	 * Instantiates a new TransferHistory dto
	 * 
	 * @param productDTO
	 * 			the product dto
	 * @param recipient
	 * 			the recipient
	 */
	private TransferHistoryDTO(long id, ProductDTO productDTO, AccountDTO recipient, long amount, ProductPoolDTO productPoolDTO, AccountDTO sender) {
		this.id = id;
		this.productDTO = productDTO;
		this.recipient = recipient;
		this.amount = amount;
		this.productPoolDTO = productPoolDTO;
		this.sender = sender;
	}
	
	/**
	 * Creates a new TransferHistoryDTO. This method is typically called from the frontend.
	 * 
	 * @param productDTO
	 * @param recipient
	 * @param amount
	 * @param productPoolDTO
	 * @param sender
	 * @return TransferHistoryDTO
	 */
	public static TransferHistoryDTO createNewTransferHistoryDTO(ProductDTO productDTO, AccountDTO recipient, long amount, ProductPoolDTO productPoolDTO, AccountDTO sender){
		return new TransferHistoryDTO(-1, productDTO, recipient, amount, productPoolDTO, sender);
	}
	
	/**
	 * Creates TransferHistoryDTO from an exsisting TransferHistory which is already stored in dbs. This method is typically called from the backend.
	 * 
	 * @param id
	 * @param productDTO
	 * @param recipient
	 * @param amount
	 * @param productPoolDTO
	 * @param sender
	 * @return TransferHistoryDTO
	 */
	public static TransferHistoryDTO createTransferHistoryDTOFromExsistingTransferHistoryEntity(long id, ProductDTO productDTO, AccountDTO recipient, long amount, ProductPoolDTO productPoolDTO, AccountDTO sender) {
		return new TransferHistoryDTO(id, productDTO, recipient, amount, productPoolDTO, sender);
	}
	
	/**
	 * Gets the Product
	 * 
	 * @return the product
	 */
	public ProductDTO getProductDTO() {
		return productDTO;
	}

	/**
	 * Gets the Recipient
	 * 
	 * @return the recipient
	 */
	public AccountDTO getRecipient() {
		return recipient;
	}

	/**
	 * Gets the Amount
	 * 
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}

	/**
	 * Gets the ProductPool
	 * 
	 * @return the productpool
	 */
	public ProductPoolDTO getProductPoolDTO() {
		return productPoolDTO;
	}

	/**
	 * Gets the Sender
	 * 
	 * @return the sender
	 */
	public AccountDTO getSender() {
		return sender;
	}
	

	public long getId() {
		return id;
	}
}
