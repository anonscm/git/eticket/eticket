package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;

public final class TextTemplateDTO implements Serializable {

	public enum TextTemplateNames {

		NEW_ACCOUNT("new_account", "activationlink"),

		INVITATION_RECIPIENT("invitation_recipient"),

		INVITATION_SENDER("invitation_sender"),

		SENT_PRODUCT("sent_product", "senderfirstname", "senderlastname", "recemail"),

		RECEIVED_PRODUCT("rec_product", "senderfirstname", "senderlastname"),
		
		RECEIVED_PRODUCT_BY_SYSTEM("rec_product_by_system"),
		
		DECLINE_PRODUCT("dec_product", "recipient", "product"),

		CANCELED_PRODUCT("canc_product", "recipient", "product");

		private String type;
		private String[] buzzwords;

		private TextTemplateNames(String type, String... buzzwords) {
			this.type = type;
			this.buzzwords = buzzwords;

		}

		public String[] getBuzzwords() {
			return buzzwords;
		}

		/** The Constant stringToEnum. */
		private static final Map<String, TextTemplateNames> stringToEnum = new HashMap<String, TextTemplateNames>();
		static {// initialise map from const name to enum const
			for (TextTemplateNames tP : values())
				stringToEnum.put(tP.toString(), tP);
		}

		public static TextTemplateNames fromString(String type) {
			return stringToEnum.get(type);
		}

		@Override
		public String toString() {
			return type;
		}

		public static TextTemplateNames fromInt(int size) {
			return values()[size - 1];
		}
	}

	private static final long serialVersionUID = -2961916918341551349L;

	private final ESystemDTO systemDTO;

	private final LocaleDTO localeDTO;

	private final String name;

	private final String descripption;

	private final String text;

	private volatile int hashcode;

	private TextTemplateDTO(ESystemDTO systemDTO, LocaleDTO localeDTO,
			String name, String text, String description) {
		if (systemDTO == null || localeDTO == null || name == null)
			throw new IllegalArgumentException(
					"systemDTO, localeDTO or name must not be null.");
		this.systemDTO = systemDTO;
		this.localeDTO = localeDTO;
		this.name = name;
		this.text = text;
		this.descripption = description;
	}

	/**
	 * Creates a new TextTemplateDTO. This method is typically called from the frontend.
	 * 
	 * @param systemDTO
	 * @param localeDTO
	 * @param name
	 * @param text
	 * @return
	 */
	public static TextTemplateDTO createNewTextTemplateDTO(
			ESystemDTO systemDTO, LocaleDTO localeDTO, TextTemplateNames name,
			String text) {
		return new TextTemplateDTO(systemDTO, localeDTO, name.toString(), text,
				null);
	}

	/**
	 * Creates a new TextTemplateDTO. This method is typically called from the frontend.
	 * 
	 * @param systemDTO
	 * @param localeDTO
	 * @param name
	 * @param text
	 * @return
	 */
	public static TextTemplateDTO createNewTextTemplateDTO(
			ESystemDTO systemDTO, LocaleDTO localeDTO, String name, String text) {
		return new TextTemplateDTO(systemDTO, localeDTO, name, text, null);
	}

	/**
	 * Creates a new TextTemplateDTO. This method is typically called from the frontend.
	 * 
	 * @param systemDTO
	 * @param localeDTO
	 * @param name
	 * @param text
	 * @param desc
	 * @return
	 */
	public static TextTemplateDTO createNewTextTemplateDTO(
			ESystemDTO systemDTO, LocaleDTO localeDTO, TextTemplateNames name,
			String text, String desc) {
		return new TextTemplateDTO(systemDTO, localeDTO, name.toString(), text,
				desc);
	}

	/**
	 * Creates TextTemplateDTO from an existing TextTemplate which is already stored in dbs. This method is typically called from the backend.
	 * 
	 * @param systemDTO
	 * @param localeDTO
	 * @param name
	 * @param text
	 * @param desc
	 * @return
	 */
	public static TextTemplateDTO createTextTemplateDTOFromExsistingTextTemplateEntity(
			ESystemDTO systemDTO, LocaleDTO localeDTO, String name,
			String text, String desc) {
		return new TextTemplateDTO(systemDTO, localeDTO, name, text, desc);
	}

	public ESystemDTO getSystemDTO() {
		return systemDTO;
	}

	public LocaleDTO getLocaleDTO() {
		return localeDTO;
	}

	public String getName() {
		return name;
	}

	public String getDescripption() {
		return descripption;
	}

	public String getText() {
		return text;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof TextTemplateDTO))
			return false;
		TextTemplateDTO ttD = (TextTemplateDTO) obj;
		return ttD.getSystemDTO().equals(getSystemDTO())
				&& ttD.getLocaleDTO().equals(getLocaleDTO())
				&& ttD.getName().equals(getName());
	}

	@Override
	public int hashCode() {
		int result = hashcode;
		if (hashcode == 0) {
			result = 17;
			result = 31 * result + getSystemDTO().hashCode();
			result = 31 * result + getLocaleDTO().hashCode();
			result = 31 * result + getName().hashCode();
		}
		return result;
	}

}
