package org.evolvis.eticket.business.product;

import java.util.List;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;

/**
 * With {@link ProductBeanService} you are able to create, read, update and
 * delete a product in a system. The CUD parts are limited to user who have at
 * least 'productadmin' rights.
 * 
 * @author phil
 * 
 */
public interface ProductBeanService {

	
	  /**
     * creates a new product in system. This method is limited to user/accounts
     * who are at least 'ProductAdmin' in the system.
     * 
     * @param productAdmin
     *            a account of a product admin, the password is needed in as 
     *            hash.
     * @param productDTO
     *            the product to create.
     */
	public void createPWhashed(AccountDTO productAdmin, ProductDTO productDTO);
    /**
     * creates a new product in system. This method is limited to user/accounts
     * who are at least 'ProductAdmin' in the system.
     * 
     * @param productAdmin
     *            a account of a product admin, the password is needed in plain
     *            text.
     * @param productDTO
     *            the product to create.
     */
	  public void create(AccountDTO productAdmin, ProductDTO productDTO);

    /**
     * updates a product, since the product gets found by its uin and system,
     * the uin and system of a product can't get changed. If you want to change
     * the uin of a product please delete the old product first and create a new
     * one. This method is limited to user/accounts who are at least
     * 'ProductAdmin' in the system.
     * 
     * @param productAdmin
     *            a account of a product admin, the password is needed in plain
     *            text.
     * 
     * @param productDTO
     *            the product to update.
     */
    public void update(AccountDTO productAdmin, ProductDTO productDTO);

    /**
     * deletes a product. This method is limited to user/accounts who are at
     * least 'ProductAdmin' in the system.
     * 
     * @param productAdmin
     *            a account of a product admin, the password is needed in plain
     *            text.
     * @param productDTO
     *            the product to delete, at least product uin and system name
     *            are needed.
     */
    public void delete(AccountDTO productAdmin, ProductDTO productDTO);

    /**
     * gets a product by the name of the system and the uin of a product. This
     * method has no limitation.
     * 
     * @param systemName
     *            name of the system in which the product exists.
     * @param productUin
     *            the uin of a product
     * @return a data transfer object of a product.
     */
    public ProductDTO get(String systemName, String productUin);

    /**
     * creates or updates a {@link ProductLocaleDTO} This method is limited to
     * user/accounts who are at least 'ProductAdmin' in the system.
     * 
     * @param pDto
     * @return the created or updated dto.
     */
    public ProductLocaleDTO putProductLocale(AccountDTO productAdmin, ProductLocaleDTO pDto);

    /**
     * creates or updates a {@link TemplatesDTO} This method is limited to
     * user/accounts who are at least 'ProductAdmin' in the system.
     * 
     * @param templatesDTO
     * @return the created or modified template
     */
    public TemplatesDTO putTemplate(AccountDTO productAdmin, TemplatesDTO templatesDTO);

    /**
     * returns a ProductLocaleDTO found by localecode and templateuin.
     * 
     * @param localecode
     * @param templateUin
     * @return
     */
    public ProductLocaleDTO getProductLocale(String localecode, String templateUin);

    /**
     * returns a ProductLocaleDTO found by localecode and product id and system id.
     * 
     * @param localecode
     * @param uin
     * @param systemId
     * @return
     */
    public ProductLocaleDTO getProductLocale(String localecode, String uin, long systemId);

    
    /**
     * returns a templateDto found by its uin.
     * 
     * @param uin
     * @return
     */
    public TemplatesDTO getTemplate(String uin);
    
   
    
    
    public List<ProductDTO> listBySystemClass(AccountDTO account,ESystemDTO systemDTO);

    /**
     * adds a product to transfers it when a given method get called.
     * 
     * @param productAdmin
     * 
     * @param productDTO
     * @param className
     * @param methodName
     * @param amount
     */
    public void transferProductByMethodcall(AccountDTO productAdmin, ProductDTO productDTO, TransferMethod method, long amount);
    

    /**
     * returns allProductlocales off the caller's system
     * 
     * @param account
     *        must be an admin
     */
	public List<ProductLocaleDTO> getAllProductLocales(AccountDTO account);
	
	/**
     * returns allProducttemplates off the caller's system
     * 
     * @param account
     *        must be an admin
     */
	public List<TemplatesDTO> getAllTemplates(AccountDTO productAdmin);

	/**
	 * creates a productlocale
	 * @param padmin should be at least a productAdmin
	 * @param plocale, the ProductLocaleDTO, to be created
	 */
	public void createProductlocale(AccountDTO padmin,ProductLocaleDTO plocale);
	
	/**gets a ProductLocaleDTO by ID
	 * @param admin
	 *        must be a ProductAdmin
	 * @param Id
	 * 		  the Id of the Product
	 * 
	 */
	
	public ProductLocaleDTO getProductLocalebyId(AccountDTO admin, long Id);
	/**normal getMethod, nothing special at all
	 * 
	 * @param admin
	 *        should be at least a productadmin
	 *        
	 * @param id
	 *        productid
	 * @return
	 *       the product, if there is no product with the id, you will got null.
	 */
	public ProductDTO get(AccountDTO admin, long id);
	
	/**This method can also change the name of the product
	 * 
	 * @param productAdmin
	 * 		  should be at least a productAdmin 
	 * @param productDTO
	 *        Product to uptade
	 */
	public void updatebyID(AccountDTO productAdmin, ProductDTO productDTO);
	
	/**Updates the productlocale
	 * 
	 * @param plocale
	 *        The productlocale needs a valid ID
	 */
	public  void updateProductlocale(AccountDTO admin, ProductLocaleDTO plocale);
    

    
}
