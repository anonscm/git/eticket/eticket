package org.evolvis.eticket.model.entity.dto;

import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;

public class ProductLocaleDTOTest {

	private ProductLocaleDTO pltdo1;
	private ProductLocaleDTO pltdo2;
	private ProductLocaleDTO pltdo3;
	private ProductLocaleDTO pltdo4;
	private ProductLocaleDTO pltdo5;

	private static class AsDouble extends BaseMatcher<BigDecimal> {

		private final double wanted;

		AsDouble(double wanted) {
			this.wanted = wanted;
		}
		
		static AsDouble is(double wanted){
			return new AsDouble(wanted);
		}

		@Override
		public boolean matches(Object arg0) {
			if (!(arg0 instanceof BigDecimal))
				return false;
			return ((BigDecimal) arg0).doubleValue() == wanted;
		}

		@Override
		public void describeTo(Description arg0) {
			arg0.appendValue(wanted);

		}

	}

	@Before
	public void before() {
		pltdo1 = ProductLocaleDTO.createNewProductLocaleDTO(
				LocaleDTO.createNewLocaleDTO("ah", "ha"), "$", "Produktname",
				"Beschreibung", null, 1999.99, 0.34);
		pltdo2 = ProductLocaleDTO.createNewProductLocaleDTO(
				LocaleDTO.createNewLocaleDTO("ah", "ha"), "$", "Produktname",
				"Beschreibung", null, 82.46, 0.27);
		pltdo3 = ProductLocaleDTO.createNewProductLocaleDTO(
				LocaleDTO.createNewLocaleDTO("ah", "ha"), "$", "Produktname",
				"Beschreibung", null, 9.00, 0.16);
		pltdo4 = ProductLocaleDTO.createNewProductLocaleDTO(
				LocaleDTO.createNewLocaleDTO("ah", "ha"), "$", "Produktname",
				"Beschreibung", null, 185.72, 0.19);
		pltdo5 = ProductLocaleDTO.createNewProductLocaleDTO(
				LocaleDTO.createNewLocaleDTO("ah", "ha"), "$", "Produktname",
				"Beschreibung", null, 19.84, 0.55);
	}


	@Test
	public void shouldHaveOnlyTwoDecimalPlaces() {
		assertThat(pltdo1.getPriceIncludingTax(), AsDouble.is(2679.99));
		assertThat(pltdo2.getPriceIncludingTax(), AsDouble.is(104.72));
		assertThat(pltdo3.getPriceIncludingTax(), AsDouble.is(10.44));
		assertThat(pltdo4.getPriceIncludingTax(), AsDouble.is(221.01));
		assertThat(pltdo5.getPriceIncludingTax(), AsDouble.is(30.75));
	}

}
