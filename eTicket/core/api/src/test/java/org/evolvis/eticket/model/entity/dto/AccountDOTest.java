package org.evolvis.eticket.model.entity.dto;

import java.util.Arrays;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.junit.Test;

public class AccountDOTest {

	private ESystemDTO eSystemDTO = ESystemDTO.createNewESystemDTO("test",
			LocaleDTO.createNewLocaleDTO("a", "b"), Arrays.asList(LocaleDTO
					.createNewLocaleDTO("a", "b")));

	@Test(expected = IllegalArgumentException.class)
	public void failToCreateCauseEverythingIsNull() {
		AccountDTO.init(eSystemDTO, null, null, null, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void failToCreateCauseUsernameIsNUll() {
		AccountDTO.hashPasswordAndGetAccount(eSystemDTO, null, "blubb",
				RoleDTO.getUser(0), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void failToCreateCausePWIsNUll() {
		AccountDTO.hashPasswordAndGetAccount(eSystemDTO, "null", null,
				RoleDTO.getUser(0), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void failToCreateCauseRoleIsNUll() {
		AccountDTO.hashPasswordAndGetAccount(eSystemDTO, "null", "null", null, null);
	}

	@Test
	public void hooray() {
		AccountDTO.hashPasswordAndGetAccount(eSystemDTO, "Arthur",
				"TheKingIsBack", RoleDTO.getPlatformAdmin(), null);
	}

}
