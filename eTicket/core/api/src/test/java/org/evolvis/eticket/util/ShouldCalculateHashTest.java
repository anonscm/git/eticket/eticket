package org.evolvis.eticket.util;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.junit.Test;

public class ShouldCalculateHashTest {

	private static final String PW = "Don't forget your towel!";
	private ESystemDTO eSystemDTO = ESystemDTO.createNewESystemDTO("aha");
	String salt = CalculateHash.generateSalt();
	String oneHash = CalculateHash.calculate(PW);
	String oneSaltedHash = CalculateHash.calculateWithSalt(salt, oneHash);

	@Test
	public void shouldGenerateTheSameHashOnSameInput() {
		String second = CalculateHash.calculate(PW);
		assertThat(second, is(oneHash));
	}

	@Test
	public void shouldGenerateDifferentHashsOnDifferentInput() {
		String second = CalculateHash.calculate("one");
		assertThat(second, is(not(oneHash)));
	}

	@Test
	public void shouldBeCorrectWhenPasswordHashOfAccountIsTheSameAsHashed() {
		AccountDTO acc = AccountDTO.init(eSystemDTO, "Fnord Prefect", PW, RoleDTO.getPlatformAdmin(), null);
		assertThat(CalculateHash.isPasswordCorrect(acc, salt, oneSaltedHash),
				is(true));
	}

	@Test
	public void shouldNotBeCorrectWhenPasswordHashOfAccountIsTheSameAsHashed() {
		AccountDTO acc = AccountDTO.init(eSystemDTO, "Fnord Prefect",
				"Forgot my towel ...", RoleDTO.getPlatformAdmin(), null);
		assertThat(CalculateHash.isPasswordCorrect(acc, salt, oneSaltedHash),
				is(not(true)));
	}
	
}
