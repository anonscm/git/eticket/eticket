package org.evolvis.eticket.model.entity.dto;

import junit.framework.Assert;
import org.junit.Test;

import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;

public class ProductDTOTest {

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionIfUinIsNull() {

		
		ProductDTO.createNewProductDTO(
				null,
				ProductType.DIGITAL,
				ESystemDTO.createNewESystemDTO("ha",
						LocaleDTO.createNewLocaleDTO("de", "deutsch"), null), null,0, 0);

	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionIfSystemIsNull() {

	

		ProductDTO.createNewProductDTO("a", ProductType.DIGITAL, null, null, 0,0);

	}

	@Test
	public void shouldNotThrowExceptionIfTemplateDtoIsNull() {

		ProductDTO.createNewProductDTO(
				"a",
				ProductType.DIGITAL,
				ESystemDTO.createNewESystemDTO("ha",
						LocaleDTO.createNewLocaleDTO("de", "deutsch"), null), null,0, 0);

	}

	@Test
	public void shouldCreateAInstance() {

		ProductLocaleDTO[] pl = {ProductLocaleDTO.createNewProductLocaleDTO(LocaleDTO.createNewLocaleDTO(
				"ah", "ha"), "$", "ui", "aii", null, 0, 0) };

		ProductDTO pDTO = ProductDTO.createNewProductDTO(
				"a",
				ProductType.DIGITAL,
				ESystemDTO.createNewESystemDTO("ha",
						LocaleDTO.createNewLocaleDTO("de", "deutsch"), null), pl,210, 0);

		Assert.assertNotNull(pDTO);
	}
}
