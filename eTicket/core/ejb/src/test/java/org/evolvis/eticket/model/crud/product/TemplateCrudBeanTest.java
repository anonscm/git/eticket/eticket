package org.evolvis.eticket.model.crud.product;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import junit.framework.Assert;

import org.evolvis.eticket.model.entity.Templates;
import org.junit.Before;
import org.junit.Test;

public class TemplateCrudBeanTest {
    private TemplateCRUDBean bean = new TemplateCRUDBean();
    private EntityManager manager = mock(EntityManager.class);
    private Query query = mock(Query.class);

    @Before
    public void init() {
	bean.setManager(manager);
	when(manager.createNamedQuery(anyString())).thenReturn(query);
    }

    @Test
    public void shouldGetById() {
	bean.findById(4242);
	verify(manager).find(Templates.class, (long) 4242);
    }

    @Test
    public void shouldReturnNullTemplateIsUnknown() {
	Templates tp = bean.findByUin("ehw");
	verify(manager).createNamedQuery("findTemplateByUin");
	verify(query).getResultList();
	Assert.assertTrue(tp == null);

    }

    @Test
    public void shouldReturnTemplate() {
	List<Templates> temps = Arrays.asList(new Templates[] { new Templates() });
	when(query.getResultList()).thenReturn(temps);
	Templates tp = bean.findByUin("ehw");
	verify(manager).createNamedQuery("findTemplateByUin");
	verify(query).getResultList();
	Assert.assertTrue(tp != null);
    }

}
