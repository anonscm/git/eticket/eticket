package org.evolvis.eticket.model.crud.platform;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.ejb.EJBException;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import junit.framework.Assert;

import org.evolvis.eticket.model.entity.PlatformAccount;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.util.MessageConstClass;
import org.junit.Before;
import org.junit.Test;

public class PlatformAccountCRUDBeanTest {
    private PlatformAccountCRUDBean bean = new PlatformAccountCRUDBean();
    private EntityManager manager = mock(EntityManager.class);
    private Query query = mock(Query.class);

    /**
     * Inits the.
     */
    @Before
    public void init() {
	bean.setManager(manager);
	when(manager.createNamedQuery(anyString())).thenReturn(query);
    }

    /**
     * Should find by id.
     */
    @Test
    public void shouldFindById() {
	bean.findById(42);
	verify(manager).find(PlatformAccount.class, (long) 42);
    }

    /**
     * Should find by username.
     */
    @Test
    public void shouldFindByUsername() {
	bean.findByUsername("uh");
	verify(manager).createNamedQuery("findPlatformAccountByUsername");
	verify(query).setParameter(1, "uh");
	verify(query).getSingleResult();
    }

    /**
     * Should list platform accounts.
     */
    @Test
    public void shouldListPlatformAccounts() {
	bean.listPlatformAccounts();
	verify(manager).createNamedQuery("listPlatformAccounts");
	verify(query, never()).setParameter(anyInt(), anyString());
	verify(query).getResultList();
    }

    /**
     * Should return account when everything is shiny.
     * 
     * @throws IllegalAccessException
     *             the illegal access exception
     */
    @Test
    public void shouldReturnAccountWhenEverythingIsShiny() throws IllegalAccessException {
	AccountDTO platformAdmin = AccountDTO.getPlatformAccount("uh", "eh", "openId@OpenId.de");
	PlatformAccount acc = new PlatformAccount(platformAdmin);
	when(query.getSingleResult()).thenReturn(acc);
	AccountDTO result = bean.checkPassword(platformAdmin).toDto();
	// well, result has a hashed pw, our admin not ...
	Assert.assertEquals(platformAdmin.getUsername(), result.getUsername());

    }

    /**
     * Should throw exception when password is wrong.
     * 
     * @throws IllegalAccessException
     *             the illegal access exception
     */
    @Test(expected = IllegalAccessException.class)
    public void shouldThrowExceptionWhenPasswordIsWrong() throws IllegalAccessException {
	AccountDTO platformAdmin = AccountDTO.getPlatformAccount("uh", "eh", "openId@OpenId.de");
	PlatformAccount acc = new PlatformAccount(platformAdmin);
	when(query.getSingleResult()).thenReturn(acc);
	try {
	    platformAdmin = AccountDTO.getPlatformAccount("uh", "ehw", "openId@OpenId.de");
	    bean.checkPassword(platformAdmin);
	} catch (IllegalAccessException e) {
	    if (!e.getMessage().equals(MessageConstClass.USER_UNKNOWN_OR_PASSWORD_INVALID))
		// haha
		Assert.fail(MessageConstClass.USER_UNKNOWN_OR_PASSWORD_INVALID);
	    throw e;
	}
    }

    /**
     * Should throw exception when user is unknown.
     * 
     * @throws IllegalAccessException
     *             the illegal access exception
     */
    @Test(expected = IllegalAccessException.class)
    public void shouldThrowExceptionWhenUserIsUnknown() throws IllegalAccessException {
	when(query.getSingleResult()).thenReturn(new EJBException());
	try {
	    bean.checkPassword(AccountDTO.getPlatformAccount("uh", "ehw", "openId@OpenId.de"));
	} catch (IllegalAccessException e) {
	    if (!e.getMessage().equals(MessageConstClass.USER_UNKNOWN_OR_PASSWORD_INVALID))
		// haha
		Assert.fail(MessageConstClass.USER_UNKNOWN_OR_PASSWORD_INVALID);
	    throw e;
	}
    }

}
