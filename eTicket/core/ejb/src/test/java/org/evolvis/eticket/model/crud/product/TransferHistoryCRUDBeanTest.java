package org.evolvis.eticket.model.crud.product;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.TransferHistory.TransferState;
import org.junit.Before;
import org.junit.Test;

public class TransferHistoryCRUDBeanTest {
	
	private TransferHistoryCRUDBean transferHistoryCRUDBean = new TransferHistoryCRUDBean();
	private EntityManager manager = mock(EntityManager.class);
	private Query query = mock(Query.class);
	
	@Before
	public void init() {
		transferHistoryCRUDBean.setManager(manager);
		when(manager.createNamedQuery(anyString())).thenReturn(query);
	}
	
	@Test
	public void shouldfindBySenderAndTransferState(){
		transferHistoryCRUDBean.findBySenderAndTransferState("", TransferState.OPEN);
		verify(manager).createNamedQuery("findHistoryBySenderUsernameAndTransferState");
		verify(query).getResultList();
	}
	
	@Test
	public void shouldfindByRecipientAndTransferState(){
		transferHistoryCRUDBean.findByRecipientUsernameAndTransferState("", TransferState.OPEN);
		verify(manager).createNamedQuery("findHistoryByRecipientUsernameAndTransferState");
		verify(query).getResultList();
	}
	
	@Test
	public void shouldfindByfindByPoolRecipientAndTransferState(){
		transferHistoryCRUDBean.findByPoolRecipientAndTransferState(new ProductPool(), new Account(), 
				TransferState.OPEN);
		verify(manager).createNamedQuery("findHistoryByPoolRecipientAndTransferState");
		verify(query).getResultList();
	}

}
