package org.evolvis.eticket.business.system;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.evolvis.eticket.business.platform.PlatformBeanService;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.platform.PlatformAccountCRUD;
import org.evolvis.eticket.model.crud.platform.PlatformCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.system.RoleCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.PlatformAccount;
import org.evolvis.eticket.model.entity.Role;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.InOrder;
import org.mockito.Matchers;

public class SystemBeanTest {

	private static final String PLATFORM_ID = "aha";

	private static class HelpVerifyEticketSystem extends
			ArgumentMatcher<ESystem> {

		private String sysname;

		HelpVerifyEticketSystem(String systemName) {
			sysname = systemName;
		}

		@Override
		public boolean matches(Object argument) {
			if (!(argument instanceof ESystem))
				return false;
			ESystem system = (ESystem) argument;
			return system.getName().equals(sysname);
		}

	}

	private SystemBean bean = new SystemBean();
	private AccountCRUD accountCRUDBeanLocal = mock(AccountCRUD.class);
	private ESystemCRUD eSystemCRUDBeanLocal = mock(ESystemCRUD.class);
	private LocaleCRUD localeCRUDBeanLocal = mock(LocaleCRUD.class);
	private PlatformAccountCRUD platformAccountCRUDBeanLocal = mock(PlatformAccountCRUD.class);
	private PlatformBeanService platformBeanLocal = mock(PlatformBeanService.class);
	private PlatformCRUD platformCrudBeanLocal = mock(PlatformCRUD.class);
	private RoleCRUD roleCRUDBeanLocal = mock(RoleCRUD.class);
	private AccountDTO pa = AccountDTO.getPlatformAccount("pa", "pa",
			"openId@OpenId.de");
	private LocaleDTO[] supportedLocales = {
			LocaleDTO.createNewLocaleDTO("de", "deutsch"),
			LocaleDTO.createNewLocaleDTO("en", "english") };

	private ESystemDTO systemDTO = ESystemDTO
			.createESystemDTOFromExsistingESystemEntity(22, "sys",
					supportedLocales[0], Arrays.asList(supportedLocales));

	private ESystemDTO simpleSys = ESystemDTO
			.createESystemDTOFromExsistingESystemEntity(22, "sys",
					supportedLocales[0], null);

	private ESystemDTO simpleSys2 = ESystemDTO
			.createESystemDTOFromExsistingESystemEntity(0, "sys",
					supportedLocales[0], null);

	private List<Locale> helpGenLocales(LocaleDTO[] supportedLocales) {
		List<Locale> locales = new ArrayList<Locale>();
		for (LocaleDTO l : supportedLocales)
			locales.add(new Locale(l));
		return locales;
	}

	private void helpSetEsystemCrud() {
		bean.seteSystemCRUDBeanLocal(eSystemCRUDBeanLocal);
		ESystem system = new ESystem();

		system.setName(systemDTO.getName());
		system.setDefaultLocale(new Locale(supportedLocales[0]));
		system.setSupportedLocales(helpGenLocales(supportedLocales));
		system.setId(systemDTO.getId());

		when(eSystemCRUDBeanLocal.findByName(systemDTO.getName())).thenReturn(
				system);
		when(eSystemCRUDBeanLocal.findById(22)).thenReturn(system);
	}

	private void helpSetLocaleCrud() {
		bean.setLocaleCRUDBeanLocal(localeCRUDBeanLocal);
		when(localeCRUDBeanLocal.findByCode("de")).thenReturn(
				new Locale(supportedLocales[0]));
		when(localeCRUDBeanLocal.findByCode("en")).thenReturn(
				new Locale(supportedLocales[1]));
	}

	private void helpSetPlatformAccountCrud() {
		bean.setPlatformAccountCRUDBeanLocal(platformAccountCRUDBeanLocal);
		when(platformAccountCRUDBeanLocal.findByUsername("pa")).thenReturn(
				new PlatformAccount(pa));
	}

	private void helpSetRoleCrud() {
		bean.setRoleCRUDBeanLocal(roleCRUDBeanLocal);
		Role[] value = { new Role(), new Role() };
		when(roleCRUDBeanLocal.listSystem((ESystem) anyObject())).thenReturn(
				Arrays.asList(value));
	}

	@Before
	public void init() {
		bean.setAccountCRUDBeanLocal(accountCRUDBeanLocal);
		helpSetEsystemCrud();
		helpSetLocaleCrud();
		helpSetPlatformAccountCrud();
		bean.setPlatformBeanLocal(platformBeanLocal);
		bean.setPlatformCrudBeanLocal(platformCrudBeanLocal);
		helpSetRoleCrud();
	}

	@Test
	public void shouldDelete() {
		InOrder inOrder = inOrder(eSystemCRUDBeanLocal, platformBeanLocal);

		bean.deleteSystem(pa, PLATFORM_ID, systemDTO);

		inOrder.verify(eSystemCRUDBeanLocal).findByName(systemDTO.getName());

		inOrder.verify(platformBeanLocal).deleteSystemFromPlatform(pa,
				PLATFORM_ID, simpleSys);

		inOrder.verify(eSystemCRUDBeanLocal).delete(
				Matchers.argThat(new HelpVerifyEticketSystem(systemDTO
						.getName())));
	}

	@Test
	public void shouldGetById() {
		ESystemDTO result = bean.getSystemById(22);
		verify(eSystemCRUDBeanLocal).findById(22);
		Assert.assertEquals(systemDTO, result);

	}

	@Test
	public void shouldGetByName() {
		ESystemDTO result = bean.getSystemByName(systemDTO.getName());
		verify(eSystemCRUDBeanLocal).findByName(systemDTO.getName());
		Assert.assertEquals(systemDTO, result);
	}

	@Test
	public void shouldInsertSystem() {
		InOrder inOrder = inOrder(localeCRUDBeanLocal, eSystemCRUDBeanLocal,
				platformCrudBeanLocal, platformAccountCRUDBeanLocal,
				accountCRUDBeanLocal, platformBeanLocal);
		bean.createSystem(pa, PLATFORM_ID, systemDTO);
		inOrder.verify(localeCRUDBeanLocal, times(3)).findByCode(anyString());
		inOrder.verify(platformCrudBeanLocal).findByName(PLATFORM_ID);
		inOrder.verify(eSystemCRUDBeanLocal).insert(
				Matchers.argThat(new HelpVerifyEticketSystem(systemDTO
						.getName())));

		inOrder.verify(platformBeanLocal).addSystemToPlatform(pa, PLATFORM_ID,
				simpleSys2);

		inOrder.verify(platformAccountCRUDBeanLocal).findByUsername("pa");
		inOrder.verify(accountCRUDBeanLocal).insert((Account) anyObject());
	}

	@Test
	public void shouldUpdSys() {
		bean.updateSystem(pa, PLATFORM_ID, systemDTO);
		verify(eSystemCRUDBeanLocal).findById(systemDTO.getId());
		verify(eSystemCRUDBeanLocal).update((ESystem) anyObject());
	}
}
