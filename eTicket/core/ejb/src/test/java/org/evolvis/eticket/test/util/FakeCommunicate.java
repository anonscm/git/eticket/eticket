package org.evolvis.eticket.test.util;

import java.util.Map;

import org.evolvis.eticket.business.communication.Communicate;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.TextTemplate;

public class FakeCommunicate implements Communicate {

    @Override
    public void sendTo(Account sender, Account recipient, TextTemplate template, Map<String, String> buzzwordsAndValues) {

    }

}
