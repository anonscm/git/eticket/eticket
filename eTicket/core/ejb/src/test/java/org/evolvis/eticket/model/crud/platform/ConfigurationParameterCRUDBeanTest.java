package org.evolvis.eticket.model.crud.platform;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.ConfigurationParameter;
import org.evolvis.eticket.model.entity.ESystem;
import org.junit.Before;
import org.junit.Test;

public class ConfigurationParameterCRUDBeanTest {
    private static final String KEY = "holla";
    private ConfigurationParameterCRUDBean bean = new ConfigurationParameterCRUDBean();
    private EntityManager manager = mock(EntityManager.class);
    private Query query = mock(Query.class);
    private ESystem system = new ESystem();

    @Before
    public void init() {
	bean.setManager(manager);
	when(manager.createNamedQuery(anyString())).thenReturn(query);
    }

    @Test
    public void shouldFindById() {
	bean.findById(12);
	verify(manager).find(ConfigurationParameter.class, (long) 12);
    }

    @Test
    public void shouldFindSystemAndName() {
	bean.findBySystemAndKey(system, KEY);
	verify(manager).createNamedQuery("findConfigurationParameterBySystemAndKey");
	verify(query).setParameter(1, system);
	verify(query).setParameter(2, KEY);
    }

    @Test
    public void shouldListCPInSystem() {
	bean.listConfigurationParameterInSystem(system);
	verify(manager).createNamedQuery("listConfigurationParameterInSystem");
	verify(query).setParameter(1, system);

    }

}
