package org.evolvis.eticket.model.crud.user;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.Credentials;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.util.CalculateHash;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AccountCRUDBeanTest {
	private AccountCRUDBean bean = new AccountCRUDBean();
	private EntityManager manager = mock(EntityManager.class);
	private Query query = mock(Query.class);
	private ESystem system = new ESystem();

	@Before
	public void init() {
		bean.setManager(manager);
		when(manager.createNamedQuery(anyString())).thenReturn(query);
	}
	
	@Test
	public void shouldExecuteFindAccountBySystemAndUsername() {
		bean.findBySystemAndUserName(system, "test");
		verify(manager).createNamedQuery("findAccountBySystemAndUsername");
		verify(query).getSingleResult();
	}

	@Test
	public void shouldExecuteListInactiveAccountsInSystem() {
		bean.listInactiveAccountsInSystem(system);
		verify(manager).createNamedQuery("listInactiveAccountsInSystem");
		verify(query).getResultList();
	}

	@Test
	public void shouldReturnAccountWhenPwIsCorrect()
			throws IllegalAccessException {
		ESystemDTO sys = ESystemDTO.createNewESystemDTO("sys", LocaleDTO.createNewLocaleDTO("sy", "haha"),
				null);
		AccountDTO accountDTO = AccountDTO.init(sys, "Test", "test1",
				RoleDTO.getModerator(0), "openId@OpenId.de");
		Account acc = new Account();
		String salt = CalculateHash.generateSalt();
		acc.setCredential(new Credentials("Test", salt,CalculateHash.calculateWithSalt(salt, CalculateHash.calculate("test1")), null));
		when(query.getSingleResult()).thenReturn(acc);
		Assert.assertEquals(acc, bean.checkPwGetAccount(system, accountDTO));
		verify(manager).createNamedQuery("findAccountBySystemAndUsername");
		verify(query).getSingleResult();
	}

	@Test(expected = IllegalAccessException.class)
	public void shouldThrowExceptionWhenNoResult()
			throws IllegalAccessException {
		ESystemDTO sys = ESystemDTO.createNewESystemDTO("sys", LocaleDTO.createNewLocaleDTO("sy", "haha"),
				null);
		AccountDTO accountDTO = AccountDTO.init(sys, "Test", "hadjk",
				RoleDTO.getModerator(0), "openId@OpenId.de");
		when(query.getSingleResult()).thenReturn(new NoResultException());
		bean.checkPwGetAccount(system, accountDTO);
	}

	@Test(expected = IllegalAccessException.class)
	public void shouldThrowExceptionWhenPwIsWrong()
			throws IllegalAccessException {
		ESystemDTO sys = ESystemDTO.createNewESystemDTO("sys", LocaleDTO.createNewLocaleDTO("sy", "haha"),
				null);
		AccountDTO accountDTO = AccountDTO.init(sys, "Test", "test1",
				RoleDTO.getModerator(0), "openId@OpenId.de");
		Account acc = new Account();
		String salt = CalculateHash.generateSalt();
		acc.setCredential(new Credentials("Test", salt,CalculateHash.calculateWithSalt(salt, CalculateHash.calculate("test")), null));
//		acc.setCredential(new Credentials("Test", Hash.getHash("test"), null));
		when(query.getSingleResult()).thenReturn(acc);
		bean.checkPwGetAccount(system, accountDTO);
	}
	
	@Test
	public void shouldfindAccountBySystemAndOpenId(){
		bean.findBySystemAndOpenId(system, "test");
		verify(manager).createNamedQuery("findAccountBysystemAndOpenid");
		verify(query).getSingleResult();
	}
}
