package org.evolvis.eticket.model.entity;

import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;
import org.junit.Assert;
import org.junit.Test;

public class ProductPoolTest {
	private ProductPool pool = new ProductPool();

	@Test
	public void shouldBe1WhenAmountIsNotSet() {
		Assert.assertEquals(1, pool.getAmount());
	}

	@Test
	public void shouldInsertActor() {
		Account account = new Account();
		pool.setActor(account);
		Assert.assertEquals(account, pool.getActor());
	}

	@Test
	public void shouldInsertAmount() {
		pool.setAmount(10);
		Assert.assertEquals(10, pool.getAmount());
	}

	@Test
	public void shouldInsertAProduct() {
		Product product = new Product();
		pool.setProduct(product);
		Assert.assertEquals(product, pool.getProduct());
	}

	@Test
	public void shouldInsertFixedType() {
		pool.setType(ProductPoolStatus.FIXED);
		Assert.assertEquals(ProductPoolStatus.FIXED, pool.getType());
	}

	@Test
	public void shouldInsertRegisteredType() {
		pool.setType(ProductPoolStatus.REGISTERED);
		Assert.assertEquals(ProductPoolStatus.REGISTERED, pool.getType());
	}

	@Test
	public void shouldInsertVolatileType() {
		pool.setType(ProductPoolStatus.VOLATILE);
		Assert.assertEquals(ProductPoolStatus.VOLATILE, pool.getType());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenProductIsNUll() {
		pool.setProduct(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenTypeIsNull() {
		pool.setType(null);
	}

}
