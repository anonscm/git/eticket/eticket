package org.evolvis.eticket.model.crud.user;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ESystem;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ActivateAccountTokenCRUDBeanTest {
    private ActivateAccountTokenCRUDBean bean = new ActivateAccountTokenCRUDBean();
    private EntityManager manager = mock(EntityManager.class);
    private Query query = mock(Query.class);
    private ESystem system = new ESystem();
    private Account account = new Account();
    private Date validUntil = new Date();

    @Before
    public void init() {
	bean.setManager(manager);
	when(manager.createNamedQuery(anyString())).thenReturn(query);
    }

    @Test
    public void shouldCreateActivateAccountToken() {
	Assert.assertNotNull(bean.createNewToken(account, validUntil));
    }

    @Test
    public void shouldExecuteDeleteOutdatedActivateAccountTokenInSystem() {
	bean.deleteOutdatedInSystem(system);
	verify(manager).createNamedQuery("deleteOutdatedActivateAccountTokenInSystem");
	verify(query).executeUpdate();
    }

    @Test
    public void shouldExecutefindActivateAccountTokenByAccountAndToken() {
	bean.findByAccountAndToken(account, "12345678");
	verify(manager).createNamedQuery("findActivateAccountTokenByAccountAndToken");
	verify(query).getSingleResult();
    }

    @Test
    public void shouldExecuteListOutdatedActivateAccountTokenInSystem() {
	bean.listOutdatedInSystem(system);
	verify(manager).createNamedQuery("listOutdatedActivateAccountTokenInSystem");
	verify(query).getResultList();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateActivateAccountTokenWhenAccountIsNull() {
	Assert.assertNotNull(bean.createNewToken(null, validUntil));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateActivateAccountTokenWhenDateIsNull() {
	Assert.assertNotNull(bean.createNewToken(account, null));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfTryingToFindAccountByTokenWhenAccountIsNull() {
	bean.findByAccountAndToken(null, "12345678");
    }

}
