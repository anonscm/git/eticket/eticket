package org.evolvis.eticket.test.business;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.eticket.business.product.ProductPoolBean;
import org.evolvis.eticket.model.crud.product.ProductPoolCRUD;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class ProductPoolBeanTest {
	
	private ProductPoolCRUD productPoolCRUDLocal = Mockito.mock(ProductPoolCRUD.class);
	private List<ProductPool> productPools = new ArrayList<ProductPool>();
	private ProductPoolBean productPoolBean = new ProductPoolBean();
	
	@Before
	public void init(){
		Mockito.when(productPoolCRUDLocal.findByStatusAndUsername
				(Mockito.eq(ProductPoolStatus.VOLATILE), Mockito.anyString())).thenReturn(productPools);
		productPoolBean.setProductPoolCRUDLocal(productPoolCRUDLocal);
	}
	
	@Test
	public void shouldGetListOfProductPool(){
		productPoolBean.getProductPoolList("VOLATILE", "");
		Mockito.verify(productPoolCRUDLocal).findByStatusAndUsername(ProductPoolStatus.VOLATILE, "");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldNotGetListOfProductPool(){
		productPoolBean.getProductPoolList("Sheldonopolis", "");
	}
	
}
