package org.evolvis.eticket.model.crud.system;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.ESystem;
import org.junit.Before;
import org.junit.Test;

public class RoleCrudBeanTest {

    private RoleCRUDBean bean = new RoleCRUDBean();

    private EntityManager manager = mock(EntityManager.class);
    private Query query = mock(Query.class);
    private ESystem system = new ESystem();

    @Before
    public void init() {
	bean.setManager(manager);
	when(manager.createNamedQuery(anyString())).thenReturn(query);
    }

    @Test
    public void shouldExecuteFindRoleBySystemAndNameSQL() {
	bean.findBySystemAndName(system, "blubb");
	verify(manager).createNamedQuery("findRoleBySystemAndName");
	verify(query).getSingleResult();
    }

    @Test
    public void shouldExecuteListRolesInSystem() {
	bean.listSystem(system);
	verify(manager).createNamedQuery("listRolesInSystem");
	verify(query).getResultList();
    }

}
