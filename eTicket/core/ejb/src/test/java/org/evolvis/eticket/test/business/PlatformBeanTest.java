package org.evolvis.eticket.test.business;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.eticket.business.platform.PlatformBean;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.platform.PlatformAccountCRUD;
import org.evolvis.eticket.model.crud.platform.PlatformCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.Platform;
import org.evolvis.eticket.model.entity.PlatformAccount;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PlatformBeanTest {
	private static final String ID = "aha";
	private PlatformBean platformBean = new PlatformBean();
	private LocaleCRUD localeCRUDBeanLocal = mock(LocaleCRUD.class);
	private ESystemCRUD eSystemCRUDBeanLocal = mock(ESystemCRUD.class);
	private PlatformAccountCRUD platformAccountCRUDBeanLocal = mock(PlatformAccountCRUD.class);
	private PlatformCRUD platformCRUDBeanLocal = mock(PlatformCRUD.class);
	private AccountDTO platformAdmin = AccountDTO.getPlatformAccount(ID,
			"sow", "openId@OpenId.de");
	private LocaleDTO localeDto = LocaleDTO.createNewLocaleDTO("de", "ehw");
	private ESystemDTO system =ESystemDTO.createNewESystemDTO(ID, localeDto, null);
	private Platform platform = new Platform("haha");
	private PlatformAccount platformAccount = new PlatformAccount(platformAdmin);
	
	private Locale helpCreateLocale() {
		Locale locale = new Locale(localeDto);
		when(localeCRUDBeanLocal.findByCode("de")).thenReturn(locale);
		return locale;
	}

	@Before
	public void init() {
		platformBean.seteSystemCRUDBeanLocal(eSystemCRUDBeanLocal);
		platformBean.setLocaleCRUDBeanLocal(localeCRUDBeanLocal);
		platformBean
				.setPlatformAccountCRUDBeanLocal(platformAccountCRUDBeanLocal);
		platformBean.setPlatformCRUDBeanLocal(platformCRUDBeanLocal);
		when(platformCRUDBeanLocal.findByName(ID)).thenReturn(platform);
		platform.setOwner(platformAccount);		
	}

	@Test
	public void shouldAddSystemToPlatform() {
		platformBean.addSystemToPlatform(platformAdmin, ID, system);
		verify(eSystemCRUDBeanLocal).findByName(system.getName());
		verify(platformCRUDBeanLocal).update(platform);
	}

	@Test
	public void shouldCreateLocale() {
		platformBean.createLocale(platformAdmin, ID, localeDto);
		verify(localeCRUDBeanLocal).insert((Locale) anyObject());
	}

	@Test
	public void shouldCreatePlatformAccountAndPlatformWhenNoPlatformadminIsKnown()
			throws IllegalAccessException {
		platformBean.createPlatform(platformAdmin, "haha");
		verify(platformAccountCRUDBeanLocal).listPlatformAccounts();
		verify(platformAccountCRUDBeanLocal).insert(
				(PlatformAccount) anyObject());
		verify(platformCRUDBeanLocal).insert((Platform) anyObject());
	}

	@Test
	public void shouldCreatePlatformButNoAccountWhenPlatformadminIsKnown()
			throws IllegalAccessException {
		List<PlatformAccount> value = new ArrayList<PlatformAccount>();
		PlatformAccount platformAccount = mock(PlatformAccount.class);
		value.add(platformAccount);
		when(platformAccountCRUDBeanLocal.listPlatformAccounts()).thenReturn(
				value);
		platformBean.createPlatform(platformAdmin, "haha");
		verify(platformAccountCRUDBeanLocal).listPlatformAccounts();
		verify(platformAccountCRUDBeanLocal).checkPassword(platformAdmin);
		verify(platformCRUDBeanLocal).insert((Platform) anyObject());
	}

	@Test
	public void shouldDeleteLocale() {
		Locale locale = helpCreateLocale();
		platformBean.deleteLocale(platformAdmin, ID, "de");
		verify(localeCRUDBeanLocal).findByCode("de");
		verify(localeCRUDBeanLocal).delete(locale);
	}

	@Test
	public void shouldDeletePlatform() {
		platformBean.deletePlatform(platformAdmin, ID);
		verify(platformCRUDBeanLocal).findByName(ID);
		verify(platformCRUDBeanLocal).delete(platform);
	}

	@Test
	public void shouldDeleteSystemFromPlatform() {
		platformBean.deleteSystemFromPlatform(platformAdmin, ID, system);
		verify(eSystemCRUDBeanLocal).findByName(system.getName());
		verify(platformCRUDBeanLocal).update(platform);
	}

	@Test
	public void shouldGetLocale() {
		helpCreateLocale();
		LocaleDTO result = platformBean.getLocale("de");
		verify(localeCRUDBeanLocal).findByCode("de");
		Assert.assertEquals(localeDto, result);
	}

}
