/**
 * 
 */
package org.evolvis.eticket.test.business;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.evolvis.eticket.business.communication.Communicate;
import org.evolvis.eticket.business.communication.EMailBean;
import org.evolvis.eticket.business.system.account.AccountBean;
import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.system.RoleCRUD;
import org.evolvis.eticket.model.crud.system.TextTemplateCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.crud.user.ActivateAccountTokenCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ActivateAccountToken;
import org.evolvis.eticket.model.entity.ConfigurationParameter;
import org.evolvis.eticket.model.entity.Credentials;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.PersonalInformation;
import org.evolvis.eticket.model.entity.Platform;
import org.evolvis.eticket.model.entity.Role;
import org.evolvis.eticket.model.entity.TextTemplate;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.util.CalculateHash;
import org.evolvis.eticket.util.GenericServiceLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * @author phil
 * 
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ GenericServiceLoader.class })
public class AccountBeanTest {

	/**
	 * 
	 */
	private static final String LOCALECODE = "de";
	private AccountBean accountBean = new AccountBean();
	private ESystemDTO systemCrud = ESystemDTO.createNewESystemDTO("TestSys",
			LocaleDTO.createNewLocaleDTO(LOCALECODE, "..."), null);
	private AccountDTO accountDTO = AccountDTO.init(systemCrud, "test", "aha",
			RoleDTO.getSysAdmin(1), "openId@OpenId.de");
	private AccountCRUD accountCRUDBeanLocal = mock(AccountCRUD.class);
	private ActivateAccountTokenCRUD activateAccountTokenCRUDBeanLocal = mock(ActivateAccountTokenCRUD.class);
	private ConfigurationParameterCRUD configurationParameterBeanLocal = mock(ConfigurationParameterCRUD.class);
	private ESystemCRUD eSystemCRUDBeanLocal = mock(ESystemCRUD.class);
	private LocaleCRUD localeCRUDBeanLocal = mock(LocaleCRUD.class);
	private RoleCRUD roleCRUDBeanLocal = mock(RoleCRUD.class);
	private Communicate send = mock(Communicate.class);
	private TextTemplateCRUD textTemplateCRUDBeanLocal = mock(TextTemplateCRUD.class);
	private Account account;
	private PersonalInformation personalInformation = new PersonalInformation();
	private PersonalInformationDTO personalInformationDTO = PersonalInformationDTO
			.createNewPersonalInformationDTO("", "", "", "", "", "", "", "",
					LocaleDTO.createNewLocaleDTO(LOCALECODE, "..."), "", "");

	private ActivateAccountToken helpCreateAccountAndToken(ESystem system) {
		final ActivateAccountToken accountToken = new ActivateAccountToken();
		account = new Account();
		String salt = CalculateHash.generateSalt();
		account.setCredential(new Credentials("TestSys", salt, CalculateHash.calculateWithSalt(salt, "dhskahdjg2ug"), null));
		account.setSystem(system);
		helpSetPersonalInformation();
		account.setPersonalInformation(personalInformation);
		accountToken.setAccount(account);
		final List<Role> roles = new ArrayList<Role>();
		roles.add(new Role());
		account.setRoles(roles);
		return accountToken;
	}

	private void helpSetPersonalInformation() {
		personalInformation.setAddress("");
		personalInformation.setCity("");
		personalInformation.setCountry("");
		personalInformation.setEmail("");
		personalInformation.setFirstname("");
		personalInformation.setLastname("");
		personalInformation.setLocale(new Locale(LocaleDTO.createNewLocaleDTO(LOCALECODE,
				"...")));
		personalInformation.setOrganisation("");
		personalInformation.setSalutation("");
		personalInformation.setTitle("");
		personalInformation.setZip("");
	}

	private ESystem helpCreateSystem() {
		final ESystem system = new ESystem();
		system.setName("TestSys");
		system.setPlatform(new Platform("haha"));
		system.setDefaultLocale(new Locale(LocaleDTO.createNewLocaleDTO(LOCALECODE, "...")));
		return system;
	}

	private void helpSetAccountBean() {
		accountBean.setAccountCRUDBeanLocal(accountCRUDBeanLocal);
		accountBean
				.setActivateAccountTokenCRUDBeanLocal(activateAccountTokenCRUDBeanLocal);
		accountBean
				.setConfigurationParameterBeanLocal(configurationParameterBeanLocal);
		accountBean.seteSystemCRUDBeanLocal(eSystemCRUDBeanLocal);
		accountBean.setLocaleCRUDBeanLocal(localeCRUDBeanLocal);
		accountBean.setRoleCRUDBeanLocal(roleCRUDBeanLocal);
		mockStatic(GenericServiceLoader.class);
		Mockito.when(
				GenericServiceLoader.get(Communicate.class, EMailBean.class))
				.thenReturn(send);
		accountBean.setTextTemplateCRUDBeanLocal(textTemplateCRUDBeanLocal);
	}

	private void helpSetReturnValuesInMocks() {
		final ESystem system = helpCreateSystem();
		final ActivateAccountToken accountToken = helpCreateAccountAndToken(system);
		when(
				accountCRUDBeanLocal.findBySystemAndUserName(
						(ESystem) anyObject(), eq(accountDTO.getUsername())))
				.thenReturn(account);
		when(eSystemCRUDBeanLocal.findByName("TestSys")).thenReturn(system);
		when(
				activateAccountTokenCRUDBeanLocal.createNewToken(
						(Account) anyObject(), (Date) anyObject())).thenReturn(
				accountToken);
		when(eSystemCRUDBeanLocal.findById(account.getSystem().getId()))
				.thenReturn(account.getSystem());
		when(
				accountCRUDBeanLocal.findBySystemAndUserName(account.getSystem(),
						accountDTO.getUsername())).thenReturn(account);
		when(eSystemCRUDBeanLocal.findByName(systemCrud.getName())).thenReturn(
				account.getSystem());
		ConfigurationParameter value = new ConfigurationParameter();
		value.setValue("1234");
		when(
				configurationParameterBeanLocal.findBySystemAndKey(
						(ESystem) anyObject(), Matchers.anyString()))
				.thenReturn(value);
	}

	@Before
	public void init() {
		helpSetAccountBean();
		helpSetReturnValuesInMocks();
	}

	@Test
	public void shouldActivateAccount() {
		accountBean.activate(accountDTO, "1234");
		verify(activateAccountTokenCRUDBeanLocal).delete(
				(ActivateAccountToken) anyObject());
	}

	@Test
	public void shouldCheckPwOfAccount() throws IllegalAccessException {
		when(
				accountCRUDBeanLocal.checkPwGetAccount(account.getSystem(),
						accountDTO)).thenReturn(account);
		accountBean.validatePassword(accountDTO);
		verify(accountCRUDBeanLocal).checkPwGetAccount(account.getSystem(),
				accountDTO);
	}

	@Test
	public void shouldCreateAccount() {
		accountBean.create(accountDTO, accountDTO);
		verify(accountCRUDBeanLocal).insert((Account) anyObject());
		verify(activateAccountTokenCRUDBeanLocal, never()).createNewToken(
				(Account) anyObject(), (Date) anyObject());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCreateUserAccount() {
		AccountDTO useraccount = AccountDTO.init(systemCrud, "test", "aha",
				RoleDTO.getUser(1), "openId@OpenId.de");
		accountBean.createUser(useraccount, LOCALECODE);
		verify(accountCRUDBeanLocal).insert((Account) anyObject());
		verify(activateAccountTokenCRUDBeanLocal).createNewToken(
				(Account) anyObject(), (Date) anyObject());
		verify(send).sendTo((Account) anyObject(), (Account) anyObject(),
				(TextTemplate) anyObject(), (Map<String, String>) anyObject());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCreateUserAccountWithoutHashedPw() {
		AccountDTO useraccount = AccountDTO.init(systemCrud, "test", "aha",
				RoleDTO.getUser(1), "openId@OpenId.de");
		accountBean.createUser(useraccount, LOCALECODE);
		verify(accountCRUDBeanLocal).insert((Account) anyObject());
		verify(activateAccountTokenCRUDBeanLocal).createNewToken(
				(Account) anyObject(), (Date) anyObject());
		verify(send).sendTo((Account) anyObject(), (Account) anyObject(),
				(TextTemplate) anyObject(), (Map<String, String>) anyObject());
	}

	@Test
	public void shouldDeleteAccount() {
		accountBean.delete(accountDTO, accountDTO);
		verify(accountCRUDBeanLocal).delete(account);
	}

	@Test
	public void shouldFindAccount() {
		accountBean.find(accountDTO.getSystem(), accountDTO.getUsername());
		verify(accountCRUDBeanLocal).findBySystemAndUserName(
				(ESystem) anyObject(), eq(accountDTO.getUsername()));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldGenerateToken() {
		accountBean.generateToken(accountDTO);
		verify(activateAccountTokenCRUDBeanLocal).createNewToken(
				(Account) anyObject(), (Date) anyObject());
		verify(send).sendTo((Account) anyObject(), (Account) anyObject(),
				(TextTemplate) anyObject(), (Map<String, String>) anyObject());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotCreateUserAccountWhenAccountIsNotAUser() {
		accountBean.createUser(accountDTO, LOCALECODE);
	}

	@Test
	public void shouldUpdateAccount() {
		accountBean.update(accountDTO, accountDTO);
		verify(accountCRUDBeanLocal).update(account);
	}

	@Test
	public void shouldGetPersonalinformation() {
		accountBean.getPersonalInformationDTO(accountDTO.getSystem(),
				accountDTO.getUsername());
		verify(accountCRUDBeanLocal).findBySystemAndUserName(
				(ESystem) anyObject(), eq(accountDTO.getUsername()));
	}

	@Test
	public void shouldUpdatePersonalInformation() {
		accountBean.UpdatePersonalInformation(accountDTO,
				personalInformationDTO);
		verify(accountCRUDBeanLocal).findBySystemAndUserName(
				(ESystem) anyObject(), eq(accountDTO.getUsername()));
		verify(accountCRUDBeanLocal).update((Account) anyObject());
	}
	
	@Test
	public void shouldgetActivationState(){
		accountBean.isActivated(accountDTO.getSystem(), accountDTO.getUsername());
		verify(accountCRUDBeanLocal).findBySystemAndUserName(
				(ESystem) anyObject(), eq(accountDTO.getUsername()));
	}
}
