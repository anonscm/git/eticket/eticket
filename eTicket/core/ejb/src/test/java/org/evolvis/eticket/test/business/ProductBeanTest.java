package org.evolvis.eticket.test.business;

import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.evolvis.eticket.business.product.ProductBean;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.product.ProductCRUD;
import org.evolvis.eticket.model.crud.product.ProductLocaleCRUD;
import org.evolvis.eticket.model.crud.product.TemplateCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductLocale;
import org.evolvis.eticket.model.entity.Templates;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Matchers;
import org.mockito.Mockito;

public class ProductBeanTest {

	private ProductBean productBean = new ProductBean();
	private ProductCRUD productCRUDBeanLocal = mock(ProductCRUD.class);
	private ESystemCRUD eSystemCRUDBeanLocal = mock(ESystemCRUD.class);
	private TemplateCRUD templateCRUDBeanLocal = mock(TemplateCRUD.class);
	private LocaleDTO localeDTO = LocaleDTO
			.createLocaleDTOFromExsistingLocaleEntity(2, "de", "deutsch");
	private ESystemDTO system = ESystemDTO
			.createESystemDTOFromExsistingESystemEntity(3, "mhm", localeDTO,
					null);
	private TemplatesDTO template = TemplatesDTO.createNewTemplatesDTO("test",
			"Hallo!".getBytes(), "example/plain", null);

	private ProductLocaleDTO[] productlocales = { ProductLocaleDTO
			.createProductLocaleDTOFromExsistingProductLocaleEntity(22,
					localeDTO, "€", "ProductTest", "ProductDes", template, 0,
					19) };
	private ProductDTO productDTO = ProductDTO
			.createProductDTOFromExsistingProductEntity("a",
					ProductType.DIGITAL, system, productlocales, 0, 0, null, 0,
					12);
	private AccountDTO productAdmin = AccountDTO.init(system,
			"pAdmin", "jo",

			RoleDTO.getProductAdmin(0), "openId@OpenId.de");
	private ProductLocaleCRUD productLocaleCRUDBeanLocal = mock(ProductLocaleCRUD.class);
	private LocaleCRUD localeCRUDBeanLocal = mock(LocaleCRUD.class);
	private Product product = new Product(productDTO);
	private ESystem eSystem = new ESystem();

	private void helpSetLocale() {
		Locale locale = new Locale(localeDTO);

		Mockito.when(localeCRUDBeanLocal.findByCode(localeDTO.getCode()))
				.thenReturn(locale);
		productBean.setLocaleCRUDBeanLocal(localeCRUDBeanLocal);
	}

	private void helpSetProduct() {
		product.setSystem(eSystem);
		List<ProductLocale> locales = new ArrayList<ProductLocale>();
		for (ProductLocaleDTO p : productlocales) {
			ProductLocale e = new ProductLocale(p);
			e.setLocale(new Locale(localeDTO));
			locales.add(e);
		}
		product.setLocales(locales);
		Mockito.when(productCRUDBeanLocal.findBySystemAndUin(eSystem, "a"))
				.thenReturn(product);
	}

	private void helpSetSystem() {
		ArrayList<Locale> al = new ArrayList<Locale>();
		Locale locale = new Locale(localeDTO);
		al.add(locale);

		eSystem.setName(system.getName());
		eSystem.setId(system.getId());
		eSystem.setDefaultLocale(new Locale(localeDTO));
		eSystem.setSupportedLocales(al);

		Mockito.when(eSystemCRUDBeanLocal.findByName("mhm"))
				.thenReturn(eSystem);
	}

	private void helpSetTemplate() {
		Templates temp = new Templates();
		temp.setUin("test");
		temp.setBinaryData(new byte[] { 042, 0x23 });
		temp.setMimeType("example/application");
		Mockito.when(templateCRUDBeanLocal.findByUin("test")).thenReturn(temp);
	}

	@Before
	public void init() {
		productBean.setProductCRUDBeanLocal(productCRUDBeanLocal);
		productBean.setESystemCRUDBeanLocal(eSystemCRUDBeanLocal);
		productBean.setTemplateCRUDBeanLocal(templateCRUDBeanLocal);
		productBean.setProductLocaleCRUDBeanLocal(productLocaleCRUDBeanLocal);

		helpSetSystem();
		helpSetProduct();
		helpSetTemplate();
		helpSetLocale();
	}

	@Test
	public void shouldCreate() {
		InOrder inOrder = Mockito.inOrder(eSystemCRUDBeanLocal,
				templateCRUDBeanLocal, localeCRUDBeanLocal,
				productCRUDBeanLocal);
		productBean.create(productAdmin, productDTO);
		inOrder.verify(eSystemCRUDBeanLocal).findByName("mhm");
		inOrder.verify(templateCRUDBeanLocal).findByUin("test");

		inOrder.verify(localeCRUDBeanLocal).findByCode("de");
		inOrder.verify(productCRUDBeanLocal).insert(
				(Product) Matchers.anyObject());
	}

	@Test
	public void shouldCreateProductLocale() {

		ProductLocaleDTO productLocaleDTO = ProductLocaleDTO
				.createNewProductLocaleDTO(localeDTO, "cow", "blubb", "blob",
						null, 23.42, 0.19);

		productBean.putProductLocale(productAdmin, productLocaleDTO);
		Mockito.verify(productLocaleCRUDBeanLocal).insert(
				(ProductLocale) Matchers.anyObject());
		Mockito.verify(templateCRUDBeanLocal, Mockito.never()).update(
				(Templates) Matchers.anyObject());
	}

	@Test
	public void shouldCreateProductLocaleUpdateTemplate() {
		productBean.putProductLocale(productAdmin, productlocales[0]);
		Mockito.verify(productLocaleCRUDBeanLocal).insert(
				(ProductLocale) Matchers.anyObject());
		Mockito.verify(templateCRUDBeanLocal).update(
				(Templates) Matchers.anyObject());
	}

	@Test
	public void shouldCreateTemplate() {
		TemplatesDTO templatesDTO = productlocales[0].getTemplate();
		Mockito.when(templateCRUDBeanLocal.findByUin(templatesDTO.getUin()))
				.thenReturn(null);
		productBean.putTemplate(productAdmin, templatesDTO);
		Mockito.verify(templateCRUDBeanLocal).insert(
				(Templates) Matchers.anyObject());
	}

	@Test
	public void shouldDelete() {
		InOrder inOrder = Mockito.inOrder(eSystemCRUDBeanLocal,
				templateCRUDBeanLocal, productCRUDBeanLocal);
		productBean.delete(productAdmin, productDTO);
		inOrder.verify(eSystemCRUDBeanLocal).findByName("mhm");
		inOrder.verify(productCRUDBeanLocal).findBySystemAndUin(eSystem, "a");
		inOrder.verify(productCRUDBeanLocal).delete(product);
	}

	@Test
	public void shouldGet() {
		InOrder inOrder = Mockito.inOrder(eSystemCRUDBeanLocal,
				templateCRUDBeanLocal, productCRUDBeanLocal);

		ProductDTO result = productBean.get(system.getName(),
				productDTO.getUin());

		Assert.assertTrue(result.equals(productDTO));

		inOrder.verify(eSystemCRUDBeanLocal).findByName("mhm");

		inOrder.verify(productCRUDBeanLocal).findBySystemAndUin(eSystem, "a");
	}

	@Test
	public void shouldReturnProductLocale() {
		Mockito.when(
				productLocaleCRUDBeanLocal.findByLocaleCodeTemplateUin("de",
						"test")).thenReturn(product.getLocales().get(0));
		productBean.getProductLocale("de", "test");
		Mockito.verify(productLocaleCRUDBeanLocal).findByLocaleCodeTemplateUin(
				"de", "test");
	}

	@Test
	public void shouldReturnTemplate() {

		productBean.getTemplate("test");
		Mockito.verify(templateCRUDBeanLocal).findByUin("test");
	}

	@Test
	public void shouldupdate() {
		InOrder inOrder = Mockito.inOrder(eSystemCRUDBeanLocal,
				templateCRUDBeanLocal, localeCRUDBeanLocal,
				productCRUDBeanLocal);

		ProductDTO productDTO2 = ProductDTO.createNewProductDTO("a",
				ProductType.ANALOG, system, productlocales, 0, 12);

		productBean.update(productAdmin, productDTO2);
		product.fromDto(productDTO2);
		inOrder.verify(eSystemCRUDBeanLocal).findByName("mhm");
		inOrder.verify(productCRUDBeanLocal).findBySystemAndUin(eSystem, "a");
		inOrder.verify(templateCRUDBeanLocal).findByUin("test");
		inOrder.verify(localeCRUDBeanLocal).findByCode("de");
		inOrder.verify(productCRUDBeanLocal).update(product);
	}

	@Test
	public void shouldUpdateProductLocaleCreateTemplate() {
		TemplatesDTO templatesDTO = productlocales[0].getTemplate();
		Mockito.when(templateCRUDBeanLocal.findByUin(templatesDTO.getUin()))
				.thenReturn(null);
		ProductLocale value = new ProductLocale(
				ProductLocaleDTO
						.createProductLocaleDTOFromExsistingProductLocaleEntity(
								22,

								localeDTO, "$", "TestProduct", "des",
								templatesDTO, 4223, 0.42));

		value.setLocale(new Locale(localeDTO));
		Mockito.when(productLocaleCRUDBeanLocal.findById(22)).thenReturn(value);

		productBean.putProductLocale(productAdmin, productlocales[0]);
		Mockito.verify(productLocaleCRUDBeanLocal).update(
				(ProductLocale) Matchers.anyObject());
		Mockito.verify(templateCRUDBeanLocal).insert(
				(Templates) Matchers.anyObject());

		// @Test
		// public void shouldUpdateTemplate() {
		// TemplatesDTO templatesDTO = productlocales[0].getTemplate();
		// productBean.putTemplate(productAdmin, templatesDTO);
		// Mockito.verify(templateCRUDBeanLocal).update(
		// (Templates) Matchers.anyObject());
		// }

	}
}
