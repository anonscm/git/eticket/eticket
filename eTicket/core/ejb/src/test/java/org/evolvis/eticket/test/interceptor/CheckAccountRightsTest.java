package org.evolvis.eticket.test.interceptor;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.interceptor.InvocationContext;

import org.evolvis.eticket.business.system.SystemBean;
import org.evolvis.eticket.business.system.account.AccountBean;
import org.evolvis.eticket.interceptors.CheckAccountRights;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.system.RoleCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.Credentials;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Role;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Matchers;

public class CheckAccountRightsTest {
	private RoleCRUD roleCRUD = mock(RoleCRUD.class);
	private AccountCRUD accountCRUD = mock(AccountCRUD.class);
	private ESystemCRUD eSystemCRUD = mock(ESystemCRUD.class);
	private CheckAccountRights checkAccountRights = new CheckAccountRights();
	private InvocationContext context = mock(InvocationContext.class);

	private Account helpMockAccount(List<Role> roles)
			throws IllegalAccessException {
		Account account = mock(Account.class);
		when(account.getRoles()).thenReturn(roles);
		Credentials credentials = mock(Credentials.class);
		when(credentials.getUsername()).thenReturn("Narf");
		when(account.getCredential()).thenReturn(credentials);
		when(
				accountCRUD.checkPwGetAccount((ESystem) anyObject(),
						(AccountDTO) anyObject())).thenReturn(account);
		return account;
	}

	private void helpSetMockContext() throws NoSuchMethodException {
		when(context.getMethod()).thenReturn(
				AccountBean.class.getMethod("find", ESystemDTO.class,
						String.class));
		when(context.getTarget()).thenReturn(new AccountBean());
		AccountDTO params[] = { AccountDTO.init(ESystemDTO.createNewESystemDTO("b",
				LocaleDTO.createNewLocaleDTO("de", "blubb"), null), "h", "h", RoleDTO.get(
				AllowedRoles.USER, 0), "openId@OpenId.de") };
		when(context.getParameters()).thenReturn(params);
	}

	@Before
	public void init() {
		checkAccountRights.setAccountCRUDBeanLocal(accountCRUD);
		checkAccountRights.seteSystemCRUDBeanLocal(eSystemCRUD);
		checkAccountRights.setRoleCRUDBeanLocal(roleCRUD);
	}

	@Test
	public void shouldProceedWhenUserHasRight() throws Exception {
		Role role = new Role();
		role.setId(42);
		List<Role> roles = new ArrayList<Role>();
		roles.add(role);
		helpSetMockContext();
		helpMockAccount(roles);
		when(roleCRUD.findBySystemAndName((ESystem) anyObject(), anyString()))
				.thenReturn(role);
		checkAccountRights.checkCallerHasRole(context);
		InOrder inOrder = inOrder(accountCRUD, roleCRUD, context);
		inOrder.verify(accountCRUD).checkPwGetAccount((ESystem) anyObject(),
				(AccountDTO) anyObject());
		inOrder.verify(roleCRUD).findBySystemAndName((ESystem) anyObject(),
				Matchers.eq("sysadmin"));
		inOrder.verify(context).proceed();
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenAlowedRolesIsNull() throws Exception {
		when(context.getMethod()).thenReturn(SystemBean.class.getMethods()[0]);
		when(context.getTarget()).thenReturn(new SystemBean());
		checkAccountRights.checkCallerHasRole(context);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenParamtypeIsNotAccountDTO()
			throws Exception {
		when(context.getMethod()).thenReturn(
				AccountBean.class.getMethod("find", ESystemDTO.class,
						String.class));
		when(context.getTarget()).thenReturn(new AccountBean());
		checkAccountRights.checkCallerHasRole(context);
	}

	@Test(expected = IllegalAccessException.class)
	public void shouldThrowExceptionWhenUserHasNotTheExspectedRights()
			throws Exception {
		Role role = new Role();
		role.setId(23);
		List<Role> roles = new ArrayList<Role>();
		Role role2 = new Role();
		role.setId(42);
		roles.add(role2);
		helpSetMockContext();
		helpMockAccount(roles);
		when(roleCRUD.findBySystemAndName((ESystem) anyObject(), anyString()))
				.thenReturn(role);
		checkAccountRights.checkCallerHasRole(context);
	}

}
