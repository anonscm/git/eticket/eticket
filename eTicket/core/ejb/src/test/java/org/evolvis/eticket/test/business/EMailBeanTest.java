package org.evolvis.eticket.test.business;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import org.evolvis.eticket.business.communication.EMailBean;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.Credentials;
import org.evolvis.eticket.model.entity.PersonalInformation;
import org.evolvis.eticket.model.entity.TextTemplate;
import org.evolvis.eticket.util.CalculateHash;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Session.class, EMailBean.class })
public class EMailBeanTest {
	private EMailBean eMailBean = new EMailBean();

	private Account sender = helpMockAccount("sender", "sender");
	private Account recipient = helpMockAccount("recipient@f42.org", "pass");
	private PersonalInformation senderPersonals = mock(PersonalInformation.class);
	private Session session;

	private TextTemplate template = helpMockTemplate();
	private Transport transport = mock(Transport.class);
	private MimeMessage mime = mock(MimeMessage.class);

	// private Email email = mock(Email.class);

	private Map<String, String> buzzwordsAndValues = helpCreateBuzzwords();

	private Map<String, String> helpCreateBuzzwords() {
		Map<String, String> buzzwordsAndValues = new HashMap<String, String>();
		buzzwordsAndValues.put("recipient", "reci-pient");
		buzzwordsAndValues.put("sender", "sendöäür");
		return buzzwordsAndValues;
	}

	private Account helpMockAccount(String user, String pass) {
		Account result = mock(Account.class);
		String salt = CalculateHash.generateSalt();
		when(result.getCredential()).thenReturn(
				new Credentials(user, salt , CalculateHash.calculateWithSalt(salt, pass), null));
		return result;
	}

	private TextTemplate helpMockTemplate() {
		TextTemplate template = mock(TextTemplate.class);
		when(template.getText()).thenReturn(
				"Hello %recipient% and %sender%\nHow are ya?");
		return template;
	}

	@Before
	public void init() throws Exception {
		session = PowerMockito.mock(Session.class);
		mockStatic(Session.class);
		Mockito.when(Session.getInstance((Properties) Matchers.anyObject()))
				.thenReturn(session);
		Mockito.when(
				Session.getInstance((Properties) Matchers.anyObject(),
						(Authenticator) Matchers.anyObject())).thenReturn(
				session);
		when(sender.getPersonalInformation()).thenReturn(senderPersonals);
		when(senderPersonals.getEmail()).thenReturn("sender@f.f");
		when(session.getTransport()).thenReturn(transport);
		PowerMockito.whenNew(MimeMessage.class).withArguments(session)
				.thenReturn(mime);
	}

	// @Test(expected = RuntimeException.class)
	// public void shouldWrapEmailException(){
	// Mockito.when(GenericServiceLoader.get(Email.class,
	// MultiPartEmail.class)).thenReturn(null);
	// eMailBean.sendTo(sender, recipient, template, buzzwordsAndValues);
	// }

	@Test
	public void shouldSentMessage() throws Exception {
		eMailBean.sendTo(sender, recipient, template, buzzwordsAndValues);
		Mockito.verify(template, times(1)).getText();
		Mockito.verify(senderPersonals, times(1)).getEmail();
		Mockito.verify(sender, times(1)).getCredential();
		Mockito.verify(recipient, times(2)).getCredential();
		Mockito.verify(recipient, times(1)).getPersonalInformation();
		Mockito.verify(senderPersonals, times(1)).getFirstname();
		Mockito.verify(senderPersonals, times(1)).getLastname();
		Mockito.verify(transport, times(1)).connect();
		Mockito.verify(transport, times(1)).sendMessage(Matchers.eq(mime),
				(Address[]) Matchers.anyObject());
		Mockito.verify(transport, times(1)).close();
		Mockito.verify(session).getTransport();
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionBecauseSenderHasNoValidEmail()
			throws Throwable {
		when(sender.getPersonalInformation()).thenReturn(null);
		try {
			eMailBean.sendTo(sender, recipient, template, buzzwordsAndValues);
		} catch (Exception e) {
			Assert.assertEquals("No valid e-mail address given.", e.getCause()
					.getMessage());
			throw e.getCause();
		}
		Assert.fail("should thrown a exception");
	}

}
