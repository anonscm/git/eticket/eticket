package org.evolvis.eticket.test.interceptor;

import java.lang.reflect.Method;

import javax.interceptor.InvocationContext;

import org.evolvis.eticket.interceptors.CreateAccountHistory;
import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.crud.user.AccountCRUDBean;
import org.evolvis.eticket.model.crud.user.AccountHistoryCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.AccountHistory;
import org.evolvis.eticket.model.entity.CredentialHistory.Action;
import org.evolvis.eticket.model.entity.Credentials;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.util.CalculateHash;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mockito;

public class CreateAccountHistoryTest {

	private static class HelpVerifyAccountHistory extends
			ArgumentMatcher<AccountHistory> {
		private Action action;

		HelpVerifyAccountHistory(Action action) {
			this.action = action;
		}

		@Override
		public boolean matches(Object o) {
			if (!(o instanceof AccountHistory))
				return false;
			AccountHistory a = (AccountHistory) o;
			return a.getAction() == action;
		}
	}

	private CreateAccountHistory history = new CreateAccountHistory();
	private AccountHistoryCRUD accountHistoryCRUDBeanLocal = Mockito
			.mock(AccountHistoryCRUD.class);

	private InvocationContext context = Mockito.mock(InvocationContext.class);

	private void helpSetContext(InvocationContext context, String methodName,
			Class<?> target, Class<?>... types) throws NoSuchMethodException {
		Mockito.when(context.getTarget()).thenReturn(new AccountCRUDBean());
		Account account = new Account();
		String salt = CalculateHash.generateSalt();

		account.setCredential(new Credentials("dunno", salt, CalculateHash
				.calculateWithSalt(salt,
						CalculateHash.calculate("TotallyAwesomePassword")),
				null));
		Object[] param = { account };
		Mockito.when(context.getParameters()).thenReturn(param);
		Method method = target.getMethod(methodName, types);
		Mockito.when(context.getMethod()).thenReturn(method);
	}

	private void helpVerifyBehavior(String methodName, Action action)
			throws NoSuchMethodException, Exception {
		helpSetContext(context, methodName, GenericCUDSkeleton.class,
				Object.class);
		history.logCall(context);
		Mockito.verify(accountHistoryCRUDBeanLocal).insert(
				Matchers.argThat(new HelpVerifyAccountHistory(action)));
		Mockito.verify(context).proceed();
	}

	@Before
	public void init() {
		history.setAccountHistoryCRUDBeanLocal(accountHistoryCRUDBeanLocal);
	}

	@Test
	public void shouldCreateCreateActionWhenInsert() throws Exception {
		helpVerifyBehavior("insert", Action.INSERT);
	}

	@Test
	public void shouldCreateDeleteActionWhenDelete() throws Exception {
		helpVerifyBehavior("delete", Action.DELETE);
	}

	@Test
	public void shouldCreateUpdateActionWhenUpdate() throws Exception {
		helpVerifyBehavior("update", Action.UPDATE);
	}

	@Test
	public void shouldProceedWhenNoActionGotTriggered() throws Exception {
		helpSetContext(context, "listInactiveAccountsInSystem",
				AccountCRUDBean.class, ESystem.class);
		history.logCall(context);
		Mockito.verify(accountHistoryCRUDBeanLocal, Mockito.never()).insert(
				(AccountHistory) Matchers.anyObject());
		Mockito.verify(context).proceed();
	}
}
