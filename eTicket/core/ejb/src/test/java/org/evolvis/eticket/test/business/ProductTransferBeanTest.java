package org.evolvis.eticket.test.business;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.evolvis.eticket.business.communication.Communicate;
import org.evolvis.eticket.business.communication.EMailBean;
import org.evolvis.eticket.business.product.ProductTransferBean;
import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.crud.product.ProductCRUD;
import org.evolvis.eticket.model.crud.product.ProductPoolCRUD;
import org.evolvis.eticket.model.crud.product.TransferHistoryCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.system.TextTemplateCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.ConfigurationParameter;
import org.evolvis.eticket.model.entity.Credentials;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;
import org.evolvis.eticket.model.entity.TextTemplate;
import org.evolvis.eticket.model.entity.TransferHistory;
import org.evolvis.eticket.model.entity.TransferHistory.TransferState;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.util.CalculateHash;
import org.evolvis.eticket.util.GenericServiceLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InOrder;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * When the account will get transfered a product it will create a pool with the
 * status 'registered', by the creation of this pool it will take the amount off
 * the current amount of this product lying around in the system when it got
 * sent by the system or it will get take off from the volatile pool of the
 * sending account to reserve the amount of product for that account.
 * 
 * In the next step the account must accept the product (e.g. by registering,
 * activation or something else --decided by the client), when she accepted the
 * product, the pool will get the status volatile. In this state the product can
 * be sent to anyone or anything (even to a vengefully flowerpot, but only when
 * your name is Arthur Dent!) else.
 * 
 * When the account prints it or when it got sent (e.g. a Shirt orso) the pool
 * gets the status fixed. When the pool has this status it cannot be sent to
 * anyone else anymore.
 * 
 * If the account is arrogant enough to decline the product, the amount will get
 * transfered back to the sender.
 * 
 * @author phil
 * 
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ GenericServiceLoader.class })
public class ProductTransferBeanTest {

	private static class HelpVerifyProductPool extends
			ArgumentMatcher<ProductPool> {

		private ProductPoolStatus status;
		private long amount;

		public HelpVerifyProductPool(ProductPoolStatus status, long amount) {
			this.amount = amount;
			this.status = status;
		}

		@Override
		public boolean matches(Object argument) {
			if (!(argument instanceof ProductPool))
				return false;
			ProductPool pp = (ProductPool) argument;
			return pp.getType() == status && pp.getAmount() == amount;
		}

	}

	private static class HelpVerifyTransferHistory extends
			ArgumentMatcher<TransferHistory> {

		private Product product;
		private int amount;
		private Account sender;
		private Account rec;
		private TransferState state;

		public HelpVerifyTransferHistory(Product product, int amount,
				Account sender, Account rec, TransferState state) {
			this.product = product;
			this.amount = amount;
			this.sender = sender;
			this.rec = rec;
			this.state = state;
		}

		@Override
		public boolean matches(Object argument) {
			if (!(argument instanceof TransferHistory))
				return false;
			TransferHistory th = (TransferHistory) argument;
			boolean a = th.getState() == state;
			boolean b = sender == null ? th.getSender() == null : sender
					.equals(th.getSender());
			boolean c = rec.equals(th.getRecipient());
			boolean d = product.equals(th.getProduct());
			boolean e = th.getAmount() == amount;
			return a && b && c && d && e;
		}

	}

	private static final int AMOUNT = 22;

	private ProductTransferBean bean = new ProductTransferBean();
	private ProductPoolCRUD poolCRUD = mock(ProductPoolCRUD.class);
	private ESystemCRUD systemCRUD = mock(ESystemCRUD.class);
	private AccountCRUD accountCRUD = mock(AccountCRUD.class);
	private ProductCRUD productCRUD = mock(ProductCRUD.class);
	private TransferHistoryCRUD transferHistoryCRUD = Mockito
			.mock(TransferHistoryCRUD.class);
	private ESystemDTO system = ESystemDTO.createNewESystemDTO("shinySystem",
			LocaleDTO.createNewLocaleDTO("de", "deutsche"), null);
	private AccountDTO sender = AccountDTO.init(system,
			"s@e.de", "123", RoleDTO.getProductAdmin(0), "openId@OpenId.de");

	private ProductDTO product = ProductDTO.createNewProductDTO("PDUC",
			ProductType.DIGITAL, system, null, 0, 0);
	private AccountDTO receiver = AccountDTO.init(system,
			"s@hi.ny", "haha",

			RoleDTO.getUser(0), "openId@OpenId.de");
	private Account recAccount = helpCreateAccount(receiver);
	private Product prod = helpCreateProduct();
	private Account senderAcc = helpCreateAccount(sender);
	private ProductPool pool = helpCreatePool(recAccount,
			ProductPoolStatus.VOLATILE);
	private Communicate send = mock(Communicate.class);
	private ConfigurationParameterCRUD configurationParameterCRUD = mock(ConfigurationParameterCRUD.class);
	private TextTemplateCRUD templateCRUD = mock(TextTemplateCRUD.class);

	private Account helpCreateAccount(AccountDTO acc) {
		Account result = new Account();
		String salt = CalculateHash.generateSalt();
		String password = CalculateHash.calculateWithSalt(salt,
				acc.getPassword());
		result.setCredential(new Credentials(acc.getUsername(), salt, password,
				null));
		return result;
	}

	private ProductPool helpCreatePool(Account acc, ProductPoolStatus type) {
		ProductPool pool = new ProductPool();
		pool.setId(1);
		pool.setAmount(AMOUNT);
		pool.setProduct(prod);
		pool.setActor(acc);
		pool.setType(type);
		return pool;
	}

	private Product helpCreateProduct() {
		Product result = new Product(product);
		ESystem eSystem = new ESystem(system);
		result.setSystem(eSystem);
		return result;

	}

	private TransferHistory helpCreateTransferHistory() {
		TransferHistory th = new TransferHistory();
		th.setId(222);
		th.setSender(senderAcc);
		th.setPool(pool);
		th.setRecipient(recAccount);
		th.setProduct(prod);
		th.setAmount(AMOUNT);
		return th;
	}

	private void helpSetAccountMock() {
		when(
				accountCRUD.findBySystemAndUserName((ESystem) anyObject(),
						eq(receiver.getUsername()))).thenReturn(recAccount);
		String salt = CalculateHash.generateSalt();
		String pass = CalculateHash.calculateWithSalt(salt, sender.getPassword());
		when(
				accountCRUD.findBySystemAndUserName((ESystem) anyObject(),
						eq(sender.getUsername()))).thenReturn(senderAcc);
		senderAcc.setCredential(new Credentials(sender.getUsername(), salt, pass, null));
		bean.setAccountCRUD(accountCRUD);
	}

	private void helpSetCommMock() {
		ConfigurationParameter cp = new ConfigurationParameter();
		when(
				configurationParameterCRUD.findBySystemAndKey(
						(ESystem) anyObject(),
						eq(CPKey.SYSTEM_SENDER.toString()))).thenReturn(cp);
		bean.setConfigurationParameterBeanLocal(configurationParameterCRUD);
		bean.setTemplateCRUD(templateCRUD);

		mockStatic(GenericServiceLoader.class);
		when(GenericServiceLoader.get(Communicate.class, EMailBean.class))
				.thenReturn(send);

	}

	private void helpSetProductMock() {
		bean.setPoolCRUD(poolCRUD);
		when(productCRUD.findBySystemAndUin((ESystem) anyObject(), eq("PDUC")))
				.thenReturn(prod);
		bean.setProductCRUD(productCRUD);
		bean.setTransferHistoryCRUD(transferHistoryCRUD);
		when(poolCRUD.find(1)).thenReturn(pool);
	}

	private void helpSetSystemMock() {
		when(systemCRUD.findByName("shinySystem")).thenReturn(new ESystem());
		bean.setSystemCRUD(systemCRUD);
	}

	private void helpVerifyBookBack() {
		verify(transferHistoryCRUD).findById(1);
		verify(poolCRUD).findByProductStatusActor(prod,
				ProductPoolStatus.VOLATILE, senderAcc);
		verify(poolCRUD).update(
				Matchers.argThat(new HelpVerifyProductPool(
						ProductPoolStatus.VOLATILE, 2 * AMOUNT)));
		verify(poolCRUD).delete(pool);
	}

	private void helpVerifyProductUpd(Product prod) {
		verify(productCRUD).update(prod);
		Assert.assertEquals(1, prod.getCurrAmount());
	}

	@Before
	public void init() {
		bean.setTransferHistoryCRUD(transferHistoryCRUD);
		helpSetSystemMock();
		helpSetAccountMock();
		helpSetProductMock();
		helpSetCommMock();
		prod.setCurrAmount(0);
		prod.setMaxAmount(12);
	}

	@Test
	public void shouldAcceptProductAndCreateVolatilePool() {
		TransferHistory th = helpCreateTransferHistory();
		when(transferHistoryCRUD.findById(1)).thenReturn(th);
		bean.accept(receiver, 1);
		verify(transferHistoryCRUD).findById(1);
		verify(poolCRUD).insert(
				Matchers.argThat(new HelpVerifyProductPool(
						ProductPoolStatus.VOLATILE, AMOUNT)));
		verify(poolCRUD).delete(pool);
		verify(transferHistoryCRUD).update(
				Matchers.argThat(new HelpVerifyTransferHistory(prod, AMOUNT,
						senderAcc, recAccount, TransferState.ACCEPTED)));
	}

	@Test
	public void shouldAcceptProductAndUpdateVolatilePool() {
		InOrder inOrder = Mockito.inOrder(poolCRUD, transferHistoryCRUD);
		when(
				poolCRUD.findByProductStatusActor(prod,
						ProductPoolStatus.VOLATILE, recAccount)).thenReturn(
				pool);
		TransferHistory th = helpCreateTransferHistory();
		when(transferHistoryCRUD.findById(1)).thenReturn(th);
		bean.accept(receiver, 1);
		inOrder.verify(transferHistoryCRUD).findById(1);
		inOrder.verify(poolCRUD).findByProductStatusActor(prod,
				ProductPoolStatus.VOLATILE, recAccount);
		inOrder.verify(poolCRUD).update((ProductPool) anyObject());
		inOrder.verify(transferHistoryCRUD).update(
				Matchers.argThat(new HelpVerifyTransferHistory(prod, AMOUNT,
						senderAcc, recAccount, TransferState.ACCEPTED)));
		inOrder.verify(poolCRUD).update((ProductPool) anyObject());
	}

	@Test
	@SuppressWarnings("unchecked")
	public void shouldCancelTransfer() {
		TransferHistory th = helpCreateTransferHistory();
		when(transferHistoryCRUD.findById(1)).thenReturn(th);
		ProductPool pool2 = helpCreatePool(senderAcc,
				ProductPoolStatus.VOLATILE);
		Product product = new Product();
		product.setCurrAmount(55);
		when(productCRUD.findById(anyLong())).thenReturn(product);
		when(
				poolCRUD.findByProductStatusActor(prod,
						ProductPoolStatus.VOLATILE, senderAcc)).thenReturn(
				pool2);
		bean.cancel(sender, 1);

		verify(transferHistoryCRUD).update(
				Matchers.argThat(new HelpVerifyTransferHistory(prod, AMOUNT,
						senderAcc, recAccount, TransferState.CANCELED)));
		verify(send).sendTo((Account) anyObject(), (Account) anyObject(),
				(TextTemplate) anyObject(), (Map<String, String>) anyObject());
	}

	@Test
	@SuppressWarnings("unchecked")
	public void shouldDeclineProduct() {
		TransferHistory th = helpCreateTransferHistory();
		when(transferHistoryCRUD.findById(1)).thenReturn(th);
		ProductPool pool2 = helpCreatePool(senderAcc,
				ProductPoolStatus.VOLATILE);
		Product product = new Product();
		product.setCurrAmount(55);
		when(productCRUD.findById(anyLong())).thenReturn(product);
		when(
				poolCRUD.findByProductStatusActor(prod,
						ProductPoolStatus.VOLATILE, senderAcc)).thenReturn(
				pool2);
		bean.decline(receiver, 1);
		helpVerifyBookBack();
		verify(transferHistoryCRUD).update(
				Matchers.argThat(new HelpVerifyTransferHistory(prod, AMOUNT,
						senderAcc, recAccount, TransferState.DECLINED)));
		verify(send).sendTo((Account) anyObject(), (Account) anyObject(),
				(TextTemplate) anyObject(), (Map<String, String>) anyObject());
	}

	@Test
	@SuppressWarnings("unchecked")
	public void shouldSendProductFromUserToReceiver() {
		when(
				poolCRUD.findByProductStatusActor(prod,
						ProductPoolStatus.VOLATILE, senderAcc))
				.thenReturn(pool);
		bean.transferProductToAccount(sender, product, receiver, 1);
		verify(transferHistoryCRUD).insert(
				Matchers.argThat(new HelpVerifyTransferHistory(prod, 1,
						senderAcc, recAccount, TransferState.OPEN)));
		verify(poolCRUD).update(
				Matchers.argThat(new HelpVerifyProductPool(
						ProductPoolStatus.VOLATILE, AMOUNT - 1)));
		verify(poolCRUD).insert(
				Matchers.argThat(new HelpVerifyProductPool(
						ProductPoolStatus.REGISTERED, 1)));
		verify(send, times(2)).sendTo((Account) anyObject(),
				(Account) anyObject(), (TextTemplate) anyObject(),
				(Map<String, String>) anyObject());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionCallerIsNotSenderByCancelTransfer() {
		TransferHistory th = helpCreateTransferHistory();
		when(transferHistoryCRUD.findById(1)).thenReturn(th);
		Product product = new Product();
		product.setCurrAmount(55);
		when(productCRUD.findById(anyLong())).thenReturn(product);
		ProductPool pool2 = helpCreatePool(senderAcc,
				ProductPoolStatus.VOLATILE);
		when(
				poolCRUD.findByProductStatusActor(prod,
						ProductPoolStatus.VOLATILE, senderAcc)).thenReturn(
				pool2);
		bean.cancel(receiver, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenPoolIsNull() {
		bean.accept(sender, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenSenderHasNoPool() {
		bean.transferProductToAccount(sender, product, receiver, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenSenderIsNotTheOwnerOfPool() {
		pool.setActor(recAccount);
		bean.accept(sender, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenTheAmountLimitOfProductIsReached() {
		prod.setCurrAmount(12);
		bean.transferSystemProductToAccount(sender, product, receiver, 1);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void shouldTransferProductFromSystemAndCreateNewPoolIfUserHasNone() {
		bean.transferSystemProductToAccount(sender, product, receiver, 1);
		verify(poolCRUD).findByProductStatusActor(prod,
				ProductPoolStatus.REGISTERED, recAccount);
		verify(poolCRUD).insert(
				Matchers.argThat(new HelpVerifyProductPool(
						ProductPoolStatus.REGISTERED, 1)));
		helpVerifyProductUpd(prod);
		verify(transferHistoryCRUD).insert(
				Matchers.argThat(new HelpVerifyTransferHistory(prod, 1, null,
						recAccount, TransferState.OPEN)));
		verify(send).sendTo((Account) anyObject(), (Account) anyObject(),
				(TextTemplate) anyObject(), (Map<String, String>) anyObject());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldTransferProductFromSystemAndUpdatePool() {
		ProductPool pool = helpCreatePool(null, ProductPoolStatus.VOLATILE);
		when(
				poolCRUD.findByProductStatusActor(prod,
						ProductPoolStatus.REGISTERED, recAccount)).thenReturn(
				pool);
		bean.transferSystemProductToAccount(sender, product, receiver, 1);
		verify(poolCRUD).findByProductStatusActor(prod,
				ProductPoolStatus.REGISTERED, recAccount);
		verify(poolCRUD).update(pool);
		Assert.assertEquals(23, pool.getAmount());
		helpVerifyProductUpd(prod);
		verify(transferHistoryCRUD).insert(
				Matchers.argThat(new HelpVerifyTransferHistory(prod, 1, null,
						recAccount, TransferState.OPEN)));
		verify(send).sendTo((Account) anyObject(), (Account) anyObject(),
				(TextTemplate) anyObject(), (Map<String, String>) anyObject());
	}

	@Test
	public void shouldFindTransferActivitiesSender() {
		List<TransferHistory> transferHistories = new ArrayList<TransferHistory>();
		when(
				transferHistoryCRUD.findBySenderAndTransferState(
						"Sheldon Lee Cooper", TransferState.ACCEPTED))
				.thenReturn(transferHistories);
		bean.findTransferActivitiesSender("Sheldon Lee Cooper", "ACCEPTED");
		verify(transferHistoryCRUD).findBySenderAndTransferState(
				"Sheldon Lee Cooper", TransferState.ACCEPTED);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionIllegalArgumentExceptionByFindTransferActivitiesSender() {
		bean.findTransferActivitiesSender("Sheldon Lee", "Cooper");
	}

	@Test
	public void shouldFindTransferActivities() {
		List<TransferHistory> transferHistories = new ArrayList<TransferHistory>();
		when(
				transferHistoryCRUD.findByRecipientUsernameAndTransferState(
						"Sheldon Lee Cooper", TransferState.ACCEPTED))
				.thenReturn(transferHistories);
		bean.findTransferActivitiesRecipient("Sheldon Lee Cooper", "ACCEPTED");
		verify(transferHistoryCRUD).findByRecipientUsernameAndTransferState(
				"Sheldon Lee Cooper", TransferState.ACCEPTED);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionIllegalArgumentExceptionByFindTransferActivities() {
		bean.findTransferActivitiesRecipient("Sheldon Lee", "Cooper");
	}
}
