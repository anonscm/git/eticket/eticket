package org.evolvis.eticket.business.system;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.ConfigurationParameter;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;

public class ConfigurationParameterBeanTest {
	private static class HelpVerifyConfigurationParameter extends
			ArgumentMatcher<ConfigurationParameter> {

		private String value;
		private CPKey key;

		public HelpVerifyConfigurationParameter(CPKey key, String value) {
			this.value = value;
			this.key = key;
		}

		@Override
		public boolean matches(Object argument) {
			if (!(argument instanceof ConfigurationParameter))
				return false;
			ConfigurationParameter cp = (ConfigurationParameter) argument;
			return (value == null ? cp.getValue() == null : cp.getValue()
					.equals(value)) && CPKey.fromString(cp.getKey()) == key;
		}

	}

	private ConfigurationParameterBean bean = new ConfigurationParameterBean();
	private ESystemCRUD eSystemCRUDBeanLocal = mock(ESystemCRUD.class);
	private ConfigurationParameterCRUD configurationParameterCRUDBeanLocal = mock(ConfigurationParameterCRUD.class);
	private ESystemDTO system = ESystemDTO.createNewESystemDTO("Ehw",
			LocaleDTO.createNewLocaleDTO("de", "deutsch"), null);
	private AccountDTO caller = AccountDTO.init(system, "Uhw",
			"Aha", RoleDTO.getModerator(0), "openId@OpenId.de");

	private ConfigurationParameter cp = new ConfigurationParameter();

	private void helpSetConfigCrud() {
		when(
				configurationParameterCRUDBeanLocal.findBySystemAndKey(
						(ESystem) anyObject(), anyString())).thenReturn(cp);

	}

	@Before
	public void init() {
		bean.setConfigurationParameterCRUDBeanLocal(configurationParameterCRUDBeanLocal);
		bean.seteSystemCRUDBeanLocal(eSystemCRUDBeanLocal);
		helpSetConfigCrud();
	}

	@Test
	public void shouldDeleteCp() {
		bean.deleteCP(caller, system, CPKey.ACCOUNT_VALIDATION_LIMIT);
		cp.setValue("");
		cp.setKey(CPKey.ACCOUNT_VALIDATION_LIMIT.toString());
		verify(configurationParameterCRUDBeanLocal).delete(
				Matchers.argThat(new HelpVerifyConfigurationParameter(
						CPKey.ACCOUNT_VALIDATION_LIMIT, "")));
	}

	@Test
	public void shouldFindCp() {
		bean.find(system, CPKey.SYSTEM_SENDER.toString());
		verify(configurationParameterCRUDBeanLocal).findBySystemAndKey(
				(ESystem) anyObject(), anyString());
	}

	@Test
	public void shouldFindCpAsInt() {
		cp.setKey(CPKey.ACCOUNT_VALIDATION_LIMIT.toString());
		cp.setValue("23");
		bean.findAsInt(system, CPKey.ACCOUNT_VALIDATION_LIMIT.toString());
		verify(configurationParameterCRUDBeanLocal).findBySystemAndKey(
				(ESystem) anyObject(), anyString());
	}

	@Test
	public void shouldFindCpAsLong() {
		cp.setKey(CPKey.ACCOUNT_VALIDATION_LIMIT.toString());
		cp.setValue("23424223");
		bean.findAsLong(system, CPKey.ACCOUNT_VALIDATION_LIMIT.toString());
		verify(configurationParameterCRUDBeanLocal).findBySystemAndKey(
				(ESystem) anyObject(), anyString());
	}

	@Test
	public void shouldInsertCp() {
		bean.insertCP(caller, system, CPKey.ACCOUNT_VALIDATION_LIMIT, "0");
		verify(configurationParameterCRUDBeanLocal).insert(
				Matchers.argThat(new HelpVerifyConfigurationParameter(
						CPKey.ACCOUNT_VALIDATION_LIMIT, "0")));
	}

	@Test
	public void shouldUpdateCp() {
		cp.setValue("localhorst");
		cp.setKey(CPKey.SYSTEM_SENDER.toString());
		bean.setValueOfCP(caller, system, CPKey.SYSTEM_SENDER, "localhorst");
		verify(configurationParameterCRUDBeanLocal).update(
				Matchers.argThat(new HelpVerifyConfigurationParameter(
						CPKey.SYSTEM_SENDER, "localhorst")));
	}

}
