package org.evolvis.eticket.model.crud.system;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.ESystem;
import org.junit.Before;
import org.junit.Test;

public class ESystemCrudBeanTest {
	private ESystemCRUDBean bean = new ESystemCRUDBean();
	private EntityManager manager = mock(EntityManager.class);
	private Query query = mock(Query.class);

	@Before
	public void init() {
		bean.setManager(manager);
		when(manager.createNamedQuery(anyString())).thenReturn(query);
	}

	@Test
	public void shouldFindByName() {
		bean.findByName("ehw");
		verify(manager).createNamedQuery("findSystemByName");
	}

	@Test
	public void shouldGetById() {
		bean.findById(4242);
		verify(manager).find(ESystem.class, (long) 4242);
	}

}
