package org.evolvis.eticket.interceptors;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import javax.interceptor.InvocationContext;

import org.evolvis.eticket.model.crud.product.ProductCRUD;
import org.evolvis.eticket.model.crud.product.ProductPoolCRUD;
import org.evolvis.eticket.model.crud.product.TransferHistoryCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.system.TextTemplateCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductAutomaticTransfer;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.junit.Before;
import org.junit.Test;

public class TransferProductTest {

	private TransferProduct transferProduct = new TransferProduct();
	private InvocationContext context = mock(InvocationContext.class);
	private LocaleDTO[] locales = { LocaleDTO.createNewLocaleDTO("te", "fhahka"),
			LocaleDTO.createNewLocaleDTO("et", "fhahka") };
	private ESystemDTO system = ESystemDTO.createNewESystemDTO("blubb", locales[0],
			Arrays.asList(locales));
	private AccountDTO account = AccountDTO.init(system, "blubb", "bbulb",
			RoleDTO.getSysAdmin(0), "openId@OpenId.de");

	private ProductDTO productDTO = ProductDTO.createNewProductDTO("dunno",
			ProductType.DIGITAL, system, null,0, 42);

	private AccountCRUD accountCRUD = mock(AccountCRUD.class);
	private ProductPoolCRUD poolCRUD = mock(ProductPoolCRUD.class);
	private ProductCRUD productCRUD = mock(ProductCRUD.class);
	private ESystemCRUD systemCRUD = mock(ESystemCRUD.class);
	private TextTemplateCRUD templateCRUD = mock(TextTemplateCRUD.class);
	private TransferHistoryCRUD transferHistoryCRUD = mock(TransferHistoryCRUD.class);
	private Method method = this.getClass().getMethods()[0];
	private Object target = mock(Object.class);
	private Class<?> className = target.getClass();
	private String methodName = method.getName();
	private ESystem eSystem = mock(ESystem.class);
	private Product prod = mock(Product.class);
	private Account account2 = new Account();

	private void helpMockContext() {
		when(context.getMethod()).thenReturn(method);
		when(context.getTarget()).thenReturn(target);
	}

	private void helpMockProduct() {
		ProductAutomaticTransfer pat = mock(ProductAutomaticTransfer.class);
		when(pat.getAmount()).thenReturn((long) 1);
		when(prod.getMaxAmount()).thenReturn(2);
		when(prod.getPat()).thenReturn(pat);
		List<Product> products = Arrays.asList(prod);
		transferProduct.setProductCRUD(productCRUD);
		when(
				productCRUD.listBySystemClassAndMethod(eSystem, className,
						methodName)).thenReturn(products);
	}

	private void helpMockSystem() {
		transferProduct.setSystemCRUD(systemCRUD);
		when(systemCRUD.findByName(system.getName())).thenReturn(eSystem);
	}

	private void setParams(Object... params) {
		when(context.getParameters()).thenReturn(params);
	}

	@Before
	public void setUp() {
		helpMockContext();
		transferProduct.setAccountCRUD(accountCRUD);
		// transferProduct.setConfigurationParameterBeanLocal(configurationParameterBeanLocal);
		transferProduct.setPoolCRUD(poolCRUD);
		helpMockProduct();
		helpMockSystem();
		transferProduct.setTemplateCRUD(templateCRUD);
		transferProduct.setTransferHistoryCRUD(transferHistoryCRUD);
		account2.setActivated(false);
		when(accountCRUD.findBySystemAndUserName((ESystem) anyObject(), eq("blubb"))).thenReturn(account2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceotionWhenParamsAreNull() throws Exception {
		transferProduct.aop(context);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenFirstParamIsNotAccountDto()
			throws Exception {
		setParams(new Object(), productDTO);
		transferProduct.aop(context);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenParamsLengthDiffer() throws Exception {
		setParams(new Object());
		transferProduct.aop(context);
	}

	@Test
	public void shouldTransferProductAccount() throws Exception {
		setParams(account);
		transferProduct.aop(context);
		verify(context).getTarget();
		verify(context).getMethod();
		verify(systemCRUD).findByName(account.getSystem().getName());
		verify(productCRUD).listBySystemClassAndMethod(eSystem, className,
				methodName);
		verify(prod).setCurrAmount(1);
		verify(productCRUD).update(prod);
	}

}
