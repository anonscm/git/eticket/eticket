package org.evolvis.eticket.model.crud.product;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import junit.framework.Assert;

import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.ProductLocale;
import org.junit.Before;
import org.junit.Test;

public class ProductLocaleCRUDBeanTest {
	private ProductLocaleCRUDBean bean = new ProductLocaleCRUDBean();
	private EntityManager manager = mock(EntityManager.class);
	private Query query = mock(Query.class);

	@Before
	public void init() {
		bean.setManager(manager);
		when(manager.createNamedQuery(anyString())).thenReturn(query);
	}

	@Test
	public void shouldReturnNullWhenProductLocaleIsUnknown() {
		ProductLocale pl = bean.findByLocaleCodeTemplateUin("de", "ehw");
		verify(manager).createNamedQuery(
				"findProductLocaleByLocaleCodeAndTemplateUin");
		verify(query).getResultList();
		Assert.assertTrue(pl == null);
	}

	@Test
	public void shouldReturnProductLocaleByLocalecodeAndProductUinAndSystemId() {
		// findProductLocaleByLocalecodeAndProductUinAndSystemId
		ProductLocale plocale = new ProductLocale();

		when(query.getSingleResult()).thenReturn(plocale);

		ProductLocale pl = bean
				.findProductLocaleByLocalecodeAndProductUinAndSystemId("de",
						"p_uin", 0);

		verify(manager).createNamedQuery(
				"findProductLocaleByLocalecodeAndProductUinAndSystemId");
		verify(query).getSingleResult();

		Assert.assertTrue(pl != null);
	}

	@Test
	public void shouldReturnProductLocale() {

List<ProductLocale> locales = Arrays
				.asList(new ProductLocale[] { new ProductLocale() });
		when(query.getResultList()).thenReturn(locales);
		ProductLocale pl = bean.findByLocaleCodeTemplateUin("de", "ehw");
		verify(manager).createNamedQuery(
				"findProductLocaleByLocaleCodeAndTemplateUin");
		verify(query).getResultList();
		Assert.assertTrue(pl != null);
	}

	@Test
	public void findByLocaleCodeAndProductId() {
		when(query.getSingleResult()).thenReturn(new ProductLocale());
		ProductLocale pl = bean.findByLocaleCodeAndProductId(new Locale(), 333);
		verify(manager).createNamedQuery("findProductLocaleByLocaleCodeAndProductId");
		verify(query).getSingleResult();
		Assert.assertTrue(pl != null);		
	

		List<ProductLocale> locales = Arrays
				.asList(new ProductLocale[] { new ProductLocale() });
		when(query.getResultList()).thenReturn(locales);
		ProductLocale pl2 = bean.findByLocaleCodeTemplateUin("fhf", "fghgfh");
		verify(manager).createNamedQuery(
				"findProductLocaleByLocaleCodeAndTemplateUin");
		verify(query).getResultList();
		Assert.assertTrue(pl2 != null);

		verify(manager).createNamedQuery(
				"findProductLocaleByLocaleCodeAndProductId");
		verify(query).getSingleResult();
		Assert.assertTrue(pl != null);
	}

}
