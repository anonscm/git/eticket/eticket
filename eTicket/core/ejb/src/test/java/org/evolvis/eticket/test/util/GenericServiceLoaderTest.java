package org.evolvis.eticket.test.util;

import java.util.List;

import org.evolvis.eticket.business.communication.Communicate;
import org.evolvis.eticket.util.GenericServiceLoader;
import org.junit.Assert;
import org.junit.Test;

public class GenericServiceLoaderTest {
    @Test
    public void shouldGetAllImpl() {
	List<Communicate> emails = GenericServiceLoader.getAll(Communicate.class);
	Assert.assertEquals(2, emails.size());
    }

    @Test
    public void shouldLoadDummyImpl() {
	FakeCommunicate test = GenericServiceLoader.get(Communicate.class, FakeCommunicate.class);
	Assert.assertNotNull(test);
    }

    @Test
    public void shouldLoadEMailBeanFromServiceLoader() {
	Communicate mpe = GenericServiceLoader.get(Communicate.class);
	Assert.assertNotNull(mpe);
    }

    @Test
    public void shouldLoadFCImpl() {
	FakeCommunicate test = GenericServiceLoader.get(Communicate.class, FakeCommunicate.class);
	Assert.assertNotNull(test);
    }

    @Test
    public void shouldReturnNullWhenImplIsUnknown() {
	Communicate o = GenericServiceLoader.get(Communicate.class, Test.class);
	Assert.assertNull(o);
    }

    @Test
    public void shouldReturnNullWhenInterfaceIsUnknown() {
	Object o = GenericServiceLoader.get(Test.class);
	Assert.assertNull(o);
    }

}
