package org.evolvis.eticket.model.crud.product;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.junit.Before;
import org.junit.Test;

public class ProductCrudBeanTest {
	private ProductCRUDBean bean = new ProductCRUDBean();
	private EntityManager manager = mock(EntityManager.class);
	private Query query = mock(Query.class);

	@Before
	public void init() {
		bean.setManager(manager);
		when(manager.createNamedQuery(anyString())).thenReturn(query);
	}

	@Test
	public void shouldFindBySystemAndUin() {
		bean.findBySystemAndUin(new ESystem(), "ehw");
		verify(manager).createNamedQuery("findProductBySystemAndUin");
	}

	@Test
	public void shouldGetById() {
		bean.findById(4242);
		verify(manager).find(Product.class, (long) 4242);
	}

}
