package org.evolvis.eticket.model.crud.platform;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.Locale;
import org.junit.Before;
import org.junit.Test;

public class LocaleCRUDBeanTest {
    private LocaleCRUDBean bean = new LocaleCRUDBean();

    private EntityManager manager = mock(EntityManager.class);
    private Query query = mock(Query.class);

    /**
     * Inits the.
     */
    @Before
    public void init() {
	bean.setManager(manager);
	when(manager.createNamedQuery(anyString())).thenReturn(query);
    }

    /**
     * Should find by id.
     */
    @Test
    public void shouldFindById() {
	bean.findById(12);
	verify(manager).find(Locale.class, (long) 12);
    }

    /**
     * Should get locale by code.
     */
    @Test
    public void shouldGetLocaleByCode() {
	bean.findByCode("de");
	verify(manager).createNamedQuery("findLocaleByCode");
	verify(query).setParameter(1, "de");
    }

}
