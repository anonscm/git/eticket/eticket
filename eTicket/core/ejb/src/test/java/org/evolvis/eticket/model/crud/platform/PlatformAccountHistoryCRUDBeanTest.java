package org.evolvis.eticket.model.crud.platform;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Before;
import org.junit.Test;

public class PlatformAccountHistoryCRUDBeanTest {
    private PlatformAccountHistoryCRUDBean bean = new PlatformAccountHistoryCRUDBean();
    private EntityManager manager = mock(EntityManager.class);
    private Query query = mock(Query.class);

    /**
     * Inits the.
     */
    @Before
    public void init() {
	bean.setManager(manager);
	when(manager.createNamedQuery(anyString())).thenReturn(query);
    }

    /**
     * Should list history created by username.
     */
    @Test
    public void shouldListHistoryCreatedByUsername() {
	bean.listPlatformAccountHistoryCreatedByUsername("aha");
	verify(manager).createNamedQuery("listPlatformAccountHistoryCreatedByUsername");
    }

    /**
     * Should list history from username.
     */
    @Test
    public void shouldListHistoryFromUsername() {
	bean.listPlatformAccountHistoryFromUsername("aha");
	verify(manager).createNamedQuery("listPlatformAccountHistoryFromUsername");
    }

}
