package org.evolvis.eticket.model.crud;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.Role;
import org.junit.Before;
import org.junit.Test;

public class GenericCudTest {
	private static final String QNAME = "test";
	private GenericCUDSkeleton<Role> cud = new GenericCUDSkeleton<Role>() {
		private static final long serialVersionUID = 6044514495560361505L;
	};
	private EntityManager manager = mock(EntityManager.class);
	private Role role = new Role();
	Query query = mock(Query.class);

	private void helpSetQuery() {
		when(manager.createNamedQuery(QNAME)).thenReturn(query);
	}

	@Before
	public void init() {
		cud.setManager(manager);
		helpSetQuery();
	}

	@Test
	public void shouldBuildNamedQueryAndExecute() {
		cud.buildNamedQueryExecute(QNAME, role);
		verify(query).executeUpdate();
	}

	@Test
	public void shouldBuildNamedQueryAndGetResults() {
		cud.buildNamedQueryGetResults(QNAME);
		verify(query).getResultList();
	}

	@Test
	public void shouldBuildNamedQueryAndGetSingleResult() {
		cud.buildNamedQueryGetSingleResult(QNAME, role);
		verify(query).getSingleResult();
	}

	@Test
	public void shouldBuildQueryWithMoreThanArg() {
		cud.buildNamedQueryExecute(QNAME, role, role, role, role);
		verify(manager).createNamedQuery(QNAME);
		verify(query, times(4)).setParameter(anyInt(), anyObject());
	}

	@Test
	public void shouldBuildQueryWithnoArg() {
		cud.buildNamedQueryExecute(QNAME);
		verify(manager).createNamedQuery(QNAME);
		verify(query, never()).setParameter(anyInt(), anyObject());
	}

	@Test
	public void shouldBuildQueryWithOneArg() {
		cud.buildNamedQueryExecute(QNAME, role);
		verify(manager).createNamedQuery(QNAME);
		verify(query).setParameter(1, role);
	}

	@Test
	public void shouldDelete() {
		when(manager.merge(role)).thenReturn(role);
		cud.delete(role);
		verify(manager).remove(role);
		verify(manager).flush();
	}

	@Test
	public void shouldFind() {
		cud.find(Role.class, 1);
		verify(manager).find(Role.class, (long) 1);
	}

	@Test
	public void shouldInsert() {
		cud.insert(role);
		verify(manager).persist(role);
		verify(manager).flush();
	}

	@Test
	public void shouldUpdate() {
		cud.update(role);
		verify(manager).merge(role);
		verify(manager).flush();
	}

}
