package org.evolvis.eticket.model.crud.user;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.ESystem;
import org.junit.Before;
import org.junit.Test;

public class AccountHistoryCRUDBeanTest {
    private AccountHistoryCRUDBean bean = new AccountHistoryCRUDBean();
    private EntityManager manager = mock(EntityManager.class);
    private Query query = mock(Query.class);
    private ESystem system = new ESystem();

    @Before
    public void init() {
	bean.setManager(manager);
	when(manager.createNamedQuery(anyString())).thenReturn(query);
    }

    @Test
    public void shouldExecuteListAccountHistoriesBySystemAndUsername() {
	bean.listAccountHistoriesBySystemAndUsername(system, "aha");
	verify(manager).createNamedQuery("listAccountHistoriesBySystemAndUsername");
	verify(query).getResultList();
    }

}
