package org.evolvis.eticket.interceptors;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;

import javax.interceptor.InvocationContext;

import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.junit.Before;
import org.junit.Test;

/**
 * @AroundInvoke public Object log(InvocationContext context) throws Exception {
 *               String className = context.getTarget().getClass().getName();
 *               Logger logger = Logger.getLogger(className); String method =
 *               context.getMethod().getName(); logger.info("entering:" +
 *               className + "#" + method); try { return context.proceed(); }
 *               catch (Exception e) { logger.warning("Exception:" +
 *               e.getMessage() + " in " + className + "#" + method); throw e; }
 *               }
 * @author phil
 * 
 */
public class LoggingTest {
    private Logging ic = new Logging();
    private InvocationContext context = mock(InvocationContext.class);

    // private Method method = mock(Method.class);

    @Before
    public void init() {
	when(context.getTarget()).thenReturn(RoleDTO.getPlatformAdmin());
	Method method = RoleDTO.class.getMethods()[0];
	when(context.getMethod()).thenReturn(method);
    }

    @Test
    public void shouldProceedWhenLogEntering() throws Exception {
	ic.log(context);
	verify(context).getTarget();
	verify(context).proceed();
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionWhenProceedThrowsOne() throws Exception {
	when(context.proceed()).thenThrow(new RuntimeException());
	ic.log(context);
	verify(context).getTarget();
	verify(context).proceed();
    }
}
