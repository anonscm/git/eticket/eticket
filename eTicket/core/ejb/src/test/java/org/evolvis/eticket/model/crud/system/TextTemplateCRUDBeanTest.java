package org.evolvis.eticket.model.crud.system;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.junit.Before;
import org.junit.Test;

public class TextTemplateCRUDBeanTest {
    private TextTemplateCRUDBean bean = new TextTemplateCRUDBean();

    private EntityManager manager = mock(EntityManager.class);
    private Query query = mock(Query.class);
    private ESystem system = new ESystem();
    private Locale locale = new Locale();

    @Before
    public void init() {
	bean.setManager(manager);
	when(manager.createNamedQuery(anyString())).thenReturn(query);
    }

    @Test
    public void shouldExecuteFindTextTemplateBySystemLocaleName() {
	bean.findBySystemLocaleAndName(system, locale, "aha");
	verify(manager).createNamedQuery("findTextTemplateBySystemLocaleName");
	verify(query).getSingleResult();
    }

    @Test
    public void shouldExecuteListTextTemplateInSystemLocale() {
	bean.listSystemLocale(system, locale);
	verify(manager).createNamedQuery("listTextTemplateInSystemLocale");
	verify(query).getResultList();
    }

}
