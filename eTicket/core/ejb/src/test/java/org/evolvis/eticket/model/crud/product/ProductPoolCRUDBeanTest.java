package org.evolvis.eticket.model.crud.product;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class ProductPoolCRUDBeanTest {
	private EntityManager manager = Mockito.mock(EntityManager.class);
	private Query query = Mockito.mock(Query.class);
	private ProductPoolCRUDBean bean = new ProductPoolCRUDBean();

	@Before
	public void init() {
		Mockito.when(manager.createNamedQuery("findPoolByProductStatusActor"))
				.thenReturn(query);
		Mockito.when(manager.createNamedQuery("findPoolByStatusAndUsername"))
		.thenReturn(query);
		Mockito.when(manager.createNamedQuery("findPoolByProductStatusUsername"))
		.thenReturn(query);
		bean.setManager(manager);
	}

	@Test
	public void shouldGetPoolByProductStatusAndActor() {
		bean.findByProductStatusActor(new Product(),
				ProductPoolStatus.VOLATILE, new Account());
		Mockito.verify(manager)
				.createNamedQuery("findPoolByProductStatusActor");
		Mockito.verify(query).getSingleResult();

	}
	
	@Test
	public void shouldGetPoolByStatusAndUsername() {
		bean.findByStatusAndUsername(ProductPoolStatus.VOLATILE, "");
		Mockito.verify(manager).createNamedQuery("findPoolByStatusAndUsername");
		Mockito.verify(query).getResultList();

	}

	@Test
	public void shouldGetPoolByProductStatusAndUsername() {
		bean.findByProductStatusUsername(new Product(),
				ProductPoolStatus.FIXED, "");
		Mockito.verify(manager).createNamedQuery(
				"findPoolByProductStatusUsername");
		Mockito.verify(query).getSingleResult();
	}
}
