package org.evolvis.eticket.business.system;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.system.TextTemplateCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.TextTemplate;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO.TextTemplateNames;
import org.junit.Before;
import org.junit.Test;

public class TextTemplateBeanTest {
    private TextTemplateBean bean = new TextTemplateBean();
    private ESystemCRUD eSystemCRUDBeanLocal = mock(ESystemCRUD.class);
    private LocaleCRUD localeCRUDBeanLocal = mock(LocaleCRUD.class);
    private TextTemplateCRUD templateCRUDBeanLocal = mock(TextTemplateCRUD.class);
    private LocaleDTO defaultLocale = LocaleDTO.createNewLocaleDTO("de", "blibb");
    private ESystemDTO eSystemDTO = ESystemDTO.createNewESystemDTO("tet", defaultLocale, null);
    private AccountDTO caller = AccountDTO.init(eSystemDTO, "test", "aha", RoleDTO.getSysAdmin(0), "openId@OpenId.de");
    private TextTemplateDTO textTemplate = TextTemplateDTO.createNewTextTemplateDTO(eSystemDTO, defaultLocale,
	    TextTemplateNames.NEW_ACCOUNT.toString(), null);

    private ESystem helpGenerateSystem() {
	Locale locale = new Locale(defaultLocale);
	ESystem system = new ESystem();
	system.setName("testSystem");
	system.setDefaultLocale(locale);
	return system;
    }

    private TextTemplate helpGenerateTemplae() {
	TextTemplate value = new TextTemplate();
	ESystem system = helpGenerateSystem();
	value.setLocale(system.getDefaultLocale());
	value.setSystem(system);
	value.setName("aha");
	return value;
    }

    @Before
    public void init() {
	bean.seteSystemCRUDBeanLocal(eSystemCRUDBeanLocal);
	bean.setLocaleCRUDBeanLocal(localeCRUDBeanLocal);
	bean.setTemplateCRUDBeanLocal(templateCRUDBeanLocal);
	when(eSystemCRUDBeanLocal.findByName(eSystemDTO.getName())).thenReturn(new ESystem());
    }

    @Test
    public void shouldDeleteTextTemplate() {
	bean.delete(caller, textTemplate);
	verify(templateCRUDBeanLocal).delete((TextTemplate) anyObject());
    }

    @Test
    public void shouldFindTextTemplate() {
	TextTemplate value = helpGenerateTemplae();
	when(
		templateCRUDBeanLocal.findBySystemLocaleAndName((ESystem) anyObject(), (Locale) anyObject(),
			eq(TextTemplateNames.NEW_ACCOUNT.toString()))).thenReturn(value);
	bean.find(eSystemDTO, defaultLocale, TextTemplateNames.NEW_ACCOUNT.toString());
	verify(templateCRUDBeanLocal).findBySystemLocaleAndName((ESystem) anyObject(), (Locale) anyObject(),
		eq(TextTemplateNames.NEW_ACCOUNT.toString()));
    }

    @Test
    public void shouldInsertTextTemplate() {
	bean.insert(caller, textTemplate);
	verify(templateCRUDBeanLocal).insert((TextTemplate) anyObject());
    }

    @Test
    public void shouldUpdateTextTemplate() {
	TextTemplate template = new TextTemplate();
	when(templateCRUDBeanLocal.findBySystemLocaleAndName((ESystem) anyObject(), (Locale) anyObject(), anyString()))
		.thenReturn(template);
	bean.update(caller, textTemplate);
	verify(templateCRUDBeanLocal).update((TextTemplate) anyObject());
    }

}
