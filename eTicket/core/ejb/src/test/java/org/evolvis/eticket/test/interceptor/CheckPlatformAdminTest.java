package org.evolvis.eticket.test.interceptor;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.interceptor.InvocationContext;

import org.evolvis.eticket.business.platform.PlatformBean;
import org.evolvis.eticket.interceptors.CheckPlatformAdmin;
import org.evolvis.eticket.model.crud.platform.PlatformCRUD;
import org.evolvis.eticket.model.entity.Platform;
import org.evolvis.eticket.model.entity.PlatformAccount;
import org.evolvis.eticket.model.entity.PlatformCredentials;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.util.CalculateHash;
import org.junit.Before;
import org.junit.Test;

public class CheckPlatformAdminTest {
	private CheckPlatformAdmin checkPlatformAdmin = new CheckPlatformAdmin();

	private PlatformCRUD platformCRUDBeanLocal = mock(PlatformCRUD.class);

	private InvocationContext context = mock(InvocationContext.class);

	private Platform platform;

	@Before
	public void init() {
		checkPlatformAdmin.setPlatformCRUDBeanLocal(platformCRUDBeanLocal);
		when(context.getTarget()).thenReturn(new PlatformBean());
		platform = new Platform("haha");
		PlatformAccount owner = new PlatformAccount();
		String salt = CalculateHash.generateSalt();
		owner.setCredentials(new PlatformCredentials("Leela", salt,
				CalculateHash.calculateWithSalt(salt,
						CalculateHash.calculate("hi"))));
		platform.setOwner(owner);
		when(platformCRUDBeanLocal.findByName("1")).thenReturn(platform);
	}

	private void setContextParam(String username, String password) {
		String id = "1";
		Object[] params = {
				AccountDTO.getPlatformAccount(username, password,
						"openId@OpenId.de"), id };
		when(context.getParameters()).thenReturn(params);
	}

	@Test
	public void shouldProceedWhenUsernameAndPwAreCorrect() throws Exception {
		setContextParam("Leela", "hi");
		checkPlatformAdmin.checkPlatformadmin(context);
		verify(context).proceed();
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionMissuseOfInterceptorMissingParams()
			throws Exception {
		checkPlatformAdmin.checkPlatformadmin(context);
	}

	@Test(expected = IllegalAccessException.class)
	public void shouldThrowExceptionWrongPassword() throws Exception {
		setContextParam("Leela", "OneEye");
		checkPlatformAdmin.checkPlatformadmin(context);
	}

	@Test(expected = IllegalAccessException.class)
	public void shouldThrowExceptionWrongUsername() throws Exception {
		setContextParam("LookLeelourImSomeFrenchGuyeee", "Hi");
		checkPlatformAdmin.checkPlatformadmin(context);
	}

}
