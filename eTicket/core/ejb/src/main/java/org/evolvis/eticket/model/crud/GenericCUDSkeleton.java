package org.evolvis.eticket.model.crud;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * The Class GenericCUD, creates, updates and deletes a Entity.
 * 
 * @param <T>
 *            the generic type of a Entity which wants to be controlled (;
 */
public abstract class GenericCUDSkeleton<T> implements GenericCUD<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3732230962721302336L;

	/** The entity manager does the dirty work for us. */
	@PersistenceContext
	protected EntityManager manager ;
	
	public void setManager(EntityManager manager) {
		this.manager = manager;
	}

	/**
	 * Finds the Entity.
	 * 
	 * @param entityClass
	 *            the entity class
	 * @param id
	 *            the id
	 * @return the t
	 */
	public T find(Class<T> entityClass, long id) {
		return manager.find(entityClass, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.evolvis.model.crud.GenericCUDInterface#update(java.lang.Object)
	 */
	@Override
	public void update(T entity) {
		manager.merge(entity);
		manager.flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.evolvis.model.crud.GenericCUDInterface#insert(java.lang.Object)
	 */
	@Override
	public void insert(T entity) {
		manager.persist(entity);
		manager.flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.evolvis.model.crud.GenericCUDInterface#delete(java.lang.Object)
	 */
	@Override
	public void delete(T entity) {
		manager.remove(manager.merge(entity));
		manager.flush();
	}

	/**
	 * this method is NOT type safe, it is just a helper method to call named
	 * queries and get a single result. The developer must ensure that the named
	 * query has the same result type as T. Use it at your own risk.
	 * 
	 * @param queryName
	 *            the query name
	 * @param objects
	 *            the objects
	 * @return the t
	 */
	protected T buildNamedQueryGetSingleResult(String queryName,
			Object... objects) {
		Query query = buildQuery(queryName, objects);
		// result of the query must be the same class as T, this must be checked
		// by the developer
		@SuppressWarnings("unchecked")
		T result = (T) query.getSingleResult();
		return result;
	}

	/**
	 * this method is NOT type safe, it is just a helper method to call named
	 * queries and get results. The developer must ensure that the named query
	 * has the same result class as T. Use it at your own risk.
	 * 
	 * @param queryName
	 *            the query name
	 * @param objects
	 *            the objects
	 * @return list of T
	 */
	protected List<T> buildNamedQueryGetResults(String queryName,
			Object... objects) {
		Query query = buildQuery(queryName, objects);

		@SuppressWarnings("unchecked")
		List<T> result = query.getResultList();
		return result;
	}

	/**
	 * this method is NOT type safe, it is just a helper method to call named
	 * queries and get results. The developer must ensure that the named query
	 * has the same result class as T. Use it at your own risk.
	 * 
	 * @param queryName
	 *            the query name
	 * @param objects
	 *            the objects
	 * @return number of rows affected by this execution
	 */
	protected int buildNamedQueryExecute(String queryName, Object... objects) {
		Query query = buildQuery(queryName, objects);
		int result = query.executeUpdate();
		return result;
	}

	/**
	 * Builds the query.
	 * 
	 * @param queryName
	 *            the query name
	 * @param objects
	 *            the objects
	 * @return the query
	 */
	private Query buildQuery(String queryName, Object... objects) {
		Query query = manager.createNamedQuery(queryName);
		int paramIndex = 1;
		for (Object paramValue : objects)
			query.setParameter(paramIndex++, paramValue);
		return query;
	}

}
