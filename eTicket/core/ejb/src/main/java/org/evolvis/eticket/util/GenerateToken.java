package org.evolvis.eticket.util;

import java.util.GregorianCalendar;
import java.util.UUID;

public class GenerateToken {
	private GenerateToken() {

	}

	public static String generateToken() {
		return generateUUIDToken();
	}

	static String generateUUIDToken() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	static String generateTimeHashToken() {
		return CalculateHash.calculate(""
				+ GregorianCalendar.getInstance().getTime().getTime());
	}

}
