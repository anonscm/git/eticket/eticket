package org.evolvis.eticket.model.crud.product;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Actor;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;

@Stateless
@Local(ProductPoolCRUD.class)
public class ProductPoolCRUDBean extends GenericCUDSkeleton<ProductPool>
		implements ProductPoolCRUD {

	private static final long serialVersionUID = -6217720229009415772L;

	@Override
	public ProductPool findByProductStatusActor(Product product,
			ProductPoolStatus status, Actor actor) {
		try {
			return buildNamedQueryGetSingleResult(
					"findPoolByProductStatusActor", product, status, actor);
		} catch (NoResultException b) {
			return null;
		}
	}

	@Override
	public ProductPool find(long id) {
		return find(ProductPool.class, id);
	}

	@Override
	public List<ProductPool> findByStatusAndUsername(ProductPoolStatus status,
			String username) {
		try {
			return buildNamedQueryGetResults("findPoolByStatusAndUsername",
					status, username);
		} catch (NoResultException b) {
			return null;
		}
	}

	public List<ProductPool> findByproduct(Product product) {
		try {
			return buildNamedQueryGetResults("findPoolsByProduct", product);
		} catch (NoResultException b) {
			return null;
		}
	}

	@Override
	public ProductPool findByProductStatusUsername(Product product,
			ProductPoolStatus status, String username) {
		try {
			return buildNamedQueryGetSingleResult(
					"findPoolByProductStatusUsername", product, status,
					username);
		} catch (NoResultException b) {
			return null;
		}
	}

	@Override
	public List<ProductPool> findByUsername(String username) {

		return buildNamedQueryGetResults("findPoolByActor", username);

	}

}
