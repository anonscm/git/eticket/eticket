package org.evolvis.eticket.model.entity;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "product_id",
		"actor_id", "type" }))
@NamedQueries({

		@NamedQuery(name = "findPoolsByProduct", query = "SELECT p FROM ProductPool p WHERE p.product = ?1"),
		@NamedQuery(name = "findPoolByActor", query = "SELECT p FROM ProductPool p WHERE p.actor.credential.username  = ?1"),
		@NamedQuery(name = "findPoolByProductStatusActor", query = "SELECT p FROM ProductPool p WHERE p.product = ?1 AND p.type = ?2 AND p.actor = ?3"),
		@NamedQuery(name = "findPoolByProductStatusUsername", query = "SELECT p FROM ProductPool p WHERE p.product = ?1 AND p.type = ?2 AND p.actor.credential.username = ?3"),
		@NamedQuery(name = "findPoolByStatusAndUsername", query = "SELECT p FROM ProductPool p WHERE p.type = ?1 AND p.actor.credential.username = ?2") })
public class ProductPool {

	public enum ProductPoolStatus {
		REGISTERED("REGISTERED"), VOLATILE("VOLATILE"), FIXED("FIXED");

		private String type;

		private static Map<String, ProductPoolStatus> enumToString = new HashMap<String, ProductPool.ProductPoolStatus>();
		static {
			for (ProductPoolStatus pps : values())
				enumToString.put(pps.toString(), pps);
		}

		ProductPoolStatus(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return type;
		}

		public static ProductPoolStatus fromString(String type) {
			return enumToString.get(type);
		}

	}

	@Id
	@GeneratedValue
	private long id;
	@ManyToOne(optional = false)
	private Product product;
	@ManyToOne
	private Actor actor;
	@Column(nullable = false)
	private ProductPoolStatus type;
	private long amount = 1;
	private long productNumber;

	public ProductPool() {
	}

	public ProductPool(ProductPoolDTO productPoolDTO) {
		fromDto(productPoolDTO);
	}

	public void setProduct(Product product) {
		if (product == null)
			throw new IllegalArgumentException("product must not be null.");
		this.product = product;
	}

	public Product getProduct() {
		return product;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setActor(Actor actor) {
		this.actor = actor;

	}

	public Actor getActor() {
		return actor;
	}

	public void setType(ProductPoolStatus type) {
		if (type == null)
			throw new IllegalArgumentException("type must not be null.");
		this.type = type;
	}

	public ProductPoolStatus getType() {
		return type;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public long getAmount() {
		return amount;
	}

	public long getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(long productNumber) {
		this.productNumber = productNumber;
	}

	@Transient
	public void fromDto(ProductPoolDTO productPoolDTO) {
		amount = Long.valueOf(productPoolDTO.getAmount());
		product = new Product(productPoolDTO.getProductDTO());

		if (productPoolDTO.getId() > -1) {
			this.id = productPoolDTO.getId();
		}
	}

	@Transient
	public ProductPoolDTO toDto() {

		ProductDTO productDTO = product.toDto();
		return ProductPoolDTO
				.createProductPoolDTOFromExsistingProductPoolEntity(id,
						productDTO, amount + "");
	}

}
