package org.evolvis.eticket.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "system_id", "uin" }))
@NamedQueries({
		@NamedQuery(name = "findProductBySystemAndUin", query = "SELECT p FROM Product p WHERE p.system = ?1 AND p.uin = ?2"),
		@NamedQuery(name = "findProductbyId", query = "SELECT p FROM Product p WHERE p.id = ?1"),
		@NamedQuery(name = "listProductsBySystemMethod", query = "SELECT p FROM Product p WHERE p.system = ?1 AND p.pat.method = ?2"),
		@NamedQuery(name = "listProductsBySystem", query = "SELECT p FROM Product p WHERE p.system = ?1") })
public class Product {
	private long id;
	private String uin;
	private ESystem system;
	private List<ProductLocale> locales = new ArrayList<ProductLocale>();
	private long expiryTTL;
	private Date deadline;
	private int maxAmount;
	private int currAmount;
	private ProductType productType = ProductType.DIGITAL;
	private ProductAutomaticTransfer pat;

	public Product() {

	}

	public Product(ProductDTO productDTO) {
		this.uin = productDTO.getUin();
		fromDto(productDTO);
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getUin() {
		return uin;
	}

	public void setUin(String uin) {
		this.uin = uin;
	}

	@ManyToOne(optional = false)
	public ESystem getSystem() {
		return system;
	}

	public void setSystem(ESystem system) {
		this.system = system;
	}

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public List<ProductLocale> getLocales() {
		return locales;
	}

	public void setLocales(List<ProductLocale> locales) {
		this.locales = locales;
	}

	public long getExpiryTTL() {
		return expiryTTL;
	}

	public void setExpiryTTL(long expiryTTL) {
		this.expiryTTL = expiryTTL;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public int getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(int maxAmount) {
		this.maxAmount = maxAmount;
	}

	public int getCurrAmount() {
		return currAmount;
	}

	public void setCurrAmount(int currAmount) {
		this.currAmount = currAmount;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public ProductType getProductType() {
		return productType;
	}
	
	@Transient
	public void fromDtoWithoutLocales(ProductDTO productDTO) {
		this.uin = productDTO.getUin();
		this.system = new ESystem(productDTO.getSystem());
		this.expiryTTL = productDTO.getExpiryTTL();
		this.deadline = productDTO.getDeadline();
		this.maxAmount = productDTO.getMaxAmount();
		this.currAmount = productDTO.getCurrAmount();
		this.productType = productDTO.getProductType();

		if (productDTO.getId() > -1) {
			this.id = productDTO.getId();
		}
	}

	@Transient
	public void fromDto(ProductDTO productDTO) {
		this.uin = productDTO.getUin();
		this.system = new ESystem(productDTO.getSystem());
		this.locales = convertLocales(productDTO.getProductLocales());
		this.expiryTTL = productDTO.getExpiryTTL();
		this.deadline = productDTO.getDeadline();
		this.maxAmount = productDTO.getMaxAmount();
		this.currAmount = productDTO.getCurrAmount();
		this.productType = productDTO.getProductType();

		if (productDTO.getId() > -1) {
			this.id = productDTO.getId();
		}
	}

	@Transient
	public ProductDTO toDto() {
		List<ProductLocaleDTO> productLocales = localesToDto();

		ProductDTO result = ProductDTO
				.createProductDTOFromExsistingProductEntity(uin, productType,
						system.toDto(), productLocales
								.toArray(new ProductLocaleDTO[productLocales
										.size()]), currAmount, maxAmount,
						deadline, expiryTTL, id);

		return result;
	}

	private List<ProductLocaleDTO> localesToDto() {
		List<ProductLocaleDTO> productLocales = new ArrayList<ProductLocaleDTO>();
		for (ProductLocale pl : locales) {
			productLocales.add(pl.toDto());
		}
		return productLocales;
	}

	private List<ProductLocale> convertLocales(
			ProductLocaleDTO[] productLocaleDTOs) {
		List<ProductLocale> list = new ArrayList<ProductLocale>();
		if(productLocaleDTOs != null) {
			for (ProductLocaleDTO p : productLocaleDTOs) {
				list.add(new ProductLocale(p));
			}
		}
		return list;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	public ProductAutomaticTransfer getPat() {
		return pat;
	}

	public void setPat(ProductAutomaticTransfer pat) {
		this.pat = pat;
	}

}
