package org.evolvis.eticket.model.crud.product;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.TransferHistory;
import org.evolvis.eticket.model.entity.TransferHistory.TransferState;

@Stateless
@Local(TransferHistoryCRUD.class)
public class TransferHistoryCRUDBean extends
		GenericCUDSkeleton<TransferHistory> implements TransferHistoryCRUD {

	private static final long serialVersionUID = 5011357404869721438L;

	@Override
	public TransferHistory findByPool(ProductPool registerPool) {
		return buildNamedQueryGetSingleResult("findHistoryByPool", registerPool);
	}
	public List<TransferHistory> findallByPool(ProductPool registerPool) {
		return buildNamedQueryGetResults("findHistoryByPool", registerPool);
	}
	
	@Override
	public List<TransferHistory> findBySenderAndTransferState(String senderUsername, TransferState state) {
		return buildNamedQueryGetResults("findHistoryBySenderUsernameAndTransferState", senderUsername, state);
	}
	
	@Override
	public List<TransferHistory> findByRecipientUsernameAndTransferState(String recipientUsername, TransferState state){
		return buildNamedQueryGetResults("findHistoryByRecipientUsernameAndTransferState", recipientUsername, state);
	}

	@Override
	public List<TransferHistory> findByPoolRecipientAndTransferState(
			ProductPool registerPool, Account recipient, TransferState state) {
		return buildNamedQueryGetResults("findHistoryByPoolRecipientAndTransferState", registerPool, recipient, state);
	}

	@Override
	public TransferHistory findById(long id) {
		return find(TransferHistory.class, id);
	}

}
