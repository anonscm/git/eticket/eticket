package org.evolvis.eticket.model.crud.user;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.AccountHistory;
import org.evolvis.eticket.model.entity.ESystem;

@Stateless
@Local(AccountHistoryCRUD.class)
public class AccountHistoryCRUDBean extends GenericCUDSkeleton<AccountHistory>
		implements AccountHistoryCRUD {

	private static final long serialVersionUID = -4591501721674753799L;

	// @PrePersist
	// public void prePersist(Account account) {
	// createAndInsertAccountEvent(account, Action.CREATE);
	// }
	//
	// @PreRemove
	// public void postRemove(Account account) {
	// createAndInsertAccountEvent(account, Action.DELETE);
	// }

	@Override
	public List<AccountHistory> listAccountHistoriesBySystemAndUsername(
			ESystem system, String username) {
		return buildNamedQueryGetResults(
				"listAccountHistoriesBySystemAndUsername", system, username);
	}

}
