package org.evolvis.eticket.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;

@Entity
@NamedQueries({
		@NamedQuery(name = "findProductLocaleByLocaleCodeAndProductId", query = "SELECT pl FROM Product p INNER JOIN p.locales pl WHERE pl.locale = ?1 AND p.id = ?2"),
		@NamedQuery(name = "findProductLocale", query = "SELECT pl FROM ProductLocale pl "),
		@NamedQuery(name = "findProductLocalebyId", query = "SELECT pl FROM ProductLocale pl WHERE pl.id = ?1 "),
		@NamedQuery(name = "findProductLocaleByLocalecodeAndProductUinAndSystemId", query = "SELECT pl FROM Product p LEFT JOIN p.locales pl WHERE pl.locale.localecode=?1 AND p.uin=?2 AND p.system.id=?3"),
		@NamedQuery(name = "findProductLocaleByLocaleCodeAndTemplateUin", query = "SELECT p FROM ProductLocale p WHERE p.locale.localecode = ?1 AND p.template.uin = ?2")})
public class ProductLocale {
	private long id;
	private Locale locale;
	private String currencyCode;
	private double price = -1;
	private double salesTax = -1;
	private String productName;
	private String productDescription;
	private Templates template;

	public ProductLocale() {
	}

	public ProductLocale(ProductLocaleDTO pDto) {
		fromDto(pDto);
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne(optional = false)
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getSalesTax() {
		return salesTax;
	}

	public void setSalesTax(double salesTax) {
		this.salesTax = salesTax;
	}

	public void setTemplate(Templates template) {
		this.template = template;
	}

	@ManyToOne
	public Templates getTemplate() {
		return template;
	}

	public ProductLocaleDTO toDto() {
		return  ProductLocaleDTO.createProductLocaleDTOFromExsistingProductLocaleEntity(id, locale.toDto(), currencyCode,
				productName, productDescription,
				(template == null ? null : template.toDto()), price, salesTax);
	}

	public void fromDto(ProductLocaleDTO pDto) {
		this.locale = new Locale(pDto.getLocale());
		this.currencyCode = pDto.getCurrencyCode();
		this.price = pDto.getPrice();
		this.salesTax = pDto.getSalesTax();
		this.productName = pDto.getProductName();
		this.productDescription = pDto.getProductDescription();
		this.template = new Templates(pDto.getTemplate());
		if(pDto.getId() > -1) {
			this.id = pDto.getId();
		}
	}
	
	
	public void fromDTOupdate(ProductLocaleDTO pDto) {
		this.locale = new Locale(pDto.getLocale());
		this.currencyCode = pDto.getCurrencyCode();
		this.price = pDto.getPrice();
		this.salesTax = pDto.getSalesTax();
		this.productName = pDto.getProductName();
		this.productDescription = pDto.getProductDescription();
		Templates template = new Templates();
		template.fromDto(pDto.getTemplate());
		this.template =template;
		if(pDto.getId() > -1) {
			this.id = pDto.getId();
		}
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(nullable = false)
	public String getProductName() {
		return productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	@Column(nullable = false)
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
}
