package org.evolvis.eticket.model.crud.product;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.ProductLocale;

@Stateless
@Local(ProductLocaleCRUD.class)
public class ProductLocaleCRUDBean extends GenericCUDSkeleton<ProductLocale>
		implements ProductLocaleCRUD {

	private static final long serialVersionUID = -4074861259213970596L;

	@Override
	public ProductLocale findByLocaleCodeTemplateUin(String localecode,
			String templateUin) {
		List<ProductLocale> locales = buildNamedQueryGetResults(
				"findProductLocaleByLocaleCodeAndTemplateUin", localecode,
				templateUin);
		if (locales.size() > 0) {
			return locales.get(0);
		} else {
			return null;
		}
	}

	@Override


	public ProductLocale findByLocaleCodeAndProductId(Locale locale,
			long productId) {
		return buildNamedQueryGetSingleResult(
				"findProductLocaleByLocaleCodeAndProductId", locale, productId);
	}

	@Override
	public ProductLocale findById(long id) {
		return find(ProductLocale.class, id);
	}


	@Override
	public ProductLocale findProductLocaleByLocalecodeAndProductUinAndSystemId(
			String localcode, String uin, long systemId) {
		return buildNamedQueryGetSingleResult(
				"findProductLocaleByLocalecodeAndProductUinAndSystemId",
				localcode, uin, systemId);
	}


	@Override
	public ProductLocale findProductLocaleById(long Id) {
		return buildNamedQueryGetSingleResult("findProductLocalebyId",Id);
	}
	

	
	@Override
	public List <ProductLocale> findallProductLocales() {
		
		return buildNamedQueryGetResults("findProductLocale");
	}
	


}
