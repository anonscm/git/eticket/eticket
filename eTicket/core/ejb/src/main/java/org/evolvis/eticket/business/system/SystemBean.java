package org.evolvis.eticket.business.system;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;

import org.evolvis.eticket.business.platform.PlatformBeanService;
import org.evolvis.eticket.interceptors.CheckPlatformAdmin;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.platform.PlatformAccountCRUD;
import org.evolvis.eticket.model.crud.platform.PlatformCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.system.RoleCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.Credentials;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.PlatformAccount;
import org.evolvis.eticket.model.entity.PlatformCredentials;
import org.evolvis.eticket.model.entity.Role;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;

@Stateless
@Remote(SystemBeanService.class)
@Interceptors({ CheckPlatformAdmin.class })
public class SystemBean implements SystemBeanService {

	private static final long serialVersionUID = 5110729437350182529L;

	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;

	@EJB
	private PlatformBeanService platformBeanLocal;

	@EJB
	private LocaleCRUD localeCRUDBeanLocal;

	@EJB
	private RoleCRUD roleCRUDBeanLocal;

	@EJB
	private AccountCRUD accountCRUDBeanLocal;

	@EJB
	private PlatformAccountCRUD platformAccountCRUDBeanLocal;

	@EJB
	private PlatformCRUD platformCrudBeanLocal;

	/**
	 * Creates the sys admin.
	 * 
	 * @param platformadmin
	 *            the platformadmin
	 * @param system
	 *            the system
	 */
	private void createSysAdmin(AccountDTO platformadmin, ESystem system) {
		PlatformAccount pa = platformAccountCRUDBeanLocal
				.findByUsername(platformadmin.getUsername());
		Account account = generateAccount(system, pa.getCredentials());
		List<Role> roles = roleCRUDBeanLocal.listSystem(system);
		// sysadmin is the highest role
		for (Role r : roles) {
			account.getRoles().add(r);
		}
		accountCRUDBeanLocal.insert(account);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.evolvis.eticket.business.system.SystemBeanRemote#createSystem(org
	 * .evolvis.eticket.model.entity.dto.AccountDTO, long,
	 * org.evolvis.eticket.model.entity.dto.ESystemDTO)
	 */
	@Override
	public void createSystem(AccountDTO platformadmin, String platformId,
			ESystemDTO systemDTO) {
		ESystem system = new ESystem();
		setSystemFromDTO(systemDTO, system);
		system.setPlatform(platformCrudBeanLocal.findByName(platformId));
		eSystemCRUDBeanLocal.insert(system);
		platformBeanLocal.addSystemToPlatform(platformadmin, platformId,
				system.toDto());
		insertDefaultRoles(system);
		// create a new sysadmin with the credentials from platformadmin, so
		// there is at least one sysadmin available
		createSysAdmin(platformadmin, system);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.evolvis.eticket.business.system.SystemBeanRemote#deleteSystem(org
	 * .evolvis.eticket.model.entity.dto.AccountDTO, long,
	 * org.evolvis.eticket.model.entity.dto.ESystemDTO)
	 */
	@Override
	public void deleteSystem(AccountDTO platformadmin, String platformId,
			ESystemDTO systemDTO) {
		ESystem system = eSystemCRUDBeanLocal.findByName(systemDTO.getName());
		platformBeanLocal.deleteSystemFromPlatform(platformadmin, platformId,
				system.toDto());
		eSystemCRUDBeanLocal.delete(system);
	}

	private Account generateAccount(ESystem system,
			PlatformCredentials platformCredentials) {
		Account account = new Account();
		account.setActivated(true);
		// share credentials with platformadmin
		account.setCredential(new Credentials(
				platformCredentials.getUsername(), platformCredentials
						.getSalt(), platformCredentials.getPasswordHash(), null));
		account.setSystem(system);
		return account;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.evolvis.eticket.business.system.SystemBeanRemote#getSystemById(long)
	 */
	@Override
	@ExcludeClassInterceptors
	public ESystemDTO getSystemById(long id) {
		return eSystemCRUDBeanLocal.findById(id).toDto(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.evolvis.eticket.business.system.SystemBeanRemote#getSystemByName(
	 * java.lang.String)
	 */
	@Override
	@ExcludeClassInterceptors
	public ESystemDTO getSystemByName(String systemname) {
		return eSystemCRUDBeanLocal.findByName(systemname).toDto(false);
	}

	/**
	 * Insert default roles.
	 * 
	 * @param system
	 *            the system
	 */
	private void insertDefaultRoles(ESystem system) {
		insertRole(system, AllowedRoles.USER.toString());
		insertRole(system, AllowedRoles.MODERATOR.toString());
		insertRole(system, AllowedRoles.PRODUCTADMIN.toString());
		insertRole(system, AllowedRoles.SYSADMIN.toString());
		// platformadmin is independent from a system
	}

	/**
	 * Insert role.
	 * 
	 * @param system
	 *            the system
	 * @param roleName
	 *            the role name
	 */
	private void insertRole(ESystem system, String roleName) {
		Role role = new Role();
		role.setName(roleName);
		role.setSystem(system);
		roleCRUDBeanLocal.insert(role);
	}

	public void setAccountCRUDBeanLocal(AccountCRUD accountCRUDBeanLocal) {
		this.accountCRUDBeanLocal = accountCRUDBeanLocal;
	}

	public void seteSystemCRUDBeanLocal(ESystemCRUD eSystemCRUDBeanLocal) {
		this.eSystemCRUDBeanLocal = eSystemCRUDBeanLocal;
	}

	public void setLocaleCRUDBeanLocal(LocaleCRUD localeCRUDBeanLocal) {
		this.localeCRUDBeanLocal = localeCRUDBeanLocal;
	}

	public void setPlatformAccountCRUDBeanLocal(
			PlatformAccountCRUD platformAccountCRUDBeanLocal) {
		this.platformAccountCRUDBeanLocal = platformAccountCRUDBeanLocal;
	}

	public void setPlatformBeanLocal(PlatformBeanService platformBeanLocal) {
		this.platformBeanLocal = platformBeanLocal;
	}

	public void setPlatformCrudBeanLocal(PlatformCRUD platformCrudBeanLocal) {
		this.platformCrudBeanLocal = platformCrudBeanLocal;
	}

	public void setRoleCRUDBeanLocal(RoleCRUD roleCRUDBeanLocal) {
		this.roleCRUDBeanLocal = roleCRUDBeanLocal;
	}

	/**
	 * Sets the system from dto.
	 * 
	 * @param systemDTO
	 *            the system dto
	 * @param system
	 *            the system
	 * @param platformID
	 */
	private void setSystemFromDTO(ESystemDTO systemDTO, ESystem system) {
		system.setName(systemDTO.getName());
		system.setDefaultLocale(localeCRUDBeanLocal.findByCode(systemDTO
				.getDefaultLocale().getCode()));
		for (LocaleDTO dto : systemDTO.getSupportedLocales()) {
			Locale locale = localeCRUDBeanLocal.findByCode(dto.getCode());
			if (!system.getSupportedLocales().contains(locale)) {
				system.getSupportedLocales().add(locale);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.evolvis.eticket.business.system.SystemBeanRemote#updateSystem(org
	 * .evolvis.eticket.model.entity.dto.AccountDTO, long,
	 * org.evolvis.eticket.model.entity.dto.ESystemDTO)
	 */
	@Override
	public void updateSystem(AccountDTO platformadmin, String platformId,
			ESystemDTO systemDTO) {
		ESystem system;
		if (systemDTO.getId() > 0)
			system = eSystemCRUDBeanLocal.findById(systemDTO.getId());
		else
			system = eSystemCRUDBeanLocal.findByName(systemDTO.getName());
		setSystemFromDTO(systemDTO, system);
		eSystemCRUDBeanLocal.update(system);
	}

	@ExcludeClassInterceptors
	public LocaleDTO getLocaleById(long id) {
		return localeCRUDBeanLocal.findById(id).toDto();
	}

}
