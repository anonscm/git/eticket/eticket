package org.evolvis.eticket.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.util.CalculateHash;

@Entity
@NamedQueries({
		@NamedQuery(name = "findPlatformAccountByUsername", query = "SELECT p FROM PlatformAccount p WHERE p.credentials.username = ?1"),
		@NamedQuery(name = "listPlatformAccounts", query = "SELECT p FROM PlatformAccount p") })
public class PlatformAccount {
	private long id;
	private PlatformCredentials credentials;

	private boolean initial = false;

	public PlatformAccount() {
	}

	public PlatformAccount(AccountDTO platformAdmin) {
		String salt = CalculateHash.generateSalt();
		setCredentials(new PlatformCredentials(platformAdmin.getUsername(),
				salt, CalculateHash.calculateWithSalt(salt,
						platformAdmin.getPassword())));
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	public PlatformCredentials getCredentials() {
		return credentials;
	}

	public void setCredentials(PlatformCredentials credentials) {
		this.credentials = credentials;
	}

	public void setInitial(boolean initial) {
		this.initial = initial;
	}

	@Transient
	public boolean isInitial() {
		return initial;
	}

	public AccountDTO toDto() {
		return AccountDTO.initWithExistingHash(null,
				getCredentials().getUsername(), getCredentials()
						.getPasswordHash(), RoleDTO.getPlatformAdmin(), null);
	}

}
