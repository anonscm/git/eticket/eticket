package org.evolvis.eticket.model.crud.product;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.business.product.TransferMethod;
import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;

@Stateless
@Local(ProductCRUD.class)
public class ProductCRUDBean extends GenericCUDSkeleton<Product> implements
		ProductCRUD {

	private static final long serialVersionUID = -7233663546531112587L;

	@Override
	public Product findById(long id) {
		return find(Product.class, id);
	}

	@Override
	public Product findBySystemAndUin(ESystem system, String uin) {
		return buildNamedQueryGetSingleResult("findProductBySystemAndUin",
				system, uin);
	}

	@Override
	public List<Product> listBySystemClassAndMethod(ESystem system,
			Class<?> clazz, String method) {
		TransferMethod transferMethod = TransferMethod.getTransferMethod(clazz,
				method);
		if (transferMethod == null)
			throw new IllegalArgumentException(clazz.getName() + "." + method
					+ " are not supported for transfering products.");
		return buildNamedQueryGetResults("listProductsBySystemMethod", system,
				transferMethod);
	}

	@Override
	public Product findProductbyId(long Id) {
		Product product = null;
		try {
			product = buildNamedQueryGetSingleResult("findProductbyId", Id);
		} catch (Exception e) {
		}
		return product;

	}

	public List<Product> listBySystemClass(ESystem system) {

		return buildNamedQueryGetResults("listProductsBySystem", system);
	}
}
