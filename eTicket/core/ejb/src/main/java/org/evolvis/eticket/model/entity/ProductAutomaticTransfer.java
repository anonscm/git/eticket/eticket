package org.evolvis.eticket.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.evolvis.eticket.business.product.TransferMethod;

@Entity

public class ProductAutomaticTransfer {
    @Id
    @GeneratedValue
    private long id;
    @Column(nullable = false)
    private TransferMethod method;
    private long amount;

    public ProductAutomaticTransfer() {

    }

    public ProductAutomaticTransfer(TransferMethod method, long amount) {
	this.setMethod(method);
	this.amount = amount;
    }

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public long getAmount() {
	return amount;
    }

    public void setAmount(long amount) {
	this.amount = amount;
    }

    public TransferMethod getMethod() {
	return method;
    }

    public void setMethod(TransferMethod method) {
	this.method = method;
    }
}
