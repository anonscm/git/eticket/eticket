
package org.evolvis.eticket.model.crud.system;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.TextTemplate;


/**
 * The Interface TextTemplateCRUDBeanLocal.
 */
public interface TextTemplateCRUD extends GenericCUD<TextTemplate> {

	/**
     * Finds entity by system locale and name.
     *
     * @param system
     *            the system
     * @param locale
     *            the locale
     * @param name
     *            the name
     * @return the text template
     */
	public TextTemplate findBySystemLocaleAndName(ESystem system, Locale locale, String name);

	/**
     * List entities in system locale.
     *
     * @param system
     *            the system
     * @param locale
     *            the locale
     * @return the list entities in
     */
	public List<TextTemplate> listSystemLocale(ESystem system, Locale locale);
}
