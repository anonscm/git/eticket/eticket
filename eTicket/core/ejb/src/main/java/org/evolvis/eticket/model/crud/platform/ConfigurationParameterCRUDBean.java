package org.evolvis.eticket.model.crud.platform;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.ConfigurationParameter;
import org.evolvis.eticket.model.entity.ESystem;
@Stateless
@Local(ConfigurationParameterCRUD.class)
public class ConfigurationParameterCRUDBean extends
	GenericCUDSkeleton<ConfigurationParameter> implements
	ConfigurationParameterCRUD {

    private static final long serialVersionUID = -1484332230279688120L;

    @Override
    public ConfigurationParameter findById(long id) {
	return find(ConfigurationParameter.class, id);
    }

    @Override
    public ConfigurationParameter findBySystemAndKey(ESystem system, String key) {
	return buildNamedQueryGetSingleResult(
		"findConfigurationParameterBySystemAndKey", system, key);
    }

    @Override
    public List<ConfigurationParameter> listConfigurationParameterInSystem(
	    ESystem system) {
	return buildNamedQueryGetResults("listConfigurationParameterInSystem",
		system);
    }

}
