package org.evolvis.eticket.business.system;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;

import org.evolvis.eticket.annotation.AllowedRole;
import org.evolvis.eticket.interceptors.CheckAccountRights;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.system.TextTemplateCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.TextTemplate;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO;

@Interceptors({ CheckAccountRights.class })
@AllowedRole(AllowedRoles.SYSADMIN)
@Stateless
@Remote(TextTemplateBeanService.class)
public class TextTemplateBean implements TextTemplateBeanService {

	private static final long serialVersionUID = 2397913271872801519L;

	@EJB
	private TextTemplateCRUD templateCRUDBeanLocal;

	
	public void setTemplateCRUDBeanLocal(
			TextTemplateCRUD templateCRUDBeanLocal) {
		this.templateCRUDBeanLocal = templateCRUDBeanLocal;
	}

	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;

	
	public void seteSystemCRUDBeanLocal(
			ESystemCRUD eSystemCRUDBeanLocal) {
		this.eSystemCRUDBeanLocal = eSystemCRUDBeanLocal;
	}

	@EJB
	private LocaleCRUD localeCRUDBeanLocal;

	
	public void setLocaleCRUDBeanLocal(LocaleCRUD localeCRUDBeanLocal) {
		this.localeCRUDBeanLocal = localeCRUDBeanLocal;
	}

	@Override
	public void insert(AccountDTO caller, TextTemplateDTO textTemplate) {
		TextTemplate template = createTamplate(textTemplate);
		templateCRUDBeanLocal.insert(template);
	}

	private TextTemplate createTamplate(TextTemplateDTO textTemplate) {
		TextTemplate template = new TextTemplate(textTemplate);
		ESystem system = eSystemCRUDBeanLocal.findByName(textTemplate
				.getSystemDTO().getName());
		Locale locale = localeCRUDBeanLocal.findByCode(textTemplate
				.getLocaleDTO().getCode());
		template.setSystem(system);
		template.setLocale(locale);
		return template;
	}

	private TextTemplate getTextTemplate(TextTemplateDTO textTemplate) {
		ESystem system = eSystemCRUDBeanLocal.findByName(textTemplate
				.getSystemDTO().getName());
		Locale locale = localeCRUDBeanLocal.findByCode(textTemplate
				.getLocaleDTO().getCode());
		return templateCRUDBeanLocal.findBySystemLocaleAndName(system, locale,
				textTemplate.getName());
	}

	@Override
	public void update(AccountDTO caller, TextTemplateDTO textTemplate) {
		TextTemplate template = getTextTemplate(textTemplate);
		template.fromDto(textTemplate);
		templateCRUDBeanLocal.update(template);
	}

	@Override
	public void delete(AccountDTO caller, TextTemplateDTO textTemplate) {
		templateCRUDBeanLocal.delete(getTextTemplate(textTemplate));
	}

	@ExcludeClassInterceptors
	@Override
	public TextTemplateDTO find(ESystemDTO eSystem, LocaleDTO localeDto,
			String name) {
		ESystem system = eSystemCRUDBeanLocal.findByName(eSystem.getName());
		Locale locale = localeCRUDBeanLocal.findByCode(localeDto.getCode());
		return templateCRUDBeanLocal.findBySystemLocaleAndName(system, locale,
				name).toDTO();
	}
}
