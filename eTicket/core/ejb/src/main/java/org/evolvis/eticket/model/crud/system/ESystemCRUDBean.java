package org.evolvis.eticket.model.crud.system;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.ESystem;

/**
 * The Class ESystemCRUDBean.
 */
@Stateless
@Local(ESystemCRUD.class)
public class ESystemCRUDBean extends GenericCUDSkeleton<ESystem> implements
		ESystemCRUD {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3777076965506599200L;

	/* (non-Javadoc)
	 * @see org.evolvis.model.crud.ESystemCRUDBeanLocal#findByName(java.lang.String)
	 */
	@Override
	public ESystem findByName(String name) {
		ESystem eSystem = (ESystem)buildNamedQueryGetSingleResult("findSystemByName", name);
		return eSystem;
	}

	/* (non-Javadoc)
	 * @see org.evolvis.model.crud.ESystemCRUDBeanLocal#findById(long)
	 */
	@Override
	public ESystem findById(long id) {
		return find(ESystem.class, id);
	}

}
