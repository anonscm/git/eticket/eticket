package org.evolvis.eticket.model.crud.user;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.dto.AccountDTO;

public interface AccountCRUD extends GenericCUD<Account> {
    public Account findById(long id);

    public Account findBySystemAndUserName(ESystem system, String username);

    public List<Account> listInactiveAccountsInSystem(ESystem system);

	public Account checkPwGetAccount(ESystem system, AccountDTO account) throws IllegalAccessException;
	
	public Account findBySystemAndOpenId(ESystem system, String openId);
	
	public List<Account> listAccountsInSystem(ESystem system);

	public List <Account> findBySystemAndLikeUserName(ESystem system,
			String username);

	public List<Account> findBySystemAndLikeNameInPerosnalinformation(ESystem system,
			String username);
	
	
}
