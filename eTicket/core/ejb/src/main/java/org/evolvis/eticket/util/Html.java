package org.evolvis.eticket.util;

public class Html {
	public static String decodeFromHtml(String in) {
		if (in != null) {
			return in.replaceAll("&auml;", "ä").replaceAll("&uuml;", "ü")
					.replaceAll("&ouml;", "ö").replaceAll("&Auml;", "Ä")
					.replaceAll("&Uuml;", "Ü").replaceAll("&Ouml;", "Ö")
					.replaceAll("&szlig;", "ß");
		} else {
			return null;
		}

	}

	public static String encodeToHtml(String in) {
		if (in != null) {
			return in.replaceAll("ä", "&auml;").replaceAll("ü", "&uuml;")
					.replaceAll("ö", "&ouml;").replaceAll("Ä", "&Auml;")
					.replaceAll("Ü", "&Uuml;").replaceAll("Ö", "&Ouml;")
					.replaceAll("ß", "&szlig;");
		} else {
			return null;
		}
	}
}
