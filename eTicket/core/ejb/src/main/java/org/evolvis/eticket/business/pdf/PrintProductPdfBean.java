package org.evolvis.eticket.business.pdf;

import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.PdfField;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;

@Stateless
@Remote(PrintProductPdfBeanService.class)
public class PrintProductPdfBean implements PrintProductPdfBeanService {

	private static final long serialVersionUID = -3316012670197680517L;

	@EJB
	private ConfigurationParameterCRUD configurationParameterCRUD;
	@EJB
	private ESystemCRUD eSystemCRUD;
	
	
	@Override
	public byte[] createPDF(Map<PdfField, Object> values,
			TemplatesDTO pdfTemplate, String productNumber, ESystemDTO eSystemDTO ) {
		
		if(Boolean.valueOf(values.get(PdfField.BARCODE).toString()) == true){
			ESystem system = eSystemCRUD.findByName(eSystemDTO.getName());
			String fixedTicketNumber = configurationParameterCRUD.findBySystemAndKey(system, CPKey.FIXED_TICKETNUMBER.toString()).getValue();
			values.put(PdfField.BARCODE, new BarcodeCreator().generateBarcodeImage(productNumber,fixedTicketNumber));
		}
		
		return PdfCreator.generate(pdfTemplate.getBinaryData(), values);
	}


}
