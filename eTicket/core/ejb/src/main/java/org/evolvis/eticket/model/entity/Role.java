package org.evolvis.eticket.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "NAME",
		"SYSTEM_ID" }))
@NamedQueries({
		@NamedQuery(name = "findRoleBySystemAndName", query = "SELECT r FROM Role r WHERE r.system = ?1 AND r.name = ?2"),
		@NamedQuery(name = "listRolesInSystem", query = "SELECT r FROM Role r WHERE r.system = ?1") })
public class Role {
	private long id;
	private String name;
	private ESystem system;
	
	@Column(nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne(optional = false)
	public ESystem getSystem() {
		return system;
	}

	public void setSystem(ESystem system) {
		this.system = system;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Role))
			return false;
		Role role = (Role) obj;
		return getId() == role.getId();
	}

	@Override
	public int hashCode() {
		int result = 17;
		result += 31 * result + id;
		return result;
	}

}
