package org.evolvis.eticket.model.crud.platform;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.ConfigurationParameter;
import org.evolvis.eticket.model.entity.ESystem;

public interface ConfigurationParameterCRUD extends
	GenericCUD<ConfigurationParameter> {
    public ConfigurationParameter findById(long id);

    public ConfigurationParameter findBySystemAndKey(ESystem system, String key);

    public List<ConfigurationParameter> listConfigurationParameterInSystem(
	    ESystem system);

}
