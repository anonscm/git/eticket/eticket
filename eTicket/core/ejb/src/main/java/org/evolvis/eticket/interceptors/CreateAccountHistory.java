package org.evolvis.eticket.interceptors;

import java.lang.reflect.Method;
import java.util.GregorianCalendar;

import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.evolvis.eticket.model.crud.user.AccountCRUDBean;
import org.evolvis.eticket.model.crud.user.AccountHistoryCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.AccountHistory;
import org.evolvis.eticket.model.entity.CredentialHistory.Action;

public class CreateAccountHistory {
	@EJB
	private AccountHistoryCRUD accountHistoryCRUDBeanLocal;

	public void setAccountHistoryCRUDBeanLocal(
			AccountHistoryCRUD accountHistoryCRUDBeanLocal) {
		this.accountHistoryCRUDBeanLocal = accountHistoryCRUDBeanLocal;
	}

	@AroundInvoke
	public Object logCall(InvocationContext context) throws Exception {
		if (context.getTarget().getClass().equals(AccountCRUDBean.class)
				&& context.getParameters()[0] instanceof Account)
			createAccountEvent(context.getMethod(),
					(Account) context.getParameters()[0]);
		return context.proceed();
	}

	private void createAccountEvent(Method method, Account account)
			throws SecurityException, NoSuchMethodException {
		// just using the circumstance that the names of GenericCUD does equal the
		// action names (insert, update, delete)
		Action action = Action.fromString(method.getName());
		if (action != null)
			createAndInsertAccountEvent(account, action);
	}

	private void createAndInsertAccountEvent(Account account, Action action) {
		AccountHistory accountHistory = new AccountHistory();
		accountHistory.setAction(action);
		accountHistory.setSystem(account.getSystem());
		accountHistory.setUsername(account.getCredential().getUsername());
		accountHistory.setDate(GregorianCalendar.getInstance().getTime());
		accountHistoryCRUDBeanLocal.insert(accountHistory);
	}

}
