package org.evolvis.eticket.model.crud.platform;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.PlatformAccountHistory;

public interface PlatformAccountHistoryCRUD extends
	GenericCUD<PlatformAccountHistory> {
    public List<PlatformAccountHistory> listPlatformAccountHistoryFromUsername(
	    String username);

    public List<PlatformAccountHistory> listPlatformAccountHistoryCreatedByUsername(
	    String username);

}
