package org.evolvis.eticket.business.platform;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;

import org.evolvis.eticket.interceptors.CheckPlatformAdmin;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.platform.PlatformAccountCRUD;
import org.evolvis.eticket.model.crud.platform.PlatformCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.Platform;
import org.evolvis.eticket.model.entity.PlatformAccount;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;

@Stateless
@Remote(PlatformBeanService.class)
@Interceptors({ CheckPlatformAdmin.class })
public class PlatformBean implements PlatformBeanService {

	@EJB
	private PlatformCRUD platformCRUDBeanLocal;
	@EJB
	private PlatformAccountCRUD platformAccountCRUDBeanLocal;
	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;

	public void setPlatformCRUDBeanLocal(PlatformCRUD platformCRUDBeanLocal) {
		this.platformCRUDBeanLocal = platformCRUDBeanLocal;
	}

	public void setPlatformAccountCRUDBeanLocal(
			PlatformAccountCRUD platformAccountCRUDBeanLocal) {
		this.platformAccountCRUDBeanLocal = platformAccountCRUDBeanLocal;
	}

	public void seteSystemCRUDBeanLocal(ESystemCRUD eSystemCRUDBeanLocal) {
		this.eSystemCRUDBeanLocal = eSystemCRUDBeanLocal;
	}

	public void setLocaleCRUDBeanLocal(LocaleCRUD localeCRUDBeanLocal) {
		this.localeCRUDBeanLocal = localeCRUDBeanLocal;
	}

	@EJB
	private LocaleCRUD localeCRUDBeanLocal;

	private static final long serialVersionUID = -4096420906926220966L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.evolvis.eticket.business.platform.PlatformBeanLocal#
	 * createPlatformAndReturnId
	 * (org.evolvis.eticket.model.entity.dto.AccountDTO)
	 */
	@Override
	@ExcludeClassInterceptors
	public void createPlatform(AccountDTO platformAdmin, String platformName)
			throws IllegalAccessException {
		PlatformAccount platformAccount = getPlatformOwner(platformAdmin);
		Platform platform = new Platform(platformName);
		platform.setOwner(platformAccount);
		platformCRUDBeanLocal.insert(platform);
	}

	/**
	 * Gets the platform owner.
	 * 
	 * @param platformAdmin
	 *            the platform admin
	 * @return the platform owner
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 */
	private PlatformAccount getPlatformOwner(AccountDTO platformAdmin)
			throws IllegalAccessException {
		PlatformAccount platformAccount;
		if (platformAccountCRUDBeanLocal.listPlatformAccounts().size() == 0) {
			platformAccount = new PlatformAccount(platformAdmin);
			platformAccountCRUDBeanLocal.insert(platformAccount);
		} else
			platformAccount = platformAccountCRUDBeanLocal
					.checkPassword(platformAdmin);
		return platformAccount;
	}

	@Override
	public void addSystemToPlatform(AccountDTO platformAdmin,
			String platformId, ESystemDTO system) {
		Platform platform = getPlatform(platformId);
		ESystem eSystem = getSystem(system);
		platform.getSystems().add(eSystem);
		platformCRUDBeanLocal.update(platform);
	}

	private ESystem getSystem(ESystemDTO system) {
		ESystem eSystem = eSystemCRUDBeanLocal.findByName(system.getName());
		return eSystem;
	}

	@Override
	public void deleteSystemFromPlatform(AccountDTO platformAdmin,
			String platformId, ESystemDTO system) {
		Platform platform = getPlatform(platformId);
		ESystem eSystem = getSystem(system);
		platform.getSystems().remove(eSystem);
		platformCRUDBeanLocal.update(platform);
	}

	private Platform getPlatform(String id) {
		Platform platform = platformCRUDBeanLocal.findByName(id);
		return platform;
	}

	@Override
	public void deletePlatform(AccountDTO platformAdmin, String platformId) {
		Platform platform = getPlatform(platformId);
		platformCRUDBeanLocal.delete(platform);
	}

	@Override
	public void createLocale(AccountDTO platformAdmin, String platFormID,
			LocaleDTO locale) {
		Locale locale2 = new Locale(locale);
		localeCRUDBeanLocal.insert(locale2);
	}

	@Override
	@ExcludeClassInterceptors
	public LocaleDTO getLocale(String localecode) {
		return localeCRUDBeanLocal.findByCode(localecode).toDto();
	}

	@Override
	public void deleteLocale(AccountDTO platformAdmin, String id,
			String localecode) {
		localeCRUDBeanLocal.delete(localeCRUDBeanLocal.findByCode(localecode));

	}

}
