package org.evolvis.eticket.model.crud.platform;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Locale;

/**
 * The Class LocaleCRUDBean.
 */
@Stateless
@Local(LocaleCRUD.class)
public class LocaleCRUDBean extends GenericCUDSkeleton<Locale> implements LocaleCRUD {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6711419242598251696L;

	/* (non-Javadoc)
	 * @see org.evolvis.model.crud.LocaleCRUDBeanLocal#findById(long)
	 */
	@Override
	public Locale findById(long id) {
		return find(Locale.class, id);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.model.crud.LocaleCRUDBeanLocal#findByName(java.lang.String)
	 */
	@Override
	public Locale findByCode(String code) {
		return buildNamedQueryGetSingleResult("findLocaleByCode", code);
	}

}
