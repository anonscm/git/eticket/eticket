package org.evolvis.eticket.business.product;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;

import org.evolvis.eticket.annotation.AllowedRole;
import org.evolvis.eticket.interceptors.CheckAccountRights;
import org.evolvis.eticket.model.crud.product.ProductNumberCRUD;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductNumber;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductNumberDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;

@Interceptors({ CheckAccountRights.class })
@AllowedRole(AllowedRoles.PRODUCTADMIN)
@Stateless
@Remote(ProductNumberBeanService.class)
public class ProductNumberBean implements ProductNumberBeanService{
	
	@EJB
	private ProductNumberCRUD productNumberCRUDLocal;
	
   @Override
	public void cerateProductNumber(AccountDTO caller, ProductNumberDTO productNumber) {
		ProductNumber pNumber = new ProductNumber();
		pNumber.fromDto(productNumber);
		productNumberCRUDLocal.insert(pNumber);
	}
	
   @Override
	public void updateProductNumber(AccountDTO caller, ProductNumberDTO productNumber) {
	   Product product = new Product(productNumber.getProduct());
		ProductNumber number = productNumberCRUDLocal.find(product);
		productNumberCRUDLocal.update(setProductNumber(number, productNumber));
	}
	
	@Override
	public void deleteProductNumber(AccountDTO caller, ProductNumberDTO productNumber) {
		Product product = new Product(productNumber.getProduct());
		ProductNumber number = productNumberCRUDLocal.find(product);
		productNumberCRUDLocal.delete(number);
	}

	@Override
	@ExcludeClassInterceptors
	public String getProductNumber(ProductDTO productDTO) {		
		Product product = new Product(productDTO);
		ProductNumber number = productNumberCRUDLocal.find(product);
		String productNumber = number.getCurrentNumber()+"";		
		number.setCurrentNumber(number.getCurrentNumber() + number.getIncrement());
		productNumberCRUDLocal.update(number);		
		return productNumber;
	}
	
	private ProductNumber setProductNumber(ProductNumber productNumber, ProductNumberDTO productNumberDTO){
		productNumber.setCurrentNumber(productNumberDTO.getCurrentNumber());
		productNumber.setIncrement(productNumberDTO.getIncrement());
		productNumber.setMaxNumber(productNumberDTO.getMaxNumber());
		return productNumber;
	}
}
