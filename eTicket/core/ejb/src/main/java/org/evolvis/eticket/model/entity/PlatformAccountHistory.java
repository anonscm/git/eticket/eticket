package org.evolvis.eticket.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = "listPlatformAccountHistoryFromUsername", query = ("SELECT p FROM PlatformAccountHistory p WHERE p.username = ?1")),
	@NamedQuery(name = "listPlatformAccountHistoryCreatedByUsername", query = ("SELECT p FROM PlatformAccountHistory p WHERE p.createdBy = ?1")) })
public class PlatformAccountHistory extends CredentialHistory {
    private String createdBy;

    @Column(nullable = false)
    public String getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
    }
}
