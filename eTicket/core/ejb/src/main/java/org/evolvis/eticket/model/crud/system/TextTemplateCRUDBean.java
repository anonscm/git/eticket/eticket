package org.evolvis.eticket.model.crud.system;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.TextTemplate;

/**
 * The Class RoleCRUDBean.
 */
@Stateless
@Local(TextTemplateCRUD.class)
public class TextTemplateCRUDBean extends GenericCUDSkeleton<TextTemplate> implements TextTemplateCRUD {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 966179077108817257L;

	/* (non-Javadoc)
	 * @see org.evolvis.model.crud.RoleCRUDBeanLocal#findBySystemAndName(org.evolvis.model.entity.ESystem, java.lang.String)
	 */
	@Override
	public TextTemplate findBySystemLocaleAndName(ESystem system, Locale locale, String name) {
		return buildNamedQueryGetSingleResult("findTextTemplateBySystemLocaleName", system, locale, name);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.model.crud.RoleCRUDBeanLocal#listSystem(org.evolvis.model.entity.ESystem)
	 */
	@Override
	public List<TextTemplate> listSystemLocale(ESystem system, Locale locale) {
		return buildNamedQueryGetResults("listTextTemplateInSystemLocale", system, locale);
	}



}
