package org.evolvis.eticket.model.crud.product;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.TransferHistory;
import org.evolvis.eticket.model.entity.TransferHistory.TransferState;

public interface TransferHistoryCRUD extends GenericCUD<TransferHistory>{

	public TransferHistory findByPool(ProductPool registerPool);
	public List <TransferHistory> findallByPool(ProductPool registerPool);
	
	public List<TransferHistory> findBySenderAndTransferState(String senderUsername, TransferState state);
	
	public List<TransferHistory> findByRecipientUsernameAndTransferState(String recipientUsername, TransferState state);
	
	public List<TransferHistory> findByPoolRecipientAndTransferState(ProductPool registerPool, Account sender, TransferState state);

	public TransferHistory findById(long id);
}
