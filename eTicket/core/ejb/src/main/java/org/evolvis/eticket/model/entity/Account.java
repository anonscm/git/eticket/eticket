package org.evolvis.eticket.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;

@Entity
@NamedQueries({
		@NamedQuery(name = "findAccountBySystemAndUsername", query = "SELECT a FROM Account a WHERE a.system = ?1 AND a.credential.username = ?2"),
		@NamedQuery(name = "listInactiveAccountsInSystem", query = "SELECT a FROM Account a WHERE a.system = ?1 AND a.activated = false"),
		@NamedQuery(name = "searchLikeByNameAndSystem", query = "SELECT a FROM Account a WHERE  a.credential.username LIKE (?1)"),
		@NamedQuery(name = "searchLikeByNameAndSysteminPersonalInformation", query = "SELECT a FROM Account a Inner Join a.personalInformation p WHERE  p.firstname LIKE (?1) OR p.lastname LIKE (?1)"),
		@NamedQuery(name = "listAccountsInSystem", query = "SELECT a FROM Account a WHERE a.system = ?1"),
		@NamedQuery(name = "findAccountBysystemAndOpenid", query = "SELECT a FROM Account a WHERE a.system = ?1 AND a.credential.openId = ?2" ) })
		
		
// @EntityListeners(AccountHistoryCRUDBean.class)
public class Account extends Actor {

	private PersonalInformation personalInformation;
	private boolean activated = false;
	
	private ESystem system;
	
	public Account() {
		
	}
	
	@OneToOne(cascade = (CascadeType.ALL))
	public PersonalInformation getPersonalInformation() {
		return personalInformation;
	}

	public void setPersonalInformation(PersonalInformation personalInformation) {
		this.personalInformation = personalInformation;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	
	public void setSystem(ESystem eSystem) {
		if (getPlatform() == null){
			setPlatform(eSystem.getPlatform());
		}
		this.system = eSystem;
	}

	@ManyToOne
	public ESystem getSystem() {
		return system;
	}
	
	@Transient
	public AccountDTO toAccountDTO() {
		return AccountDTO
				.initWithExistingHash(getSystem().toDto(), getCredential()
						.getUsername(), getCredential().getPasswordHash(),
						RoleDTO.get(AllowedRoles.fromInt(getRoles().size()),
								getSystem().getId()), getCredential().getOpenId());
	}
}
