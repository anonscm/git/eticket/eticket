package org.evolvis.eticket.business.pdf;

import java.awt.Image;
import java.util.Random;

import javax.swing.ImageIcon;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.output.OutputException;

/**
 * This Class create a barcode for the Product
 * 
 * @author mjohnk
 * 
 */
public class BarcodeCreator {

	Image generateBarcodeImage(String productNumber, String fixedTicketNumber) {
		Random random = new Random(System.currentTimeMillis());
		try {
			StringBuffer barcodeNumber = new StringBuffer();
			barcodeNumber.append(fixedTicketNumber);
			while (productNumber.length() < 8){
				productNumber  = "0" + productNumber; 					 
			}
			barcodeNumber.append(productNumber);
			while (barcodeNumber.length() < 24){
				barcodeNumber.append(random.nextInt(9));
			}			
			Barcode barcode = BarcodeFactory.createInt2of5(barcodeNumber.toString());
			barcode.setDrawingText(false);
			ImageIcon image = new ImageIcon();
			image.setImage(BarcodeImageHandler.getImage(barcode));
			return image.getImage();
		} catch (OutputException e) {
			throw new IllegalArgumentException(e);
		} catch (BarcodeException e) {
			throw new IllegalArgumentException(e);
		}
	}

}
