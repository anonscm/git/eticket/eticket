package org.evolvis.eticket.model.crud.product;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Actor;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;

public interface ProductPoolCRUD extends GenericCUD<ProductPool> {
	/**
	 * Finds a pool by its product, status and actor.
	 * 
	 * @param product
	 * @param status
	 * @param actor
	 * @return
	 */
	public ProductPool findByProductStatusActor(Product product,
			ProductPoolStatus status, Actor actor);

	/**
	 * Find pool by its id.
	 * 
	 * @param id
	 * @return
	 */
	public ProductPool find(long id);
	
	
	/**
	 * Get a List of ProductPoolDTO 
	 * 
	 * @param status
	 * 			the status of the ProductPool can only be VOLATILE or FIXED
	 * @param username
	 * 			the Username of the account 
	 * @return the List of the ProductPoolDTO
	 */
	public List<ProductPool> findByStatusAndUsername(ProductPoolStatus status, String username);
	
	
	/**
	 * Gets a pool by its product, status and the username of the Actor
	 * 
	 * @param product
	 * 			the product
	 * @param status
	 * 			the status
	 * @param username
	 * 			the username
	 * @return the productPool
	 */
	public ProductPool findByProductStatusUsername(Product product,
			ProductPoolStatus status, String username);

	/**
	 * Gets pools by its product
	 * 
	 * @param product
	 * 			the product
	 * @return the productPools
	 */
	
	public List<ProductPool> findByproduct(Product product);

	public List<ProductPool> findByUsername(String username);
}



