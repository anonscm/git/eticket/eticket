package org.evolvis.eticket.business.pdf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.evolvis.eticket.model.entity.PdfField;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.FieldPosition;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;


/**
 * 
 * copy a PDF document and fills the TextFields and the ImageField with given
 * 
 * Values Author: Piers-Axel Scholle, p.scholle@tarent.de
 */

public class PdfCreator {

	private final AcroFields fields;
	private final PdfReader reader;
	private final PdfStamper stamper;
	private final ByteArrayOutputStream baos = new ByteArrayOutputStream();
	private final PdfContentByte content;
	
	protected PdfCreator(byte[] src) throws IOException, DocumentException {
		throwExceptionWhenSourceIsNull(src);
		reader = new PdfReader(src);
		stamper = new PdfStamper(reader, baos);
		content = stamper.getOverContent(1);
		fields = stamper.getAcroFields();
	}

	private void throwExceptionWhenSourceIsNull(byte[] src) {
		if (src == null)
			throw new IllegalArgumentException("Source must not be null");
	}

	private void close() throws DocumentException, IOException {
		reader.close();
		stamper.close();
	}

	public static byte[] generate(byte[] src, Map<PdfField, Object> values) {
		try {
			PdfCreator p = new PdfCreator(src);		
			return p.generate(values);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		} catch (DocumentException e) {
			throw new IllegalArgumentException(e);
		}
	}

	protected byte[] generate(Map<PdfField, Object> values) throws IOException, DocumentException {
		throwExceptionWhenValuesIsNull(values);
		setFields(values);
		close();
		return baos.toByteArray();
	}
	
	private void throwExceptionWhenValuesIsNull(Map<PdfField, Object> values) {
		if (values == null)
			throw new IllegalArgumentException("Values are null.");
	}

	private void setFields(Map<PdfField, Object> values) throws IOException,
			DocumentException {
		for (PdfField f : PdfField.values()) {
			setField(f, values.get(f));
		}
	}

	private void setField(PdfField field, Object value) throws IOException,
			DocumentException {
		if (isFieldATextField(field, value)){
			List<FieldPosition>  pos = fields.getFieldPositions(field.toString());
			setText(value.toString(), pos);			
		} else if (isFieldAImageField(field, value))
			addImage(field.toString(), (java.awt.Image) value);
		fields.removeField(field.toString());
	}
	
	private void setText(String value, List<FieldPosition> pos) throws DocumentException{
		ColumnText ct = new ColumnText( content );
		if(pos != null){
			Font helvetica = new Font(FontFamily.HELVETICA, 10);
			ct.setSimpleColumn(pos.get(0).position.getLeft(),pos.get(0).position.getBottom(),pos.get(0).position.getRight(),pos.get(0).position.getTop());
			ct.setText(new Phrase(value, helvetica));
			ct.go();
		}
	}

	private boolean isFieldAImageField(PdfField field, Object value) {
		return field.isImg() && value != null
				&& value instanceof java.awt.Image;
	}

	private boolean isFieldATextField(PdfField field, Object value) {
		return !field.isImg() && value != null && value instanceof String;
	}

	private void addImage(String fieldName, java.awt.Image image)
			throws IOException, DocumentException {
		Image img = Image.getInstance(image, null);
		scaleImageToFieldSize(fieldName, img);
		stamper.getOverContent(1).addImage(img);
	}

	private void scaleImageToFieldSize(String fieldName, Image img)
			throws IOException, DocumentException {
		List<FieldPosition> fieldPositions = fields
				.getFieldPositions(fieldName);
		Rectangle r = fieldPositions.get(0).position;
		img.scaleAbsolute(r.getWidth(), r.getHeight());
		img.setAbsolutePosition(r.getLeft(), r.getBottom());
	}
}
