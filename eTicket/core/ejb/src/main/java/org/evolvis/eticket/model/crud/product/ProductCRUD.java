package org.evolvis.eticket.model.crud.product;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;

public interface ProductCRUD extends GenericCUD<Product> {
    public Product findById(long id);
    public List<Product> listBySystemClass(ESystem system);

    public Product findBySystemAndUin(ESystem system, String uin);

	public List<Product> listBySystemClassAndMethod(ESystem system,
			Class<?> clazz, String method);
	public Product findProductbyId(long Id);
	
}
