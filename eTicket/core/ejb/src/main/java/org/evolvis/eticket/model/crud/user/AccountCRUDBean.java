package org.evolvis.eticket.model.crud.user;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;

import org.evolvis.eticket.interceptors.CreateAccountHistory;
import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.util.CalculateHash;
import org.evolvis.eticket.util.MessageConstClass;

@Stateless
@Local(AccountCRUD.class)
@Interceptors(CreateAccountHistory.class)
public class AccountCRUDBean extends GenericCUDSkeleton<Account> implements
		AccountCRUD {

	private static final long serialVersionUID = -6821446266083100427L;

	@Override
	@ExcludeClassInterceptors
	public Account findById(long id) {
		return find(Account.class, id);
	}

	@Override
	@ExcludeClassInterceptors
	public Account findBySystemAndUserName(ESystem system, String username) {
		return buildNamedQueryGetSingleResult("findAccountBySystemAndUsername",
				system, username);
	}

	@Override
	@ExcludeClassInterceptors
	public Account findBySystemAndOpenId(ESystem system, String openId) {
		return buildNamedQueryGetSingleResult("findAccountBysystemAndOpenid",
				system, openId);
	}

	@Override
	@ExcludeClassInterceptors
	public List<Account> listInactiveAccountsInSystem(ESystem system) {
		return buildNamedQueryGetResults("listInactiveAccountsInSystem", system);
	}

	@ExcludeClassInterceptors
	public List<Account> listAccountsInSystem(ESystem system) {
		return buildNamedQueryGetResults("listAccountsInSystem", system);
	}

	@Override
	@ExcludeClassInterceptors
	public Account checkPwGetAccount(ESystem system, AccountDTO account)
			throws IllegalAccessException {
		Account result;
		try {
			result = findBySystemAndUserName(system, account.getUsername());
		} catch (RuntimeException e) {
			// obfuscate the reason
			throw new IllegalAccessException(
					MessageConstClass.USER_UNKNOWN_OR_PASSWORD_INVALID);
		}
		if (CalculateHash.isPasswordCorrect(account, result.getCredential()
				.getSalt(), result.getCredential().getPasswordHash()))
			return result;
		else
			throw new IllegalAccessException(
					MessageConstClass.USER_UNKNOWN_OR_PASSWORD_INVALID);
	}

	@Override
	@ExcludeClassInterceptors
	public List<Account> findBySystemAndLikeUserName(ESystem system,
			String username) {
		// For getting results with the like Operator in the query, the string
		// needs to be surrounded by %...%
		// In Query setting off % is impossible ?
		String userquery = "%" + username + "%";
		return buildNamedQueryGetResults("searchLikeByNameAndSystem", userquery);
	}

	@Override
	@ExcludeClassInterceptors
	public List<Account> findBySystemAndLikeNameInPerosnalinformation(
			ESystem system, String username) {
		// For getting results with the like Operator in the query, the string
		// needs to be surrounded by %...%
		// In Query setting off % is impossible ?
		String userquery = "%" + username + "%";
		return buildNamedQueryGetResults(
				"searchLikeByNameAndSysteminPersonalInformation", userquery);
	}
}
