package org.evolvis.eticket.model.crud.platform;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.PlatformAccount;
import org.evolvis.eticket.model.entity.dto.AccountDTO;

public interface PlatformAccountCRUD extends GenericCUD<PlatformAccount> {
    public PlatformAccount findById(long id);
    public PlatformAccount findByUsername(String username);
    public List<PlatformAccount> listPlatformAccounts();
	public PlatformAccount checkPassword(AccountDTO platformAdmin) throws IllegalAccessException;

}
