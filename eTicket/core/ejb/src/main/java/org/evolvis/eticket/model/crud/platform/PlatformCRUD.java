package org.evolvis.eticket.model.crud.platform;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Platform;

/**
 * The Interface PlatformCRUDLocal.
 */
public interface PlatformCRUD extends GenericCUD<Platform> {
	

	/**
	 * Finds the Entity by id.
	 *
	 * @param id the id
	 * @return the platform
	 */
	public Platform findByName(String id);
}
