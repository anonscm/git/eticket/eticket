package org.evolvis.eticket.model.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class CredentialHistory {
	public enum Action {
		INSERT("insert"), ACTIVATE("activate"), DELETE("delete"), UPDATE("update");
		private String type;

		Action(String type) {
			this.type = type;

		}

		private static final Map<String, Action> stringToEnum = new HashMap<String, CredentialHistory.Action>();
		static {// initialise map from const name to enum const
			for (Action tP : values())
				stringToEnum.put(tP.toString(), tP);
		}

		public static Action fromString(String type) {
			return stringToEnum.get(type);
		}

		@Override
		public String toString() {
			return type;
		}
	}

	private long id;
	private String username;
	private Action action;
	private Date date;

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(nullable = false)
	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	@Column(nullable = false)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
