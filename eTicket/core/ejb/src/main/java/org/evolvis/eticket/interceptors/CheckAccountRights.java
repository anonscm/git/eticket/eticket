package org.evolvis.eticket.interceptors;

import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.evolvis.eticket.annotation.AllowedRole;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.system.RoleCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.Role;
import org.evolvis.eticket.model.entity.dto.AccountDTO;

public class CheckAccountRights {
	@EJB
	private RoleCRUD roleCRUDBeanLocal;

	public void setRoleCRUDBeanLocal(RoleCRUD roleCRUDBeanLocal) {
		this.roleCRUDBeanLocal = roleCRUDBeanLocal;
	}

	@EJB
	private AccountCRUD accountCRUDBeanLocal;

	public void setAccountCRUDBeanLocal(
			AccountCRUD accountCRUDBeanLocal) {
		this.accountCRUDBeanLocal = accountCRUDBeanLocal;
	}

	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;

	public void seteSystemCRUDBeanLocal(
			ESystemCRUD eSystemCRUDBeanLocal) {
		this.eSystemCRUDBeanLocal = eSystemCRUDBeanLocal;
	}

	/**
	 * Checks if a caller has the expected role, the calling methods must has
	 * either in the class or at the method itself the AllowedRole annotation
	 * with an expected role in it.
	 * 
	 * method annotation beats class annotation.
	 * 
	 * @param context
	 *            the context
	 * @return the object
	 * @throws Exception
	 *             the exception
	 */
	@AroundInvoke
	public Object checkCallerHasRole(InvocationContext context)
			throws Exception {
		AllowedRole rolesAllowed = context.getMethod().getAnnotation(
				AllowedRole.class);

		if (rolesAllowed == null)
			rolesAllowed = context.getTarget().getClass()
					.getAnnotation(AllowedRole.class);
		if (rolesAllowed != null) {
			Account account = getAccount(context.getParameters());
			checkRole(rolesAllowed, account, context.getMethod().getName());
		} else {
			throw new IllegalArgumentException("Illegal use of Interceptor "
					+ this.getClass().getSimpleName() + ".");
		}
		return context.proceed();
	}

	private Account getAccount(Object... object) throws IllegalAccessException {
		if (object != null && object[0] instanceof AccountDTO) {
			AccountDTO accDo = (AccountDTO) object[0];
			return accountCRUDBeanLocal.checkPwGetAccount(
					eSystemCRUDBeanLocal.findByName(accDo.getSystem().getName()),
					accDo);
		} else {
			throw new IllegalArgumentException("Illegal use of Interceptor "
					+ this.getClass().getSimpleName() + ".");
		}
	}

	private void checkRole(AllowedRole rolesAllowed, Account account,
			String methodName) throws IllegalAccessException {
		Role role = roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
				rolesAllowed.value().toString());
		if (!(account.getRoles().contains(role)))
			throw new IllegalAccessException("Caller "
					+ account.getCredential().getUsername()
					+ " is not allowed to call " + methodName);
	}

}
