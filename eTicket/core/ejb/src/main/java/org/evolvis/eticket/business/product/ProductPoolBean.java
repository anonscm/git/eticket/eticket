package org.evolvis.eticket.business.product;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.evolvis.eticket.annotation.AllowedRole;
import org.evolvis.eticket.interceptors.CheckAccountRights;
import org.evolvis.eticket.model.crud.product.ProductCRUD;
import org.evolvis.eticket.model.crud.product.ProductNumberCRUD;
import org.evolvis.eticket.model.crud.product.ProductPoolCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductNumber;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;

@Stateless
@Remote(ProductPoolBeanService.class)
public class ProductPoolBean implements ProductPoolBeanService{
	
	@EJB
	private ProductPoolCRUD productPoolCRUDLocal;
	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;
	@EJB
	private ProductCRUD productCRUDBeanLocal;
	@EJB
	private ProductNumberCRUD productNumberCRUDLocal;
	
	public void setProductPoolCRUDLocal(ProductPoolCRUD productPoolCRUDLocal) {
		this.productPoolCRUDLocal = productPoolCRUDLocal;
	}
	
	@Override
	public List<ProductPoolDTO> getProductPoolList(String status, String username){
		
		if(! (status.equals(ProductPoolStatus.VOLATILE.toString()) 
				|| (status.equals(ProductPoolStatus.FIXED.toString()))
				||(status.equals(ProductPoolStatus.REGISTERED.toString()))) ){
			throw new IllegalArgumentException
			("Incorrect status only VOLATILE, FIXED and REGISTERED are allowed.");
		}
		
		final List<ProductPool> productPools = productPoolCRUDLocal.findByStatusAndUsername
				(ProductPoolStatus.fromString(status), username);
		
		List<ProductPoolDTO> productPoolDTOs = new ArrayList<ProductPoolDTO>();
		for (ProductPool productPool : productPools) {
			productPoolDTOs.add(productPool.toDto());
		}
		return productPoolDTOs;
	}
	
	@Override
	public void fixProduct(AccountDTO accountDTO, ProductDTO productDTO){
		ESystem eSystem = eSystemCRUDBeanLocal.findByName(productDTO.getSystem().getName());
		Product product = productCRUDBeanLocal.findBySystemAndUin(eSystem, productDTO.getUin());
		ProductPool productPool = productPoolCRUDLocal.findByProductStatusUsername
				(product, ProductPoolStatus.VOLATILE, accountDTO.getUsername());
	
		//productCRUDBeanLocal.update(product);
		if(productPool.getAmount() == 1){
			productPool.setType(ProductPoolStatus.FIXED);
			productPool.setProductNumber(getProductNumber(product));
		}else{
			ProductPool pool = new ProductPool();
			productPool.setAmount(productPool.getAmount()-1);
			pool.setActor(productPool.getActor());
			pool.setAmount(1);
			pool.setProduct(productPool.getProduct());
			pool.setType(ProductPoolStatus.FIXED);
			pool.setProductNumber(getProductNumber(product));
			productPoolCRUDLocal.insert(pool);
		}
	}
	
	private long getProductNumber(Product product){
		ProductNumber productNumber = productNumberCRUDLocal.find(product);
		long nextNumber = productNumber.getCurrentNumber();
		productNumber.setCurrentNumber(productNumber.getCurrentNumber() + productNumber.getIncrement());
		productNumberCRUDLocal.update(productNumber);
		return nextNumber;
	}

	@Override
	public String getProductNumber(long productPoolId) {
		ProductPool productPool = productPoolCRUDLocal.find(productPoolId);
		return String.valueOf(productPool.getProductNumber());
	}	
	
	@Interceptors({ CheckAccountRights.class })
	@AllowedRole(AllowedRoles.SYSADMIN)
	public void fixProductasAdmin(AccountDTO admin,AccountDTO accountDTO, ProductDTO productDTO){
		ESystem eSystem = eSystemCRUDBeanLocal.findByName(productDTO.getSystem().getName());
		Product product = productCRUDBeanLocal.findBySystemAndUin(eSystem, productDTO.getUin());
		ProductPool productPool = productPoolCRUDLocal.findByProductStatusUsername
				(product, ProductPoolStatus.VOLATILE, accountDTO.getUsername());
		if(productPool.getAmount() == 1){
			productPool.setType(ProductPoolStatus.FIXED);
			productPool.setProductNumber(getProductNumber(product));
		}else{
			ProductPool pool = new ProductPool();
			productPool.setAmount(productPool.getAmount()-1);
			pool.setActor(productPool.getActor());
			pool.setAmount(1);
			pool.setProduct(productPool.getProduct());
			pool.setProductNumber(getProductNumber(product));
			pool.setType(ProductPoolStatus.FIXED);
			productPoolCRUDLocal.insert(pool);
		}
		
	}
	
	

}
