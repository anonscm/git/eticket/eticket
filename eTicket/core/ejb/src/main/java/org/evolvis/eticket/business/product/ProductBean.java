package org.evolvis.eticket.business.product;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;

import org.evolvis.eticket.annotation.AllowedRole;
import org.evolvis.eticket.interceptors.CheckAccountRights;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.product.ProductAutomaticTransferCRUD;
import org.evolvis.eticket.model.crud.product.ProductCRUD;
import org.evolvis.eticket.model.crud.product.ProductLocaleCRUD;
import org.evolvis.eticket.model.crud.product.TemplateCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductAutomaticTransfer;
import org.evolvis.eticket.model.entity.ProductLocale;
import org.evolvis.eticket.model.entity.Templates;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;

@Interceptors({ CheckAccountRights.class })
@AllowedRole(AllowedRoles.PRODUCTADMIN)
@Stateless
@Remote(ProductBeanService.class)
public class ProductBean implements ProductBeanService {
	@EJB
	private ProductCRUD productCRUDBeanLocal;
	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;
	@EJB
	private TemplateCRUD templateCRUDBeanLocal;
	@EJB
	private ProductLocaleCRUD productLocaleCRUDBeanLocal;
	@EJB
	private LocaleCRUD localeCRUDBeanLocal;
	@EJB
	private ProductAutomaticTransferCRUD productAutomaticTransferCRUDBeanlocal;


	@ExcludeClassInterceptors
	public List<ProductDTO> listBySystemClass(AccountDTO account,
			ESystemDTO systemDTO) {
		final ESystem system = eSystemCRUDBeanLocal.findByName(account
				.getSystem().getName());
		List<Product> produkte = productCRUDBeanLocal.listBySystemClass(system);
		List<ProductDTO> produkteDTO = new ArrayList<ProductDTO>();
		produkte.get(0).getLocales();
		for (int i = 0; i < produkte.size(); i++) {
			produkteDTO.add(produkte.get(i).toDto());
		}

		return produkteDTO;
	}

	public void setLocaleCRUDBeanLocal(LocaleCRUD localeCRUDBeanLocal) {
		this.localeCRUDBeanLocal = localeCRUDBeanLocal;
	}

	public void setProductLocaleCRUDBeanLocal(
			ProductLocaleCRUD productLocaleCRUDBeanLocal) {
		this.productLocaleCRUDBeanLocal = productLocaleCRUDBeanLocal;
	}

	public void setTemplateCRUDBeanLocal(TemplateCRUD templateCRUDBeanLocal) {
		this.templateCRUDBeanLocal = templateCRUDBeanLocal;
	}

	public void setProductCRUDBeanLocal(ProductCRUD productCRUDBeanLocal) {
		this.productCRUDBeanLocal = productCRUDBeanLocal;
	}

	public void setESystemCRUDBeanLocal(ESystemCRUD eSystemCRUDBeanLocal) {
		this.eSystemCRUDBeanLocal = eSystemCRUDBeanLocal;

	}

	// Daniel to-do These Method is replaced by createPWhashed, do we need this
	// method longer ?
	@Override
	public void create(AccountDTO productAdmin, ProductDTO productDTO) {
		Product product = generateProduct(productDTO);
		productCRUDBeanLocal.insert(product);
	}

	public void createProductlocale(AccountDTO padmin, ProductLocaleDTO plocale) {
		ProductLocale productLocale = new ProductLocale();
		productLocale.fromDto(plocale);
		productLocale.setLocale(localeCRUDBeanLocal.findById(plocale
				.getLocale().getId()));
		productLocale.setTemplate(templateCRUDBeanLocal.findById(plocale
				.getTemplate().getId()));
		productLocaleCRUDBeanLocal.insert(productLocale);
	}

	private Product generateProduct(ProductDTO productDTO) {
		ESystem eSystem = getSystem(productDTO);
		Product product = new Product(productDTO);
		product.setSystem(eSystem);
		product.setLocales(getProductLocalesFromDtos(productDTO));

		return product;
	}

	//
	// private ProductLocale getProductLocale(ProductDTO productDTO){
	// for (ProductLocaleDTO pDto : productDTO.getProductLocales()) {
	// productLocaleCRUDBeanLocal.findByLocaleCodeTemplateUin(pDto.getLocale().getCode(),
	// pDto.getTemplate().get)
	// }
	// }

	private ESystem getSystem(ProductDTO productDTO) {
		ESystem eSystem = eSystemCRUDBeanLocal.findByName(productDTO
				.getSystem().getName());
		return eSystem;
	}

	private List<ProductLocale> getProductLocalesFromDtos(ProductDTO productDTO) {
		List<ProductLocale> locales = new ArrayList<ProductLocale>();
		for (ProductLocaleDTO pDto : productDTO.getProductLocales()) {
			locales.add(createOrUpdProductLocale(pDto));
		}
		return locales;
	}

	@Override
	public ProductLocaleDTO putProductLocale(AccountDTO productAdmin,
			ProductLocaleDTO pDto) {
		return createOrUpdProductLocale(pDto).toDto();
	}

	@Override
	@ExcludeClassInterceptors
	public ProductLocaleDTO getProductLocale(String localecode,
			String templateUin) {
		return productLocaleCRUDBeanLocal.findByLocaleCodeTemplateUin(
				localecode, templateUin).toDto();
	}

	@Override
	@ExcludeClassInterceptors
	public List<ProductLocaleDTO> getAllProductLocales(AccountDTO account) {
		List<ProductLocale> locales = productLocaleCRUDBeanLocal
				.findallProductLocales();
		List<ProductLocaleDTO> localesDTO = new ArrayList<ProductLocaleDTO>();
		for (ProductLocale locale : locales) {
			localesDTO.add(locale.toDto());
		}
		return localesDTO;
	}

	@Override
	@ExcludeClassInterceptors
	public ProductLocaleDTO getProductLocale(String localecode, String uin,
			long systemId) {
		return productLocaleCRUDBeanLocal
				.findProductLocaleByLocalecodeAndProductUinAndSystemId(
						localecode, uin, systemId).toDto();
	}

	@Override
	public TemplatesDTO putTemplate(AccountDTO productAdmin,
			TemplatesDTO templatesDTO) {
		Templates templates = new Templates(templatesDTO);
		templateCRUDBeanLocal.insert(templates);
		return templatesDTO;
	}

	@Override
	@ExcludeClassInterceptors
	public TemplatesDTO getTemplate(String uin) {
		return templateCRUDBeanLocal.findByUin(uin).toDto();
	}

	@Override
	public ProductLocaleDTO getProductLocalebyId(AccountDTO admin, long Id) {
		ProductLocale locale = productLocaleCRUDBeanLocal.findById(Id);
		return locale.toDto();
	}

	@Override
	public List<TemplatesDTO> getAllTemplates(AccountDTO productAdmin) {
		return templateCRUDBeanLocal.getAllTemplatesDTOs();
	}

	private ProductLocale createOrUpdProductLocale(ProductLocaleDTO pDto) {
		// String localecode = pDto.getLocale().getCode();
		// String templateUin = (pDto.getTemplate() != null ? pDto.getTemplate()
		// .getUin() : null);
		ProductLocale productLocale = productLocaleCRUDBeanLocal.findById(pDto
				.getId());
		productLocale = createProductLocaleIfNullOrUpdate(pDto, productLocale);
		return productLocale;
	}

	private ProductLocale createProductLocaleIfNullOrUpdate(
			ProductLocaleDTO pDto, ProductLocale productLocale) {
		if (productLocale == null)
			productLocale = createProductLocale(pDto);
		else
			productLocale = mayUpdateProductLocale(pDto, productLocale);
		return productLocale;
	}

	private ProductLocale mayUpdateProductLocale(ProductLocaleDTO pDto,
			ProductLocale productLocale) {
		if (!productLocale.toDto().equals(pDto))
			updateProductLocale(pDto, productLocale);
		return productLocale;
	}

	private void updateProductLocale(ProductLocaleDTO pDto,
			ProductLocale productLocale) {
		productLocale.fromDto(pDto);
		setTemplate(pDto, productLocale);
		productLocaleCRUDBeanLocal.update(productLocale);
	}

	private ProductLocale createProductLocale(ProductLocaleDTO pDto) {
		ProductLocale productLocale = generateProductLocale(pDto);
		productLocaleCRUDBeanLocal.insert(productLocale);
		return productLocale;
	}

	private ProductLocale generateProductLocale(ProductLocaleDTO pDto) {
		ProductLocale productLocale;
		productLocale = new ProductLocale(pDto);
		setTemplate(pDto, productLocale);
		productLocale.setLocale(localeCRUDBeanLocal.findByCode(pDto.getLocale()
				.getCode()));
		return productLocale;
	}

	private void setTemplate(ProductLocaleDTO pDto, ProductLocale productLocale) {
		Templates templates = null;
		if (pDto.getTemplate() != null) {
			templates = createOrUpdTemplate(pDto.getTemplate());
		}
		productLocale.setTemplate(templates);
	}

	private Templates createOrUpdTemplate(TemplatesDTO tempDto) {
		Templates templates = templateCRUDBeanLocal.findByUin(tempDto.getUin());
		if (templates != null) {
			templates = mayUpdate(tempDto, templates);
		} else {
			templates = insertTemplate(tempDto);
		}
		return templates;
	}

	private Templates insertTemplate(TemplatesDTO templateDto) {
		Templates templates = new Templates(templateDto);
		templateCRUDBeanLocal.insert(templates);
		return templates;
	}

	private Templates mayUpdate(TemplatesDTO templateDto, Templates templates) {
		if (!templates.toDto().equals(templateDto)) {
			templates.fromDto(templateDto);
			templateCRUDBeanLocal.update(templates);
		}
		return templates;
	}

	
	@Override
	public void updateProductlocale(AccountDTO admoin, ProductLocaleDTO plocale){
		ProductLocale productlocale =productLocaleCRUDBeanLocal.findById(plocale.getId());
		productlocale.fromDTOupdate(plocale);
		productLocaleCRUDBeanLocal.update(productlocale);
		
	}
	
	@Override
	public void update(AccountDTO productAdmin, ProductDTO productDTO) {
		Product product = getProduct(productDTO);
		//product.fromDtoWithoutLocales(productDTO);
		product.setLocales(getProductLocalesFromDtos(productDTO));
		product.fromDto(productDTO);
		productCRUDBeanLocal.update(product);
	}

	@Override
	public void updatebyID(AccountDTO productAdmin, ProductDTO productDTO) {
		Product product = getProductbyId(productDTO);
		product.setLocales(getProductLocalesFromDtos(productDTO));
		product.fromDto(productDTO);
		product.setUin(productDTO.getUin());
		product.setCurrAmount(productDTO.getCurrAmount());
		productCRUDBeanLocal.update(product);
	}

	@Override
	public void delete(AccountDTO productAdmin, ProductDTO productDTO) {
		Product product = getProduct(productDTO);
		productCRUDBeanLocal.delete(product);
	}

	@Override
	public void transferProductByMethodcall(AccountDTO productAdmin,
			ProductDTO productDTO, TransferMethod method, long amount) {
		Product product = getProduct(productDTO);
		
		
		ProductAutomaticTransfer pa= product.getPat();   
		if (method != null) {
			product.setPat(new ProductAutomaticTransfer(method, amount));
		} else {
			product.setPat(null);
		}
		productCRUDBeanLocal.update(product);
		if (pa!=null){
	    	productAutomaticTransferCRUDBeanlocal.delete(pa);	    	
	    }
	}

	@Override
	public ProductDTO get(AccountDTO admin, long id) {
		try {
			return productCRUDBeanLocal.findById(id).toDto();
		} catch (Exception e) {
			return null;
		}
	}

	private Product getProductbyId(ProductDTO productDTO) {
		Product product = productCRUDBeanLocal.findById(productDTO.getId());
		return product;
	}

	private Product getProduct(ProductDTO productDTO) {
		ESystem system = getSystem(productDTO);
		Product product = productCRUDBeanLocal.findBySystemAndUin(system,
				productDTO.getUin());
		return product;
	}

	@ExcludeClassInterceptors
	@Override
	public ProductDTO get(String systemName, String productUin) {
		ESystem system = eSystemCRUDBeanLocal.findByName(systemName);
		Product product = productCRUDBeanLocal.findBySystemAndUin(system,
				productUin);
		return product.toDto();
	}

	@Override
	@AllowedRole(AllowedRoles.PRODUCTADMIN)
	public void createPWhashed(AccountDTO productAdmin, ProductDTO productDTO) {
		Product product = generateProduct(productDTO);
		productCRUDBeanLocal.insert(product);

	}

	public ProductAutomaticTransferCRUD getProductAutomaticTransferCRUDBeanlocal() {
		return productAutomaticTransferCRUDBeanlocal;
	}

	public void setProductAutomaticTransferCRUDBeanlocal(
			ProductAutomaticTransferCRUD productAutomaticTransferCRUDBeanlocal) {
		this.productAutomaticTransferCRUDBeanlocal = productAutomaticTransferCRUDBeanlocal;
	}







}
