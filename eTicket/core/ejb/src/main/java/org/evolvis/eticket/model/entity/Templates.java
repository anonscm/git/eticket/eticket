package org.evolvis.eticket.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.evolvis.eticket.model.entity.dto.TemplatesDTO;

/**
 * Contains templates for a eTicketSystem.
 * 
 */
@Entity

@NamedQueries({ 
	@NamedQuery(name = "getAllTemplates", query = "SELECT t FROM Templates t "),
	@NamedQuery(name = "findTemplateByUin", query = "SELECT t FROM Templates t WHERE t.uin = ?1") })
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "uin" }))
public class Templates implements Serializable {

	private static final long serialVersionUID = 907249315193096670L;

	private long id;
	private byte[] binaryData;

	private String mimeType;
	private String description;
	private String uin;

	public Templates() {
	}

	public Templates(TemplatesDTO template) {
		if (template != null) {
			fromDto(template);
		}
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(nullable = false)
	public byte[] getBinaryData() {
		return binaryData;
	}

	public void setBinaryData(byte[] binaryData) {
		this.binaryData = binaryData;
	}

	@Column(nullable = false)
	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setUin(String uin) {
		this.uin = uin;
	}

	@Column(nullable = false)
	public String getUin() {
		return uin;
	}
	
	@Transient
	public void fromDto(TemplatesDTO templateDTO) {
		this.binaryData = templateDTO.getBinaryData();
		this.uin = templateDTO.getUin();
		this.description = templateDTO.getDescription();
		this.mimeType = templateDTO.getMimeType();
		
		if (templateDTO.getId() > -1) {
			this.id = templateDTO.getId();
		}
	}

	@Transient
	public TemplatesDTO toDto() {
		return TemplatesDTO.createTemplatesDTOFromExsistingTemplatesEntity(id, uin, binaryData, mimeType, description);
	}	

}
