package org.evolvis.eticket.business.product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;

import org.evolvis.eticket.annotation.AllowedRole;
import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.communication.CommunicateUtil;
import org.evolvis.eticket.interceptors.CheckAccountRights;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductLocale;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;
import org.evolvis.eticket.model.entity.TransferHistory;
import org.evolvis.eticket.model.entity.TransferHistory.TransferState;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO.TextTemplateNames;
import org.evolvis.eticket.model.entity.dto.TransferHistoryDTO;

@Stateless
@Remote(ProductTransferService.class)
@Interceptors({ CheckAccountRights.class })
@AllowedRole(AllowedRoles.USER)
// TODO refactor
public class ProductTransferBean extends ProductTransferSkeleton implements
		ProductTransferService {

	private AccountBeanService accountBeanService;

	@EJB
	public void setAccountBeanService(AccountBeanService accountBeanService) {
		this.accountBeanService = accountBeanService;
	}

	@Override
	@AllowedRole(AllowedRoles.PRODUCTADMIN)
	public void transferSystemProductToAccount(AccountDTO sender,
			ProductDTO product, AccountDTO receiver, long amount) {
		ESystem system = systemCRUD.findByName(sender.getSystem().getName());
		Account rec = accountCRUD.findBySystemAndUserName(system,
				receiver.getUsername());
		Product prod = productCRUD.findBySystemAndUin(system, product.getUin());
		ProductPool pool = transferProductFromSystemToReceiver(amount, system,
				rec, prod);
		sendRecMail(amount, system, null, rec, prod);
		insertHistoryEvent(pool, null, rec, amount, TransferState.OPEN);
	}
	
	@Override
	@ExcludeClassInterceptors
	public void transferOrderedSystemProductToAccountAndAccept(ProductDTO product, AccountDTO receiver, long amount) {
		ESystem system = systemCRUD.findByName(receiver.getSystem().getName());
		Account rec = accountCRUD.findBySystemAndUserName(system,
				receiver.getUsername());
		Product prod = productCRUD.findBySystemAndUin(system, product.getUin());
		ProductPool pool = transferProductFromSystemToReceiver(amount, system,
				rec, prod);
		sendRecMail(amount, system, null, rec, prod);
		long transferhistoryId = insertHistoryEvent(pool, null, rec, amount, TransferState.OPEN);
		accept(receiver, transferhistoryId);
	}

	@Override
	public void transferProductToAccount(AccountDTO sender, ProductDTO product,
			AccountDTO receiver, long amount) {
		ESystem system = systemCRUD.findByName(sender.getSystem().getName());
		Account owner = accountCRUD.findBySystemAndUserName(system,
				sender.getUsername());
		Product prod = productCRUD.findBySystemAndUin(system, product.getUin());
		ProductPool sendersPool = getAndCheckSenderPool(owner, prod, amount);
		Account rec = getOrCreateAccount(receiver, system, prod);
		ProductPool recPool = poolCRUD.findByProductStatusActor(prod,
				ProductPoolStatus.REGISTERED, rec);
		recPool = putPool(amount, rec, prod, recPool,
				ProductPoolStatus.REGISTERED);
		putPool(amount * -1, owner, prod, sendersPool,
				ProductPoolStatus.VOLATILE);
		sendRecMail(amount, system, owner, rec, prod);
		sendSentMail(amount, system, owner, rec, prod);
		insertHistoryEvent(recPool, owner, rec, amount, TransferState.OPEN);
	}

	private Account getOrCreateAccount(AccountDTO receiver, ESystem system,
			Product prod) {
		try {
			return accountCRUD.findBySystemAndUserName(system,
					receiver.getUsername());
		} catch (EJBException e) {
			accountBeanService.createUser(receiver, prod.getSystem()
					.getDefaultLocale().getLocalecode());
			return accountCRUD.findBySystemAndUserName(system,
					receiver.getUsername());
		}
	}

	@Override
	public void accept(AccountDTO sender, long id) {
		TransferHistory transferHistory = transferHistoryCRUD.findById(id);
		if (transferHistory == null)
			throw new IllegalArgumentException("Unable to find a pool with id:"
					+ id);
		ProductPool registerPool = transferHistory.getPool();
		checkIfSenderIsOwnerOfPool(sender, id, registerPool);
		ProductPool volatilePool = poolCRUD.findByProductStatusActor(
				registerPool.getProduct(), ProductPoolStatus.VOLATILE,
				registerPool.getActor());
		volatilePool = putPool(transferHistory.getAmount(),
				(Account) registerPool.getActor(), registerPool.getProduct(),
				volatilePool, ProductPoolStatus.VOLATILE);

		updateHistoryEvent(transferHistory, volatilePool,
				TransferState.ACCEPTED);

		if (transferHistory.getAmount() == registerPool.getAmount()) {
			poolCRUD.delete(registerPool);
		} else {
			registerPool.setAmount(registerPool.getAmount()
					- transferHistory.getAmount());
			poolCRUD.update(registerPool);
		}

	}

	@Override
	public void decline(AccountDTO sender, long id) {
		TransferHistory transferHistory = transferHistoryCRUD.findById(id);
		ProductPool registerPool = transferHistory.getPool();
		checkIfSenderIsOwnerOfPool(sender, id, registerPool);
		updateHistoryEvent(transferHistory, null, TransferState.DECLINED);
		bookBackAndDeleteRegisterPool(registerPool, transferHistory);
		
		String localecode = null;
		if(transferHistory.getRecipient().getPersonalInformation() != null){
			localecode = transferHistory.getRecipient().getPersonalInformation().getLocale().getLocalecode();
		}else{
			localecode = transferHistory.getProduct().getSystem().getDefaultLocale().getLocalecode();
		}

		if (transferHistory.getSender() != null) {
			String productname ="";
			for (ProductLocale locale : transferHistory.getProduct().getLocales()) {
				if(locale.getLocale().getLocalecode().equals(localecode)){
					productname = locale.getProductName();
					break;
				}					
			}
			Map<String, String> buzzwords = CommunicateUtil.getBuzzwords(TextTemplateNames.DECLINE_PRODUCT, sender.getUsername(), productname);
			sendStatusMail(transferHistory.getRecipient(),
					transferHistory.getSender(), transferHistory,
					TextTemplateNames.DECLINE_PRODUCT, buzzwords);
		}
	}

	@AllowedRole(AllowedRoles.PRODUCTADMIN)
	public void declinebyadmin(AccountDTO admin,AccountDTO sender, long id) {
		TransferHistory transferHistory = transferHistoryCRUD.findById(id);
		ProductPool registerPool = transferHistory.getPool();
		checkIfSenderIsOwnerOfPool(sender, id, registerPool);
		Product product =productCRUD.findById(transferHistory.getProduct().getId());
		int dif = (int) transferHistory.getAmount();
		product.setCurrAmount(product.getCurrAmount()-dif);
		updateHistoryEvent(transferHistory, null, TransferState.TAKENBYADMIN);
		if (transferHistory.getAmount() == registerPool.getAmount()) {
			poolCRUD.delete(registerPool);
		} else {
			registerPool.setAmount(registerPool.getAmount()
					- transferHistory.getAmount());
			poolCRUD.update(registerPool);
		}
		productCRUD.update(product);
	}	

	@SuppressWarnings("static-access")
	@AllowedRole(AllowedRoles.PRODUCTADMIN)
	public void deleteopentickets(AccountDTO admin, ProductPoolDTO poolDTO,int amount){
		ProductPool pool = poolCRUD.find(poolDTO.getId());
		Product product =productCRUD.findById(pool.getProduct().getId());
		product.setCurrAmount(product.getCurrAmount()-amount);
		if (amount==Integer.parseInt(poolDTO.getAmount())){
		    List <TransferHistory> histories=transferHistoryCRUD.findallByPool(pool);
		    for (TransferHistory history:histories){
			  updateHistoryEvent(history,null,history.getState().TAKENBYADMIN);
		    }
		    poolCRUD.delete(pool);	
		}
		else if(amount<Integer.parseInt(poolDTO.getAmount())){
			pool.setAmount(amount);
			poolCRUD.update(pool);
		}
		productCRUD.update(product);
	}
	
	
	@Override
	public void cancel(AccountDTO sender, long id) {
		TransferHistory transferHistory = transferHistoryCRUD.findById(id);
		ProductPool registerPool = transferHistory.getPool();

		updateHistoryEventAndPool(registerPool, transferHistory);
		if(!transferHistory.getSender().getCredential().getUsername()
				.equals(sender.getUsername()))
			throw new IllegalArgumentException(sender.getUsername()
					+ " is not allowed to abort transfer " + id);
		String productname ="";
		String localecode = null;
		if(transferHistory.getRecipient().getPersonalInformation() != null){
			localecode = transferHistory.getRecipient().getPersonalInformation().getLocale().getLocalecode();
		}else{
			localecode = transferHistory.getProduct().getSystem().getDefaultLocale().getLocalecode();
		}
		
		for (ProductLocale locale : transferHistory.getProduct().getLocales()) {
			if(locale.getLocale().getLocalecode().equals(localecode)){
				productname = locale.getProductName();
				break;
			}			
		}
		Map<String, String> buzzwords = CommunicateUtil.getBuzzwords(TextTemplateNames.DECLINE_PRODUCT, sender.getUsername(), productname);
		sendStatusMail(transferHistory.getSender(),
				transferHistory.getRecipient(), transferHistory,
				TextTemplateNames.CANCELED_PRODUCT, buzzwords);
	}

	private void updateHistoryEventAndPool(ProductPool registerPool,
			TransferHistory transferHistory) {
		updateHistoryEvent(transferHistory, null, TransferState.CANCELED);
		bookBackAndDeleteRegisterPool(registerPool, transferHistory);
	}

	@Override
	@ExcludeClassInterceptors
	public List<TransferHistoryDTO> findTransferActivitiesSender(
			String senderUsername, String state) {

		if (!(state.equals(TransferState.OPEN.toString())
				|| state.equals(TransferState.CANCELED.toString())
				|| state.equals(TransferState.TAKENBYADMIN.toString())
				|| state.equals(TransferState.DECLINED.toString()) || state
					.equals(TransferState.ACCEPTED.toString()))) {
			throw new IllegalArgumentException(
					"Incorrect state only OPEN, CANCELED, DECLINED are allowed.");
		}

		List<TransferHistory> history = transferHistoryCRUD
				.findBySenderAndTransferState(senderUsername,
						TransferState.fromString(state));
		List<TransferHistoryDTO> transferHistoryDTOs = new ArrayList<TransferHistoryDTO>();
		for (TransferHistory transferHistory : history) {
			transferHistoryDTOs.add(transferHistory.toDTO());
		}
		return transferHistoryDTOs;
	}

	@Override
	@ExcludeClassInterceptors
	public List<TransferHistoryDTO> findTransferActivitiesRecipient(
			String recipientUsername, String state) {
		if (!(state.equals(TransferState.OPEN.toString())
				|| state.equals(TransferState.TAKENBYADMIN.toString())
				|| state.equals(TransferState.CANCELED.toString())
				|| state.equals(TransferState.DECLINED.toString()) || state
					.equals(TransferState.ACCEPTED.toString()))) {
			throw new IllegalArgumentException(
					"Incorrect state only OPEN, CANCELED, DECLINED are allowed.");
		}

		List<TransferHistory> history = transferHistoryCRUD
				.findByRecipientUsernameAndTransferState(recipientUsername,
						TransferState.fromString(state));
		List<TransferHistoryDTO> transferHistoryDTOs = new ArrayList<TransferHistoryDTO>();
		for (TransferHistory transferHistory : history) {
			transferHistoryDTOs.add(transferHistory.toDTO());
		}

		return transferHistoryDTOs;
	}

}
