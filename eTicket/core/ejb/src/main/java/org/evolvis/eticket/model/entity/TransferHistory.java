package org.evolvis.eticket.model.entity;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;
import org.evolvis.eticket.model.entity.dto.TransferHistoryDTO;

@Entity
@NamedQueries({
	@NamedQuery(name = "findHistoryByPool", query = "SELECT t FROM TransferHistory t WHERE t.pool = ?1"),
	@NamedQuery(name = "findHistoryBySenderUsernameAndTransferState", query = "SELECT t FROM TransferHistory t " +
			"WHERE t.sender.credential.username = ?1 AND t.state = ?2" ),
	@NamedQuery(name = "findHistoryByRecipientUsernameAndTransferState", query = "SELECT t FROM TransferHistory t " +
			"WHERE t.recipient.credential.username = ?1 AND t.state = ?2" ),
	@NamedQuery(name = "findHistoryByPoolRecipientAndTransferState", query = "SELECT t FROM TransferHistory t " +
			"WHERE t.pool = ?1 AND t.recipient = ?2 AND t.state = ?3" )		
})
public class TransferHistory {
	public enum TransferState {
		OPEN("OPEN"), ACCEPTED("ACCEPTED"), DECLINED("DECLINED"), CANCELED("CANCELED"),TAKENBYADMIN("TAKENBYADMIN");
		private String type;

		private static Map<String, TransferState> enumToString = new HashMap<String, TransferHistory.TransferState>();
		static {
			for (TransferState pps : values())
				enumToString.put(pps.toString(), pps);
		}

		TransferState(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return type;
		}

		public static TransferState fromString(String type) {
			return enumToString.get(type);
		}
	}

	@Id
	@GeneratedValue
	private long id;
	@ManyToOne
	private Product product;
	private long amount;
	private TransferState state;
	@ManyToOne
	private Account sender;
	@ManyToOne
	private Account recipient;
	@ManyToOne
	private ProductPool pool;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public Account getSender() {
		return sender;
	}

	public void setSender(Account sender) {
		this.sender = sender;
	}

	public Account getRecipient() {
		return recipient;
	}

	public void setRecipient(Account recipient) {
		this.recipient = recipient;
	}

	public void setState(TransferState state) {
		this.state = state;
	}

	public TransferState getState() {
		return state;
	}

	public void setPool(ProductPool pool) {
		this.pool = pool;
	}

	public ProductPool getPool() {
		return pool;
	}
	
	@Transient
	public TransferHistoryDTO toDTO(){
		ProductDTO productDTO = product.toDto();
		AccountDTO recipientDTO = recipient.toAccountDTO();
		AccountDTO senderDTO = null;
		if(sender != null)
			senderDTO = sender.toAccountDTO();
		ProductPoolDTO productPoolDTO = null;
		if(pool != null)
			productPoolDTO = pool.toDto();
		return TransferHistoryDTO.createTransferHistoryDTOFromExsistingTransferHistoryEntity(id, productDTO, recipientDTO, amount, productPoolDTO, senderDTO);
	}

}
