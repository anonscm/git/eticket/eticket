package org.evolvis.eticket.business.system;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;

import org.evolvis.eticket.annotation.AllowedRole;
import org.evolvis.eticket.interceptors.CheckAccountRights;
import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.ConfigurationParameter;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;

@Stateless
@Remote(ConfigurationParameterBeanService.class)
@Interceptors({ CheckAccountRights.class })
@AllowedRole(AllowedRoles.SYSADMIN)
public class ConfigurationParameterBean implements
		ConfigurationParameterBeanService {

	private static final long serialVersionUID = -2130104517469222290L;

	@EJB
	private ConfigurationParameterCRUD configurationParameterCRUDBeanLocal;

	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;

	public void setConfigurationParameterCRUDBeanLocal(
			ConfigurationParameterCRUD configurationParameterCRUDBeanLocal) {
		this.configurationParameterCRUDBeanLocal = configurationParameterCRUDBeanLocal;
	}

	public void seteSystemCRUDBeanLocal(
			ESystemCRUD eSystemCRUDBeanLocal) {
		this.eSystemCRUDBeanLocal = eSystemCRUDBeanLocal;
	}

	@Override
	public void insertCP(AccountDTO caller, ESystemDTO system, CPKey key,
			String value) {
		ConfigurationParameter cp = new ConfigurationParameter();
		cp.setKey(key.toString());
		cp.setValue(value);
		cp.setSystem(getSystem(system));
		configurationParameterCRUDBeanLocal.insert(cp);
	}

	private ESystem getSystem(ESystemDTO system) {
		return eSystemCRUDBeanLocal.findByName(system.getName());
	}

	@Override
	public void setValueOfCP(AccountDTO caller, ESystemDTO system, CPKey key,
			String value) {
		ConfigurationParameter cp = configurationParameterCRUDBeanLocal
				.findBySystemAndKey(getSystem(system), key.toString());
		cp.setValue(value);
		configurationParameterCRUDBeanLocal.update(cp);
	}

	@Override
	public void deleteCP(AccountDTO caller, ESystemDTO system, CPKey key) {
		ConfigurationParameter cp = configurationParameterCRUDBeanLocal
				.findBySystemAndKey(getSystem(system), key.toString());
		configurationParameterCRUDBeanLocal.delete(cp);
	}

	@Override
	@ExcludeClassInterceptors
	public String find(ESystemDTO system, String key) {
		return configurationParameterCRUDBeanLocal.findBySystemAndKey(
				getSystem(system), key).getValue();
	}

	@Override
	@ExcludeClassInterceptors
	public int findAsInt(ESystemDTO system, String key) {
		return configurationParameterCRUDBeanLocal.findBySystemAndKey(
				getSystem(system), key).toInt();
	}

	@Override
	@ExcludeClassInterceptors
	public long findAsLong(ESystemDTO system, String key) {
		return configurationParameterCRUDBeanLocal.findBySystemAndKey(
				getSystem(system), key).toLong();
	}
}
