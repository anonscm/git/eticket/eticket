package org.evolvis.eticket.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.evolvis.eticket.model.entity.dto.LocaleDTO;

@Entity
@NamedQuery(name = "findLocaleByCode", query = "SELECT l FROM Locale l WHERE l.localecode LIKE ?1")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "LOCALECODE" }))
public class Locale implements Serializable {
	
	private static final long serialVersionUID = 5463010777951625806L;
	private long id;
	private String localecode;
	private String name;

	public Locale() {

	}
	

	public Locale(String localeCode) {
		setLocalecode(localeCode);
	}

	public Locale(LocaleDTO localeDTO) {
		fromDto(localeDTO);
	}

	public void setId(long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	@Column(nullable = false)
	public String getLocalecode() {
		return localecode;
	}

	public void setLocalecode(String localecode) {
		this.localecode = localecode;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Locale))
			return false;
		Locale locale = (Locale) obj;
		return (locale.getLocalecode().equals(getLocalecode()) && (locale
				.getName() == null ? getName() == null : locale.getName()
				.equals(getName())));
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + getLocalecode().hashCode();
		result += 31 * result + (getName() == null ? 0 : getName().hashCode());
		return result;
	}

	@Transient
	public LocaleDTO toDto() {
		return LocaleDTO.createLocaleDTOFromExsistingLocaleEntity(id, getLocalecode(), getName());
	}
	
	@Transient
	public void fromDto(LocaleDTO localeDTO) {
	    this.localecode = localeDTO.getCode();
	    this.name = localeDTO.getName();
	    
		if(localeDTO.getId() > -1) {
			this.id = localeDTO.getId();
		}
	}

}
