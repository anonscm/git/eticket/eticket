package org.evolvis.eticket.business.product;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;

import org.evolvis.eticket.business.communication.CommunicateUtil;
import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.crud.product.ProductCRUD;
import org.evolvis.eticket.model.crud.product.ProductPoolCRUD;
import org.evolvis.eticket.model.crud.product.TransferHistoryCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.system.TextTemplateCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;
import org.evolvis.eticket.model.entity.TextTemplate;
import org.evolvis.eticket.model.entity.TransferHistory;
import org.evolvis.eticket.model.entity.TransferHistory.TransferState;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO.TextTemplateNames;

public abstract class ProductTransferSkeleton {
	protected ProductPoolCRUD poolCRUD;
	protected ESystemCRUD systemCRUD;
	protected AccountCRUD accountCRUD;
	protected ProductCRUD productCRUD;
	protected TransferHistoryCRUD transferHistoryCRUD;
	protected ConfigurationParameterCRUD configurationParameterBeanLocal;
	protected TextTemplateCRUD templateCRUD;

	@EJB
	public void setTemplateCRUD(TextTemplateCRUD templateCRUD) {
		this.templateCRUD = templateCRUD;
	}

	@EJB
	public void setConfigurationParameterBeanLocal(
			ConfigurationParameterCRUD configurationParameterBeanLocal) {
		this.configurationParameterBeanLocal = configurationParameterBeanLocal;
	}

	@EJB
	public void setTransferHistoryCRUD(TransferHistoryCRUD transferHistoryCRUD) {
		this.transferHistoryCRUD = transferHistoryCRUD;
	}

	@EJB
	public void setProductCRUD(ProductCRUD productCRUD) {
		this.productCRUD = productCRUD;
	}

	@EJB
	public void setAccountCRUD(AccountCRUD accountCRUD) {
		this.accountCRUD = accountCRUD;
	}

	@EJB
	public void setPoolCRUD(ProductPoolCRUD poolCRUD) {
		this.poolCRUD = poolCRUD;
	}

	@EJB
	public void setSystemCRUD(ESystemCRUD systemCRUD) {
		this.systemCRUD = systemCRUD;
	}

	protected void sendRecMail(long amount, ESystem system, Account sender,
			Account rec, Product prod) {
		
		Map<String, String> buzzwords = null;
		if(sender != null && sender.getPersonalInformation() != null && sender.getPersonalInformation().getFirstname() != null && sender.getPersonalInformation().getLastname() != null){
			buzzwords = CommunicateUtil.getBuzzwords(TextTemplateNames.RECEIVED_PRODUCT,
					sender.getPersonalInformation().getFirstname(), sender.getPersonalInformation().getLastname());
			sendTransferToMail(amount, system, sender, rec, prod,
					TextTemplateNames.RECEIVED_PRODUCT, buzzwords);
		}else{
			buzzwords = new HashMap<String, String>();
			sendTransferToMail(amount, system, sender, rec, prod,
					TextTemplateNames.RECEIVED_PRODUCT_BY_SYSTEM, buzzwords);
			
		}
		
	}

	protected void sendSentMail(long amount, ESystem system, Account sender,
			Account rec, Product prod) {
		Map<String, String> buzzwords = null;
		if(sender != null && sender.getPersonalInformation() != null && sender.getPersonalInformation().getFirstname() != null && sender.getPersonalInformation().getLastname() != null){
			buzzwords = CommunicateUtil.getBuzzwords(TextTemplateNames.SENT_PRODUCT,
					sender.getPersonalInformation().getFirstname(), sender.getPersonalInformation().getLastname(), rec.getCredential().getUsername());
		}else{
			buzzwords = CommunicateUtil.getBuzzwords(TextTemplateNames.SENT_PRODUCT,
					"", "", rec.getCredential().getUsername());
		}
		sendTransferToMail(amount, system, rec, sender, prod,
				TextTemplateNames.SENT_PRODUCT, buzzwords);
	}

	protected void sendTransferToMail(long amount, ESystem system,
			Account sender, Account rec, Product prod,
			TextTemplateNames tempName, Map<String, String> buzzwords) {
		TextTemplate template = null;
		if(rec != null && rec.getPersonalInformation() != null){
			template = templateCRUD.findBySystemLocaleAndName(system,
				rec.getPersonalInformation().getLocale(), tempName.toString());
		} else {
			template = templateCRUD.findBySystemLocaleAndName(system,
					system.getDefaultLocale(), tempName.toString());
		}
		
		CommunicateUtil.sendMail(configurationParameterBeanLocal, system, rec,
				template, buzzwords);
	}

	protected long insertHistoryEvent(ProductPool pool, Account sender,
			Account rec, long amount, TransferState state) {
		TransferHistory transferHistory = new TransferHistory();
		Product product = pool.getProduct();
		setHistory(pool, sender, rec, state, transferHistory, product, amount);
		transferHistoryCRUD.insert(transferHistory);
		return transferHistory.getId();
	}

	protected void setHistory(ProductPool pool, Account sender, Account rec,
			TransferState state, TransferHistory transferHistory,
			Product product, long amount) {
		transferHistory.setProduct(product);
		transferHistory.setSender(sender);
		transferHistory.setRecipient(rec);
		transferHistory.setAmount(amount);
		transferHistory.setState(state);
		transferHistory.setPool(pool);
	}

	protected ProductPool transferProductFromSystemToReceiver(long amount,
			ESystem system, Account rec, Product prod) {
		if (prod.getCurrAmount() < prod.getMaxAmount()) {
			ProductPool pool = poolCRUD.findByProductStatusActor(prod,
					ProductPoolStatus.REGISTERED, rec);
			pool = putPool(amount, rec, prod, pool,
					ProductPoolStatus.REGISTERED);
			setAmountOfProduct(amount, prod);
			return pool;
		} else {
			throw new IllegalArgumentException("Product " + prod.getUin()
					+ " has reached its maximal amount in system "
					+ system.getName() + ".");
		}
	}

	protected void setAmountOfProduct(long amount, Product prod) {
		prod.setCurrAmount((int) (prod.getCurrAmount() + amount));
		productCRUD.update(prod);
	}

	protected ProductPool putPool(long amount, Account rec, Product prod,
			ProductPool pool, ProductPoolStatus status) {

		pool = setPool(amount, rec, prod, pool, status);
		if (pool.getId() < 1)
			poolCRUD.insert(pool);
		else
			poolCRUD.update(pool);
		return pool;
	}

	/**
	 * sets the a pool to the given parameter.
	 * 
	 * @param amount
	 * @param rec
	 * @param prod
	 * @param pool
	 * @param status
	 * @return
	 */
	protected ProductPool setPool(long amount, Account rec, Product prod,
			ProductPool pool, ProductPoolStatus status) {
		if (pool == null) {
			pool = new ProductPool();
			pool.setActor(rec);
			pool.setType(status);
			pool.setProduct(prod);
		} else {
			amount += pool.getAmount();
		}
		pool.setAmount(amount);
		return pool;
	}

	protected void updateHistoryEvent(TransferHistory history,
			ProductPool volatilePool, TransferState state) {
		history.setState(state);
		history.setPool(volatilePool);
		transferHistoryCRUD.update(history);
	}

	protected void checkIfSenderIsOwnerOfPool(AccountDTO sender, long id,
			ProductPool registerPool) {
		if (!(registerPool.getActor().getCredential().getUsername()
				.equals(sender.getUsername())))
			throw new IllegalArgumentException(
					"Sender is not the owner of pool " + id);
	}

	protected ProductPool getPoolWithId(long id) {
		ProductPool pool = poolCRUD.find(id);
		if (pool == null)
			throw new IllegalArgumentException("Unable to find a pool with id:"
					+ id);
		return pool;
	}

	protected void sendStatusMail(Account sender, Account recipient,
			TransferHistory history, TextTemplateNames tempName, Map<String, String> buzzwords) {
		ESystem system = history.getProduct().getSystem();
		TextTemplate template;
		
		if(recipient.getPersonalInformation() != null ){
			template = templateCRUD.findBySystemLocaleAndName(system,
					recipient.getPersonalInformation().getLocale(), tempName.toString());
		}else {
			template = templateCRUD.findBySystemLocaleAndName(system,
					system.getDefaultLocale(), tempName.toString());
		}
		CommunicateUtil.sendMail(configurationParameterBeanLocal, system,
				recipient, template, buzzwords);
	}

	protected void bookBackAndDeleteRegisterPool(ProductPool registerPool,
			TransferHistory history) {
		ProductPool sendersPool = poolCRUD.findByProductStatusActor(
				registerPool.getProduct(), ProductPoolStatus.VOLATILE,
				history.getSender());

		if (history.getSender() != null) {
			putPool(history.getAmount(), history.getSender(),
					registerPool.getProduct(), sendersPool,
					ProductPoolStatus.VOLATILE);
		} else {
			Product product = productCRUD.findBySystemAndUin(registerPool.getProduct().getSystem(), registerPool.getProduct().getUin());
			product.setCurrAmount(Integer.parseInt((product.getCurrAmount() - history.getAmount())+""));
		}

		if (history.getAmount() == registerPool.getAmount()) {
			poolCRUD.delete(registerPool);
			Product product =productCRUD.findById(history.getProduct().getId());
			int dif = (int) history.getAmount();
			product.setCurrAmount(product.getCurrAmount()-dif);
			productCRUD.update(product);
		} else {
			registerPool.setAmount(registerPool.getAmount()
					- history.getAmount());
			poolCRUD.update(registerPool);
		}
	}

	protected ProductPool getAndCheckSenderPool(Account owner, Product prod,
			long amount) {
		ProductPool sendersPool = poolCRUD.findByProductStatusActor(prod,
				ProductPoolStatus.VOLATILE, owner);
		checkIfPoolIsNull(owner, prod, sendersPool);
		checkAmountOfPool(owner, prod, amount, sendersPool);
		return sendersPool;
	}

	protected void checkAmountOfPool(Account owner, Product prod, long amount,
			ProductPool sendersPool) {
		if (amount < 0) {
			throw new IllegalArgumentException(
					"It's not allowed to send negative amounts of a product.");
		} else if (sendersPool.getAmount() < amount)
			throw new IllegalArgumentException(owner.getCredential()
					.getUsername()
					+ " has not enough amounts of "
					+ prod.getUin() + " to send the amount " + amount + ".");
	}

	protected void checkIfPoolIsNull(Account owner, Product prod,
			ProductPool sendersPool) {
		if (sendersPool == null)
			throw new IllegalArgumentException(owner.getCredential()
					.getUsername()
					+ " has no valid pool for product "
					+ prod.getUin() + " to send from.");
	}
}
