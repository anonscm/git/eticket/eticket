package org.evolvis.eticket.model.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "token", "account_id" }))
@NamedQueries({
		@NamedQuery(name = "findActivateAccountTokenByAccountAndToken", query = "SELECT a FROM ActivateAccountToken a WHERE a.account.id = ?1 AND a.token = ?2"),
		@NamedQuery(name = "findActivateAccountTokenByAccount", query = "SELECT a FROM ActivateAccountToken a WHERE a.account.id = ?1"),
		@NamedQuery(name = "listOutdatedActivateAccountTokenInSystem", query = "SELECT a FROM ActivateAccountToken a WHERE a.account.system = ?1 AND a.validUntil < ?2"),
		@NamedQuery(name = "deleteOutdatedActivateAccountTokenInSystem", query = "DELETE FROM ActivateAccountToken a WHERE a.account.system = ?1 AND a.validUntil < ?2")})
public class ActivateAccountToken extends Token {
	
	private Account account;

	public void setAccount(Account account) {
		this.account = account;
	}

	@ManyToOne(optional = false)
	public Account getAccount() {
		return account;
	}	
}
