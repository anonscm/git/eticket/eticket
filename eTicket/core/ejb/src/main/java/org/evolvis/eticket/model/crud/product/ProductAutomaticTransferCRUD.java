package org.evolvis.eticket.model.crud.product;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.ProductAutomaticTransfer;

public interface ProductAutomaticTransferCRUD extends GenericCUD<ProductAutomaticTransfer>{

}
