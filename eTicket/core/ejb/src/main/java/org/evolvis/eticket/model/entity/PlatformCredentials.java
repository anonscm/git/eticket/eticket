package org.evolvis.eticket.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "username" }))
public class PlatformCredentials {
	@Id
	@GeneratedValue
	private long id;
	@Column(nullable = false)
	private String username;
	@Column(nullable = false)
	private String passwordHash;
	@Column(nullable = false)
	private String salt;

	public PlatformCredentials() {

	}

	public PlatformCredentials(String username, String salt, String password) {
		this.username = username;
		this.passwordHash = password;
		this.salt = salt;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

}
