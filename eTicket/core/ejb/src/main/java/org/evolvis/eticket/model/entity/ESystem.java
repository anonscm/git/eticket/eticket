package org.evolvis.eticket.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;

@Entity
@NamedQuery(name = "findSystemByName", query = "SELECT e FROM ESystem e WHERE e.name LIKE ?1")		
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "name", "platform_id" }))
public class ESystem {
	private String name;
	private Locale defaultLocale;
	private List<Locale> supportedLocales;
	//for cascading purpose only
	private List<AccountHistory> accountHistories;
	private List<Account> accounts;
	private List<Role> roles;
	private List<TextTemplate> textTemplates;
	private List<ConfigurationParameter> parameters;
	private List<Product> products;

	protected long id;

	private Platform platform;
	//just for deleting purpose!
	

	public ESystem() {
	}
	
	public ESystem(ESystemDTO eSystemDTO) {
		fromDto(eSystemDTO, true);
	}

	public void setId(long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	@Column(nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// @Column(nullable = false)
	@ManyToOne(cascade = CascadeType.PERSIST, optional = false)
	public Locale getDefaultLocale() {
		return defaultLocale;
	}

	public void setDefaultLocale(Locale defaultLocale) {
		this.defaultLocale = defaultLocale;
	}

	@ManyToMany
	public List<Locale> getSupportedLocales() {
		if (supportedLocales == null)
			supportedLocales = new ArrayList<Locale>();
		return supportedLocales;
	}

	public void setSupportedLocales(List<Locale> supportedLocales) {
		this.supportedLocales = supportedLocales;
	}

	@Transient
	public ESystemDTO toDto() {
		return toDto(true);
	}

	@Transient
	public ESystemDTO toDto(boolean withoutLocales) {
		List<LocaleDTO> suDtos = new ArrayList<LocaleDTO>();
		if (!withoutLocales)
			for (Locale l : getSupportedLocales())
				suDtos.add(l.toDto());
		return ESystemDTO.createESystemDTOFromExsistingESystemEntity(id, name, defaultLocale.toDto(), suDtos);
	}
	
	@Transient
	public void fromDto(ESystemDTO eSystemDTO, boolean withoutLocales) {
		if(!withoutLocales) {
			ArrayList<Locale> al = new ArrayList<Locale>();
			for(LocaleDTO l : eSystemDTO.getSupportedLocales()) {
				Locale locale = new Locale(l);
				al.add(locale);
			}
			supportedLocales = al;
		}
		
		defaultLocale = new Locale(eSystemDTO.getDefaultLocale());
		name = eSystemDTO.getName();
		
		if(eSystemDTO.getId() > -1) {
			this.id = eSystemDTO.getId();
		}
	}

	public void setPlatform(Platform platform) {
		this.platform = platform;
	}

	@ManyToOne(optional = false)
	public Platform getPlatform() {
		return platform;
	}

	public void setAccountHistories(List<AccountHistory> accountHistories) {
		this.accountHistories = accountHistories;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "system")
	public List<AccountHistory> getAccountHistories() {
		return accountHistories;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "system")
	public List<Role> getRoles() {
		return roles;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "system")
	public List<TextTemplate> getTextTemplates() {
		return textTemplates;
	}

	public void setTextTemplates(List<TextTemplate> textTemplates) {
		this.textTemplates = textTemplates;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "system")
	public List<ConfigurationParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<ConfigurationParameter> parameters) {
		this.parameters = parameters;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "system")
	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "system")
	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
}
