package org.evolvis.eticket.model.crud.platform;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.PlatformAccountHistory;

public class PlatformAccountHistoryCRUDBean extends
	GenericCUDSkeleton<PlatformAccountHistory> implements
	PlatformAccountHistoryCRUD {

    /**
     *
     */
    private static final long serialVersionUID = -3252679200011083082L;

    @Override
    public List<PlatformAccountHistory> listPlatformAccountHistoryFromUsername(
	    String username) {
	return buildNamedQueryGetResults(
		"listPlatformAccountHistoryFromUsername", username);
    }

    @Override
    public List<PlatformAccountHistory> listPlatformAccountHistoryCreatedByUsername(
	    String username) {
	return buildNamedQueryGetResults(
		"listPlatformAccountHistoryCreatedByUsername", username);
    }

}
