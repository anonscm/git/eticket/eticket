package org.evolvis.eticket.model.crud.platform;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Locale;

/**
 * The Interface LocaleCRUDBeanLocal.
 */
public interface LocaleCRUD extends GenericCUD<Locale> {
	
	/**
	 * Finds the Entity by id.
	 *
	 * @param id the id
	 * @return the locale
	 */
	public Locale findById(long id);
	
	/**
	 * Finds the Entity by code.
	 *
	 * @param code the code
	 * @return the locale
	 */
	public Locale findByCode(String code);
}
