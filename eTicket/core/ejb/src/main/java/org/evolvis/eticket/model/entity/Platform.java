package org.evolvis.eticket.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 * A platform holds systems and may hold default system properties for creating
 * new systems.
 * 
 */
@Entity
@NamedQuery(name = "findPlatformByName", query="SELECT p FROM Platform p WHERE p.name = ?1")
public class Platform {

	private List<ESystem> systems;
	private long id;
	private String name;
	private PlatformAccount owner;
	private List<Actor> actors;


	public Platform() {
	}

	public Platform(String platformName) {
		name = platformName;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(unique = true, nullable = false)
	public String getName() {
		return name;
	}

	/**
	 * Gets all systems.
	 * 
	 * @return the systems
	 */
	@OneToMany(cascade = { CascadeType.REMOVE, CascadeType.REFRESH })
	public List<ESystem> getSystems() {
		if (systems == null)
			systems = new ArrayList<ESystem>();
		return systems;
	}

	public void setSystems(List<ESystem> systems) {
		this.systems = systems;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Platform))
			return false;
		Platform p = (Platform) obj;
		return getName() == p.getName();
	}

	@Override
	public int hashCode() {
		int result = 17;
		result += 31 * result + name.hashCode();
		return result;
	}

	public void setOwner(PlatformAccount owner) {
		this.owner = owner;
	}

	@ManyToOne(cascade = CascadeType.PERSIST)
	public PlatformAccount getOwner() {
		return owner;
	}
	
	public void setActors(List<Actor> actors) {
		this.actors = actors;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "platform")
	public List<Actor> getActors() {
		return actors;
	}


	// public void addSystem(ESystem system) {
	// if (systems == null)
	// systems = new ArrayList<ESystem>();
	// systems.add(system);
	// }

}
