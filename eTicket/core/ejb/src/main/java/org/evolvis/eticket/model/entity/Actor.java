package org.evolvis.eticket.model.entity;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.IndexColumn;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
//@NamedQuery(name = "listActorInSystem", query = "SELECT a FROM Actor a WHERE a.system = ?1")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "credential_id",
		"platform_id" }))
public class Actor {

	private long id;

	private URI logo = null;
	
	private Platform platform;
	// private List<ProductPool> productPools = new ArrayList<ProductPool>();

	// TODO: may create a enum for for those values?
	private boolean locked = false;
	private boolean deleted = false;

	private Credentials credential;
	
	private List<Role> roles;

	@Id
	@GeneratedValue
	@Column(name = "id")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	public Credentials getCredential() {
		return credential;
	}

	public void setCredential(Credentials credential) {
		this.credential = credential;
	}

	public URI getLogo() {
		return logo;
	}

	public void setLogo(URI logo) {
		this.logo = logo;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@IndexColumn(name="Roles")
	public List<Role> getRoles() {
		if (roles == null)
			roles = new ArrayList<Role>();
		return roles;
	}

	@ManyToOne(optional = false)
	public Platform getPlatform() {
		return platform;
	}

	public void setPlatform(Platform platform) {
		this.platform = platform;
	}


}
