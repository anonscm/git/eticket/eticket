package org.evolvis.eticket.interceptors;

import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.evolvis.eticket.business.platform.PlatformBean;
import org.evolvis.eticket.business.system.SystemBean;
import org.evolvis.eticket.model.crud.platform.PlatformCRUD;
import org.evolvis.eticket.model.entity.Platform;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.util.CalculateHash;

public class CheckPlatformAdmin {

	@EJB
	private PlatformCRUD platformCRUDBeanLocal;

	public void setPlatformCRUDBeanLocal(PlatformCRUD platformCRUDBeanLocal) {
		this.platformCRUDBeanLocal = platformCRUDBeanLocal;
	}

	@AroundInvoke
	public Object checkPlatformadmin(InvocationContext context)
			throws Exception {
		checkCorrectUseOfInteceptor(context);
		String platformId = (String) context.getParameters()[1];
		Platform platform = platformCRUDBeanLocal.findByName(platformId);
		AccountDTO accountDTO = (AccountDTO) context.getParameters()[0];
		if (!checkPlatformAccount(platform,
				accountDTO))
			throw new IllegalAccessException("Caller is not Admin of platform "
					+ platformId);
		return context.proceed();
	}

	private void checkCorrectUseOfInteceptor(InvocationContext context) {
		if (!((context.getTarget().getClass().equals(PlatformBean.class) || context
				.getTarget().getClass().equals(SystemBean.class))
				&& context.getParameters() != null
				&& context.getParameters()[0] instanceof AccountDTO && context
					.getParameters()[1] instanceof String)) {
			throw new IllegalArgumentException("Illegal use of Interceptor"
					+ this.getClass().getSimpleName() + ".");
		}

	}

	private boolean checkPlatformAccount(Platform platform,
			AccountDTO accountDTO) {
		boolean eqUsername = accountDTO.getUsername().equals(
				platform.getOwner().getCredentials().getUsername());
		return eqUsername && isPasswordEqual(platform, accountDTO);
	}

	private boolean isPasswordEqual(Platform platform, AccountDTO accountDTO) {
		String salt = platform.getOwner().getCredentials().getSalt();
		String savedHash = platform.getOwner().getCredentials()
				.getPasswordHash();
		return CalculateHash.isPasswordCorrect(accountDTO, salt, savedHash);
	}


}