package org.evolvis.eticket.model.crud.system;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.ESystem;

/**
 * The Interface PlatformCRUDLocal.
 */
public interface ESystemCRUD extends GenericCUD<ESystem> {


	/**
	 * Finds the Entity by id.
	 *
	 * @param id the id
	 * @return the platform
	 */
	public ESystem findById(long id);

	/**
	 * Finds the Entity by name.
	 *
	 * @param name the name
	 * @return the e system
	 */
	public ESystem findByName(String name);

}
