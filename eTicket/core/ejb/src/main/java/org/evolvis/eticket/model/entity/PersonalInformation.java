package org.evolvis.eticket.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;

@Entity
public class PersonalInformation {
	private long id;
	private String firstname;
	private String lastname;
	private String title;
	private String salutation;
	private String address;
	private String city;
	private String zip;
	private String country;
	private String email;
	private Locale locale;
	private String organisation;

	public PersonalInformation() {
	}

	public PersonalInformation(String firstname, String surname,
			Locale defaultLocale) {
		setFirstname(firstname);
		setLastname(surname);
		setLocale(defaultLocale);
	}
	
	public PersonalInformation(PersonalInformationDTO personalInformationDTO) {
		fromDto(personalInformationDTO);
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	@ManyToOne(optional = false)
	public Locale getLocale() {
		return locale;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getOrganisation() {
		return organisation;
	}

	@Transient
	public PersonalInformationDTO toDto() {
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(
				locale.getLocalecode(), locale.getName());
		return PersonalInformationDTO
				.createPersonalInformationDTOFromExsistingPersonalInformationEntity(id,
						salutation, title, firstname, lastname, address, zip,
						city, country, localeDTO, organisation, email);
	}

	@Transient
	public void fromDto(PersonalInformationDTO personalInformationDTO) {
		this.salutation = personalInformationDTO.getSalutation();
		this.title = personalInformationDTO.getTitle();
		this.firstname = personalInformationDTO.getFirstname();
		this.lastname = personalInformationDTO.getLastname();
		this.address = personalInformationDTO.getAddress();
		this.zip = personalInformationDTO.getZipcode();
		this.city = personalInformationDTO.getCity();
		this.country = personalInformationDTO.getCountry();
		this.locale = new Locale();
		this.locale.fromDto(personalInformationDTO.getLocaleDTO());
		this.organisation = personalInformationDTO.getOrganisation();
		this.email = personalInformationDTO.getEmail();
		
		if(personalInformationDTO.getId() > -1) {
			id = personalInformationDTO.getId();
		}
	}
}
