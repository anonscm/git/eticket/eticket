package org.evolvis.eticket.model.crud.product;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductNumber;

@Stateless
@Local(ProductNumberCRUD.class)
public class ProductNumberCRUDBean extends GenericCUDSkeleton<ProductNumber> implements ProductNumberCRUD{

	private static final long serialVersionUID = 5715187678848052336L;

	@Override
	public ProductNumber find(Product product) {
		return buildNamedQueryGetSingleResult("findProductNumber", product);
	}

}
