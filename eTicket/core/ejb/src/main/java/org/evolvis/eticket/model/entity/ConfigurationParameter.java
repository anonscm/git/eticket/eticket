package org.evolvis.eticket.model.entity;

import java.math.BigDecimal;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "KEY", "SYSTEM_ID" }))
@NamedQueries({
		@NamedQuery(name = "findConfigurationParameterBySystemAndKey", query = "SELECT c FROM ConfigurationParameter c WHERE c.system = ?1 AND c.key = ?2"),
		@NamedQuery(name = "listConfigurationParameterInSystem", query = "SELECT c FROM ConfigurationParameter c WHERE c.system = ?1") })
public class ConfigurationParameter {



	private long id;
	private String key;
	private String value;
	private ESystem system;

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setSystem(ESystem system) {
		this.system = system;
	}

	@ManyToOne(optional = false)
	public ESystem getSystem() {
		return system;
	}

	@Transient
	public int toInt() {
		if (isInteger(value))
			return Integer.parseInt(value);
		return -1;
	}

	@Transient
	public long toLong() {
		if (isInteger(value))
			return Long.parseLong(value);
		return -1;
	}

	@Transient
	public BigDecimal toBigDecimal() {
		if (isInteger(value))
			return new BigDecimal(value);
		return new BigDecimal(-1);

	}

	@Transient
	private boolean isInteger(String value2) {
		Pattern isInteger = Pattern.compile("\\d+");
		return isInteger.matcher(value2).matches();
	}

	@Override
	public String toString() {
		return value;
	}

}
