package org.evolvis.eticket.model.crud.platform;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Platform;

/**
 * creates, reads, updates, deletes a Platform.
 * 
 * @author PEder
 */
@Stateless
@Local(PlatformCRUD.class)
public class PlatformCRUDBean extends GenericCUDSkeleton<Platform> implements
		PlatformCRUD {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 598491190496900745L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.evolvis.model.crud.PlatformCRUDLocal#findById(long)
	 */
	@Override
	public Platform findByName(String id) {
		return buildNamedQueryGetSingleResult("findPlatformByName", id);
	}

}
