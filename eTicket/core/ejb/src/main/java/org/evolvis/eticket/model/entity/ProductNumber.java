package org.evolvis.eticket.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.evolvis.eticket.model.entity.dto.ProductNumberDTO;

@Entity
@NamedQueries({
	@NamedQuery(name = "findProductNumber", query = "SELECT p FROM ProductNumber p WHERE p.product = ?1") })
public class ProductNumber {

	@Id
	@GeneratedValue()
	private long id;
	private long maxNumber;
	private long currentNumber;
	private long increment;
	@OneToOne
	private Product product;
		
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getMaxNumber() {
		return maxNumber;
	}
	public void setMaxNumber(long maxNumber) {
		this.maxNumber = maxNumber;
	}
	public long getCurrentNumber() {
		return currentNumber;
	}
	public void setCurrentNumber(long currentNumber) {
		this.currentNumber = currentNumber;
	}
	public long getIncrement() {
		return increment;
	}
	public void setIncrement(long increment) {
		this.increment = increment;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	
	@Transient
	public ProductNumberDTO toDto(){

		return ProductNumberDTO.createProductNumberDTOFromExsistingProductNumberLocaleEntity(id, getMaxNumber(), getCurrentNumber(), getIncrement(), getProduct().toDto()); 

	}
	
	@Transient
	public void fromDto(ProductNumberDTO productNumber){
		this.currentNumber = productNumber.getCurrentNumber();
		this.increment = productNumber.getIncrement();
		this.maxNumber = productNumber.getMaxNumber();
		Product product = new Product();
		product.fromDto(productNumber.getProduct());
		this.product = product;
		if(productNumber.getId() > -1) {
			this.id = productNumber.getId();
		}
	}	
}
