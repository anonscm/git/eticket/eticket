package org.evolvis.eticket.model.crud.product;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.ProductLocale;

public interface ProductLocaleCRUD extends GenericCUD<ProductLocale> {

	public ProductLocale findByLocaleCodeTemplateUin(String localecode,
			String templateUin);
	
	public ProductLocale findProductLocaleByLocalecodeAndProductUinAndSystemId(String localecode, String uin, long systemId);

	public List<ProductLocale> findallProductLocales();
	
	public ProductLocale findByLocaleCodeAndProductId(Locale locale,
			long productId);
	
	public ProductLocale findById(long id);

	public ProductLocale findProductLocaleById(long Id); 
}
