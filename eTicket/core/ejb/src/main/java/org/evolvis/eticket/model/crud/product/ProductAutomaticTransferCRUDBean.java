package org.evolvis.eticket.model.crud.product;

import javax.ejb.Local;
import javax.ejb.Stateless;
import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.ProductAutomaticTransfer;

@Stateless
@Local(ProductAutomaticTransferCRUD.class)
public class ProductAutomaticTransferCRUDBean extends
		GenericCUDSkeleton<ProductAutomaticTransfer> implements
		ProductAutomaticTransferCRUD {

	private static final long serialVersionUID = -3665639837586320996L;

	
	

}
