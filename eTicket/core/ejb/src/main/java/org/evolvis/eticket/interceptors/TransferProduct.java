package org.evolvis.eticket.interceptors;

import java.util.List;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.evolvis.eticket.business.product.ProductTransferSkeleton;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.TransferHistory.TransferState;
import org.evolvis.eticket.model.entity.dto.AccountDTO;

//TODO refactor
public class TransferProduct extends ProductTransferSkeleton {
	@AroundInvoke
	public Object aop(InvocationContext context) throws Exception {
		validateIcUse(context);
		transferProduct(context);
		return context.proceed();
	}

	private void transferProduct(InvocationContext context) {
		AccountDTO accountDto = (AccountDTO) context.getParameters()[0];
		ESystem system = systemCRUD
				.findByName(accountDto.getSystem().getName());
		Account rec = accountCRUD.findBySystemAndUserName(system,
				accountDto.getUsername());
		
		if (!(rec.isActivated())){
		  List<Product> products = getProducts(system, context);		
		  for (Product p : products){			
			ProductPool pool = transferProductFromSystemToReceiver(p.getPat().getAmount(), system,
					rec, p);
			insertHistoryEvent(pool, null, rec, p.getPat().getAmount(), TransferState.OPEN);
		  }
		}
	}

	private List<Product> getProducts(ESystem system, InvocationContext context) {
		String method = context.getMethod().getName();
		return productCRUD.listBySystemClassAndMethod(system, context
				.getTarget().getClass(), method);
	}

	private void validateIcUse(InvocationContext context) {
		if (context.getParameters() == null
				|| context.getParameters().length < 1
				|| (!(context.getParameters()[0] instanceof AccountDTO)))
			throw new IllegalArgumentException("Illegal use of interceptor "
					+ this.getClass().getSimpleName());
	}

}
