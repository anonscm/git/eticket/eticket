package org.evolvis.eticket.model.crud.user;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ActivateAccountToken;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.util.GenerateToken;

@Stateless
@Local(ActivateAccountTokenCRUD.class)
public class ActivateAccountTokenCRUDBean extends
		GenericCUDSkeleton<ActivateAccountToken> implements
		ActivateAccountTokenCRUD {

	private static final long serialVersionUID = 8448392952364685875L;

	@Override
	public ActivateAccountToken createNewToken(Account account, Date validUntil) {
		if (account == null || validUntil == null)
			throw new IllegalArgumentException(
					"Account and validUntil must not be null");
		ActivateAccountToken result = new ActivateAccountToken();
		result.setValidUntil(validUntil);
		result.setToken(GenerateToken.generateToken());
		result.setAccount(account);
		insert(result);
		return result;
	}

	@Override
	public ActivateAccountToken findByAccountAndToken(Account account,
			String token) {
		if (account == null)
			throw new IllegalArgumentException("Account must not be null");
		return buildNamedQueryGetSingleResult(
				"findActivateAccountTokenByAccountAndToken", account.getId(),
				token);
	}

	@Override
	public ActivateAccountToken findByAccount(Account account) {
		if (account == null)
			throw new IllegalArgumentException("Account must not be null");
		try {
			return buildNamedQueryGetSingleResult(
					"findActivateAccountTokenByAccount", account.getId());
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public List<ActivateAccountToken> listOutdatedInSystem(ESystem system) {
		return buildNamedQueryGetResults(
				"listOutdatedActivateAccountTokenInSystem", system,
				GregorianCalendar.getInstance().getTime());
	}

	@Override
	public ActivateAccountToken findById(long id) {
		return find(ActivateAccountToken.class, id);
	}

	@Override
	public int deleteOutdatedInSystem(ESystem system) {
		return buildNamedQueryExecute(
				"deleteOutdatedActivateAccountTokenInSystem", system,
				GregorianCalendar.getInstance().getTime());

	}

}
