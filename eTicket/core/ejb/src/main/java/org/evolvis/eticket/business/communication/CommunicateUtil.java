package org.evolvis.eticket.business.communication;

import java.util.HashMap;
import java.util.Map;

import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.Credentials;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.TextTemplate;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO.TextTemplateNames;
import org.evolvis.eticket.util.CalculateHash;
import org.evolvis.eticket.util.GenerateToken;
import org.evolvis.eticket.util.GenericServiceLoader;

public class CommunicateUtil {

	private static final Class<? extends Communicate> EMAIL = EMailBean.class;

	public static void sendMail(
			ConfigurationParameterCRUD configurationParameterBeanLocal,
			ESystem system, Account account, TextTemplate template,
			Map<String, String> buzzwords) {
		send(configurationParameterBeanLocal, system, account, template,
				buzzwords, EMAIL);
	}

	private static void send(
			ConfigurationParameterCRUD configurationParameterBeanLocal,
			ESystem system, Account account, final TextTemplate template,
			Map<String, String> buzzwords, Class<?> impl) {
		Communicate send = GenericServiceLoader.get(Communicate.class, impl);
		Account systemAccount = getSystemAccount(system,
				configurationParameterBeanLocal);
		send.sendTo(systemAccount, account, template, buzzwords);
	}

	private static Account getSystemAccount(ESystem eSystem,
			ConfigurationParameterCRUD configurationParameterBeanLocal) {
		final Account result = new Account();
		String name = configurationParameterBeanLocal.findBySystemAndKey(
				eSystem, CPKey.SYSTEM_SENDER.toString()).getValue();
		String salt = CalculateHash.generateSalt();
		String hash = CalculateHash.calculateWithSalt(salt, GenerateToken.generateToken());
		result.setCredential(new Credentials(name, salt, hash, null));
		return result;
	}

	public static Map<String, String> getBuzzwords(TextTemplateNames tempName, String...values){
		return generateMap(tempName.getBuzzwords(), values);
	}

	private static Map<String, String> generateMap(String[] keys,
			String[] values) {
		if (keys.length != values.length)
			throw new IllegalArgumentException(
					"The length of the keys differs the length of the objects.");
		final Map<String, String> result = new HashMap<String, String>();
		for (int i = 0; i < keys.length; i++)
			result.put(keys[i], values[i]);
		return result;
	}

}
