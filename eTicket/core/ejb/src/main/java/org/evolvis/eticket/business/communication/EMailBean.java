package org.evolvis.eticket.business.communication;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.TextTemplate;
import org.evolvis.eticket.util.Html;
import org.evolvis.eticket.util.LoadPropertiesFile;

public class EMailBean implements Communicate {
	private static final String USER = "USER";
	private static final String PASSWORD = "PASSWORD";
	private static final String EMAIL_PATTERN = "^[^@]+@.+\\.[^.]+$";
	private static final String PORT = "PORT";
	private static final String HOST = "HOST";
	private static final String EMAIL_PROPERTIES = "email.properties";

	private boolean isValidEMail(String email) {
		return Pattern.compile(EMAIL_PATTERN, Pattern.CASE_INSENSITIVE)
				.matcher(email).matches();

	}

	@Override
	public void sendTo(Account sender, Account recipient,
			TextTemplate template, Map<String, String> buzzwordsAndValues) {
		String fulltext = generateText(template, buzzwordsAndValues);
		String subject = getSubject(fulltext);
		String[] text = fulltext.split("\n", 2);
		try {
			sendMail(sender, recipient, subject, text[1]);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("An error occured while sending email.",
					e);
		}

	}

	private void sendMail(Account sender, Account recipient, String subject,
			String text) throws MessagingException,
			UnsupportedEncodingException {
		Session session = initSession();
		Transport transport = session.getTransport();
		MimeMessage message = createMsg(sender, recipient, subject, text,
				session);
		send(transport, message);

	}

	private void send(Transport transport, MimeMessage message)
			throws MessagingException {
		transport.connect();
		transport.sendMessage(message,
				message.getRecipients(Message.RecipientType.TO));
		transport.close();
	}

	private MimeMessage createMsg(Account sender, Account recipient,
			String subject, String text, Session session)
			throws MessagingException, UnsupportedEncodingException {
		MimeMessage message = new MimeMessage(session);
		message.setSubject(subject);
		message.setHeader("Content-Type", "text/plain; charset=utf-8");
		message.setText(text, "utf-8");
		message.setFrom(new InternetAddress(getEmail(sender), getName(sender)));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(
			getEmail(recipient), getName(recipient)));
		return message;
	}

	private String getName(Account account) {
		if (account.getPersonalInformation() != null) {
			return account.getPersonalInformation().getFirstname() + " "
					+ account.getPersonalInformation().getLastname();
		}
		return null;
	}

	private String getEmail(Account account) {
		if (isValidEMail(account.getCredential().getUsername())) {
			return account.getCredential().getUsername();
		} else if (account.getPersonalInformation() != null) {
			return account.getPersonalInformation().getEmail();
		} else {
			throw new IllegalArgumentException("No valid e-mail address given.");
		}
	}

	private Session initSession() throws NoSuchProviderException {
		Properties props = new Properties();
		Properties properties = getProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", properties.getProperty(HOST));
		props.put("mail.smtp.port", properties.getProperty(PORT));
		String user = properties.getProperty(USER);
		String pass = properties.getProperty(PASSWORD);
		Session mailSession = setAuth(props, user, pass);
		return mailSession;
	}

	private Session setAuth(Properties props, final String user, final String pass) {
		if (user != null) {
			props.put("mail.smtp.auth", "true");
			Authenticator auth = new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(user, pass);
				}
			};
			return Session.getInstance(props, auth);
		}
		return Session.getInstance(props);
	}

	private Properties getProperties() {
		Properties properties;
		try {
			properties = (new LoadPropertiesFile()).open(EMAIL_PROPERTIES);
		} catch (Exception e) {
			properties = new Properties();
			properties.put(HOST, "localhost");
			properties.put(PORT, "25");
		}
		return properties;
	}

	public String getSubject(String template) {
		int firstLineBreak = template.indexOf('\n');
		return template.substring(0, firstLineBreak);
	}

	private String generateText(TextTemplate template,
			Map<String, String> buzzwordsAndValues) {
		String text = template.getText();
		Iterator<Map.Entry<String, String>> iter = buzzwordsAndValues
				.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, String> entry = iter.next();
			text = text.replaceAll("%" + entry.getKey() + "%",
					Html.decodeFromHtml(entry.getValue()));
		}
		return text;
	}
}