package org.evolvis.eticket.business.system.account;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;

import org.evolvis.eticket.annotation.AllowedRole;
import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.communication.CommunicateUtil;
import org.evolvis.eticket.interceptors.CheckAccountRights;
import org.evolvis.eticket.interceptors.TransferProduct;
import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.product.ProductPoolCRUD;
import org.evolvis.eticket.model.crud.product.TransferHistoryCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.system.RoleCRUD;
import org.evolvis.eticket.model.crud.system.TextTemplateCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.crud.user.ActivateAccountTokenCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ActivateAccountToken;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.Credentials;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.PersonalInformation;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;
import org.evolvis.eticket.model.entity.TextTemplate;
import org.evolvis.eticket.model.entity.TransferHistory;
import org.evolvis.eticket.model.entity.TransferHistory.TransferState;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.AccountShortSummeryDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO.TextTemplateNames;
import org.evolvis.eticket.util.CalculateHash;

@Stateless
@Remote(AccountBeanService.class)
@Interceptors({ CheckAccountRights.class })
@AllowedRole(AllowedRoles.SYSADMIN)
public class AccountBean implements AccountBeanService {

	private static final long serialVersionUID = -3611506774235578115L;

	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;

	@EJB
	private AccountCRUD accountCRUDBeanLocal;

	@EJB
	private RoleCRUD roleCRUDBeanLocal;

	@EJB
	private ActivateAccountTokenCRUD activateAccountTokenCRUDBeanLocal;

	@EJB
	private ConfigurationParameterCRUD configurationParameterBeanLocal;

	@EJB
	private TextTemplateCRUD textTemplateCRUDBeanLocal;

	@EJB
	private LocaleCRUD localeCRUDBeanLocal;

	@EJB
	private ProductPoolCRUD productPoolCRUDBeanLocal;

	@EJB
	private TransferHistoryCRUD transferHistoryCRUDBeanLocal;

	@Override
	@ExcludeClassInterceptors
	@Interceptors({ TransferProduct.class })
	public void activate(AccountDTO accountDTO, String token) {
		activateAccountAndSetPassword(accountDTO, token);
	}

	@Override
	@ExcludeClassInterceptors
	public void setPassword(AccountDTO accountDTO, String token) {
		activateAccountAndSetPassword(accountDTO, token);
	}

	@AllowedRole(AllowedRoles.SYSADMIN)
	public String getAccountMount(AccountDTO accountDTO) {
		final ESystem system = eSystemCRUDBeanLocal.findByName(accountDTO
				.getSystem().getName());
		List<Account> accounts = (List<Account>) accountCRUDBeanLocal
				.listAccountsInSystem(system);
		long amount = accounts.size();

		return Long.toString(amount);
	}

	@AllowedRole(AllowedRoles.SYSADMIN)
	public String getInaktiveAccountMount(AccountDTO accountDTO) {
		final ESystem system = eSystemCRUDBeanLocal.findByName(accountDTO
				.getSystem().getName());
		List<Account> accounts = (List<Account>) accountCRUDBeanLocal
				.listInactiveAccountsInSystem(system);
		long amount = accounts.size();

		return Long.toString(amount);
	}

	private void activateAccountAndSetPassword(AccountDTO accountDTO,
			String token) {
		final ESystem system = eSystemCRUDBeanLocal.findByName(accountDTO
				.getSystem().getName());
		final Account account = accountCRUDBeanLocal.findBySystemAndUserName(
				system, accountDTO.getUsername());
		final ActivateAccountToken activateAccountToken = activateAccountTokenCRUDBeanLocal
				.findByAccountAndToken(account, token);
		account.setActivated(true);
		account.getCredential().setPasswordValid(true);
		String newHash = CalculateHash.calculateWithSalt(account.getCredential().getSalt(), accountDTO.getPassword());
		account.getCredential().setPasswordHash(newHash);
		activateAccountTokenCRUDBeanLocal.delete(activateAccountToken);
	}

	/**
	 * Adds the roles.
	 * 
	 * @param account
	 *            the account
	 * @param fromString
	 *            the from string
	 * @return the account
	 */
	private Account addRoles(Account account, AllowedRoles fromString) {
		switch (fromString) {
		case SYSADMIN:
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.SYSADMIN.toString()));
		case PRODUCTADMIN:
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.PRODUCTADMIN.toString()));
		case MODERATOR:
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.MODERATOR.toString()));
		case USER:
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.USER.toString()));
			break;
		default:
			throw new IllegalArgumentException("Unhandled role "
					+ fromString.toString() + ".");
		}
		return account;
	}

	private Account updateRoles(Account account, AllowedRoles fromString) {

		switch (fromString) {
		case SYSADMIN:
			account.getRoles().clear();
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.SYSADMIN.toString()));
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.PRODUCTADMIN.toString()));
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.MODERATOR.toString()));
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.USER.toString()));
			break;
		case PRODUCTADMIN:
			account.getRoles().clear();
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.PRODUCTADMIN.toString()));
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.MODERATOR.toString()));
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.USER.toString()));
			break;
		case MODERATOR:
			account.getRoles().clear();
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.MODERATOR.toString()));
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.USER.toString()));
			break;
		case USER:
			account.getRoles().clear();
			account.getRoles().add(
					roleCRUDBeanLocal.findBySystemAndName(account.getSystem(),
							AllowedRoles.USER.toString()));
			break;
		default:
			throw new IllegalArgumentException("Unhandled role "
					+ fromString.toString() + ".");
		}
		return account;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.evolvis.eticket.business.system.account.AccountBeanLocal#createAccount
	 * (org.evolvis.eticket.model.entity.Account,
	 * org.evolvis.eticket.model.entity.dto.AccountDTO)
	 */
	@Override
	public void create(AccountDTO caller, AccountDTO accountDTO) {
		createAccount(accountDTO, true);
	}

	/**
	 * Creates the account.
	 * 
	 * @param system
	 *            the system
	 * @param accountDTO
	 *            the account dto
	 */
	private Account createAccount(AccountDTO accountDTO, boolean activated) {
		final ESystem eSystem = eSystemCRUDBeanLocal.findByName(accountDTO
				.getSystem().getName());
		Account account = new Account();
		Credentials credential = createCredential(accountDTO);
		account.setCredential(credential );
//		account.setCredential(new Credentials(accountDTO.getUsername(),
//				(accountDTO.isPasswordHashed() ? accountDTO.getPassword()
//						: Hash.getHash(accountDTO.getPassword())), accountDTO
//						.getOpenId()));
		account.setSystem(eSystem);
		account = addRoles(account,
				AllowedRoles.fromString(accountDTO.getRole().getRole()));
		account.setActivated(activated);
		accountCRUDBeanLocal.insert(account);
		return account;
	}

	private Credentials createCredential(AccountDTO accountDTO) {
		String salt = CalculateHash.generateSalt();
		Credentials credential = new Credentials(accountDTO.getUsername(), salt, CalculateHash.calculateWithSalt(salt, accountDTO.getPassword()), accountDTO.getOpenId());
		return credential;
	}

	@AllowedRole(AllowedRoles.SYSADMIN)
	public void createunactivatedAccount(AccountDTO admin, AccountDTO user,
			PersonalInformationDTO persinf) {
		final Account account = createAccount(user, false);
		final ESystem eSystem = eSystemCRUDBeanLocal.findByName(user
				.getSystem().getName());
		createToken(eSystem, account, persinf.getLocaleDTO().getCode())
				.getToken();
		UpdatePersonalInformation(user, persinf);

	}

	private ActivateAccountToken createToken(ESystem system, Account account,
			String localecode) {
		final long activationIntervall = configurationParameterBeanLocal
				.findBySystemAndKey(system,
						CPKey.ACCOUNT_VALIDATION_LIMIT.toString()).toLong();
		final ActivateAccountToken result = activateAccountTokenCRUDBeanLocal
				.createNewToken(account, new Date(Calendar.getInstance()
						.getTimeInMillis() + activationIntervall));
		send(system, account, result, TextTemplateNames.NEW_ACCOUNT, localecode);
		return result;
	}

	private void send(ESystem system, Account account,
			final ActivateAccountToken result,
			final TextTemplateNames templateName, String localecode) {
		final Locale locale = getLocale(system, account, localecode);
		final TextTemplate template = textTemplateCRUDBeanLocal
				.findBySystemLocaleAndName(system, locale,
						templateName.toString());
		String sysDomain = configurationParameterBeanLocal.findBySystemAndKey(
				system, CPKey.SYSTEM_DOMAIN.toString()).getValue();
		String al = sysDomain + "/dispatcher/activate" + "?token="
				+ result.getToken() + "&clientname="
				+ account.getCredential().getUsername() + "&platform="
				+ system.getPlatform().getName() + "&system=" + system.getName();
		Map<String, String> buzzwords = CommunicateUtil.getBuzzwords(
				templateName, al);
		CommunicateUtil.sendMail(configurationParameterBeanLocal, system,
				account, template, buzzwords);
	}

	@Override
	@ExcludeClassInterceptors
	// @Interceptors({ CheckAccountRights.class })
	public void createUser(AccountDTO accountDTO, String localecode) {
		if (AllowedRoles.fromString(accountDTO.getRole().getRole()) != AllowedRoles.USER) {
			throw new IllegalArgumentException(
					"Only accounts with role user can be created with createUser.");
		}
		final ESystem eSystem = eSystemCRUDBeanLocal.findByName(accountDTO
				.getSystem().getName());
		final Account account = createAccount(accountDTO, false);
		createToken(eSystem, account, localecode).getToken();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.evolvis.eticket.business.system.account.AccountBeanLocal#deleteAccount
	 * (org.evolvis.eticket.model.entity.Account,
	 * org.evolvis.eticket.model.entity.Account)
	 */
	@Override
	public void delete(AccountDTO caller, AccountDTO deleteAccount) {
		final Account acc = accountCRUDBeanLocal.findBySystemAndUserName(
				eSystemCRUDBeanLocal.findByName(deleteAccount.getSystem()
						.getName()), deleteAccount.getUsername());
		deleteActivateAccountToken(acc);
		deleteTransferHistory(acc);
		deleteProductPool(acc);
		accountCRUDBeanLocal.delete(acc);
	}

	@AllowedRole(AllowedRoles.SYSADMIN)
	public void deletePwhashed(AccountDTO caller, AccountDTO deleteAccount) {
		final Account acc = accountCRUDBeanLocal.findBySystemAndUserName(
				eSystemCRUDBeanLocal
						.findById(deleteAccount.getSystem().getId()),
				deleteAccount.getUsername());
		deleteActivateAccountToken(acc);
		deleteTransferHistory(acc);
		deleteProductPool(acc);
		accountCRUDBeanLocal.delete(acc);
	}

	private void deleteActivateAccountToken(Account account) {
		try {
			ActivateAccountToken token = activateAccountTokenCRUDBeanLocal
					.findByAccount(account);
			if (token != null)
				activateAccountTokenCRUDBeanLocal.delete(token);
		} catch (Exception e) {
			// No token to delete
		}
	}

	private void deleteProductPool(Account account) {
		List<ProductPool> productPools;
		ProductPoolStatus[] status = { ProductPoolStatus.FIXED,
				ProductPoolStatus.REGISTERED, ProductPoolStatus.VOLATILE };

		for (ProductPoolStatus productPoolStatus : status) {
			try {
				productPools = productPoolCRUDBeanLocal
						.findByStatusAndUsername(productPoolStatus, account
								.getCredential().getUsername());
				for (ProductPool productPool : productPools) {
					productPoolCRUDBeanLocal.delete(productPool);
				}
			} catch (Exception e) {
				// No pool to delete
			}
		}
	}

	private void deleteTransferHistory(Account account) {
		List<TransferHistory> transferHistories;
		TransferState[] states = { TransferState.ACCEPTED,
				TransferState.CANCELED, TransferState.DECLINED,
				TransferState.OPEN, TransferState.TAKENBYADMIN };

		for (TransferState transferState : states) {
			try {
				transferHistories = transferHistoryCRUDBeanLocal
						.findBySenderAndTransferState(account.getCredential()
								.getUsername(), transferState);
				deleteTransferHistorySender(transferHistories);
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				transferHistories = transferHistoryCRUDBeanLocal
						.findByRecipientUsernameAndTransferState(account
								.getCredential().getUsername(), transferState);
				deleteTransferHistoryRecipient(transferHistories);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void deleteTransferHistorySender(
			List<TransferHistory> transferHistories) {
		for (TransferHistory transferHistory : transferHistories) {
			if (transferHistory.getRecipient() != null) {
				transferHistory.setSender(null);
			} else {
				transferHistoryCRUDBeanLocal.delete(transferHistory);
			}
		}
	}

	private void deleteTransferHistoryRecipient(
			List<TransferHistory> transferHistories) {
		for (TransferHistory transferHistory : transferHistories) {
			if (transferHistory.getSender() != null) {
				transferHistory.setRecipient(null);
			} else {
				transferHistoryCRUDBeanLocal.delete(transferHistory);
			}
		}
	}

	@Override
	@ExcludeClassInterceptors
	public AccountDTO find(ESystemDTO systemDTO, String username) {
		final Account acc = accountCRUDBeanLocal.findBySystemAndUserName(
				eSystemCRUDBeanLocal.findByName(systemDTO.getName()), username);
		return acc.toAccountDTO();
	}

	@Override
	public AccountDTO searchUser(AccountDTO caller, ESystemDTO systemDTO,
			String username) {
		final Account acc = accountCRUDBeanLocal.findBySystemAndUserName(
				eSystemCRUDBeanLocal.findByName(systemDTO.getName()), username);
		return acc.toAccountDTO();
	}

	private List<String> createSearchWords(String input) {
		List<String> searchwords = new ArrayList<String>();
		int pointer = 0;
		for (int i = 0; i < input.length(); i++) {

			if (input.charAt(i) == ' ') {
				if (i > 0 && !(input.charAt(i - 1) == ' ')) {
					String add = input.substring(pointer, i);
					searchwords.add(add);
				}
				pointer = i + 1;
			}
		}
		searchwords.add(input.substring(pointer));
		return searchwords;
	}

	private List<Account> searchAccountsInCredantialsAndPersonalInforamtion(
			List<String> searchwords, List<Account> overalaccounts,
			ESystemDTO systemDTO) {
		for (String searchword : searchwords) {
			List<Account> accountsSingleword = accountCRUDBeanLocal
					.findBySystemAndLikeUserName(eSystemCRUDBeanLocal
							.findByName(systemDTO.getName()), searchword);
			List<Account> accPlusPersinf = accountCRUDBeanLocal
					.findBySystemAndLikeNameInPerosnalinformation(
							eSystemCRUDBeanLocal
									.findByName(systemDTO.getName()),
							searchword);

			for (Account ac : overalaccounts) {
				if (accPlusPersinf.contains(ac)) {
					accPlusPersinf.remove(ac);
				}
				if (accountsSingleword.contains(ac)) {
					accountsSingleword.remove(ac);
				}

			}
			for (Account ac : accPlusPersinf) {
				overalaccounts.add(ac);
			}
			for (Account ac : accountsSingleword) {
				overalaccounts.add(ac);
			}

		}
		return overalaccounts;
	}

	private List<AccountShortSummeryDTO> createAccountShortSummyDTO(
			List<Account> overalaccounts) {

		List<AccountShortSummeryDTO> accounts = new ArrayList<AccountShortSummeryDTO>();
		for (Account ac : overalaccounts) {
			long amount = 0;
			List<ProductPool> pools = productPoolCRUDBeanLocal
					.findByUsername(ac.getCredential().getUsername());
			if (pools.size() > 0) {
				for (ProductPool pool : pools) {
					amount += pool.getAmount();
				}
			}
			String realName = "no personalinformation";
			if (ac.getPersonalInformation() != null) {
				realName = ac.getPersonalInformation().getFirstname() + " "
						+ ac.getPersonalInformation().getLastname();
			}
			accounts.add(AccountShortSummeryDTO.createAccountShortSummeryDTO(ac
					.getCredential().getUsername(), realName, String
					.valueOf(amount)));
		}
		return accounts;
	}

	@Override
	public List<AccountShortSummeryDTO> searchUsers(AccountDTO caller,
			ESystemDTO systemDTO, String username) {

		List<Account> overalaccounts = accountCRUDBeanLocal
				.findBySystemAndLikeUserName(
						eSystemCRUDBeanLocal.findByName(systemDTO.getName()),
						username);
		List<String> searchwords = createSearchWords(username);
		overalaccounts = searchAccountsInCredantialsAndPersonalInforamtion(
				searchwords, overalaccounts, systemDTO);
		return createAccountShortSummyDTO(overalaccounts);
	}

	@ExcludeClassInterceptors
	@Override
	public void generateToken(AccountDTO accountDTO) {
		final ESystem system = eSystemCRUDBeanLocal.findByName(accountDTO
				.getSystem().getName());
		final Account account = accountCRUDBeanLocal.findBySystemAndUserName(
				system, accountDTO.getUsername());
		createToken(system, account, null);
	}

	private Locale getLocale(ESystem system, Account account, String localecode) {
		if (localecode != null) {
			return localeCRUDBeanLocal.findByCode(localecode);
		}
		if (account.getPersonalInformation() != null) {
			return account.getPersonalInformation().getLocale();
		}

		return system.getDefaultLocale();
	}

	@ExcludeClassInterceptors
	public void setAccountCRUDBeanLocal(AccountCRUD accountCRUDBeanLocal) {
		this.accountCRUDBeanLocal = accountCRUDBeanLocal;
	}

	@ExcludeClassInterceptors
	public void setActivateAccountTokenCRUDBeanLocal(
			ActivateAccountTokenCRUD activateAccountTokenCRUDBeanLocal) {
		this.activateAccountTokenCRUDBeanLocal = activateAccountTokenCRUDBeanLocal;
	}

	@ExcludeClassInterceptors
	public void setConfigurationParameterBeanLocal(
			ConfigurationParameterCRUD configurationParameterBeanLocal) {
		this.configurationParameterBeanLocal = configurationParameterBeanLocal;
	}

	@ExcludeClassInterceptors
	public void seteSystemCRUDBeanLocal(ESystemCRUD eSystemCRUDBeanLocal) {
		this.eSystemCRUDBeanLocal = eSystemCRUDBeanLocal;
	}

	@ExcludeClassInterceptors
	public void setLocaleCRUDBeanLocal(LocaleCRUD localeCRUDBeanLocal) {
		this.localeCRUDBeanLocal = localeCRUDBeanLocal;
	}

	@ExcludeClassInterceptors
	public void setRoleCRUDBeanLocal(RoleCRUD roleCRUDBeanLocal) {
		this.roleCRUDBeanLocal = roleCRUDBeanLocal;
	}

	@ExcludeClassInterceptors
	public void setTextTemplateCRUDBeanLocal(
			TextTemplateCRUD textTemplateCRUDBeanLocal) {
		this.textTemplateCRUDBeanLocal = textTemplateCRUDBeanLocal;
	}

	@Override
	public void update(AccountDTO caller, AccountDTO updAccount) {
		Account acc = accountCRUDBeanLocal.findBySystemAndUserName(
				eSystemCRUDBeanLocal.findById(updAccount.getSystem().getId()),
				updAccount.getUsername());
		acc = setNewData(acc, updAccount);
		accountCRUDBeanLocal.update(acc);
	}

	@Override
	@AllowedRole(AllowedRoles.SYSADMIN)
	public void updateRole(AccountDTO caller, AccountDTO accounttoupdate,
			AccountDTO updAccount) {
		// TODO Auto-generated method stub
		Account acc = accountCRUDBeanLocal.findBySystemAndUserName(
				eSystemCRUDBeanLocal.findById(accounttoupdate.getSystem()
						.getId()), accounttoupdate.getUsername());
		acc = updateRoles(acc,
				AllowedRoles.fromString(updAccount.getRole().getRole()));

		accountCRUDBeanLocal.update(acc);
	}

	private Account setNewData(Account account, AccountDTO updAccount) {
		Credentials credential = account.getCredential();
		credential.setUsername(updAccount.getUsername());
		credential.setOpenId(updAccount.getOpenId());
		credential.setPasswordHash(updAccount.getPassword());

		account = addRoles(account,
				AllowedRoles.fromString(updAccount.getRole().getRole()));
		final ESystem system = eSystemCRUDBeanLocal.findByName(updAccount
				.getSystem().getName());
		account.setSystem(system);
		account.setCredential(credential);
		return account;
	}

	@Override
	@ExcludeClassInterceptors
	public AccountDTO validatePassword(AccountDTO accountDTO)
			throws IllegalAccessException {
		final ESystem eSystem = eSystemCRUDBeanLocal.findByName(accountDTO
				.getSystem().getName());
		return accountCRUDBeanLocal.checkPwGetAccount(eSystem, accountDTO)
				.toAccountDTO();
	}

	@Override
	@ExcludeClassInterceptors
	public PersonalInformationDTO getPersonalInformationDTO(
			ESystemDTO systemDTO, String username) {
		final Account acc = accountCRUDBeanLocal.findBySystemAndUserName(
				eSystemCRUDBeanLocal.findByName(systemDTO.getName()), username);
		if (acc.getPersonalInformation() != null)
			return acc.getPersonalInformation().toDto();
		else
			return null;
	}

	@Override
	@ExcludeClassInterceptors
	public void UpdatePersonalInformation(AccountDTO accountDTO,
			PersonalInformationDTO personalInformationDTO) {
		final Account acc = accountCRUDBeanLocal.findBySystemAndUserName(
				eSystemCRUDBeanLocal.findByName(accountDTO.getSystem()
						.getName()), accountDTO.getUsername());
		PersonalInformation personalInformation = acc.getPersonalInformation();
		if (personalInformation == null) {
			personalInformation = new PersonalInformation();
		}
		Locale locale = localeCRUDBeanLocal.findByCode(personalInformationDTO
				.getLocaleDTO().getCode());
		personalInformation.setAddress(personalInformationDTO.getAddress());
		personalInformation.setCity(personalInformationDTO.getCity());
		personalInformation.setCountry(personalInformationDTO.getCountry());
		personalInformation.setEmail(personalInformationDTO.getEmail());
		personalInformation.setFirstname(personalInformationDTO.getFirstname());
		personalInformation.setLastname(personalInformationDTO.getLastname());
		personalInformation.setLocale(locale);
		personalInformation.setOrganisation(personalInformationDTO
				.getOrganisation());
		personalInformation.setSalutation(personalInformationDTO
				.getSalutation());
		personalInformation.setTitle(personalInformationDTO.getTitle());
		personalInformation.setZip(personalInformationDTO.getZipcode());
		acc.setPersonalInformation(personalInformation);
		accountCRUDBeanLocal.update(acc);
	}

	@ExcludeClassInterceptors
	// @Interceptors({ TransferProduct.class })
	public void UpdatePersonalInformationplusticket(AccountDTO accountDTO,
			PersonalInformationDTO personalInformationDTO) {
		final Account acc = accountCRUDBeanLocal.findBySystemAndUserName(
				eSystemCRUDBeanLocal.findByName(accountDTO.getSystem()
						.getName()), accountDTO.getUsername());
		PersonalInformation personalInformation = acc.getPersonalInformation();
		if (personalInformation == null) {
			personalInformation = new PersonalInformation();
		}
		Locale locale = localeCRUDBeanLocal.findByCode(personalInformationDTO
				.getLocaleDTO().getCode());
		personalInformation.setAddress(personalInformationDTO.getAddress());
		personalInformation.setCity(personalInformationDTO.getCity());
		personalInformation.setCountry(personalInformationDTO.getCountry());
		personalInformation.setEmail(personalInformationDTO.getEmail());
		personalInformation.setFirstname(personalInformationDTO.getFirstname());
		personalInformation.setLastname(personalInformationDTO.getLastname());
		personalInformation.setLocale(locale);
		personalInformation.setOrganisation(personalInformationDTO
				.getOrganisation());
		personalInformation.setSalutation(personalInformationDTO
				.getSalutation());
		personalInformation.setTitle(personalInformationDTO.getTitle());
		personalInformation.setZip(personalInformationDTO.getZipcode());
		acc.setPersonalInformation(personalInformation);
		accountCRUDBeanLocal.update(acc);
	}

	@Override
	@ExcludeClassInterceptors
	public boolean isActivated(ESystemDTO systemDTO, String username) {
		final Account acc = accountCRUDBeanLocal.findBySystemAndUserName(
				eSystemCRUDBeanLocal.findByName(systemDTO.getName()), username);
		return acc.isActivated();
	}

	@Override
	@ExcludeClassInterceptors
	public AccountDTO findWithOpenId(ESystemDTO systemDTO, String openId) {
		final Account acc = accountCRUDBeanLocal.findBySystemAndOpenId(
				eSystemCRUDBeanLocal.findByName(systemDTO.getName()), openId);
		return acc.toAccountDTO();
	}

}
