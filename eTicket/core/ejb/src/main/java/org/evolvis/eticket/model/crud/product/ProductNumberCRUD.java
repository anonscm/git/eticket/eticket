package org.evolvis.eticket.model.crud.product;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductNumber;

public interface ProductNumberCRUD extends GenericCUD<ProductNumber>{
	
	public ProductNumber find(Product product);
}
