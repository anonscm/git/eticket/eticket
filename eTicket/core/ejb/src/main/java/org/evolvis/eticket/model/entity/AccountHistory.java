package org.evolvis.eticket.model.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "listAccountHistoriesBySystemAndUsername", query = "SELECT a FROM AccountHistory a WHERE a.system = ?1 AND a.username = ?2")
public class AccountHistory extends CredentialHistory {


	private ESystem system;

	public void setSystem(ESystem system) {
		this.system = system;
	}
	
	@ManyToOne(optional = false)
	public ESystem getSystem() {
		return system;
	}
	

}
