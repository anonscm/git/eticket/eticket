package org.evolvis.eticket.model.crud.user;

import java.util.Date;
import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ActivateAccountToken;
import org.evolvis.eticket.model.entity.ESystem;

public interface ActivateAccountTokenCRUD extends GenericCUD<ActivateAccountToken> {
	public ActivateAccountToken findById(long id);
	public ActivateAccountToken findByAccountAndToken(Account account, String token);
	public ActivateAccountToken findByAccount(Account account);
	public List<ActivateAccountToken> listOutdatedInSystem(ESystem system);
	public ActivateAccountToken createNewToken(Account account, Date validUntil);
	public int deleteOutdatedInSystem(ESystem system);

}
