package org.evolvis.eticket.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "username" }))
public class Credentials {

	private long id;

	private String username;
	private String passwordHash;
	private String salt;
	private String openId;
	private boolean passwordValid = false;

	public Credentials(String username, String salt, String passwordHash, String openId) {
		setUsername(username);
		setPasswordHash(passwordHash);
		setOpenId(openId);
		setSalt(salt);
	}

	public Credentials() {
		super();
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isPasswordValid() {
		return passwordValid;
	}

	public void setPasswordValid(boolean valid) {
		this.passwordValid = valid;
	}

	@Column(nullable = false)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(nullable = false)
	public String getPasswordHash() {
		return passwordHash;
	}
	
	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}
}
