package org.evolvis.eticket.model.crud.system;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Role;

/**
 * The Interface RoleCRUDBeanLocal.
 */
public interface RoleCRUD extends GenericCUD<Role> {

	/**
	 * Finds the Entity by system and name.
	 *
	 * @param system
	 *            the system
	 * @param name
	 *            the name
	 * @return the role
	 */
	public Role findBySystemAndName(ESystem system, String name);

	/**
	 * List entities in system.
	 *
	 * @param system
	 *            the system
	 * @return list of entities in system
	 */
	public List<Role> listSystem(ESystem system);
}
