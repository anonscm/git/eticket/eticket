package org.evolvis.eticket.model.crud.platform;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.PlatformAccount;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.util.CalculateHash;
import org.evolvis.eticket.util.MessageConstClass;

@Stateless
@Local(PlatformAccountCRUD.class)
public class PlatformAccountCRUDBean extends
		GenericCUDSkeleton<PlatformAccount> implements PlatformAccountCRUD {

	private static final long serialVersionUID = 5919394248839664414L;

	@Override
	public PlatformAccount findById(long id) {
		return find(PlatformAccount.class, id);
	}

	@Override
	public PlatformAccount findByUsername(String username) {
		return buildNamedQueryGetSingleResult("findPlatformAccountByUsername",
				username);
	}

	@Override
	public List<PlatformAccount> listPlatformAccounts() {
		return buildNamedQueryGetResults("listPlatformAccounts");
	}

	@Override
	public PlatformAccount checkPassword(AccountDTO platformAdmin)
			throws IllegalAccessException {
		PlatformAccount result;
		try {
			result = findByUsername(platformAdmin.getUsername());
		} catch (RuntimeException e) {
			// obfuscate the reason
			throw new IllegalAccessException(
					MessageConstClass.USER_UNKNOWN_OR_PASSWORD_INVALID);
		}

		if (CalculateHash.isPasswordCorrect(platformAdmin, result
				.getCredentials().getSalt(), result.getCredentials()
				.getPasswordHash()))
			return result;
		else
			throw new IllegalAccessException(
					MessageConstClass.USER_UNKNOWN_OR_PASSWORD_INVALID);
	}
}
