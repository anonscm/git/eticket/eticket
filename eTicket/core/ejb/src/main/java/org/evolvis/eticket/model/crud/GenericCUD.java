package org.evolvis.eticket.model.crud;

import java.io.Serializable;

/**
 * The Interface GenericCUDInterface basically for GenericCUD, its for
 * createing, updating and deleting a entity T.
 *
 * @param <T>
 *            the generic type of a Entity
 */
public interface GenericCUD<T> extends Serializable {
	// public T find(long id);

	/**
	 * Update a given Enity.
	 *
	 * @param entity
	 *            the entity
	 */
	public void update(T entity);

	/**
	 * Insert a given Enity.
	 *
	 * @param entity
	 *            the entity
	 */
	public void insert(T entity);

	/**
	 * Delete a given Enity.
	 *
	 * @param entity
	 *            the entity
	 */
	public void delete(T entity);

//	/**
//	 * Finds the Entity.
//	 *
//	 * @param entityClass the entity class
//	 * @param id the id
//	 * @return the t
//	 */
//	public T find(Class<T> entityClass, long id);

}
