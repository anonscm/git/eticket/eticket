package org.evolvis.eticket.model.crud.system;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Role;

/**
 * The Class RoleCRUDBean.
 */
@Stateless
@Local(RoleCRUD.class)
public class RoleCRUDBean extends GenericCUDSkeleton<Role> implements RoleCRUD {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 966179077108817257L;

    /*
     * (non-Javadoc)
     *
     * @see
     * org.evolvis.model.crud.RoleCRUDBeanLocal#findBySystemAndName(org.evolvis
     * .model.entity.ESystem, java.lang.String)
     */
    @Override
    public Role findBySystemAndName(ESystem system, String name) {
	return buildNamedQueryGetSingleResult("findRoleBySystemAndName",
		system, name);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.evolvis.model.crud.RoleCRUDBeanLocal#listSystem(org.evolvis.model
     * .entity.ESystem)
     */
    @Override
    public List<Role> listSystem(ESystem system) {
	return buildNamedQueryGetResults("listRolesInSystem", system);
    }

}
