package org.evolvis.eticket.model.crud.product;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Templates;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;


@Stateless
// I still like the idea of local interfaces ... even it's not necessary
// anymore, call it a tribute to ejb 3.0 if you wish.
@Local(TemplateCRUD.class)
public class TemplateCRUDBean extends GenericCUDSkeleton<Templates> implements
		TemplateCRUD {
	private static final long serialVersionUID = -6254084337378016291L;

	@Override
	public List<TemplatesDTO> getAllTemplatesDTOs(){
		List <Templates>liste=buildNamedQueryGetResults("getAllTemplates");
		List <TemplatesDTO>returnlist=new ArrayList<TemplatesDTO>();
		for (Templates item:liste){
			returnlist.add(item.toDto());
		}
		return returnlist;
		
	}
	@Override
	public Templates findByUin(String uin) {
		List<Templates> result = buildNamedQueryGetResults("findTemplateByUin",
				uin);
		if (result.size() > 0)
			return result.get(0);
		else
			return null;
	}

	@Override
	public Templates findById(long id) {
		return find(Templates.class, id);
	}

}
