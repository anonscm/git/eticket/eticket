package org.evolvis.eticket.model.crud.user;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.AccountHistory;
import org.evolvis.eticket.model.entity.ESystem;

public interface AccountHistoryCRUD extends GenericCUD<AccountHistory> {
    public List<AccountHistory> listAccountHistoriesBySystemAndUsername(ESystem system, String username);
}
