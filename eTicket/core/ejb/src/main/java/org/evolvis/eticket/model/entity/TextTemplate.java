package org.evolvis.eticket.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.evolvis.eticket.model.entity.dto.TextTemplateDTO;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "NAME",
		"SYSTEM_ID", "LOCALE_ID" }))
@NamedQueries({
		@NamedQuery(name = "findTextTemplateBySystemLocaleName", query = "SELECT t FROM TextTemplate t WHERE t.system = ?1 AND t.locale = ?2 AND t.name = ?3"),
		@NamedQuery(name = "listTextTemplateInSystemLocale", query = "SELECT t FROM TextTemplate t WHERE t.system = ?1 AND t.locale = ?2") })
public class TextTemplate {
	private long id;
	private Locale locale;
	private ESystem system;
	private String name;
	private String description;

	private String text;

	public TextTemplate(TextTemplateDTO textTemplate) {
		fromDto(textTemplate);
	}

	public TextTemplate() {

	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne(optional = false)
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	@ManyToOne(optional = false)
	public ESystem getSystem() {
		return system;
	}

	public void setSystem(ESystem system) {
		this.system = system;
	}

	@Column(nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(nullable = false, columnDefinition = "VARCHAR(23542)")
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public TextTemplateDTO toDTO() {
		return TextTemplateDTO.createTextTemplateDTOFromExsistingTextTemplateEntity(getSystem().toDto(), getLocale().toDto(),
				getName(), getText(), getDescription());
	}

	public void fromDto(TextTemplateDTO textTemplate) {
		setName(textTemplate.getName());
		setDescription(textTemplate.getDescripption());
		setText(textTemplate.getText());
	}

}
