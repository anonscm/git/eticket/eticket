package org.evolvis.eticket.model.crud.product;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Templates;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;

public interface TemplateCRUD extends GenericCUD<Templates> {
    public Templates findById(long id);
    public Templates findByUin(String uin);
	public List<TemplatesDTO> getAllTemplatesDTOs();
}
