package org.evolvis.eticket.interceptors;

import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class Logging {
	private static final Level EXCEPTION_LVL = Level.WARNING;
	private static final Level DEBUG_LVL = Level.FINE;
	private Logger logger;
	private String fqnMethod;

	 @AroundInvoke
	 public Object log(InvocationContext context) throws Exception {
	 String className = sqtFqnOfMethodAndReturnClassname(context);
	 logger = Logger.getLogger(className);
	 return logMethodAndException(context);
	 }

	private String sqtFqnOfMethodAndReturnClassname(InvocationContext context) {
		String className = context.getTarget().getClass().getName();
		String method = context.getMethod().getName();
		fqnMethod = className + "#" + method;
		return className;
	}

	private Object logMethodAndException(InvocationContext context)
			throws Exception {
		try {
			return logMethod(context);
		} catch (Exception e) {
			logger.log(EXCEPTION_LVL, "Exception:" + e.getMessage() + " in "
					+ fqnMethod);
			throw e;
		}
	}

	private Object logMethod(InvocationContext context) throws Exception {
		long startTime = logEntering(context);
		Object o = context.proceed();
		logEndTime(startTime);
		return o;
	}

	private void logEndTime(long startTime) {
		long timeNeeded = GregorianCalendar.getInstance().getTimeInMillis()
				- startTime;
		logger.log(DEBUG_LVL, "endet:" + fqnMethod + " needed:" + timeNeeded
				+ "ms");
	}

	private long logEntering(InvocationContext context) {
		long startTime = GregorianCalendar.getInstance().getTimeInMillis();
		logger.log(DEBUG_LVL, "entering:" + fqnMethod + " with params: "
				+ getParamsAsString(context.getParameters()));
		return startTime;
	}

	private String getParamsAsString(Object[] parameters) {
		String result = "";
		if (parameters != null)
			result = getParamsString(parameters, result);
		return result;
	}

	private String getParamsString(Object[] parameters, String result) {
		for (Object o : parameters)
			result = extendStringWithObjectToString(result, o);
		return result;
	}

	private String extendStringWithObjectToString(String result, Object o) {
		if (o != null)
			result += o.toString() + ";";
		return result;
	}
}
