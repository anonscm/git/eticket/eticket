/*
 * 
 */
package org.evolvis.eticket.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.evolvis.eticket.interceptors.CheckAccountRights;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;


/**
 * This annotation is to annotate methods to restrict the access to a given role. Which will than checked by {@link CheckAccountRights}. 
 * 
 * 
 * @author phil
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface AllowedRole {
	
	AllowedRoles value();
}
