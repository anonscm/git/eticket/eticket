package org.evolvis.eticket.business.communication;

import java.util.Map;

import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.TextTemplate;

public interface Communicate{
	public void sendTo(Account sender, Account recipient, TextTemplate template,
			Map<String, String> buzzwordsAndValues);
}
