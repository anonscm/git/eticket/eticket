package org.evolvis.eticket.business.facades;

import java.io.IOException;
import java.util.Arrays;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.binary.Base64;
import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.facades.utils.RestUtils;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.TransferMethod;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;

@Path("/")
public class ProductRestFacade {

	private ProductBeanService getProductBeanRemote() {
		return RestUtils.lookup("ProductBean");
	}

	private SystemBeanService getSystemBean() {
		return RestUtils.lookup("SystemBean");
	}

	private AccountBeanService getAccountBean() {
		return RestUtils.lookup("AccountBean");
	}

	@POST
	@Path("put/template/{platformID}/{system}/{uin}")
	public void createOrUpdateTemplate(@FormParam("user") String user,
			@FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system, @PathParam("uin") String uin,
			@FormParam("data") String base64EncodedData,
			@FormParam("mime") String mimeType,
			@FormParam("description") String description) throws IOException {
		ESystemDTO eSystemDTO = ESystemDTO.createNewESystemDTO(system, LocaleDTO.createNewLocaleDTO("ehw",
				"unwichtig"), null);
		AccountDTO caller = AccountDTO.initWithExistingHash(eSystemDTO, user, pass,
				RoleDTO.getProductAdmin(eSystemDTO.getId()),  null);
		byte[] binaryData = Base64.decodeBase64(base64EncodedData.getBytes());// new
																				// BASE64Decoder().decodeBuffer(base64EncodedData);
		TemplatesDTO templatesDTO = TemplatesDTO.createNewTemplatesDTO(uin, binaryData, mimeType,
				description);
		getProductBeanRemote().putTemplate(caller, templatesDTO);
	}

	@GET
	@Path("get/template/{platformID}/{system}/{uin}")
	public byte[] getTemplate(@PathParam("platformID") long platformID,
			@PathParam("system") String system, @PathParam("uin") String uin) {
		return getProductBeanRemote().getTemplate(uin).getBinaryData();
	}

	@PUT
	@Path("insert/product/{platformID}/{system}")
	public void createProduct(@PathParam("platformID") long platformID,
			@PathParam("system") String system, @FormParam("user") String user,
			@FormParam("pass") String pass, @FormParam("uin") String uin,
			@FormParam("maxAmount") int maxAmount,
			@FormParam("currentAmount") int currAmount,
			@FormParam("localecode") String localecode,
			@FormParam("productType") String productType,
			@FormParam("productName") String productName,
			@FormParam("currency") String currency,
			@FormParam("price") double price,
			@FormParam("salesTax") double salesTax,
			@FormParam("templateUin") String template)
			throws IllegalAccessException {
		ESystemDTO eSystemDTO = getSystemBean().getSystemByName(system);
		RoleDTO roleDTO = getAccountBean().find(eSystemDTO, user).getRole();
		AccountDTO accountDTO = AccountDTO.initWithExistingHash(eSystemDTO, user, pass,
				roleDTO,  null);
		getAccountBean().validatePassword(accountDTO);

		
		
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(localecode, localecode);

		TemplatesDTO templatesDTO = getProductBeanRemote()
				.getTemplate(template);

		ProductLocaleDTO[] productLocaleDTO = { ProductLocaleDTO.createNewProductLocaleDTO(
				localeDTO, currency, productName, "des", templatesDTO, price,
				salesTax) };

		ProductDTO product = ProductDTO.createNewProductDTO(uin,
				ProductType.valueOf(productType), eSystemDTO, productLocaleDTO,0,
				maxAmount);

		getProductBeanRemote().create(accountDTO, product);
	}

	@PUT
	@Path("add/productlocale/{platformID}/{system}")
	public void addProductlocale(@PathParam("platformID") long platformID,
			@PathParam("system") String system, @FormParam("user") String user,
			@FormParam("pass") String pass, @FormParam("uin") String uin,
			@FormParam("localecode") String localecode,
			@FormParam("productName") String productName,
			@FormParam("currency") String currency,
			@FormParam("price") double price,
			@FormParam("salesTax") double salesTax,
			@FormParam("templateUin") String template)
			throws IllegalAccessException {

		ESystemDTO eSystemDTO = getSystemBean().getSystemByName(system);

		RoleDTO roleDTO = getAccountBean().find(eSystemDTO, user).getRole();
		AccountDTO accountDTO = AccountDTO.initWithExistingHash(eSystemDTO, user, pass,
				roleDTO,  null);
		getAccountBean().validatePassword(accountDTO);
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(localecode, localecode);
		TemplatesDTO templatesDTO = getProductBeanRemote()
				.getTemplate(template);

		ProductDTO productDTO = getProductBeanRemote().get(system, uin);
		ProductLocaleDTO[] productLocaleDTOs = productDTO.getProductLocales();
		productLocaleDTOs = Arrays.copyOf(productLocaleDTOs,
				productLocaleDTOs.length + 1);
		productLocaleDTOs[productLocaleDTOs.length - 1] = ProductLocaleDTO.createNewProductLocaleDTO(
				localeDTO, currency, productName, "des", templatesDTO,
				price, salesTax);

	
		ProductDTO newProduct = ProductDTO.createNewProductDTO(uin,
				productDTO.getProductType(), eSystemDTO, productLocaleDTOs,0,
				productDTO.getMaxAmount());
		getProductBeanRemote().update(accountDTO, newProduct);
	}

	@PUT
	@Path("update/product/{platformID}/{system}")
	public void updateProduct(@PathParam("platformID") long platformID,
			@PathParam("system") String system, @FormParam("user") String user,
			@FormParam("pass") String pass, @FormParam("uin") String uin,
			@FormParam("maxAmount") int maxAmount,
			@FormParam("currAmount") int currAmount,
			@FormParam("localecode") String localecode,
			@FormParam("productType") String productType)
			throws IllegalAccessException {
		ESystemDTO eSystemDTO = getSystemBean().getSystemByName(system);
		RoleDTO roleDTO = getAccountBean().find(eSystemDTO, user).getRole();
		AccountDTO accountDTO = AccountDTO.initWithExistingHash(eSystemDTO, user, pass,
				roleDTO,  null);
		getAccountBean().validatePassword(accountDTO);

		ProductLocaleDTO[] productLocaleDTO = getProductBeanRemote().get(
				system, uin).getProductLocales();

		ProductDTO product = ProductDTO.createNewProductDTO(uin,
				ProductType.valueOf(productType), eSystemDTO, productLocaleDTO,0,
				maxAmount);

		getProductBeanRemote().update(accountDTO, product);
	}

	@DELETE
	@Path("delete/product/{platformID}/{system}")
	public void deleteProduct(@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@QueryParam("user") String user, @QueryParam("pass") String pass,
			@QueryParam("uin") String uin) throws IllegalAccessException {
		ESystemDTO eSystemDTO = getSystemBean().getSystemByName(system);
		RoleDTO roleDTO = getAccountBean().find(eSystemDTO, user).getRole();
		AccountDTO accountDTO = AccountDTO.initWithExistingHash(eSystemDTO, user, pass,
				roleDTO,  null);
		getAccountBean().validatePassword(accountDTO);
		getProductBeanRemote().delete(accountDTO,
				getProductBeanRemote().get(system, uin));
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/product/{uin}/{platformID}/{system}")
	public ProductDTO getProduct(@PathParam("platformID") long platformID,
			@PathParam("system") String system, @PathParam("uin") String uin) {
		return getProductBeanRemote().get(system, uin);
	}

	@PUT
	@Path("insert/productloacale/{platformID}/{system}")
	public void createProductLocale(@PathParam("platformID") long platformID,
			@PathParam("system") String system, @FormParam("user") String user,
			@FormParam("pass") String pass, @FormParam("uin") String uin,
			@FormParam("localecode") String localecode,
			@FormParam("productName") String productName,
			@FormParam("currency") String currency,
			@FormParam("price") double price,
			@FormParam("salesTax") double salesTax,
			@FormParam("templateUin") String template)
			throws IllegalAccessException {
		ESystemDTO eSystemDTO = getSystemBean().getSystemByName(system);

		RoleDTO roleDTO = getAccountBean().find(eSystemDTO, user).getRole();
		AccountDTO accountDTO = AccountDTO.initWithExistingHash(eSystemDTO, user, pass,
				roleDTO,  null);
		getAccountBean().validatePassword(accountDTO);
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(localecode, localecode);
		TemplatesDTO templatesDTO = getProductBeanRemote()
				.getTemplate(template);

		ProductLocaleDTO productLocaleDTO = ProductLocaleDTO.createNewProductLocaleDTO(localeDTO,
				currency, "des", productName, templatesDTO, price, salesTax);
		getProductBeanRemote().putProductLocale(accountDTO, productLocaleDTO);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/productlocale/{platformID}/{system}")
	public ProductLocaleDTO getProductLocale(
			@PathParam("platformID") long platformID,
			@PathParam("system") String system, @FormParam("uin") String uin,
			@FormParam("localecode") String localecode) {
		ProductDTO product = getProductBeanRemote().get(system, uin);
		for (ProductLocaleDTO productLocale : product.getProductLocales()) {
			if (productLocale.getLocale().getCode().equals(localecode))
				return productLocale;
		}
		throw new IllegalArgumentException("The Product with the uin "
				+ product.getUin() + " have no Locale with the localecode "
				+ localecode + " is not available");
	}

	@PUT
	@Path("transfer/product/method/{platformID}/{system}")
	public void transferProductByMethod(
			@PathParam("platformID") long platformID,
			@PathParam("system") String system, @FormParam("user") String user,
			@FormParam("pass") String pass, @FormParam("uin") String uin,
			@FormParam("method") String method, @FormParam("amount") long amount)
			throws IllegalAccessException {
		ESystemDTO eSystemDTO = getSystemBean().getSystemByName(system);

		RoleDTO roleDTO = getAccountBean().find(eSystemDTO, user).getRole();
		AccountDTO accountDTO = AccountDTO.initWithExistingHash(eSystemDTO, user, pass,
				roleDTO,  null);
		getAccountBean().validatePassword(accountDTO);
		ProductDTO product = getProductBeanRemote().get(system, uin);
		getProductBeanRemote().transferProductByMethodcall(accountDTO, product,
				TransferMethod.fromString(method), amount);
	}

}
