//package org.evolvis.eticket.business.facades;
//
//import javax.ws.rs.DELETE;
//import javax.ws.rs.FormParam;
//import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
//import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
//import javax.ws.rs.Produces;
//import javax.ws.rs.QueryParam;
//
//import org.evolvis.eticket.business.account.AccountBeanService;
//import org.evolvis.eticket.business.facades.utils.RestUtils;
//import org.evolvis.eticket.business.platform.PlatformBeanService;
//import org.evolvis.eticket.business.system.SystemBeanService;
//import org.evolvis.eticket.model.entity.dto.AccountDTO;
//import org.evolvis.eticket.model.entity.dto.ESystemDTO;
//import org.evolvis.eticket.model.entity.dto.LocaleDTO;
//import org.evolvis.eticket.model.entity.dto.RoleDTO;
//
//@Path("/")
//public class PlatformRestFacade {
//
//	private PlatformBeanService getPlatformBeanLocal() {
//		return RestUtils.lookup("PlatformBean");
//	}
//
//	private SystemBeanService getSystemBeanRemote() {
//		return RestUtils.lookup("SystemBean");
//	}
//
//	private AccountBeanService getAccountBean() {
//		return RestUtils.lookup("AccountBean");
//	}
//
//	@PUT
//	@Path("insert/platform")
//	@Produces({ "application/json" })
//	public Long createPlatform(@FormParam("user") String user,
//			@FormParam("pass") String pass) throws IllegalAccessException {
//		return getPlatformBeanLocal().createPlatform(
//				AccountDTO.initWithExistingHash(null, user, pass,
//						RoleDTO.getPlatformAdmin(), null));
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see
//	 * org.evolvis.eticket.business.rest.PlatformRestBeanLocal#createLocale(
//	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
//	 */
//	@PUT
//	@Path("insert/locale/{id}/{localecode}/{localename}")
//	@Produces({ "application/json" })
//	public LocaleDTO createLocale(@FormParam("user") String user,
//			@FormParam("pass") String pass, @PathParam("id") long id,
//			@PathParam("localecode") String localecode,
//			@PathParam("localename") String locale)
//					throws IllegalAccessException {
//	
//		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(localecode, locale);
//
//		AccountDTO accountDTO = AccountDTO.initWithExistingHash(null, user, pass,
//
//				RoleDTO.getPlatformAdmin(),  null);
//		getPlatformBeanLocal().createLocale(accountDTO, id, localeDTO);
//		return localeDTO;
//		}
//	
//
//	@POST
//	@Produces({ "application/json" })
//	@Path("get/locale/{localecode}")
//	public LocaleDTO getLocale(@PathParam("localecode") String localecode) {
//		return getPlatformBeanLocal().getLocale(localecode);
//	}
//
//	@POST
//	@Produces({ "application/json" })
//	@Path("delete/locale/{id}/{system}/{localecode}")
//	public void deleteLocale(@FormParam("user") String user,
//			@FormParam("pass") String pass, @PathParam("id") long id,
//			@PathParam("system") String systemname,
//			@PathParam("localecode") String localecode)
//			throws IllegalAccessException {
//		ESystemDTO system = getSystemBeanRemote().getSystemByName(systemname);
//
//		RoleDTO roleDTO = getAccountBean().find(system, user).getRole();
//		AccountDTO accountDTO = AccountDTO.initWithExistingHash(system, user, pass, roleDTO,
//				 null);
//
//		getAccountBean().validatePassword(accountDTO);
//		getPlatformBeanLocal().deleteLocale(accountDTO, id, localecode);
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see
//	 * org.evolvis.eticket.business.rest.PlatformRestBeanLocal#deletePlatform
//	 * (java.lang.String, java.lang.String, long)
//	 */
//	@DELETE
//	@Path("delete/platform/{id}")
//	public void deletePlatform(@QueryParam("user") String user,
//			@QueryParam("pass") String pass, @PathParam("id") long id) {
//		getPlatformBeanLocal().deletePlatform(
//				AccountDTO.initWithExistingHash(null, user, pass,
//						RoleDTO.getPlatformAdmin(), null), id);
//	}
//
//}
