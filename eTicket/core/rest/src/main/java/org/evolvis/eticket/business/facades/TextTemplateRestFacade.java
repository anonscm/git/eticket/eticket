package org.evolvis.eticket.business.facades;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.evolvis.eticket.business.facades.utils.RestUtils;
import org.evolvis.eticket.business.system.TextTemplateBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO;
import org.evolvis.eticket.model.entity.dto.TextTemplateDTO.TextTemplateNames;

@Path("/")
public class TextTemplateRestFacade {

	private TextTemplateBeanService getTextTemplateBean() {
		return RestUtils.lookup("TextTemplateBean");
	}

	@PUT
	@Path("insert/texttemplate/{platformID}/{system}/{localecode}/{templateName}")
	public void insert(@FormParam("user") String user,
			@FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@PathParam("localecode") String localecode,
			@PathParam("templateName") String templateName,
			@FormParam("text") String text,
			@FormParam("description") String description) {
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(localecode, text);
		ESystemDTO eSystem = createSystem(system, localeDTO);
		AccountDTO caller = AccountDTO.initWithExistingHash(eSystem, user, pass,
				RoleDTO.getSysAdmin(eSystem.getId()),  null);
		TextTemplateDTO textTemplate = generateTextTemplate(eSystem,
				templateName, text, description);
		getTextTemplateBean().insert(caller, textTemplate);
	}

	private TextTemplateDTO generateTextTemplate(ESystemDTO system,
			String templateName, String text, String description) {
		TextTemplateNames name = TextTemplateNames.fromString(templateName);
		return TextTemplateDTO.createNewTextTemplateDTO(system, system.getDefaultLocale(), name,
				text, description);
	}

	private ESystemDTO createSystem(String system, LocaleDTO localeDTO) {
		ESystemDTO result = ESystemDTO.createNewESystemDTO(system, localeDTO, null);
		return result;
	}

	@POST
	@Path("update/texttemplate/{platformID}/{system}/{localecode}/{templateName}")
	public void update(@FormParam("user") String user,
			@FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@PathParam("localecode") String localecode,
			@PathParam("templateName") String templateName,
			@FormParam("text") String text,
			@FormParam("description") String description) {
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(localecode, text);
		ESystemDTO eSystem = createSystem(system, localeDTO);
		AccountDTO caller = AccountDTO.initWithExistingHash(eSystem, user, pass,
				RoleDTO.getSysAdmin(eSystem.getId()),  null);
		TextTemplateDTO textTemplate = generateTextTemplate(eSystem,
				templateName, text, description);
		getTextTemplateBean().update(caller, textTemplate);
	}

	@DELETE
	@Path("delete/texttemplate/{platformID}/{system}/{localecode}/{templateName}")
	public void delete(@QueryParam("user") String user,
			@QueryParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@PathParam("localecode") String localecode,
			@PathParam("templateName") String templateName) {
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(localecode, "t");
		ESystemDTO eSystem = createSystem(system, localeDTO);
		AccountDTO caller = AccountDTO.initWithExistingHash(eSystem, user, pass,
				RoleDTO.getSysAdmin(eSystem.getId()),  null);
		TextTemplateDTO textTemplate = generateTextTemplate(eSystem,
				templateName, null, null);
		getTextTemplateBean().delete(caller, textTemplate);
	}

	@GET
	@Produces({ "application/json" })
	@Path("delete/texttemplate/{platformID}/{system}/{localecode}/{templateName}")
	public TextTemplateDTO find(@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@PathParam("localecode") String localecode,
			@PathParam("templateName") String templateName) {
		LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO(localecode, "t");
		ESystemDTO eSystem = createSystem(system, localeDTO);
		return getTextTemplateBean().find(eSystem, localeDTO, templateName);
	}

}
