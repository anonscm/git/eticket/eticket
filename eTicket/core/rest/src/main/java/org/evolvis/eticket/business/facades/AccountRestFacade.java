package org.evolvis.eticket.business.facades;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.facades.utils.RestUtils;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;
import org.evolvis.eticket.util.GenerateToken;

@Path("/")
public class AccountRestFacade {

	private AccountBeanService getAccountBean() {
		return RestUtils.lookup("AccountBean");
	}

	private SystemBeanService getSystemBean() {
		return RestUtils.lookup("SystemBean");
	}

	@POST
	@Produces({ "application/json" })
	@Path("get/account/{platformID}/{system}")
	public AccountDTO findAccountByUsernameSystem(
			@FormParam("user") String user, @FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@FormParam("clientname") String username) {
		ESystemDTO eSystemDTO = getSystemDto(system);
		return getAccountBean().find(eSystemDTO, username);
	}

	private ESystemDTO getSystemDto(String system) {
		return ESystemDTO.createNewESystemDTO(system,
				LocaleDTO.createNewLocaleDTO("haha", "lol"), null);
	}

	@PUT
	@Produces({ "application/json" })
	@Path("insert/account/{platformID}/{system}/{localecode}")
	public void createUserAccount(@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@FormParam("clientname") String username,
			@PathParam("localecode") String localecode) {
		ESystemDTO eSystemDTO = getSystemDto(system);
		AccountDTO accountDTO = AccountDTO
				.initWithExistingHash(eSystemDTO,
						username, GenerateToken.generateToken(),
						RoleDTO.getUser(eSystemDTO.getId()), null);
		getAccountBean().createUser(accountDTO, localecode);
	}

	@PUT
	@Path("insert/account/{platformID}/{system}")
	public void createAccount(@FormParam("user") String user,
			@FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@FormParam("clientname") String username,
			@FormParam("clientPass") String password,
			@FormParam("clientRole") String role) {
		ESystemDTO eSystemDTO = getSystemDto(system);
		AccountDTO caller = AccountDTO
				.initWithExistingHash(eSystemDTO, user,
						pass, RoleDTO.getSysAdmin(eSystemDTO.getId()), null);
		AccountDTO accountDTO = AccountDTO
				.initWithExistingHash(
						eSystemDTO,
						username,
						password,
						RoleDTO.get(AllowedRoles.fromString(role),
								eSystemDTO.getId()), null);
		getAccountBean().create(caller, accountDTO);
	}

	@GET
	@Path("activate/account/{platformID}/{system}")
	public void activateAccountAndSetPassword(
			@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@QueryParam("clientname") String username,
			@QueryParam("token") String token,
			@QueryParam("clientPass") String password) {

		ESystemDTO eSystemDTO = getSystemDto(system);
		AccountDTO accountDTO = AccountDTO
				.initWithExistingHash(eSystemDTO,
						username, password,
						RoleDTO.getUser(eSystemDTO.getId()), null);
		getAccountBean().activate(accountDTO, token);
	}

	@POST
	@Produces({ "application/json" })
	@Path("login/account/{platformID}/{system}")
	public AccountDTO logIn(@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@FormParam("clientname") String username,
			@FormParam("clientPass") String password)
			throws IllegalAccessException {
		ESystemDTO eSystemDTO = getSystemDto(system);
		AccountDTO accountDTO = AccountDTO
				.initWithExistingHash(eSystemDTO,
						username, password,
						RoleDTO.getUser(eSystemDTO.getId()), null);
		return getAccountBean().validatePassword(accountDTO);

	}

	@GET
	@Path("forgotpassword/account/{platformID}/{system}")
	public void forgotPassword(@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@QueryParam("clientname") String username) {
		ESystemDTO eSystemDTO = getSystemDto(system);
		AccountDTO accountDTO = AccountDTO
				.initWithExistingHash(eSystemDTO,
						username, GenerateToken.generateToken(),
						RoleDTO.getUser(eSystemDTO.getId()), null);
		getAccountBean().generateToken(accountDTO);
	}

	@DELETE
	@Path("delete/account/{platformID}/{system}")
	public void deleteAccount(@QueryParam("user") String user,
			@QueryParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@QueryParam("clientname") String username) {
		ESystemDTO eSystemDTO = getSystemDto(system);
		AccountDTO caller = AccountDTO
				.initWithExistingHash(eSystemDTO, user,
						pass, RoleDTO.getSysAdmin(eSystemDTO.getId()), null);
		// only username and system are relevant...
		AccountDTO accountDTO = AccountDTO
				.initWithExistingHash(eSystemDTO,
						username, "IDONTCARE", RoleDTO.getPlatformAdmin(), null);
		getAccountBean().delete(caller, accountDTO);
	}

	@PUT
	@Path("update/account/{platformID}/{system}")
	public void update(@FormParam("user") String user,
			@FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@FormParam("clientUser") String clientUser,
			@FormParam("clientPassword") String clientPassword,
			@FormParam("clientRole") String clientRole)
			throws IllegalAccessException {
		ESystemDTO eSystemDTO = getSystemBean().getSystemByName(system);
		AccountDTO caller = AccountDTO
				.initWithExistingHash(eSystemDTO, user,
						pass, RoleDTO.getSysAdmin(eSystemDTO.getId()), null);
		getAccountBean().validatePassword(caller);
		caller = getAccountBean().find(eSystemDTO, user);
		AccountDTO acccall = AccountDTO
				.initWithExistingHash(caller.getSystem(),
						user, pass, caller.getRole(), "");
		AccountDTO updAccount = AccountDTO
				.initWithExistingHash(eSystemDTO,
						clientUser, clientPassword, RoleDTO.get(
								AllowedRoles.fromString(clientRole),
								eSystemDTO.getId()), "");
		getAccountBean().update(acccall, updAccount);
	}

	@POST
	@Produces({ "application/json" })
	@Path("get/personalinformation/{platformID}/{system}")
	public PersonalInformationDTO getPersonalInformationFromAccount(
			@FormParam("user") String user, @FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system) throws IllegalAccessException {
		ESystemDTO eSystemDTO = getSystemDto(system);
		AccountDTO caller = AccountDTO
				.initWithExistingHash(eSystemDTO, user,
						pass, RoleDTO.getUser(eSystemDTO.getId()), null);
		getAccountBean().validatePassword(caller);
		return getAccountBean().getPersonalInformationDTO(eSystemDTO,
				caller.getUsername());
	}

	@PUT
	@Path("update/personalinformation/{platformID}/{system}")
	public void updatePersonalInformation(@FormParam("user") String user,
			@FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@FormParam("salutation") String salutation,
			@FormParam("title") String title,
			@FormParam("firstname") String firstname,
			@FormParam("lastname") String lastname,
			@FormParam("address") String address,
			@FormParam("zipcode") String zipcode,
			@FormParam("city") String city,
			@FormParam("country") String country,
			@FormParam("localecode") String localecode,
			@FormParam("organisation") String organisation)
			throws IllegalAccessException {
		ESystemDTO eSystemDTO = getSystemDto(system);
		AccountDTO caller = AccountDTO
				.initWithExistingHash(eSystemDTO, user,
						pass, RoleDTO.getUser(eSystemDTO.getId()), null);
		getAccountBean().validatePassword(caller);
		PersonalInformationDTO personalInformationDTO = PersonalInformationDTO
				.createNewPersonalInformationDTO(salutation, title, firstname,
						lastname, address, zipcode, city, country,
						LocaleDTO.createNewLocaleDTO(localecode, localecode),
						organisation, user);
		getAccountBean().UpdatePersonalInformation(caller,
				personalInformationDTO);
	}

	@POST
	@Path("get/account/activated/{platformID}/{system}")
	public boolean isActivated(@FormParam("user") String user,
			@FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system) throws IllegalAccessException {
		ESystemDTO eSystemDTO = getSystemDto(system);
		AccountDTO caller = AccountDTO
				.initWithExistingHash(eSystemDTO, user,
						pass, RoleDTO.getUser(eSystemDTO.getId()), null);
		getAccountBean().validatePassword(caller);
		return getAccountBean().isActivated(eSystemDTO, caller.getUsername());
	}

}
