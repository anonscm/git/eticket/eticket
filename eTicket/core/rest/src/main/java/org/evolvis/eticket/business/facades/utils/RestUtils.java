package org.evolvis.eticket.business.facades.utils;

import java.util.GregorianCalendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.evolvis.eticket.util.CalculateHash;
import org.evolvis.eticket.util.LoadPropertiesFile;

@Path("/")
public class RestUtils{

	private static String projectName = null;
	
	@Path("util/hash")
	@GET
	@Produces("application/json")
	public String getHash(@QueryParam("string") String string) {
		return CalculateHash.calculate(string);
	}
	
	@Path("util/ping")
	@GET
	@Produces("application/json")
	public long ping() {
		return GregorianCalendar.getInstance().getTimeInMillis();
	}

    public static <T> T lookup(String className) {
            @SuppressWarnings("unchecked")
			T bean = (T) lookupObject("java:global/" + getProjectname() + "/"+className);
            return bean;
    }

    private static Object lookupObject(String jndiName) {
    	Context context = null;
        try {
            context = new InitialContext();
            return context.lookup(jndiName);
        } catch (NamingException ex) {
            throw new IllegalStateException("Cannot connect to bean: " + jndiName + " Reason: " + ex, ex.getCause());
        } finally {
            try {
                context.close();
            } catch (NamingException ex) {
                throw new IllegalStateException("Cannot close InitialContext. Reason: " + ex, ex.getCause());
            }
        }
    }

	private static String getProjectname() {
		if (projectName == null)
			projectName = (new LoadPropertiesFile()).open("project.properties").getProperty("PROJECT_NAME");
		return projectName;
		
	}
}
