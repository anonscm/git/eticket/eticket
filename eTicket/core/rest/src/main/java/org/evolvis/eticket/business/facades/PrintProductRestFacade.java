package org.evolvis.eticket.business.facades;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.facades.utils.RestUtils;
import org.evolvis.eticket.business.pdf.PrintProductPdfBeanService;
import org.evolvis.eticket.business.product.ProductBeanService;
import org.evolvis.eticket.business.product.ProductNumberBeanService;
import org.evolvis.eticket.business.product.ProductPoolBeanService;
import org.evolvis.eticket.business.system.SystemBeanService;
import org.evolvis.eticket.model.entity.PdfField;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductPoolDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;

@Path("/")
public class PrintProductRestFacade {

	private PrintProductPdfBeanService getPrintProductBean() {
		return RestUtils.lookup("PrintProductPdfBean");
	}
	

	private AccountBeanService getAccountBean() {
		return RestUtils.lookup("AccountBean");
	}

	private ProductPoolBeanService getProductPoolBean() {
		return RestUtils.lookup("ProductPoolBean");
	}

	private SystemBeanService getSystemBean() {
		return RestUtils.lookup("SystemBean");
	}
	
	private ProductNumberBeanService getProductNumberBean(){
		return RestUtils.lookup("ProductNumberBean");
	}
	
	private ProductBeanService getProductBeanService(){
		return RestUtils.lookup("ProductBean");
	}

	@GET
	@Produces({ "application/pdf" })
	@Path("get/pdf/{platformID}/{system}")
	public byte[] printPdf(
			@QueryParam("uin") String productUin,
			@QueryParam("user") String user, 
			@QueryParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system,
			@QueryParam("localecode") String localecode)
			throws IllegalAccessException {
		ESystemDTO eSystemDTO = getSystemBean().getSystemByName(system);
		AccountDTO caller = AccountDTO.initWithExistingHash(eSystemDTO, user, pass,
				RoleDTO.getUser(eSystemDTO.getId()),  null);
		getAccountBean().validatePassword(caller);
		List<ProductPoolDTO> poolDTOs = getProductPoolBean()
				.getProductPoolList("FIXED", user);
		
		TemplatesDTO templatesDTO = checkProductuin(poolDTOs, productUin,
				localecode);
		if (templatesDTO == null)
			throw new IllegalArgumentException("The User " + user
					+ " is not the ower of a Fixed Product with the Uin "
					+ productUin + " and the Localecode " + localecode);
		PersonalInformationDTO informationDTO = getAccountBean().getPersonalInformationDTO(eSystemDTO, user);		 
		String productnumber = getProductNumberBean().getProductNumber(getProductBeanService().get(eSystemDTO.getName(), productUin));
		return getPrintProductBean().createPDF(createMap(informationDTO), templatesDTO, productnumber,eSystemDTO);
	}	
	

	private TemplatesDTO checkProductuin(List<ProductPoolDTO> poolDTOs,
			String productUin, String localcode) {
		for (ProductPoolDTO productPoolDTO : poolDTOs) {

			// check if the product in the Pool have the same uin as the given
			// uin
			if (productPoolDTO.getProductDTO().getUin().equals(productUin)) {
				for (ProductLocaleDTO productLocaleDTO : productPoolDTO
						.getProductDTO().getProductLocales()) {

					// check if the localecode from the productLocale have the
					// same localecode as the given localecode
					if (productLocaleDTO.getLocale().getCode()
							.equals(localcode)) {
						return productLocaleDTO.getTemplate();
					}
				}
			}
		}
		return null;
	}

	private Map<PdfField, Object> createMap(PersonalInformationDTO personalInformationDTO) {
		
		if(!checkPersonalinformation(personalInformationDTO))
			throw new IllegalArgumentException("The Personal information are null");
		
		Map<PdfField, Object> values = new HashMap<PdfField, Object>();
		values.put(PdfField.FIRSTNAME, personalInformationDTO.getFirstname());
		values.put(PdfField.LASTNAME, personalInformationDTO.getLastname());
		values.put(PdfField.ADRESS, personalInformationDTO.getAddress());
		values.put(PdfField.ZIP, personalInformationDTO.getZipcode());
		values.put(PdfField.CITY, personalInformationDTO.getCity());
		values.put(PdfField.ORGA, personalInformationDTO.getOrganisation());
		values.put(PdfField.BARCODE, true);		
		return values;
	}
	
	private boolean checkPersonalinformation(PersonalInformationDTO personalInformationDTO){
		if(personalInformationDTO.getFirstname() == null)
			return false;		
		return true;
	}

}
