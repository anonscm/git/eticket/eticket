package org.evolvis.eticket.business.facades;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.evolvis.eticket.business.facades.utils.RestUtils;
import org.evolvis.eticket.business.system.ConfigurationParameterBeanService;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;

@Path("/")
public class ConfigurationParameterRestFacade {

	private ConfigurationParameterBeanService getCPBean() {
		return RestUtils.lookup("ConfigurationParameterBean");
	}

	@PUT
	@Path("insert/cp/{platformID}/{system}/{key}")
	public void createCP(@FormParam("user") String user,
			@FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system, @PathParam("key") String key,
			@FormParam("value") String value) {
		ESystemDTO eSystem = ESystemDTO.createNewESystemDTO(system, LocaleDTO.createNewLocaleDTO(
				"de", "don't care"), null);
		AccountDTO caller = AccountDTO.initWithExistingHash(eSystem, user, pass,
				RoleDTO.getSysAdmin(eSystem.getId()),  null);
		getCPBean().insertCP(caller, eSystem, CPKey.fromString(key), value);
	}

	@POST
	@Path("update/cp/{platformID}/{system}/{key}")
	public void setValue(@FormParam("user") String user,
			@FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system, @PathParam("key") String key,
			@FormParam("value") String value) {
		ESystemDTO eSystem = ESystemDTO.createNewESystemDTO(system, LocaleDTO.createNewLocaleDTO(
				"de", "don't care"), null);
		AccountDTO caller = AccountDTO.initWithExistingHash(eSystem, user, pass,
				RoleDTO.getSysAdmin(eSystem.getId()),  null);
		getCPBean().setValueOfCP(caller, eSystem, CPKey.fromString(key), value);
	}

	@DELETE
	@Path("delete/cp/{platformID}/{system}/{key}")
	public void deleteCP(@FormParam("user") String user,
			@FormParam("pass") String pass,
			@PathParam("platformID") long platformID,
			@PathParam("system") String system, @PathParam("key") String key) {
		ESystemDTO eSystem = ESystemDTO.createNewESystemDTO(system, LocaleDTO.createNewLocaleDTO(
				"de", "don't care"), null);
		AccountDTO caller = AccountDTO.initWithExistingHash(eSystem, user, pass,
				RoleDTO.getSysAdmin(eSystem.getId()),  null);
		getCPBean().deleteCP(caller, eSystem, CPKey.fromString(key));
	}

	@GET
	@Produces({ "application/json" })
	@Path("get/cp/{platformID}/{system}/{key}")
	public String getCP(@PathParam("platformID") long platformID,
			@PathParam("system") String system, @PathParam("key") String key) {
		ESystemDTO eSystem = ESystemDTO.createNewESystemDTO(system, LocaleDTO.createNewLocaleDTO(
				"de", "don't care"), null);
		return getCPBean().find(eSystem, key);
	}

}
