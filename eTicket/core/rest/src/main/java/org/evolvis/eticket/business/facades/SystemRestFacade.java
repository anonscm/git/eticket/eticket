//package org.evolvis.eticket.business.facades;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.ws.rs.DELETE;
//import javax.ws.rs.DefaultValue;
//import javax.ws.rs.FormParam;
//import javax.ws.rs.GET;
//import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
//import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
//import javax.ws.rs.Produces;
//import javax.ws.rs.QueryParam;
//
//import org.evolvis.eticket.business.facades.utils.RestUtils;
//import org.evolvis.eticket.business.system.SystemBeanService;
//import org.evolvis.eticket.model.entity.dto.AccountDTO;
//import org.evolvis.eticket.model.entity.dto.ESystemDTO;
//import org.evolvis.eticket.model.entity.dto.LocaleDTO;
//import org.evolvis.eticket.model.entity.dto.RoleDTO;
//
//@Path("/")
//public class SystemRestFacade {
//
//	private SystemBeanService getSystemBeanRemote() {
//		return RestUtils.lookup("SystemBean");
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see
//	 * org.evolvis.eticket.business.rest.SystemRestBeanLocal#createSystem(java
//	 * .lang.String, java.lang.String, long, java.lang.String, java.lang.String,
//	 * java.lang.String)
//	 */
//	@PUT
//	@Path("insert/system/{platformID}/{system}")
//	public void createSystem(@FormParam("user") String user,
//			@FormParam("pass") String pass,
//			@PathParam("platformID") long platformID,
//			@PathParam("system") String system,
//			@FormParam("defaultLocale") String localeCode,
//			@FormParam("supportedLocales") String supportedLocales) {
//		AccountDTO accountDTO = AccountDTO.initWithExistingHash(null, user, pass,
//						RoleDTO.getPlatformAdmin(), null);
//		String[] localeCodes = supportedLocales.split(",");
//		List<LocaleDTO> supported = new ArrayList<LocaleDTO>();
//		for (String loc : localeCodes)
//			supported.add(LocaleDTO.createNewLocaleDTO(loc, loc));
//		ESystemDTO eSystemDTO = ESystemDTO
//				.createNewESystemDTO(system,
//						LocaleDTO.createNewLocaleDTO(localeCode, localeCode),
//						supported);
//		getSystemBeanRemote().createSystem(accountDTO, platformID, eSystemDTO);
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see
//	 * org.evolvis.eticket.business.rest.SystemRestBeanLocal#updateSystem(java
//	 * .lang.String, java.lang.String, long, java.lang.String, java.lang.String,
//	 * java.lang.String, long)
//	 */
//	@POST
//	@Path("update/system/{platformID}/{system}")
//	public void updateSystem(@FormParam("user") String user,
//			@FormParam("pass") String pass,
//			@PathParam("platformID") long platformID,
//			@PathParam("system") String system,
//			@FormParam("defaultLocale") String localeCode,
//			@FormParam("supportedLocales") String supportedLocales,
//			@DefaultValue("-1") @FormParam("systemID") long id) {
//		AccountDTO accountDTO = AccountDTO
//				.initWithExistingHash(null, user, pass,
//						RoleDTO.getPlatformAdmin(), null);
//		String[] localeCodes = supportedLocales.split(",");
//		List<LocaleDTO> supported = new ArrayList<LocaleDTO>();
//		for (String loc : localeCodes)
//			supported.add(LocaleDTO.createNewLocaleDTO(loc, loc));
//		ESystemDTO eSystemDTO = ESystemDTO
//				.createESystemDTOFromExsistingESystemEntity(id, system,
//						LocaleDTO.createNewLocaleDTO(localeCode, localeCode),
//						supported);
//		getSystemBeanRemote().updateSystem(accountDTO, platformID, eSystemDTO);
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see
//	 * org.evolvis.eticket.business.rest.SystemRestBeanLocal#deleteSysten(java
//	 * .lang.String, java.lang.String, long, java.lang.String)
//	 */
//	@DELETE
//	@Path("delete/system/{platformID}/{system}")
//	public void deleteSysten(@QueryParam("user") String user,
//			@QueryParam("pass") String pass,
//			@PathParam("platformID") long platformID,
//			@PathParam("system") String system) {
//		AccountDTO accountDTO = AccountDTO.initWithExistingHash(null, user, pass,
//						RoleDTO.getPlatformAdmin(), null);
//		getSystemBeanRemote().deleteSystem(accountDTO, platformID,
//				getSystemByName(system));
//
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see
//	 * org.evolvis.eticket.business.rest.SystemRestBeanLocal#getSystemByName
//	 * (java.lang.String)
//	 */
//	@GET
//	@Produces({ "application/json" })
//	@Path("get/system/{system}")
//	public ESystemDTO getSystemByName(@PathParam("system") String system) {
//		return getSystemBeanRemote().getSystemByName(system);
//	}
//}
