package org.evolvis.eticketshop.model.entity.dto;

import java.io.Serializable;

import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticketshop.model.entity.ProductAssociation;

public class GtcShopDTO implements Serializable{
	
	private static final long serialVersionUID = -6382681486771193186L;
	private final long id;
	private final String value;
	private final ProductAssociation productAssociation;
	private final LocaleDTO localeDTO;
	
	private GtcShopDTO(long id, String value,
			ProductAssociation productAssociation, LocaleDTO  localeDTO) {
		super();
		this.id = id;
		this.value = value;
		this.productAssociation = productAssociation;
		this.localeDTO = localeDTO;
	}
	
	public static GtcShopDTO init(long id, String value,
			ProductAssociation productAssociation, LocaleDTO  localeDTO){
		return new GtcShopDTO(id, value, productAssociation, localeDTO);
	}
	
	/**
	 * Creates a new GtcShopDTO. This method is typically called from the frontend.
	 * 
	 * @param value
	 * @param productAssociation
	 * @return
	 */
	public static GtcShopDTO createNewGtcShopDTO(String value,
			ProductAssociation productAssociation,LocaleDTO locale){
		return new GtcShopDTO(-1, value, productAssociation,locale);
	}
	
	/**
	 * Creates GtcShopDTO from an existing GtcShop which is already stored in dbs. This method is typically called from the backend.
	 * @param id
	 * @param value
	 * @param productAssociation
	 * @return
	 */
	public static GtcShopDTO createGtcShopDTOFromExsistingGtcShopEntity(long id, String value,
			ProductAssociation productAssociation,LocaleDTO locale){
		return new GtcShopDTO(id, value, productAssociation,locale);
	}
	
	public long getId() {
		return id;
	}
	public String getValue() {
		return value;
	}
	public ProductAssociation getProductAssociation() {
		return productAssociation;
	}
	public LocaleDTO getLocaleDTO() {
		return localeDTO;
	}
	
}
