package org.evolvis.eticketshop.business.mail;

import java.util.Map;

import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticketshop.model.entity.TextTemplateNames;
import org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO;

public interface EMailBeanService {
	
	public void sendMail(ESystemDTO systemDTO, EMailAccountDTO account, ShopTextTemplateDTO templateDTO,
			Map<String, String> buzzwords, byte[] pdf);
	
	public Map<String, String> getBuzzwords(TextTemplateNames tempName, String...values);
	
}
