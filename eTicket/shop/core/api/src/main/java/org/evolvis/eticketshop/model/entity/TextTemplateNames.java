package org.evolvis.eticketshop.model.entity;

import java.util.HashMap;
import java.util.Map;

public enum TextTemplateNames {

	BOUGHT_PRODUCT_EXTERN("bought_product_extern", "productlink"), 
	BOUGHT_PRODUCT_INTERN("bought_product_intern"),
	PENDING_PRODUCT("pending_product"),
	RESEND_PRODUCT("pending_product");

	private String type;
	private String[] buzzwords;

	private TextTemplateNames(String type, String... buzzwords) {
		this.type = type;
		this.buzzwords = buzzwords;
	}

	public String[] getBuzzwords() {
		return buzzwords;
	}

	/** The Constant stringToEnum. */
	private static final Map<String, TextTemplateNames> stringToEnum = new HashMap<String, TextTemplateNames>();
	static {// initialise map from const name to enum const
		for (TextTemplateNames tP : values())
			stringToEnum.put(tP.toString(), tP);
	}

	public static TextTemplateNames fromString(String type) {
		return stringToEnum.get(type);
	}

	@Override
	public String toString() {
		return type;
	}

	public static TextTemplateNames fromInt(int size) {
		return values()[size - 1];
	}
}
