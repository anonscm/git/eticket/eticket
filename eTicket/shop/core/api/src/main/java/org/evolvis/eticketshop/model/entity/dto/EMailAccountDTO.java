package org.evolvis.eticketshop.model.entity.dto;

import java.io.Serializable;

public final class EMailAccountDTO implements Serializable{
	
	private static final long serialVersionUID = 4518565763313508794L;
	private final String email;
	private final String name;

	private EMailAccountDTO(String email, String name) {
		super();
		this.email = email;
		this.name = name;
	}
	
	public static EMailAccountDTO init(String email, String name){
		return new EMailAccountDTO(email, name);
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}
}
