package org.evolvis.eticketshop.model.entity;

import java.util.HashMap;
import java.util.Map;

public enum PaymentMethod {
	
	PAYPAL("PayPal");

	private String type;

	PaymentMethod(String type) {
		this.type = type;

	}

	private static final Map<String, PaymentMethod> stringToEnum = new HashMap<String, PaymentMethod>();
	static {// initialise map from const name to enum const
		for (PaymentMethod tP : values())
			stringToEnum.put(tP.toString(), tP);
	}

	public static PaymentMethod fromString(String type) {
		return stringToEnum.get(type);
	}

	@Override
	public String toString() {
		return type;
	}

	public static PaymentMethod fromInt(int size) {
		return values()[size - 1];
	}
}
