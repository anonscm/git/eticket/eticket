package org.evolvis.eticketshop.model.entity.dto;

import java.io.Serializable;

import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticketshop.model.entity.TextTemplateNames;

public final class ShopTextTemplateDTO implements Serializable {

	private static final long serialVersionUID = -2961916918341551349L;

	private final ESystemDTO systemDTO;

	private final LocaleDTO localeDTO;

	private final String name;

	private final String descripption;

	private final String text;

	private volatile int hashcode;

	private ShopTextTemplateDTO(ESystemDTO systemDTO, LocaleDTO localeDTO,
			String name, String text, String description) {
		if (systemDTO == null || localeDTO == null || name == null)
			throw new IllegalArgumentException(
					"systemDTO, localeDTO or name must not be null.");
		this.systemDTO = systemDTO;
		this.localeDTO = localeDTO;
		this.name = name;
		this.text = text;
		this.descripption = description;
	}

	/**
	 * Creates a new TextTemplateDTO. This method is typically called from the frontend.
	 * 
	 * @param systemDTO
	 * @param localeDTO
	 * @param name
	 * @param text
	 * @return
	 */
	public static ShopTextTemplateDTO createNewTextTemplateDTO(
			ESystemDTO systemDTO, LocaleDTO localeDTO, TextTemplateNames name,
			String text) {
		return new ShopTextTemplateDTO(systemDTO, localeDTO, name.toString(), text,
				null);
	}

	/**
	 * Creates a new TextTemplateDTO. This method is typically called from the frontend.
	 * 
	 * @param systemDTO
	 * @param localeDTO
	 * @param name
	 * @param text
	 * @return
	 */
	public static ShopTextTemplateDTO createNewTextTemplateDTO(
			ESystemDTO systemDTO, LocaleDTO localeDTO, String name, String text) {
		return new ShopTextTemplateDTO(systemDTO, localeDTO, name, text, null);
	}

	/**
	 * Creates a new TextTemplateDTO. This method is typically called from the frontend.
	 * 
	 * @param systemDTO
	 * @param localeDTO
	 * @param name
	 * @param text
	 * @param desc
	 * @return
	 */
	public static ShopTextTemplateDTO createNewTextTemplateDTO(
			ESystemDTO systemDTO, LocaleDTO localeDTO, TextTemplateNames name,
			String text, String desc) {
		return new ShopTextTemplateDTO(systemDTO, localeDTO, name.toString(), text,
				desc);
	}

	/**
	 * Creates TextTemplateDTO from an existing TextTemplate which is already stored in dbs. This method is typically called from the backend.
	 * 
	 * @param systemDTO
	 * @param localeDTO
	 * @param name
	 * @param text
	 * @param desc
	 * @return
	 */
	public static ShopTextTemplateDTO createTextTemplateDTOFromExsistingTextTemplateEntity(
			ESystemDTO systemDTO, LocaleDTO localeDTO, String name,
			String text, String desc) {
		return new ShopTextTemplateDTO(systemDTO, localeDTO, name, text, desc);
	}

	public ESystemDTO getSystemDTO() {
		return systemDTO;
	}

	public LocaleDTO getLocaleDTO() {
		return localeDTO;
	}

	public String getName() {
		return name;
	}

	public String getDescripption() {
		return descripption;
	}

	public String getText() {
		return text;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ShopTextTemplateDTO))
			return false;
		ShopTextTemplateDTO ttD = (ShopTextTemplateDTO) obj;
		return ttD.getSystemDTO().equals(getSystemDTO())
				&& ttD.getLocaleDTO().equals(getLocaleDTO())
				&& ttD.getName().equals(getName());
	}

	@Override
	public int hashCode() {
		int result = hashcode;
		if (hashcode == 0) {
			result = 17;
			result = 31 * result + getSystemDTO().hashCode();
			result = 31 * result + getLocaleDTO().hashCode();
			result = 31 * result + getName().hashCode();
		}
		return result;
	}

}
