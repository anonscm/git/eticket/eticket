package org.evolvis.eticketshop.model.entity.dto;

import java.io.Serializable;

import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticketshop.model.entity.ProductAssociation;

/**
 * This is data transfer object for ShopProduct.
 * 
 * ShopProduct is an extension for Product
 * 
 * @author ben
 * 
 */
public final class ShopProductDTO implements Serializable {

	private static final long serialVersionUID = 4554866957809547904L;

	private final ProductDTO product;
	// available = ture if the product should be shown in shop, otherwise false.
	private final boolean available;

	private final ProductAssociation productAssociation;
	
	private final long id;

	/**
	 * Creates a new ShopProductDTO. This method is typically called from the
	 * frontend.
	 * 
	 * @param product
	 *            the product with which it is associated. Must not be null.
	 * @param available
	 *            true if ShopProduct is available in shop, or false if not
	 *            available in shop. Must not be null.
	 * @return a single instance of ShopProductDTO.
	 * @throws IllegalArgumentException
	 *             when product is null.
	 * @throws NullPointerException
	 *             when available is null
	 */
	public static ShopProductDTO createNewShopProductDTO(ProductDTO product,
			boolean available, ProductAssociation productAssociation) {
		return new ShopProductDTO(-1, product, new Boolean(available),
				productAssociation);
	}

	/**
	 * Creates ShopProductDTO from an existing ShopProduct which is already
	 * stored in dbs. This method is typically called from the backend.
	 * 
	 * @param product
	 *            the product with which it is associated. Must not be null.
	 * @param available
	 *            true if ShopProduct is available in shop, or false if not
	 *            available in shop. Must not be null.
	 * @return a single instance of ShopProductDTO.
	 * @throws IllegalArgumentException
	 *             when product is null.
	 * @throws NullPointerException
	 *             when available is null
	 */
	public static ShopProductDTO createShopProductDTOFromExsistingShopProductEntity(long id,
			ProductDTO product, boolean available,
			ProductAssociation productAssociation) {

		return new ShopProductDTO(id, product, new Boolean(available),
				productAssociation);

	}

	private ShopProductDTO(long id, ProductDTO product, Boolean available,
			ProductAssociation productAssociation) {
		if (product == null || available == null) {
			throw new IllegalArgumentException(
					"product, available must be set.");
		}

		this.available = available.booleanValue();

		this.product = product;
		this.productAssociation = productAssociation;
		this.id=id;
	}

	public ProductDTO getProduct() {
		return product;
	}

	public boolean isAvailable() {
		return available;
	}

	public ProductAssociation getProductAssociation() {
		return productAssociation;
	}

	public long getId() {
		return id;
	}

}
