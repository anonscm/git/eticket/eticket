package org.evolvis.eticket.business.template;

import java.io.Serializable;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO;

public interface ShopTextTemplateBeanService extends Serializable{

	public void insert(AccountDTO caller, ShopTextTemplateDTO textTemplate);
	
	public void update(AccountDTO caller, ShopTextTemplateDTO textTemplate);
	
	public void delete(AccountDTO caller, ShopTextTemplateDTO textTemplate);

	public ShopTextTemplateDTO find(ESystemDTO eSystem, LocaleDTO locale, String name);

}
