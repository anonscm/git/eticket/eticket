package org.evolvis.eticketshop.business.gtc;

import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.dto.GtcShopDTO;

public interface GtcShopBeanService {
	
	
	public GtcShopDTO findByProductAssociation(ProductAssociation productAssociation, LocaleDTO localeDto );
	public void createGtcShop(GtcShopDTO gtcShopDTO);
	public void updateGtcShop(GtcShopDTO gtcShopDTO);
	public void deleteGtcShop(GtcShopDTO gtcShopDTO);

}
