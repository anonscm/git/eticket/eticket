package org.evolvis.eticketshop.model.entity;

import java.util.HashMap;
import java.util.Map;

public enum ProductAssociation {

	SHOP_INTERN("SHOP_INTERN"), SHOP_EXTERN("SHOP_EXTERN");

	private String type;

	ProductAssociation(String type) {
		this.type = type;

	}

	private static final Map<String, ProductAssociation> stringToEnum = new HashMap<String, ProductAssociation>();
	static {// initialise map from const name to enum const
		for (ProductAssociation tP : values())
			stringToEnum.put(tP.toString(), tP);
	}

	public static ProductAssociation fromString(String type) {
		return stringToEnum.get(type);
	}

	@Override
	public String toString() {
		return type;
	}

	public static ProductAssociation fromInt(int size) {
		return values()[size - 1];
	}
}