package org.evolvis.eticketshop.business.product;

import java.util.List;

import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductStatisticDTO;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;

/**
 * ShopProductBeanService handels the communication between view and ShopProductCRUD.
 * 
 * @author ben
 */

public interface ShopProductBeanService { 
    /**
     * gets all shopproducts in a system
     * 
     * @param systemName
     *            name of the system in which the product exists.
     * 
     * @param available
     *            available or unavailable products
     *            
     * @return a list of all active ShopProductDTO in a system.
     */
    public List<ShopProductDTO> getAllShopProductDTOFromSystem(String systemName, boolean available);
    
    /**
     * gets a specific ShopProductDTO by system and product uin
     * 
     * @param product_uin
     * @param eSystemDTO
     * @return ShopProductDTO
     */
    public ShopProductDTO getShopProcutDTOByProductUinAndProductSystem(String product_uin, ESystemDTO eSystemDTO);
    
    
    /**
     * gets all shopproducts from a shop
     * @param available
     * 			available or unavailable products
     * @param productAssociation
     * 			shopProducts for the internal or external Shop
     * @return a list of all active ShopProductDTO.
     */
    public List<ShopProductDTO> getAllShopProductDTOFromShop(boolean available, ProductAssociation productAssociation);
    
    /**
     * gets all shopproducts from a shop
     * @param available
     * 			available or unavailable products
     * @param productAssociation
     * 			shopProducts for the internal or external Shop
     * @param system
     * @return a list of all active ShopProductDTO.
     */
    public List<ShopProductDTO> getAllShopProductDTOFromShop(boolean available, ProductAssociation productAssociation, ESystemDTO eSystemDTO);
    
    public void createShopProduct(ShopProductDTO shopproduct);
	public void updateShopProduct(ShopProductDTO shopProduct);
	public void deleteShopProduct(ShopProductDTO shopProduct);

	public ProductStatisticDTO getStatistic(ProductDTO productdto); 
}
