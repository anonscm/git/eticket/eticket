package org.evolvis.eticketshop.model.entity.dto;

import junit.framework.Assert;

import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.junit.Test;

public class ShopProductDTOTest {
	@Test(expected = NullPointerException.class)
	public void shouldThrowExceptionIfBothAreNull() {
		ShopProductDTO.createNewShopProductDTO(null, (Boolean) null,
				ProductAssociation.SHOP_INTERN);
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowExceptionIfAvailableIsNull() {


		ShopProductDTO.createNewShopProductDTO(ProductDTO.createNewProductDTO("a", ProductType.DIGITAL,
				ESystemDTO.createNewESystemDTO("ha", LocaleDTO.createNewLocaleDTO("de", "deutsch"), null),
				null, 0,0), (Boolean) null, ProductAssociation.SHOP_INTERN);

	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionIfProductIsNull() {
		ShopProductDTO.createNewShopProductDTO(null, true, ProductAssociation.SHOP_EXTERN);
	}

	@Test
	public void shouldThrowNoException() {
		ShopProductDTO spdto = null;
		spdto = ShopProductDTO.createNewShopProductDTO(ProductDTO.createNewProductDTO("a", ProductType.DIGITAL,
				ESystemDTO.createNewESystemDTO("ha", LocaleDTO.createNewLocaleDTO("de", "deutsch"), null),
				null,0, 0), true, ProductAssociation.SHOP_INTERN);

		Assert.assertNotNull(spdto);
	}

}
