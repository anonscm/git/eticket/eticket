package org.evolvis.eticketshop.model.crud.template;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticketshop.model.entity.ShopTextTemplate;

// TODO: Auto-generated Javadoc
/**
 * The Class RoleCRUDBean.
 */
@Stateless
@Local(ShopTextTemplateCRUD.class)
public class ShopTextTemplateCRUDBean extends GenericCUDSkeleton<ShopTextTemplate> implements ShopTextTemplateCRUD {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 966179077108817257L;

	/* (non-Javadoc)
	 * @see org.evolvis.model.crud.RoleCRUDBeanLocal#findBySystemAndName(org.evolvis.model.entity.ESystem, java.lang.String)
	 */
	@Override
	public ShopTextTemplate findBySystemLocaleAndName(ESystem system, Locale locale, String name) {
		return buildNamedQueryGetSingleResult("findShopTextTemplateBySystemLocaleName", system, locale, name);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.model.crud.RoleCRUDBeanLocal#listSystem(org.evolvis.model.entity.ESystem)
	 */
	@Override
	public List<ShopTextTemplate> listSystemLocale(ESystem system, Locale locale) {
		return buildNamedQueryGetResults("listShopTextTemplateInSystemLocale", system, locale);
	}



}
