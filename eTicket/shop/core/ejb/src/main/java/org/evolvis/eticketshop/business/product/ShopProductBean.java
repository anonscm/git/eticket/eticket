package org.evolvis.eticketshop.business.product;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.product.ProductCRUD;
import org.evolvis.eticket.model.crud.product.ProductPoolCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.ProductPool;
import org.evolvis.eticket.model.entity.ProductPool.ProductPoolStatus;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductStatisticDTO;
import org.evolvis.eticketshop.model.crud.product.ShopProductCRUD;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.ShopProduct;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;

// TODO: Auto-generated Javadoc
/**
 * ShopProductBean handels the communication between view and ShopProductCRUD.
 * 
 * @author ben
 */
@Stateless
@Remote(ShopProductBeanService.class)
public class ShopProductBean implements ShopProductBeanService {
	
	/** The product pool crud bean local. */
	@EJB
	private ProductPoolCRUD productPoolCRUDBeanLocal;

	/** The shop product crud bean local. */
	@EJB
	private ShopProductCRUD shopProductCRUDBeanLocal;

	/** The product crud bean local. */
	@EJB
	private ProductCRUD productCRUDBeanLocal;

	/** The e system crud bean local. */
	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;

	/**
	 * Sets the product pool crud bean local.
	 *
	 * @param productPoolCRUDBeanLocal the new product pool crud bean local
	 */
	public void setProductPoolCRUDBeanLocal(
			ProductPoolCRUD productPoolCRUDBeanLocal) {
		this.productPoolCRUDBeanLocal = productPoolCRUDBeanLocal;
	}

	/**
	 * Sets the e system crud bean local.
	 *
	 * @param eSystemCRUDBeanLocal the new e system crud bean local
	 */
	public void seteSystemCRUDBeanLocal(ESystemCRUD eSystemCRUDBeanLocal) {
		this.eSystemCRUDBeanLocal = eSystemCRUDBeanLocal;
	}

	/**
	 * Sets the shop product crud bean local.
	 *
	 * @param shopProductCRUDBeanLocal the new shop product crud bean local
	 */
	public void setShopProductCRUDBeanLocal(
			ShopProductCRUD shopProductCRUDBeanLocal) {
		this.shopProductCRUDBeanLocal = shopProductCRUDBeanLocal;
	}

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.business.product.ShopProductBeanService#getAllShopProductDTOFromSystem(java.lang.String, boolean)
	 */
	@Override
	public List<ShopProductDTO> getAllShopProductDTOFromSystem(
			String systemName, boolean available) {
		ESystem system = eSystemCRUDBeanLocal.findByName(systemName);
		List<ShopProductDTO> result = new LinkedList<ShopProductDTO>();
		List<ShopProduct> shopProductList = shopProductCRUDBeanLocal
				.findShopProductsInSystem(system, available);

		for (ShopProduct e : shopProductList) {
			result.add(e.toDto());
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.business.product.ShopProductBeanService#getShopProcutDTOByProductUinAndProductSystem(java.lang.String, org.evolvis.eticket.model.entity.dto.ESystemDTO)
	 */
	@Override
	public ShopProductDTO getShopProcutDTOByProductUinAndProductSystem(
			String product_uin, ESystemDTO eSystemDTO) {
		ESystem system = eSystemCRUDBeanLocal.findById(eSystemDTO.getId());
		return shopProductCRUDBeanLocal.findByProductUinAndProductSystem(
				product_uin, system).toDto();
	}

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.business.product.ShopProductBeanService#createShopProduct(org.evolvis.eticketshop.model.entity.dto.ShopProductDTO)
	 */
	@Override
	// This method needs an existing product
	public void createShopProduct(ShopProductDTO shopproduct) {
		ShopProduct sproduct = new ShopProduct();
		ESystem system = eSystemCRUDBeanLocal.findByName(shopproduct
				.getProduct().getSystem().getName());
		Product product = productCRUDBeanLocal.findBySystemAndUin(system,
				shopproduct.getProduct().getUin());
		sproduct.fromDto(shopproduct);
		sproduct.setProduct(product);
		shopProductCRUDBeanLocal.insert(sproduct);
	}
	
	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.business.product.ShopProductBeanService#getAllShopProductDTOFromShop(boolean, org.evolvis.eticketshop.model.entity.ProductAssociation, org.evolvis.eticket.model.entity.dto.ESystemDTO)
	 */
	@Override
	public List<ShopProductDTO> getAllShopProductDTOFromShop(boolean available,
			ProductAssociation productAssociation, ESystemDTO eSystemDTO) {
		List<ShopProductDTO> shopProductDTOs = new ArrayList<ShopProductDTO>();
		List<ShopProduct> result = shopProductCRUDBeanLocal.findShopProductsBySystemAndProductAssociation(
				available, productAssociation, new ESystem(eSystemDTO));

		for (ShopProduct shopProduct : result) {
			shopProductDTOs.add(shopProduct.toDto());
		}
		return shopProductDTOs;
	}

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.business.product.ShopProductBeanService#getAllShopProductDTOFromShop(boolean, org.evolvis.eticketshop.model.entity.ProductAssociation)
	 */
	@Override
	public List<ShopProductDTO> getAllShopProductDTOFromShop(boolean available,
			ProductAssociation productAssociation) {
		List<ShopProductDTO> shopProductDTOs = new ArrayList<ShopProductDTO>();
		List<ShopProduct> result = shopProductCRUDBeanLocal.findShopProductsByProductAssociation(
				available, productAssociation);

		for (ShopProduct shopProduct : result) {
			shopProductDTOs.add(shopProduct.toDto());
		}
		return shopProductDTOs;
	}

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.business.product.ShopProductBeanService#updateShopProduct(org.evolvis.eticketshop.model.entity.dto.ShopProductDTO)
	 */
	@Override
	public void updateShopProduct(ShopProductDTO shopProduct) {
		ShopProduct product = getShopProduct(shopProduct);
		Product updatedProduct = new Product(shopProduct.getProduct());
		productCRUDBeanLocal.update(updatedProduct);
		product.setAvaible(shopProduct.isAvailable());
		product.setProductAssociation(shopProduct.getProductAssociation());
		shopProductCRUDBeanLocal.update(product);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.business.product.ShopProductBeanService#deleteShopProduct(org.evolvis.eticketshop.model.entity.dto.ShopProductDTO)
	 */
	@Override
	public void deleteShopProduct(ShopProductDTO shopProduct) {
		ShopProduct product = getShopProduct(shopProduct);
		shopProductCRUDBeanLocal.delete(product);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.business.product.ShopProductBeanService#getStatistic(org.evolvis.eticket.model.entity.dto.ProductDTO)
	 */
	@Override
	public ProductStatisticDTO getStatistic(ProductDTO productdto) {
		ESystem system = getSystem(productdto);
		Product product = productCRUDBeanLocal.findBySystemAndUin(system,
				productdto.getUin());
		List<ProductPool> pools = productPoolCRUDBeanLocal
				.findByproduct(product);
		long invintations = 0;
		long acceptedinvintations = 0;
		long fixedinvintations = 0;
		for (int i = 0; i < pools.size(); i++) {
			if (!(pools.get(i).getActor() == null)) {
				if (pools.get(i).getType() == ProductPoolStatus.REGISTERED) {
					invintations = invintations + pools.get(i).getAmount();
				}
				if (pools.get(i).getType() == ProductPoolStatus.VOLATILE) {
					acceptedinvintations = acceptedinvintations
							+ pools.get(i).getAmount();
				}
				if (pools.get(i).getType() == ProductPoolStatus.FIXED) {
					fixedinvintations = fixedinvintations
							+ pools.get(i).getAmount();
				}
			}
		}
		ShopProduct sproduct = null;
		sproduct = shopProductCRUDBeanLocal.findByProductUinAndProductSystem(
				product.getUin(), product.getSystem());
		ProductStatisticDTO statistic = null;
		if (sproduct == null) {
			statistic = ProductStatisticDTO.createIntern(productdto,
					Long.toString(invintations),
					Long.toString(acceptedinvintations),
					Long.toString(fixedinvintations));
		} else {
			statistic = ProductStatisticDTO.init(productdto, Long
					.toString(invintations), Long
					.toString(acceptedinvintations), Long
					.toString(fixedinvintations), sproduct
					.getProductAssociation().toString());
		}

		return statistic;
	}

	/**
	 * Gets the system.
	 *
	 * @param productDTO the product dto
	 * @return the system
	 */
	private ESystem getSystem(ProductDTO productDTO) {
		ESystem eSystem = eSystemCRUDBeanLocal.findByName(productDTO
				.getSystem().getName());
		return eSystem;
	}

	/**
	 * Gets the shop product.
	 *
	 * @param shopProductDTO the shop product dto
	 * @return the shop product
	 */
	private ShopProduct getShopProduct(ShopProductDTO shopProductDTO) {
		ESystem system = eSystemCRUDBeanLocal.findByName(shopProductDTO
				.getProduct().getSystem().getName());
		return shopProductCRUDBeanLocal.findByProductUinAndProductSystem(
				shopProductDTO.getProduct().getUin(), system);
	}



}
