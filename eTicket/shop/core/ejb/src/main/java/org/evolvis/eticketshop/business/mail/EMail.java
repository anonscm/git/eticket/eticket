package org.evolvis.eticketshop.business.mail;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.evolvis.eticket.util.LoadPropertiesFile;
import org.evolvis.eticketshop.model.entity.ShopTextTemplate;
import org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO;

/**
 * The Class EMail.
 */
public class EMail implements Communicate {

	/** The Constant USER. */
	private static final String USER = "USER";

	/** The Constant PASSWORD. */
	private static final String PASSWORD = "PASSWORD";

	/** The Constant PORT. */
	private static final String PORT = "PORT";

	/** The Constant HOST. */
	private static final String HOST = "HOST";

	/** The Constant EMAIL_PROPERTIES. */
	private static final String EMAIL_PROPERTIES = "email.properties";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.evolvis.eticketshop.business.mail.Communicate#sendTo(org.evolvis.
	 * eticketshop.model.entity.dto.EMailAccountDTO,
	 * org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO,
	 * org.evolvis.eticketshop.model.entity.ShopTextTemplate, java.util.Map,
	 * byte[])
	 */
	@Override
	public void sendTo(EMailAccountDTO sender, EMailAccountDTO recipient,
			ShopTextTemplate template, Map<String, String> buzzwordsAndValues,
			byte[] pdf) {
		
		String fulltext = generateText(template, buzzwordsAndValues);
		String subject = getSubject(fulltext);
		String[] text = fulltext.split("\n", 2);
		
		try {
			sendMail(sender, recipient, subject, text[1], pdf);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("An error occured while sending email.",
					e);
		}

	}

	/**
	 * Send mail.
	 * 
	 * @param sender
	 *            the sender
	 * @param recipient
	 *            the recipient
	 * @param subject
	 *            the subject
	 * @param text
	 *            the text
	 * @param pdf
	 *            the pdf
	 * @throws MessagingException
	 *             the messaging exception
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 */
	private void sendMail(EMailAccountDTO sender, EMailAccountDTO recipient,
			String subject, String text, byte[] pdf) throws MessagingException,
			UnsupportedEncodingException {
		Session session = initSession();
		Transport transport = session.getTransport();
		MimeMessage message = createMsg(sender, recipient, subject, text,
				session, pdf);
		send(transport, message);
	}

	/**
	 * Connect to the EMail provider and send the EMail
	 * 
	 * @param transport
	 *            the transport
	 * @param message
	 *            the message
	 * @throws MessagingException
	 *             the messaging exception
	 */
	private void send(Transport transport, MimeMessage message)
			throws MessagingException {
		transport.connect();
		transport.sendMessage(message,
				message.getRecipients(Message.RecipientType.TO));
		transport.close();
	}

	/**
	 * Creates the message in the EMail and add the pdf file.
	 * 
	 * @param sender
	 *            the sender
	 * @param recipient
	 *            the recipient
	 * @param subject
	 *            the subject
	 * @param text
	 *            the text
	 * @param session
	 *            the session
	 * @param pdf
	 *            the pdf
	 * @return the mime message
	 * @throws MessagingException
	 *             the messaging exception
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 */
	private MimeMessage createMsg(EMailAccountDTO sender,
			EMailAccountDTO recipient, String subject, String text,
			Session session, byte[] pdf) throws MessagingException,
			UnsupportedEncodingException {
		MimeMessage message = new MimeMessage(session);
		message.setSubject(subject);
		message.setContent(addBody(text, pdf));

		message.setFrom(new InternetAddress(sender.getEmail(), sender.getName()));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(
				recipient.getEmail(), recipient.getName()));
		return message;
	}

	/**
	 * Adds the body.
	 * 
	 * @param text
	 *            the text
	 * @param pdf
	 *            the pdf
	 * @return the multipart
	 * @throws MessagingException
	 *             the messaging exception
	 */
	private Multipart addBody(String text, byte[] pdf)
			throws MessagingException {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setHeader("Content-Type", "text/plain; charset=utf-8");
		messageBodyPart.setText(text, "utf-8");
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		if (pdf != null) {
			messageBodyPart = new MimeBodyPart();		
			messageBodyPart.setDataHandler(new DataHandler(pdf,
					"application/pdf"));
			messageBodyPart.setFileName("rechnung.pdf");
			multipart.addBodyPart(messageBodyPart);
		}
		return multipart;
	}

	/**
	 * Inits the session.
	 * 
	 * @return the session
	 * @throws NoSuchProviderException
	 *             the no such provider exception
	 */
	private Session initSession() throws NoSuchProviderException {
		Properties props = new Properties();
		Properties properties = getProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", properties.getProperty(HOST));
		props.put("mail.smtp.port", properties.getProperty(PORT));
		String user = properties.getProperty(USER);
		String pass = properties.getProperty(PASSWORD);
		Session mailSession = setAuth(props, user, pass);
		return mailSession;
	}

	/**
	 * Sets the auth.
	 * 
	 * @param props
	 *            the props
	 * @param user
	 *            the user
	 * @param pass
	 *            the pass
	 * @return the session
	 */
	private Session setAuth(Properties props, final String user,
			final String pass) {
		if (user != null) {
			props.put("mail.smtp.auth", "true");
			Authenticator auth = new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(user, pass);
				}
			};
			return Session.getInstance(props, auth);
		}
		return Session.getInstance(props);
	}

	/**
	 * Gets the properties.
	 * 
	 * @return the properties
	 */
	private Properties getProperties() {
		Properties properties;
		try {
			properties = (new LoadPropertiesFile()).open(EMAIL_PROPERTIES);
		} catch (Exception e) {
			properties = new Properties();
			properties.put(HOST, "localhost");
			properties.put(PORT, "25");
		}
		return properties;
	}

	/**
	 * Gets the subject.
	 * 
	 * @param template
	 *            the template
	 * @return the subject
	 */
	public String getSubject(String template) {
		int firstLineBreak = template.indexOf('\n');
		return template.substring(0, firstLineBreak);
	}

	/**
	 * Generate text.
	 * 
	 * @param template
	 *            the template
	 * @param buzzwordsAndValues
	 *            the buzzwords and values
	 * @return the string
	 */
	private String generateText(ShopTextTemplate template,
			Map<String, String> buzzwordsAndValues) {
		String text = template.getText();
		if (buzzwordsAndValues != null) {
			Iterator<Map.Entry<String, String>> iter = buzzwordsAndValues
					.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<String, String> entry = iter.next();
				text = text.replaceAll("%" + entry.getKey() + "%",
						encodeHtml(entry.getValue()));
			}
		}
		return text;
	}

	/**
	 * Encode html.
	 * 
	 * @param in
	 *            the in
	 * @return the string
	 */
	private String encodeHtml(String in) {
		if (in != null)
			return in.replaceAll("&auml;", "ä").replaceAll("&uuml;", "ü")
					.replaceAll("&ouml;", "ö").replaceAll("&Auml;", "Ä")
					.replaceAll("&Uuml;", "Ü").replaceAll("&Ouml;", "Ö")
					.replaceAll("&szlig;", "ß");
		else
			return null;
	}
}