package org.evolvis.eticketshop.business.mail;

import java.util.HashMap;
import java.util.Map;

import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticketshop.model.entity.ShopTextTemplate;
import org.evolvis.eticketshop.model.entity.TextTemplateNames;
import org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO;
import org.evolvis.eticketshop.util.GenericServiceLoader;

/**
 * The Class CommunicateUtil that calls the eMail class.
 */
public class CommunicateUtil {

	/** The Constant EMAIL. */
	private static final Class<? extends Communicate> EMAIL = EMail.class;

	/**
	 * Send mail to the account.
	 *
	 * @param configurationParameterBeanLocal the configuration parameter bean local
	 * @param system the system
	 * @param account the account
	 * @param template the template
	 * @param buzzwords the buzzwords
	 * @param pdf the pdf
	 */
	public static void sendMail(
			ConfigurationParameterCRUD configurationParameterBeanLocal,
			ESystem system, EMailAccountDTO account, ShopTextTemplate template,
			Map<String, String> buzzwords, byte[] pdf) {
		send(configurationParameterBeanLocal, system, account, template,
				buzzwords, EMAIL, pdf);
	}

	/**
	 * Send.
	 *
	 * @param configurationParameterBeanLocal the configuration parameter bean local
	 * @param system the system
	 * @param account the account
	 * @param template the template
	 * @param buzzwords the buzzwords
	 * @param impl the impl
	 * @param pdf the pdf
	 */
	private static void send(
			ConfigurationParameterCRUD configurationParameterBeanLocal,
			ESystem system, EMailAccountDTO account, final ShopTextTemplate template,
			Map<String, String> buzzwords, Class<?> impl, byte[] pdf) {
		Communicate send = GenericServiceLoader.get(Communicate.class, impl);
		EMailAccountDTO systemAccount = getSystemAccount(system,
				configurationParameterBeanLocal);
		send.sendTo(systemAccount, account, template, buzzwords, pdf);
	}

	/**
	 * Gets the system account, the account is the sender of the EMail.
	 *
	 * @param eSystem the e system
	 * @param configurationParameterBeanLocal the configuration parameter bean local
	 * @return the system account
	 */
	private static EMailAccountDTO getSystemAccount(ESystem eSystem,
			ConfigurationParameterCRUD configurationParameterBeanLocal) {
		String name = configurationParameterBeanLocal.findBySystemAndKey(
				eSystem, CPKey.SYSTEM_SENDER.toString()).getValue();
		EMailAccountDTO result = EMailAccountDTO.init(name, null);
		return result;
	}

	/**
	 * Gets the buzzwords, this words will be replaced in the EMail e.g. a Link to the pdf file.
	 *
	 * @param tempName the temp name
	 * @param values the values
	 * @return the buzzwords
	 */
	public static Map<String, String> getBuzzwords(TextTemplateNames tempName, String...values){
		return generateMap(tempName.getBuzzwords(), values);
	}

	/**
	 * Generate map.
	 *
	 * @param keys the keys
	 * @param values the values
	 * @return the map
	 */
	private static Map<String, String> generateMap(String[] keys,
			String[] values) {
		if (keys.length != values.length)
			throw new IllegalArgumentException(
					"The length of the keys differs the length of the objects.");
		final Map<String, String> result = new HashMap<String, String>();
		for (int i = 0; i < keys.length; i++)
			result.put(keys[i], values[i]);
		return result;
	}

}
