package org.evolvis.eticketshop.business.gtc;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;

import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticketshop.model.crud.gtc.GtcShopCRUD;
import org.evolvis.eticketshop.model.entity.GtcShop;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.dto.GtcShopDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class GtcShopBean.
 */
@Stateless
@Remote(GtcShopBeanService.class)
public class GtcShopBean implements GtcShopBeanService{
	
	/** The local interface {@link GtcShopCRUD}. */
	@EJB
	private GtcShopCRUD gtcShopCRUDLocal;
	
	/** The Remote interface {@link LocaleCRUD}. */
	@EJB
	private LocaleCRUD localeCRUDRemote;

	/**
	 * Finds a GtcShop Object by the {@link ProductAssociation} and the LocaleDTO.
	 *
	 * @param productAssociation the productassociation
	 * @param localeDto the locale dto
	 * @return GtcShopDTO, the data transfer object of GtcShop
	 */
	@Override
	@ExcludeClassInterceptors
	public GtcShopDTO findByProductAssociation(
			ProductAssociation productAssociation, LocaleDTO localeDto ) {
		Locale locale = localeCRUDRemote.findByCode(localeDto.getCode());
		return gtcShopCRUDLocal.findByProductAssociation(productAssociation, locale).toDto();
	}

	/**
	 * Creates a new GtcShop object in the database. The caller must be sysadmin
	 *
	 * @param gtcShopDTO the gtcshopdto that will be created.
	 */
	@Override
	public void createGtcShop(GtcShopDTO gtcShopDTO) {
		GtcShop gtcShop = new GtcShop();
		gtcShop.fromDto(gtcShopDTO);
		Locale locale = localeCRUDRemote.findByCode(gtcShopDTO.getLocaleDTO().getCode());
		gtcShop.setLocale(locale);
		gtcShopCRUDLocal.insert(gtcShop);
	}

	/**
	 * Update a GtcShop object in the database. The caller must be sysadmin
	 *
	 * @param gtcShopDTO the gtcshopdto that will be updated
	 */
	@Override
	public void updateGtcShop(GtcShopDTO gtcShopDTO) {
		GtcShop gtcShop = new GtcShop();
		gtcShop.fromDto(gtcShopDTO);
		gtcShopCRUDLocal.update(gtcShop);
	}

	/**
	 * Delete a GtcShop object in the database. The caller must be sysadmin
	 *
	 * @param gtcShopDTO the gtcshopdto that will be deleted.
	 */
	@Override
	public void deleteGtcShop(GtcShopDTO gtcShopDTO) {
		GtcShop gtcShop = new GtcShop();
		gtcShop.fromDto(gtcShopDTO);
		gtcShopCRUDLocal.delete(gtcShop);
	}
}
