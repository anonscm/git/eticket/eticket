
package org.evolvis.eticketshop.model.crud.template;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticketshop.model.entity.ShopTextTemplate;


// TODO: Auto-generated Javadoc
/**
 * The Interface TextTemplateCRUDBeanLocal.
 */
public interface ShopTextTemplateCRUD extends GenericCUD<ShopTextTemplate> {

	/**
     * Finds entity by system locale and name.
     *
     * @param system
     *            the system
     * @param locale
     *            the locale
     * @param name
     *            the name
     * @return the text template
     */
	public ShopTextTemplate findBySystemLocaleAndName(ESystem system, Locale locale, String name);

	/**
     * List entities in system locale.
     *
     * @param system
     *            the system
     * @param locale
     *            the locale
     * @return the list entities in
     */
	public List<ShopTextTemplate> listSystemLocale(ESystem system, Locale locale);
}
