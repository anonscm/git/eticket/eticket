package org.evolvis.eticketshop.model.crud.product;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.ShopProduct;

// TODO: Auto-generated Javadoc
/**
 * ShopProductCRUD handels database-opreations which are associated with ShopProduct.
 *
 * @author ben
 */
public interface ShopProductCRUD extends GenericCUD<ShopProduct>{
    
    /**
     * Find by id.
     *
     * @param id The id of the ShopProduct
     * @return The founded ShopProduct
     */
	public ShopProduct findById(long id);
	
	/**
	 * Find all ShopProcuts in a system.
	 * @param system The specific system.
	 * @param available Find available ShopProduct or non available ShopProducts
	 * @return List of founded ShopProduct.
	 */
    public List<ShopProduct> findShopProductsInSystem(ESystem system, boolean available);
    
    /**
     * Find a specific ShopProduct by product uin and system,.
     *
     * @param product_uin the product_uin
     * @param eSystem the e system
     * @return ShopProduct
     */
    public ShopProduct findByProductUinAndProductSystem(String product_uin, ESystem eSystem);
    
    /**
	 * Find all ShopProcuts by ProductAssociation.
	 * @param available Find available ShopProduct or non available ShopProducts
	 * @param productAssociation Find ShopProducts for the internal or external Shop 
	 * @return List of founded ShopProduct.
	 */
    public List<ShopProduct> findShopProductsByProductAssociation(boolean available, ProductAssociation productAssociation);
    
    /**
     * Find all ShopProcuts by ProductAssociation.
     *
     * @param available Find available ShopProduct or non available ShopProducts
     * @param productAssociation Find ShopProducts for the internal or external Shop
     * @param eSystem the e system
     * @return List of founded ShopProduct.
     */
    public List<ShopProduct> findShopProductsBySystemAndProductAssociation(boolean available, ProductAssociation productAssociation, ESystem eSystem);
}
