package org.evolvis.eticketshop.model.crud.product;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.ShopProduct;

// TODO: Auto-generated Javadoc
/**
 * ShopProductCRUDBean handels database-opreations which are associated with ShopProduct.
 *
 * @author ben
 */

@Stateless
@Local(ShopProductCRUD.class)
public class ShopProductCRUDBean extends GenericCUDSkeleton<ShopProduct> implements ShopProductCRUD {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9104904147239259171L;

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.model.crud.product.ShopProductCRUD#findById(long)
	 */
	@Override
	public ShopProduct findById(long id) {
		return find(ShopProduct.class, id);
	}
	
	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.model.crud.product.ShopProductCRUD#findShopProductsInSystem(org.evolvis.eticket.model.entity.ESystem, boolean)
	 */
	@Override
	public List<ShopProduct> findShopProductsInSystem(ESystem system, boolean available) {
		return buildNamedQueryGetResults("findShopProductsInSystem", system, available);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.model.crud.product.ShopProductCRUD#findByProductUinAndProductSystem(java.lang.String, org.evolvis.eticket.model.entity.ESystem)
	 */
	@Override
	public ShopProduct findByProductUinAndProductSystem(String product_uin,
			ESystem eSystem) {
		ShopProduct sproduct=null;
		try{
			sproduct=buildNamedQueryGetSingleResult("findByProductUinAndProductSystem", product_uin, eSystem);
		}catch(Exception e){
			sproduct=null;
		}
		return sproduct;
	}

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.model.crud.product.ShopProductCRUD#findShopProductsByProductAssociation(boolean, org.evolvis.eticketshop.model.entity.ProductAssociation)
	 */
	@Override
	public List<ShopProduct> findShopProductsByProductAssociation(boolean available,
			ProductAssociation productAssociation) {
		return buildNamedQueryGetResults("findShopProductsByProductAssociation", available, productAssociation);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.model.crud.product.ShopProductCRUD#findShopProductsBySystemAndProductAssociation(boolean, org.evolvis.eticketshop.model.entity.ProductAssociation, org.evolvis.eticket.model.entity.ESystem)
	 */
	@Override
	public List<ShopProduct> findShopProductsBySystemAndProductAssociation(
			boolean available, ProductAssociation productAssociation,
			ESystem eSystem) {
		return buildNamedQueryGetResults("findShopProductsBySystemAndProductAssociation", eSystem, available, productAssociation);
	}

}
