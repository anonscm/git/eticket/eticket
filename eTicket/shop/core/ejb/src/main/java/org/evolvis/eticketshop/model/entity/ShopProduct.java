package org.evolvis.eticketshop.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;

// TODO: Auto-generated Javadoc
/**
 * ShopProduct is associated with Product. ShopProduct indicates whether a
 * Product is available in Shop or not.
 * 
 * @author ben
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findShopProductsInSystem", query = "SELECT p FROM ShopProduct p WHERE p.product.system = ?1 AND p.available= ?2"),
		@NamedQuery(name = "findByProductUinAndProductSystem", query = "SELECT p FROM ShopProduct p WHERE p.product.uin = ?1 AND p.product.system = ?2"),
		@NamedQuery(name = "findShopProductsBySystemAndProductAssociation", query="SELECT p FROM ShopProduct p WHERE p.product.system = ?1 AND p.available = ?2 AND p.productAssociation = ?3" ),
		@NamedQuery(name = "findShopProductsByProductAssociation", query="SELECT p FROM ShopProduct p WHERE p.available= ?1 AND p.productAssociation = ?2" )})
public class ShopProduct {
	
	/** The id. */
	@Id
	@GeneratedValue
	private long id;

	/** The product. */
	@OneToOne(optional = false)
	private Product product;

	/** The available. */
	@NotNull
	private boolean available;

	/** The product association. */
	private ProductAssociation productAssociation;

	/*
	 * @OnetoMany private PersonGroup persongroup;
	 */

	/**
	 * Instantiates a new shop product.
	 */
	public ShopProduct() {
		available = false;
	}
	
	/**
	 * Instantiates a new shop product.
	 *
	 * @param shopProductDTO the shop product dto
	 */
	public ShopProduct(ShopProductDTO shopProductDTO) {
		fromDto(shopProductDTO);
	}


	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the product.
	 *
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * Sets the product.
	 *
	 * @param product the new product
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * Checks if is avaible.
	 *
	 * @return true, if is avaible
	 */
	public boolean isAvaible() {
		return available;
	}

	/**
	 * Sets the avaible.
	 *
	 * @param avaible the new avaible
	 */
	public void setAvaible(boolean avaible) {
		this.available = avaible;
	}

	/**
	 * Gets the product association.
	 *
	 * @return the product association
	 */
	public ProductAssociation getProductAssociation() {
		return productAssociation;
	}

	/**
	 * Sets the product association.
	 *
	 * @param productAssociation the new product association
	 */
	public void setProductAssociation(ProductAssociation productAssociation) {
		this.productAssociation = productAssociation;
	}

	/**
	 * To dto.
	 *
	 * @return the shop product dto
	 */
	@Transient
	public ShopProductDTO toDto() {
		return ShopProductDTO.createShopProductDTOFromExsistingShopProductEntity(id, product.toDto(), available, productAssociation);
	}
	
	/**
	 * From dto.
	 *
	 * @param shopProduct the shop product
	 */
	@Transient 
	public void fromDto(ShopProductDTO shopProduct){
		this.available = shopProduct.isAvailable();
		this.product = new Product(shopProduct.getProduct());
		this.productAssociation = shopProduct.getProductAssociation();	
		if(shopProduct.getId() > -1) {
			this.id = shopProduct.getId();
		}
	}
	
}
