package org.evolvis.eticket.business.template;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;

import org.evolvis.eticket.annotation.AllowedRole;
import org.evolvis.eticket.interceptors.CheckAccountRights;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO.AllowedRoles;
import org.evolvis.eticketshop.model.crud.template.ShopTextTemplateCRUD;
import org.evolvis.eticketshop.model.entity.ShopTextTemplate;
import org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO;

/**
 * The class {@link ShopTextTemplateBean} handels the communication between view and ShopTextTemplateCRUD.
 * It allows you to insert, update, delete and find  {@link ShopTextTemplate}
 * @author mjohnk
 *
 */
@Interceptors({ CheckAccountRights.class })
@AllowedRole(AllowedRoles.SYSADMIN)
@Stateless
@Remote(ShopTextTemplateBeanService.class)
public class ShopTextTemplateBean implements ShopTextTemplateBeanService {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2397913271872801519L;

	/** The template crud bean local. */
	@EJB
	private ShopTextTemplateCRUD templateCRUDBeanLocal;

	
	/**
	 * Sets the template crud bean local.
	 *
	 * @param templateCRUDBeanLocal the new template crud bean local
	 */
	public void setTemplateCRUDBeanLocal(
			ShopTextTemplateCRUD templateCRUDBeanLocal) {
		this.templateCRUDBeanLocal = templateCRUDBeanLocal;
	}

	/** The local interface {@link ESystemCRUD} */
	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;

	
	/**
	 * Sets the e system crud bean local.
	 *
	 * @param eSystemCRUDBeanLocal the new e system crud bean local
	 */
	public void seteSystemCRUDBeanLocal(
			ESystemCRUD eSystemCRUDBeanLocal) {
		this.eSystemCRUDBeanLocal = eSystemCRUDBeanLocal;
	}

	/** The local interface {@link LocaleCRUD} */
	@EJB
	private LocaleCRUD localeCRUDBeanLocal;

	
	/**
	 * Sets the LocaleCRUDBeanLocal.
	 *
	 * @param localeCRUDBeanLocal the new locale crud bean local
	 */
	public void setLocaleCRUDBeanLocal(LocaleCRUD localeCRUDBeanLocal) {
		this.localeCRUDBeanLocal = localeCRUDBeanLocal;
	}

	/**
	 * Insert the given ShopTextTemplateDTO in the database. The Caller has to be sysadmin.
	 *
	 * @param caller the caller
	 * @param textTemplate the texttemplate that will be insert to the database
	 */
	@Override
	public void insert(AccountDTO caller, ShopTextTemplateDTO textTemplate) {
		ShopTextTemplate template = createTamplate(textTemplate);
		templateCRUDBeanLocal.insert(template);
	}

	/**
	 * creates a new ShopTextTemplate from the given ShopTextTemplateDTO.
	 *
	 * @param textTemplate the Object where the information come from
	 * @return the new Object
	 */
	private ShopTextTemplate createTamplate(ShopTextTemplateDTO textTemplate) {
		ShopTextTemplate template = new ShopTextTemplate(textTemplate);
		ESystem system = eSystemCRUDBeanLocal.findByName(textTemplate
				.getSystemDTO().getName());
		Locale locale = localeCRUDBeanLocal.findByCode(textTemplate
				.getLocaleDTO().getCode());
		template.setSystem(system);
		template.setLocale(locale);
		return template;
	}

	/**
	 * Loads a ShopTextTemplate with the system, locale and the name of the given ShopTextTemplateDTO.
	 *
	 * @param textTemplate the Object where the information comes from
	 * @return the Object from the database
	 */
	private ShopTextTemplate getTextTemplate(ShopTextTemplateDTO textTemplate) {
		ESystem system = eSystemCRUDBeanLocal.findByName(textTemplate
				.getSystemDTO().getName());
		Locale locale = localeCRUDBeanLocal.findByCode(textTemplate
				.getLocaleDTO().getCode());
		return templateCRUDBeanLocal.findBySystemLocaleAndName(system, locale,
				textTemplate.getName());
	}

	/**
	 * Updates a ShopTextTemplate in the database. The caller must be sysadmin
	 *
	 * @param caller the caller
	 * @param textTemplate the texttemplate that will be updated
	 */
	@Override
	public void update(AccountDTO caller, ShopTextTemplateDTO textTemplate) {
		ShopTextTemplate template = getTextTemplate(textTemplate);
		template.fromDto(textTemplate);
		templateCRUDBeanLocal.update(template);
	}

	/**
	 * Deletes a ShopTextTemplate. The caller must be sysadmin
	 *
	 * @param caller the caller
	 * @param textTemplate the texttemplate that will be deleted
	 */
	@Override
	public void delete(AccountDTO caller, ShopTextTemplateDTO textTemplate) {
		templateCRUDBeanLocal.delete(getTextTemplate(textTemplate));
	}

	/**
	 * Finds a ShopTextTemplate with the given ESystemDTO, LocaleDTO and name.
	 *
	 * @param eSystem the esystem
	 * @param localeDto the locale 
	 * @param name the name
	 * @return the data transfer object for ShopTextTemplate
	 */
	@ExcludeClassInterceptors
	@Override
	public ShopTextTemplateDTO find(ESystemDTO eSystem, LocaleDTO localeDto,
			String name) {
		ESystem system = eSystemCRUDBeanLocal.findByName(eSystem.getName());
		Locale locale = localeCRUDBeanLocal.findByCode(localeDto.getCode());
		return templateCRUDBeanLocal.findBySystemLocaleAndName(system, locale,
				name).toDTO();
	}
}
