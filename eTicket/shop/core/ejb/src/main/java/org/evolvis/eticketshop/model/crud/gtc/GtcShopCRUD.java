package org.evolvis.eticketshop.model.crud.gtc;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticketshop.model.entity.GtcShop;
import org.evolvis.eticketshop.model.entity.ProductAssociation;

// TODO: Auto-generated Javadoc
/**
 * ShopProductCRUD handels database-opreations which are associated with GtcShop.
 *
 * @author ben
 */
public interface GtcShopCRUD extends GenericCUD<GtcShop>{
    
    /**
     * Find by id.
     *
     * @param id The id of the GtcShop
     * @return The founded GtcShop
     */
	public GtcShop findById(long id);
	
	
	/**
	 * Finds the Gtc by the ProductAssociation.
	 *
	 * @param productAssociation the ProductAssociation
	 * @param locale the locale
	 * @return the Gtc with the ProductAssociation
	 */
	public GtcShop findByProductAssociation(ProductAssociation productAssociation, Locale locale );
	
	
}
