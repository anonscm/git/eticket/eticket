package org.evolvis.eticketshop.util;

import java.util.UUID;

// TODO: Auto-generated Javadoc
/**
 * The Class GenerateToken.
 */
public class GenerateToken {
	
	/**
	 * Instantiates a new generate token.
	 */
	private GenerateToken() {

	}

	/**
	 * Generate token.
	 *
	 * @return the string
	 */
	public static String generateToken() {
		return generateUUIDToken();
	}

	/**
	 * Generate uuid token.
	 *
	 * @return the string
	 */
	static String generateUUIDToken() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
//	static String generateTimeHashToken(){
//		return Hash.getHash(""+GregorianCalendar.getInstance().getTime().getTime());
//	}
	

}
