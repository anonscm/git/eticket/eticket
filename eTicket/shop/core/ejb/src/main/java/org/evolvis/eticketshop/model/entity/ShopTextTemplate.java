package org.evolvis.eticketshop.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ShopTextTemplate.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "NAME",
		"SYSTEM_ID", "LOCALE_ID" }))
@NamedQueries({
		@NamedQuery(name = "findShopTextTemplateBySystemLocaleName", query = "SELECT t FROM ShopTextTemplate t WHERE t.system = ?1 AND t.locale = ?2 AND t.name = ?3"),
		@NamedQuery(name = "listShopTextTemplateInSystemLocale", query = "SELECT t FROM ShopTextTemplate t WHERE t.system = ?1 AND t.locale = ?2") })
public class ShopTextTemplate {
	
	/** The id. */
	private long id;
	
	/** The locale. */
	private Locale locale;
	
	/** The system. */
	private ESystem system;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;

	/** The text. */
	private String text;

	/**
	 * Instantiates a new shop text template.
	 *
	 * @param textTemplate the text template
	 */
	public ShopTextTemplate(ShopTextTemplateDTO textTemplate) {
		fromDto(textTemplate);
	}

	/**
	 * Instantiates a new shop text template.
	 */
	public ShopTextTemplate() {

	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the locale.
	 *
	 * @return the locale
	 */
	@ManyToOne(optional = false)
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Sets the locale.
	 *
	 * @param locale the new locale
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * Gets the system.
	 *
	 * @return the system
	 */
	@ManyToOne(optional = false)
	public ESystem getSystem() {
		return system;
	}

	/**
	 * Sets the system.
	 *
	 * @param system the new system
	 */
	public void setSystem(ESystem system) {
		this.system = system;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Column(nullable = false)
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	@Column(nullable = false, columnDefinition = "VARCHAR(23542)")
	public String getText() {
		return text;
	}

	/**
	 * Sets the text.
	 *
	 * @param text the new text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * To dto.
	 *
	 * @return the shop text template dto
	 */
	public ShopTextTemplateDTO toDTO() {
		return ShopTextTemplateDTO.createTextTemplateDTOFromExsistingTextTemplateEntity(getSystem().toDto(), getLocale().toDto(),
				getName(), getText(), getDescription());
	}

	/**
	 * From dto.
	 *
	 * @param textTemplate the text template
	 */
	public void fromDto(ShopTextTemplateDTO textTemplate) {
		setName(textTemplate.getName());
		setDescription(textTemplate.getDescripption());
		setText(textTemplate.getText());
	}

}
