package org.evolvis.eticketshop.business.mail;

import java.util.Map;

import org.evolvis.eticketshop.model.entity.ShopTextTemplate;
import org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO;

/**
 * The interface for {@link EMail} it allows to send a EMail.
 */
public interface Communicate{
	
	/**
	 * Send to.
	 *
	 * @param sender the sender
	 * @param recipient the recipient
	 * @param template the template
	 * @param buzzwordsAndValues the buzzwords and values
	 * @param pdf the pdf
	 */
	public void sendTo(EMailAccountDTO sender, EMailAccountDTO recipient, ShopTextTemplate template,
			Map<String, String> buzzwordsAndValues, byte[] pdf);
}
