package org.evolvis.eticketshop.util;


// TODO: Auto-generated Javadoc
/**
 * The Class MessageConstClass.
 */
public final class MessageConstClass {

	/**
	 * Instantiates a new message const class.
	 */
	private MessageConstClass() {
		// TODO Auto-generated constructor stub
	}

	/** The Constant PASSWORD_MUST_SEND_AS_PLAINTEXT. */
	public static final String PASSWORD_MUST_SEND_AS_PLAINTEXT = "The password of this user must send as plaintext in order to check it";
	
	/** The Constant USER_UNKNOWN_OR_PASSWORD_INVALID. */
	public static final String USER_UNKNOWN_OR_PASSWORD_INVALID = "The user is unknown or the password is invalid";
}
