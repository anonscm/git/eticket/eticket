package org.evolvis.eticketshop.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticketshop.model.entity.dto.GtcShopDTO;
import org.hibernate.annotations.Type;

// TODO: Auto-generated Javadoc
/**
 * The Class GtcShop.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "productAssociation", "locale_id" }))
@NamedQuery(name="findGtcShopByProductAssociation", query="SELECT g FROM GtcShop g WHERE g.productAssociation = ?1 AND g.locale = ?2 ")

public class GtcShop {
	
	/** The id. */
	@Id
	@GeneratedValue
	private long id;

	/** The value. */
	@Lob()
	@Type(type="org.hibernate.type.StringClobType")
	private String value;
	
	/** The product association. */
	private ProductAssociation productAssociation;
	
	/** The locale. */
	@ManyToOne(optional = false)
	private Locale locale;
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the product association.
	 *
	 * @return the product association
	 */
	public ProductAssociation getProductAssociation() {
		return productAssociation;
	}
	
	/**
	 * Sets the product association.
	 *
	 * @param productAssociation the new product association
	 */
	public void setProductAssociation(ProductAssociation productAssociation) {
		this.productAssociation = productAssociation;
	}
	
	/**
	 * Gets the locale.
	 *
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}
	
	/**
	 * Sets the locale.
	 *
	 * @param locale the new locale
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	
	/**
	 * To dto.
	 *
	 * @return the gtc shop dto
	 */
	@Transient
	public GtcShopDTO toDto(){
		return GtcShopDTO.init(getId(), getValue(), getProductAssociation(), locale.toDto());
	}
	
	/**
	 * From dto.
	 *
	 * @param gtcShopDTO the gtc shop dto
	 */
	@Transient
	public void fromDto(GtcShopDTO gtcShopDTO){
		this.id = gtcShopDTO.getId();
		this.productAssociation = gtcShopDTO.getProductAssociation();
		this.value = gtcShopDTO.getValue();
		Locale locale = new Locale();
		locale.fromDto(gtcShopDTO.getLocaleDTO());
		if(gtcShopDTO.getId() > -1) {
			this.id = gtcShopDTO.getId();
		}
	}
}
