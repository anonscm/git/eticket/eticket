package org.evolvis.eticketshop.model.crud.gtc;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticketshop.model.entity.GtcShop;
import org.evolvis.eticketshop.model.entity.ProductAssociation;

// TODO: Auto-generated Javadoc
/**
 * ShopProductCRUDBean handels database-opreations which are associated with GtcShop.
 *
 * @author ben
 */

@Stateless
@Local(GtcShopCRUD.class)
public class GtcShopCRUDBean extends GenericCUDSkeleton<GtcShop> implements GtcShopCRUD {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9104904147239259171L;

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.model.crud.gtc.GtcShopCRUD#findById(long)
	 */
	@Override
	public GtcShop findById(long id) {
		return find(GtcShop.class, id);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.model.crud.gtc.GtcShopCRUD#findByProductAssociation(org.evolvis.eticketshop.model.entity.ProductAssociation, org.evolvis.eticket.model.entity.Locale)
	 */
	@Override
	public GtcShop findByProductAssociation(
			ProductAssociation productAssociation, Locale locale) {
		return buildNamedQueryGetSingleResult("findGtcShopByProductAssociation", productAssociation, locale);
	}

	

}
