package org.evolvis.eticketshop.business.mail;

import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticketshop.model.crud.template.ShopTextTemplateCRUD;
import org.evolvis.eticketshop.model.entity.ShopTextTemplate;
import org.evolvis.eticketshop.model.entity.TextTemplateNames;
import org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class EMailBean.
 */
@Stateless
@Remote(EMailBeanService.class)
public class EMailBean implements EMailBeanService {

	/** The configuration parameter crud remote. */
	@EJB
	private ConfigurationParameterCRUD configurationParameterCRUDRemote;
	
	/** The e system crud remote. */
	@EJB
	private ESystemCRUD eSystemCRUDRemote;
	
	/** The text template crud remote. */
	@EJB
	private ShopTextTemplateCRUD textTemplateCRUDRemote;
	
	/** The locale crud remote. */
	@EJB
	private LocaleCRUD localeCRUDRemote;

	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.business.mail.EMailBeanService#sendMail(org.evolvis.eticket.model.entity.dto.ESystemDTO, org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO, org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO, java.util.Map, byte[])
	 */
	@Override
	public void sendMail(ESystemDTO systemDTO, EMailAccountDTO account,
			ShopTextTemplateDTO templateDTO, Map<String, String> buzzwords,
			byte[] pdf) {
		ESystem system = eSystemCRUDRemote.findByName(templateDTO
				.getSystemDTO().getName());
		Locale locale = localeCRUDRemote.findByCode(templateDTO.getLocaleDTO()
				.getCode());

		ShopTextTemplate template = textTemplateCRUDRemote
				.findBySystemLocaleAndName(system, locale,
						templateDTO.getName());
		CommunicateUtil.sendMail(configurationParameterCRUDRemote, system,
				account, template, buzzwords, pdf);
	}
	
	/* (non-Javadoc)
	 * @see org.evolvis.eticketshop.business.mail.EMailBeanService#getBuzzwords(org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO.TextTemplateNames, java.lang.String[])
	 */
	@Override
	public Map<String, String> getBuzzwords(TextTemplateNames tempName, String...values){
		return CommunicateUtil.getBuzzwords(tempName, values);
	}
}
