package org.evolvis.eticketshop.model.crud.product;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticketshop.model.entity.ShopProduct;
import org.junit.Before;
import org.junit.Test;

public class ShopProductCrudBeanTest {
	
	private ShopProductCRUDBean bean = new ShopProductCRUDBean();
	private EntityManager manager = mock(EntityManager.class);
	private Query query = mock(Query.class);

	@Before
	public void init() {
		bean.setManager(manager);
		when(manager.createNamedQuery(anyString())).thenReturn(query);
	}

	@Test
	public void shouldGetById() {
		bean.findById(4242);
		verify(manager).find(ShopProduct.class, (long) 4242);
	}
}
