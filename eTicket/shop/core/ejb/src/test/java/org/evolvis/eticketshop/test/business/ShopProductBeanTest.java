package org.evolvis.eticketshop.test.business;

import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticketshop.business.product.ShopProductBean;
import org.evolvis.eticketshop.model.crud.product.ShopProductCRUD;
import org.evolvis.eticketshop.model.entity.ShopProduct;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

public class ShopProductBeanTest {
	private ShopProductBean shopProductBean = new ShopProductBean();
	private ESystem eSystem = new ESystem();
	private LocaleDTO localeDTO = LocaleDTO.createNewLocaleDTO("de", "deutsch");
	private ESystemDTO system = ESystemDTO.createNewESystemDTO("systemname", localeDTO, null);
	private ESystemCRUD eSystemCRUDBeanLocal = mock(ESystemCRUD.class);
	private ShopProductCRUD shopProductCRUDBeanLocal = mock(ShopProductCRUD.class);

	@Before
	public void init() {
		shopProductBean.seteSystemCRUDBeanLocal(eSystemCRUDBeanLocal);
		shopProductBean.setShopProductCRUDBeanLocal(shopProductCRUDBeanLocal);

		helpSetProduct();
		helpSetSystem();
	}

	private void helpSetProduct() {
		List<ShopProduct> allShopProductDTO = new ArrayList<ShopProduct>();
		Mockito.when(
				shopProductCRUDBeanLocal
						.findShopProductsInSystem(eSystem, true)).thenReturn(
				allShopProductDTO);
	}

	private void helpSetSystem() {
		eSystem.setName(system.getName());
		eSystem.setDefaultLocale(new Locale(localeDTO));
		Mockito.when(eSystemCRUDBeanLocal.findByName(system.getName()))
				.thenReturn(eSystem);
	}

	@Test
	public void shouldGetAllShopProductDTO() {
		/*
		 * 1. Definiere mit Mockito.when was in bestimmten fällen passieren
		 * (zurückgegeben) werden soll. 2. Das testen was in
		 * getAllShopProductDTO steht 3. nicht die Interfaces, sondern Objekte
		 * benuten Bsp: ShopProductBean
		 */

		InOrder inOrder = Mockito
				.inOrder(eSystemCRUDBeanLocal, shopProductCRUDBeanLocal);

		List<ShopProductDTO> list_return = shopProductBean
				.getAllShopProductDTOFromSystem(system.getName(), true);

		Assert.assertNotNull(list_return);

		/*
		 * Mit inOrder kann ich prüfen, ob eine Funktion wie z.B. findByName vor einer anderen Funktion z.B. listShopProductsInSystem
		 * aufgerufen wurde. Bei Funktionen werden in getAllShopProductDTO aufgerufen.
		 */
		inOrder.verify(eSystemCRUDBeanLocal).findByName(system.getName());
		inOrder.verify(shopProductCRUDBeanLocal).findShopProductsInSystem(
				eSystem, true);
	}
}
