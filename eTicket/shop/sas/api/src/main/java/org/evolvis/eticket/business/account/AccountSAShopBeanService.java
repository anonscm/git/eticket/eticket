package org.evolvis.eticket.business.account;

import org.evolvis.eticket.model.entity.dto.AccountSAShopDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticketpayment.model.entity.dto.PayPalOrderDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;

/**
 * The Interface AccountSAShopBeanService.
 */
public interface AccountSAShopBeanService {
	
	/**
	 * Gets a Account by the uin 
	 * 
	 * @param uin 
	 * 			 the uin from the ProductLink table
	 * @return the accountSAShopDTO
	 */
	public AccountSAShopDTO getAccount(String uin); 
	
	public void cerate(PersonalInformationDTO personalInformation, ShopProductDTO shopProductDTO, PayPalOrderDTO payPalOrderDTO);
	
	public AccountSAShopDTO getAccount(PayPalOrderDTO payPalOrderDTO);
	
}
