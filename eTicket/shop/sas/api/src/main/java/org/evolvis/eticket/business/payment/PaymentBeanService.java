package org.evolvis.eticket.business.payment;

import java.util.Locale;

import org.evolvis.eticket.model.entity.dto.AccountSAShopDTO;

public interface PaymentBeanService {
	
	public void completePayment(Locale locale, AccountSAShopDTO account);

}
