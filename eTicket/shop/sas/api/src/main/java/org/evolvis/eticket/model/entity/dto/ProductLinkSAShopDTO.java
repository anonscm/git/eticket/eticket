package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;

import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;

/**
 * The Class ProductLinkSAShopDTO. 
 * 	
 * This is a data transfer object for the class ProductLinkSAShop
 * 	
 * @author mjohnk
 *
 */
public class ProductLinkSAShopDTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3201389199691502250L;
	
	/** The uin, its a random String with 32 letters */
	private final String uin;
	
	/** The shopProduct */
	private final ShopProductDTO shopProduct;
	
	private final long id;
	
	private final String productNumber;
	
	/**
	 * Instantiates a new ProductLinkSAShop dto.
	 * @param uin
	 * 			the uin, its a random String with 32 letters
	 * @param productSAShop
	 * 			the productSAShopDTO
	 */

	private ProductLinkSAShopDTO(long id,String uin, ShopProductDTO shopProduct, String productNumber) {
		this.uin = uin;
		this.shopProduct = shopProduct;
		this.id=id;
		this.productNumber = productNumber;
	}
	
	
	/**
	 * Creates a new ProductLinkSAShopDTO. This method is typically called from the frontend.
	 * 
	 * @param uin
	 * 			the uin, its a random String with 32 letters
	 * @param productSAShop
	 * 			the productSAShopDTO
	 * @return the productLinkSAShop
	 */
	public static ProductLinkSAShopDTO createNewAccountDTO(String uin, ShopProductDTO shopProductDTO, String productNumber){
		return new ProductLinkSAShopDTO(-1, uin, shopProductDTO, productNumber);

	}
	
	/**
	 * Creates ProductLinkSAShopDTO from an existing ProductLinkSAShop which is already stored in dbs. This method is typically called from the backend.
	 * @param uin
	 * 			the uin, its a random String with 32 letters
	 * @param productSAShop
	 * 			the productSAShopDTO
	 * @return the productLinkSAShop
	 */

	public static ProductLinkSAShopDTO createAccountDTOFromExsistingAccountEntity(long id, String uin, ShopProductDTO shopProductDTO, String productNumber){
		return new ProductLinkSAShopDTO(id, uin, shopProductDTO, productNumber);
	}
	
	/**
	 * Gets the uin
	 * @return the uin, its a random String with 32 letters
	 */
	public String getUin() {
		return uin;
	}
	
	/**
	 * Gets the shopProductDTO
	 * @return the shopProductDTO
	 */

	public ShopProductDTO getShopProductDTO() {
		return shopProduct;
	}

	public long getId() {
		return id;
	}

	public String getProductNumber() {
		return productNumber;
	}	
	
}
