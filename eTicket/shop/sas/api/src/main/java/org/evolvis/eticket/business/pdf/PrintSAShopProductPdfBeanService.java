package org.evolvis.eticket.business.pdf;

import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;

/**
 * The interface PrintSAShopProductPdfBeanService
 * 
 * @author mjohnk
 * 
 */

public interface PrintSAShopProductPdfBeanService {

	/**
	 * Create the PDF
	 * 
	 * @param personalInformationDTO
	 *            the personalInformation of the Account
	 * @param productId
	 *            the id of the ProductSAShopDTO
	 * @param barcode
	 * @param locale
	 *            the locale of the PDF
	 * @return the PDF as a byte array
	 */
	public byte[] createPDF(PersonalInformationDTO personalInformationDTO,
			String uin, boolean barcode, String locale, ESystemDTO systemDTO,String ProductNumber);

}
