package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;

/**
 * The Class AccountSAShopDTO.
 * 
 * This is a data transfer object for the class AccountSAShop
 * 
 * @author mjohnk
 * 
 */
public class AccountSAShopDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8759693934449478106L;

	/** The username of Account */
	private final String username;

	/** The productLink of the bought product */
	private final ProductLinkSAShopDTO productLink;

	/** The personal information of the account */
	private final PersonalInformationDTO personalInformation;

	private final long id;

	/**
	 * Instantiates a new accountSAShop dto.
	 * 
	 * @param username
	 *            the username of the Product
	 * @param productLink
	 *            the productLink of the bought product
	 * @param personalInformation
	 *            the personal information of the account
	 */
	private AccountSAShopDTO(long id, String username,
			ProductLinkSAShopDTO productLink,
			PersonalInformationDTO personalInformation) {
		this.username = username;
		this.productLink = productLink;
		this.personalInformation = personalInformation;
		this.id = id;
	}

	/**
	 * Creates a new AccountSAShopDTO. This method is typically called from the
	 * frontend.
	 * 
	 * @param username
	 *            the username of the Product
	 * @param productLink
	 *            the productLink of the bought product
	 * @param personalInformation
	 *            the personal information of the account
	 * @return the account
	 */
	public static AccountSAShopDTO createNewAccountSAShopDTO(String username,
			ProductLinkSAShopDTO productLink,
			PersonalInformationDTO personalInformation) {
		return new AccountSAShopDTO(-1, username, productLink,
				personalInformation);
	}

	/**
	 * Creates AccountSAShopDTO from an existing AccountSAShop which is already
	 * stored in dbs. This method is typically called from the backend.
	 * 
	 * @param username
	 *            the username of the Product
	 * @param productLink
	 *            the productLink of the bought product
	 * @param personalInformation
	 *            the personal information of the account
	 * @return the account
	 */
	public static AccountSAShopDTO createAccountSAShopDTOFromExsistingAccountSAShopEntity(
			long id, String username, ProductLinkSAShopDTO productLink,
			PersonalInformationDTO personalInformation) {
		return new AccountSAShopDTO(id, username, productLink,
				personalInformation);
	}

	/**
	 * Gets the username
	 * 
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Gets the productLink of the bought product
	 * 
	 * @return the product link
	 */
	public ProductLinkSAShopDTO getProductLink() {
		return productLink;
	}

	/**
	 * Gets the personal information of the account
	 * 
	 * @return the personal information
	 */
	public PersonalInformationDTO getPersonalInformation() {
		return personalInformation;
	}

	public long getId() {
		return id;
	}
}
