package org.evolvis.eticket.model.entity.dto;

import java.io.Serializable;

/**
 * The Class ProductSAShopDTO. 
 * 	
 * This is a data transfer object for the class ProductSAShop
 * 	
 * @author mjohnk
 *
 */
public final class ProductSAShopDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3814642478314734835L;

	/** The id of the productSAShop */
	private final long id;
	
	/** The product */
	private final ProductDTO productDTO;
	
	/** The status of the productSAShop */
	private final boolean active;
	
	/** The max amount of the productSAShop */
	private final int maxAmount;
	
	/** The current amount of the productSAShop */
	private final int currentAmount;

	/**
	 * Instantiates a new ProductSAShop dto.
	 * 
	 * @param id
	 * 			the id of the productSAShop
	 * @param productDTO
	 * 			the product
	 * @param active
	 * 			the status of the productSAShop	
	 * @param maxAmount
	 * 			the max amount of the productSAShop
	 * @param currentAmount
	 * 			the current amount of the productSAShop
	 */
	private ProductSAShopDTO(long id, ProductDTO productDTO, boolean active,
			int maxAmount, int currentAmount) {
		this.productDTO = productDTO;
		this.active = active;
		this.maxAmount = maxAmount;
		this.currentAmount = currentAmount;
		this.id = id;
	}
	
	/**
	 * Creates a new AccountDTO. This method is typically called from the frontend.
	 * 
	 * @param id
	 * 			the id of the productSAShop
	 * @param productDTO
	 * 			the product
	 * @param active
	 * 			the status of the productSAShop	
	 * @param maxAmount
	 * 			the max amount of the productSAShop
	 * @param currentAmount
	 * 			the current amount of the productSAShop
	 * @return the productSAShop
	 */
	public static ProductSAShopDTO createNewProductSAShopDTO(ProductDTO productDTO, boolean active,
			int maxAmount, int currentAmount) {
		return new ProductSAShopDTO(-1, productDTO, active, maxAmount,
				currentAmount);
	}
	
	
	/**
	 * Creates AccountDTO from an existing Account which is already stored in dbs. This method is typically called from the backend.
	 * 
	 * @param id
	 * 			the id of the productSAShop
	 * @param productDTO
	 * 			the product
	 * @param active
	 * 			the status of the productSAShop	
	 * @param maxAmount
	 * 			the max amount of the productSAShop
	 * @param currentAmount
	 * 			the current amount of the productSAShop
	 * @return the productSAShop
	 */
	public static ProductSAShopDTO createProductSAShopDTOFromExsistingProductSAShopEntity(long id, ProductDTO productDTO, boolean active,
			int maxAmount, int currentAmount) {
		return new ProductSAShopDTO(id, productDTO, active, maxAmount,
				currentAmount);
	}

	/**
	 * Gets the id 
	 * 
	 * @return the id 
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Gets the product
	 * 
	 * @return the product
	 */
	public ProductDTO getProductDTO() {
		return productDTO;
	}

	/**
	 * Gets the status of the productSAShop
	 * 
	 * @return the status 
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Gets the max amount of the productSAShop
	 * 
	 * @return the max amount 
	 */
	public int getMaxAmount() {
		return maxAmount;
	}

	/**
	 * Get the current amount of the productSAShop
	 * 
	 * @return the current amount 
	 */
	public int getCurrentAmount() {
		return currentAmount;
	}
	
	/**
	 * Gets the salable amount 
	 * 
	 * @return the amount
	 */
	public int getAmount(){
		return maxAmount - currentAmount;
	}		
}
