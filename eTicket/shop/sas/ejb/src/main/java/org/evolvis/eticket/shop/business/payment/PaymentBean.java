package org.evolvis.eticket.shop.business.payment;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.evolvis.eticket.business.payment.PaymentBeanService;
import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.dto.AccountSAShopDTO;
import org.evolvis.eticketpayment.business.invoice.InvoiceBeanService;
import org.evolvis.eticketpayment.model.entity.InvoiceField;
import org.evolvis.eticketshop.business.mail.EMailBeanService;
import org.evolvis.eticketshop.model.crud.template.ShopTextTemplateCRUD;
import org.evolvis.eticketshop.model.entity.ShopTextTemplate;
import org.evolvis.eticketshop.model.entity.TextTemplateNames;
import org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;

@Stateless
@Remote(PaymentBeanService.class)
public class PaymentBean implements PaymentBeanService{
	
	@EJB
	private InvoiceBeanService invoiceBeanRemote;
	@EJB
	private ConfigurationParameterCRUD configurationParameterBeanRemote;
	@EJB
	private ShopTextTemplateCRUD textTemplateBeanRemote;
	@EJB
	private LocaleCRUD localeBeanRemote;
	@EJB
	private ESystemCRUD eSystemBeanRemote;
	@EJB
	private EMailBeanService mailBean;

	@Override
	public void completePayment(Locale locale, AccountSAShopDTO account) {
		Map<InvoiceField, Object> values = createMap(locale, account.getProductLink().getShopProductDTO());
		byte[] pdf = invoiceBeanRemote.generateInvoice(account
				.getProductLink().getShopProductDTO(), account
				.getPersonalInformation().getLocaleDTO(), values);
		send(account.getProductLink().getShopProductDTO(), account, pdf);
	}	
	
	private Map<InvoiceField, Object> createMap(java.util.Locale locale,
			ShopProductDTO shopProductDTO) {
		Map<InvoiceField, Object> values = new HashMap<InvoiceField, Object>();
		values.put(
				InvoiceField.DATE,
				DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(
						new Date()));
		values.put(InvoiceField.AMOUNT, "1");
		values.put(
				InvoiceField.DELIVERYDATE,
				DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(
						new Date()));
		values.put(InvoiceField.INVOICENUMBER,
				invoiceBeanRemote.getNextInvoiceNumber());
		values.put(InvoiceField.PRODUCTNAME, shopProductDTO.getProduct()
				.getProductLocales()[0].getProductName());
		values.put(InvoiceField.TAXNUMBER, "777");
		values.put(InvoiceField.TAXRATE, "19%");
		return values;
	}
	
	private void send(ShopProductDTO shopProductDTO, AccountSAShopDTO account,
			byte[] pdf) {
		ESystem system = eSystemBeanRemote.findByName(shopProductDTO
				.getProduct().getSystem().getName());
		send(system, account, account.getProductLink().getUin(),
				TextTemplateNames.BOUGHT_PRODUCT_EXTERN, account
						.getPersonalInformation().getLocaleDTO().getCode(),
				pdf);
	}

	private void send(ESystem system, AccountSAShopDTO account, final String uin,
			final TextTemplateNames templateName, String localecode, byte[] pdf) {
		final org.evolvis.eticket.model.entity.Locale locale = getLocale(system, account, localecode);
		final ShopTextTemplate template = textTemplateBeanRemote
				.findBySystemLocaleAndName(system, locale,
						templateName.toString());
		String sysDomain = configurationParameterBeanRemote.findBySystemAndKey(
				system, CPKey.SHOP_DOMAIN.toString()).getValue();
		String al = sysDomain + "/dispatcher/pdf" + "?token=" + uin;
		Map<String, String> buzzwords = mailBean.getBuzzwords(
				templateName, al);
		
		EMailAccountDTO mailAccount = EMailAccountDTO.init(account.getUsername(), 
				account.getPersonalInformation().getFirstname() + account.getPersonalInformation().getLastname());
		
		mailBean.sendMail(system.toDto(),
				mailAccount, template.toDTO(), buzzwords, pdf);
	}
	
	private org.evolvis.eticket.model.entity.Locale getLocale(ESystem system, AccountSAShopDTO account,
			String localecode) {
		if (localecode != null) {
			return localeBeanRemote.findByCode(localecode);
		}
		if (account.getPersonalInformation() != null) {
			 
			localecode = account.getPersonalInformation().getLocaleDTO().getCode();
			return localeBeanRemote.findByCode(localecode);
		}

		return system.getDefaultLocale();
	}

}
