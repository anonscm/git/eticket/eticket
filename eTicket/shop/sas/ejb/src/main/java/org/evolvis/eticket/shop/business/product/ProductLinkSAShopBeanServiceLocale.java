package org.evolvis.eticket.shop.business.product;

import org.evolvis.eticket.model.entity.dto.AccountSAShopDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;

/**
 * The interface ProductLinkSAShopBeanServive
 * @author mjohnk
 *
 */
public interface ProductLinkSAShopBeanServiceLocale {
	
	/**
	 * generates the ProductLink and send a E-Mail with the bill and the URI
	 * 
	 * @param personalInformationDTO
	 * 			the personal information of the account
	 * @param productSAShopDTO
	 * 			the productSAShopDTO	 
	 * @return the account
	 */
	public AccountSAShopDTO generateProductLink(PersonalInformationDTO personalInformationDTO, ShopProductDTO shopProductDTO);

}
