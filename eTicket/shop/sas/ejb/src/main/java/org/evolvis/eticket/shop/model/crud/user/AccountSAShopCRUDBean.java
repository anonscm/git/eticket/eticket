package org.evolvis.eticket.shop.model.crud.user;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.shop.model.entity.AccountSAShop;
import org.evolvis.eticketpayment.model.entity.PayPalOrder;

@Stateless
@Local(AccountSAShopCRUD.class)
public class AccountSAShopCRUDBean extends GenericCUDSkeleton<AccountSAShop> implements
AccountSAShopCRUD{

	private static final long serialVersionUID = 3073991471988863753L;

	@Override
	public AccountSAShop findByUin(String uin) {
		return buildNamedQueryGetSingleResult("findAccountSAShopByUin", uin); 
	}

	@Override
	public List<AccountSAShop> findByUsername(String username) {
		try {
			return buildNamedQueryGetResults("findAccountSAShopByUsername", username);
		}catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public AccountSAShop findByPayPalOrder(PayPalOrder order) {
		return buildNamedQueryGetSingleResult("findAccountSAShopByPayPalOrder", order);
	}	
}
