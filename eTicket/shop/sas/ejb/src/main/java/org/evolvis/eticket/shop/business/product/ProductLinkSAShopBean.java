package org.evolvis.eticket.shop.business.product;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.evolvis.eticket.business.product.ProductLinkSAShopBeanService;
import org.evolvis.eticket.business.template.ShopTextTemplateBeanService;
import org.evolvis.eticket.model.crud.platform.ConfigurationParameterCRUD;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.CPKey;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.PersonalInformation;
import org.evolvis.eticket.model.entity.dto.AccountSAShopDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.shop.model.crud.user.AccountSAShopCRUD;
import org.evolvis.eticket.shop.model.entity.AccountSAShop;
import org.evolvis.eticket.shop.model.entity.ProductLinkSAShop;
import org.evolvis.eticketpayment.business.invoice.InvoiceBeanService;
import org.evolvis.eticketpayment.model.entity.InvoiceField;
import org.evolvis.eticketshop.business.mail.EMailBeanService;
import org.evolvis.eticketshop.model.crud.product.ShopProductCRUD;
import org.evolvis.eticketshop.model.entity.ShopProduct;
import org.evolvis.eticketshop.model.entity.TextTemplateNames;
import org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO;

@Stateless
@Remote(ProductLinkSAShopBeanService.class)
public class ProductLinkSAShopBean implements
		ProductLinkSAShopBeanServiceLocale, ProductLinkSAShopBeanService {

	@EJB
	private ShopProductCRUD shopProductCRUDBean;
	@EJB
	private AccountSAShopCRUD accountSAShopBeanLocal;
	@EJB
	private LocaleCRUD localeBeanRemote;
	@EJB
	private ESystemCRUD eSystemBeanRemote;
	@EJB
	private EMailBeanService eMailBean;
	@EJB
	private ShopTextTemplateBeanService shopTextTemplateBeanService;
	@EJB
	private InvoiceBeanService invoiceBeanService;
	@EJB
	private ConfigurationParameterCRUD configurationParameterCRUD;

	@Override
	public AccountSAShopDTO generateProductLink(
			PersonalInformationDTO personalInformationDTO,
			ShopProductDTO shopProductDTO) {
		AccountSAShop accountSAShop = createAccount(
				createPersonalInformation(personalInformationDTO),
				createProductLink(shopProductDTO));
		updateProduct(shopProductDTO);
		return accountSAShop.toDto();
	}

	@Override
	public void resend(String eMail) {
		List<AccountSAShop> accounts = accountSAShopBeanLocal
				.findByUsername(eMail);
		for (AccountSAShop accountSAShop : accounts) {
			ESystem system = accountSAShop.getProductLink().getShopProduct()
					.getProduct().getSystem();
			EMailAccountDTO account = EMailAccountDTO.init(
					accountSAShop.getUsername(), accounts.get(0)
							.getPersonalInformation().getFirstname()
							+ " "
							+ accountSAShop.getPersonalInformation()
									.getLastname());
			ShopTextTemplateDTO shopTextTemplateDTO = shopTextTemplateBeanService
					.find(system.toDto(), accountSAShop
							.getPersonalInformation().getLocale().toDto(),
							TextTemplateNames.BOUGHT_PRODUCT_EXTERN.toString());
			java.util.Locale locale = new java.util.Locale(accountSAShop
					.getPersonalInformation().getLocale().getLocalecode());
			byte[] pdf = invoiceBeanService.generateInvoice(
					accountSAShop.getProductLink().getShopProduct().toDto(),
					accountSAShop.getPersonalInformation().getLocale().toDto(),
					createMap(locale, accountSAShop.getProductLink()
							.getShopProduct().toDto()));
			eMailBean.sendMail(system.toDto(), account, shopTextTemplateDTO, createBuzzwordsMap(system, accountSAShop.getProductLink().getUin()),
					pdf);
		}
	}

	private Map<String, String> createBuzzwordsMap(ESystem system, String uin) {
		String sysDomain = configurationParameterCRUD.findBySystemAndKey(
				system, CPKey.SHOP_DOMAIN.toString()).getValue();
		String al = sysDomain + "/dispatcher/pdf" + "?token=" + uin;
		return eMailBean.getBuzzwords(TextTemplateNames.BOUGHT_PRODUCT_EXTERN,
				al);
	}

	private Map<InvoiceField, Object> createMap(java.util.Locale locale,
			ShopProductDTO shopProductDTO) {
		Map<InvoiceField, Object> values = new HashMap<InvoiceField, Object>();
		values.put(
				InvoiceField.DATE,
				DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(
						new Date()));
		values.put(InvoiceField.AMOUNT, "1");
		values.put(
				InvoiceField.DELIVERYDATE,
				DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(
						new Date()));
		values.put(InvoiceField.INVOICENUMBER,
				invoiceBeanService.getNextInvoiceNumber());
		values.put(InvoiceField.PRODUCTNAME, shopProductDTO.getProduct()
				.getProductLocales()[0].getProductName());
		values.put(InvoiceField.TAXNUMBER, "777");
		values.put(InvoiceField.TAXRATE, "19%");
		return values;
	}

	private void updateProduct(ShopProductDTO shopProductDTO) {
		ShopProduct shopProduct = shopProductCRUDBean
				.findByProductUinAndProductSystem(shopProductDTO.getProduct()
						.getUin(), getSystem(shopProductDTO.getProduct()
						.getSystem()));
		shopProduct.getProduct().setCurrAmount(
				shopProduct.getProduct().getCurrAmount() + 1);
		shopProductCRUDBean.update(shopProduct);
	}

	private PersonalInformation createPersonalInformation(
			PersonalInformationDTO personalInformationDTO) {
		PersonalInformation personalInformation = new PersonalInformation();
		personalInformation.fromDto(personalInformationDTO);
		Locale localeEntity = localeBeanRemote.findByCode(personalInformation
				.getLocale().getLocalecode());
		personalInformation.setLocale(localeEntity);
		return personalInformation;
	}

	private ESystem getSystem(ESystemDTO systemDTO) {
		return eSystemBeanRemote.findByName(systemDTO.getName());
	}

	private ProductLinkSAShop createProductLink(ShopProductDTO shopProductDTO) {
		ShopProduct shopProduct = shopProductCRUDBean
				.findByProductUinAndProductSystem(shopProductDTO.getProduct()
						.getUin(), getSystem(shopProductDTO.getProduct()
						.getSystem()));
		ProductLinkSAShop productLink = new ProductLinkSAShop();
		productLink.setShopProduct(shopProduct);
		productLink.setUin(getUin());
		return productLink;
	}

	private AccountSAShop createAccount(
			PersonalInformation personalInformation,
			ProductLinkSAShop productLinkSAShop) {
		AccountSAShop account = new AccountSAShop();
		account.setPersonalInformation(personalInformation);
		account.setUsername(personalInformation.getEmail());
		account.setProductLink(productLinkSAShop);
		accountSAShopBeanLocal.update(account);
		return account;
	}

	private String getUin() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
