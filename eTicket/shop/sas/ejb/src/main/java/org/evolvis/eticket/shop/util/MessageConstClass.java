package org.evolvis.eticket.shop.util;


public final class MessageConstClass {

	private MessageConstClass() {
		// TODO Auto-generated constructor stub
	}

	public static final String PASSWORD_MUST_SEND_AS_PLAINTEXT = "The password of this user must send as plaintext in order to check it";
	public static final String USER_UNKNOWN_OR_PASSWORD_INVALID = "The user is unknown or the password is invalid";
}
