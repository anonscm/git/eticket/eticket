package org.evolvis.eticket.shop.util;

import java.util.UUID;

public class GenerateToken {
	private GenerateToken() {

	}

	public static String generateToken() {
		return generateUUIDToken();
	}

	static String generateUUIDToken() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
//	static String generateTimeHashToken(){
//		return Hash.getHash(""+GregorianCalendar.getInstance().getTime().getTime());
//	}
	

}
