package org.evolvis.eticket.shop.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import org.evolvis.eticket.model.entity.Product;
import org.evolvis.eticket.model.entity.dto.ProductSAShopDTO;

@Entity
@NamedQueries({
	@NamedQuery(name = "findSalableProductSAShop", query = "SELECT p FROM ProductSAShop p WHERE p.active = true AND p.currentAmount <> p.maxAmount" ),
	@NamedQuery(name = "findProductSAShopByActiveState", query = "SELECT p FROM ProductSAShop p WHERE p.active = ?1")
})
public class ProductSAShop {
	
	private long id;
	private Product product;
	private boolean active;
	private int maxAmount;
	private int currentAmount;
	
	public ProductSAShop() {
		
	}

	public ProductSAShop(ProductSAShopDTO productSAShop) {
		fromDto(productSAShop);
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(nullable = false)
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(nullable = false)
	public int getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(int maxAmount) {
		this.maxAmount = maxAmount;
	}

	@Column(nullable = false)
	public int getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(int currentAmount) {
		this.currentAmount = currentAmount;
	}	
	
	@Transient
	public void fromDto(ProductSAShopDTO productSAShopDTO){
		product = new Product(productSAShopDTO.getProductDTO());
		//active
		maxAmount = productSAShopDTO.getMaxAmount();
		currentAmount = productSAShopDTO.getCurrentAmount();
		
		if(productSAShopDTO.getId() > -1) {
			id = productSAShopDTO.getId();
		}
	}
	
	@Transient
	public ProductSAShopDTO toDTO(){
		return ProductSAShopDTO.createProductSAShopDTOFromExsistingProductSAShopEntity(id, product.toDto(), active, maxAmount, currentAmount);
	}	
}