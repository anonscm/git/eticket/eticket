package org.evolvis.eticket.shop.business.pdf;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.output.OutputException;

import org.evolvis.eticket.business.pdf.PrintProductPdfBeanService;
import org.evolvis.eticket.business.pdf.PrintSAShopProductPdfBeanService;
import org.evolvis.eticket.business.product.ProductNumberBeanService;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.product.ProductLocaleCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.PdfField;
import org.evolvis.eticket.model.entity.ProductLocale;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticketshop.model.crud.product.ShopProductCRUD;
import org.evolvis.eticketshop.model.entity.ShopProduct;

@Stateless
@Remote(PrintSAShopProductPdfBeanService.class)
public class PrintSAShopProductPdfBean implements
                PrintSAShopProductPdfBeanService {
	@EJB
	private PrintProductPdfBeanService printProductPdfBean;
	@EJB
	private ShopProductCRUD shopProductCRUDBean;
	@EJB
	private ProductLocaleCRUD productLocaleCRUDBean;
	@EJB
	private LocaleCRUD localeCRUDBean;
	@EJB
	private ESystemCRUD eSystemCRUDBean;
         
    @Override
    public byte[] createPDF(PersonalInformationDTO personalInformationDTO, String uin,
                     boolean barcode, String locale, ESystemDTO eSystemDTO, String ProductNumber) {
    	ESystem system = eSystemCRUDBean.findByName(eSystemDTO.getName());
    	ShopProduct shopProduct = shopProductCRUDBean.findByProductUinAndProductSystem(uin, system);
		Locale localeEntity = localeCRUDBean.findByCode(locale);
		ProductLocale productLocale = productLocaleCRUDBean.findByLocaleCodeAndProductId(localeEntity, shopProduct.getProduct().getId());             
		try {
			return printProductPdfBean.createPDF(createMap(personalInformationDTO), productLocale.getTemplate().toDto(), ProductNumber, system.toDto());
		} catch (OutputException e) {
	    	throw new IllegalArgumentException(e);
		} catch (BarcodeException e) {
	    	throw new IllegalArgumentException(e);
	    }
    }
     
    private Map<PdfField, Object> createMap(
                    PersonalInformationDTO personalInformationDTO) throws OutputException, BarcodeException {
             Map<PdfField, Object> values = new HashMap<PdfField, Object>();
             values.put(PdfField.FIRSTNAME, personalInformationDTO.getFirstname());
             values.put(PdfField.LASTNAME, personalInformationDTO.getLastname());
             values.put(PdfField.CITY, personalInformationDTO.getCity());
             values.put(PdfField.ADRESS, personalInformationDTO.getAddress());
             values.put(PdfField.ORGA, personalInformationDTO.getOrganisation());
             values.put(PdfField.ZIP, personalInformationDTO.getZipcode());
             values.put(PdfField.BARCODE, true);
            return values;
    }
}
