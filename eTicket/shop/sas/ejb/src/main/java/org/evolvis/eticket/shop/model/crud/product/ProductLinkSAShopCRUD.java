package org.evolvis.eticket.shop.model.crud.product;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.shop.model.entity.ProductLinkSAShop;

public interface ProductLinkSAShopCRUD extends GenericCUD<ProductLinkSAShop> {

	public ProductLinkSAShop findByUin(String uin);

}
