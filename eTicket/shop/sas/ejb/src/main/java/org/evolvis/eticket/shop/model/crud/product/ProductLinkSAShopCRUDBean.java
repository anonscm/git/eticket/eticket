package org.evolvis.eticket.shop.model.crud.product;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.shop.model.entity.ProductLinkSAShop;

@Stateless
@Local(ProductLinkSAShopCRUD.class)
public class ProductLinkSAShopCRUDBean extends
		GenericCUDSkeleton<ProductLinkSAShop> implements ProductLinkSAShopCRUD {

	private static final long serialVersionUID = 9180685585815097290L;

	@Override
	public ProductLinkSAShop findByUin(String uin) {
		return buildNamedQueryGetSingleResult("findProductLinkSAShopByUin", uin);
	}

}
