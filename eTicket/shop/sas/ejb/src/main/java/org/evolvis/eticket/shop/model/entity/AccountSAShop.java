package org.evolvis.eticket.shop.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.evolvis.eticket.model.entity.PersonalInformation;
import org.evolvis.eticket.model.entity.dto.AccountSAShopDTO;
import org.evolvis.eticketpayment.model.entity.PayPalOrder;

@Entity
@NamedQueries({
	@NamedQuery(name = "findAccountSAShopByUin", query = "SELECT a FROM AccountSAShop a INNER JOIN a.productLink p WHERE p.uin = ?1"),
	@NamedQuery(name = "findAccountSAShopByUsername", query = "SELECT a FROM AccountSAShop a WHERE a.username = ?1"),
	@NamedQuery(name = "findAccountSAShopByPayPalOrder", query="SELECT a FROM AccountSAShop a WHERE a.payPalOrder = ?1")
})
public class AccountSAShop {
	
	private long id;
	private String username;
	private ProductLinkSAShop productLink;
	private PersonalInformation personalInformation;
	private PayPalOrder payPalOrder;
	
	public AccountSAShop() {
		super();
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@OneToOne(cascade = (CascadeType.ALL))
	public ProductLinkSAShop getProductLink() {
		return productLink;
	}

	public void setProductLink(ProductLinkSAShop productLink) {
		this.productLink = productLink;
	}
	
	@OneToOne
	public PayPalOrder getPayPalOrder() {
		return payPalOrder;
	}

	public void setPayPalOrder(PayPalOrder payPalOrder) {
		this.payPalOrder = payPalOrder;
	}

	@OneToOne(cascade = CascadeType.ALL)
	public PersonalInformation getPersonalInformation() {
		return personalInformation;
	}

	public void setPersonalInformation(PersonalInformation personalInformation) {
		this.personalInformation = personalInformation;
	}	
	
	@Transient
	public void fromDto(AccountSAShopDTO accountSAShopDTO){
		
		personalInformation = new PersonalInformation(accountSAShopDTO.getPersonalInformation());
		productLink = new ProductLinkSAShop(accountSAShopDTO.getProductLink());
		username = accountSAShopDTO.getUsername();
		
		if(accountSAShopDTO.getId() > -1) {
			id = accountSAShopDTO.getId();
		}
	}
	
	@Transient
	public AccountSAShopDTO toDto(){
		return AccountSAShopDTO.createAccountSAShopDTOFromExsistingAccountSAShopEntity(id, username, productLink.toDto(), personalInformation.toDto()) ;
	}
}
