package org.evolvis.eticket.shop.model.crud.user;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.shop.model.entity.AccountSAShop;
import org.evolvis.eticketpayment.model.entity.PayPalOrder;

public interface AccountSAShopCRUD extends GenericCUD<AccountSAShop>{
	
	public AccountSAShop findByUin(String uin);
	
	public List<AccountSAShop> findByUsername(String username);
	
	public AccountSAShop findByPayPalOrder(PayPalOrder order);	

}
