package org.evolvis.eticket.shop.business.account;

import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.evolvis.eticket.business.account.AccountSAShopBeanService;
import org.evolvis.eticket.business.product.ProductNumberBeanService;
import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.PersonalInformation;
import org.evolvis.eticket.model.entity.dto.AccountSAShopDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticket.shop.model.crud.user.AccountSAShopCRUD;
import org.evolvis.eticket.shop.model.entity.AccountSAShop;
import org.evolvis.eticket.shop.model.entity.ProductLinkSAShop;
import org.evolvis.eticketpayment.business.paypalorder.PayPalOrderBeanService;
import org.evolvis.eticketpayment.model.crud.paypalorder.PayPalOrderCRUD;
import org.evolvis.eticketpayment.model.entity.PayPalOrder;
import org.evolvis.eticketpayment.model.entity.dto.PayPalOrderDTO;
import org.evolvis.eticketshop.model.crud.product.ShopProductCRUD;
import org.evolvis.eticketshop.model.entity.ShopProduct;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class AccountSAShopBean.
 */
@Stateless
@Remote(AccountSAShopBeanService.class)
public class AccountSAShopBean implements AccountSAShopBeanService {

	/** The account sa shop crud local. */
	@EJB
	private AccountSAShopCRUD accountSAShopCRUDLocal;
	@EJB
	private AccountSAShopCRUD accountSAShopBeanLocal;
	@EJB
	private LocaleCRUD localeBeanRemote;
	@EJB
	private PayPalOrderCRUD payPalOrderCRUDBeanRemote;
	@EJB
	private ShopProductCRUD shopProductCRUDBean;
	@EJB
	private ProductNumberBeanService productNumberBeanService;
	

	@EJB
	private ESystemCRUD eSystemBeanRemote;	

	/* (non-Javadoc)
	 * @see org.evolvis.eticket.business.account.AccountSAShopBeanService#getAccount(java.lang.String)
	 */
	@Override
	public AccountSAShopDTO getAccount(String uin) {
		return accountSAShopCRUDLocal.findByUin(uin).toDto();
	}

	@Override
	public void cerate(PersonalInformationDTO personalInformation,
			ShopProductDTO shopProductDTO, PayPalOrderDTO payPalOrderDTO) {
		AccountSAShop account = new AccountSAShop();
		account.setPersonalInformation(createPersonalInformation(personalInformation, payPalOrderDTO.getPayPalEmailAddress()));
		account.setUsername(payPalOrderDTO.getPayPalEmailAddress());
		account.setPayPalOrder(getPayPalOrder(payPalOrderDTO));
		account.setProductLink(createProductLink(shopProductDTO));
		accountSAShopBeanLocal.insert(account);
	}
	
	@Override
	public AccountSAShopDTO getAccount(PayPalOrderDTO payPalOrderDTO) {
		PayPalOrder order = payPalOrderCRUDBeanRemote.findById(payPalOrderDTO.getId());
		return accountSAShopCRUDLocal.findByPayPalOrder(order).toDto();
	}
	
	private PersonalInformation createPersonalInformation(
			PersonalInformationDTO personalInformationDTO, String eMail) {
		PersonalInformation personalInformation = new PersonalInformation();
		personalInformation.fromDto(personalInformationDTO);
		Locale localeEntity = localeBeanRemote.findByCode(personalInformation
				.getLocale().getLocalecode());
		personalInformation.setLocale(localeEntity);
		personalInformation.setEmail(eMail);
		return personalInformation;
	}	
	
	private PayPalOrder getPayPalOrder(PayPalOrderDTO payPalOrderDTO){
		return payPalOrderCRUDBeanRemote.findById(payPalOrderDTO.getId());
	}
	
	private ProductLinkSAShop createProductLink(ShopProductDTO shopProductDTO) {
		ShopProduct shopProduct = shopProductCRUDBean
				.findByProductUinAndProductSystem(shopProductDTO.getProduct()
						.getUin(), getSystem(shopProductDTO.getProduct()
						.getSystem()));
		ProductLinkSAShop productLink = new ProductLinkSAShop();
		productLink.setShopProduct(shopProduct);
		productLink.setUin(getUin());
		productLink.setProductNumber(productNumberBeanService.getProductNumber(shopProduct.getProduct().toDto()));
		return productLink;
	}
	
	private String getUin() {
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	private ESystem getSystem(ESystemDTO systemDTO) {
		return eSystemBeanRemote.findByName(systemDTO.getName());
	}	
}
