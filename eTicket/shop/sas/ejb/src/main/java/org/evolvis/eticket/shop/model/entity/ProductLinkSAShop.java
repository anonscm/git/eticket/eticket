package org.evolvis.eticket.shop.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.evolvis.eticket.model.entity.dto.ProductLinkSAShopDTO;
import org.evolvis.eticketshop.model.entity.ShopProduct;

@Entity
@Table(name="ProductLinkSAShop")
@NamedQuery(name="findProductLinkSAShopByUin", query = "SELECT l FROM ProductLinkSAShop l WHERE  uin = ?1")
public class ProductLinkSAShop {

	private long id;
	private String uin;
	private ShopProduct shopProduct;
	private String productNumber;
	
	public ProductLinkSAShop() {
		
	}

	public ProductLinkSAShop(ProductLinkSAShopDTO linkSAShopDTO) {
		fromDTO(linkSAShopDTO);
		
	}
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}


	@Column(nullable = false)
	public String getUin() {
		return uin;
	}

	public void setUin(String uin) {
		this.uin = uin;
	}

	@ManyToOne
	public ShopProduct getShopProduct() {
		return shopProduct;
	}

	public void setShopProduct(ShopProduct shopProduct) {
		this.shopProduct = shopProduct;
	}
	
	public String getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}

	@Transient
	public void fromDTO(ProductLinkSAShopDTO linkSAShopDTO){
		uin = linkSAShopDTO.getUin();
		shopProduct = new ShopProduct(linkSAShopDTO.getShopProductDTO());
		productNumber = linkSAShopDTO.getProductNumber();	
		if(linkSAShopDTO.getId() > -1) {
			id = linkSAShopDTO.getId();
		}	
	}

	@Transient
	public ProductLinkSAShopDTO toDto(){
		return ProductLinkSAShopDTO.createAccountDTOFromExsistingAccountEntity(id, uin,shopProduct.toDto(), productNumber);
	}

}
