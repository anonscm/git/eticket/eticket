/**
 * 
 */
package org.evolvis.eticket.shop.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

/**
 * The Class GenericServiceLoader, loads given classes with the ServiceLoader so
 * the client must not ensure the life cycle of a implementation.
 * 
 * @author peder
 */
public class GenericServiceLoader {

	/**
	 * Gets the first implementation of a given interface class, the
	 * implementation must be registered in META-INF/services.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz of the interface.
	 * @return the implementation of given interface registered in
	 *         META-INF/services
	 */
	public static <T> T get(final Class<?> clazz) {
		final Iterator<T> iterator = getIterator(clazz);
		if (iterator.hasNext()) {
			return iterator.next();
		}
		return null;
	}

	/**
	 * Gets all implementations of a given interface class, the implementations
	 * must be registered in META-INF/services.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @return the implementations of given interface registered in
	 *         META-INF/services
	 */
	public static <T> List<T> getAll(final Class<?> clazz) {
		final Iterator<T> iterator = getIterator(clazz);
		final List<T> services = new ArrayList<T>();
		while (iterator.hasNext()) {
				services.add(iterator.next());
		}
		return services;
	}

	/**
	 * Gets the iterator of known implementations of a given interface.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @return the iterator
	 */
	private static <T> Iterator<T> getIterator(final Class<?> clazz) {
		@SuppressWarnings("unchecked")
		final Iterator<T> iterator = (Iterator<T>) ServiceLoader.load(clazz)
				.iterator();
		return iterator;
	}

	/**
	 * Instantiates a new generic service loader.
	 */
	private GenericServiceLoader() {

	}

	/**
	 * returns a instance of specific implementation of given interface/abstract class.
	 * 
	 * @param <T>
	 * @param interf
	 * @param impl
	 * @return
	 */
	public static <T> T get(Class<?> interf, Class<?> impl) {
		final Iterator<T> iterator = getIterator(interf);
		while (iterator.hasNext()) {
			T object = iterator.next();
			if (object.getClass().equals(impl)) {
				return object;
			}
		}
		return null;
	}
}
