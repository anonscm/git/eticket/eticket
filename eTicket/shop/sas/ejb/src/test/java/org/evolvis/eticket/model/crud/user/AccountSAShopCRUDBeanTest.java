package org.evolvis.eticket.model.crud.user;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.shop.model.crud.user.AccountSAShopCRUDBean;
import org.junit.Before;
import org.junit.Test;

public class AccountSAShopCRUDBeanTest {
	
	private AccountSAShopCRUDBean bean = new AccountSAShopCRUDBean();
	private EntityManager manager = mock(EntityManager.class);
	private Query query = mock(Query.class);
	
	@Before
	public void init() {
		bean.setManager(manager);
		when(manager.createNamedQuery(anyString())).thenReturn(query);
	}
	
	@Test
	public void shouldFindByUin(){
		bean.findByUin("123");
		verify(manager).createNamedQuery("findAccountSAShopByUin");
		verify(query).getSingleResult();
	}
	
	@Test
	public void shouldFindByUsername(){
		bean.findByUsername("a");
		verify(manager).createNamedQuery("findAccountSAShopByUsername");
		verify(query).getResultList();
	}

}
