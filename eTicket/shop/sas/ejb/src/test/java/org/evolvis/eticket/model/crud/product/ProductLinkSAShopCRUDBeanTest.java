package org.evolvis.eticket.model.crud.product;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.shop.model.crud.product.ProductLinkSAShopCRUDBean;
import org.junit.Before;
import org.junit.Test;

public class ProductLinkSAShopCRUDBeanTest {
	
	private ProductLinkSAShopCRUDBean bean = new ProductLinkSAShopCRUDBean();
	private EntityManager manager = mock(EntityManager.class);
	private Query query = mock(Query.class);
	
	@Before
	public void init() {
		bean.setManager(manager);
		when(manager.createNamedQuery(anyString())).thenReturn(query);
	}
	
	@Test
	public void shouldFindByUin() {
		bean.findByUin("123");
		verify(manager).createNamedQuery("findProductLinkSAShopByUin");
		verify(query).getSingleResult();
	}

}
