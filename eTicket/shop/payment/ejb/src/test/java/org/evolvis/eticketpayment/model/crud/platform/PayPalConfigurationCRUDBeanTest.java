package org.evolvis.eticketpayment.model.crud.platform;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.junit.Before;
import org.junit.Test;

public class PayPalConfigurationCRUDBeanTest {
	private PayPalConfigurationCRUDBean payPalConfigurationCRUDBean = new PayPalConfigurationCRUDBean();
	private EntityManager manager = mock(EntityManager.class);
	private Query query = mock(Query.class);

	@Before
	public void init() {
		preparePayPalConfigurationCRUDBean();
	}

	private void preparePayPalConfigurationCRUDBean() {
		payPalConfigurationCRUDBean.setManager(manager);
		when(manager.createNamedQuery(anyString())).thenReturn(query);
	}
	
	@Test
	public void shouldFindPayPalConfigurationParameterBySystemAndKeyAndProductAssociation() {
		payPalConfigurationCRUDBean.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(new ESystem(), "key", ProductAssociation.SHOP_INTERN);
		verify(manager).createNamedQuery("findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation");
	}

	@Test
	public void shouldListPayPalConfigurationParameterBySystemAndProductAssociation() {
		payPalConfigurationCRUDBean.listPayPalConfigurationParameterBySystemAndProductAssociation(new ESystem(), ProductAssociation.SHOP_INTERN);
		verify(manager).createNamedQuery("listPayPalConfigurationParameterBySystemAndProductAssociation");
		
	}
}
