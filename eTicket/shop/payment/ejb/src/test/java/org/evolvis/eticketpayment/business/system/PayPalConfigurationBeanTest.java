package org.evolvis.eticketpayment.business.system;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;

import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.Platform;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticketpayment.model.crud.platform.PayPalConfigurationCRUD;
import org.evolvis.eticketpayment.model.entity.PayPalConfiguration;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class PayPalConfigurationBeanTest {

	private PayPalConfigurationBean payPalConfigurationBean = new PayPalConfigurationBean();

	private PayPalConfigurationCRUD payPalConfigurationCRUD = mock(PayPalConfigurationCRUD.class);
	private ESystemCRUD eSystemCRUD = mock(ESystemCRUD.class);

	private ESystem eSystem;
	private LocaleDTO localeDTO = LocaleDTO
			.createLocaleDTOFromExsistingLocaleEntity(2, "de", "deutsch");
	private ESystemDTO eSystemDTO = ESystemDTO
			.createESystemDTOFromExsistingESystemEntity(99, "mhm", localeDTO,
					null);

	private PayPalConfiguration payPalConfiguration_okay1 = new PayPalConfiguration();
	private PayPalConfiguration payPalConfiguration_okay2 = new PayPalConfiguration();
	private PayPalConfiguration payPalConfiguration_fail = new PayPalConfiguration();

	private ArrayList<PayPalConfiguration> payPalConfigurationList = new ArrayList<PayPalConfiguration>();

	@Before
	public void init() {
		preparePayPalConfiguration();
		prepareSystem();
		preparepayPalConfigurationBean();
		prepareReturns();
	}

	private void preparePayPalConfiguration() {
		payPalConfiguration_okay1.setId(1);
		payPalConfiguration_okay1.setKey("TEST");
		payPalConfiguration_okay1.setValue("1234567890");
		payPalConfiguration_okay1
				.setProductAssociation(ProductAssociation.SHOP_INTERN);
		payPalConfiguration_okay1.setSystem(eSystem);

		payPalConfiguration_okay2.setId(1);
		payPalConfiguration_okay2.setKey("TEST31");
		payPalConfiguration_okay2.setValue("abcdefghij");
		payPalConfiguration_okay2
				.setProductAssociation(ProductAssociation.SHOP_INTERN);
		payPalConfiguration_okay2.setSystem(eSystem);

		payPalConfigurationList.add(payPalConfiguration_okay1);
		payPalConfigurationList.add(payPalConfiguration_okay2);

		payPalConfiguration_fail.setId(1);
		payPalConfiguration_fail.setKey("TEST");
		payPalConfiguration_fail.setValue("1234567890");
		payPalConfiguration_fail.setSystem(eSystem);
	}

	private void prepareSystem() {
		eSystem = new ESystem();
		eSystem.setName(eSystemDTO.getName());
		eSystem.setPlatform(new Platform("aha"));
		eSystem.setDefaultLocale(new Locale(LocaleDTO.createNewLocaleDTO("DE",
				"...")));
	}

	private void preparepayPalConfigurationBean() {
		payPalConfigurationBean.seteSystemCRUDBeanLocal(eSystemCRUD);
		payPalConfigurationBean
				.setPayPalConfigurationCRUD(payPalConfigurationCRUD);
	}

	private void prepareReturns() {
		Mockito.when(eSystemCRUD.findById(eSystemDTO.getId())).thenReturn(
				eSystem);

		Mockito.when(eSystemCRUD.findByName(eSystemDTO.getName())).thenReturn(
				eSystem);

		Mockito.when(
				payPalConfigurationCRUD
						.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
								eSystem, payPalConfiguration_okay1.getKey(),
								payPalConfiguration_okay1
										.getProductAssociation())).thenReturn(
				payPalConfiguration_okay1);

		Mockito.when(
				payPalConfigurationCRUD
						.listPayPalConfigurationParameterBySystemAndProductAssociation(
								eSystem, payPalConfiguration_okay1
										.getProductAssociation())).thenReturn(
				payPalConfigurationList);
	}

	@Test
	public void shouldGetPayPalConfigurationParameterBySystemAndKeyAndProductAssociation() {
		payPalConfigurationBean
				.getPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
						eSystemDTO, payPalConfiguration_okay1.getKey(),
						payPalConfiguration_okay1.getProductAssociation());
		verify(eSystemCRUD).findByName(eSystemDTO.getName());
		verify(payPalConfigurationCRUD)
				.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
						eSystem, payPalConfiguration_okay1.getKey(),
						payPalConfiguration_okay1.getProductAssociation());
	}

	@Test
	public void shouldInsertConfiguration() {
		String key = "key123";
		String value = "value123";
		ProductAssociation productAssociation = ProductAssociation.SHOP_INTERN;

		payPalConfigurationBean.insertConfiguration(key, value,
				productAssociation, eSystemDTO);

		verify(eSystemCRUD).findByName(eSystemDTO.getName());

		verify(payPalConfigurationCRUD).insert(
				(PayPalConfiguration) anyObject());
	}
}
