package org.evolvis.eticketpayment.business.paypalorder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.Credentials;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.PersonalInformation;
import org.evolvis.eticket.model.entity.Platform;
import org.evolvis.eticket.model.entity.Role;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO;
import org.evolvis.eticket.model.entity.dto.ProductDTO.ProductType;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticket.model.entity.dto.RoleDTO;
import org.evolvis.eticket.model.entity.dto.TemplatesDTO;
import org.evolvis.eticket.util.CalculateHash;
import org.evolvis.eticketpayment.model.crud.orderinformation.OrderInformationCRUD;
import org.evolvis.eticketpayment.model.crud.paymenttransaction.PaymentTransactionCRUD;
import org.evolvis.eticketpayment.model.crud.paypalorder.PayPalOrderCRUD;
import org.evolvis.eticketpayment.model.crud.platform.PayPalConfigurationCRUD;
import org.evolvis.eticketpayment.model.entity.OrderStatus;
import org.evolvis.eticketpayment.model.entity.PayPalConfiguration;
import org.evolvis.eticketpayment.model.entity.PayPalOrder;
import org.evolvis.eticketpayment.model.entity.dto.OrderInformationDTO;
import org.evolvis.eticketpayment.model.entity.dto.PayPalOrderDTO;
import org.evolvis.eticketpayment.model.entity.dto.PaymentTransactionDTO;
import org.evolvis.eticketshop.model.crud.product.ShopProductCRUD;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.ShopProduct;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(PayPalOrderBean.class)
public class PayPalOrderBeanTest {
	// PayPalOrderBean and needed CRUD
	private PayPalOrderBean payPalOrderBean = new PayPalOrderBean();
	private PayPalOrderBean payPalOrderBeanPrivateMockingClass;
	private PayPalOrderCRUD paypalorderCRUDBeanLocal = mock(PayPalOrderCRUD.class);
	private PayPalConfigurationCRUD payPalConfigurationCRUD = mock(PayPalConfigurationCRUD.class);
	private ShopProductCRUD shopProductCRUDBeanLocal = mock(ShopProductCRUD.class);
	private ESystemCRUD eSystemCRUDBeanLocal = mock(ESystemCRUD.class);
	private PaymentTransactionCRUD paymentTransactionCRUD = mock(PaymentTransactionCRUD.class);
	private OrderInformationCRUD orderInformationCRUD = mock(OrderInformationCRUD.class);
	private AccountCRUD accountCRUDBean = mock(AccountCRUD.class);

	private static final String LOCALECODE = "de";

	// ESystemDTO and LocaleDTO
	private LocaleDTO localeDTO = LocaleDTO
			.createLocaleDTOFromExsistingLocaleEntity(2, "de", "deutsch");
	private ESystemDTO eSystemDTO = ESystemDTO
			.createESystemDTOFromExsistingESystemEntity(99, "mhm", localeDTO,
					null);

	// ESystem
	private ESystem eSystem;

	// AccountDTO
	private AccountDTO accountDTO = AccountDTO.init(eSystemDTO,
			"pAdmin", "jo", RoleDTO.getProductAdmin(0), "openId@OpenId.de");

	// Account
	private Account account;

	// TemplateDTO
	private TemplatesDTO templateDTO = TemplatesDTO.createNewTemplatesDTO(
			"test", "Hallo!".getBytes(), "example/plain", null);

	// ProductLocalesDTO
	private ProductLocaleDTO[] productlocales = { ProductLocaleDTO
			.createProductLocaleDTOFromExsistingProductLocaleEntity(22,
					localeDTO, "€", "ProductTest", "ProductDes", templateDTO,
					10.99, 19) };

	// ProductDTO
	private ProductDTO productDTO = ProductDTO
			.createProductDTOFromExsistingProductEntity("TP123",
					ProductType.DIGITAL, eSystemDTO, productlocales, 0, 20,
					null, 0, 51);

	// ShopProductDTO
	private ShopProductDTO shopProductDTO = ShopProductDTO
			.createNewShopProductDTO(productDTO, true,
					ProductAssociation.SHOP_INTERN);

	private ShopProduct shopProduct = new ShopProduct(shopProductDTO);

	// OrderInformationDTO
	private OrderInformationDTO orderInformationDTO = OrderInformationDTO
			.createNewOrderDTO(OrderStatus.PENDING, eSystemDTO, shopProductDTO,
					productlocales[0], 13);

	// PaymentTransactionDTO
	private PaymentTransactionDTO paymentTransactionDTO = PaymentTransactionDTO
			.createNewTransactionDTO(accountDTO);

	// PayPalOrderDTO okay
	private PayPalOrderDTO payPalOrderDTO_okay = PayPalOrderDTO
			.createPayPalOrderDTOFromExsistingPayPalOrderEntity(22, "token123",
					"Test@web.de", null, null, orderInformationDTO,
					paymentTransactionDTO);

	// PayPalOrderDTO complete
	private PayPalOrderDTO payPalOrderDTO_complete = PayPalOrderDTO
			.createPayPalOrderDTOFromExsistingPayPalOrderEntity(22, "token123",
					"Test@web.de", "payerid123", null, orderInformationDTO,
					paymentTransactionDTO);

	// PayPalOrderDTO missing paymentTransactionDTO
	private PayPalOrderDTO payPalOrderDTO_missing_paymentTransactionDTO = PayPalOrderDTO
			.createNewPayPalOrderDTO("token123", "Test@web.de", null, null,
					orderInformationDTO, null);

	// PayPalOrder okay
	PayPalOrder payPalOrder_okay = new PayPalOrder(payPalOrderDTO_okay);

	// PayPalOrder complete
	PayPalOrder payPalOrder_complete = new PayPalOrder(payPalOrderDTO_complete);

	@Before
	public void init() {
		preparePayPalOrderBean();
		preparePayPalOrderBeanTestClassForPrivateMocking();
		prepareSystem();
		prepareAccount();
		preparePayPalOrder();
		prepareReturns();
	}

	private void prepareSystem() {
		eSystem = new ESystem();
		eSystem.setName(eSystemDTO.getName());
		eSystem.setPlatform(new Platform("aha"));
		eSystem.setDefaultLocale(new Locale(LocaleDTO.createNewLocaleDTO(
				LOCALECODE, "...")));
	}

	private void prepareAccount() {
		account = new Account();
		String salt = CalculateHash.generateSalt();
		account.setCredential(new Credentials(
				accountDTO.getUsername(),
				salt,
				CalculateHash.calculateWithSalt(salt, accountDTO.getPassword()),
				null));
		account.setSystem(eSystem);

		PersonalInformation personalInformation = new PersonalInformation();
		personalInformation.setAddress("");
		personalInformation.setCity("");
		personalInformation.setCountry("");
		personalInformation.setEmail("");
		personalInformation.setFirstname("");
		personalInformation.setLastname("");
		personalInformation.setLocale(new Locale(LocaleDTO.createNewLocaleDTO(
				LOCALECODE, "...")));
		personalInformation.setOrganisation("");
		personalInformation.setSalutation("");
		personalInformation.setTitle("");
		personalInformation.setZip("");

		account.setPersonalInformation(personalInformation);

		List<Role> roles = new ArrayList<Role>();
		roles.add(new Role());
		account.setRoles(roles);
	}

	private void preparePayPalOrder() {
		// TODO Account Problem - AccountDTO and Account must be fixed so that
		// Account.fromDto and Account.toDto can be used.
		payPalOrder_okay.getPaymentTransaction().setAccount(account);
		payPalOrder_complete.getPaymentTransaction().setAccount(account);
	}

	private void preparePayPalOrderBeanTestClassForPrivateMocking() {
		payPalOrderBeanPrivateMockingClass = PowerMockito
				.spy(new PayPalOrderBean());
		payPalOrderBeanPrivateMockingClass.setAccountCRUDBean(accountCRUDBean);
		payPalOrderBeanPrivateMockingClass
				.seteSystemCRUDBeanLocal(eSystemCRUDBeanLocal);
		payPalOrderBeanPrivateMockingClass
				.setOrderInformationCRUD(orderInformationCRUD);
		payPalOrderBeanPrivateMockingClass
				.setPaymentTransactionCRUD(paymentTransactionCRUD);
		payPalOrderBeanPrivateMockingClass
				.setPaypalorderCRUDBeanLocal(paypalorderCRUDBeanLocal);
		payPalOrderBeanPrivateMockingClass
				.setShopProductCRUDBeanLocal(shopProductCRUDBeanLocal);
		payPalOrderBeanPrivateMockingClass
				.setPayPalConfigurationCRUD(payPalConfigurationCRUD);
	}

	private void prepareReturns() {
		Mockito.when(eSystemCRUDBeanLocal.findById(eSystemDTO.getId()))
				.thenReturn(eSystem);

		Mockito.when(
				paypalorderCRUDBeanLocal.findById(payPalOrderDTO_okay.getId()))
				.thenReturn(payPalOrder_okay);

		Mockito.when(eSystemCRUDBeanLocal.findByName(eSystemDTO.getName()))
				.thenReturn(eSystem);

		Mockito.when(
				shopProductCRUDBeanLocal.findByProductUinAndProductSystem(
						productDTO.getUin(), eSystem)).thenReturn(shopProduct);

		Mockito.when(
				accountCRUDBean.findBySystemAndUserName((ESystem) anyObject(),
						eq(accountDTO.getUsername()))).thenReturn(account);

		preparePayPalconnection();
	}

	private void preparePayPalconnection() {

		final String USER = "USER";
		final String PWD = "PWD";
		final String SIGNATURE = "SIGNATURE";
		final String RETURNURL = "RETURNURL";
		final String CANCELURL = "CANCELURL";
		final String PAYPALAPI = "PAYPALAPI";
		final String PAYPALPAY = "PAYPALPAY";
		final String VERSION = "VERSION";
		final String PAYMENTREQUEST_0_PAYMENTACTION = "PAYMENTREQUEST_0_PAYMENTACTION";
		PayPalConfiguration payPalConfiguration = null;

		// Property-File
		payPalConfiguration = new PayPalConfiguration();
		payPalConfiguration.setValue("b.krap_1327405301_biz_api1.tarent.de");
		Mockito.when(
				payPalConfigurationCRUD
						.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
								(ESystem) anyObject(), eq(USER),
								(ProductAssociation) anyObject())).thenReturn(
				payPalConfiguration);

		payPalConfiguration = new PayPalConfiguration();
		payPalConfiguration.setValue("1327405331");
		Mockito.when(
				payPalConfigurationCRUD
						.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
								(ESystem) anyObject(), eq(PWD),
								(ProductAssociation) anyObject())).thenReturn(
				payPalConfiguration);

		payPalConfiguration = new PayPalConfiguration();
		payPalConfiguration
				.setValue("Aj.VSuWefenYfx9Vhzix0K1gYHDaAUTRZDpu0l4rfmShkjJoIbd3n4Vx");
		Mockito.when(
				payPalConfigurationCRUD
						.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
								(ESystem) anyObject(), eq(SIGNATURE),
								(ProductAssociation) anyObject())).thenReturn(
				payPalConfiguration);

		payPalConfiguration = new PayPalConfiguration();
		payPalConfiguration
				.setValue("https://eticket.linuxtag.org/LinuxTagUserWeb/dispatcher/shop?completePayPalCheckout");
		Mockito.when(
				payPalConfigurationCRUD
						.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
								(ESystem) anyObject(), eq(RETURNURL),
								(ProductAssociation) anyObject())).thenReturn(
				payPalConfiguration);

		payPalConfiguration = new PayPalConfiguration();
		payPalConfiguration
				.setValue("https://eticket.linuxtag.org/LinuxTagUserWeb/dispatcher/shop?canclepayment");
		Mockito.when(
				payPalConfigurationCRUD
						.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
								(ESystem) anyObject(), eq(CANCELURL),
								(ProductAssociation) anyObject())).thenReturn(
				payPalConfiguration);

		payPalConfiguration = new PayPalConfiguration();
		payPalConfiguration.setValue("https://api-3t.sandbox.paypal.com/nvp");
		Mockito.when(
				payPalConfigurationCRUD
						.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
								(ESystem) anyObject(), eq(PAYPALAPI),
								(ProductAssociation) anyObject())).thenReturn(
				payPalConfiguration);

		payPalConfiguration = new PayPalConfiguration();
		payPalConfiguration
				.setValue("https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&useraction=commit&token=");
		Mockito.when(
				payPalConfigurationCRUD
						.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
								(ESystem) anyObject(), eq(PAYPALPAY),
								(ProductAssociation) anyObject())).thenReturn(
				payPalConfiguration);

		payPalConfiguration = new PayPalConfiguration();
		payPalConfiguration.setValue("84.0");
		Mockito.when(
				payPalConfigurationCRUD
						.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
								(ESystem) anyObject(), eq(VERSION),
								(ProductAssociation) anyObject())).thenReturn(
				payPalConfiguration);

		payPalConfiguration = new PayPalConfiguration();
		payPalConfiguration.setValue("Sale");
		Mockito.when(
				payPalConfigurationCRUD
						.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
								(ESystem) anyObject(),
								eq(PAYMENTREQUEST_0_PAYMENTACTION),
								(ProductAssociation) anyObject())).thenReturn(
				payPalConfiguration);

		// URLConnection
		URLConnection uRLConnection;
		URL uRL;
		try {
			uRL = new URL("https://api-3t.sandbox.paypal.com/nvp");
			uRLConnection = uRL.openConnection();
			PowerMockito.doReturn(uRLConnection).when(
					payPalOrderBeanPrivateMockingClass, "paypalURLConnection");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void preparePayPalOrderBean() {
		payPalOrderBean.setAccountCRUDBean(accountCRUDBean);
		payPalOrderBean.seteSystemCRUDBeanLocal(eSystemCRUDBeanLocal);
		payPalOrderBean.setOrderInformationCRUD(orderInformationCRUD);
		payPalOrderBean.setPaymentTransactionCRUD(paymentTransactionCRUD);
		payPalOrderBean.setPaypalorderCRUDBeanLocal(paypalorderCRUDBeanLocal);
		payPalOrderBean.setShopProductCRUDBeanLocal(shopProductCRUDBeanLocal);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotCreate1() {
		PayPalOrderDTO.createNewPayPalOrderDTO(null, null, null, null,
				orderInformationDTO, paymentTransactionDTO);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotCreate2() {
		PayPalOrderDTO.createNewPayPalOrderDTO("token123", null, null, null,
				null, paymentTransactionDTO);
	}

	@Test
	public void shouldCreate() {
		// Should create
		InOrder inOrder = Mockito.inOrder(eSystemCRUDBeanLocal,
				eSystemCRUDBeanLocal, shopProductCRUDBeanLocal,
				accountCRUDBean, paypalorderCRUDBeanLocal);

		payPalOrderBean.create(null, payPalOrderDTO_okay);

		inOrder.verify(eSystemCRUDBeanLocal).findByName("mhm");

		inOrder.verify(eSystemCRUDBeanLocal).findById(99);

		inOrder.verify(shopProductCRUDBeanLocal)
				.findByProductUinAndProductSystem("TP123", eSystem);

		inOrder.verify(accountCRUDBean).findBySystemAndUserName(
				(ESystem) anyObject(), eq("pAdmin"));

		inOrder.verify(paypalorderCRUDBeanLocal).insert(
				(PayPalOrder) anyObject());
	}

	@Test
	public void shouldCreateWithMissingTransactionDTO() {
		// Should create, although paymentTransactionDTO is missing
		InOrder inOrder = Mockito.inOrder(eSystemCRUDBeanLocal,
				shopProductCRUDBeanLocal, accountCRUDBean,
				paypalorderCRUDBeanLocal);

		payPalOrderBean.create(null,
				payPalOrderDTO_missing_paymentTransactionDTO);

		inOrder.verify(eSystemCRUDBeanLocal).findByName("mhm");

		inOrder.verify(eSystemCRUDBeanLocal).findById(99);

		inOrder.verify(shopProductCRUDBeanLocal)
				.findByProductUinAndProductSystem("TP123", eSystem);

		inOrder.verify(accountCRUDBean, Mockito.never())
				.findBySystemAndUserName(new ESystem(accountDTO.getSystem()),
						accountDTO.getUsername());

		inOrder.verify(paypalorderCRUDBeanLocal).insert(
				(PayPalOrder) anyObject());
	}

	@Test
	public void shouldUpdate() {
		InOrder inOrder = Mockito.inOrder(paypalorderCRUDBeanLocal,
				paypalorderCRUDBeanLocal);

		// changed payerId
		PayPalOrderDTO dto = PayPalOrderDTO
				.createPayPalOrderDTOFromExsistingPayPalOrderEntity(
						payPalOrderDTO_okay.getId(),
						payPalOrderDTO_okay.getToken(),
						payPalOrderDTO_okay.getPayPalEmailAddress(), "ppp123",
						payPalOrderDTO_okay.getTransactionId(),
						payPalOrderDTO_okay.getOrderInformationDTO(),
						payPalOrderDTO_okay.getTransactionDTO());

		payPalOrderBean.update(null, dto);

		inOrder.verify(paypalorderCRUDBeanLocal).findById(
				payPalOrderDTO_okay.getId());

		payPalOrder_okay.fromDto(dto);
		inOrder.verify(paypalorderCRUDBeanLocal).update(payPalOrder_okay);
	}

	@Test
	public void shouldDelete() {
		InOrder inOrder = Mockito.inOrder(paypalorderCRUDBeanLocal);
		payPalOrderBean.delete(null, payPalOrderDTO_okay);
		inOrder.verify(paypalorderCRUDBeanLocal).findById(
				payPalOrderDTO_okay.getId());
		inOrder.verify(paypalorderCRUDBeanLocal).delete(payPalOrder_okay);
	}

	/*
	 * With AccountDTO
	 */
	@Test
	public void shouldStartPaymentProcessWithAccountDTO() throws Exception {
		final int quantity = 1;
		final String expected[] = new String[] {
				"0",
				"https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&useraction=commit&token=EC-9YH45647XL82480C" };
		final String paypalAPISetExpresscheckout = "paypalAPISetExpresscheckout";
		final String paypalAPISetExpresscheckoutMessage = "TOKEN=EC-9YH45647XL82480C&TIMESTAMP=2012-02-23T12:50:07Z&CORRELATIONID=bc9025d31743a&ACK=Success&VERSION=84.0&BUILD=2571254";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPISetExpresscheckoutMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPISetExpresscheckout, productDTO.getProductLocales()[0],
				quantity);

		// Call public method. public method calls private method from above.
		String ret[] = payPalOrderBeanPrivateMockingClass.startPaymentProcess(
				accountDTO, productDTO.getProductLocales()[0],
				productDTO.getUin(), productDTO.getSystem(), quantity);

		// Verify return values
		assertEquals(expected[0], ret[0]);
		assertEquals(expected[1], ret[1]);

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how much the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPISetExpresscheckout,
				productDTO.getProductLocales()[0], quantity);
	}

	/*
	 * AccountDTO is null
	 */
	@Test
	public void shouldStartPaymentProcessWithoutAccountDTO() throws Exception {
		final int quantity = 1;
		final String expected[] = new String[] {
				"0",
				"https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&useraction=commit&token=EC-9YH45647XL82480C" };
		final String paypalAPISetExpresscheckout = "paypalAPISetExpresscheckout";
		final String paypalAPISetExpresscheckoutMessage = "TOKEN=EC-9YH45647XL82480C&TIMESTAMP=2012-02-23T12:50:07Z&CORRELATIONID=bc9025d31743a&ACK=Success&VERSION=84.0&BUILD=2571254";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPISetExpresscheckoutMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPISetExpresscheckout, productDTO.getProductLocales()[0],
				quantity);

		// Call public method. public method calls private method from above.
		String ret[] = payPalOrderBeanPrivateMockingClass.startPaymentProcess(
				null, productDTO.getProductLocales()[0], productDTO.getUin(),
				productDTO.getSystem(), quantity);

		// Verify return values
		assertEquals(expected[0], ret[0]);
		assertEquals(expected[1], ret[1]);

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPISetExpresscheckout,
				productDTO.getProductLocales()[0], quantity);
	}

	/*
	 * Try null values for startPaymentProcess as parameters. Exception must be
	 * thrown.
	 */
	@Test
	public void startPaymentProcessShouldThrowExceptionAckIsUndefined()
			throws Exception {
		final String ack = "xyz";
		final int quantity = 1;
		final String paypalAPISetExpresscheckout = "paypalAPISetExpresscheckout";
		final String paypalAPISetExpresscheckoutMessage = "TOKEN=EC-9YH45647XL82480C&TIMESTAMP=2012-02-23T12:50:07Z&CORRELATIONID=bc9025d31743a&ACK="
				+ ack + "&VERSION=84.0&BUILD=2571254";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPISetExpresscheckoutMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPISetExpresscheckout, productDTO.getProductLocales()[0],
				quantity);

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass.startPaymentProcess(accountDTO,
					productDTO.getProductLocales()[0], productDTO.getUin(),
					productDTO.getSystem(), quantity);

			fail();
		} catch (PayPalException e) {
			assertEquals(e.getMessage(),
					"PaypalAPISetExpresscheckout - Paypal acknowledgement-status is not Success");
		}

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPISetExpresscheckout,
				productDTO.getProductLocales()[0], quantity);
	}

	/*
	 * The return message paypalAPISetExpresscheckoutMessage is null. Exception
	 * must be thrown.
	 */
	@Test
	public void startPaymentProcessShouldThrowExceptionPaypalAPISetExpresscheckoutMessageReturnsNull()
			throws Exception {
		final int quantity = 1;
		final String paypalAPISetExpresscheckout = "paypalAPISetExpresscheckout";
		final String paypalAPISetExpresscheckoutMessage = null;

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPISetExpresscheckoutMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPISetExpresscheckout, productDTO.getProductLocales()[0],
				quantity);

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass.startPaymentProcess(accountDTO,
					productDTO.getProductLocales()[0], productDTO.getUin(),
					productDTO.getSystem(), quantity);

			fail();
		} catch (PayPalException e) {
			assertEquals(e.getMessage(),
					"Acknowledgement status could not be determined. Message is NULL");
		}

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPISetExpresscheckout,
				productDTO.getProductLocales()[0], quantity);
	}

	/*
	 * The return message paypalAPISetExpresscheckoutMessage is "". Exception
	 * must be thrown.
	 */
	@Test
	public void startPaymentProcessShouldThrowExceptionRaypalAPISetExpresscheckoutMessageReturnsEmptyString()
			throws Exception {
		final int quantity = 1;
		final String paypalAPISetExpresscheckout = "paypalAPISetExpresscheckout";
		final String paypalAPISetExpresscheckoutMessage = "";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPISetExpresscheckoutMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPISetExpresscheckout, productDTO.getProductLocales()[0],
				quantity);

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass.startPaymentProcess(accountDTO,
					productDTO.getProductLocales()[0], productDTO.getUin(),
					productDTO.getSystem(), quantity);

			fail();
		} catch (PayPalException e) {
			assertEquals(e.getMessage(),
					"PaypalAPISetExpresscheckout - Paypal acknowledgement-status is not Success");
		}

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPISetExpresscheckout,
				productDTO.getProductLocales()[0], quantity);
	}

	/*
	 * Try null parameter. Exception must be thrown.
	 */
	@Test
	public void startPaymentProcessShouldThrowExceptionTryNullParameter()
			throws Exception {
		final int quantity = 1;
		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass
					.startPaymentProcess(accountDTO, null, productDTO.getUin(),
							productDTO.getSystem(), quantity);
		} catch (Exception e) {
			assertEquals(e.getMessage(),
					"You must set productLocaleDTO, product_uin, eSystemDTO and quantity!");
		}

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass.startPaymentProcess(accountDTO,
					productDTO.getProductLocales()[0], null,
					productDTO.getSystem(), quantity);

			fail();
		} catch (Exception e) {
			assertEquals(e.getMessage(),
					"You must set productLocaleDTO, product_uin, eSystemDTO and quantity!");
		}

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass.startPaymentProcess(accountDTO,
					productDTO.getProductLocales()[0], productDTO.getUin(),
					null, quantity);

			fail();
		} catch (Exception e) {
			assertEquals(e.getMessage(),
					"You must set productLocaleDTO, product_uin, eSystemDTO and quantity!");
		}

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass.startPaymentProcess(accountDTO,
					productDTO.getProductLocales()[0], productDTO.getUin(),
					productDTO.getSystem(), -1);

			fail();
		} catch (Exception e) {
			assertEquals(e.getMessage(),
					"You must set productLocaleDTO, product_uin, eSystemDTO and quantity!");
		}
	}

	/*
	 * Should return OrderStatus.COMPLETED
	 */
	@Test
	public void shouldCheckStatusOfPayPalOrderCompleted() throws Exception {
		final OrderStatus expected = OrderStatus.COMPLETED;
		final String currency = "EUR";
		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIGetExpressCheckoutDetailsMessage = "TOKEN=EC-9YH45647XL812480C&CHECKOUTSTATUS="
				+ PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionCompleted
						.toString()
				+ "&TIMESTAMP=2012-02-23T12:53:07Z&CORRELATIONID=a88d6753579ce&ACK=Success&VERSION=84.0&BUILD=2571254&EMAIL=spainanier@local.local&PAYERID=5AKKECPYNESSY&PAYERSTATUS=verified&FIRSTNAME=Max&LASTNAME=Spain&COUNTRYCODE=ES&SHIPTONAME=Max Spain&SHIPTOSTREET=Spainstreet 343&SHIPTOCITY=Madrid&SHIPTOSTATE=Ceuta&SHIPTOZIP=12345&SHIPTOCOUNTRYCODE=ES&SHIPTOCOUNTRYNAME=Spain&ADDRESSSTATUS=Unconfirmed&CURRENCYCODE="
				+ currency
				+ "&AMT=27.87&SHIPPINGAMT=0.00&HANDLINGAMT=0.00&TAXAMT=0.00&INSURANCEAMT=0.00&SHIPDISCAMT=0.00&PAYMENTREQUEST_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTREQUEST_0_AMT=&PAYMENTREQUEST_0_SHIPPINGAMT=0.00&PAYMENTREQUEST_0_HANDLINGAMT=0.00&PAYMENTREQUEST_0_TAXAMT=0.00&PAYMENTREQUEST_0_INSURANCEAMT=0.00&PAYMENTREQUEST_0_SHIPDISCAMT=0.00&PAYMENTREQUEST_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED=false&PAYMENTREQUEST_0_SHIPTONAME=Max Spain&PAYMENTREQUEST_0_SHIPTOSTREET=Spainstreet 343&PAYMENTREQUEST_0_SHIPTOCITY=Madrid&PAYMENTREQUEST_0_SHIPTOSTATE=Ceuta&PAYMENTREQUEST_0_SHIPTOZIP=12345&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=ES&PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME=Spain&PAYMENTREQUESTINFO_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUESTINFO_0_ERRORCODE=0";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		// Call public method. public method calls private method from above.
		OrderStatus orderStatus = payPalOrderBeanPrivateMockingClass
				.checkStatusOfPayPalOrder(payPalOrderDTO_okay);

		// Verify return values
		assertEquals(orderStatus, expected);

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());
	}

	/*
	 * Should return OrderStatus.FAILURE
	 */
	@Test
	public void shouldCheckStatusOfPayPalOrderFailure() throws Exception {
		final String currency = "EUR";
		final OrderStatus expected = OrderStatus.FAILURE;
		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIGetExpressCheckoutDetailsMessage = "TOKEN=EC-9YH45647XL812480C&CHECKOUTSTATUS="
				+ PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionFailed
						.toString()
				+ "&TIMESTAMP=2012-02-23T12:53:07Z&CORRELATIONID=a88d6753579ce&ACK=Success&VERSION=84.0&BUILD=2571254&EMAIL=spainanier@local.local&PAYERID=5AKKECPYNESSY&PAYERSTATUS=verified&FIRSTNAME=Max&LASTNAME=Spain&COUNTRYCODE=ES&SHIPTONAME=Max Spain&SHIPTOSTREET=Spainstreet 343&SHIPTOCITY=Madrid&SHIPTOSTATE=Ceuta&SHIPTOZIP=12345&SHIPTOCOUNTRYCODE=ES&SHIPTOCOUNTRYNAME=Spain&ADDRESSSTATUS=Unconfirmed&CURRENCYCODE="
				+ currency
				+ "&AMT=27.87&SHIPPINGAMT=0.00&HANDLINGAMT=0.00&TAXAMT=0.00&INSURANCEAMT=0.00&SHIPDISCAMT=0.00&PAYMENTREQUEST_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTREQUEST_0_AMT=27.87&PAYMENTREQUEST_0_SHIPPINGAMT=0.00&PAYMENTREQUEST_0_HANDLINGAMT=0.00&PAYMENTREQUEST_0_TAXAMT=0.00&PAYMENTREQUEST_0_INSURANCEAMT=0.00&PAYMENTREQUEST_0_SHIPDISCAMT=0.00&PAYMENTREQUEST_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED=false&PAYMENTREQUEST_0_SHIPTONAME=Max Spain&PAYMENTREQUEST_0_SHIPTOSTREET=Spainstreet 343&PAYMENTREQUEST_0_SHIPTOCITY=Madrid&PAYMENTREQUEST_0_SHIPTOSTATE=Ceuta&PAYMENTREQUEST_0_SHIPTOZIP=12345&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=ES&PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME=Spain&PAYMENTREQUESTINFO_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUESTINFO_0_ERRORCODE=0";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		// Call public method. public method calls private method from above.
		OrderStatus orderStatus = payPalOrderBeanPrivateMockingClass
				.checkStatusOfPayPalOrder(payPalOrderDTO_okay);

		// Verify return values
		assertEquals(orderStatus, expected);

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());
	}

	/*
	 * Should return OrderStatus.PENDING
	 */
	@Test
	public void shouldCheckStatusOfPayPalOrderPending() throws Exception {
		final String currency = "EUR";
		final OrderStatus expected = OrderStatus.PENDING;
		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIGetExpressCheckoutDetailsMessage = "TOKEN=EC-9YH45647XL812480C&CHECKOUTSTATUS="
				+ PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionInProgress
						.toString()
				+ "&TIMESTAMP=2012-02-23T12:53:07Z&CORRELATIONID=a88d6753579ce&ACK=Success&VERSION=84.0&BUILD=2571254&EMAIL=spainanier@local.local&PAYERID=5AKKECPYNESSY&PAYERSTATUS=verified&FIRSTNAME=Max&LASTNAME=Spain&COUNTRYCODE=ES&SHIPTONAME=Max Spain&SHIPTOSTREET=Spainstreet 343&SHIPTOCITY=Madrid&SHIPTOSTATE=Ceuta&SHIPTOZIP=12345&SHIPTOCOUNTRYCODE=ES&SHIPTOCOUNTRYNAME=Spain&ADDRESSSTATUS=Unconfirmed&CURRENCYCODE="
				+ currency
				+ "&AMT=27.87&SHIPPINGAMT=0.00&HANDLINGAMT=0.00&TAXAMT=0.00&INSURANCEAMT=0.00&SHIPDISCAMT=0.00&PAYMENTREQUEST_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTREQUEST_0_AMT=27.87&PAYMENTREQUEST_0_SHIPPINGAMT=0.00&PAYMENTREQUEST_0_HANDLINGAMT=0.00&PAYMENTREQUEST_0_TAXAMT=0.00&PAYMENTREQUEST_0_INSURANCEAMT=0.00&PAYMENTREQUEST_0_SHIPDISCAMT=0.00&PAYMENTREQUEST_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED=false&PAYMENTREQUEST_0_SHIPTONAME=Max Spain&PAYMENTREQUEST_0_SHIPTOSTREET=Spainstreet 343&PAYMENTREQUEST_0_SHIPTOCITY=Madrid&PAYMENTREQUEST_0_SHIPTOSTATE=Ceuta&PAYMENTREQUEST_0_SHIPTOZIP=12345&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=ES&PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME=Spain&PAYMENTREQUESTINFO_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUESTINFO_0_ERRORCODE=0";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		// Call public method. public method calls private method from above.
		OrderStatus orderStatus = payPalOrderBeanPrivateMockingClass
				.checkStatusOfPayPalOrder(payPalOrderDTO_okay);

		// Verify return values
		assertEquals(orderStatus, expected);

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());
	}

	/*
	 * Should return OrderStatus.FAILURE
	 */
	@Test
	public void shouldCheckStatusOfPayPalOrderFailureBecauseStatusIsPaymentActionNotInitiated()
			throws Exception {
		final String currency = "EUR";
		final OrderStatus expected = OrderStatus.FAILURE;
		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIGetExpressCheckoutDetailsMessage = "TOKEN=EC-9YH45647XL812480C&CHECKOUTSTATUS="
				+ PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionNotInitiated
						.toString()
				+ "&TIMESTAMP=2012-02-23T12:53:07Z&CORRELATIONID=a88d6753579ce&ACK=Success&VERSION=84.0&BUILD=2571254&EMAIL=spainanier@local.local&PAYERID=5AKKECPYNESSY&PAYERSTATUS=verified&FIRSTNAME=Max&LASTNAME=Spain&COUNTRYCODE=ES&SHIPTONAME=Max Spain&SHIPTOSTREET=Spainstreet 343&SHIPTOCITY=Madrid&SHIPTOSTATE=Ceuta&SHIPTOZIP=12345&SHIPTOCOUNTRYCODE=ES&SHIPTOCOUNTRYNAME=Spain&ADDRESSSTATUS=Unconfirmed&CURRENCYCODE="
				+ currency
				+ "&AMT=27.87&SHIPPINGAMT=0.00&HANDLINGAMT=0.00&TAXAMT=0.00&INSURANCEAMT=0.00&SHIPDISCAMT=0.00&PAYMENTREQUEST_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTREQUEST_0_AMT=27.87&PAYMENTREQUEST_0_SHIPPINGAMT=0.00&PAYMENTREQUEST_0_HANDLINGAMT=0.00&PAYMENTREQUEST_0_TAXAMT=0.00&PAYMENTREQUEST_0_INSURANCEAMT=0.00&PAYMENTREQUEST_0_SHIPDISCAMT=0.00&PAYMENTREQUEST_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED=false&PAYMENTREQUEST_0_SHIPTONAME=Max Spain&PAYMENTREQUEST_0_SHIPTOSTREET=Spainstreet 343&PAYMENTREQUEST_0_SHIPTOCITY=Madrid&PAYMENTREQUEST_0_SHIPTOSTATE=Ceuta&PAYMENTREQUEST_0_SHIPTOZIP=12345&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=ES&PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME=Spain&PAYMENTREQUESTINFO_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUESTINFO_0_ERRORCODE=0";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		// Call public method. public method calls private method from above.
		OrderStatus orderStatus = payPalOrderBeanPrivateMockingClass
				.checkStatusOfPayPalOrder(payPalOrderDTO_okay);

		// Verify return values
		assertEquals(orderStatus, expected);

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());
	}

	/*
	 * PaypalAPIGetExpressCheckoutDetailsMessage is null. Exception must be
	 * thrown.
	 */
	@Test
	public void checkStatusOfPayPalOrderShouldThrowExceptionPaypalAPIGetExpressCheckoutDetailsMessageIsNull()
			throws Exception {
		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIGetExpressCheckoutDetailsMessage = null;

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass
					.checkStatusOfPayPalOrder(payPalOrderDTO_okay);

			fail();
		} catch (PayPalException e) {
			assertEquals(e.getMessage(),
					"Acknowledgement status could not be determined. Message is NULL");
		}

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());
	}

	/*
	 * CHECKOUTSTATUS in PaypalAPIGetExpressCheckoutDetailsMessage has an
	 * undifiened value. Exception must be thrown.
	 */
	@Test
	public void checkStatusOfPayPalOrderShouldThrowExceptionCheckoutStatusIsUndefined()
			throws Exception {
		final String currency = "EUR";
		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIGetExpressCheckoutDetailsMessage = "TOKEN=EC-9YH45647XL812480C&CHECKOUTSTATUS="
				+ "XYZ"
				+ "&TIMESTAMP=2012-02-23T12:53:07Z&CORRELATIONID=a88d6753579ce&ACK=Success&VERSION=84.0&BUILD=2571254&EMAIL=spainanier@local.local&PAYERID=5AKKECPYNESSY&PAYERSTATUS=verified&FIRSTNAME=Max&LASTNAME=Spain&COUNTRYCODE=ES&SHIPTONAME=Max Spain&SHIPTOSTREET=Spainstreet 343&SHIPTOCITY=Madrid&SHIPTOSTATE=Ceuta&SHIPTOZIP=12345&SHIPTOCOUNTRYCODE=ES&SHIPTOCOUNTRYNAME=Spain&ADDRESSSTATUS=Unconfirmed&CURRENCYCODE="
				+ currency
				+ "&AMT=27.87&SHIPPINGAMT=0.00&HANDLINGAMT=0.00&TAXAMT=0.00&INSURANCEAMT=0.00&SHIPDISCAMT=0.00&PAYMENTREQUEST_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTREQUEST_0_AMT=27.87&PAYMENTREQUEST_0_SHIPPINGAMT=0.00&PAYMENTREQUEST_0_HANDLINGAMT=0.00&PAYMENTREQUEST_0_TAXAMT=0.00&PAYMENTREQUEST_0_INSURANCEAMT=0.00&PAYMENTREQUEST_0_SHIPDISCAMT=0.00&PAYMENTREQUEST_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED=false&PAYMENTREQUEST_0_SHIPTONAME=Max Spain&PAYMENTREQUEST_0_SHIPTOSTREET=Spainstreet 343&PAYMENTREQUEST_0_SHIPTOCITY=Madrid&PAYMENTREQUEST_0_SHIPTOSTATE=Ceuta&PAYMENTREQUEST_0_SHIPTOZIP=12345&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=ES&PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME=Spain&PAYMENTREQUESTINFO_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUESTINFO_0_ERRORCODE=0";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass
					.checkStatusOfPayPalOrder(payPalOrderDTO_okay);

			fail();
		} catch (PayPalException e) {
			assertEquals(e.getMessage(),
					"PayPal-API GetExpressCheckoutDetails - CHECKOUTSTATUS is null.");
		}

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());
	}

	/*
	 * PayPalOrder could not be found in database. Exception must be thrown.
	 */
	@Test
	public void checkStatusOfPayPalOrderShouldThrowExceptionPayPalOrderNotInDb()
			throws Exception {
		// Return null value
		Mockito.when(
				paypalorderCRUDBeanLocal.findById(payPalOrderDTO_okay.getId()))
				.thenReturn(null);

		try {
			payPalOrderBeanPrivateMockingClass
					.checkStatusOfPayPalOrder(payPalOrderDTO_okay);

			fail();
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Could not find entity in database!");
		}
	}

	/*
	 * PayPalOrderDTO is null. Exception must be thrown.
	 */
	@Test
	public void checkStatusOfPayPalOrderShouldThrowExceptionPayPalOrderDTOIsNull()
			throws Exception {
		try {
			payPalOrderBeanPrivateMockingClass.checkStatusOfPayPalOrder(null);

			fail();
		} catch (Exception e) {
			assertEquals(e.getMessage(), "PayPalOrderDTO could not be null!");
		}
	}

	/*
	 * Should complete Transaction
	 */
	@Test
	public void shouldCompleteTransaction() throws Exception {
		final String total = "27.87";
		final String currency = "EUR";

		final OrderStatus expected = OrderStatus.COMPLETED;

		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIDoExpressCheckoutPayment = "paypalAPIDoExpressCheckoutPayment";

		final String paypalAPIGetExpressCheckoutDetailsMessage = "TOKEN=EC-9YH45647XL812480C&CHECKOUTSTATUS=PaymentActionCompleted&TIMESTAMP=2012-02-23T12:53:07Z&CORRELATIONID=a88d6753579ce&ACK=Success&VERSION=84.0&BUILD=2571254&EMAIL=spainanier@local.local&PAYERID=5AKKECPYNESSY&PAYERSTATUS=verified&FIRSTNAME=Max&LASTNAME=Spain&COUNTRYCODE=ES&SHIPTONAME=Max Spain&SHIPTOSTREET=Spainstreet 343&SHIPTOCITY=Madrid&SHIPTOSTATE=Ceuta&SHIPTOZIP=12345&SHIPTOCOUNTRYCODE=ES&SHIPTOCOUNTRYNAME=Spain&ADDRESSSTATUS=Unconfirmed&CURRENCYCODE="
				+ currency
				+ "&AMT="
				+ total
				+ "&SHIPPINGAMT=0.00&HANDLINGAMT=0.00&TAXAMT=0.00&INSURANCEAMT=0.00&SHIPDISCAMT=0.00&PAYMENTREQUEST_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTREQUEST_0_AMT="
				+ total
				+ "&PAYMENTREQUEST_0_SHIPPINGAMT=0.00&PAYMENTREQUEST_0_HANDLINGAMT=0.00&PAYMENTREQUEST_0_TAXAMT=0.00&PAYMENTREQUEST_0_INSURANCEAMT=0.00&PAYMENTREQUEST_0_SHIPDISCAMT=0.00&PAYMENTREQUEST_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED=false&PAYMENTREQUEST_0_SHIPTONAME=Max Spain&PAYMENTREQUEST_0_SHIPTOSTREET=Spainstreet 343&PAYMENTREQUEST_0_SHIPTOCITY=Madrid&PAYMENTREQUEST_0_SHIPTOSTATE=Ceuta&PAYMENTREQUEST_0_SHIPTOZIP=12345&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=ES&PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME=Spain&PAYMENTREQUESTINFO_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUESTINFO_0_ERRORCODE=0";
		final String paypalAPIDoExpressCheckoutPaymentMessage = "TOKEN=EC-9YH45647XL812480C&SUCCESSPAGEREDIRECTREQUESTED=false&TIMESTAMP=2012-02-23T12:52:43Z&CORRELATIONID=9b4aa5e62a08f&ACK=Success&VERSION=84.0&BUILD=2571254&INSURANCEOPTIONSELECTED=false&SHIPPINGOPTIONISDEFAULT=false&PAYMENTINFO_0_TRANSACTIONID=2V3054523B7072154&PAYMENTINFO_0_TRANSACTIONTYPE=expresscheckout&PAYMENTINFO_0_PAYMENTTYPE=instant&PAYMENTINFO_0_ORDERTIME=2012-02-23T12:52:40Z&PAYMENTINFO_0_AMT="
				+ total
				+ "&PAYMENTINFO_0_FEEAMT=0.88&PAYMENTINFO_0_TAXAMT=0.00&PAYMENTINFO_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTINFO_0_PAYMENTSTATUS=Completed&PAYMENTINFO_0_PENDINGREASON=None&PAYMENTINFO_0_REASONCODE=None&PAYMENTINFO_0_PROTECTIONELIGIBILITY=Eligible&PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE=ItemNotReceivedEligible,UnauthorizedPaymentEligible&PAYMENTINFO_0_SECUREMERCHANTACCOUNTID=CL5APJ4EKSGJY&PAYMENTINFO_0_ERRORCODE=0&PAYMENTINFO_0_ACK=Success";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIDoExpressCheckoutPaymentMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIDoExpressCheckoutPayment,
				payPalOrderDTO_complete.getToken(),
				payPalOrderDTO_complete.getPayerId(), total, currency);

		// Call public method. public method calls private method from above.
		PayPalOrderDTO result = payPalOrderBeanPrivateMockingClass
				.completeTransaction(payPalOrderDTO_complete.getId(),
						payPalOrderDTO_complete.getToken(),
						payPalOrderDTO_complete.getPayerId());

		// Verify return values
		assertEquals(expected, result.getOrderInformationDTO().getOrderStatus());

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(2)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_complete.getToken());

		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIDoExpressCheckoutPayment,
				payPalOrderDTO_complete.getToken(),
				payPalOrderDTO_complete.getPayerId(), total, currency);
	}

	/*
	 * Try null values as parameters. Exception must be thrown.
	 */
	@Test
	public void completeTransactionShouldThrowExceptionTryNullValuesAsParameter()
			throws Exception {
		try {
			payPalOrderBeanPrivateMockingClass.completeTransaction(-1, null,
					null);

			fail();
		} catch (Exception e) {
			assertEquals(e.getMessage(),
					"Token and PayerId must no be null. Id must be a positiv value");
		}

		try {
			payPalOrderBeanPrivateMockingClass.completeTransaction(22, null,
					null);
		} catch (Exception e) {
			assertEquals(e.getMessage(),
					"Token and PayerId must no be null. Id must be a positiv value");
		}

		try {
			payPalOrderBeanPrivateMockingClass.completeTransaction(22, "test",
					null);
		} catch (Exception e) {
			assertEquals(e.getMessage(),
					"Token and PayerId must no be null. Id must be a positiv value");
		}

		try {
			payPalOrderBeanPrivateMockingClass.completeTransaction(22, null,
					"test");
		} catch (Exception e) {
			assertEquals(e.getMessage(),
					"Token and PayerId must no be null. Id must be a positiv value");
		}
	}

	/*
	 * PayPalOrder could not be found in database. Exception must be thrown.
	 */
	@Test
	public void completeTransactionShouldThrowExceptionBecauseEntityCouldNotBeFoundInDBS()
			throws Exception {
		// Return null value
		Mockito.when(
				paypalorderCRUDBeanLocal.findById(payPalOrderDTO_okay.getId()))
				.thenReturn(null);

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass.completeTransaction(
					payPalOrderDTO_complete.getId(),
					payPalOrderDTO_complete.getToken(),
					payPalOrderDTO_complete.getPayerId());

			fail();
		} catch (Exception e) {
			assertEquals(e.getMessage(),
					"Could not find PayPalOrder-Object in database.");
		}
	}

	/*
	 * Return message of paypalAPIDoExpressCheckoutPayment is null. Throw
	 * Exception.
	 */
	@Test
	public void completeTransactionShouldThrowExceptionRetrunMessageOfPaypalAPIDoExpressCheckoutPaymentIsNull()
			throws Exception {
		final String total = "27.87";
		final String currency = "EUR";

		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIDoExpressCheckoutPayment = "paypalAPIDoExpressCheckoutPayment";

		final String paypalAPIGetExpressCheckoutDetailsMessage = "TOKEN=EC-9YH45647XL812480C&CHECKOUTSTATUS=PaymentActionCompleted&TIMESTAMP=2012-02-23T12:53:07Z&CORRELATIONID=a88d6753579ce&ACK=Success&VERSION=84.0&BUILD=2571254&EMAIL=spainanier@local.local&PAYERID=5AKKECPYNESSY&PAYERSTATUS=verified&FIRSTNAME=Max&LASTNAME=Spain&COUNTRYCODE=ES&SHIPTONAME=Max Spain&SHIPTOSTREET=Spainstreet 343&SHIPTOCITY=Madrid&SHIPTOSTATE=Ceuta&SHIPTOZIP=12345&SHIPTOCOUNTRYCODE=ES&SHIPTOCOUNTRYNAME=Spain&ADDRESSSTATUS=Unconfirmed&CURRENCYCODE="
				+ currency
				+ "&AMT="
				+ total
				+ "&SHIPPINGAMT=0.00&HANDLINGAMT=0.00&TAXAMT=0.00&INSURANCEAMT=0.00&SHIPDISCAMT=0.00&PAYMENTREQUEST_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTREQUEST_0_AMT="
				+ total
				+ "&PAYMENTREQUEST_0_SHIPPINGAMT=0.00&PAYMENTREQUEST_0_HANDLINGAMT=0.00&PAYMENTREQUEST_0_TAXAMT=0.00&PAYMENTREQUEST_0_INSURANCEAMT=0.00&PAYMENTREQUEST_0_SHIPDISCAMT=0.00&PAYMENTREQUEST_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED=false&PAYMENTREQUEST_0_SHIPTONAME=Max Spain&PAYMENTREQUEST_0_SHIPTOSTREET=Spainstreet 343&PAYMENTREQUEST_0_SHIPTOCITY=Madrid&PAYMENTREQUEST_0_SHIPTOSTATE=Ceuta&PAYMENTREQUEST_0_SHIPTOZIP=12345&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=ES&PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME=Spain&PAYMENTREQUESTINFO_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUESTINFO_0_ERRORCODE=0";
		final String paypalAPIDoExpressCheckoutPaymentMessage = null;

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIDoExpressCheckoutPaymentMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIDoExpressCheckoutPayment,
				payPalOrderDTO_complete.getToken(),
				payPalOrderDTO_complete.getPayerId(), total, currency);

		// Call public method. public method calls private method from above.
		try {
			payPalOrderBeanPrivateMockingClass.completeTransaction(
					payPalOrderDTO_complete.getId(),
					payPalOrderDTO_complete.getToken(),
					payPalOrderDTO_complete.getPayerId());

			fail();
		} catch (PayPalException e) {
			assertEquals(
					"Acknowledgement status could not be determined. Message is NULL",
					e.getMessage());
		}

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_complete.getToken());

		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIDoExpressCheckoutPayment,
				payPalOrderDTO_complete.getToken(),
				payPalOrderDTO_complete.getPayerId(), total, currency);
	}

	/*
	 * Return message of paypalAPIDoExpressCheckoutPayment method is "". Throw
	 * Exception.
	 */
	@Test
	public void completeTransactionShouldThrowExceptionReturnMessageOfPaypalAPIDoExpressCheckoutPaymentIsNothing()
			throws Exception {
		final String total = "27.87";
		final String currency = "EUR";

		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIDoExpressCheckoutPayment = "paypalAPIDoExpressCheckoutPayment";

		final String paypalAPIGetExpressCheckoutDetailsMessage = "TOKEN=EC-9YH45647XL812480C&CHECKOUTSTATUS=PaymentActionCompleted&TIMESTAMP=2012-02-23T12:53:07Z&CORRELATIONID=a88d6753579ce&ACK=Success&VERSION=84.0&BUILD=2571254&EMAIL=spainanier@local.local&PAYERID=5AKKECPYNESSY&PAYERSTATUS=verified&FIRSTNAME=Max&LASTNAME=Spain&COUNTRYCODE=ES&SHIPTONAME=Max Spain&SHIPTOSTREET=Spainstreet 343&SHIPTOCITY=Madrid&SHIPTOSTATE=Ceuta&SHIPTOZIP=12345&SHIPTOCOUNTRYCODE=ES&SHIPTOCOUNTRYNAME=Spain&ADDRESSSTATUS=Unconfirmed&CURRENCYCODE="
				+ currency
				+ "&AMT="
				+ total
				+ "&SHIPPINGAMT=0.00&HANDLINGAMT=0.00&TAXAMT=0.00&INSURANCEAMT=0.00&SHIPDISCAMT=0.00&PAYMENTREQUEST_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTREQUEST_0_AMT="
				+ total
				+ "&PAYMENTREQUEST_0_SHIPPINGAMT=0.00&PAYMENTREQUEST_0_HANDLINGAMT=0.00&PAYMENTREQUEST_0_TAXAMT=0.00&PAYMENTREQUEST_0_INSURANCEAMT=0.00&PAYMENTREQUEST_0_SHIPDISCAMT=0.00&PAYMENTREQUEST_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED=false&PAYMENTREQUEST_0_SHIPTONAME=Max Spain&PAYMENTREQUEST_0_SHIPTOSTREET=Spainstreet 343&PAYMENTREQUEST_0_SHIPTOCITY=Madrid&PAYMENTREQUEST_0_SHIPTOSTATE=Ceuta&PAYMENTREQUEST_0_SHIPTOZIP=12345&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=ES&PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME=Spain&PAYMENTREQUESTINFO_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUESTINFO_0_ERRORCODE=0";
		final String paypalAPIDoExpressCheckoutPaymentMessage = "";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIDoExpressCheckoutPaymentMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIDoExpressCheckoutPayment,
				payPalOrderDTO_complete.getToken(),
				payPalOrderDTO_complete.getPayerId(), total, currency);

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass.completeTransaction(
					payPalOrderDTO_complete.getId(),
					payPalOrderDTO_complete.getToken(),
					payPalOrderDTO_complete.getPayerId());

			fail();
		} catch (PayPalException e) {
			assertEquals(
					"PayPal-API DoExpressCheckoutPayment - ACK is not set to Success.",
					e.getMessage());
		}

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_complete.getToken());

		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIDoExpressCheckoutPayment,
				payPalOrderDTO_complete.getToken(),
				payPalOrderDTO_complete.getPayerId(), total, currency);
	}

	/*
	 * Return message of paypalAPIGetExpressCheckoutDetails is null. Throw
	 * Exception.
	 */
	@Test
	public void completeTransactionShouldThrowExceptionReturnMessageOfPaypalAPIGetExpressCheckoutDetailsIsNull()
			throws Exception {
		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIGetExpressCheckoutDetailsMessage = null;

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		// Call public method. public method calls private method from above.
		try {
			payPalOrderBeanPrivateMockingClass.completeTransaction(
					payPalOrderDTO_complete.getId(),
					payPalOrderDTO_complete.getToken(),
					payPalOrderDTO_complete.getPayerId());

			fail();
		} catch (PayPalException e) {
			assertEquals(
					"Acknowledgement status could not be determined. Message is NULL",
					e.getMessage());
		}

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_complete.getToken());
	}

	/*
	 * Return message of paypalAPIGetExpressCheckoutDetails method is "". Throw
	 * Exception.
	 */
	@Test
	public void completeTransactionShouldThrowExceptionReturnMessageOfPaypalAPIGetExpressCheckoutDetailsIsNothing()
			throws Exception {

		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIGetExpressCheckoutDetailsMessage = "";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass.completeTransaction(
					payPalOrderDTO_complete.getId(),
					payPalOrderDTO_complete.getToken(),
					payPalOrderDTO_complete.getPayerId());

			fail();
		} catch (PayPalException e) {
			assertEquals(
					"PayPal-API GetExpressCheckoutDetails - ACK is not set to Success.",
					e.getMessage());
		}

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_complete.getToken());
	}

	/*
	 * Total price is "". getTotalPriceFromMessage must an throw exception
	 * Exception.
	 */
	@Test
	public void completeTransactionShouldThrowExceptionReturnMessageOfGetTotalPriceFromMessageIsNothing()
			throws Exception {
		final String total = "";
		final String currency = "EUR";
		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";

		final String paypalAPIGetExpressCheckoutDetailsMessage = "TOKEN=EC-9YH45647XL812480C&CHECKOUTSTATUS=PaymentActionCompleted&TIMESTAMP=2012-02-23T12:53:07Z&CORRELATIONID=a88d6753579ce&ACK=Success&VERSION=84.0&BUILD=2571254&EMAIL=spainanier@local.local&PAYERID=5AKKECPYNESSY&PAYERSTATUS=verified&FIRSTNAME=Max&LASTNAME=Spain&COUNTRYCODE=ES&SHIPTONAME=Max Spain&SHIPTOSTREET=Spainstreet 343&SHIPTOCITY=Madrid&SHIPTOSTATE=Ceuta&SHIPTOZIP=12345&SHIPTOCOUNTRYCODE=ES&SHIPTOCOUNTRYNAME=Spain&ADDRESSSTATUS=Unconfirmed&CURRENCYCODE="
				+ currency
				+ "&AMT="
				+ total
				+ "&SHIPPINGAMT=0.00&HANDLINGAMT=0.00&TAXAMT=0.00&INSURANCEAMT=0.00&SHIPDISCAMT=0.00&PAYMENTREQUEST_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTREQUEST_0_AMT="
				+ total
				+ "&PAYMENTREQUEST_0_SHIPPINGAMT=0.00&PAYMENTREQUEST_0_HANDLINGAMT=0.00&PAYMENTREQUEST_0_TAXAMT=0.00&PAYMENTREQUEST_0_INSURANCEAMT=0.00&PAYMENTREQUEST_0_SHIPDISCAMT=0.00&PAYMENTREQUEST_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED=false&PAYMENTREQUEST_0_SHIPTONAME=Max Spain&PAYMENTREQUEST_0_SHIPTOSTREET=Spainstreet 343&PAYMENTREQUEST_0_SHIPTOCITY=Madrid&PAYMENTREQUEST_0_SHIPTOSTATE=Ceuta&PAYMENTREQUEST_0_SHIPTOZIP=12345&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=ES&PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME=Spain&PAYMENTREQUESTINFO_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUESTINFO_0_ERRORCODE=0";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass.completeTransaction(
					payPalOrderDTO_complete.getId(),
					payPalOrderDTO_complete.getToken(),
					payPalOrderDTO_complete.getPayerId());

			fail();
		} catch (Exception e) {
			assertEquals("getTotalPriceFromMessage - Could not get total.",
					e.getMessage());
		}

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_complete.getToken());
	}

	/*
	 * Transaction id is "". getTransactionIdFromMessage must an throw exception
	 * Exception.
	 */
	@Test
	public void completeTransactionShouldThrowExceptionReturnMessageOfGetTransactionIdFromMessageIsNothing()
			throws Exception {
		final String total = "27.87";
		String id = "";
		final String currency = "EUR";
		final String paypalAPIGetExpressCheckoutDetails = "paypalAPIGetExpressCheckoutDetails";
		final String paypalAPIDoExpressCheckoutPayment = "paypalAPIDoExpressCheckoutPayment";

		final String paypalAPIGetExpressCheckoutDetailsMessage = "TOKEN=EC-9YH45647XL812480C&CHECKOUTSTATUS=PaymentActionCompleted&TIMESTAMP=2012-02-23T12:53:07Z&CORRELATIONID=a88d6753579ce&ACK=Success&VERSION=84.0&BUILD=2571254&EMAIL=spainanier@local.local&PAYERID=5AKKECPYNESSY&PAYERSTATUS=verified&FIRSTNAME=Max&LASTNAME=Spain&COUNTRYCODE=ES&SHIPTONAME=Max Spain&SHIPTOSTREET=Spainstreet 343&SHIPTOCITY=Madrid&SHIPTOSTATE=Ceuta&SHIPTOZIP=12345&SHIPTOCOUNTRYCODE=ES&SHIPTOCOUNTRYNAME=Spain&ADDRESSSTATUS=Unconfirmed&CURRENCYCODE="
				+ currency
				+ "&AMT="
				+ total
				+ "&SHIPPINGAMT=0.00&HANDLINGAMT=0.00&TAXAMT=0.00&INSURANCEAMT=0.00&SHIPDISCAMT=0.00&PAYMENTREQUEST_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTREQUEST_0_AMT="
				+ total
				+ "&PAYMENTREQUEST_0_SHIPPINGAMT=0.00&PAYMENTREQUEST_0_HANDLINGAMT=0.00&PAYMENTREQUEST_0_TAXAMT=0.00&PAYMENTREQUEST_0_INSURANCEAMT=0.00&PAYMENTREQUEST_0_SHIPDISCAMT=0.00&PAYMENTREQUEST_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED=false&PAYMENTREQUEST_0_SHIPTONAME=Max Spain&PAYMENTREQUEST_0_SHIPTOSTREET=Spainstreet 343&PAYMENTREQUEST_0_SHIPTOCITY=Madrid&PAYMENTREQUEST_0_SHIPTOSTATE=Ceuta&PAYMENTREQUEST_0_SHIPTOZIP=12345&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=ES&PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME=Spain&PAYMENTREQUESTINFO_0_TRANSACTIONID=2V3054523B7072154&PAYMENTREQUESTINFO_0_ERRORCODE=0";
		final String paypalAPIDoExpressCheckoutPaymentMessage = "TOKEN=EC-9YH45647XL812480C&SUCCESSPAGEREDIRECTREQUESTED=false&TIMESTAMP=2012-02-23T12:52:43Z&CORRELATIONID=9b4aa5e62a08f&ACK=Success&VERSION=84.0&BUILD=2571254&INSURANCEOPTIONSELECTED=false&SHIPPINGOPTIONISDEFAULT=false&PAYMENTINFO_0_TRANSACTIONID="
				+ id
				+ "&PAYMENTINFO_0_TRANSACTIONTYPE=expresscheckout&PAYMENTINFO_0_PAYMENTTYPE=instant&PAYMENTINFO_0_ORDERTIME=2012-02-23T12:52:40Z&PAYMENTINFO_0_AMT="
				+ total
				+ "&PAYMENTINFO_0_FEEAMT=0.88&PAYMENTINFO_0_TAXAMT=0.00&PAYMENTINFO_0_CURRENCYCODE="
				+ currency
				+ "&PAYMENTINFO_0_PAYMENTSTATUS=Completed&PAYMENTINFO_0_PENDINGREASON=None&PAYMENTINFO_0_REASONCODE=None&PAYMENTINFO_0_PROTECTIONELIGIBILITY=Eligible&PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE=ItemNotReceivedEligible,UnauthorizedPaymentEligible&PAYMENTINFO_0_SECUREMERCHANTACCOUNTID=CL5APJ4EKSGJY&PAYMENTINFO_0_ERRORCODE=0&PAYMENTINFO_0_ACK=Success";

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIGetExpressCheckoutDetailsMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_okay.getToken());

		// Set the return value of private method
		PowerMockito.doReturn(paypalAPIDoExpressCheckoutPaymentMessage).when(
				payPalOrderBeanPrivateMockingClass,
				paypalAPIDoExpressCheckoutPayment,
				payPalOrderDTO_complete.getToken(),
				payPalOrderDTO_complete.getPayerId(), total, currency);

		try {
			// Call public method. public method calls private method from
			// above.
			payPalOrderBeanPrivateMockingClass.completeTransaction(
					payPalOrderDTO_complete.getId(),
					payPalOrderDTO_complete.getToken(),
					payPalOrderDTO_complete.getPayerId());

			fail();
		} catch (Exception e) {
			assertEquals(
					"getTransactionIdFromMessage - Could not get transactionid.",
					e.getMessage());
		}

		// Verify if private method has been called. Times must be 1 + X. X is
		// the number how musch the private method is called in your code.
		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIGetExpressCheckoutDetails,
				payPalOrderDTO_complete.getToken());

		PowerMockito.verifyPrivate(payPalOrderBeanPrivateMockingClass,
				Mockito.times(1)).invoke(paypalAPIDoExpressCheckoutPayment,
				payPalOrderDTO_complete.getToken(),
				payPalOrderDTO_complete.getPayerId(), total, currency);
	}
}
