package org.evolvis.eticketpayment.model.crud.paypalorder;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticketpayment.model.entity.OrderStatus;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.junit.Before;
import org.junit.Test;

public class PayPalOrderCRUDBeanTest {
	private PayPalOrderCRUDBean payPalOrderCRUDBean = new PayPalOrderCRUDBean();
	private EntityManager manager = mock(EntityManager.class);
	private Query query = mock(Query.class);
	
	@Before
	public void init() {
		preparePayPalOrderCRUDBean();
	}

	private void preparePayPalOrderCRUDBean() {
		payPalOrderCRUDBean.setManager(manager);
		when(manager.createNamedQuery(anyString())).thenReturn(query);
	}
	
	@Test
	public void shouldFindById() {
		payPalOrderCRUDBean.findById(22);
		verify(manager).createNamedQuery("findById");
	}
	
	
	@Test
	public void shouldFindByTokenAndAccountAndStatus() {
		payPalOrderCRUDBean.findByTokenAndAccountAndStatus("123", new Account(), OrderStatus.OPEN.toString());
		verify(manager).createNamedQuery("findByTokenAndAccountAndStatus");
	}
	
	@Test
	public void shouldFindByOrderStatus() {
		payPalOrderCRUDBean.findByOrderStatus(OrderStatus.OPEN);
		verify(manager).createNamedQuery("findAllPayPalOrderbyStatus");
	}
	
	@Test
	public void shouldFindWithStatusAndProductAssociation() {
		payPalOrderCRUDBean.findWithStatusAndProductAssociation(OrderStatus.PENDING, ProductAssociation.SHOP_INTERN);
		verify(manager).createNamedQuery("findAllPayPalOrderWithStatusAndProductAssociation");
	}
	
}
