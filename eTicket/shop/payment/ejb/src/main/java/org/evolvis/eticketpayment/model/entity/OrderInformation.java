package org.evolvis.eticketpayment.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.ProductLocale;
import org.evolvis.eticketpayment.model.entity.dto.OrderInformationDTO;
import org.evolvis.eticketshop.model.entity.ShopProduct;

@Entity
public class OrderInformation {
	@Id
	@GeneratedValue
	private long id; // auto
	@ManyToOne(optional = false)
	@NotNull
	private ESystem system; // PayPalOrderBean.generatePayPalOrder()
	@NotNull
	private OrderStatus orderStatus;
	@ManyToOne
	@NotNull
	private ShopProduct shopProduct; // PayPalOrderBean.generatePayPalOrder()
	@ManyToOne
	@NotNull
	private ProductLocale productLocale;
	@NotNull
	private long amount;
	@Column(name = "timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date; // auto
	
	public OrderInformation() {
		
	}
	
	public OrderInformation(OrderInformationDTO orderInformationDTO){
		fromDto(orderInformationDTO);
	}
	
	@SuppressWarnings("unused")
	@PrePersist
	private void prePersist() {
		setDate(new Date());
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public ESystem getSystem() {
		return system;
	}
	public void setSystem(ESystem system) {
		this.system = system;
	}
	public OrderStatus getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	} 
	
	public ShopProduct getShopProduct() {
		return shopProduct;
	}
	public void setShopProduct(ShopProduct shopProduct) {
		this.shopProduct = shopProduct;
	}
	
	public ProductLocale getProductLocale() {
		return productLocale;
	}

	public void setProductLocale(ProductLocale productLocale) {
		this.productLocale = productLocale;
	}
	
	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Transient
	public OrderInformationDTO toDto() {
		return OrderInformationDTO.createOrderDTOFromExsistingOrderEntity(id, orderStatus, system.toDto(), shopProduct.toDto(), productLocale.toDto(), amount, date);
	}

	@Transient
	public void fromDto(OrderInformationDTO orderInformationDTO) {
		this.orderStatus = orderInformationDTO.getOrderStatus();
		this.system = new ESystem(orderInformationDTO.getSystem());
		this.shopProduct = new ShopProduct(orderInformationDTO.getShopProductDTO());
		this.productLocale = new ProductLocale(orderInformationDTO.getProductLocaleDTO());
		this.amount = orderInformationDTO.getAmount();
		this.date = orderInformationDTO.getDate();
		
		if (orderInformationDTO.getId() > -1) {
			this.id = orderInformationDTO.getId();
		}

	}



}
