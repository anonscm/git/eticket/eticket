package org.evolvis.eticketpayment.business.invoice;

import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.platform.LocaleCRUD;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticketpayment.model.crud.invoice.InvoiceNumberCRUD;
import org.evolvis.eticketpayment.model.crud.invoice.InvoiceTemplateCRUD;
import org.evolvis.eticketpayment.model.entity.InvoiceField;
import org.evolvis.eticketpayment.model.entity.InvoiceNumber;
import org.evolvis.eticketpayment.model.entity.InvoiceTemplate;
import org.evolvis.eticketpayment.model.entity.dto.InvoiceNumberDTO;
import org.evolvis.eticketpayment.model.entity.dto.InvoiceTemplateDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;

@Stateless
@Remote(InvoiceBeanService.class)
public class InvoiceBean implements InvoiceBeanService {
	
	@EJB
	private InvoiceTemplateCRUD invoiceTemplateCRUDLocal;
	@EJB
	private LocaleCRUD localeCRUDRemote;
	@EJB
	private InvoiceNumberCRUD invoiceNumberCRUDLocal;

	@Override
	public byte[] generateInvoice(ShopProductDTO shopProductDTO, LocaleDTO localeDTO, Map<InvoiceField, Object> values) {
		Locale locale = localeCRUDRemote.findByCode(localeDTO.getCode());
		InvoiceTemplate invoiceTemplate = invoiceTemplateCRUDLocal.findByLocale(locale);
		return PdfCreator.generate(invoiceTemplate.getBinaryData(), values); 
	}

	@Override
	public void cerateInvoiceTemplate(InvoiceTemplateDTO invoiceTemplateDTO) {
		Locale locale = localeCRUDRemote.findByCode(invoiceTemplateDTO.getLocale().getCode());
		InvoiceTemplate invoiceTemplate = new InvoiceTemplate();
		invoiceTemplate.fromDto(invoiceTemplateDTO);
		invoiceTemplate.setLocale(locale);
		invoiceTemplateCRUDLocal.insert(invoiceTemplate);
	}

	@Override
	public void updateInvoiceTemplate(InvoiceTemplateDTO invoiceTemplateDTO) {
		Locale locale = localeCRUDRemote.findByCode(invoiceTemplateDTO.getLocale().getCode());
		InvoiceTemplate invoiceTemplate = invoiceTemplateCRUDLocal.findByLocale(locale);
		invoiceTemplate.setBinaryData(invoiceTemplateDTO.getBinaryData());
		invoiceTemplate.setDescription(invoiceTemplateDTO.getDescription());
		invoiceTemplateCRUDLocal.update(invoiceTemplate);
	}

	@Override
	public void deleteInvoiceTemplate(InvoiceTemplateDTO invoiceTemplateDTO) {
		Locale locale = localeCRUDRemote.findByCode(invoiceTemplateDTO.getLocale().getCode());
		InvoiceTemplate invoiceTemplate = invoiceTemplateCRUDLocal.findByLocale(locale);
		invoiceTemplateCRUDLocal.delete(invoiceTemplate);
	}

	@Override
	public void createOrUpdateInvoiceNumber(InvoiceNumberDTO invoiceNumberDTO) {
		InvoiceNumber invoiceNumber = invoiceNumberCRUDLocal.findInvoiceNumber(); 
		if(invoiceNumber != null){
			invoiceNumberCRUDLocal.delete(invoiceNumber);
		}
		InvoiceNumber number = new InvoiceNumber();
		number.fromDto(invoiceNumberDTO);
		invoiceNumberCRUDLocal.insert(number);
	}

	@Override
	public void deleteInvoiceNumber(InvoiceNumberDTO invoiceNumberDTO) {
		InvoiceNumber invoiceNumber = invoiceNumberCRUDLocal.findInvoiceNumber(); 
		if(invoiceNumber != null){
			invoiceNumberCRUDLocal.delete(invoiceNumber);
		}
	}

	@Override
	public String getNextInvoiceNumber() {
		InvoiceNumber invoiceNumber = invoiceNumberCRUDLocal.findInvoiceNumber();
		String value = String.valueOf(invoiceNumber.getCurrentNumber());
		invoiceNumber.setCurrentNumber(invoiceNumber.getCurrentNumber() + 1);
		invoiceNumberCRUDLocal.update(invoiceNumber);
		return value;
	}

}
