package org.evolvis.eticketpayment.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticketpayment.model.entity.dto.InvoiceTemplateDTO;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "locale_id" }))
@NamedQuery(name="findInvoiceTemplateByLocale", query="SELECT i FROM InvoiceTemplate i WHERE i.locale = ?1")
public class InvoiceTemplate implements Serializable{
	
	@Id
	@GeneratedValue
	private long id;
	private byte[] binaryData;
	private String description;
	@ManyToOne(optional = false)
	private Locale locale;
	private static final long serialVersionUID = 6860538220386176741L;
	
	public InvoiceTemplate() {
	
	}
	
	public byte[] getBinaryData() {
		return binaryData;
	}
	public void setBinaryData(byte[] binaryData) {
		this.binaryData = binaryData;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Locale getLocale() {
		return locale;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Transient
	public InvoiceTemplateDTO toDto(){
		return InvoiceTemplateDTO.init(binaryData, description, locale.toDto());
	}
	@Transient
	public void fromDto(InvoiceTemplateDTO invoiceTemplateDTO){
		this.binaryData = invoiceTemplateDTO.getBinaryData();
		this.description = invoiceTemplateDTO.getDescription();
		Locale locale = new Locale(invoiceTemplateDTO.getLocale());
		this.locale = locale;
	}
	
	
}
