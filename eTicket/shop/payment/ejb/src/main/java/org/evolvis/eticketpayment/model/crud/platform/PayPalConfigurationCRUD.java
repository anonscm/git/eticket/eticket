package org.evolvis.eticketpayment.model.crud.platform;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticketpayment.model.entity.PayPalConfiguration;
import org.evolvis.eticketshop.model.entity.ProductAssociation;


public interface PayPalConfigurationCRUD extends GenericCUD<PayPalConfiguration> {
    public PayPalConfiguration findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(ESystem system, String key, ProductAssociation productAssociation);
    public List<PayPalConfiguration> listPayPalConfigurationParameterBySystemAndProductAssociation(ESystem system, ProductAssociation productAssociation);
}
