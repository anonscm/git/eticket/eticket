package org.evolvis.eticketpayment.model.crud.paypalorder;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticketpayment.model.entity.OrderStatus;
import org.evolvis.eticketpayment.model.entity.PayPalOrder;
import org.evolvis.eticketshop.model.entity.ProductAssociation;

/**
 * PayPalOrderCRUDBean handels database-opreations which are associated with PayPalOrder
 * @author ben
 *
 */

@Stateless
@Local(PayPalOrderCRUD.class)
public class PayPalOrderCRUDBean extends GenericCUDSkeleton<PayPalOrder>
		implements PayPalOrderCRUD {

	private static final long serialVersionUID = -5870760368980604193L;

	@Override
	public PayPalOrder findById(long id) {
		return buildNamedQueryGetSingleResult("findById", id);
	}

	@Override
	public PayPalOrder findByTokenAndAccountAndStatus(String token, Account account,
			String status) {
		return buildNamedQueryGetSingleResult("findByTokenAndAccountAndStatus", token, account, status);
	}
	
	
	@Override
	public List <PayPalOrder> findByOrderStatus(OrderStatus status){
		return buildNamedQueryGetResults("findAllPayPalOrderbyStatus",status);
	}

	@Override
	public List<PayPalOrder> findWithStatusAndProductAssociation(
			OrderStatus status, ProductAssociation productAssociation) {
		return buildNamedQueryGetResults("findAllPayPalOrderWithStatusAndProductAssociation", status, productAssociation);
	}

}
