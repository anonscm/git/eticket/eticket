package org.evolvis.eticketpayment.model.crud.orderinformation;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticketpayment.model.entity.OrderInformation;

public interface OrderInformationCRUD extends GenericCUD<OrderInformation> {

}
