package org.evolvis.eticketpayment.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import org.evolvis.eticketpayment.model.entity.dto.InvoiceNumberDTO;

@Entity
@NamedQuery(name="findInvoiceNumber", query="SELECT i FROM InvoiceNumber i")
public class InvoiceNumber implements Serializable{
	
	private static final long serialVersionUID = -1496759578542346683L;
	@Id
	@GeneratedValue()
	private long id;
	private long maxNumber;
	private long currentNumber;	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getMaxNumber() {
		return maxNumber;
	}
	public void setMaxNumber(long maxNumber) {
		this.maxNumber = maxNumber;
	}
	public long getCurrentNumber() {
		return currentNumber;
	}
	public void setCurrentNumber(long currentNumber) {
		this.currentNumber = currentNumber;
	}
	
	@Transient
	public InvoiceNumberDTO toDto(){
		return InvoiceNumberDTO.init(maxNumber, currentNumber);
	}
	@Transient
	public void fromDto(InvoiceNumberDTO invoiceNumberDTO){
		this.maxNumber = invoiceNumberDTO.getMaxNumber();
		this.currentNumber = invoiceNumberDTO.getCurrentNumber();
	}
}
