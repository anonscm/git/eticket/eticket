package org.evolvis.eticketpayment.model.crud.paymenttransaction;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticketpayment.model.entity.PaymentTransaction;

@Stateless
@Local(PaymentTransactionCRUD.class)
public class PaymentTransactionCRUDBean extends GenericCUDSkeleton<PaymentTransaction> implements PaymentTransactionCRUD {
	private static final long serialVersionUID = 2820949803759977864L;
}
