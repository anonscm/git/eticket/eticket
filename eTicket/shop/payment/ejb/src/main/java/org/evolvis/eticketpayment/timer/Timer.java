package org.evolvis.eticketpayment.timer;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;

import org.evolvis.eticket.business.account.AccountBeanService;
import org.evolvis.eticket.business.account.AccountSAShopBeanService;
import org.evolvis.eticket.business.payment.PaymentBeanService;
import org.evolvis.eticket.business.product.ProductTransferService;
import org.evolvis.eticket.business.template.ShopTextTemplateBeanService;
import org.evolvis.eticket.model.entity.dto.AccountSAShopDTO;
import org.evolvis.eticket.model.entity.dto.PersonalInformationDTO;
import org.evolvis.eticketpayment.business.invoice.InvoiceBeanService;
import org.evolvis.eticketpayment.business.paypalorder.PayPalException;
import org.evolvis.eticketpayment.business.paypalorder.PayPalOrderBeanService;
import org.evolvis.eticketpayment.model.crud.paypalorder.PayPalOrderCRUD;
import org.evolvis.eticketpayment.model.entity.InvoiceField;
import org.evolvis.eticketpayment.model.entity.OrderStatus;
import org.evolvis.eticketpayment.model.entity.PayPalOrder;
import org.evolvis.eticketpayment.model.entity.dto.OrderInformationDTO;
import org.evolvis.eticketpayment.model.entity.dto.PayPalOrderDTO;
import org.evolvis.eticketshop.business.mail.EMailBeanService;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.TextTemplateNames;
import org.evolvis.eticketshop.model.entity.dto.EMailAccountDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopTextTemplateDTO;

@Singleton
public class Timer {
	@EJB
	private PayPalOrderCRUD payPalOrderCRUDLocal;
	@EJB
	private PayPalOrderBeanService payPalOrderBean;
	@EJB
	private AccountSAShopBeanService accountSAShopBeanService;
	@EJB
	private PaymentBeanService paymentBeanService;
	@EJB
	private InvoiceBeanService invoiceBeanService;
	@EJB
	private EMailBeanService eMailBeanService;
	@EJB
	private AccountBeanService accountBeanService;
	@EJB
	private ShopTextTemplateBeanService shopTextTemplateBeanService;
	@EJB
	private ProductTransferService productTransferBeanService;
	@EJB
	private PayPalOrderBeanService payPalOrderBeanService;

	@Schedule(second = "1", minute = "*", hour = "*", persistent = false)
	public void doWork() throws PayPalException, Exception {
		List<PayPalOrder> orders = payPalOrderCRUDLocal
				.findWithStatusAndProductAssociation(OrderStatus.PENDING,
						ProductAssociation.SHOP_EXTERN);
		for (PayPalOrder payPalOrder : orders) {
			OrderStatus status = payPalOrderBean
					.checkStatusOfPayPalOrder(payPalOrder.toDto());
			if (status.equals(OrderStatus.COMPLETED)) {
				completePayment(payPalOrder);
			}
		}

		orders = payPalOrderCRUDLocal.findWithStatusAndProductAssociation(
				OrderStatus.PENDING, ProductAssociation.SHOP_INTERN);
		for (PayPalOrder payPalOrder : orders) {
			OrderStatus status = payPalOrderBean
					.checkStatusOfPayPalOrder(payPalOrder.toDto());
			if (status.equals(OrderStatus.COMPLETED)) {
				completePaymentForInternShop(payPalOrder);
			}
		}
	}

	/**
	 * Complete payment. extern
	 */
	private void completePayment(PayPalOrder payPalOrder) {
		AccountSAShopDTO accountSAShopDTO = accountSAShopBeanService
				.getAccount(payPalOrder.toDto());
		Locale locale = new Locale(accountSAShopDTO.getPersonalInformation()
				.getLocaleDTO().getCode());
		paymentBeanService.completePayment(locale, accountSAShopDTO);
	}

	/**
	 * Complete payment. intern
	 * @throws Exception 
	 * @throws PayPalException 
	 */
	private void completePaymentForInternShop(PayPalOrder payPalOrder) throws PayPalException, Exception {
		if(payPalOrderBean.checkStatusOfPayPalOrder(payPalOrder.toDto()) == OrderStatus.COMPLETED) {
			//transfer
			transferProduct(payPalOrder);
			sendInvoice(payPalOrder);
		}
		
	
	}

	/**
	 * Send invoice
	 */
	private void sendInvoice(PayPalOrder payPalOrder) {
		payPalOrder.getOrderInformation().getSystem().toDto(false);

		byte pdf[] = invoiceBeanService.generateInvoice(payPalOrder
				.getOrderInformation().getShopProduct().toDto(), payPalOrder
				.getOrderInformation().getProductLocale().getLocale().toDto(),
				generatePdfMap(payPalOrder));

		PersonalInformationDTO personalInformationDTO = accountBeanService
				.getPersonalInformationDTO(payPalOrder.getOrderInformation()
						.getSystem().toDto(false), payPalOrder
						.getPaymentTransaction().getAccount().getCredential()
						.getUsername());
		EMailAccountDTO eMailAccountDTO = EMailAccountDTO.init(payPalOrder
				.getPaymentTransaction().getAccount().getCredential()
				.getUsername(), personalInformationDTO.getFirstname() + " "
				+ personalInformationDTO.getLastname());
		ShopTextTemplateDTO shopTextTemplateDTO = shopTextTemplateBeanService
				.find(payPalOrder.getOrderInformation().getSystem()
						.toDto(false), payPalOrder.getOrderInformation()
						.getProductLocale().getLocale().toDto(),
						TextTemplateNames.BOUGHT_PRODUCT_INTERN.toString());

		eMailBeanService.sendMail(payPalOrder.getOrderInformation().getSystem()
				.toDto(false), eMailAccountDTO, shopTextTemplateDTO,
				new HashMap<String, String>(), pdf);
	}

	private Map<InvoiceField, Object> generatePdfMap(PayPalOrder payPalOrder) {
		Locale locale = new Locale(payPalOrder.getOrderInformation()
				.getProductLocale().getLocale().getLocalecode());
		Map<InvoiceField, Object> ret = new HashMap<InvoiceField, Object>();

		ret.put(InvoiceField.AMOUNT,
				String.valueOf(payPalOrder.getOrderInformation().getAmount()));
		ret.put(InvoiceField.DATE,
				DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(
						new Date()));
		ret.put(InvoiceField.DELIVERYDATE,
				DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(
						new Date()));
		ret.put(InvoiceField.INVOICENUMBER,
				invoiceBeanService.getNextInvoiceNumber());
		ret.put(InvoiceField.PRODUCTNAME, payPalOrder.getOrderInformation()
				.getProductLocale().getProductName());
		ret.put(InvoiceField.TAXNUMBER, "?TAXNUMBER?");
		ret.put(InvoiceField.TAXRATE,
				String.valueOf(payPalOrder.getOrderInformation()
						.getProductLocale().getSalesTax() * 100));
		return ret;
	}
	
	private void transferProduct(PayPalOrder payPalOrder) {
		productTransferBeanService
				.transferOrderedSystemProductToAccountAndAccept(payPalOrder.getOrderInformation().getShopProduct().getProduct().toDto(), payPalOrder.getPaymentTransaction().getAccount().toAccountDTO(), payPalOrder.getOrderInformation().getAmount());

		OrderInformationDTO orderInformationDTO = OrderInformationDTO
				.createOrderDTOFromExsistingOrderEntity(payPalOrder.toDto()
						.getOrderInformationDTO().getId(),
						OrderStatus.COMPLETED_AND_DELIVERED, payPalOrder.toDto()
								.getOrderInformationDTO().getSystem(),
								payPalOrder.toDto().getOrderInformationDTO()
								.getShopProductDTO(), payPalOrder.toDto().getOrderInformationDTO().getProductLocaleDTO(), payPalOrder.toDto().getOrderInformationDTO().getAmount(), payPalOrder.toDto()
								.getOrderInformationDTO().getDate());

		PayPalOrderDTO newPalOrderDTO = PayPalOrderDTO
				.createPayPalOrderDTOFromExsistingPayPalOrderEntity(
						payPalOrder.toDto().getId(), payPalOrder.toDto().getToken(), payPalOrder.toDto().getPayPalEmailAddress(),
						payPalOrder.toDto().getPayerId(),
						payPalOrder.toDto().getTransactionId(), orderInformationDTO,
						payPalOrder.toDto().getTransactionDTO());

		payPalOrderBeanService.update(null, newPalOrderDTO);
	}
	
	public void setPayPalOrderCRUDLocal(PayPalOrderCRUD payPalOrderCRUDLocal) {
		this.payPalOrderCRUDLocal = payPalOrderCRUDLocal;
	}

	public void setPayPalOrderBean(PayPalOrderBeanService payPalOrderBean) {
		this.payPalOrderBean = payPalOrderBean;
	}

	public void setAccountSAShopBeanService(
			AccountSAShopBeanService accountSAShopBeanService) {
		this.accountSAShopBeanService = accountSAShopBeanService;
	}

	public void setPaymentBeanService(PaymentBeanService paymentBeanService) {
		this.paymentBeanService = paymentBeanService;
	}

	public void setInvoiceBeanService(InvoiceBeanService invoiceBeanService) {
		this.invoiceBeanService = invoiceBeanService;
	}

	public void seteMailBeanService(EMailBeanService eMailBeanService) {
		this.eMailBeanService = eMailBeanService;
	}

	public void setAccountBeanService(AccountBeanService accountBeanService) {
		this.accountBeanService = accountBeanService;
	}

	public void setShopTextTemplateBeanService(
			ShopTextTemplateBeanService shopTextTemplateBeanService) {
		this.shopTextTemplateBeanService = shopTextTemplateBeanService;
	}

	public void setProductTransferBeanService(
			ProductTransferService productTransferBeanService) {
		this.productTransferBeanService = productTransferBeanService;
	}

	public void setPayPalOrderBeanService(
			PayPalOrderBeanService payPalOrderBeanService) {
		this.payPalOrderBeanService = payPalOrderBeanService;
	}


}
