package org.evolvis.eticketpayment.business.paypalorder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.crud.user.AccountCRUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticketpayment.model.crud.orderinformation.OrderInformationCRUD;
import org.evolvis.eticketpayment.model.crud.paymenttransaction.PaymentTransactionCRUD;
import org.evolvis.eticketpayment.model.crud.paypalorder.PayPalOrderCRUD;
import org.evolvis.eticketpayment.model.crud.platform.PayPalConfigurationCRUD;
import org.evolvis.eticketpayment.model.entity.OrderInformation;
import org.evolvis.eticketpayment.model.entity.OrderStatus;
import org.evolvis.eticketpayment.model.entity.PayPalOrder;
import org.evolvis.eticketpayment.model.entity.PayPalOrder.PayPalOrderCheckoutStatus;
import org.evolvis.eticketpayment.model.entity.PaymentTransaction;
import org.evolvis.eticketpayment.model.entity.dto.OrderInformationDTO;
import org.evolvis.eticketpayment.model.entity.dto.PayPalOrderDTO;
import org.evolvis.eticketpayment.model.entity.dto.PaymentTransactionDTO;
import org.evolvis.eticketshop.model.crud.product.ShopProductCRUD;
import org.evolvis.eticketshop.model.entity.ProductAssociation;
import org.evolvis.eticketshop.model.entity.ShopProduct;

/**
 * With {@link PayPalOrderBean} you are able to create, read, update and delete
 * a paypalorder in a system. The CUD parts are limited to user who have at
 * least 'productadmin' rights.
 * 
 * @author ben
 * 
 */

@Stateless
@Remote(PayPalOrderBeanService.class)
public class PayPalOrderBean implements PayPalOrderBeanService {
	private static final String USER = "USER";
	private static final String PWD = "PWD";
	private static final String SIGNATURE = "SIGNATURE";
	private static final String RETURNURL = "RETURNURL";
	private static final String CANCELURL = "CANCELURL";
	private static final String PAYPALAPI = "PAYPALAPI";
	private static final String PAYPALPAY = "PAYPALPAY";
	private static final String VERSION = "VERSION";
	private static final String PAYMENTREQUEST_0_PAYMENTACTION = "PAYMENTREQUEST_0_PAYMENTACTION";
	private static final String CHECKOUTSTATUS = "CHECKOUTSTATUS";
	private Properties properties;

	@EJB
	private PayPalOrderCRUD paypalorderCRUDBeanLocal;

	@EJB
	private ShopProductCRUD shopProductCRUDBeanLocal;

	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;

	@EJB
	private PaymentTransactionCRUD paymentTransactionCRUD;

	@EJB
	private OrderInformationCRUD orderInformationCRUD;

	@EJB
	private AccountCRUD accountCRUDBean;

	@EJB
	private PayPalConfigurationCRUD payPalConfigurationCRUD;

	public PayPalOrderBean() {

	}

	/*
	 * 
	 * Public database methods to create, update and delete PayPalOrder. A
	 * normal user need the rights to create and update a PayPalOrder!
	 */
	@Override
	public void create(AccountDTO productAdmin, PayPalOrderDTO paypalorderDTO) {
		PayPalOrder payPalOrder = generatePayPalOrder(paypalorderDTO);
		paypalorderCRUDBeanLocal.insert(payPalOrder);
	}

	@Override
	public void update(AccountDTO productAdmin, PayPalOrderDTO paypalorderDTO) {
		PayPalOrder payPalOrder = getPayPalOrder(paypalorderDTO);
		// TODO Account Problem - If you use
		// "payPalOrder.fromDto(paypalorderDTO)", then
		// paypalorder.paymentTransaction.account is null, because there is no
		// easy way to translate accountDTO to account. AccountDTO and Account
		// must be fixed so that Account.fromDto and Account.toDto can be used.
		payPalOrder.fromDto(paypalorderDTO);
		orderInformationCRUD.update(payPalOrder.getOrderInformation());
		paypalorderCRUDBeanLocal.update(payPalOrder);
	}

	@Override
	public void delete(AccountDTO productAdmin, PayPalOrderDTO paypalorderDTO) {
		PayPalOrder payPalOrder = getPayPalOrder(paypalorderDTO);
		paypalorderCRUDBeanLocal.delete(payPalOrder);
	}

	@Override
	public void updateOrderstatus(long paypalId, OrderStatus updatedStatus) {
		PayPalOrder order = paypalorderCRUDBeanLocal.findById(paypalId);
		order.getOrderInformation().setOrderStatus(updatedStatus);
		paypalorderCRUDBeanLocal.update(order);
	}

	/*
	 * 
	 * Public methods to start a paypal-transaction and complete a paypal
	 * transaction
	 */
	@Override
	public String[] startPaymentProcess(AccountDTO accountDTO,
			ProductLocaleDTO productLocaleDTO, String product_uin,
			ESystemDTO eSystemDTO, int quantity) throws Exception {

		if (productLocaleDTO == null || product_uin == null
				|| eSystemDTO == null || quantity <= 0) {
			throw new Exception(
					"You must set productLocaleDTO, product_uin, eSystemDTO and quantity!");
		}

		String ret[] = new String[2];

		// get the shopProduct
		ESystem eSystem = eSystemCRUDBeanLocal.findById(eSystemDTO.getId());
		ShopProduct shopProduct = shopProductCRUDBeanLocal
				.findByProductUinAndProductSystem(product_uin, eSystem);

		ProductAssociation productAssociation = null;
		productAssociation = shopProduct.getProductAssociation();
		setProperties(productAssociation, eSystem);

		String message = paypalAPISetExpresscheckout(productLocaleDTO, quantity);
		if (isAcknowledgementSuccess(message) == false) {
			throw new PayPalException(
					"PaypalAPISetExpresscheckout - Paypal acknowledgement-status is not Success",
					getErrorFromMessage(message));
		}

		// Create TransactionDTO database entry
		PaymentTransactionDTO paymentTransactionDTO = null;
		if (accountDTO != null) {
			paymentTransactionDTO = PaymentTransactionDTO
					.createNewTransactionDTO(accountDTO);
			PaymentTransaction paymentTransaction = new PaymentTransaction(
					paymentTransactionDTO);
			Account account = accountCRUDBean.findBySystemAndUserName(
					new ESystem(accountDTO.getSystem()),
					accountDTO.getUsername());
			paymentTransaction.setAccount(account);
			paymentTransactionCRUD.insert(paymentTransaction);
			paymentTransactionDTO = paymentTransaction.toDto();
		}

		// Create OrderDTO database entry
		OrderInformationDTO orderInformationDTO = OrderInformationDTO
				.createNewOrderDTO(OrderStatus.OPEN, eSystemDTO,
						shopProduct.toDto(), productLocaleDTO, quantity);
		OrderInformation orderInformation = new OrderInformation(
				orderInformationDTO);

		orderInformationCRUD.insert(orderInformation);
		orderInformationDTO = orderInformation.toDto();

		// Create PayPalOrderDTO database entry
		PayPalOrderDTO payPalOrderDTO = PayPalOrderDTO.createNewPayPalOrderDTO(

		getTokenFromMessage(message), null, null, null, orderInformationDTO,
				paymentTransactionDTO);

		// Commit
		PayPalOrder payPalOrder = generatePayPalOrder(payPalOrderDTO);
		paypalorderCRUDBeanLocal.insert(payPalOrder);
		payPalOrderDTO = payPalOrder.toDto();

		ret[0] = String.valueOf(payPalOrderDTO.getId());
		ret[1] = properties.getProperty(PAYPALPAY)
				+ getTokenFromMessage(message);
		return ret;
	}

	@Override
	public PayPalOrderDTO completeTransaction(long id, String token,
			String payerId) throws PayPalException, Exception {
		String message = null;

		if (token == null || payerId == null || id < 0) {
			throw new Exception(
					"Token and PayerId must no be null. Id must be a positiv value");
		}

		// Get PayPalOrder
		PayPalOrder payPalOrder = paypalorderCRUDBeanLocal.findById(id);

		// Throw Exception if PayPalOrder could not be found.
		if (payPalOrder == null) {
			throw new Exception(
					"Could not find PayPalOrder-Object in database.");
		}

		ProductAssociation productAssociation = payPalOrder
				.getOrderInformation().getShopProduct().getProductAssociation();
		setProperties(productAssociation, payPalOrder.getOrderInformation()
				.getSystem());

		// Set payerid
		payPalOrder.setPayerId(payerId);

		// Try to get transaction details.
		message = paypalAPIGetExpressCheckoutDetails(token);
		if (isAcknowledgementSuccess(message) == false) {
			String error = getErrorFromMessage(message);
			// set status to FAILURE
			payPalOrder.getOrderInformation().setOrderStatus(
					OrderStatus.FAILURE);
			update(null, payPalOrder.toDto());
			throw new PayPalException(
					"PayPal-API GetExpressCheckoutDetails - ACK is not set to Success.",
					error);
		}

		payPalOrder
				.setPayPalEmailAddress(getPayPalEmailAddressFromMessage(message));

		// Try to complete transaction.
		message = paypalAPIDoExpressCheckoutPayment(token, payerId,
				getTotalPriceFromMessage(message),
				getCurrencyCodeFromMesage(message));
		if (isAcknowledgementSuccess(message) == false) {
			String error = getErrorFromMessage(message);
			// set status to FAILURE
			payPalOrder.getOrderInformation().setOrderStatus(
					OrderStatus.FAILURE);
			update(null, payPalOrder.toDto());
			throw new PayPalException(
					"PayPal-API DoExpressCheckoutPayment - ACK is not set to Success.",
					error);
		}
		// set transaction id
		payPalOrder.setTransactionId(getTransactionIdFromMessage(message));

		// Verify if transaction has been completed.
		message = paypalAPIGetExpressCheckoutDetails(token);
		payPalOrder = verifyTransactionStatus(message, payPalOrder);

		update(null, payPalOrder.toDto());
		return payPalOrder.toDto();
	}

	@Override
	public OrderStatus checkStatusOfPayPalOrder(PayPalOrderDTO payPalOrderDTO)
			throws PayPalException, Exception {

		if (payPalOrderDTO == null) {
			throw new Exception("PayPalOrderDTO could not be null!");
		}

		PayPalOrder payPalOrder = null;
		String message = null;

		if ((payPalOrder = paypalorderCRUDBeanLocal.findById(payPalOrderDTO
				.getId())) == null) {
			throw new Exception("Could not find entity in database!");
		}

		ProductAssociation productAssociation = payPalOrder
				.getOrderInformation().getShopProduct().getProductAssociation();
		setProperties(productAssociation, payPalOrder.getOrderInformation()
				.getSystem());

		message = paypalAPIGetExpressCheckoutDetails(payPalOrderDTO.getToken());
		payPalOrder = verifyTransactionStatus(message, payPalOrder);
		update(null, payPalOrder.toDto());

		return payPalOrder.toDto().getOrderInformationDTO().getOrderStatus();
	}

	@Override
	public List<PayPalOrderDTO> getOrders(AccountDTO AdminAccount,
			OrderStatus type) {
		List<PayPalOrderDTO> ordersDTO = new ArrayList<PayPalOrderDTO>();
		List<PayPalOrder> orders = paypalorderCRUDBeanLocal
				.findByOrderStatus(type);
		for (PayPalOrder order : orders) {
			ordersDTO.add(order.toDto());
		}
		return ordersDTO;
	}

	/*
	 * 
	 * PayPal-Helper-Methods to extract information from an paypal-respone
	 */

	/**
	 * Check return value of an called PayPal-API function
	 * 
	 * @param message
	 *            Http response from Paypal
	 * @return Success = true, otherwise = false
	 * @throws PayPalException
	 */
	private boolean isAcknowledgementSuccess(String message)
			throws PayPalException {
		if (message == null) {
			throw new PayPalException(
					"Acknowledgement status could not be determined. Message is NULL",
					"");
		}
		if (message.contains("ACK=Success")) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the eMail address of paypal account
	 * 
	 * @param message
	 *            Http response from Paypal
	 * @return payPal EMail Address
	 * @throws PayPalException
	 */
	private String getPayPalEmailAddressFromMessage(String message)
			throws PayPalException {
		String email = message.substring(message.indexOf("EMAIL="));
		email = email.replace("EMAIL=", "");
		email = email.substring(0, email.indexOf("&"));

		if (email == null) {
			throw new PayPalException(
					"Could not get paypal eMail address from message!", "");
		} else if (email.length() < 1) {
			throw new PayPalException(
					"Could not get paypal eMail address from message!", "");
		}

		return email;
	}

	/**
	 * Function is used to check the value of CHECKOUTSTATUS. This value is
	 * needed to verify the status of the transaction. Possible values:
	 * PaymentActionNotInitiated PaymentActionFailed PaymentActionInProgress
	 * PaymentCompleted
	 * 
	 * @param message
	 *            Http response from Paypal
	 * @return PayPalOrderCheckoutStatus
	 */
	private PayPalOrderCheckoutStatus getCheckoutStatusFromMessage(
			String message) {
		if (message
				.contains(CHECKOUTSTATUS
						+ "="
						+ PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionNotInitiated
								.toString())) {
			return PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionNotInitiated;
		} else if (message.contains(CHECKOUTSTATUS
				+ "="
				+ PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionFailed
						.toString())) {
			return PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionFailed;
		} else if (message.contains(CHECKOUTSTATUS
				+ "="
				+ PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionInProgress
						.toString())) {
			return PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionInProgress;
		} else if (message.contains(CHECKOUTSTATUS
				+ "="
				+ PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionCompleted
						.toString())) {
			return PayPalOrder.PayPalOrderCheckoutStatus.PaymentActionCompleted;
		}

		return null;
	}

	/**
	 * Extract token form paypal message
	 * 
	 * @param message
	 *            Http response from Paypal
	 * @return returns a token from a paypal message.
	 */
	private String getTokenFromMessage(String message) {
		String token = message.substring(message.indexOf("TOKEN="));
		token = token.replace("TOKEN=", "");
		token = token.substring(0, token.indexOf("&"));
		return token;
	}

	/**
	 * Extract total price from paypal message
	 * 
	 * @param Http
	 *            response from Paypal
	 * @return
	 * @throws Exception
	 */
	private String getTotalPriceFromMessage(String message) throws Exception {
		String total = null;
		total = message.substring(message.indexOf("PAYMENTREQUEST_0_AMT="));
		total = total.replace("PAYMENTREQUEST_0_AMT=", "");
		total = total.substring(0, total.indexOf("&"));

		if (total == null) {
			throw new Exception(
					"getTotalPriceFromMessage - Could not get total.");
		} else if (total.length() < 1) {
			throw new Exception(
					"getTotalPriceFromMessage - Could not get total.");
		}

		return total;
	}

	/**
	 * Extract transaction id from paypal message
	 * 
	 * @param Http
	 *            response from Paypal
	 * @return
	 * @throws Exception
	 */
	private String getTransactionIdFromMessage(String message) throws Exception {
		String id = null;
		id = message.substring(message.indexOf("PAYMENTINFO_0_TRANSACTIONID="));
		id = id.replace("PAYMENTINFO_0_TRANSACTIONID=", "");
		id = id.substring(0, id.indexOf("&"));

		if (id == null) {
			throw new Exception(
					"getTransactionIdFromMessage - Could not get transactionid.");
		} else if (id.length() < 1) {
			throw new Exception(
					"getTransactionIdFromMessage - Could not get transactionid.");
		}

		return id;
	}

	/**
	 * 
	 * @param message
	 * @return
	 * @throws Exception
	 */
	private String getCurrencyCodeFromMesage(String message) throws Exception {
		final String CURRENCYCODE = "CURRENCYCODE=";
		final String PAYMENTINFO_0_CURRENCYCODE = "PAYMENTINFO_0_CURRENCYCODE=";
		String cc = null;

		if (message.contains(CURRENCYCODE)) {
			cc = message.substring(message.indexOf(CURRENCYCODE));
			cc = cc.replace(CURRENCYCODE, "");
			cc = cc.substring(0, cc.indexOf("&"));
		} else if ((message.contains(PAYMENTINFO_0_CURRENCYCODE))) {
			cc = message.substring(message.indexOf(PAYMENTINFO_0_CURRENCYCODE));
			cc = cc.replace(PAYMENTINFO_0_CURRENCYCODE, "");
			cc = cc.substring(0, cc.indexOf("&"));
		}

		if (cc == null) {
			throw new Exception(
					"getCurrencyCodeFromMesage - Could not get currency code from message.");
		} else if (cc.length() < 1) {
			throw new Exception(
					"getCurrencyCodeFromMesage - Could not get currency code from message.");
		}

		return cc;
	}

	/**
	 * Extract all error messages from paypal message
	 * 
	 * @param Http
	 *            response from Paypal
	 * @return String with all error messages
	 */
	private String getErrorFromMessage(String message) {
		String error = "";
		int counter = 0;
		while (message.contains("L_LONGMESSAGE" + counter)) {
			message = message.substring(message.indexOf("L_LONGMESSAGE"
					+ counter + "="));
			message.replace("L_LONGMESSAGE" + counter + "=", "");
			error = error + message.substring(0, message.indexOf("&")) + "\n";
			counter++;
		}
		return error;
	}

	/**
	 * Connect to paypal api and try to check transaction status of an paypal
	 * order
	 * 
	 * @param payPalApiMessage
	 * @param payPalOrder
	 * @return paypal order with updated transaction status
	 * @throws Exception
	 */
	private PayPalOrder verifyTransactionStatus(String payPalApiMessage,
			PayPalOrder payPalOrder) throws Exception {
		if (payPalOrder == null) {
			throw new Exception("PayPalOrder could not be null!");
		}

		if (isAcknowledgementSuccess(payPalApiMessage) == false) {
			String error = getErrorFromMessage(payPalApiMessage);
			// set status to FAILURE
			payPalOrder.getOrderInformation().setOrderStatus(
					OrderStatus.FAILURE);
			update(null, payPalOrder.toDto());
			throw new PayPalException(
					"PayPal-API GetExpressCheckoutDetails - ACK is not set to Success.",
					error);
		}

		PayPalOrderCheckoutStatus payPalOrderCheckoutStatus = getCheckoutStatusFromMessage(payPalApiMessage);
		if (payPalOrderCheckoutStatus == null) {
			String error = getErrorFromMessage(payPalApiMessage);
			// set status to FAILURE
			payPalOrder.getOrderInformation().setOrderStatus(
					OrderStatus.FAILURE);
			update(null, payPalOrder.toDto());
			throw new PayPalException(
					"PayPal-API GetExpressCheckoutDetails - CHECKOUTSTATUS is null.",
					error);
		}

		// set CheckoutStatus and return
		if (payPalOrderCheckoutStatus.toString().equals(
				PayPalOrderCheckoutStatus.PaymentActionFailed.toString())) {
			payPalOrder.getOrderInformation().setOrderStatus(
					OrderStatus.FAILURE);
		} else if (payPalOrderCheckoutStatus.toString().equals(
				PayPalOrderCheckoutStatus.PaymentActionInProgress.toString())) {
			payPalOrder.getOrderInformation().setOrderStatus(
					OrderStatus.PENDING);
		} else if (payPalOrderCheckoutStatus.toString().equals(
				PayPalOrderCheckoutStatus.PaymentActionNotInitiated.toString())) {
			payPalOrder.getOrderInformation().setOrderStatus(
					OrderStatus.FAILURE);
		} else if (payPalOrderCheckoutStatus.toString().equals(
				PayPalOrderCheckoutStatus.PaymentActionCompleted.toString())) {
			payPalOrder.getOrderInformation().setOrderStatus(
					OrderStatus.COMPLETED);
		} else {
			payPalOrder.getOrderInformation().setOrderStatus(
					OrderStatus.FAILURE);
		}

		return payPalOrder;
	}

	/*
	 * 
	 * PayPal-API
	 */

	/**
	 * Initiate a URLConnection to PayPal-Server
	 * 
	 * @return URLConnection
	 */
	private URLConnection paypalURLConnection() {
		try {
			URLConnection uRLConnection;
			URL uRL = new URL(properties.getProperty(PAYPALAPI));
			uRLConnection = uRL.openConnection();
			return uRLConnection;
		} catch (Exception ex) {
		}
		return null;
	}

	/**
	 * Method is called to send a message to paypal.
	 * 
	 * @param message
	 *            Message to be send
	 * @param uRLConnection
	 *            A established URLConnection
	 * @return Return message of the called api-function.
	 * @throws Exception
	 */
	private String sendDataToPayPal(String message, URLConnection uRLConnection)
			throws Exception {
		String res = "";

		uRLConnection.setDoOutput(true);
		BufferedReader rd;
		OutputStreamWriter wr;

		wr = new OutputStreamWriter(uRLConnection.getOutputStream());
		wr.write(message);
		wr.flush();
		rd = new BufferedReader(new InputStreamReader(
				uRLConnection.getInputStream()));
		String line;
		while ((line = rd.readLine()) != null) {
			res = res + line;
		}
		rd.close();
		return URLDecoder.decode(res, "UTF-8");
	}

	/**
	 * PayPal-API-method which is used to initiate a payment-process. This is
	 * the first method to be called.
	 * 
	 * @param productLocaleDTO
	 *            Holds all information of an product (like price).
	 * @param quantity
	 *            How much the user want to buy
	 * @return Return message of the called api-function.
	 * @throws Exception
	 */
	private String paypalAPISetExpresscheckout(
			ProductLocaleDTO productLocaleDTO, int quantity) throws Exception {
		URLConnection uRLConnection;

		if ((uRLConnection = paypalURLConnection()) == null) {
			throw new Exception("Could not establish connection to paypal.");
		}

		BigDecimal bd = productLocaleDTO.getPriceIncludingTax();
		bd = bd.multiply(new BigDecimal(quantity));
		String totalPrice = bd.toString();

		String itemPrice = productLocaleDTO.getPriceIncludingTax().toString();

		totalPrice = twoDecimalPositions(totalPrice);
		itemPrice = twoDecimalPositions(itemPrice);

		String message = URLEncoder.encode("METHOD", "UTF-8") + "="
				+ URLEncoder.encode("SetExpressCheckout", "UTF-8");
		message += "&" + URLEncoder.encode("USER", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(USER), "UTF-8");
		message += "&" + URLEncoder.encode("PWD", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(PWD), "UTF-8");
		message += "&" + URLEncoder.encode("SIGNATURE", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(SIGNATURE), "UTF-8");
		message += "&" + URLEncoder.encode("VERSION", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(VERSION), "UTF-8");
		message += "&"
				+ URLEncoder.encode("PAYMENTREQUEST_0_PAYMENTACTION", "UTF-8")
				+ "="
				+ URLEncoder
						.encode(properties
								.getProperty("PAYMENTREQUEST_0_PAYMENTACTION"),
								"UTF-8");
		message += "&" + URLEncoder.encode("PAYMENTREQUEST_0_AMT", "UTF-8")
				+ "=" + URLEncoder.encode(totalPrice, "UTF-8");
		message += "&" + URLEncoder.encode("L_PAYMENTREQUEST_0_NAME0", "UTF-8")
				+ "="
				+ URLEncoder.encode(productLocaleDTO.getProductName(), "UTF-8");
		message += "&" + URLEncoder.encode("L_PAYMENTREQUEST_0_AMT0", "UTF-8")
				+ "=" + URLEncoder.encode(itemPrice, "UTF-8");
		message += "&" + URLEncoder.encode("L_PAYMENTREQUEST_0_QTY0", "UTF-8")
				+ "=" + URLEncoder.encode(String.valueOf(quantity), "UTF-8");
		message += "&"
				+ URLEncoder.encode("PAYMENTREQUEST_0_CURRENCYCODE", "UTF-8")
				+ "="
				+ URLEncoder
						.encode(productLocaleDTO.getCurrencyCode(), "UTF-8");
		message += "&" + URLEncoder.encode("RETURNURL", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(RETURNURL), "UTF-8");
		message += "&" + URLEncoder.encode("CANCELURL", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(CANCELURL), "UTF-8");

		return sendDataToPayPal(message, uRLConnection);
	}

	/**
	 * PayPal-API-method which is used to get informations about the transaction
	 * status. This method is the second and fourth method to be called.
	 * 
	 * @param token
	 *            Token from paypal which is returned by SetExpresscheckout
	 * @return Return message of the called api-function.
	 * @throws Exception
	 */
	private String paypalAPIGetExpressCheckoutDetails(String token)
			throws Exception {
		URLConnection uRLConnection;
		if ((uRLConnection = paypalURLConnection()) == null) {
			throw new Exception("Could not establish connection to paypal.");
		}

		String message = URLEncoder.encode("METHOD", "UTF-8") + "="
				+ URLEncoder.encode("GetExpressCheckoutDetails", "UTF-8");
		message += "&" + URLEncoder.encode("USER", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(USER), "UTF-8");
		message += "&" + URLEncoder.encode("PWD", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(PWD), "UTF-8");
		message += "&" + URLEncoder.encode("SIGNATURE", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(SIGNATURE), "UTF-8");
		message += "&" + URLEncoder.encode("VERSION", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(VERSION), "UTF-8");
		message += "&" + URLEncoder.encode("TOKEN", "UTF-8") + "="
				+ URLEncoder.encode(token, "UTF-8");

		return sendDataToPayPal(message, uRLConnection);
	}

	/**
	 * PayPal-API-method which is used to complete the transaction. This method
	 * is the third method to be called.
	 * 
	 * @param token
	 *            Token from paypal which is returned by SetExpresscheckout
	 * @param payerid
	 *            PayerId from paypal which is returned by
	 *            GetExpressCheckoutDetails
	 * @param total
	 *            Total amount of the transaction
	 * @return Return message of the called api-function.
	 * @throws Exception
	 */
	private String paypalAPIDoExpressCheckoutPayment(String token,
			String payerid, String total, String currencyCode) throws Exception {
		URLConnection uRLConnection;
		if ((uRLConnection = paypalURLConnection()) == null) {
			throw new Exception("Could not establish connection to paypal.");
		}

		String message = URLEncoder.encode("METHOD", "UTF-8") + "="
				+ URLEncoder.encode("DoExpressCheckoutPayment", "UTF-8");
		message += "&" + URLEncoder.encode("USER", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(USER), "UTF-8");
		message += "&" + URLEncoder.encode("PWD", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(PWD), "UTF-8");
		message += "&" + URLEncoder.encode("SIGNATURE", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(SIGNATURE), "UTF-8");
		message += "&" + URLEncoder.encode("VERSION", "UTF-8") + "="
				+ URLEncoder.encode(properties.getProperty(VERSION), "UTF-8");
		message += "&"
				+ URLEncoder.encode("PAYMENTREQUEST_0_PAYMENTACTION", "UTF-8")
				+ "="
				+ URLEncoder
						.encode(properties
								.getProperty("PAYMENTREQUEST_0_PAYMENTACTION"),
								"UTF-8");
		message += "&" + URLEncoder.encode("PAYMENTREQUEST_0_AMT", "UTF-8")
				+ "=" + URLEncoder.encode(total, "UTF-8");
		message += "&"
				+ URLEncoder.encode("PAYMENTREQUEST_0_CURRENCYCODE", "UTF-8")
				+ "=" + URLEncoder.encode(currencyCode, "UTF-8");
		message += "&" + URLEncoder.encode("TOKEN", "UTF-8") + "="
				+ URLEncoder.encode(token, "UTF-8");
		message += "&" + URLEncoder.encode("PAYERID", "UTF-8") + "="
				+ URLEncoder.encode(payerid, "UTF-8");

		return sendDataToPayPal(message, uRLConnection);
	}

	/*
	 * 
	 * Other helper-methods
	 */

	/**
	 * @param Number
	 *            as String
	 * @return String with two decimal positions
	 */
	private String twoDecimalPositions(String str) {
		if (str.matches("\\d+.\\d\\d") == false) {
			str = str + "0";
		}
		return str;
	}

	/**
	 * PayPalOrderDTO is transfered to PayPalOrder
	 * 
	 * @param payPalOrderDTO
	 *            Which should be transfered to PayPalOrder
	 * @return New PayPalOrder
	 */
	private PayPalOrder generatePayPalOrder(PayPalOrderDTO payPalOrderDTO) {
		ESystem eSystem = getSystem(payPalOrderDTO);
		ShopProduct shopProduct = getShopProduct(payPalOrderDTO);
		PayPalOrder payPalOrder = new PayPalOrder(payPalOrderDTO);
		if (payPalOrderDTO.getTransactionDTO() != null) {
			Account account = getAccount(payPalOrderDTO.getTransactionDTO()
					.getAccountDTO());
			payPalOrder.getPaymentTransaction().setAccount(account);
		}
		payPalOrder.getOrderInformation().setSystem(eSystem);
		payPalOrder.getOrderInformation().setShopProduct(shopProduct);
		return payPalOrder;
	}

	/**
	 * Get ShopProduct from PayPalOrderDTO
	 * 
	 * @param payPalOrderDTO
	 * @return ShopProduct
	 */
	private ShopProduct getShopProduct(PayPalOrderDTO payPalOrderDTO) {
		ESystem eSystem = eSystemCRUDBeanLocal.findById(payPalOrderDTO
				.getOrderInformationDTO().getShopProductDTO().getProduct()
				.getSystem().getId());
		ShopProduct shopProduct = shopProductCRUDBeanLocal
				.findByProductUinAndProductSystem(payPalOrderDTO
						.getOrderInformationDTO().getShopProductDTO()
						.getProduct().getUin(), eSystem);
		return shopProduct;
	}

	/**
	 * Get current System from PayPalOrderDTO
	 * 
	 * @param payPalOrderDTO
	 * @return system
	 */
	private ESystem getSystem(PayPalOrderDTO payPalOrderDTO) {
		ESystem eSystem = eSystemCRUDBeanLocal.findByName(payPalOrderDTO
				.getOrderInformationDTO().getSystem().getName());
		return eSystem;
	}

	/**
	 * Get PayPalOrder from PayPalOrderDTO
	 * 
	 * @param payPalOrderDTO
	 * @return PayPalOrder
	 */
	private PayPalOrder getPayPalOrder(PayPalOrderDTO payPalOrderDTO) {
		PayPalOrder payPalOrder = paypalorderCRUDBeanLocal
				.findById(payPalOrderDTO.getId());
		return payPalOrder;
	}

	/**
	 * Get Account from AccountDTO
	 * 
	 * @param AccountDTO
	 * @return Account
	 */
	private Account getAccount(AccountDTO accountDTO) {
		Account account = null;
		account = accountCRUDBean.findBySystemAndUserName(new ESystem(
				accountDTO.getSystem()), accountDTO.getUsername());
		return account;
	}

	/**
	 * read configuration parameter from db into propertie
	 * 
	 */
	private void setProperties(ProductAssociation productAssociation,
			ESystem eSystem) {

		properties = new Properties();

		properties
				.put(USER,
						getPayPalConfigurationCRUD()
								.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
										eSystem, USER, productAssociation)
								.getValue());
		properties
				.put(PWD,
						getPayPalConfigurationCRUD()
								.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
										eSystem, PWD, productAssociation)
								.getValue());
		properties
				.put(SIGNATURE,
						getPayPalConfigurationCRUD()
								.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
										eSystem, SIGNATURE, productAssociation)
								.getValue());
		properties
				.put(RETURNURL,
						getPayPalConfigurationCRUD()
								.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
										eSystem, RETURNURL, productAssociation)
								.getValue());
		properties
				.put(CANCELURL,
						getPayPalConfigurationCRUD()
								.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
										eSystem, CANCELURL, productAssociation)
								.getValue());
		properties
				.put(PAYPALAPI,
						getPayPalConfigurationCRUD()
								.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
										eSystem, PAYPALAPI, productAssociation)
								.getValue());
		properties
				.put(PAYPALPAY,
						getPayPalConfigurationCRUD()
								.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
										eSystem, PAYPALPAY, productAssociation)
								.getValue());
		properties
				.put(VERSION,
						getPayPalConfigurationCRUD()
								.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
										eSystem, VERSION, productAssociation)
								.getValue());
		properties
				.put(PAYMENTREQUEST_0_PAYMENTACTION,
						getPayPalConfigurationCRUD()
								.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
										eSystem,
										PAYMENTREQUEST_0_PAYMENTACTION,
										productAssociation).getValue());
	}

	public PayPalOrderCRUD getPaypalorderCRUDBeanLocal() {
		return paypalorderCRUDBeanLocal;
	}

	public void setPaypalorderCRUDBeanLocal(
			PayPalOrderCRUD paypalorderCRUDBeanLocal) {
		this.paypalorderCRUDBeanLocal = paypalorderCRUDBeanLocal;
	}

	public ShopProductCRUD getShopProductCRUDBeanLocal() {
		return shopProductCRUDBeanLocal;
	}

	public void setShopProductCRUDBeanLocal(
			ShopProductCRUD shopProductCRUDBeanLocal) {
		this.shopProductCRUDBeanLocal = shopProductCRUDBeanLocal;
	}

	public ESystemCRUD geteSystemCRUDBeanLocal() {
		return eSystemCRUDBeanLocal;
	}

	public void seteSystemCRUDBeanLocal(ESystemCRUD eSystemCRUDBeanLocal) {
		this.eSystemCRUDBeanLocal = eSystemCRUDBeanLocal;
	}

	public PaymentTransactionCRUD getPaymentTransactionCRUD() {
		return paymentTransactionCRUD;
	}

	public void setPaymentTransactionCRUD(
			PaymentTransactionCRUD paymentTransactionCRUD) {
		this.paymentTransactionCRUD = paymentTransactionCRUD;
	}

	public OrderInformationCRUD getOrderInformationCRUD() {
		return orderInformationCRUD;
	}

	public void setOrderInformationCRUD(
			OrderInformationCRUD orderInformationCRUD) {
		this.orderInformationCRUD = orderInformationCRUD;
	}

	public AccountCRUD getAccountCRUDBean() {
		return accountCRUDBean;
	}

	public void setAccountCRUDBean(AccountCRUD accountCRUDBean) {
		this.accountCRUDBean = accountCRUDBean;
	}

	public PayPalConfigurationCRUD getPayPalConfigurationCRUD() {
		return payPalConfigurationCRUD;
	}

	public void setPayPalConfigurationCRUD(
			PayPalConfigurationCRUD payPalConfigurationCRUD) {
		this.payPalConfigurationCRUD = payPalConfigurationCRUD;
	}

}
