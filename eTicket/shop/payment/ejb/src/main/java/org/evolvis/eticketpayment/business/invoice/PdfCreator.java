package org.evolvis.eticketpayment.business.invoice;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.evolvis.eticketpayment.model.entity.InvoiceField;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.FieldPosition;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

/**
 * 
 * copy a PDF document and fills the TextFields and the ImageField with given
 * 
 * Values Author: Piers-Axel Scholle, p.scholle@tarent.de
 */

public class PdfCreator {

	private final AcroFields fields;
	private final PdfReader reader;
	private final PdfStamper stamper;
	private final ByteArrayOutputStream baos = new ByteArrayOutputStream();
	private final PdfContentByte content;

	protected PdfCreator(byte[] src) throws IOException, DocumentException {
		throwExceptionWhenSourceIsNull(src);
		reader = new PdfReader(src);
		stamper = new PdfStamper(reader, baos);
		content = stamper.getOverContent(1);
		fields = stamper.getAcroFields();
	}

	private void throwExceptionWhenSourceIsNull(byte[] src) {
		if (src == null)
			throw new IllegalArgumentException("Source must not be null");
	}

	private void close() throws DocumentException, IOException {
		reader.close();
		stamper.close();
	}

	public static byte[] generate(byte[] src, Map<InvoiceField, Object> values) {
		try {
			PdfCreator p = new PdfCreator(src);
			return p.generate(values);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		} catch (DocumentException e) {
			throw new IllegalArgumentException(e);
		}
	}

	protected byte[] generate(Map<InvoiceField, Object> values)
			throws IOException, DocumentException {
		throwExceptionWhenValuesIsNull(values);
		setFields(values);
		close();
		return baos.toByteArray();
	}

	private void throwExceptionWhenValuesIsNull(Map<InvoiceField, Object> values) {
		if (values == null)
			throw new IllegalArgumentException("Values are null.");
	}

	private void setFields(Map<InvoiceField, Object> values)
			throws IOException, DocumentException {
		for (InvoiceField i : InvoiceField.values()) {
			List<FieldPosition> pos = fields.getFieldPositions(i.toString());
			ColumnText ct = new ColumnText(content);
			if (pos != null) {
				ct.setSimpleColumn(pos.get(0).position.getLeft(),
						pos.get(0).position.getBottom(),
						pos.get(0).position.getRight(),
						pos.get(0).position.getTop());
				ct.setText(new Phrase((String)values.get(i)));
				ct.go();
			}
		}
	}
}
