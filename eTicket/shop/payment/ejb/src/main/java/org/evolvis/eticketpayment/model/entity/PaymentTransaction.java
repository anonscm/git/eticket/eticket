package org.evolvis.eticketpayment.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticketpayment.model.entity.dto.PaymentTransactionDTO;

@Entity
public class PaymentTransaction {
	@Id
	@GeneratedValue
	private long id; // auto
	@NotNull
	@ManyToOne
	private Account account;

	public PaymentTransaction() {

	}

	public PaymentTransaction(PaymentTransactionDTO paymentTransactionDTO) {
		fromDto(paymentTransactionDTO);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Transient
	public PaymentTransactionDTO toDto() {
		return PaymentTransactionDTO
				.createTransactionDTOFromExsistingTransactionEntity(id,
						account.toAccountDTO());
	}

	@Transient
	public void fromDto(PaymentTransactionDTO paymentTransactionDTO) {
		// TODO Account Problem - Could not use
		// "new Account(transactionDTO.getAccountDTO())", because there is no
		// way to easily convert accountDTO to account. AccountDTO and Account
		// must be fixed so that Account.fromDto and Account.toDto can be used.
		// this.account = new Account(transactionDTO.getAccountDTO());

		if (paymentTransactionDTO.getId() > -1) {
			this.id = paymentTransactionDTO.getId();
		}
	}

}
