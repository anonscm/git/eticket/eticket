package org.evolvis.eticketpayment.model.crud.invoice;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticketpayment.model.entity.InvoiceNumber;

public interface InvoiceNumberCRUD extends GenericCUD<InvoiceNumber>{
	
	public InvoiceNumber findInvoiceNumber();

}
