package org.evolvis.eticketpayment.model.entity;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.evolvis.eticketpayment.model.entity.dto.PayPalOrderDTO;

/**
 * PayPalOrder represents an payment-transaction made with paypal.
 * 
 * @author ben
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllPayPalOrderWithStatusAndProductAssociation", query = "SELECT p FROM PayPalOrder p WHERE p.orderInformation.orderStatus = ?1 AND p.orderInformation.shopProduct.productAssociation  = ?2 "),
		@NamedQuery(name = "findAllPayPalOrderbyStatus", query = "SELECT p FROM PayPalOrder p WHERE p.orderInformation.orderStatus = ?1"),
		@NamedQuery(name = "findByTokenAndAccountAndStatus", query = "SELECT p FROM PayPalOrder p WHERE p.token = ?1 AND p.paymentTransaction.account = ?2 AND  p.orderInformation.orderStatus LIKE(?3)"),
		@NamedQuery(name = "findById", query = "SELECT p FROM PayPalOrder p WHERE p.id = ?1") })
public class PayPalOrder {
	public enum PayPalOrderCheckoutStatus {
		PaymentActionNotInitiated("PaymentActionNotInitiated"), PaymentActionFailed(
				"PaymentActionFailed"), PaymentActionInProgress(
				"PaymentActionInProgress"), PaymentActionCompleted(
				"PaymentActionCompleted");

		private String type;

		private static Map<String, PayPalOrderCheckoutStatus> enumToString = new HashMap<String, PayPalOrder.PayPalOrderCheckoutStatus>();

		static {
			for (PayPalOrderCheckoutStatus pps : values())
				enumToString.put(pps.toString(), pps);
		}

		PayPalOrderCheckoutStatus(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return type;
		}

		public static PayPalOrderCheckoutStatus fromString(String type) {
			return enumToString.get(type);
		}
	}

	@Id
	@GeneratedValue
	private long id; // auto
	private String transactionId; // fromDto()
	private String payerId; // fromDto()
	@NotNull
	private String token; // fromDto()
	private String payPalEmailAddress;
	@OneToOne(cascade = { CascadeType.REMOVE })
	private OrderInformation orderInformation;
	@OneToOne(cascade = { CascadeType.REMOVE })
	private PaymentTransaction paymentTransaction;

	public PayPalOrder() {

	}

	public PayPalOrder(PayPalOrderDTO payPalOrderDTO) {
		fromDto(payPalOrderDTO);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;

	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getPayerId() {
		return payerId;
	}

	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPayPalEmailAddress() {
		return payPalEmailAddress;
	}

	public void setPayPalEmailAddress(String payPalEmailAddress) {
		this.payPalEmailAddress = payPalEmailAddress;
	}

	public OrderInformation getOrderInformation() {
		return orderInformation;
	}

	public void setOrderInformation(OrderInformation orderInformation) {
		this.orderInformation = orderInformation;
	}

	public PaymentTransaction getPaymentTransaction() {
		return paymentTransaction;
	}

	public void setPaymentTransaction(PaymentTransaction paymentTransaction) {
		this.paymentTransaction = paymentTransaction;
	}

	@Transient
	public PayPalOrderDTO toDto() {
		if (paymentTransaction != null)
			return PayPalOrderDTO
					.createPayPalOrderDTOFromExsistingPayPalOrderEntity(id,
							token, payPalEmailAddress, payerId, transactionId,
							orderInformation.toDto(),
							paymentTransaction.toDto());
		else
			return PayPalOrderDTO
					.createPayPalOrderDTOFromExsistingPayPalOrderEntity(id,
							token, payPalEmailAddress, payerId, transactionId,
							orderInformation.toDto(), null);

	}

	@Transient
	public void fromDto(PayPalOrderDTO payPalOrderDTO) {
		this.payerId = payPalOrderDTO.getPayerId();
		this.token = payPalOrderDTO.getToken();
		this.transactionId = payPalOrderDTO.getTransactionId();
		this.orderInformation = new OrderInformation(
				payPalOrderDTO.getOrderInformationDTO());
		// TODO Account Problem - Could not use "new PaymentTransaction",
		// because there is no way to easily convert accountDTO to account.
		// AccountDTO and Account must be fixed so that Account.fromDto and
		// Account.toDto can be used.
		if (this.paymentTransaction == null) {
			if (payPalOrderDTO.getTransactionDTO() != null)
				this.paymentTransaction = new PaymentTransaction(
						payPalOrderDTO.getTransactionDTO());
		} else {
			if (this.paymentTransaction.getAccount() == null) {
				this.paymentTransaction = new PaymentTransaction(
						payPalOrderDTO.getTransactionDTO());
			}
		}

		if (payPalOrderDTO.getId() > -1) {
			this.id = payPalOrderDTO.getId();
		}

	}

}
