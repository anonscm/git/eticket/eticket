package org.evolvis.eticketpayment.model.crud.paymenttransaction;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticketpayment.model.entity.PaymentTransaction;

public interface PaymentTransactionCRUD extends GenericCUD<PaymentTransaction> {

}
