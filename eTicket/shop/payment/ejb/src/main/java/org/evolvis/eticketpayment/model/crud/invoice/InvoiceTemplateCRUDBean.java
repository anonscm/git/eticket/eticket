package org.evolvis.eticketpayment.model.crud.invoice;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticketpayment.model.entity.InvoiceTemplate;

@Stateless
@Remote(InvoiceTemplateCRUD.class)
public class InvoiceTemplateCRUDBean extends GenericCUDSkeleton<InvoiceTemplate> implements InvoiceTemplateCRUD {

	private static final long serialVersionUID = 8300703934057542667L;

	@Override
	public InvoiceTemplate findByLocale(Locale locale) {
		return buildNamedQueryGetSingleResult("findInvoiceTemplateByLocale", locale);
	}	

}
