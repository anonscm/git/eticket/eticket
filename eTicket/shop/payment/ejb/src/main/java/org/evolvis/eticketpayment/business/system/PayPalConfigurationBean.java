package org.evolvis.eticketpayment.business.system;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.system.ESystemCRUD;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticketpayment.model.crud.platform.PayPalConfigurationCRUD;
import org.evolvis.eticketpayment.model.entity.PayPalConfiguration;
import org.evolvis.eticketshop.model.entity.ProductAssociation;

@Stateless
@Remote(PayPalConfigurationBeanService.class)
public class PayPalConfigurationBean implements PayPalConfigurationBeanService {
	private static final long serialVersionUID = -714633956002861764L;

	@EJB
	private PayPalConfigurationCRUD payPalConfigurationCRUD;

	@EJB
	private ESystemCRUD eSystemCRUDBeanLocal;
	
	@Override
	public String getPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
			ESystemDTO system, String key, ProductAssociation productAssociation) {
		ESystem eSystem = eSystemCRUDBeanLocal.findByName(system.getName());
		return payPalConfigurationCRUD.findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(eSystem, key, productAssociation).getValue();
	}
	
	@Override
	public void insertConfiguration(String key, String value,
			ProductAssociation productAssociation, ESystemDTO system) {
		PayPalConfiguration payPalConfiguration = new PayPalConfiguration();
		payPalConfiguration.setKey(key);
		payPalConfiguration.setValue(value);
		payPalConfiguration.setProductAssociation(productAssociation);
		payPalConfiguration.setSystem(getSystem(system));
		
		payPalConfigurationCRUD.insert(payPalConfiguration);
	}
	

	private ESystem getSystem(ESystemDTO system) {
		return eSystemCRUDBeanLocal.findByName(system.getName());
	}
	
	public void setPayPalConfigurationCRUD(
			PayPalConfigurationCRUD payPalConfigurationCRUD) {
		this.payPalConfigurationCRUD = payPalConfigurationCRUD;
	}

	public void seteSystemCRUDBeanLocal(ESystemCRUD eSystemCRUDBeanLocal) {
		this.eSystemCRUDBeanLocal = eSystemCRUDBeanLocal;
	}
}
