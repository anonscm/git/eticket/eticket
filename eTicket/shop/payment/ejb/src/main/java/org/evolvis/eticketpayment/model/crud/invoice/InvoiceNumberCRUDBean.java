package org.evolvis.eticketpayment.model.crud.invoice;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticketpayment.model.entity.InvoiceNumber;

@Stateless
@Remote(InvoiceNumberCRUD.class)
public class InvoiceNumberCRUDBean extends GenericCUDSkeleton<InvoiceNumber> implements InvoiceNumberCRUD {

	private static final long serialVersionUID = 8300703934057542667L;

	@Override
	public InvoiceNumber findInvoiceNumber() {
		try{
			return buildNamedQueryGetSingleResult("findInvoiceNumber");
		}catch (Exception e) {
			return null;
		}
	}

		

}
