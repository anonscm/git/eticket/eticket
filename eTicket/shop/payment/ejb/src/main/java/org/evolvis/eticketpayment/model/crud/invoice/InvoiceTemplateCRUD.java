package org.evolvis.eticketpayment.model.crud.invoice;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Locale;
import org.evolvis.eticketpayment.model.entity.InvoiceTemplate;

public interface InvoiceTemplateCRUD extends GenericCUD<InvoiceTemplate>{
	
	public InvoiceTemplate findByLocale(Locale locale);

}
