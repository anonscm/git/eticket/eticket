package org.evolvis.eticketpayment.model.crud.platform;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticketpayment.model.entity.PayPalConfiguration;
import org.evolvis.eticketshop.model.entity.ProductAssociation;

@Stateless
@Local(PayPalConfigurationCRUD.class)
public class PayPalConfigurationCRUDBean extends
		GenericCUDSkeleton<PayPalConfiguration> implements
		PayPalConfigurationCRUD {
	private static final long serialVersionUID = -1239715099249548747L;

	@Override
	public PayPalConfiguration findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(
			ESystem system, String key, ProductAssociation productAssociation) {
		return buildNamedQueryGetSingleResult(
				"findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation", system, key, productAssociation);
	}

	@Override
	public List<PayPalConfiguration> listPayPalConfigurationParameterBySystemAndProductAssociation(
			ESystem system, ProductAssociation productAssociation) {
		return buildNamedQueryGetResults(
				"listPayPalConfigurationParameterBySystemAndProductAssociation", system, productAssociation);
	}
}
