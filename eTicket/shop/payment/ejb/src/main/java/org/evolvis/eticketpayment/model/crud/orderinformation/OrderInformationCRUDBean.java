package org.evolvis.eticketpayment.model.crud.orderinformation;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.evolvis.eticket.model.crud.GenericCUDSkeleton;
import org.evolvis.eticketpayment.model.entity.OrderInformation;

@Stateless
@Local(OrderInformationCRUD.class)
public class OrderInformationCRUDBean extends GenericCUDSkeleton<OrderInformation> implements OrderInformationCRUD {
	private static final long serialVersionUID = -4911337843532991463L;
}
