package org.evolvis.eticketpayment.model.crud.paypalorder;

import java.util.List;

import org.evolvis.eticket.model.crud.GenericCUD;
import org.evolvis.eticket.model.entity.Account;
import org.evolvis.eticketpayment.model.entity.OrderStatus;
import org.evolvis.eticketpayment.model.entity.PayPalOrder;
import org.evolvis.eticketshop.model.entity.ProductAssociation;

/**
 * PayPalOrderCRUD handels database-opreations which are associated with
 * PayPalOrder
 * 
 * @author ben
 * 
 */
public interface PayPalOrderCRUD extends GenericCUD<PayPalOrder> {

	/**
	 * Find a specific PayPalOrder in database.
	 * 
	 * @param id
	 *            The id of the PayPalOrder
	 * @return PayPalOrder
	 */
	PayPalOrder findById(long id);

	/**
	 * Find a specific PayPalOrder in database.
	 * 
	 * @param token
	 * @param account
	 * @param status
	 * @return PayPalOrder
	 */
	public PayPalOrder findByTokenAndAccountAndStatus(String token, Account account,
			String status);

	
	/**
	 * Find a List of PayPalOrder in database
	 *
	 * @param status
	 * @param productAssociation
	 * @return List of PayPalOrder
	 */	
	public List<PayPalOrder> findWithStatusAndProductAssociation(OrderStatus status, ProductAssociation productAssociation);


	/**Gets all open Paypalorders
	 * 
	 * @return all open PayPalOrders
	 */
	public List<PayPalOrder> findByOrderStatus(OrderStatus status);
}
