package org.evolvis.eticketpayment.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

import org.evolvis.eticket.model.entity.ESystem;
import org.evolvis.eticketshop.model.entity.ProductAssociation;

@Entity
@NamedQueries({
	@NamedQuery(name = "findPayPalConfigurationParameterBySystemAndKeyAndProductAssociation", query = "SELECT c FROM PayPalConfiguration c WHERE c.system = ?1 AND c.key = ?2 AND c.productAssociation = ?3"),
	@NamedQuery(name = "listPayPalConfigurationParameterBySystemAndProductAssociation", query = "SELECT c FROM PayPalConfiguration c WHERE c.system = ?1 AND c.productAssociation = ?2") })
public class PayPalConfiguration {
	@Id
	@GeneratedValue
	private long id;
	@NotNull
	private String key;
	@NotNull
	private String value;
	@ManyToOne
	@NotNull
	private ESystem system;
	@NotNull
	private ProductAssociation productAssociation;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public ESystem getSystem() {
		return system;
	}

	public void setSystem(ESystem system) {
		this.system = system;
	}

	public ProductAssociation getProductAssociation() {
		return productAssociation;
	}

	public void setProductAssociation(ProductAssociation productAssociation) {
		this.productAssociation = productAssociation;
	}

	@Override
	public String toString() {
		return value;
	}
}
