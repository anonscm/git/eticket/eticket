package org.evolvis.eticketpayment.model.entity.dto;

import java.io.Serializable;
import java.util.Date;

import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;
import org.evolvis.eticketpayment.model.entity.OrderStatus;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;

public final class OrderInformationDTO implements Serializable {
	private static final long serialVersionUID = 6834854422033194090L;
	private final long id;
	private final OrderStatus orderStatus;
	private final ESystemDTO eSystemDTO;
	private final ShopProductDTO shopProductDTO;
	private final ProductLocaleDTO productLocaleDTO;
	private final long amount;
	private final Date date;
	
	private OrderInformationDTO(long id, OrderStatus orderStatus, ESystemDTO eSystemDTO, ShopProductDTO shopProductDTO, ProductLocaleDTO productLocaleDTO, long amount, Date date) {
		if (orderStatus == null || eSystemDTO == null || shopProductDTO == null) {
			throw new IllegalArgumentException("OrderStatus, ESystemDTO and ShopProductDTO must be set.");
		}
		
		this.id = id;
		this.orderStatus = orderStatus;
		this.eSystemDTO = eSystemDTO;
		this.shopProductDTO = shopProductDTO;
		this.productLocaleDTO = productLocaleDTO;
		this.amount = amount;
		this.date = date;
	}

	/**
	 * Creates new instance of OrderDTO. Typically called in frontend.
	 * 
	 * @param orderStatus
	 * @param eSystemDTO
	 * @param shopProductDTO
	 * @param amount
	 * @return
	 */
	public static OrderInformationDTO createNewOrderDTO(OrderStatus orderStatus, ESystemDTO eSystemDTO, ShopProductDTO shopProductDTO, ProductLocaleDTO productLocaleDTO, long amount ) {
		return new OrderInformationDTO(-1, orderStatus, eSystemDTO, shopProductDTO, productLocaleDTO, amount, null);
	}
	
	/**
	 * Creates OrderDTO from an existing Order which is already
	 * stored in dbs. This method is typically called from the backend.
	 * 
	 * @param id
	 * @param orderStatus
	 * @param eSystemDTO
	 * @param shopProductDTO
	 * @param amount
	 * @param date
	 * @return
	 */
	public static OrderInformationDTO createOrderDTOFromExsistingOrderEntity(long id, OrderStatus orderStatus, ESystemDTO eSystemDTO, ShopProductDTO shopProductDTO, ProductLocaleDTO productLocaleDTO, long amount, Date date) {
		return new OrderInformationDTO(id, orderStatus, eSystemDTO, shopProductDTO, productLocaleDTO, amount, date);
	}
	
	public long getId() {
		return id;
	}
	
	public OrderStatus getOrderStatus() {
		return orderStatus;
	}
	
	public ESystemDTO getSystem() {
		return eSystemDTO;
	}

	public ShopProductDTO getShopProductDTO() {
		return shopProductDTO;
	}

	public Date getDate() {
		return date;
	}
	
	public ProductLocaleDTO getProductLocaleDTO() {
		return productLocaleDTO;
	}

	public long getAmount() {
		return amount;
	}
}
