package org.evolvis.eticketpayment.business.paypalorder;

public class PayPalException extends Exception {
	private static final long serialVersionUID = 1L;
	private String payPalMessage;
	
	public PayPalException() {
		super();
	}
	
	public PayPalException(String exception, String payPalMessage) {
		super(exception);
		this.payPalMessage = payPalMessage;
	}

	public String getPayPalMessage() {
		return payPalMessage;
	}

	public void setPayPalMessage(String payPalMessage) {
		this.payPalMessage = payPalMessage;
	}
}
