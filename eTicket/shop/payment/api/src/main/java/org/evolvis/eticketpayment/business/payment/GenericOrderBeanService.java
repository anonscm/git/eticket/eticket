package org.evolvis.eticketpayment.business.payment;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticket.model.entity.dto.ProductLocaleDTO;

/**
 * With {@link GenericOrderBeanService} you are able to initiate a order
 * transaction in a system. You must add (Annotation) ExcludeClassInterceptors,
 * cause normal user must be able to execute startPaymentProcess.
 * 
 * @author ben
 * 
 */
public interface GenericOrderBeanService {
	/**
	 * Starts the payment process.
	 * 
	 * @param account
	 *            The account which initiated the payment process.
	 * @param eSystemDTO
	 *            The current system.
	 * @param product_uin
	 *            The uni of the product.
	 * @return A URL which is pointed to the payment provider.
	 * @throws Exception 
	 */

	public String[] startPaymentProcess(AccountDTO accountDTO,ProductLocaleDTO productLocaleDTO, String product_uin,
			ESystemDTO eSystemDTO, int quantity) throws Exception;
}
