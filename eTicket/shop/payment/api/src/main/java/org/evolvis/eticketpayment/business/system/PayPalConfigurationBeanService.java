package org.evolvis.eticketpayment.business.system;

import java.io.Serializable;

import org.evolvis.eticket.model.entity.dto.ESystemDTO;
import org.evolvis.eticketshop.model.entity.ProductAssociation;

public interface PayPalConfigurationBeanService extends Serializable {

	public String getPayPalConfigurationParameterBySystemAndKeyAndProductAssociation(ESystemDTO system, String key, ProductAssociation productAssociation);
	
	public void insertConfiguration(String key, String value, ProductAssociation productAssociation, ESystemDTO system);
}
