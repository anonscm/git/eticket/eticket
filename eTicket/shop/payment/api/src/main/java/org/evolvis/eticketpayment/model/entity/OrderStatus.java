package org.evolvis.eticketpayment.model.entity;

import java.util.HashMap;
import java.util.Map;

/**
 *  Order can have the defined status.
 * 
 * @author ben
 *
 */
public enum OrderStatus {
	OPEN("OPEN"), PENDING("PENDING"), COMPLETED("COMPLETED"), COMPLETED_AND_DELIVERED("COMPLETED_AND_DELIVERED"), FAILURE("FAILURE"), CANCELED("CANCELED");

	private String type;

	private static Map<String, OrderStatus> enumToString = new HashMap<String, OrderStatus>();
	static {
		for (OrderStatus pps : values())
			enumToString.put(pps.toString(), pps);
	}

	OrderStatus(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return type;
	}

	public static OrderStatus fromString(String type) {
		return enumToString.get(type);
	}
}