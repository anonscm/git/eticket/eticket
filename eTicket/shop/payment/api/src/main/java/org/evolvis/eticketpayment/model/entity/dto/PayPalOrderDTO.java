package org.evolvis.eticketpayment.model.entity.dto;

import java.io.Serializable;


/**
 * This is data transfer object for PayPalOrder. PayPalOrder persists
 * transactions made with PayPal.
 * 
 * @author ben
 * 
 */
public final class PayPalOrderDTO implements Serializable {

	private static final long serialVersionUID = 7852134151864285825L;
	private final long id;
	private final String transactionId;
	private final String payerId;
	private final String token;
	private final String payPalEmailAddress;
	private final OrderInformationDTO orderInformationDTO;
	private final PaymentTransactionDTO paymentTransactionDTO;


	/**
	 * Creates new instance of PayPalOrderDTO. Typically called in frontend.
	 * 
	 * @param token
	 *            token that is generated by paypal
	 *            
 	 * @param payPalEmailAddress
	 * 			  EMail-Address of PayPal-Account
	 * @param payerId
	 *            payerid is received from paypal
	 * @param transactionID
	 *            id from paypal
	 * @param orderInformationDTO
	 * 			  Corresponding orderDTO Object.
	 * @param transferHistoryDTO
	 * 			  Corresponding transactionDTO. Could be null.
	 * @return a single instance of PayPalOrderDTO.
	 * @throws IllegalArgumentException
	 *             when token, status or account is null.
	 */
	public static PayPalOrderDTO createNewPayPalOrderDTO(String token, String payPalEmailAddress,
			String payerId, String transactionID, OrderInformationDTO orderInformationDTO, PaymentTransactionDTO paymentTransactionDTO) {
		return new PayPalOrderDTO(-1, token, payPalEmailAddress,  payerId, transactionID, orderInformationDTO, paymentTransactionDTO);
	}

	/**
	 * Creates PayPalOrderDTO from an existing PayPalOrder which is already
	 * stored in dbs. This method is typically called from the backend.
	 * 
	 * @param id
	 *            The unique primary-key set by the db-system.
	 * @param token
	 *            token that is generated by paypal
	 * @param payPalEmailAddress
	 * 			  EMail-Address of PayPal-Account
	 * @param payerId
	 *            payerid is received from paypal
	 * @param transactionID
	 *            id from paypal
	 * @param orderInformationDTO
	 * 			  Corresponding orderDTO Object.
	 * @param transferHistoryDTO
	 * 			  Corresponding transactionDTO. Could be null.

	 * @return a single instance of PayPalOrderDTO.
	 * @throws IllegalArgumentException
	 *             when token, status or account is null.
	 */
	public static PayPalOrderDTO createPayPalOrderDTOFromExsistingPayPalOrderEntity(
			long id, String token, String payPalEmailAddress, String payerId, String transactionID, OrderInformationDTO orderInformationDTO, PaymentTransactionDTO paymentTransactionDTO) {
		return new PayPalOrderDTO(id, token, payPalEmailAddress, payerId, transactionID, orderInformationDTO, paymentTransactionDTO);
	}

	private PayPalOrderDTO(long id, String token, String payPalEmailAddress, String payerId,
			String transactionId, OrderInformationDTO orderInformationDTO, PaymentTransactionDTO paymentTransactionDTO) {
		if (token == null || orderInformationDTO == null) {
			throw new IllegalArgumentException("Token and OrderDTO must be set.");

		}

		this.id = id;
		this.token = token;
		this.payPalEmailAddress = payPalEmailAddress;
		this.payerId = payerId;

		this.transactionId = transactionId;
		this.orderInformationDTO = orderInformationDTO;
		this.paymentTransactionDTO = paymentTransactionDTO;

	}

	public long getId() {
		return id;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public String getPayerId() {
		return payerId;
	}

	public String getToken() {
		return token;
	}


	public OrderInformationDTO getOrderInformationDTO() {
		return orderInformationDTO;
	}

	public PaymentTransactionDTO getTransactionDTO() {
		return paymentTransactionDTO;
	}

	public String getPayPalEmailAddress() {
		return payPalEmailAddress;
	}
}
