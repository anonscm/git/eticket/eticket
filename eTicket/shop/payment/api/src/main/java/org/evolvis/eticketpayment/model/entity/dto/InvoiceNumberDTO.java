package org.evolvis.eticketpayment.model.entity.dto;

import java.io.Serializable;

public final class InvoiceNumberDTO implements Serializable{
	
	private static final long serialVersionUID = -2702840855357049928L;
	private final long maxNumber;
	private final long currentNumber;
	
	private InvoiceNumberDTO(long maxNumber, long currentNumber) {
		super();
		this.maxNumber = maxNumber;
		this.currentNumber = currentNumber;
	}
	
	public static InvoiceNumberDTO init(long maxNumber, long currentNumber){
		return new InvoiceNumberDTO(maxNumber, currentNumber);
	}
	
	public long getMaxNumber() {
		return maxNumber;
	}	
	public long getCurrentNumber() {
		return currentNumber;
	}	
}
