package org.evolvis.eticketpayment.business.paypalorder;

import java.util.List;

import org.evolvis.eticket.model.entity.dto.AccountDTO;
import org.evolvis.eticketpayment.business.payment.GenericOrderBeanService;
import org.evolvis.eticketpayment.model.entity.OrderStatus;
import org.evolvis.eticketpayment.model.entity.dto.PayPalOrderDTO;

/**
 * With {@link PayPalOrderBeanService} you are able to create, read, update and
 * delete a paypalorder in a system. The CUD parts are limited to user who have
 * at least 'productadmin' rights.
 * 
 * @author ben
 * 
 */
public interface PayPalOrderBeanService extends GenericOrderBeanService {

	/**
	 * creates a new paypalorder in system.
	 * 
	 * @param productAdmin
	 *            a account of a product admin, the password is needed in plain
	 *            text.
	 * @param paypalorderDTO
	 *            the paypalorder to create.
	 */
	public void create(AccountDTO productAdmin, PayPalOrderDTO paypalorderDTO);

	/**
	 * updates a paypalorder, since the paypalorder gets found.
	 * 
	 * @param productAdmin
	 *            a account of a product admin, the password is needed in plain
	 *            text.
	 * 
	 * @param paypalorderDTO
	 *            the paypalorder to update.
	 */
	public void update(AccountDTO productAdmin, PayPalOrderDTO paypalorderDTO);

	/**
	 * deletes a paypalorder.
	 * 
	 * @param productAdmin
	 *            a account of a product admin, the password is needed in plain
	 *            text.
	 * @param paypalorder
	 *            the paypalorder to delete.
	 */
	public void delete(AccountDTO productAdmin, PayPalOrderDTO paypalorderDTO);

	/**
	 * Complete the transaction GetExpressCheckoutDetails and
	 * DoExpressCheckoutPayment from PayPal API must be called.
	 * 
	 * @param id - primary key
	 * @param token
	 * @param payerId
	 * @return PayPalOrderDTO
	 * @throws PayPalException
	 * @throws Exception
	 */
	public PayPalOrderDTO completeTransaction(long id, String token, String payerId) throws PayPalException, Exception;
	
	/**
	 * Check the status of an paypal-order via paypal-api.
	 * 
	 * @param payPalOrderDTO - to check
	 * @return OrderStatus - status of payPalOrderDTO
	 * @throws PayPalException
	 * @throws Exception
	 */
	public OrderStatus checkStatusOfPayPalOrder(PayPalOrderDTO payPalOrderDTO) throws PayPalException, Exception;

	
	/**Gets the Order by the selected type
	 * 
	 * @param AdminAccount - must be an admin
	 * @param type  valid String type
	 * @return List of Payparorders
	 */
	public List<PayPalOrderDTO> getOrders(AccountDTO AdminAccount, OrderStatus type);

	
	/**This Method updates the selected Paypalorder.Orderinformation Orderstatus .
	 * 
	 * @param paypalId The ID off the PaypalOrder, 
	 * @param updatedStatus   new OrderStatus
	 */
	public void updateOrderstatus(long paypalId, OrderStatus updatedStatus);

}
