package org.evolvis.eticketpayment.model.entity.dto;

import java.io.Serializable;

import org.evolvis.eticket.model.entity.dto.LocaleDTO;

public final class InvoiceTemplateDTO implements Serializable {

	private static final long serialVersionUID = -5553895207800593130L;
	private final byte[] binaryData;
	private final String description;
	private final LocaleDTO locale;

	private InvoiceTemplateDTO(byte[] binaryData, String description,
			LocaleDTO locale) {
		this.binaryData = binaryData;
		this.description = description;
		this.locale = locale;
	}

	public static InvoiceTemplateDTO init(byte[] binaryData,
			String description, LocaleDTO locale) {
		return new InvoiceTemplateDTO(binaryData, description, locale);
	}

	public byte[] getBinaryData() {
		return binaryData;
	}

	public String getDescription() {
		return description;
	}

	public LocaleDTO getLocale() {
		return locale;
	}
}
