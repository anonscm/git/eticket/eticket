package org.evolvis.eticketpayment.business.invoice;

import java.util.Map;

import org.evolvis.eticket.model.entity.dto.LocaleDTO;
import org.evolvis.eticketpayment.model.entity.InvoiceField;
import org.evolvis.eticketpayment.model.entity.dto.InvoiceNumberDTO;
import org.evolvis.eticketpayment.model.entity.dto.InvoiceTemplateDTO;
import org.evolvis.eticketshop.model.entity.dto.ShopProductDTO;

public interface InvoiceBeanService {
	
	public byte[] generateInvoice(ShopProductDTO shopProductDTO, LocaleDTO localeDTO, Map<InvoiceField, Object> values);
	
	public void cerateInvoiceTemplate(InvoiceTemplateDTO invoiceTemplateDTO);
	public void updateInvoiceTemplate(InvoiceTemplateDTO invoiceTemplateDTO);
	public void deleteInvoiceTemplate(InvoiceTemplateDTO invoiceTemplateDTO);
	
	public void createOrUpdateInvoiceNumber(InvoiceNumberDTO invoiceNumberDTO);
	public void deleteInvoiceNumber(InvoiceNumberDTO invoiceNumberDTO);
	public String getNextInvoiceNumber();
	
}
