package org.evolvis.eticketpayment.model.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Payment metohds are defined in this enum.
 * 
 * @author ben
 *
 */
public enum PaymentMethod {
	PayPalOrderBean("PayPal");
	
	private String type;

	private static Map<String, PaymentMethod> enumToString = new HashMap<String, PaymentMethod>();
	static {
		for (PaymentMethod pps : values())
			enumToString.put(pps.toString(), pps);
	}

	PaymentMethod(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return type;
	}

	public static PaymentMethod fromString(String type) {
		return enumToString.get(type);
	}
}