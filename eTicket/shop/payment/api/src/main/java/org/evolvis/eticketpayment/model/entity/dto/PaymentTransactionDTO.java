package org.evolvis.eticketpayment.model.entity.dto;

import java.io.Serializable;

import org.evolvis.eticket.model.entity.dto.AccountDTO;

public final class PaymentTransactionDTO implements Serializable {

	private static final long serialVersionUID = 104108014167047937L;
	private final long id;
	private final AccountDTO accountDTO;

	private PaymentTransactionDTO(long id, AccountDTO accountDTO) {
		if (accountDTO == null) {
			throw new IllegalArgumentException("AccountDTO must be set.");
		}
		this.id = id;
		this.accountDTO = accountDTO;
	}

	/**
	 * Creates new instance of TransactionDTO. Typically called in frontend.
	 * 
	 * @param accountDTO
	 * @return
	 */
	public static PaymentTransactionDTO createNewTransactionDTO(AccountDTO accountDTO) {
		return new PaymentTransactionDTO(-1, accountDTO);
	}

	/**
	 * Creates TransactionDTO from an existing Transaction which is already
	 * stored in dbs. This method is typically called from the backend.
	 * 
	 * @param id
	 * @param accountDTO
	 * @return
	 */
	public static PaymentTransactionDTO createTransactionDTOFromExsistingTransactionEntity(
			long id, AccountDTO accountDTO) {
		return new PaymentTransactionDTO(id, accountDTO);
	}

	public long getId() {
		return id;
	}

	public AccountDTO getAccountDTO() {
		return accountDTO;
	}
}
