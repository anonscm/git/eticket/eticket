package org.evolvis.eticketpayment.model.entity;

import java.util.HashMap;
import java.util.Map;

public enum InvoiceField {

	TAXNUMBER("taxnumber"), DATE("date"), INVOICENUMBER("invoicenumber"), PRODUCTNAME(
			"productname"), AMOUNT("amount"), DELIVERYDATE("deliverydate"), TAXRATE(
			"taxrate");

	private String type;

	InvoiceField(String type) {
		this.type = type;
	}

	private static final Map<String, InvoiceField> stringToEnum = new HashMap<String, InvoiceField>();
	static {// initialise map from const name to enum const
		for (InvoiceField tP : values())
			stringToEnum.put(tP.toString(), tP);
	}

	public static InvoiceField fromString(String type) {
		return stringToEnum.get(type);
	}

	@Override
	public String toString() {
		return type;
	}

	public static InvoiceField fromInt(int size) {
		return values()[size - 1];
	}

}
